# 1 "./kwset.cil.c"
# 1 "/home/yavuz/crest/benchmarks/grep-2.2/src//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./kwset.cil.c"



extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 23 "system.h"
typedef void *ptr_t;
# 147 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef long ptrdiff_t;
# 23 "kwset.h"
struct kwsmatch {
   int index ;
   char *beg[1] ;
   size_t size[1] ;
};
# 30 "kwset.h"
typedef ptr_t kwset_t;
# 161 "obstack.h"
struct _obstack_chunk {
   char *limit ;
   struct _obstack_chunk *prev ;
   char contents[4] ;
};
# 168 "obstack.h"
struct obstack {
   long chunk_size ;
   struct _obstack_chunk *chunk ;
   char *object_base ;
   char *next_free ;
   char *chunk_limit ;
   ptrdiff_t temp ;
   int alignment_mask ;
   struct _obstack_chunk *(*chunkfun)(void * , long ) ;
   void (*freefun)(void * , struct _obstack_chunk * ) ;
   void *extra_arg ;
   unsigned int use_extra_arg : 1 ;
   unsigned int maybe_empty_object : 1 ;
   unsigned int alloc_failed : 1 ;
};
# 50 "kwset.c"
struct trie;
# 50 "kwset.c"
struct tree {
   struct tree *llink ;
   struct tree *rlink ;
   struct trie *trie ;
   unsigned char label ;
   char balance ;
};
# 60 "kwset.c"
struct trie {
   unsigned int accepting ;
   struct tree *links ;
   struct trie *parent ;
   struct trie *next ;
   struct trie *fail ;
   int depth ;
   int shift ;
   int maxshift ;
};
# 73 "kwset.c"
struct kwset {
   struct obstack obstack ;
   int words ;
   struct trie *trie ;
   int mind ;
   int maxd ;
   unsigned char delta[256] ;
   struct trie *next[256] ;
   char *target ;
   int mind2 ;
   char *trans ;
};
# 146 "kwset.c"
enum __anonenum_dirs_25 {
    L = 0,
    R = 1
} ;
# 483 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void ( __attribute__((__leaf__)) free)(void *__ptr ) ;
# 515 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__, __noreturn__)) void ( __attribute__((__leaf__)) abort)(void) ;
# 96 "/usr/include/string.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__nonnull__(1), __leaf__)) memchr)(void const *__s ,
                                                                                               int __c ,
                                                                                               size_t __n ) __attribute__((__pure__)) ;
# 105 "../intl/libintl.h"
extern char *dcgettext(char const *__domainname , char const *__msgid , int __category ) ;
# 36 "kwset.h"
kwset_t kwsalloc(char *trans ) ;
# 41 "kwset.h"
char *kwsincr(kwset_t kws , char *text , size_t len ) ;
# 45 "kwset.h"
char *kwsprep(kwset_t kws ) ;
# 53 "kwset.h"
char *kwsexec(kwset_t kws , char *text , size_t size , struct kwsmatch *kwsmatch ) ;
# 56 "kwset.h"
void kwsfree(kwset_t kws ) ;
# 202 "obstack.h"
extern void _obstack_newchunk(struct obstack * , int ) ;
# 204 "obstack.h"
extern int _obstack_begin(struct obstack * , int , int , void *(*)(long ) , void (*)(void * ) ) ;
# 230 "obstack.h"
extern void obstack_free(struct obstack *obstack , void *block ) ;
# 40 "kwset.c"
extern char *xmalloc() ;
# 88 "kwset.c"
static void enqueue(struct tree *tree , struct trie **last ) ;
# 89 "kwset.c"
static void treefails(struct tree *tree , struct trie *fail , struct trie *recourse ) ;
# 90 "kwset.c"
static void treedelta(struct tree *tree , unsigned int depth , unsigned char *delta ) ;
# 91 "kwset.c"
static int hasevery(struct tree *a , struct tree *b ) ;
# 92 "kwset.c"
static void treenext(struct tree *tree , struct trie **next ) ;
# 93 "kwset.c"
static char *bmexec(kwset_t kws , char *text , size_t size ) ;
# 94 "kwset.c"
static char *cwexec(kwset_t kws , char *text , size_t len , struct kwsmatch *kwsmatch ) ;
# 98 "kwset.c"
kwset_t kwsalloc(char *trans )
{
  struct kwset *kwset ;
  char *tmp ;
  struct obstack *__h ;
  struct obstack *__o ;
  int __len ;
  struct obstack *__o1 ;
  void *value ;
  struct trie *mem_9 ;
  struct trie *mem_10 ;
  struct trie *mem_11 ;
  struct trie *mem_12 ;
  struct trie *mem_13 ;
  struct trie *mem_14 ;
  struct trie *mem_15 ;
  kwset_t __retres16 ;

  {
  __CrestCall(33041, 271);

  __CrestLoad(33042, (unsigned long )0, (long long )sizeof(struct kwset ));
# 104 "kwset.c"
  tmp = xmalloc(sizeof(struct kwset ));
  __CrestClearStack(33043);
# 104 "kwset.c"
  kwset = (struct kwset *)tmp;
  {
  __CrestLoad(33046, (unsigned long )(& kwset), (long long )((unsigned long )kwset));
  __CrestLoad(33045, (unsigned long )0, (long long )0);
  __CrestApply2(33044, 12, (long long )(kwset == 0));
# 105 "kwset.c"
  if (kwset == 0) {
    __CrestBranch(33047, 6612, 1);
# 106 "kwset.c"
    __retres16 = (kwset_t )0;
# 106 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33048, 6614, 0);

  }
  }
  __CrestLoad(33049, (unsigned long )0, (long long )0);
  __CrestLoad(33050, (unsigned long )0, (long long )0);
# 108 "kwset.c"
  _obstack_begin(& kwset->obstack, 0, 0, (void *(*)(long ))(& xmalloc), (void (*)(void * ))(& free));
  __CrestClearStack(33051);
  __CrestLoad(33052, (unsigned long )0, (long long )0);
  __CrestStore(33053, (unsigned long )(& kwset->words));
# 109 "kwset.c"
  kwset->words = 0;
# 111 "kwset.c"
  __h = & kwset->obstack;
# 111 "kwset.c"
  __o = __h;
  __CrestLoad(33054, (unsigned long )0, (long long )((int )sizeof(struct trie )));
  __CrestStore(33055, (unsigned long )(& __len));
# 111 "kwset.c"
  __len = (int )sizeof(struct trie );
  {
  __CrestLoad(33060, (unsigned long )(& __o->chunk_limit), (long long )((unsigned long )__o->chunk_limit));
  __CrestLoad(33059, (unsigned long )(& __o->next_free), (long long )((unsigned long )__o->next_free));
  __CrestApply2(33058, 18, (long long )(__o->chunk_limit - __o->next_free));
  __CrestLoad(33057, (unsigned long )(& __len), (long long )__len);
  __CrestApply2(33056, 16, (long long )(__o->chunk_limit - __o->next_free < (long )__len));
# 111 "kwset.c"
  if (__o->chunk_limit - __o->next_free < (long )__len) {
    __CrestBranch(33061, 6617, 1);
    __CrestLoad(33063, (unsigned long )(& __len), (long long )__len);
# 111 "kwset.c"
    _obstack_newchunk(__o, __len);
    __CrestClearStack(33064);
  } else {
    __CrestBranch(33062, 6618, 0);

  }
  }
# 111 "kwset.c"
  __o->next_free += __len;
# 111 "kwset.c"
  __o1 = __h;
# 111 "kwset.c"
  value = (void *)__o1->object_base;
  {
  __CrestLoad(33067, (unsigned long )(& __o1->next_free), (long long )((unsigned long )__o1->next_free));
  __CrestLoad(33066, (unsigned long )(& value), (long long )((unsigned long )value));
  __CrestApply2(33065, 12, (long long )((unsigned long )__o1->next_free == (unsigned long )value));
# 111 "kwset.c"
  if ((unsigned long )__o1->next_free == (unsigned long )value) {
    __CrestBranch(33068, 6621, 1);
# 111 "kwset.c"
    __o1->maybe_empty_object = 1U;
  } else {
    __CrestBranch(33069, 6622, 0);

  }
  }
# 111 "kwset.c"
  __o1->next_free = (char *)0 + (((__o1->next_free - (char *)0) + (long )__o1->alignment_mask) & (long )(~ __o1->alignment_mask));
  {
  __CrestLoad(33076, (unsigned long )(& __o1->next_free), (long long )((unsigned long )__o1->next_free));
  __CrestLoad(33075, (unsigned long )(& __o1->chunk), (long long )((unsigned long )__o1->chunk));
  __CrestApply2(33074, 18, (long long )(__o1->next_free - (char *)__o1->chunk));
  __CrestLoad(33073, (unsigned long )(& __o1->chunk_limit), (long long )((unsigned long )__o1->chunk_limit));
  __CrestLoad(33072, (unsigned long )(& __o1->chunk), (long long )((unsigned long )__o1->chunk));
  __CrestApply2(33071, 18, (long long )(__o1->chunk_limit - (char *)__o1->chunk));
  __CrestApply2(33070, 14, (long long )(__o1->next_free - (char *)__o1->chunk > __o1->chunk_limit - (char *)__o1->chunk));
# 111 "kwset.c"
  if (__o1->next_free - (char *)__o1->chunk > __o1->chunk_limit - (char *)__o1->chunk) {
    __CrestBranch(33077, 6625, 1);
# 111 "kwset.c"
    __o1->next_free = __o1->chunk_limit;
  } else {
    __CrestBranch(33078, 6626, 0);

  }
  }
# 111 "kwset.c"
  __o1->object_base = __o1->next_free;
# 111 "kwset.c"
  kwset->trie = (struct trie *)value;
  {
  __CrestLoad(33081, (unsigned long )(& kwset->trie), (long long )((unsigned long )kwset->trie));
  __CrestLoad(33080, (unsigned long )0, (long long )0);
  __CrestApply2(33079, 12, (long long )(kwset->trie == 0));
# 112 "kwset.c"
  if (kwset->trie == 0) {
    __CrestBranch(33082, 6629, 1);
# 114 "kwset.c"
    kwsfree((kwset_t )kwset);
    __CrestClearStack(33084);
# 115 "kwset.c"
    __retres16 = (kwset_t )0;
# 115 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33083, 6632, 0);

  }
  }
# 117 "kwset.c"
  mem_9 = kwset->trie;
  __CrestLoad(33085, (unsigned long )0, (long long )0U);
  __CrestStore(33086, (unsigned long )(& mem_9->accepting));
# 117 "kwset.c"
  mem_9->accepting = 0U;
# 118 "kwset.c"
  mem_10 = kwset->trie;
# 118 "kwset.c"
  mem_10->links = (struct tree *)0;
# 119 "kwset.c"
  mem_11 = kwset->trie;
# 119 "kwset.c"
  mem_11->parent = (struct trie *)0;
# 120 "kwset.c"
  mem_12 = kwset->trie;
# 120 "kwset.c"
  mem_12->next = (struct trie *)0;
# 121 "kwset.c"
  mem_13 = kwset->trie;
# 121 "kwset.c"
  mem_13->fail = (struct trie *)0;
# 122 "kwset.c"
  mem_14 = kwset->trie;
  __CrestLoad(33087, (unsigned long )0, (long long )0);
  __CrestStore(33088, (unsigned long )(& mem_14->depth));
# 122 "kwset.c"
  mem_14->depth = 0;
# 123 "kwset.c"
  mem_15 = kwset->trie;
  __CrestLoad(33089, (unsigned long )0, (long long )0);
  __CrestStore(33090, (unsigned long )(& mem_15->shift));
# 123 "kwset.c"
  mem_15->shift = 0;
  __CrestLoad(33091, (unsigned long )0, (long long )2147483647);
  __CrestStore(33092, (unsigned long )(& kwset->mind));
# 124 "kwset.c"
  kwset->mind = 2147483647;
  __CrestLoad(33093, (unsigned long )0, (long long )-1);
  __CrestStore(33094, (unsigned long )(& kwset->maxd));
# 125 "kwset.c"
  kwset->maxd = -1;
# 126 "kwset.c"
  kwset->target = (char *)0;
# 127 "kwset.c"
  kwset->trans = trans;
# 129 "kwset.c"
  __retres16 = (kwset_t )kwset;
  return_label:
  {
  __CrestReturn(33095);
# 98 "kwset.c"
  return (__retres16);
  }
}
}
# 134 "kwset.c"
char *kwsincr(kwset_t kws , char *text , size_t len )
{
  struct kwset *kwset ;
  register struct trie *trie ;
  unsigned char label ;
  struct tree *link___0 ;
  int depth ;
  struct tree *links[12] ;
  enum __anonenum_dirs_25 dirs[12] ;
  struct tree *t ;
  struct tree *r ;
  struct tree *l ;
  struct tree *rl ;
  struct tree *lr ;
  int tmp ;
  int tmp___0 ;
  struct obstack *__h ;
  struct obstack *__o ;
  int __len ;
  struct obstack *__o1 ;
  void *value ;
  char *tmp___1 ;
  struct obstack *__h___0 ;
  struct obstack *__o___0 ;
  int __len___0 ;
  struct obstack *__o1___0 ;
  void *value___0 ;
  char *tmp___2 ;
  char tmp___3 ;
  char tmp___4 ;
  size_t tmp___5 ;
  char *mem_33 ;
  struct trie *mem_34 ;
  struct trie *mem_35 ;
  struct trie *mem_36 ;
  struct trie *mem_37 ;
  struct trie *mem_38 ;
  struct trie *mem_39 ;
  struct trie *mem_40 ;
  struct tree *mem_41 ;
  struct tree *mem_42 ;
  struct tree *mem_43 ;
  struct tree *mem_44 ;
  struct tree *mem_45 ;
  struct tree *mem_46 ;
  struct tree *mem_47 ;
  struct tree *mem_48 ;
  struct tree *mem_49 ;
  struct tree *mem_50 ;
  struct tree *mem_51 ;
  struct tree *mem_52 ;
  struct tree *mem_53 ;
  struct tree *mem_54 ;
  struct tree *mem_55 ;
  struct tree *mem_56 ;
  char *__retres57 ;

  {
  __CrestCall(33097, 272);
  __CrestStore(33096, (unsigned long )(& len));
# 149 "kwset.c"
  kwset = (struct kwset *)kws;
# 150 "kwset.c"
  trie = kwset->trie;
# 151 "kwset.c"
  text += len;
  {
# 155 "kwset.c"
  while (1) {
    while_continue: ;
    __CrestLoad(33098, (unsigned long )(& len), (long long )len);
    __CrestStore(33099, (unsigned long )(& tmp___5));
# 155 "kwset.c"
    tmp___5 = len;
    __CrestLoad(33102, (unsigned long )(& len), (long long )len);
    __CrestLoad(33101, (unsigned long )0, (long long )1UL);
    __CrestApply2(33100, 1, (long long )(len - 1UL));
    __CrestStore(33103, (unsigned long )(& len));
# 155 "kwset.c"
    len --;
    {
    __CrestLoad(33106, (unsigned long )(& tmp___5), (long long )tmp___5);
    __CrestLoad(33105, (unsigned long )0, (long long )0);
    __CrestApply2(33104, 13, (long long )(tmp___5 != 0));
# 155 "kwset.c"
    if (tmp___5 != 0) {
      __CrestBranch(33107, 6642, 1);

    } else {
      __CrestBranch(33108, 6643, 0);
# 155 "kwset.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(33111, (unsigned long )(& kwset->trans), (long long )((unsigned long )kwset->trans));
    __CrestLoad(33110, (unsigned long )0, (long long )0);
    __CrestApply2(33109, 13, (long long )(kwset->trans != 0));
# 157 "kwset.c"
    if (kwset->trans != 0) {
      __CrestBranch(33112, 6645, 1);
# 157 "kwset.c"
      text --;
# 157 "kwset.c"
      mem_33 = kwset->trans + (unsigned char )*text;
      __CrestLoad(33114, (unsigned long )mem_33, (long long )*mem_33);
      __CrestStore(33115, (unsigned long )(& label));
# 157 "kwset.c"
      label = (unsigned char )*mem_33;
    } else {
      __CrestBranch(33113, 6646, 0);
# 157 "kwset.c"
      text --;
      __CrestLoad(33116, (unsigned long )text, (long long )*text);
      __CrestStore(33117, (unsigned long )(& label));
# 157 "kwset.c"
      label = (unsigned char )*text;
    }
    }
# 162 "kwset.c"
    link___0 = trie->links;
# 163 "kwset.c"
    links[0] = (struct tree *)(& trie->links);
    __CrestLoad(33118, (unsigned long )0, (long long )((enum __anonenum_dirs_25 )0));
    __CrestStore(33119, (unsigned long )(& dirs[0]));
# 164 "kwset.c"
    dirs[0] = (enum __anonenum_dirs_25 )0;
    __CrestLoad(33120, (unsigned long )0, (long long )1);
    __CrestStore(33121, (unsigned long )(& depth));
# 165 "kwset.c"
    depth = 1;
    {
# 167 "kwset.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(33124, (unsigned long )(& link___0), (long long )((unsigned long )link___0));
      __CrestLoad(33123, (unsigned long )0, (long long )0);
      __CrestApply2(33122, 13, (long long )(link___0 != 0));
# 167 "kwset.c"
      if (link___0 != 0) {
        __CrestBranch(33125, 6652, 1);
        {
        __CrestLoad(33129, (unsigned long )(& label), (long long )label);
        __CrestLoad(33128, (unsigned long )(& link___0->label), (long long )link___0->label);
        __CrestApply2(33127, 13, (long long )((int )label != (int )link___0->label));
# 167 "kwset.c"
        if ((int )label != (int )link___0->label) {
          __CrestBranch(33130, 6653, 1);

        } else {
          __CrestBranch(33131, 6654, 0);
# 167 "kwset.c"
          goto while_break___0;
        }
        }
      } else {
        __CrestBranch(33126, 6655, 0);
# 167 "kwset.c"
        goto while_break___0;
      }
      }
# 169 "kwset.c"
      links[depth] = link___0;
      {
      __CrestLoad(33134, (unsigned long )(& label), (long long )label);
      __CrestLoad(33133, (unsigned long )(& link___0->label), (long long )link___0->label);
      __CrestApply2(33132, 16, (long long )((int )label < (int )link___0->label));
# 170 "kwset.c"
      if ((int )label < (int )link___0->label) {
        __CrestBranch(33135, 6658, 1);
        __CrestLoad(33137, (unsigned long )(& depth), (long long )depth);
        __CrestStore(33138, (unsigned long )(& tmp));
# 171 "kwset.c"
        tmp = depth;
        __CrestLoad(33141, (unsigned long )(& depth), (long long )depth);
        __CrestLoad(33140, (unsigned long )0, (long long )1);
        __CrestApply2(33139, 0, (long long )(depth + 1));
        __CrestStore(33142, (unsigned long )(& depth));
# 171 "kwset.c"
        depth ++;
        __CrestLoad(33143, (unsigned long )0, (long long )((enum __anonenum_dirs_25 )0));
        __CrestStore(33144, (unsigned long )(& dirs[tmp]));
# 171 "kwset.c"
        dirs[tmp] = (enum __anonenum_dirs_25 )0;
# 171 "kwset.c"
        link___0 = link___0->llink;
      } else {
        __CrestBranch(33136, 6659, 0);
        __CrestLoad(33145, (unsigned long )(& depth), (long long )depth);
        __CrestStore(33146, (unsigned long )(& tmp___0));
# 173 "kwset.c"
        tmp___0 = depth;
        __CrestLoad(33149, (unsigned long )(& depth), (long long )depth);
        __CrestLoad(33148, (unsigned long )0, (long long )1);
        __CrestApply2(33147, 0, (long long )(depth + 1));
        __CrestStore(33150, (unsigned long )(& depth));
# 173 "kwset.c"
        depth ++;
        __CrestLoad(33151, (unsigned long )0, (long long )((enum __anonenum_dirs_25 )1));
        __CrestStore(33152, (unsigned long )(& dirs[tmp___0]));
# 173 "kwset.c"
        dirs[tmp___0] = (enum __anonenum_dirs_25 )1;
# 173 "kwset.c"
        link___0 = link___0->rlink;
      }
      }
    }
    while_break___0: ;
    }
    {
    __CrestLoad(33155, (unsigned long )(& link___0), (long long )((unsigned long )link___0));
    __CrestLoad(33154, (unsigned long )0, (long long )0);
    __CrestApply2(33153, 12, (long long )(link___0 == 0));
# 179 "kwset.c"
    if (link___0 == 0) {
      __CrestBranch(33156, 6662, 1);
# 181 "kwset.c"
      __h = & kwset->obstack;
# 181 "kwset.c"
      __o = __h;
      __CrestLoad(33158, (unsigned long )0, (long long )((int )sizeof(struct tree )));
      __CrestStore(33159, (unsigned long )(& __len));
# 181 "kwset.c"
      __len = (int )sizeof(struct tree );
      {
      __CrestLoad(33164, (unsigned long )(& __o->chunk_limit), (long long )((unsigned long )__o->chunk_limit));
      __CrestLoad(33163, (unsigned long )(& __o->next_free), (long long )((unsigned long )__o->next_free));
      __CrestApply2(33162, 18, (long long )(__o->chunk_limit - __o->next_free));
      __CrestLoad(33161, (unsigned long )(& __len), (long long )__len);
      __CrestApply2(33160, 16, (long long )(__o->chunk_limit - __o->next_free < (long )__len));
# 181 "kwset.c"
      if (__o->chunk_limit - __o->next_free < (long )__len) {
        __CrestBranch(33165, 6664, 1);
        __CrestLoad(33167, (unsigned long )(& __len), (long long )__len);
# 181 "kwset.c"
        _obstack_newchunk(__o, __len);
        __CrestClearStack(33168);
      } else {
        __CrestBranch(33166, 6665, 0);

      }
      }
# 181 "kwset.c"
      __o->next_free += __len;
# 181 "kwset.c"
      __o1 = __h;
# 181 "kwset.c"
      value = (void *)__o1->object_base;
      {
      __CrestLoad(33171, (unsigned long )(& __o1->next_free), (long long )((unsigned long )__o1->next_free));
      __CrestLoad(33170, (unsigned long )(& value), (long long )((unsigned long )value));
      __CrestApply2(33169, 12, (long long )((unsigned long )__o1->next_free == (unsigned long )value));
# 181 "kwset.c"
      if ((unsigned long )__o1->next_free == (unsigned long )value) {
        __CrestBranch(33172, 6668, 1);
# 181 "kwset.c"
        __o1->maybe_empty_object = 1U;
      } else {
        __CrestBranch(33173, 6669, 0);

      }
      }
# 181 "kwset.c"
      __o1->next_free = (char *)0 + (((__o1->next_free - (char *)0) + (long )__o1->alignment_mask) & (long )(~ __o1->alignment_mask));
      {
      __CrestLoad(33180, (unsigned long )(& __o1->next_free), (long long )((unsigned long )__o1->next_free));
      __CrestLoad(33179, (unsigned long )(& __o1->chunk), (long long )((unsigned long )__o1->chunk));
      __CrestApply2(33178, 18, (long long )(__o1->next_free - (char *)__o1->chunk));
      __CrestLoad(33177, (unsigned long )(& __o1->chunk_limit), (long long )((unsigned long )__o1->chunk_limit));
      __CrestLoad(33176, (unsigned long )(& __o1->chunk), (long long )((unsigned long )__o1->chunk));
      __CrestApply2(33175, 18, (long long )(__o1->chunk_limit - (char *)__o1->chunk));
      __CrestApply2(33174, 14, (long long )(__o1->next_free - (char *)__o1->chunk > __o1->chunk_limit - (char *)__o1->chunk));
# 181 "kwset.c"
      if (__o1->next_free - (char *)__o1->chunk > __o1->chunk_limit - (char *)__o1->chunk) {
        __CrestBranch(33181, 6672, 1);
# 181 "kwset.c"
        __o1->next_free = __o1->chunk_limit;
      } else {
        __CrestBranch(33182, 6673, 0);

      }
      }
# 181 "kwset.c"
      __o1->object_base = __o1->next_free;
# 181 "kwset.c"
      link___0 = (struct tree *)value;
      {
      __CrestLoad(33185, (unsigned long )(& link___0), (long long )((unsigned long )link___0));
      __CrestLoad(33184, (unsigned long )0, (long long )0);
      __CrestApply2(33183, 12, (long long )(link___0 == 0));
# 183 "kwset.c"
      if (link___0 == 0) {
        __CrestBranch(33186, 6676, 1);
        __CrestLoad(33188, (unsigned long )0, (long long )5);
# 184 "kwset.c"
        tmp___1 = dcgettext((char const *)((void *)0), "memory exhausted", 5);
        __CrestClearStack(33189);
# 184 "kwset.c"
        __retres57 = tmp___1;
# 184 "kwset.c"
        goto return_label;
      } else {
        __CrestBranch(33187, 6679, 0);

      }
      }
# 185 "kwset.c"
      link___0->llink = (struct tree *)0;
# 186 "kwset.c"
      link___0->rlink = (struct tree *)0;
# 187 "kwset.c"
      __h___0 = & kwset->obstack;
# 187 "kwset.c"
      __o___0 = __h___0;
      __CrestLoad(33190, (unsigned long )0, (long long )((int )sizeof(struct trie )));
      __CrestStore(33191, (unsigned long )(& __len___0));
# 187 "kwset.c"
      __len___0 = (int )sizeof(struct trie );
      {
      __CrestLoad(33196, (unsigned long )(& __o___0->chunk_limit), (long long )((unsigned long )__o___0->chunk_limit));
      __CrestLoad(33195, (unsigned long )(& __o___0->next_free), (long long )((unsigned long )__o___0->next_free));
      __CrestApply2(33194, 18, (long long )(__o___0->chunk_limit - __o___0->next_free));
      __CrestLoad(33193, (unsigned long )(& __len___0), (long long )__len___0);
      __CrestApply2(33192, 16, (long long )(__o___0->chunk_limit - __o___0->next_free < (long )__len___0));
# 187 "kwset.c"
      if (__o___0->chunk_limit - __o___0->next_free < (long )__len___0) {
        __CrestBranch(33197, 6682, 1);
        __CrestLoad(33199, (unsigned long )(& __len___0), (long long )__len___0);
# 187 "kwset.c"
        _obstack_newchunk(__o___0, __len___0);
        __CrestClearStack(33200);
      } else {
        __CrestBranch(33198, 6683, 0);

      }
      }
# 187 "kwset.c"
      __o___0->next_free += __len___0;
# 187 "kwset.c"
      __o1___0 = __h___0;
# 187 "kwset.c"
      value___0 = (void *)__o1___0->object_base;
      {
      __CrestLoad(33203, (unsigned long )(& __o1___0->next_free), (long long )((unsigned long )__o1___0->next_free));
      __CrestLoad(33202, (unsigned long )(& value___0), (long long )((unsigned long )value___0));
      __CrestApply2(33201, 12, (long long )((unsigned long )__o1___0->next_free == (unsigned long )value___0));
# 187 "kwset.c"
      if ((unsigned long )__o1___0->next_free == (unsigned long )value___0) {
        __CrestBranch(33204, 6686, 1);
# 187 "kwset.c"
        __o1___0->maybe_empty_object = 1U;
      } else {
        __CrestBranch(33205, 6687, 0);

      }
      }
# 187 "kwset.c"
      __o1___0->next_free = (char *)0 + (((__o1___0->next_free - (char *)0) + (long )__o1___0->alignment_mask) & (long )(~ __o1___0->alignment_mask));
      {
      __CrestLoad(33212, (unsigned long )(& __o1___0->next_free), (long long )((unsigned long )__o1___0->next_free));
      __CrestLoad(33211, (unsigned long )(& __o1___0->chunk), (long long )((unsigned long )__o1___0->chunk));
      __CrestApply2(33210, 18, (long long )(__o1___0->next_free - (char *)__o1___0->chunk));
      __CrestLoad(33209, (unsigned long )(& __o1___0->chunk_limit), (long long )((unsigned long )__o1___0->chunk_limit));
      __CrestLoad(33208, (unsigned long )(& __o1___0->chunk), (long long )((unsigned long )__o1___0->chunk));
      __CrestApply2(33207, 18, (long long )(__o1___0->chunk_limit - (char *)__o1___0->chunk));
      __CrestApply2(33206, 14, (long long )(__o1___0->next_free - (char *)__o1___0->chunk > __o1___0->chunk_limit - (char *)__o1___0->chunk));
# 187 "kwset.c"
      if (__o1___0->next_free - (char *)__o1___0->chunk > __o1___0->chunk_limit - (char *)__o1___0->chunk) {
        __CrestBranch(33213, 6690, 1);
# 187 "kwset.c"
        __o1___0->next_free = __o1___0->chunk_limit;
      } else {
        __CrestBranch(33214, 6691, 0);

      }
      }
# 187 "kwset.c"
      __o1___0->object_base = __o1___0->next_free;
# 187 "kwset.c"
      link___0->trie = (struct trie *)value___0;
      {
      __CrestLoad(33217, (unsigned long )(& link___0->trie), (long long )((unsigned long )link___0->trie));
      __CrestLoad(33216, (unsigned long )0, (long long )0);
      __CrestApply2(33215, 12, (long long )(link___0->trie == 0));
# 189 "kwset.c"
      if (link___0->trie == 0) {
        __CrestBranch(33218, 6694, 1);
        __CrestLoad(33220, (unsigned long )0, (long long )5);
# 190 "kwset.c"
        tmp___2 = dcgettext((char const *)((void *)0), "memory exhausted", 5);
        __CrestClearStack(33221);
# 190 "kwset.c"
        __retres57 = tmp___2;
# 190 "kwset.c"
        goto return_label;
      } else {
        __CrestBranch(33219, 6697, 0);

      }
      }
# 191 "kwset.c"
      mem_34 = link___0->trie;
      __CrestLoad(33222, (unsigned long )0, (long long )0U);
      __CrestStore(33223, (unsigned long )(& mem_34->accepting));
# 191 "kwset.c"
      mem_34->accepting = 0U;
# 192 "kwset.c"
      mem_35 = link___0->trie;
# 192 "kwset.c"
      mem_35->links = (struct tree *)0;
# 193 "kwset.c"
      mem_36 = link___0->trie;
# 193 "kwset.c"
      mem_36->parent = trie;
# 194 "kwset.c"
      mem_37 = link___0->trie;
# 194 "kwset.c"
      mem_37->next = (struct trie *)0;
# 195 "kwset.c"
      mem_38 = link___0->trie;
# 195 "kwset.c"
      mem_38->fail = (struct trie *)0;
# 196 "kwset.c"
      mem_39 = link___0->trie;
      __CrestLoad(33226, (unsigned long )(& trie->depth), (long long )trie->depth);
      __CrestLoad(33225, (unsigned long )0, (long long )1);
      __CrestApply2(33224, 0, (long long )(trie->depth + 1));
      __CrestStore(33227, (unsigned long )(& mem_39->depth));
# 196 "kwset.c"
      mem_39->depth = trie->depth + 1;
# 197 "kwset.c"
      mem_40 = link___0->trie;
      __CrestLoad(33228, (unsigned long )0, (long long )0);
      __CrestStore(33229, (unsigned long )(& mem_40->shift));
# 197 "kwset.c"
      mem_40->shift = 0;
      __CrestLoad(33230, (unsigned long )(& label), (long long )label);
      __CrestStore(33231, (unsigned long )(& link___0->label));
# 198 "kwset.c"
      link___0->label = label;
      __CrestLoad(33232, (unsigned long )0, (long long )(char)0);
      __CrestStore(33233, (unsigned long )(& link___0->balance));
# 199 "kwset.c"
      link___0->balance = (char)0;
      __CrestLoad(33236, (unsigned long )(& depth), (long long )depth);
      __CrestLoad(33235, (unsigned long )0, (long long )1);
      __CrestApply2(33234, 1, (long long )(depth - 1));
      __CrestStore(33237, (unsigned long )(& depth));
# 202 "kwset.c"
      depth --;
      {
      __CrestLoad(33240, (unsigned long )(& dirs[depth]), (long long )dirs[depth]);
      __CrestLoad(33239, (unsigned long )0, (long long )0U);
      __CrestApply2(33238, 12, (long long )((unsigned int )dirs[depth] == 0U));
# 202 "kwset.c"
      if ((unsigned int )dirs[depth] == 0U) {
        __CrestBranch(33241, 6700, 1);
# 203 "kwset.c"
        mem_41 = links[depth];
# 203 "kwset.c"
        mem_41->llink = link___0;
      } else {
        __CrestBranch(33242, 6701, 0);
# 205 "kwset.c"
        mem_42 = links[depth];
# 205 "kwset.c"
        mem_42->rlink = link___0;
      }
      }
      {
# 208 "kwset.c"
      while (1) {
        while_continue___1: ;
        {
        __CrestLoad(33245, (unsigned long )(& depth), (long long )depth);
        __CrestLoad(33244, (unsigned long )0, (long long )0);
        __CrestApply2(33243, 13, (long long )(depth != 0));
# 208 "kwset.c"
        if (depth != 0) {
          __CrestBranch(33246, 6706, 1);
          {
# 208 "kwset.c"
          mem_43 = links[depth];
          {
          __CrestLoad(33250, (unsigned long )(& mem_43->balance), (long long )mem_43->balance);
          __CrestLoad(33249, (unsigned long )0, (long long )0);
          __CrestApply2(33248, 12, (long long )(mem_43->balance == 0));
# 208 "kwset.c"
          if (mem_43->balance == 0) {
            __CrestBranch(33251, 6709, 1);

          } else {
            __CrestBranch(33252, 6710, 0);
# 208 "kwset.c"
            goto while_break___1;
          }
          }
          }
        } else {
          __CrestBranch(33247, 6711, 0);
# 208 "kwset.c"
          goto while_break___1;
        }
        }
        {
        __CrestLoad(33255, (unsigned long )(& dirs[depth]), (long long )dirs[depth]);
        __CrestLoad(33254, (unsigned long )0, (long long )0U);
        __CrestApply2(33253, 12, (long long )((unsigned int )dirs[depth] == 0U));
# 210 "kwset.c"
        if ((unsigned int )dirs[depth] == 0U) {
          __CrestBranch(33256, 6713, 1);
# 211 "kwset.c"
          mem_44 = links[depth];
# 211 "kwset.c"
          mem_45 = links[depth];
          __CrestLoad(33260, (unsigned long )(& mem_45->balance), (long long )mem_45->balance);
          __CrestLoad(33259, (unsigned long )0, (long long )1);
          __CrestApply2(33258, 1, (long long )((int )mem_45->balance - 1));
          __CrestStore(33261, (unsigned long )(& mem_44->balance));
# 211 "kwset.c"
          mem_44->balance = (char )((int )mem_45->balance - 1);
        } else {
          __CrestBranch(33257, 6714, 0);
# 213 "kwset.c"
          mem_46 = links[depth];
# 213 "kwset.c"
          mem_47 = links[depth];
          __CrestLoad(33264, (unsigned long )(& mem_47->balance), (long long )mem_47->balance);
          __CrestLoad(33263, (unsigned long )0, (long long )1);
          __CrestApply2(33262, 0, (long long )((int )mem_47->balance + 1));
          __CrestStore(33265, (unsigned long )(& mem_46->balance));
# 213 "kwset.c"
          mem_46->balance = (char )((int )mem_47->balance + 1);
        }
        }
        __CrestLoad(33268, (unsigned long )(& depth), (long long )depth);
        __CrestLoad(33267, (unsigned long )0, (long long )1);
        __CrestApply2(33266, 1, (long long )(depth - 1));
        __CrestStore(33269, (unsigned long )(& depth));
# 214 "kwset.c"
        depth --;
      }
      while_break___1: ;
      }
      {
      __CrestLoad(33272, (unsigned long )(& depth), (long long )depth);
      __CrestLoad(33271, (unsigned long )0, (long long )0);
      __CrestApply2(33270, 13, (long long )(depth != 0));
# 218 "kwset.c"
      if (depth != 0) {
        __CrestBranch(33273, 6718, 1);
        {
        __CrestLoad(33277, (unsigned long )(& dirs[depth]), (long long )dirs[depth]);
        __CrestLoad(33276, (unsigned long )0, (long long )0U);
        __CrestApply2(33275, 12, (long long )((unsigned int )dirs[depth] == 0U));
# 218 "kwset.c"
        if ((unsigned int )dirs[depth] == 0U) {
          __CrestBranch(33278, 6719, 1);
# 218 "kwset.c"
          mem_48 = links[depth];
# 218 "kwset.c"
          mem_49 = links[depth];
          __CrestLoad(33282, (unsigned long )(& mem_49->balance), (long long )mem_49->balance);
          __CrestLoad(33281, (unsigned long )0, (long long )1);
          __CrestApply2(33280, 1, (long long )((int )mem_49->balance - 1));
          __CrestStore(33283, (unsigned long )(& mem_48->balance));
# 218 "kwset.c"
          mem_48->balance = (char )((int )mem_49->balance - 1);
          {
# 218 "kwset.c"
          mem_50 = links[depth];
          {
          __CrestLoad(33286, (unsigned long )(& mem_50->balance), (long long )mem_50->balance);
          __CrestLoad(33285, (unsigned long )0, (long long )0);
          __CrestApply2(33284, 13, (long long )(mem_50->balance != 0));
# 218 "kwset.c"
          if (mem_50->balance != 0) {
            __CrestBranch(33287, 6723, 1);
# 218 "kwset.c"
            goto _L;
          } else {
            __CrestBranch(33288, 6724, 0);
# 218 "kwset.c"
            goto _L___0;
          }
          }
          }
        } else {
          __CrestBranch(33279, 6725, 0);
          _L___0:
          {
          __CrestLoad(33291, (unsigned long )(& dirs[depth]), (long long )dirs[depth]);
          __CrestLoad(33290, (unsigned long )0, (long long )1U);
          __CrestApply2(33289, 12, (long long )((unsigned int )dirs[depth] == 1U));
# 218 "kwset.c"
          if ((unsigned int )dirs[depth] == 1U) {
            __CrestBranch(33292, 6726, 1);
# 218 "kwset.c"
            mem_51 = links[depth];
# 218 "kwset.c"
            mem_52 = links[depth];
            __CrestLoad(33296, (unsigned long )(& mem_52->balance), (long long )mem_52->balance);
            __CrestLoad(33295, (unsigned long )0, (long long )1);
            __CrestApply2(33294, 0, (long long )((int )mem_52->balance + 1));
            __CrestStore(33297, (unsigned long )(& mem_51->balance));
# 218 "kwset.c"
            mem_51->balance = (char )((int )mem_52->balance + 1);
            {
# 218 "kwset.c"
            mem_53 = links[depth];
            {
            __CrestLoad(33300, (unsigned long )(& mem_53->balance), (long long )mem_53->balance);
            __CrestLoad(33299, (unsigned long )0, (long long )0);
            __CrestApply2(33298, 13, (long long )(mem_53->balance != 0));
# 218 "kwset.c"
            if (mem_53->balance != 0) {
              __CrestBranch(33301, 6730, 1);
              _L:
              {
# 221 "kwset.c"
              mem_54 = links[depth];
              {
              {
              __CrestLoad(33305, (unsigned long )(& mem_54->balance), (long long )mem_54->balance);
              __CrestLoad(33304, (unsigned long )0, (long long )-2);
              __CrestApply2(33303, 12, (long long )((int )mem_54->balance == -2));
# 223 "kwset.c"
              if ((int )mem_54->balance == -2) {
                __CrestBranch(33306, 6734, 1);
# 223 "kwset.c"
                goto case_neg_2;
              } else {
                __CrestBranch(33307, 6735, 0);

              }
              }
              {
              __CrestLoad(33310, (unsigned long )(& mem_54->balance), (long long )mem_54->balance);
              __CrestLoad(33309, (unsigned long )0, (long long )2);
              __CrestApply2(33308, 12, (long long )((int )mem_54->balance == 2));
# 243 "kwset.c"
              if ((int )mem_54->balance == 2) {
                __CrestBranch(33311, 6737, 1);
# 243 "kwset.c"
                goto case_2;
              } else {
                __CrestBranch(33312, 6738, 0);

              }
              }
# 263 "kwset.c"
              goto switch_default___1;
              case_neg_2:
              {
              {
              __CrestLoad(33315, (unsigned long )(& dirs[depth + 1]), (long long )dirs[depth + 1]);
              __CrestLoad(33314, (unsigned long )0, (long long )0U);
              __CrestApply2(33313, 12, (long long )((unsigned int )dirs[depth + 1] == 0U));
# 226 "kwset.c"
              if ((unsigned int )dirs[depth + 1] == 0U) {
                __CrestBranch(33316, 6742, 1);
# 226 "kwset.c"
                goto case_0;
              } else {
                __CrestBranch(33317, 6743, 0);

              }
              }
              {
              __CrestLoad(33320, (unsigned long )(& dirs[depth + 1]), (long long )dirs[depth + 1]);
              __CrestLoad(33319, (unsigned long )0, (long long )1U);
              __CrestApply2(33318, 12, (long long )((unsigned int )dirs[depth + 1] == 1U));
# 231 "kwset.c"
              if ((unsigned int )dirs[depth + 1] == 1U) {
                __CrestBranch(33321, 6745, 1);
# 231 "kwset.c"
                goto case_1;
              } else {
                __CrestBranch(33322, 6746, 0);

              }
              }
# 239 "kwset.c"
              goto switch_default;
              case_0:
# 227 "kwset.c"
              r = links[depth];
# 227 "kwset.c"
              t = r->llink;
# 227 "kwset.c"
              rl = t->rlink;
# 228 "kwset.c"
              t->rlink = r;
# 228 "kwset.c"
              r->llink = rl;
              __CrestLoad(33323, (unsigned long )0, (long long )(char)0);
              __CrestStore(33324, (unsigned long )(& tmp___3));
# 229 "kwset.c"
              tmp___3 = (char)0;
              __CrestLoad(33325, (unsigned long )(& tmp___3), (long long )tmp___3);
              __CrestStore(33326, (unsigned long )(& r->balance));
# 229 "kwset.c"
              r->balance = tmp___3;
              __CrestLoad(33327, (unsigned long )(& tmp___3), (long long )tmp___3);
              __CrestStore(33328, (unsigned long )(& t->balance));
# 229 "kwset.c"
              t->balance = tmp___3;
# 230 "kwset.c"
              goto switch_break___0;
              case_1:
# 232 "kwset.c"
              r = links[depth];
# 232 "kwset.c"
              l = r->llink;
# 232 "kwset.c"
              t = l->rlink;
# 233 "kwset.c"
              rl = t->rlink;
# 233 "kwset.c"
              lr = t->llink;
# 234 "kwset.c"
              t->llink = l;
# 234 "kwset.c"
              l->rlink = lr;
# 234 "kwset.c"
              t->rlink = r;
# 234 "kwset.c"
              r->llink = rl;
              {
              __CrestLoad(33331, (unsigned long )(& t->balance), (long long )t->balance);
              __CrestLoad(33330, (unsigned long )0, (long long )1);
              __CrestApply2(33329, 13, (long long )((int )t->balance != 1));
# 235 "kwset.c"
              if ((int )t->balance != 1) {
                __CrestBranch(33332, 6752, 1);
                __CrestLoad(33334, (unsigned long )0, (long long )(char)0);
                __CrestStore(33335, (unsigned long )(& l->balance));
# 235 "kwset.c"
                l->balance = (char)0;
              } else {
                __CrestBranch(33333, 6753, 0);
                __CrestLoad(33336, (unsigned long )0, (long long )(char)-1);
                __CrestStore(33337, (unsigned long )(& l->balance));
# 235 "kwset.c"
                l->balance = (char)-1;
              }
              }
              {
              __CrestLoad(33340, (unsigned long )(& t->balance), (long long )t->balance);
              __CrestLoad(33339, (unsigned long )0, (long long )-1);
              __CrestApply2(33338, 13, (long long )((int )t->balance != -1));
# 236 "kwset.c"
              if ((int )t->balance != -1) {
                __CrestBranch(33341, 6755, 1);
                __CrestLoad(33343, (unsigned long )0, (long long )(char)0);
                __CrestStore(33344, (unsigned long )(& r->balance));
# 236 "kwset.c"
                r->balance = (char)0;
              } else {
                __CrestBranch(33342, 6756, 0);
                __CrestLoad(33345, (unsigned long )0, (long long )(char)1);
                __CrestStore(33346, (unsigned long )(& r->balance));
# 236 "kwset.c"
                r->balance = (char)1;
              }
              }
              __CrestLoad(33347, (unsigned long )0, (long long )(char)0);
              __CrestStore(33348, (unsigned long )(& t->balance));
# 237 "kwset.c"
              t->balance = (char)0;
# 238 "kwset.c"
              goto switch_break___0;
              switch_default:
# 240 "kwset.c"
              abort();
              __CrestClearStack(33349);
              switch_break___0: ;
              }
# 242 "kwset.c"
              goto switch_break;
              case_2:
              {
              {
              __CrestLoad(33352, (unsigned long )(& dirs[depth + 1]), (long long )dirs[depth + 1]);
              __CrestLoad(33351, (unsigned long )0, (long long )1U);
              __CrestApply2(33350, 12, (long long )((unsigned int )dirs[depth + 1] == 1U));
# 246 "kwset.c"
              if ((unsigned int )dirs[depth + 1] == 1U) {
                __CrestBranch(33353, 6764, 1);
# 246 "kwset.c"
                goto case_1___0;
              } else {
                __CrestBranch(33354, 6765, 0);

              }
              }
              {
              __CrestLoad(33357, (unsigned long )(& dirs[depth + 1]), (long long )dirs[depth + 1]);
              __CrestLoad(33356, (unsigned long )0, (long long )0U);
              __CrestApply2(33355, 12, (long long )((unsigned int )dirs[depth + 1] == 0U));
# 251 "kwset.c"
              if ((unsigned int )dirs[depth + 1] == 0U) {
                __CrestBranch(33358, 6767, 1);
# 251 "kwset.c"
                goto case_0___0;
              } else {
                __CrestBranch(33359, 6768, 0);

              }
              }
# 259 "kwset.c"
              goto switch_default___0;
              case_1___0:
# 247 "kwset.c"
              l = links[depth];
# 247 "kwset.c"
              t = l->rlink;
# 247 "kwset.c"
              lr = t->llink;
# 248 "kwset.c"
              t->llink = l;
# 248 "kwset.c"
              l->rlink = lr;
              __CrestLoad(33360, (unsigned long )0, (long long )(char)0);
              __CrestStore(33361, (unsigned long )(& tmp___4));
# 249 "kwset.c"
              tmp___4 = (char)0;
              __CrestLoad(33362, (unsigned long )(& tmp___4), (long long )tmp___4);
              __CrestStore(33363, (unsigned long )(& l->balance));
# 249 "kwset.c"
              l->balance = tmp___4;
              __CrestLoad(33364, (unsigned long )(& tmp___4), (long long )tmp___4);
              __CrestStore(33365, (unsigned long )(& t->balance));
# 249 "kwset.c"
              t->balance = tmp___4;
# 250 "kwset.c"
              goto switch_break___1;
              case_0___0:
# 252 "kwset.c"
              l = links[depth];
# 252 "kwset.c"
              r = l->rlink;
# 252 "kwset.c"
              t = r->llink;
# 253 "kwset.c"
              lr = t->llink;
# 253 "kwset.c"
              rl = t->rlink;
# 254 "kwset.c"
              t->llink = l;
# 254 "kwset.c"
              l->rlink = lr;
# 254 "kwset.c"
              t->rlink = r;
# 254 "kwset.c"
              r->llink = rl;
              {
              __CrestLoad(33368, (unsigned long )(& t->balance), (long long )t->balance);
              __CrestLoad(33367, (unsigned long )0, (long long )1);
              __CrestApply2(33366, 13, (long long )((int )t->balance != 1));
# 255 "kwset.c"
              if ((int )t->balance != 1) {
                __CrestBranch(33369, 6774, 1);
                __CrestLoad(33371, (unsigned long )0, (long long )(char)0);
                __CrestStore(33372, (unsigned long )(& l->balance));
# 255 "kwset.c"
                l->balance = (char)0;
              } else {
                __CrestBranch(33370, 6775, 0);
                __CrestLoad(33373, (unsigned long )0, (long long )(char)-1);
                __CrestStore(33374, (unsigned long )(& l->balance));
# 255 "kwset.c"
                l->balance = (char)-1;
              }
              }
              {
              __CrestLoad(33377, (unsigned long )(& t->balance), (long long )t->balance);
              __CrestLoad(33376, (unsigned long )0, (long long )-1);
              __CrestApply2(33375, 13, (long long )((int )t->balance != -1));
# 256 "kwset.c"
              if ((int )t->balance != -1) {
                __CrestBranch(33378, 6777, 1);
                __CrestLoad(33380, (unsigned long )0, (long long )(char)0);
                __CrestStore(33381, (unsigned long )(& r->balance));
# 256 "kwset.c"
                r->balance = (char)0;
              } else {
                __CrestBranch(33379, 6778, 0);
                __CrestLoad(33382, (unsigned long )0, (long long )(char)1);
                __CrestStore(33383, (unsigned long )(& r->balance));
# 256 "kwset.c"
                r->balance = (char)1;
              }
              }
              __CrestLoad(33384, (unsigned long )0, (long long )(char)0);
              __CrestStore(33385, (unsigned long )(& t->balance));
# 257 "kwset.c"
              t->balance = (char)0;
# 258 "kwset.c"
              goto switch_break___1;
              switch_default___0:
# 260 "kwset.c"
              abort();
              __CrestClearStack(33386);
              switch_break___1: ;
              }
# 262 "kwset.c"
              goto switch_break;
              switch_default___1:
# 264 "kwset.c"
              abort();
              __CrestClearStack(33387);
              switch_break: ;
              }
              }
              {
              __CrestLoad(33390, (unsigned long )(& dirs[depth - 1]), (long long )dirs[depth - 1]);
              __CrestLoad(33389, (unsigned long )0, (long long )0U);
              __CrestApply2(33388, 12, (long long )((unsigned int )dirs[depth - 1] == 0U));
# 267 "kwset.c"
              if ((unsigned int )dirs[depth - 1] == 0U) {
                __CrestBranch(33391, 6787, 1);
# 268 "kwset.c"
                mem_55 = links[depth - 1];
# 268 "kwset.c"
                mem_55->llink = t;
              } else {
                __CrestBranch(33392, 6788, 0);
# 270 "kwset.c"
                mem_56 = links[depth - 1];
# 270 "kwset.c"
                mem_56->rlink = t;
              }
              }
            } else {
              __CrestBranch(33302, 6789, 0);

            }
            }
            }
          } else {
            __CrestBranch(33293, 6790, 0);

          }
          }
        }
        }
      } else {
        __CrestBranch(33274, 6791, 0);

      }
      }
    } else {
      __CrestBranch(33157, 6792, 0);

    }
    }
# 274 "kwset.c"
    trie = link___0->trie;
  }
  while_break: ;
  }
  {
  __CrestLoad(33395, (unsigned long )(& trie->accepting), (long long )trie->accepting);
  __CrestLoad(33394, (unsigned long )0, (long long )0);
  __CrestApply2(33393, 12, (long long )(trie->accepting == 0));
# 279 "kwset.c"
  if (trie->accepting == 0) {
    __CrestBranch(33396, 6796, 1);
    __CrestLoad(33402, (unsigned long )0, (long long )1);
    __CrestLoad(33401, (unsigned long )0, (long long )2);
    __CrestLoad(33400, (unsigned long )(& kwset->words), (long long )kwset->words);
    __CrestApply2(33399, 2, (long long )(2 * kwset->words));
    __CrestApply2(33398, 0, (long long )(1 + 2 * kwset->words));
    __CrestStore(33403, (unsigned long )(& trie->accepting));
# 280 "kwset.c"
    trie->accepting = (unsigned int )(1 + 2 * kwset->words);
  } else {
    __CrestBranch(33397, 6797, 0);

  }
  }
  __CrestLoad(33406, (unsigned long )(& kwset->words), (long long )kwset->words);
  __CrestLoad(33405, (unsigned long )0, (long long )1);
  __CrestApply2(33404, 0, (long long )(kwset->words + 1));
  __CrestStore(33407, (unsigned long )(& kwset->words));
# 281 "kwset.c"
  (kwset->words) ++;
  {
  __CrestLoad(33410, (unsigned long )(& trie->depth), (long long )trie->depth);
  __CrestLoad(33409, (unsigned long )(& kwset->mind), (long long )kwset->mind);
  __CrestApply2(33408, 16, (long long )(trie->depth < kwset->mind));
# 284 "kwset.c"
  if (trie->depth < kwset->mind) {
    __CrestBranch(33411, 6800, 1);
    __CrestLoad(33413, (unsigned long )(& trie->depth), (long long )trie->depth);
    __CrestStore(33414, (unsigned long )(& kwset->mind));
# 285 "kwset.c"
    kwset->mind = trie->depth;
  } else {
    __CrestBranch(33412, 6801, 0);

  }
  }
  {
  __CrestLoad(33417, (unsigned long )(& trie->depth), (long long )trie->depth);
  __CrestLoad(33416, (unsigned long )(& kwset->maxd), (long long )kwset->maxd);
  __CrestApply2(33415, 14, (long long )(trie->depth > kwset->maxd));
# 286 "kwset.c"
  if (trie->depth > kwset->maxd) {
    __CrestBranch(33418, 6803, 1);
    __CrestLoad(33420, (unsigned long )(& trie->depth), (long long )trie->depth);
    __CrestStore(33421, (unsigned long )(& kwset->maxd));
# 287 "kwset.c"
    kwset->maxd = trie->depth;
  } else {
    __CrestBranch(33419, 6804, 0);

  }
  }
# 289 "kwset.c"
  __retres57 = (char *)0;
  return_label:
  {
  __CrestReturn(33422);
# 134 "kwset.c"
  return (__retres57);
  }
}
}
# 294 "kwset.c"
static void enqueue(struct tree *tree , struct trie **last )
{
  struct trie *tmp ;
  struct trie *mem_4 ;

  {
  __CrestCall(33423, 273);

  {
  __CrestLoad(33426, (unsigned long )(& tree), (long long )((unsigned long )tree));
  __CrestLoad(33425, (unsigned long )0, (long long )0);
  __CrestApply2(33424, 12, (long long )(tree == 0));
# 299 "kwset.c"
  if (tree == 0) {
    __CrestBranch(33427, 6808, 1);
# 300 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33428, 6810, 0);

  }
  }
# 301 "kwset.c"
  enqueue(tree->llink, last);
  __CrestClearStack(33429);
# 302 "kwset.c"
  enqueue(tree->rlink, last);
  __CrestClearStack(33430);
# 303 "kwset.c"
  tmp = tree->trie;
# 303 "kwset.c"
  mem_4 = *last;
# 303 "kwset.c"
  mem_4->next = tmp;
# 303 "kwset.c"
  *last = tmp;

  return_label:
  {
  __CrestReturn(33431);
# 294 "kwset.c"
  return;
  }
}
}
# 309 "kwset.c"
static void treefails(struct tree *tree , struct trie *fail , struct trie *recourse )
{
  struct tree *link___0 ;
  struct trie *mem_5 ;
  struct trie *mem_6 ;

  {
  __CrestCall(33432, 274);

  {
  __CrestLoad(33435, (unsigned long )(& tree), (long long )((unsigned long )tree));
  __CrestLoad(33434, (unsigned long )0, (long long )0);
  __CrestApply2(33433, 12, (long long )(tree == 0));
# 317 "kwset.c"
  if (tree == 0) {
    __CrestBranch(33436, 6815, 1);
# 318 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33437, 6817, 0);

  }
  }
# 320 "kwset.c"
  treefails(tree->llink, fail, recourse);
  __CrestClearStack(33438);
# 321 "kwset.c"
  treefails(tree->rlink, fail, recourse);
  __CrestClearStack(33439);
  {
# 325 "kwset.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(33442, (unsigned long )(& fail), (long long )((unsigned long )fail));
    __CrestLoad(33441, (unsigned long )0, (long long )0);
    __CrestApply2(33440, 13, (long long )(fail != 0));
# 325 "kwset.c"
    if (fail != 0) {
      __CrestBranch(33443, 6823, 1);

    } else {
      __CrestBranch(33444, 6824, 0);
# 325 "kwset.c"
      goto while_break;
    }
    }
# 327 "kwset.c"
    link___0 = fail->links;
    {
# 328 "kwset.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(33447, (unsigned long )(& link___0), (long long )((unsigned long )link___0));
      __CrestLoad(33446, (unsigned long )0, (long long )0);
      __CrestApply2(33445, 13, (long long )(link___0 != 0));
# 328 "kwset.c"
      if (link___0 != 0) {
        __CrestBranch(33448, 6830, 1);
        {
        __CrestLoad(33452, (unsigned long )(& tree->label), (long long )tree->label);
        __CrestLoad(33451, (unsigned long )(& link___0->label), (long long )link___0->label);
        __CrestApply2(33450, 13, (long long )((int )tree->label != (int )link___0->label));
# 328 "kwset.c"
        if ((int )tree->label != (int )link___0->label) {
          __CrestBranch(33453, 6831, 1);

        } else {
          __CrestBranch(33454, 6832, 0);
# 328 "kwset.c"
          goto while_break___0;
        }
        }
      } else {
        __CrestBranch(33449, 6833, 0);
# 328 "kwset.c"
        goto while_break___0;
      }
      }
      {
      __CrestLoad(33457, (unsigned long )(& tree->label), (long long )tree->label);
      __CrestLoad(33456, (unsigned long )(& link___0->label), (long long )link___0->label);
      __CrestApply2(33455, 16, (long long )((int )tree->label < (int )link___0->label));
# 329 "kwset.c"
      if ((int )tree->label < (int )link___0->label) {
        __CrestBranch(33458, 6835, 1);
# 330 "kwset.c"
        link___0 = link___0->llink;
      } else {
        __CrestBranch(33459, 6836, 0);
# 332 "kwset.c"
        link___0 = link___0->rlink;
      }
      }
    }
    while_break___0: ;
    }
    {
    __CrestLoad(33462, (unsigned long )(& link___0), (long long )((unsigned long )link___0));
    __CrestLoad(33461, (unsigned long )0, (long long )0);
    __CrestApply2(33460, 13, (long long )(link___0 != 0));
# 333 "kwset.c"
    if (link___0 != 0) {
      __CrestBranch(33463, 6839, 1);
# 335 "kwset.c"
      mem_5 = tree->trie;
# 335 "kwset.c"
      mem_5->fail = link___0->trie;
# 336 "kwset.c"
      goto return_label;
    } else {
      __CrestBranch(33464, 6842, 0);

    }
    }
# 338 "kwset.c"
    fail = fail->fail;
  }
  while_break: ;
  }
# 341 "kwset.c"
  mem_6 = tree->trie;
# 341 "kwset.c"
  mem_6->fail = recourse;

  return_label:
  {
  __CrestReturn(33465);
# 309 "kwset.c"
  return;
  }
}
}
# 346 "kwset.c"
static void treedelta(struct tree *tree , unsigned int depth , unsigned char *delta )
{
  unsigned char *mem_4 ;
  unsigned char *mem_5 ;

  {
  __CrestCall(33467, 275);
  __CrestStore(33466, (unsigned long )(& depth));
  {
  __CrestLoad(33470, (unsigned long )(& tree), (long long )((unsigned long )tree));
  __CrestLoad(33469, (unsigned long )0, (long long )0);
  __CrestApply2(33468, 12, (long long )(tree == 0));
# 352 "kwset.c"
  if (tree == 0) {
    __CrestBranch(33471, 6849, 1);
# 353 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33472, 6851, 0);

  }
  }
  __CrestLoad(33473, (unsigned long )(& depth), (long long )depth);
# 354 "kwset.c"
  treedelta(tree->llink, depth, delta);
  __CrestClearStack(33474);
  __CrestLoad(33475, (unsigned long )(& depth), (long long )depth);
# 355 "kwset.c"
  treedelta(tree->rlink, depth, delta);
  __CrestClearStack(33476);
  {
# 356 "kwset.c"
  mem_4 = delta + tree->label;
  {
  __CrestLoad(33479, (unsigned long )(& depth), (long long )depth);
  __CrestLoad(33478, (unsigned long )mem_4, (long long )*mem_4);
  __CrestApply2(33477, 16, (long long )(depth < (unsigned int )*mem_4));
# 356 "kwset.c"
  if (depth < (unsigned int )*mem_4) {
    __CrestBranch(33480, 6856, 1);
# 357 "kwset.c"
    mem_5 = delta + tree->label;
    __CrestLoad(33482, (unsigned long )(& depth), (long long )depth);
    __CrestStore(33483, (unsigned long )mem_5);
# 357 "kwset.c"
    *mem_5 = (unsigned char )depth;
  } else {
    __CrestBranch(33481, 6857, 0);

  }
  }
  }

  return_label:
  {
  __CrestReturn(33484);
# 346 "kwset.c"
  return;
  }
}
}
# 361 "kwset.c"
static int hasevery(struct tree *a , struct tree *b )
{
  int tmp ;
  int tmp___0 ;
  int __retres5 ;

  {
  __CrestCall(33485, 276);

  {
  __CrestLoad(33488, (unsigned long )(& b), (long long )((unsigned long )b));
  __CrestLoad(33487, (unsigned long )0, (long long )0);
  __CrestApply2(33486, 12, (long long )(b == 0));
# 366 "kwset.c"
  if (b == 0) {
    __CrestBranch(33489, 6861, 1);
    __CrestLoad(33491, (unsigned long )0, (long long )1);
    __CrestStore(33492, (unsigned long )(& __retres5));
# 367 "kwset.c"
    __retres5 = 1;
# 367 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33490, 6863, 0);

  }
  }
# 368 "kwset.c"
  tmp = hasevery(a, b->llink);
  __CrestHandleReturn(33494, (long long )tmp);
  __CrestStore(33493, (unsigned long )(& tmp));
  {
  __CrestLoad(33497, (unsigned long )(& tmp), (long long )tmp);
  __CrestLoad(33496, (unsigned long )0, (long long )0);
  __CrestApply2(33495, 13, (long long )(tmp != 0));
# 368 "kwset.c"
  if (tmp != 0) {
    __CrestBranch(33498, 6866, 1);

  } else {
    __CrestBranch(33499, 6867, 0);
    __CrestLoad(33500, (unsigned long )0, (long long )0);
    __CrestStore(33501, (unsigned long )(& __retres5));
# 369 "kwset.c"
    __retres5 = 0;
# 369 "kwset.c"
    goto return_label;
  }
  }
# 370 "kwset.c"
  tmp___0 = hasevery(a, b->rlink);
  __CrestHandleReturn(33503, (long long )tmp___0);
  __CrestStore(33502, (unsigned long )(& tmp___0));
  {
  __CrestLoad(33506, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestLoad(33505, (unsigned long )0, (long long )0);
  __CrestApply2(33504, 13, (long long )(tmp___0 != 0));
# 370 "kwset.c"
  if (tmp___0 != 0) {
    __CrestBranch(33507, 6871, 1);

  } else {
    __CrestBranch(33508, 6872, 0);
    __CrestLoad(33509, (unsigned long )0, (long long )0);
    __CrestStore(33510, (unsigned long )(& __retres5));
# 371 "kwset.c"
    __retres5 = 0;
# 371 "kwset.c"
    goto return_label;
  }
  }
  {
# 372 "kwset.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(33513, (unsigned long )(& a), (long long )((unsigned long )a));
    __CrestLoad(33512, (unsigned long )0, (long long )0);
    __CrestApply2(33511, 13, (long long )(a != 0));
# 372 "kwset.c"
    if (a != 0) {
      __CrestBranch(33514, 6878, 1);
      {
      __CrestLoad(33518, (unsigned long )(& b->label), (long long )b->label);
      __CrestLoad(33517, (unsigned long )(& a->label), (long long )a->label);
      __CrestApply2(33516, 13, (long long )((int )b->label != (int )a->label));
# 372 "kwset.c"
      if ((int )b->label != (int )a->label) {
        __CrestBranch(33519, 6879, 1);

      } else {
        __CrestBranch(33520, 6880, 0);
# 372 "kwset.c"
        goto while_break;
      }
      }
    } else {
      __CrestBranch(33515, 6881, 0);
# 372 "kwset.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(33523, (unsigned long )(& b->label), (long long )b->label);
    __CrestLoad(33522, (unsigned long )(& a->label), (long long )a->label);
    __CrestApply2(33521, 16, (long long )((int )b->label < (int )a->label));
# 373 "kwset.c"
    if ((int )b->label < (int )a->label) {
      __CrestBranch(33524, 6883, 1);
# 374 "kwset.c"
      a = a->llink;
    } else {
      __CrestBranch(33525, 6884, 0);
# 376 "kwset.c"
      a = a->rlink;
    }
    }
  }
  while_break: ;
  }
  __CrestLoad(33528, (unsigned long )(& a), (long long )((unsigned long )a));
  __CrestApply1(33527, 21, (long long )(! a));
  __CrestApply1(33526, 21, (long long )(! (! a)));
  __CrestStore(33529, (unsigned long )(& __retres5));
# 377 "kwset.c"
  __retres5 = ! (! a);
  return_label:
  {
  __CrestLoad(33530, (unsigned long )(& __retres5), (long long )__retres5);
  __CrestReturn(33531);
# 361 "kwset.c"
  return (__retres5);
  }
}
}
# 382 "kwset.c"
static void treenext(struct tree *tree , struct trie **next )
{
  struct trie **mem_3 ;

  {
  __CrestCall(33532, 277);

  {
  __CrestLoad(33535, (unsigned long )(& tree), (long long )((unsigned long )tree));
  __CrestLoad(33534, (unsigned long )0, (long long )0);
  __CrestApply2(33533, 12, (long long )(tree == 0));
# 387 "kwset.c"
  if (tree == 0) {
    __CrestBranch(33536, 6889, 1);
# 388 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33537, 6891, 0);

  }
  }
# 389 "kwset.c"
  treenext(tree->llink, next);
  __CrestClearStack(33538);
# 390 "kwset.c"
  treenext(tree->rlink, next);
  __CrestClearStack(33539);
# 391 "kwset.c"
  mem_3 = next + tree->label;
# 391 "kwset.c"
  *mem_3 = tree->trie;

  return_label:
  {
  __CrestReturn(33540);
# 382 "kwset.c"
  return;
  }
}
}
# 396 "kwset.c"
char *kwsprep(kwset_t kws )
{
  register struct kwset *kwset ;
  int i ;
  struct trie *curr ;
  struct trie *fail ;
  char *trans ;
  unsigned char delta[256] ;
  struct trie *last ;
  struct trie *next[256] ;
  struct obstack *__h ;
  struct obstack *__o ;
  int __len ;
  struct obstack *__o1 ;
  void *value ;
  int tmp ;
  char *mem_16 ;
  struct tree *mem_17 ;
  struct tree *mem_18 ;
  char *mem_19 ;
  char *mem_20 ;
  char *mem_21 ;
  struct trie *mem_22 ;
  struct trie *mem_23 ;
  struct trie *mem_24 ;
  struct trie *mem_25 ;
  char *mem_26 ;
  char *mem_27 ;
  char *__retres28 ;

  {
  __CrestCall(33541, 278);
# 407 "kwset.c"
  kwset = (struct kwset *)kws;
  {
  __CrestLoad(33544, (unsigned long )(& kwset->mind), (long long )kwset->mind);
  __CrestLoad(33543, (unsigned long )0, (long long )256);
  __CrestApply2(33542, 16, (long long )(kwset->mind < 256));
# 412 "kwset.c"
  if (kwset->mind < 256) {
    __CrestBranch(33545, 6897, 1);
    __CrestLoad(33547, (unsigned long )0, (long long )0);
    __CrestStore(33548, (unsigned long )(& i));
# 413 "kwset.c"
    i = 0;
    {
# 413 "kwset.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(33551, (unsigned long )(& i), (long long )i);
      __CrestLoad(33550, (unsigned long )0, (long long )256);
      __CrestApply2(33549, 16, (long long )(i < 256));
# 413 "kwset.c"
      if (i < 256) {
        __CrestBranch(33552, 6902, 1);

      } else {
        __CrestBranch(33553, 6903, 0);
# 413 "kwset.c"
        goto while_break;
      }
      }
      __CrestLoad(33554, (unsigned long )(& kwset->mind), (long long )kwset->mind);
      __CrestStore(33555, (unsigned long )(& delta[i]));
# 414 "kwset.c"
      delta[i] = (unsigned char )kwset->mind;
      __CrestLoad(33558, (unsigned long )(& i), (long long )i);
      __CrestLoad(33557, (unsigned long )0, (long long )1);
      __CrestApply2(33556, 0, (long long )(i + 1));
      __CrestStore(33559, (unsigned long )(& i));
# 413 "kwset.c"
      i ++;
    }
    while_break: ;
    }
  } else {
    __CrestBranch(33546, 6906, 0);
    __CrestLoad(33560, (unsigned long )0, (long long )0);
    __CrestStore(33561, (unsigned long )(& i));
# 416 "kwset.c"
    i = 0;
    {
# 416 "kwset.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(33564, (unsigned long )(& i), (long long )i);
      __CrestLoad(33563, (unsigned long )0, (long long )256);
      __CrestApply2(33562, 16, (long long )(i < 256));
# 416 "kwset.c"
      if (i < 256) {
        __CrestBranch(33565, 6911, 1);

      } else {
        __CrestBranch(33566, 6912, 0);
# 416 "kwset.c"
        goto while_break___0;
      }
      }
      __CrestLoad(33567, (unsigned long )0, (long long )(unsigned char)255);
      __CrestStore(33568, (unsigned long )(& delta[i]));
# 417 "kwset.c"
      delta[i] = (unsigned char)255;
      __CrestLoad(33571, (unsigned long )(& i), (long long )i);
      __CrestLoad(33570, (unsigned long )0, (long long )1);
      __CrestApply2(33569, 0, (long long )(i + 1));
      __CrestStore(33572, (unsigned long )(& i));
# 416 "kwset.c"
      i ++;
    }
    while_break___0: ;
    }
  }
  }
  {
  __CrestLoad(33575, (unsigned long )(& kwset->words), (long long )kwset->words);
  __CrestLoad(33574, (unsigned long )0, (long long )1);
  __CrestApply2(33573, 12, (long long )(kwset->words == 1));
# 421 "kwset.c"
  if (kwset->words == 1) {
    __CrestBranch(33576, 6916, 1);
    {
    __CrestLoad(33580, (unsigned long )(& kwset->trans), (long long )((unsigned long )kwset->trans));
    __CrestLoad(33579, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(33578, 12, (long long )((unsigned long )kwset->trans == (unsigned long )((char *)0)));
# 421 "kwset.c"
    if ((unsigned long )kwset->trans == (unsigned long )((char *)0)) {
      __CrestBranch(33581, 6917, 1);
# 424 "kwset.c"
      __h = & kwset->obstack;
# 424 "kwset.c"
      __o = __h;
      __CrestLoad(33583, (unsigned long )(& kwset->mind), (long long )kwset->mind);
      __CrestStore(33584, (unsigned long )(& __len));
# 424 "kwset.c"
      __len = kwset->mind;
      {
      __CrestLoad(33589, (unsigned long )(& __o->chunk_limit), (long long )((unsigned long )__o->chunk_limit));
      __CrestLoad(33588, (unsigned long )(& __o->next_free), (long long )((unsigned long )__o->next_free));
      __CrestApply2(33587, 18, (long long )(__o->chunk_limit - __o->next_free));
      __CrestLoad(33586, (unsigned long )(& __len), (long long )__len);
      __CrestApply2(33585, 16, (long long )(__o->chunk_limit - __o->next_free < (long )__len));
# 424 "kwset.c"
      if (__o->chunk_limit - __o->next_free < (long )__len) {
        __CrestBranch(33590, 6919, 1);
        __CrestLoad(33592, (unsigned long )(& __len), (long long )__len);
# 424 "kwset.c"
        _obstack_newchunk(__o, __len);
        __CrestClearStack(33593);
      } else {
        __CrestBranch(33591, 6920, 0);

      }
      }
# 424 "kwset.c"
      __o->next_free += __len;
# 424 "kwset.c"
      __o1 = __h;
# 424 "kwset.c"
      value = (void *)__o1->object_base;
      {
      __CrestLoad(33596, (unsigned long )(& __o1->next_free), (long long )((unsigned long )__o1->next_free));
      __CrestLoad(33595, (unsigned long )(& value), (long long )((unsigned long )value));
      __CrestApply2(33594, 12, (long long )((unsigned long )__o1->next_free == (unsigned long )value));
# 424 "kwset.c"
      if ((unsigned long )__o1->next_free == (unsigned long )value) {
        __CrestBranch(33597, 6923, 1);
# 424 "kwset.c"
        __o1->maybe_empty_object = 1U;
      } else {
        __CrestBranch(33598, 6924, 0);

      }
      }
# 424 "kwset.c"
      __o1->next_free = (char *)0 + (((__o1->next_free - (char *)0) + (long )__o1->alignment_mask) & (long )(~ __o1->alignment_mask));
      {
      __CrestLoad(33605, (unsigned long )(& __o1->next_free), (long long )((unsigned long )__o1->next_free));
      __CrestLoad(33604, (unsigned long )(& __o1->chunk), (long long )((unsigned long )__o1->chunk));
      __CrestApply2(33603, 18, (long long )(__o1->next_free - (char *)__o1->chunk));
      __CrestLoad(33602, (unsigned long )(& __o1->chunk_limit), (long long )((unsigned long )__o1->chunk_limit));
      __CrestLoad(33601, (unsigned long )(& __o1->chunk), (long long )((unsigned long )__o1->chunk));
      __CrestApply2(33600, 18, (long long )(__o1->chunk_limit - (char *)__o1->chunk));
      __CrestApply2(33599, 14, (long long )(__o1->next_free - (char *)__o1->chunk > __o1->chunk_limit - (char *)__o1->chunk));
# 424 "kwset.c"
      if (__o1->next_free - (char *)__o1->chunk > __o1->chunk_limit - (char *)__o1->chunk) {
        __CrestBranch(33606, 6927, 1);
# 424 "kwset.c"
        __o1->next_free = __o1->chunk_limit;
      } else {
        __CrestBranch(33607, 6928, 0);

      }
      }
# 424 "kwset.c"
      __o1->object_base = __o1->next_free;
# 424 "kwset.c"
      kwset->target = (char *)value;
      __CrestLoad(33610, (unsigned long )(& kwset->mind), (long long )kwset->mind);
      __CrestLoad(33609, (unsigned long )0, (long long )1);
      __CrestApply2(33608, 1, (long long )(kwset->mind - 1));
      __CrestStore(33611, (unsigned long )(& i));
# 425 "kwset.c"
      i = kwset->mind - 1;
# 425 "kwset.c"
      curr = kwset->trie;
      {
# 425 "kwset.c"
      while (1) {
        while_continue___1: ;
        {
        __CrestLoad(33614, (unsigned long )(& i), (long long )i);
        __CrestLoad(33613, (unsigned long )0, (long long )0);
        __CrestApply2(33612, 17, (long long )(i >= 0));
# 425 "kwset.c"
        if (i >= 0) {
          __CrestBranch(33615, 6934, 1);

        } else {
          __CrestBranch(33616, 6935, 0);
# 425 "kwset.c"
          goto while_break___1;
        }
        }
# 427 "kwset.c"
        mem_16 = kwset->target + i;
# 427 "kwset.c"
        mem_17 = curr->links;
        __CrestLoad(33617, (unsigned long )(& mem_17->label), (long long )mem_17->label);
        __CrestStore(33618, (unsigned long )mem_16);
# 427 "kwset.c"
        *mem_16 = (char )mem_17->label;
# 428 "kwset.c"
        mem_18 = curr->links;
# 428 "kwset.c"
        curr = mem_18->trie;
        __CrestLoad(33621, (unsigned long )(& i), (long long )i);
        __CrestLoad(33620, (unsigned long )0, (long long )1);
        __CrestApply2(33619, 1, (long long )(i - 1));
        __CrestStore(33622, (unsigned long )(& i));
# 425 "kwset.c"
        i --;
      }
      while_break___1: ;
      }
      __CrestLoad(33623, (unsigned long )0, (long long )0);
      __CrestStore(33624, (unsigned long )(& i));
# 431 "kwset.c"
      i = 0;
      {
# 431 "kwset.c"
      while (1) {
        while_continue___2: ;
        {
        __CrestLoad(33627, (unsigned long )(& i), (long long )i);
        __CrestLoad(33626, (unsigned long )(& kwset->mind), (long long )kwset->mind);
        __CrestApply2(33625, 16, (long long )(i < kwset->mind));
# 431 "kwset.c"
        if (i < kwset->mind) {
          __CrestBranch(33628, 6943, 1);

        } else {
          __CrestBranch(33629, 6944, 0);
# 431 "kwset.c"
          goto while_break___2;
        }
        }
# 432 "kwset.c"
        mem_19 = kwset->target + i;
        __CrestLoad(33634, (unsigned long )(& kwset->mind), (long long )kwset->mind);
        __CrestLoad(33633, (unsigned long )(& i), (long long )i);
        __CrestLoad(33632, (unsigned long )0, (long long )1);
        __CrestApply2(33631, 0, (long long )(i + 1));
        __CrestApply2(33630, 1, (long long )(kwset->mind - (i + 1)));
        __CrestStore(33635, (unsigned long )(& delta[(unsigned char )*mem_19]));
# 432 "kwset.c"
        delta[(unsigned char )*mem_19] = (unsigned char )(kwset->mind - (i + 1));
        __CrestLoad(33638, (unsigned long )(& i), (long long )i);
        __CrestLoad(33637, (unsigned long )0, (long long )1);
        __CrestApply2(33636, 0, (long long )(i + 1));
        __CrestStore(33639, (unsigned long )(& i));
# 431 "kwset.c"
        i ++;
      }
      while_break___2: ;
      }
      __CrestLoad(33640, (unsigned long )(& kwset->mind), (long long )kwset->mind);
      __CrestStore(33641, (unsigned long )(& kwset->mind2));
# 433 "kwset.c"
      kwset->mind2 = kwset->mind;
      __CrestLoad(33642, (unsigned long )0, (long long )0);
      __CrestStore(33643, (unsigned long )(& i));
# 436 "kwset.c"
      i = 0;
      {
# 436 "kwset.c"
      while (1) {
        while_continue___3: ;
        {
        __CrestLoad(33648, (unsigned long )(& i), (long long )i);
        __CrestLoad(33647, (unsigned long )(& kwset->mind), (long long )kwset->mind);
        __CrestLoad(33646, (unsigned long )0, (long long )1);
        __CrestApply2(33645, 1, (long long )(kwset->mind - 1));
        __CrestApply2(33644, 16, (long long )(i < kwset->mind - 1));
# 436 "kwset.c"
        if (i < kwset->mind - 1) {
          __CrestBranch(33649, 6952, 1);

        } else {
          __CrestBranch(33650, 6953, 0);
# 436 "kwset.c"
          goto while_break___3;
        }
        }
        {
# 437 "kwset.c"
        mem_20 = kwset->target + i;
# 437 "kwset.c"
        mem_21 = kwset->target + (kwset->mind - 1);
        {
        __CrestLoad(33653, (unsigned long )mem_20, (long long )*mem_20);
        __CrestLoad(33652, (unsigned long )mem_21, (long long )*mem_21);
        __CrestApply2(33651, 12, (long long )((int )*mem_20 == (int )*mem_21));
# 437 "kwset.c"
        if ((int )*mem_20 == (int )*mem_21) {
          __CrestBranch(33654, 6957, 1);
          __CrestLoad(33660, (unsigned long )(& kwset->mind), (long long )kwset->mind);
          __CrestLoad(33659, (unsigned long )(& i), (long long )i);
          __CrestLoad(33658, (unsigned long )0, (long long )1);
          __CrestApply2(33657, 0, (long long )(i + 1));
          __CrestApply2(33656, 1, (long long )(kwset->mind - (i + 1)));
          __CrestStore(33661, (unsigned long )(& kwset->mind2));
# 438 "kwset.c"
          kwset->mind2 = kwset->mind - (i + 1);
        } else {
          __CrestBranch(33655, 6958, 0);

        }
        }
        }
        __CrestLoad(33664, (unsigned long )(& i), (long long )i);
        __CrestLoad(33663, (unsigned long )0, (long long )1);
        __CrestApply2(33662, 0, (long long )(i + 1));
        __CrestStore(33665, (unsigned long )(& i));
# 436 "kwset.c"
        i ++;
      }
      while_break___3: ;
      }
    } else {
      __CrestBranch(33582, 6961, 0);
# 421 "kwset.c"
      goto _L;
    }
    }
  } else {
    __CrestBranch(33577, 6962, 0);
    _L:
# 444 "kwset.c"
    last = kwset->trie;
# 444 "kwset.c"
    curr = last;
    {
# 444 "kwset.c"
    while (1) {
      while_continue___4: ;
      {
      __CrestLoad(33668, (unsigned long )(& curr), (long long )((unsigned long )curr));
      __CrestLoad(33667, (unsigned long )0, (long long )0);
      __CrestApply2(33666, 13, (long long )(curr != 0));
# 444 "kwset.c"
      if (curr != 0) {
        __CrestBranch(33669, 6967, 1);

      } else {
        __CrestBranch(33670, 6968, 0);
# 444 "kwset.c"
        goto while_break___4;
      }
      }
# 447 "kwset.c"
      enqueue(curr->links, & last);
      __CrestClearStack(33671);
      __CrestLoad(33672, (unsigned long )(& kwset->mind), (long long )kwset->mind);
      __CrestStore(33673, (unsigned long )(& curr->shift));
# 449 "kwset.c"
      curr->shift = kwset->mind;
      __CrestLoad(33674, (unsigned long )(& kwset->mind), (long long )kwset->mind);
      __CrestStore(33675, (unsigned long )(& curr->maxshift));
# 450 "kwset.c"
      curr->maxshift = kwset->mind;
      __CrestLoad(33676, (unsigned long )(& curr->depth), (long long )curr->depth);
# 453 "kwset.c"
      treedelta(curr->links, (unsigned int )curr->depth, delta);
      __CrestClearStack(33677);
# 456 "kwset.c"
      treefails(curr->links, curr->fail, kwset->trie);
      __CrestClearStack(33678);
# 460 "kwset.c"
      fail = curr->fail;
      {
# 460 "kwset.c"
      while (1) {
        while_continue___5: ;
        {
        __CrestLoad(33681, (unsigned long )(& fail), (long long )((unsigned long )fail));
        __CrestLoad(33680, (unsigned long )0, (long long )0);
        __CrestApply2(33679, 13, (long long )(fail != 0));
# 460 "kwset.c"
        if (fail != 0) {
          __CrestBranch(33682, 6974, 1);

        } else {
          __CrestBranch(33683, 6975, 0);
# 460 "kwset.c"
          goto while_break___5;
        }
        }
# 465 "kwset.c"
        tmp = hasevery(fail->links, curr->links);
        __CrestHandleReturn(33685, (long long )tmp);
        __CrestStore(33684, (unsigned long )(& tmp));
        {
        __CrestLoad(33688, (unsigned long )(& tmp), (long long )tmp);
        __CrestLoad(33687, (unsigned long )0, (long long )0);
        __CrestApply2(33686, 13, (long long )(tmp != 0));
# 465 "kwset.c"
        if (tmp != 0) {
          __CrestBranch(33689, 6978, 1);

        } else {
          __CrestBranch(33690, 6979, 0);
          {
          __CrestLoad(33695, (unsigned long )(& curr->depth), (long long )curr->depth);
          __CrestLoad(33694, (unsigned long )(& fail->depth), (long long )fail->depth);
          __CrestApply2(33693, 1, (long long )(curr->depth - fail->depth));
          __CrestLoad(33692, (unsigned long )(& fail->shift), (long long )fail->shift);
          __CrestApply2(33691, 16, (long long )(curr->depth - fail->depth < fail->shift));
# 466 "kwset.c"
          if (curr->depth - fail->depth < fail->shift) {
            __CrestBranch(33696, 6980, 1);
            __CrestLoad(33700, (unsigned long )(& curr->depth), (long long )curr->depth);
            __CrestLoad(33699, (unsigned long )(& fail->depth), (long long )fail->depth);
            __CrestApply2(33698, 1, (long long )(curr->depth - fail->depth));
            __CrestStore(33701, (unsigned long )(& fail->shift));
# 467 "kwset.c"
            fail->shift = curr->depth - fail->depth;
          } else {
            __CrestBranch(33697, 6981, 0);

          }
          }
        }
        }
        {
        __CrestLoad(33704, (unsigned long )(& curr->accepting), (long long )curr->accepting);
        __CrestLoad(33703, (unsigned long )0, (long long )0);
        __CrestApply2(33702, 13, (long long )(curr->accepting != 0));
# 472 "kwset.c"
        if (curr->accepting != 0) {
          __CrestBranch(33705, 6983, 1);
          {
          __CrestLoad(33711, (unsigned long )(& fail->maxshift), (long long )fail->maxshift);
          __CrestLoad(33710, (unsigned long )(& curr->depth), (long long )curr->depth);
          __CrestLoad(33709, (unsigned long )(& fail->depth), (long long )fail->depth);
          __CrestApply2(33708, 1, (long long )(curr->depth - fail->depth));
          __CrestApply2(33707, 14, (long long )(fail->maxshift > curr->depth - fail->depth));
# 472 "kwset.c"
          if (fail->maxshift > curr->depth - fail->depth) {
            __CrestBranch(33712, 6984, 1);
            __CrestLoad(33716, (unsigned long )(& curr->depth), (long long )curr->depth);
            __CrestLoad(33715, (unsigned long )(& fail->depth), (long long )fail->depth);
            __CrestApply2(33714, 1, (long long )(curr->depth - fail->depth));
            __CrestStore(33717, (unsigned long )(& fail->maxshift));
# 473 "kwset.c"
            fail->maxshift = curr->depth - fail->depth;
          } else {
            __CrestBranch(33713, 6985, 0);

          }
          }
        } else {
          __CrestBranch(33706, 6986, 0);

        }
        }
# 460 "kwset.c"
        fail = fail->fail;
      }
      while_break___5: ;
      }
# 444 "kwset.c"
      curr = curr->next;
    }
    while_break___4: ;
    }
# 479 "kwset.c"
    mem_22 = kwset->trie;
# 479 "kwset.c"
    curr = mem_22->next;
    {
# 479 "kwset.c"
    while (1) {
      while_continue___6: ;
      {
      __CrestLoad(33720, (unsigned long )(& curr), (long long )((unsigned long )curr));
      __CrestLoad(33719, (unsigned long )0, (long long )0);
      __CrestApply2(33718, 13, (long long )(curr != 0));
# 479 "kwset.c"
      if (curr != 0) {
        __CrestBranch(33721, 6996, 1);

      } else {
        __CrestBranch(33722, 6997, 0);
# 479 "kwset.c"
        goto while_break___6;
      }
      }
      {
# 481 "kwset.c"
      mem_23 = curr->parent;
      {
      __CrestLoad(33725, (unsigned long )(& curr->maxshift), (long long )curr->maxshift);
      __CrestLoad(33724, (unsigned long )(& mem_23->maxshift), (long long )mem_23->maxshift);
      __CrestApply2(33723, 14, (long long )(curr->maxshift > mem_23->maxshift));
# 481 "kwset.c"
      if (curr->maxshift > mem_23->maxshift) {
        __CrestBranch(33726, 7001, 1);
# 482 "kwset.c"
        mem_24 = curr->parent;
        __CrestLoad(33728, (unsigned long )(& mem_24->maxshift), (long long )mem_24->maxshift);
        __CrestStore(33729, (unsigned long )(& curr->maxshift));
# 482 "kwset.c"
        curr->maxshift = mem_24->maxshift;
      } else {
        __CrestBranch(33727, 7002, 0);

      }
      }
      }
      {
      __CrestLoad(33732, (unsigned long )(& curr->shift), (long long )curr->shift);
      __CrestLoad(33731, (unsigned long )(& curr->maxshift), (long long )curr->maxshift);
      __CrestApply2(33730, 14, (long long )(curr->shift > curr->maxshift));
# 483 "kwset.c"
      if (curr->shift > curr->maxshift) {
        __CrestBranch(33733, 7004, 1);
        __CrestLoad(33735, (unsigned long )(& curr->maxshift), (long long )curr->maxshift);
        __CrestStore(33736, (unsigned long )(& curr->shift));
# 484 "kwset.c"
        curr->shift = curr->maxshift;
      } else {
        __CrestBranch(33734, 7005, 0);

      }
      }
# 479 "kwset.c"
      curr = curr->next;
    }
    while_break___6: ;
    }
    __CrestLoad(33737, (unsigned long )0, (long long )0);
    __CrestStore(33738, (unsigned long )(& i));
# 489 "kwset.c"
    i = 0;
    {
# 489 "kwset.c"
    while (1) {
      while_continue___7: ;
      {
      __CrestLoad(33741, (unsigned long )(& i), (long long )i);
      __CrestLoad(33740, (unsigned long )0, (long long )256);
      __CrestApply2(33739, 16, (long long )(i < 256));
# 489 "kwset.c"
      if (i < 256) {
        __CrestBranch(33742, 7013, 1);

      } else {
        __CrestBranch(33743, 7014, 0);
# 489 "kwset.c"
        goto while_break___7;
      }
      }
# 490 "kwset.c"
      next[i] = (struct trie *)0;
      __CrestLoad(33746, (unsigned long )(& i), (long long )i);
      __CrestLoad(33745, (unsigned long )0, (long long )1);
      __CrestApply2(33744, 0, (long long )(i + 1));
      __CrestStore(33747, (unsigned long )(& i));
# 489 "kwset.c"
      i ++;
    }
    while_break___7: ;
    }
# 491 "kwset.c"
    mem_25 = kwset->trie;
# 491 "kwset.c"
    treenext(mem_25->links, next);
    __CrestClearStack(33748);
# 493 "kwset.c"
    trans = kwset->trans;
    {
    __CrestLoad(33751, (unsigned long )(& trans), (long long )((unsigned long )trans));
    __CrestLoad(33750, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(33749, 13, (long long )((unsigned long )trans != (unsigned long )((char *)0)));
# 493 "kwset.c"
    if ((unsigned long )trans != (unsigned long )((char *)0)) {
      __CrestBranch(33752, 7019, 1);
      __CrestLoad(33754, (unsigned long )0, (long long )0);
      __CrestStore(33755, (unsigned long )(& i));
# 494 "kwset.c"
      i = 0;
      {
# 494 "kwset.c"
      while (1) {
        while_continue___8: ;
        {
        __CrestLoad(33758, (unsigned long )(& i), (long long )i);
        __CrestLoad(33757, (unsigned long )0, (long long )256);
        __CrestApply2(33756, 16, (long long )(i < 256));
# 494 "kwset.c"
        if (i < 256) {
          __CrestBranch(33759, 7024, 1);

        } else {
          __CrestBranch(33760, 7025, 0);
# 494 "kwset.c"
          goto while_break___8;
        }
        }
# 495 "kwset.c"
        mem_26 = trans + i;
# 495 "kwset.c"
        kwset->next[i] = next[(unsigned char )*mem_26];
        __CrestLoad(33763, (unsigned long )(& i), (long long )i);
        __CrestLoad(33762, (unsigned long )0, (long long )1);
        __CrestApply2(33761, 0, (long long )(i + 1));
        __CrestStore(33764, (unsigned long )(& i));
# 494 "kwset.c"
        i ++;
      }
      while_break___8: ;
      }
    } else {
      __CrestBranch(33753, 7028, 0);
      __CrestLoad(33765, (unsigned long )0, (long long )0);
      __CrestStore(33766, (unsigned long )(& i));
# 497 "kwset.c"
      i = 0;
      {
# 497 "kwset.c"
      while (1) {
        while_continue___9: ;
        {
        __CrestLoad(33769, (unsigned long )(& i), (long long )i);
        __CrestLoad(33768, (unsigned long )0, (long long )256);
        __CrestApply2(33767, 16, (long long )(i < 256));
# 497 "kwset.c"
        if (i < 256) {
          __CrestBranch(33770, 7033, 1);

        } else {
          __CrestBranch(33771, 7034, 0);
# 497 "kwset.c"
          goto while_break___9;
        }
        }
# 498 "kwset.c"
        kwset->next[i] = next[i];
        __CrestLoad(33774, (unsigned long )(& i), (long long )i);
        __CrestLoad(33773, (unsigned long )0, (long long )1);
        __CrestApply2(33772, 0, (long long )(i + 1));
        __CrestStore(33775, (unsigned long )(& i));
# 497 "kwset.c"
        i ++;
      }
      while_break___9: ;
      }
    }
    }
  }
  }
# 502 "kwset.c"
  trans = kwset->trans;
  {
  __CrestLoad(33778, (unsigned long )(& trans), (long long )((unsigned long )trans));
  __CrestLoad(33777, (unsigned long )0, (long long )((unsigned long )((char *)0)));
  __CrestApply2(33776, 13, (long long )((unsigned long )trans != (unsigned long )((char *)0)));
# 502 "kwset.c"
  if ((unsigned long )trans != (unsigned long )((char *)0)) {
    __CrestBranch(33779, 7039, 1);
    __CrestLoad(33781, (unsigned long )0, (long long )0);
    __CrestStore(33782, (unsigned long )(& i));
# 503 "kwset.c"
    i = 0;
    {
# 503 "kwset.c"
    while (1) {
      while_continue___10: ;
      {
      __CrestLoad(33785, (unsigned long )(& i), (long long )i);
      __CrestLoad(33784, (unsigned long )0, (long long )256);
      __CrestApply2(33783, 16, (long long )(i < 256));
# 503 "kwset.c"
      if (i < 256) {
        __CrestBranch(33786, 7044, 1);

      } else {
        __CrestBranch(33787, 7045, 0);
# 503 "kwset.c"
        goto while_break___10;
      }
      }
# 504 "kwset.c"
      mem_27 = trans + i;
      __CrestLoad(33788, (unsigned long )(& delta[(unsigned char )*mem_27]), (long long )delta[(unsigned char )*mem_27]);
      __CrestStore(33789, (unsigned long )(& kwset->delta[i]));
# 504 "kwset.c"
      kwset->delta[i] = delta[(unsigned char )*mem_27];
      __CrestLoad(33792, (unsigned long )(& i), (long long )i);
      __CrestLoad(33791, (unsigned long )0, (long long )1);
      __CrestApply2(33790, 0, (long long )(i + 1));
      __CrestStore(33793, (unsigned long )(& i));
# 503 "kwset.c"
      i ++;
    }
    while_break___10: ;
    }
  } else {
    __CrestBranch(33780, 7048, 0);
    __CrestLoad(33794, (unsigned long )0, (long long )0);
    __CrestStore(33795, (unsigned long )(& i));
# 506 "kwset.c"
    i = 0;
    {
# 506 "kwset.c"
    while (1) {
      while_continue___11: ;
      {
      __CrestLoad(33798, (unsigned long )(& i), (long long )i);
      __CrestLoad(33797, (unsigned long )0, (long long )256);
      __CrestApply2(33796, 16, (long long )(i < 256));
# 506 "kwset.c"
      if (i < 256) {
        __CrestBranch(33799, 7053, 1);

      } else {
        __CrestBranch(33800, 7054, 0);
# 506 "kwset.c"
        goto while_break___11;
      }
      }
      __CrestLoad(33801, (unsigned long )(& delta[i]), (long long )delta[i]);
      __CrestStore(33802, (unsigned long )(& kwset->delta[i]));
# 507 "kwset.c"
      kwset->delta[i] = delta[i];
      __CrestLoad(33805, (unsigned long )(& i), (long long )i);
      __CrestLoad(33804, (unsigned long )0, (long long )1);
      __CrestApply2(33803, 0, (long long )(i + 1));
      __CrestStore(33806, (unsigned long )(& i));
# 506 "kwset.c"
      i ++;
    }
    while_break___11: ;
    }
  }
  }
# 509 "kwset.c"
  __retres28 = (char *)0;
  {
  __CrestReturn(33807);
# 396 "kwset.c"
  return (__retres28);
  }
}
}
# 515 "kwset.c"
static char *bmexec(kwset_t kws , char *text , size_t size )
{
  struct kwset *kwset ;
  register unsigned char *d1 ;
  char *ep ;
  register char *sp ;
  char *tp ;
  int d ;
  int gc ;
  int i ;
  int len ;
  int md2 ;
  void *tmp ;
  char *mem_15 ;
  char *mem_16 ;
  char *mem_17 ;
  unsigned char *mem_18 ;
  char *mem_19 ;
  unsigned char *mem_20 ;
  char *mem_21 ;
  unsigned char *mem_22 ;
  char *mem_23 ;
  unsigned char *mem_24 ;
  char *mem_25 ;
  unsigned char *mem_26 ;
  char *mem_27 ;
  unsigned char *mem_28 ;
  char *mem_29 ;
  unsigned char *mem_30 ;
  char *mem_31 ;
  unsigned char *mem_32 ;
  char *mem_33 ;
  unsigned char *mem_34 ;
  char *mem_35 ;
  unsigned char *mem_36 ;
  char *mem_37 ;
  char *mem_38 ;
  char *mem_39 ;
  char *mem_40 ;
  unsigned char *mem_41 ;
  char *mem_42 ;
  unsigned char *mem_43 ;
  char *mem_44 ;
  char *mem_45 ;
  char *mem_46 ;
  char *__retres47 ;

  {
  __CrestCall(33809, 279);
  __CrestStore(33808, (unsigned long )(& size));
# 526 "kwset.c"
  kwset = (struct kwset *)kws;
  __CrestLoad(33810, (unsigned long )(& kwset->mind), (long long )kwset->mind);
  __CrestStore(33811, (unsigned long )(& len));
# 527 "kwset.c"
  len = kwset->mind;
  {
  __CrestLoad(33814, (unsigned long )(& len), (long long )len);
  __CrestLoad(33813, (unsigned long )0, (long long )0);
  __CrestApply2(33812, 12, (long long )(len == 0));
# 529 "kwset.c"
  if (len == 0) {
    __CrestBranch(33815, 7061, 1);
# 530 "kwset.c"
    __retres47 = text;
# 530 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33816, 7063, 0);

  }
  }
  {
  __CrestLoad(33819, (unsigned long )(& len), (long long )len);
  __CrestLoad(33818, (unsigned long )(& size), (long long )size);
  __CrestApply2(33817, 14, (long long )((size_t )len > size));
# 531 "kwset.c"
  if ((size_t )len > size) {
    __CrestBranch(33820, 7065, 1);
# 532 "kwset.c"
    __retres47 = (char *)0;
# 532 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33821, 7067, 0);

  }
  }
  {
  __CrestLoad(33824, (unsigned long )(& len), (long long )len);
  __CrestLoad(33823, (unsigned long )0, (long long )1);
  __CrestApply2(33822, 12, (long long )(len == 1));
# 533 "kwset.c"
  if (len == 1) {
    __CrestBranch(33825, 7069, 1);
# 534 "kwset.c"
    mem_15 = kwset->target + 0;
    __CrestLoad(33827, (unsigned long )mem_15, (long long )*mem_15);
    __CrestLoad(33828, (unsigned long )(& size), (long long )size);
# 534 "kwset.c"
    tmp = memchr((void const *)text, (int )*mem_15, size);
    __CrestClearStack(33829);
# 534 "kwset.c"
    __retres47 = (char *)tmp;
# 534 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33826, 7072, 0);

  }
  }
# 536 "kwset.c"
  d1 = kwset->delta;
# 537 "kwset.c"
  sp = kwset->target + len;
# 538 "kwset.c"
  mem_16 = sp + -2;
  __CrestLoad(33830, (unsigned long )mem_16, (long long )*mem_16);
  __CrestStore(33831, (unsigned long )(& gc));
# 538 "kwset.c"
  gc = (int )((unsigned char )*mem_16);
  __CrestLoad(33832, (unsigned long )(& kwset->mind2), (long long )kwset->mind2);
  __CrestStore(33833, (unsigned long )(& md2));
# 539 "kwset.c"
  md2 = kwset->mind2;
# 540 "kwset.c"
  tp = text + len;
  {
  __CrestLoad(33838, (unsigned long )(& size), (long long )size);
  __CrestLoad(33837, (unsigned long )0, (long long )12);
  __CrestLoad(33836, (unsigned long )(& len), (long long )len);
  __CrestApply2(33835, 2, (long long )(12 * len));
  __CrestApply2(33834, 14, (long long )(size > (size_t )(12 * len)));
# 543 "kwset.c"
  if (size > (size_t )(12 * len)) {
    __CrestBranch(33839, 7075, 1);
# 545 "kwset.c"
    ep = (text + size) - 11 * len;
    {
# 545 "kwset.c"
    while (1) {
      while_continue: ;
      {
# 547 "kwset.c"
      while (1) {
        while_continue___0: ;
        {
        __CrestLoad(33843, (unsigned long )(& tp), (long long )((unsigned long )tp));
        __CrestLoad(33842, (unsigned long )(& ep), (long long )((unsigned long )ep));
        __CrestApply2(33841, 15, (long long )((unsigned long )tp <= (unsigned long )ep));
# 547 "kwset.c"
        if ((unsigned long )tp <= (unsigned long )ep) {
          __CrestBranch(33844, 7083, 1);

        } else {
          __CrestBranch(33845, 7084, 0);
# 547 "kwset.c"
          goto while_break___0;
        }
        }
# 549 "kwset.c"
        mem_17 = tp + -1;
# 549 "kwset.c"
        mem_18 = d1 + (unsigned char )*mem_17;
        __CrestLoad(33846, (unsigned long )mem_18, (long long )*mem_18);
        __CrestStore(33847, (unsigned long )(& d));
# 549 "kwset.c"
        d = (int )*mem_18;
# 549 "kwset.c"
        tp += d;
# 550 "kwset.c"
        mem_19 = tp + -1;
# 550 "kwset.c"
        mem_20 = d1 + (unsigned char )*mem_19;
        __CrestLoad(33848, (unsigned long )mem_20, (long long )*mem_20);
        __CrestStore(33849, (unsigned long )(& d));
# 550 "kwset.c"
        d = (int )*mem_20;
# 550 "kwset.c"
        tp += d;
        {
        __CrestLoad(33852, (unsigned long )(& d), (long long )d);
        __CrestLoad(33851, (unsigned long )0, (long long )0);
        __CrestApply2(33850, 12, (long long )(d == 0));
# 551 "kwset.c"
        if (d == 0) {
          __CrestBranch(33853, 7087, 1);
# 552 "kwset.c"
          goto found;
        } else {
          __CrestBranch(33854, 7088, 0);

        }
        }
# 553 "kwset.c"
        mem_21 = tp + -1;
# 553 "kwset.c"
        mem_22 = d1 + (unsigned char )*mem_21;
        __CrestLoad(33855, (unsigned long )mem_22, (long long )*mem_22);
        __CrestStore(33856, (unsigned long )(& d));
# 553 "kwset.c"
        d = (int )*mem_22;
# 553 "kwset.c"
        tp += d;
# 554 "kwset.c"
        mem_23 = tp + -1;
# 554 "kwset.c"
        mem_24 = d1 + (unsigned char )*mem_23;
        __CrestLoad(33857, (unsigned long )mem_24, (long long )*mem_24);
        __CrestStore(33858, (unsigned long )(& d));
# 554 "kwset.c"
        d = (int )*mem_24;
# 554 "kwset.c"
        tp += d;
# 555 "kwset.c"
        mem_25 = tp + -1;
# 555 "kwset.c"
        mem_26 = d1 + (unsigned char )*mem_25;
        __CrestLoad(33859, (unsigned long )mem_26, (long long )*mem_26);
        __CrestStore(33860, (unsigned long )(& d));
# 555 "kwset.c"
        d = (int )*mem_26;
# 555 "kwset.c"
        tp += d;
        {
        __CrestLoad(33863, (unsigned long )(& d), (long long )d);
        __CrestLoad(33862, (unsigned long )0, (long long )0);
        __CrestApply2(33861, 12, (long long )(d == 0));
# 556 "kwset.c"
        if (d == 0) {
          __CrestBranch(33864, 7091, 1);
# 557 "kwset.c"
          goto found;
        } else {
          __CrestBranch(33865, 7092, 0);

        }
        }
# 558 "kwset.c"
        mem_27 = tp + -1;
# 558 "kwset.c"
        mem_28 = d1 + (unsigned char )*mem_27;
        __CrestLoad(33866, (unsigned long )mem_28, (long long )*mem_28);
        __CrestStore(33867, (unsigned long )(& d));
# 558 "kwset.c"
        d = (int )*mem_28;
# 558 "kwset.c"
        tp += d;
# 559 "kwset.c"
        mem_29 = tp + -1;
# 559 "kwset.c"
        mem_30 = d1 + (unsigned char )*mem_29;
        __CrestLoad(33868, (unsigned long )mem_30, (long long )*mem_30);
        __CrestStore(33869, (unsigned long )(& d));
# 559 "kwset.c"
        d = (int )*mem_30;
# 559 "kwset.c"
        tp += d;
# 560 "kwset.c"
        mem_31 = tp + -1;
# 560 "kwset.c"
        mem_32 = d1 + (unsigned char )*mem_31;
        __CrestLoad(33870, (unsigned long )mem_32, (long long )*mem_32);
        __CrestStore(33871, (unsigned long )(& d));
# 560 "kwset.c"
        d = (int )*mem_32;
# 560 "kwset.c"
        tp += d;
        {
        __CrestLoad(33874, (unsigned long )(& d), (long long )d);
        __CrestLoad(33873, (unsigned long )0, (long long )0);
        __CrestApply2(33872, 12, (long long )(d == 0));
# 561 "kwset.c"
        if (d == 0) {
          __CrestBranch(33875, 7095, 1);
# 562 "kwset.c"
          goto found;
        } else {
          __CrestBranch(33876, 7096, 0);

        }
        }
# 563 "kwset.c"
        mem_33 = tp + -1;
# 563 "kwset.c"
        mem_34 = d1 + (unsigned char )*mem_33;
        __CrestLoad(33877, (unsigned long )mem_34, (long long )*mem_34);
        __CrestStore(33878, (unsigned long )(& d));
# 563 "kwset.c"
        d = (int )*mem_34;
# 563 "kwset.c"
        tp += d;
# 564 "kwset.c"
        mem_35 = tp + -1;
# 564 "kwset.c"
        mem_36 = d1 + (unsigned char )*mem_35;
        __CrestLoad(33879, (unsigned long )mem_36, (long long )*mem_36);
        __CrestStore(33880, (unsigned long )(& d));
# 564 "kwset.c"
        d = (int )*mem_36;
# 564 "kwset.c"
        tp += d;
      }
      while_break___0: ;
      }
# 566 "kwset.c"
      goto while_break;
      found:
      {
# 568 "kwset.c"
      mem_37 = tp + -2;
      {
      __CrestLoad(33883, (unsigned long )mem_37, (long long )*mem_37);
      __CrestLoad(33882, (unsigned long )(& gc), (long long )gc);
      __CrestApply2(33881, 12, (long long )((int )((unsigned char )*mem_37) == gc));
# 568 "kwset.c"
      if ((int )((unsigned char )*mem_37) == gc) {
        __CrestBranch(33884, 7103, 1);
        __CrestLoad(33886, (unsigned long )0, (long long )3);
        __CrestStore(33887, (unsigned long )(& i));
# 570 "kwset.c"
        i = 3;
        {
# 570 "kwset.c"
        while (1) {
          while_continue___1: ;
          {
          __CrestLoad(33890, (unsigned long )(& i), (long long )i);
          __CrestLoad(33889, (unsigned long )(& len), (long long )len);
          __CrestApply2(33888, 15, (long long )(i <= len));
# 570 "kwset.c"
          if (i <= len) {
            __CrestBranch(33891, 7108, 1);
            {
# 570 "kwset.c"
            mem_38 = tp + - i;
# 570 "kwset.c"
            mem_39 = sp + - i;
            {
            __CrestLoad(33895, (unsigned long )mem_38, (long long )*mem_38);
            __CrestLoad(33894, (unsigned long )mem_39, (long long )*mem_39);
            __CrestApply2(33893, 12, (long long )((int )((unsigned char )*mem_38) == (int )((unsigned char )*mem_39)));
# 570 "kwset.c"
            if ((int )((unsigned char )*mem_38) == (int )((unsigned char )*mem_39)) {
              __CrestBranch(33896, 7111, 1);

            } else {
              __CrestBranch(33897, 7112, 0);
# 570 "kwset.c"
              goto while_break___1;
            }
            }
            }
          } else {
            __CrestBranch(33892, 7113, 0);
# 570 "kwset.c"
            goto while_break___1;
          }
          }
          __CrestLoad(33900, (unsigned long )(& i), (long long )i);
          __CrestLoad(33899, (unsigned long )0, (long long )1);
          __CrestApply2(33898, 0, (long long )(i + 1));
          __CrestStore(33901, (unsigned long )(& i));
# 570 "kwset.c"
          i ++;
        }
        while_break___1: ;
        }
        {
        __CrestLoad(33904, (unsigned long )(& i), (long long )i);
        __CrestLoad(33903, (unsigned long )(& len), (long long )len);
        __CrestApply2(33902, 14, (long long )(i > len));
# 572 "kwset.c"
        if (i > len) {
          __CrestBranch(33905, 7117, 1);
# 573 "kwset.c"
          __retres47 = tp - len;
# 573 "kwset.c"
          goto return_label;
        } else {
          __CrestBranch(33906, 7119, 0);

        }
        }
      } else {
        __CrestBranch(33885, 7120, 0);

      }
      }
      }
# 575 "kwset.c"
      tp += md2;
    }
    while_break: ;
    }
  } else {
    __CrestBranch(33840, 7123, 0);

  }
  }
# 580 "kwset.c"
  ep = text + size;
# 581 "kwset.c"
  mem_40 = tp + -1;
# 581 "kwset.c"
  mem_41 = d1 + (unsigned char )*mem_40;
  __CrestLoad(33907, (unsigned long )mem_41, (long long )*mem_41);
  __CrestStore(33908, (unsigned long )(& d));
# 581 "kwset.c"
  d = (int )*mem_41;
  {
# 582 "kwset.c"
  while (1) {
    while_continue___2: ;
    {
    __CrestLoad(33913, (unsigned long )(& d), (long long )d);
    __CrestLoad(33912, (unsigned long )(& ep), (long long )((unsigned long )ep));
    __CrestLoad(33911, (unsigned long )(& tp), (long long )((unsigned long )tp));
    __CrestApply2(33910, 18, (long long )(ep - tp));
    __CrestApply2(33909, 15, (long long )((long )d <= ep - tp));
# 582 "kwset.c"
    if ((long )d <= ep - tp) {
      __CrestBranch(33914, 7129, 1);

    } else {
      __CrestBranch(33915, 7130, 0);
# 582 "kwset.c"
      goto while_break___2;
    }
    }
# 584 "kwset.c"
    tp += d;
# 584 "kwset.c"
    mem_42 = tp + -1;
# 584 "kwset.c"
    mem_43 = d1 + (unsigned char )*mem_42;
    __CrestLoad(33916, (unsigned long )mem_43, (long long )*mem_43);
    __CrestStore(33917, (unsigned long )(& d));
# 584 "kwset.c"
    d = (int )*mem_43;
    {
    __CrestLoad(33920, (unsigned long )(& d), (long long )d);
    __CrestLoad(33919, (unsigned long )0, (long long )0);
    __CrestApply2(33918, 13, (long long )(d != 0));
# 585 "kwset.c"
    if (d != 0) {
      __CrestBranch(33921, 7133, 1);
# 586 "kwset.c"
      goto while_continue___2;
    } else {
      __CrestBranch(33922, 7134, 0);

    }
    }
    {
# 587 "kwset.c"
    mem_44 = tp + -2;
    {
    __CrestLoad(33925, (unsigned long )mem_44, (long long )*mem_44);
    __CrestLoad(33924, (unsigned long )(& gc), (long long )gc);
    __CrestApply2(33923, 12, (long long )((int )((unsigned char )*mem_44) == gc));
# 587 "kwset.c"
    if ((int )((unsigned char )*mem_44) == gc) {
      __CrestBranch(33926, 7138, 1);
      __CrestLoad(33928, (unsigned long )0, (long long )3);
      __CrestStore(33929, (unsigned long )(& i));
# 589 "kwset.c"
      i = 3;
      {
# 589 "kwset.c"
      while (1) {
        while_continue___3: ;
        {
        __CrestLoad(33932, (unsigned long )(& i), (long long )i);
        __CrestLoad(33931, (unsigned long )(& len), (long long )len);
        __CrestApply2(33930, 15, (long long )(i <= len));
# 589 "kwset.c"
        if (i <= len) {
          __CrestBranch(33933, 7143, 1);
          {
# 589 "kwset.c"
          mem_45 = tp + - i;
# 589 "kwset.c"
          mem_46 = sp + - i;
          {
          __CrestLoad(33937, (unsigned long )mem_45, (long long )*mem_45);
          __CrestLoad(33936, (unsigned long )mem_46, (long long )*mem_46);
          __CrestApply2(33935, 12, (long long )((int )((unsigned char )*mem_45) == (int )((unsigned char )*mem_46)));
# 589 "kwset.c"
          if ((int )((unsigned char )*mem_45) == (int )((unsigned char )*mem_46)) {
            __CrestBranch(33938, 7146, 1);

          } else {
            __CrestBranch(33939, 7147, 0);
# 589 "kwset.c"
            goto while_break___3;
          }
          }
          }
        } else {
          __CrestBranch(33934, 7148, 0);
# 589 "kwset.c"
          goto while_break___3;
        }
        }
        __CrestLoad(33942, (unsigned long )(& i), (long long )i);
        __CrestLoad(33941, (unsigned long )0, (long long )1);
        __CrestApply2(33940, 0, (long long )(i + 1));
        __CrestStore(33943, (unsigned long )(& i));
# 589 "kwset.c"
        i ++;
      }
      while_break___3: ;
      }
      {
      __CrestLoad(33946, (unsigned long )(& i), (long long )i);
      __CrestLoad(33945, (unsigned long )(& len), (long long )len);
      __CrestApply2(33944, 14, (long long )(i > len));
# 591 "kwset.c"
      if (i > len) {
        __CrestBranch(33947, 7152, 1);
# 592 "kwset.c"
        __retres47 = tp - len;
# 592 "kwset.c"
        goto return_label;
      } else {
        __CrestBranch(33948, 7154, 0);

      }
      }
    } else {
      __CrestBranch(33927, 7155, 0);

    }
    }
    }
    __CrestLoad(33949, (unsigned long )(& md2), (long long )md2);
    __CrestStore(33950, (unsigned long )(& d));
# 594 "kwset.c"
    d = md2;
  }
  while_break___2: ;
  }
# 597 "kwset.c"
  __retres47 = (char *)0;
  return_label:
  {
  __CrestReturn(33951);
# 515 "kwset.c"
  return (__retres47);
  }
}
}
# 601 "kwset.c"
static char *cwexec(kwset_t kws , char *text , size_t len , struct kwsmatch *kwsmatch )
{
  struct kwset *kwset ;
  struct trie **next ;
  struct trie *trie ;
  struct trie *accept ;
  char *beg ;
  char *lim ;
  char *mch ;
  char *lmch ;
  unsigned char c ;
  register unsigned char *delta ;
  int d ;
  char *end ;
  char *qlim ;
  struct tree *tree ;
  char *trans ;
  unsigned char *mem_20 ;
  unsigned char *mem_21 ;
  unsigned char *mem_22 ;
  char *mem_23 ;
  unsigned char *mem_24 ;
  char *mem_25 ;
  unsigned char *mem_26 ;
  struct trie **mem_27 ;
  char *mem_28 ;
  char *mem_29 ;
  unsigned char *mem_30 ;
  struct trie **mem_31 ;
  char *mem_32 ;
  char *__retres33 ;

  {
  __CrestCall(33953, 280);
  __CrestStore(33952, (unsigned long )(& len));
# 622 "kwset.c"
  kwset = (struct kwset *)kws;
  {
  __CrestLoad(33956, (unsigned long )(& len), (long long )len);
  __CrestLoad(33955, (unsigned long )(& kwset->mind), (long long )kwset->mind);
  __CrestApply2(33954, 16, (long long )(len < (size_t )kwset->mind));
# 623 "kwset.c"
  if (len < (size_t )kwset->mind) {
    __CrestBranch(33957, 7162, 1);
# 624 "kwset.c"
    __retres33 = (char *)0;
# 624 "kwset.c"
    goto return_label;
  } else {
    __CrestBranch(33958, 7164, 0);

  }
  }
# 625 "kwset.c"
  next = kwset->next;
# 626 "kwset.c"
  delta = kwset->delta;
# 627 "kwset.c"
  trans = kwset->trans;
# 628 "kwset.c"
  lim = text + len;
# 629 "kwset.c"
  end = text;
  __CrestLoad(33959, (unsigned long )(& kwset->mind), (long long )kwset->mind);
  __CrestStore(33960, (unsigned long )(& d));
# 630 "kwset.c"
  d = kwset->mind;
  {
  __CrestLoad(33963, (unsigned long )(& d), (long long )d);
  __CrestLoad(33962, (unsigned long )0, (long long )0);
  __CrestApply2(33961, 13, (long long )(d != 0));
# 630 "kwset.c"
  if (d != 0) {
    __CrestBranch(33964, 7167, 1);
# 631 "kwset.c"
    mch = (char *)0;
  } else {
    __CrestBranch(33965, 7168, 0);
# 634 "kwset.c"
    mch = text;
# 634 "kwset.c"
    accept = kwset->trie;
# 635 "kwset.c"
    goto match;
  }
  }
  {
  __CrestLoad(33970, (unsigned long )(& len), (long long )len);
  __CrestLoad(33969, (unsigned long )0, (long long )4);
  __CrestLoad(33968, (unsigned long )(& kwset->mind), (long long )kwset->mind);
  __CrestApply2(33967, 2, (long long )(4 * kwset->mind));
  __CrestApply2(33966, 17, (long long )(len >= (size_t )(4 * kwset->mind)));
# 638 "kwset.c"
  if (len >= (size_t )(4 * kwset->mind)) {
    __CrestBranch(33971, 7171, 1);
# 639 "kwset.c"
    qlim = lim - 4 * kwset->mind;
  } else {
    __CrestBranch(33972, 7172, 0);
# 641 "kwset.c"
    qlim = (char *)0;
  }
  }
  {
# 643 "kwset.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(33977, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(33976, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestApply2(33975, 18, (long long )(lim - end));
    __CrestLoad(33974, (unsigned long )(& d), (long long )d);
    __CrestApply2(33973, 17, (long long )(lim - end >= (long )d));
# 643 "kwset.c"
    if (lim - end >= (long )d) {
      __CrestBranch(33978, 7177, 1);

    } else {
      __CrestBranch(33979, 7178, 0);
# 643 "kwset.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(33982, (unsigned long )(& qlim), (long long )((unsigned long )qlim));
    __CrestLoad(33981, (unsigned long )0, (long long )0);
    __CrestApply2(33980, 13, (long long )(qlim != 0));
# 645 "kwset.c"
    if (qlim != 0) {
      __CrestBranch(33983, 7180, 1);
      {
      __CrestLoad(33987, (unsigned long )(& end), (long long )((unsigned long )end));
      __CrestLoad(33986, (unsigned long )(& qlim), (long long )((unsigned long )qlim));
      __CrestApply2(33985, 15, (long long )((unsigned long )end <= (unsigned long )qlim));
# 645 "kwset.c"
      if ((unsigned long )end <= (unsigned long )qlim) {
        __CrestBranch(33988, 7181, 1);
# 647 "kwset.c"
        end += d - 1;
        {
# 648 "kwset.c"
        while (1) {
          while_continue___0: ;
          __CrestLoad(33990, (unsigned long )end, (long long )*end);
          __CrestStore(33991, (unsigned long )(& c));
# 648 "kwset.c"
          c = (unsigned char )*end;
# 648 "kwset.c"
          mem_20 = delta + c;
          __CrestLoad(33992, (unsigned long )mem_20, (long long )*mem_20);
          __CrestStore(33993, (unsigned long )(& d));
# 648 "kwset.c"
          d = (int )*mem_20;
          {
          __CrestLoad(33996, (unsigned long )(& d), (long long )d);
          __CrestLoad(33995, (unsigned long )0, (long long )0);
          __CrestApply2(33994, 13, (long long )(d != 0));
# 648 "kwset.c"
          if (d != 0) {
            __CrestBranch(33997, 7187, 1);
            {
            __CrestLoad(34001, (unsigned long )(& end), (long long )((unsigned long )end));
            __CrestLoad(34000, (unsigned long )(& qlim), (long long )((unsigned long )qlim));
            __CrestApply2(33999, 16, (long long )((unsigned long )end < (unsigned long )qlim));
# 648 "kwset.c"
            if ((unsigned long )end < (unsigned long )qlim) {
              __CrestBranch(34002, 7188, 1);

            } else {
              __CrestBranch(34003, 7189, 0);
# 648 "kwset.c"
              goto while_break___0;
            }
            }
          } else {
            __CrestBranch(33998, 7190, 0);
# 648 "kwset.c"
            goto while_break___0;
          }
          }
# 650 "kwset.c"
          end += d;
# 651 "kwset.c"
          mem_21 = delta + (unsigned char )*end;
# 651 "kwset.c"
          end += (int )*mem_21;
# 652 "kwset.c"
          mem_22 = delta + (unsigned char )*end;
# 652 "kwset.c"
          end += (int )*mem_22;
        }
        while_break___0: ;
        }
# 654 "kwset.c"
        end ++;
      } else {
        __CrestBranch(33989, 7194, 0);
# 657 "kwset.c"
        end += d;
# 657 "kwset.c"
        mem_23 = end + -1;
        __CrestLoad(34004, (unsigned long )mem_23, (long long )*mem_23);
        __CrestStore(34005, (unsigned long )(& c));
# 657 "kwset.c"
        c = (unsigned char )*mem_23;
# 657 "kwset.c"
        mem_24 = delta + c;
        __CrestLoad(34006, (unsigned long )mem_24, (long long )*mem_24);
        __CrestStore(34007, (unsigned long )(& d));
# 657 "kwset.c"
        d = (int )*mem_24;
      }
      }
    } else {
      __CrestBranch(33984, 7195, 0);
# 657 "kwset.c"
      end += d;
# 657 "kwset.c"
      mem_25 = end + -1;
      __CrestLoad(34008, (unsigned long )mem_25, (long long )*mem_25);
      __CrestStore(34009, (unsigned long )(& c));
# 657 "kwset.c"
      c = (unsigned char )*mem_25;
# 657 "kwset.c"
      mem_26 = delta + c;
      __CrestLoad(34010, (unsigned long )mem_26, (long long )*mem_26);
      __CrestStore(34011, (unsigned long )(& d));
# 657 "kwset.c"
      d = (int )*mem_26;
    }
    }
    {
    __CrestLoad(34014, (unsigned long )(& d), (long long )d);
    __CrestLoad(34013, (unsigned long )0, (long long )0);
    __CrestApply2(34012, 13, (long long )(d != 0));
# 658 "kwset.c"
    if (d != 0) {
      __CrestBranch(34015, 7197, 1);
# 659 "kwset.c"
      goto while_continue;
    } else {
      __CrestBranch(34016, 7198, 0);

    }
    }
# 660 "kwset.c"
    beg = end - 1;
# 661 "kwset.c"
    mem_27 = next + c;
# 661 "kwset.c"
    trie = *mem_27;
    {
    __CrestLoad(34019, (unsigned long )(& trie->accepting), (long long )trie->accepting);
    __CrestLoad(34018, (unsigned long )0, (long long )0);
    __CrestApply2(34017, 13, (long long )(trie->accepting != 0));
# 662 "kwset.c"
    if (trie->accepting != 0) {
      __CrestBranch(34020, 7201, 1);
# 664 "kwset.c"
      mch = beg;
# 665 "kwset.c"
      accept = trie;
    } else {
      __CrestBranch(34021, 7202, 0);

    }
    }
    __CrestLoad(34022, (unsigned long )(& trie->shift), (long long )trie->shift);
    __CrestStore(34023, (unsigned long )(& d));
# 667 "kwset.c"
    d = trie->shift;
    {
# 668 "kwset.c"
    while (1) {
      while_continue___1: ;
      {
      __CrestLoad(34026, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34025, (unsigned long )(& text), (long long )((unsigned long )text));
      __CrestApply2(34024, 14, (long long )((unsigned long )beg > (unsigned long )text));
# 668 "kwset.c"
      if ((unsigned long )beg > (unsigned long )text) {
        __CrestBranch(34027, 7208, 1);

      } else {
        __CrestBranch(34028, 7209, 0);
# 668 "kwset.c"
        goto while_break___1;
      }
      }
      {
      __CrestLoad(34031, (unsigned long )(& trans), (long long )((unsigned long )trans));
      __CrestLoad(34030, (unsigned long )0, (long long )0);
      __CrestApply2(34029, 13, (long long )(trans != 0));
# 670 "kwset.c"
      if (trans != 0) {
        __CrestBranch(34032, 7211, 1);
# 670 "kwset.c"
        beg --;
# 670 "kwset.c"
        mem_28 = trans + (unsigned char )*beg;
        __CrestLoad(34034, (unsigned long )mem_28, (long long )*mem_28);
        __CrestStore(34035, (unsigned long )(& c));
# 670 "kwset.c"
        c = (unsigned char )*mem_28;
      } else {
        __CrestBranch(34033, 7212, 0);
# 670 "kwset.c"
        beg --;
        __CrestLoad(34036, (unsigned long )beg, (long long )*beg);
        __CrestStore(34037, (unsigned long )(& c));
# 670 "kwset.c"
        c = (unsigned char )*beg;
      }
      }
# 671 "kwset.c"
      tree = trie->links;
      {
# 672 "kwset.c"
      while (1) {
        while_continue___2: ;
        {
        __CrestLoad(34040, (unsigned long )(& tree), (long long )((unsigned long )tree));
        __CrestLoad(34039, (unsigned long )0, (long long )0);
        __CrestApply2(34038, 13, (long long )(tree != 0));
# 672 "kwset.c"
        if (tree != 0) {
          __CrestBranch(34041, 7218, 1);
          {
          __CrestLoad(34045, (unsigned long )(& c), (long long )c);
          __CrestLoad(34044, (unsigned long )(& tree->label), (long long )tree->label);
          __CrestApply2(34043, 13, (long long )((int )c != (int )tree->label));
# 672 "kwset.c"
          if ((int )c != (int )tree->label) {
            __CrestBranch(34046, 7219, 1);

          } else {
            __CrestBranch(34047, 7220, 0);
# 672 "kwset.c"
            goto while_break___2;
          }
          }
        } else {
          __CrestBranch(34042, 7221, 0);
# 672 "kwset.c"
          goto while_break___2;
        }
        }
        {
        __CrestLoad(34050, (unsigned long )(& c), (long long )c);
        __CrestLoad(34049, (unsigned long )(& tree->label), (long long )tree->label);
        __CrestApply2(34048, 16, (long long )((int )c < (int )tree->label));
# 673 "kwset.c"
        if ((int )c < (int )tree->label) {
          __CrestBranch(34051, 7223, 1);
# 674 "kwset.c"
          tree = tree->llink;
        } else {
          __CrestBranch(34052, 7224, 0);
# 676 "kwset.c"
          tree = tree->rlink;
        }
        }
      }
      while_break___2: ;
      }
      {
      __CrestLoad(34055, (unsigned long )(& tree), (long long )((unsigned long )tree));
      __CrestLoad(34054, (unsigned long )0, (long long )0);
      __CrestApply2(34053, 13, (long long )(tree != 0));
# 677 "kwset.c"
      if (tree != 0) {
        __CrestBranch(34056, 7227, 1);
# 679 "kwset.c"
        trie = tree->trie;
        {
        __CrestLoad(34060, (unsigned long )(& trie->accepting), (long long )trie->accepting);
        __CrestLoad(34059, (unsigned long )0, (long long )0);
        __CrestApply2(34058, 13, (long long )(trie->accepting != 0));
# 680 "kwset.c"
        if (trie->accepting != 0) {
          __CrestBranch(34061, 7229, 1);
# 682 "kwset.c"
          mch = beg;
# 683 "kwset.c"
          accept = trie;
        } else {
          __CrestBranch(34062, 7230, 0);

        }
        }
      } else {
        __CrestBranch(34057, 7231, 0);
# 687 "kwset.c"
        goto while_break___1;
      }
      }
      __CrestLoad(34063, (unsigned long )(& trie->shift), (long long )trie->shift);
      __CrestStore(34064, (unsigned long )(& d));
# 688 "kwset.c"
      d = trie->shift;
    }
    while_break___1: ;
    }
    {
    __CrestLoad(34067, (unsigned long )(& mch), (long long )((unsigned long )mch));
    __CrestLoad(34066, (unsigned long )0, (long long )0);
    __CrestApply2(34065, 13, (long long )(mch != 0));
# 690 "kwset.c"
    if (mch != 0) {
      __CrestBranch(34068, 7235, 1);
# 691 "kwset.c"
      goto match;
    } else {
      __CrestBranch(34069, 7236, 0);

    }
    }
  }
  while_break: ;
  }
# 693 "kwset.c"
  __retres33 = (char *)0;
# 693 "kwset.c"
  goto return_label;
  match:
  {
  __CrestLoad(34074, (unsigned long )(& lim), (long long )((unsigned long )lim));
  __CrestLoad(34073, (unsigned long )(& mch), (long long )((unsigned long )mch));
  __CrestApply2(34072, 18, (long long )(lim - mch));
  __CrestLoad(34071, (unsigned long )(& kwset->maxd), (long long )kwset->maxd);
  __CrestApply2(34070, 14, (long long )(lim - mch > (long )kwset->maxd));
# 699 "kwset.c"
  if (lim - mch > (long )kwset->maxd) {
    __CrestBranch(34075, 7241, 1);
# 700 "kwset.c"
    lim = mch + kwset->maxd;
  } else {
    __CrestBranch(34076, 7242, 0);

  }
  }
# 701 "kwset.c"
  lmch = (char *)0;
  __CrestLoad(34077, (unsigned long )0, (long long )1);
  __CrestStore(34078, (unsigned long )(& d));
# 702 "kwset.c"
  d = 1;
  {
# 703 "kwset.c"
  while (1) {
    while_continue___3: ;
    {
    __CrestLoad(34083, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(34082, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestApply2(34081, 18, (long long )(lim - end));
    __CrestLoad(34080, (unsigned long )(& d), (long long )d);
    __CrestApply2(34079, 17, (long long )(lim - end >= (long )d));
# 703 "kwset.c"
    if (lim - end >= (long )d) {
      __CrestBranch(34084, 7248, 1);

    } else {
      __CrestBranch(34085, 7249, 0);
# 703 "kwset.c"
      goto while_break___3;
    }
    }
# 705 "kwset.c"
    end += d;
# 705 "kwset.c"
    mem_29 = end + -1;
    __CrestLoad(34086, (unsigned long )mem_29, (long long )*mem_29);
    __CrestStore(34087, (unsigned long )(& c));
# 705 "kwset.c"
    c = (unsigned char )*mem_29;
# 705 "kwset.c"
    mem_30 = delta + c;
    __CrestLoad(34088, (unsigned long )mem_30, (long long )*mem_30);
    __CrestStore(34089, (unsigned long )(& d));
# 705 "kwset.c"
    d = (int )*mem_30;
    {
    __CrestLoad(34092, (unsigned long )(& d), (long long )d);
    __CrestLoad(34091, (unsigned long )0, (long long )0);
    __CrestApply2(34090, 13, (long long )(d != 0));
# 705 "kwset.c"
    if (d != 0) {
      __CrestBranch(34093, 7252, 1);
# 706 "kwset.c"
      goto while_continue___3;
    } else {
      __CrestBranch(34094, 7253, 0);

    }
    }
# 707 "kwset.c"
    beg = end - 1;
# 708 "kwset.c"
    mem_31 = next + c;
# 708 "kwset.c"
    trie = *mem_31;
    {
    __CrestLoad(34097, (unsigned long )(& trie), (long long )((unsigned long )trie));
    __CrestLoad(34096, (unsigned long )0, (long long )0);
    __CrestApply2(34095, 13, (long long )(trie != 0));
# 708 "kwset.c"
    if (trie != 0) {
      __CrestBranch(34098, 7256, 1);

    } else {
      __CrestBranch(34099, 7257, 0);
      __CrestLoad(34100, (unsigned long )0, (long long )1);
      __CrestStore(34101, (unsigned long )(& d));
# 710 "kwset.c"
      d = 1;
# 711 "kwset.c"
      goto while_continue___3;
    }
    }
    {
    __CrestLoad(34104, (unsigned long )(& trie->accepting), (long long )trie->accepting);
    __CrestLoad(34103, (unsigned long )0, (long long )0);
    __CrestApply2(34102, 13, (long long )(trie->accepting != 0));
# 713 "kwset.c"
    if (trie->accepting != 0) {
      __CrestBranch(34105, 7260, 1);
      {
      __CrestLoad(34109, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34108, (unsigned long )(& mch), (long long )((unsigned long )mch));
      __CrestApply2(34107, 15, (long long )((unsigned long )beg <= (unsigned long )mch));
# 713 "kwset.c"
      if ((unsigned long )beg <= (unsigned long )mch) {
        __CrestBranch(34110, 7261, 1);
# 715 "kwset.c"
        lmch = beg;
# 716 "kwset.c"
        accept = trie;
      } else {
        __CrestBranch(34111, 7262, 0);

      }
      }
    } else {
      __CrestBranch(34106, 7263, 0);

    }
    }
    __CrestLoad(34112, (unsigned long )(& trie->shift), (long long )trie->shift);
    __CrestStore(34113, (unsigned long )(& d));
# 718 "kwset.c"
    d = trie->shift;
    {
# 719 "kwset.c"
    while (1) {
      while_continue___4: ;
      {
      __CrestLoad(34116, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34115, (unsigned long )(& text), (long long )((unsigned long )text));
      __CrestApply2(34114, 14, (long long )((unsigned long )beg > (unsigned long )text));
# 719 "kwset.c"
      if ((unsigned long )beg > (unsigned long )text) {
        __CrestBranch(34117, 7269, 1);

      } else {
        __CrestBranch(34118, 7270, 0);
# 719 "kwset.c"
        goto while_break___4;
      }
      }
      {
      __CrestLoad(34121, (unsigned long )(& trans), (long long )((unsigned long )trans));
      __CrestLoad(34120, (unsigned long )0, (long long )0);
      __CrestApply2(34119, 13, (long long )(trans != 0));
# 721 "kwset.c"
      if (trans != 0) {
        __CrestBranch(34122, 7272, 1);
# 721 "kwset.c"
        beg --;
# 721 "kwset.c"
        mem_32 = trans + (unsigned char )*beg;
        __CrestLoad(34124, (unsigned long )mem_32, (long long )*mem_32);
        __CrestStore(34125, (unsigned long )(& c));
# 721 "kwset.c"
        c = (unsigned char )*mem_32;
      } else {
        __CrestBranch(34123, 7273, 0);
# 721 "kwset.c"
        beg --;
        __CrestLoad(34126, (unsigned long )beg, (long long )*beg);
        __CrestStore(34127, (unsigned long )(& c));
# 721 "kwset.c"
        c = (unsigned char )*beg;
      }
      }
# 722 "kwset.c"
      tree = trie->links;
      {
# 723 "kwset.c"
      while (1) {
        while_continue___5: ;
        {
        __CrestLoad(34130, (unsigned long )(& tree), (long long )((unsigned long )tree));
        __CrestLoad(34129, (unsigned long )0, (long long )0);
        __CrestApply2(34128, 13, (long long )(tree != 0));
# 723 "kwset.c"
        if (tree != 0) {
          __CrestBranch(34131, 7279, 1);
          {
          __CrestLoad(34135, (unsigned long )(& c), (long long )c);
          __CrestLoad(34134, (unsigned long )(& tree->label), (long long )tree->label);
          __CrestApply2(34133, 13, (long long )((int )c != (int )tree->label));
# 723 "kwset.c"
          if ((int )c != (int )tree->label) {
            __CrestBranch(34136, 7280, 1);

          } else {
            __CrestBranch(34137, 7281, 0);
# 723 "kwset.c"
            goto while_break___5;
          }
          }
        } else {
          __CrestBranch(34132, 7282, 0);
# 723 "kwset.c"
          goto while_break___5;
        }
        }
        {
        __CrestLoad(34140, (unsigned long )(& c), (long long )c);
        __CrestLoad(34139, (unsigned long )(& tree->label), (long long )tree->label);
        __CrestApply2(34138, 16, (long long )((int )c < (int )tree->label));
# 724 "kwset.c"
        if ((int )c < (int )tree->label) {
          __CrestBranch(34141, 7284, 1);
# 725 "kwset.c"
          tree = tree->llink;
        } else {
          __CrestBranch(34142, 7285, 0);
# 727 "kwset.c"
          tree = tree->rlink;
        }
        }
      }
      while_break___5: ;
      }
      {
      __CrestLoad(34145, (unsigned long )(& tree), (long long )((unsigned long )tree));
      __CrestLoad(34144, (unsigned long )0, (long long )0);
      __CrestApply2(34143, 13, (long long )(tree != 0));
# 728 "kwset.c"
      if (tree != 0) {
        __CrestBranch(34146, 7288, 1);
# 730 "kwset.c"
        trie = tree->trie;
        {
        __CrestLoad(34150, (unsigned long )(& trie->accepting), (long long )trie->accepting);
        __CrestLoad(34149, (unsigned long )0, (long long )0);
        __CrestApply2(34148, 13, (long long )(trie->accepting != 0));
# 731 "kwset.c"
        if (trie->accepting != 0) {
          __CrestBranch(34151, 7290, 1);
          {
          __CrestLoad(34155, (unsigned long )(& beg), (long long )((unsigned long )beg));
          __CrestLoad(34154, (unsigned long )(& mch), (long long )((unsigned long )mch));
          __CrestApply2(34153, 15, (long long )((unsigned long )beg <= (unsigned long )mch));
# 731 "kwset.c"
          if ((unsigned long )beg <= (unsigned long )mch) {
            __CrestBranch(34156, 7291, 1);
# 733 "kwset.c"
            lmch = beg;
# 734 "kwset.c"
            accept = trie;
          } else {
            __CrestBranch(34157, 7292, 0);

          }
          }
        } else {
          __CrestBranch(34152, 7293, 0);

        }
        }
      } else {
        __CrestBranch(34147, 7294, 0);
# 738 "kwset.c"
        goto while_break___4;
      }
      }
      __CrestLoad(34158, (unsigned long )(& trie->shift), (long long )trie->shift);
      __CrestStore(34159, (unsigned long )(& d));
# 739 "kwset.c"
      d = trie->shift;
    }
    while_break___4: ;
    }
    {
    __CrestLoad(34162, (unsigned long )(& lmch), (long long )((unsigned long )lmch));
    __CrestLoad(34161, (unsigned long )0, (long long )0);
    __CrestApply2(34160, 13, (long long )(lmch != 0));
# 741 "kwset.c"
    if (lmch != 0) {
      __CrestBranch(34163, 7298, 1);
# 743 "kwset.c"
      mch = lmch;
# 744 "kwset.c"
      goto match;
    } else {
      __CrestBranch(34164, 7300, 0);

    }
    }
    {
    __CrestLoad(34167, (unsigned long )(& d), (long long )d);
    __CrestLoad(34166, (unsigned long )0, (long long )0);
    __CrestApply2(34165, 12, (long long )(d == 0));
# 746 "kwset.c"
    if (d == 0) {
      __CrestBranch(34168, 7302, 1);
      __CrestLoad(34170, (unsigned long )0, (long long )1);
      __CrestStore(34171, (unsigned long )(& d));
# 747 "kwset.c"
      d = 1;
    } else {
      __CrestBranch(34169, 7303, 0);

    }
    }
  }
  while_break___3: ;
  }
  {
  __CrestLoad(34174, (unsigned long )(& kwsmatch), (long long )((unsigned long )kwsmatch));
  __CrestLoad(34173, (unsigned long )0, (long long )0);
  __CrestApply2(34172, 13, (long long )(kwsmatch != 0));
# 750 "kwset.c"
  if (kwsmatch != 0) {
    __CrestBranch(34175, 7306, 1);
    __CrestLoad(34179, (unsigned long )(& accept->accepting), (long long )accept->accepting);
    __CrestLoad(34178, (unsigned long )0, (long long )2U);
    __CrestApply2(34177, 3, (long long )(accept->accepting / 2U));
    __CrestStore(34180, (unsigned long )(& kwsmatch->index));
# 752 "kwset.c"
    kwsmatch->index = (int )(accept->accepting / 2U);
# 753 "kwset.c"
    kwsmatch->beg[0] = mch;
    __CrestLoad(34181, (unsigned long )(& accept->depth), (long long )accept->depth);
    __CrestStore(34182, (unsigned long )(& kwsmatch->size[0]));
# 754 "kwset.c"
    kwsmatch->size[0] = (size_t )accept->depth;
  } else {
    __CrestBranch(34176, 7307, 0);

  }
  }
# 756 "kwset.c"
  __retres33 = mch;
  return_label:
  {
  __CrestReturn(34183);
# 601 "kwset.c"
  return (__retres33);
  }
}
}
# 766 "kwset.c"
char *kwsexec(kwset_t kws , char *text , size_t size , struct kwsmatch *kwsmatch )
{
  struct kwset *kwset ;
  char *ret ;
  char *tmp ;
  char *__retres8 ;

  {
  __CrestCall(34185, 281);
  __CrestStore(34184, (unsigned long )(& size));
# 776 "kwset.c"
  kwset = (struct kwset *)kws;
  {
  __CrestLoad(34188, (unsigned long )(& kwset->words), (long long )kwset->words);
  __CrestLoad(34187, (unsigned long )0, (long long )1);
  __CrestApply2(34186, 12, (long long )(kwset->words == 1));
# 777 "kwset.c"
  if (kwset->words == 1) {
    __CrestBranch(34189, 7312, 1);
    {
    __CrestLoad(34193, (unsigned long )(& kwset->trans), (long long )((unsigned long )kwset->trans));
    __CrestLoad(34192, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(34191, 12, (long long )((unsigned long )kwset->trans == (unsigned long )((char *)0)));
# 777 "kwset.c"
    if ((unsigned long )kwset->trans == (unsigned long )((char *)0)) {
      __CrestBranch(34194, 7313, 1);
      __CrestLoad(34196, (unsigned long )(& size), (long long )size);
# 779 "kwset.c"
      ret = bmexec(kws, text, size);
      __CrestClearStack(34197);
      {
      __CrestLoad(34200, (unsigned long )(& kwsmatch), (long long )((unsigned long )kwsmatch));
      __CrestLoad(34199, (unsigned long )0, (long long )((unsigned long )((struct kwsmatch *)0)));
      __CrestApply2(34198, 13, (long long )((unsigned long )kwsmatch != (unsigned long )((struct kwsmatch *)0)));
# 780 "kwset.c"
      if ((unsigned long )kwsmatch != (unsigned long )((struct kwsmatch *)0)) {
        __CrestBranch(34201, 7315, 1);
        {
        __CrestLoad(34205, (unsigned long )(& ret), (long long )((unsigned long )ret));
        __CrestLoad(34204, (unsigned long )0, (long long )((unsigned long )((char *)0)));
        __CrestApply2(34203, 13, (long long )((unsigned long )ret != (unsigned long )((char *)0)));
# 780 "kwset.c"
        if ((unsigned long )ret != (unsigned long )((char *)0)) {
          __CrestBranch(34206, 7316, 1);
          __CrestLoad(34208, (unsigned long )0, (long long )0);
          __CrestStore(34209, (unsigned long )(& kwsmatch->index));
# 782 "kwset.c"
          kwsmatch->index = 0;
# 783 "kwset.c"
          kwsmatch->beg[0] = ret;
          __CrestLoad(34210, (unsigned long )(& kwset->mind), (long long )kwset->mind);
          __CrestStore(34211, (unsigned long )(& kwsmatch->size[0]));
# 784 "kwset.c"
          kwsmatch->size[0] = (size_t )kwset->mind;
        } else {
          __CrestBranch(34207, 7317, 0);

        }
        }
      } else {
        __CrestBranch(34202, 7318, 0);

      }
      }
# 786 "kwset.c"
      __retres8 = ret;
# 786 "kwset.c"
      goto return_label;
    } else {
      __CrestBranch(34195, 7321, 0);
      __CrestLoad(34212, (unsigned long )(& size), (long long )size);
# 789 "kwset.c"
      tmp = cwexec(kws, text, size, kwsmatch);
      __CrestClearStack(34213);
# 789 "kwset.c"
      __retres8 = tmp;
# 789 "kwset.c"
      goto return_label;
    }
    }
  } else {
    __CrestBranch(34190, 7324, 0);
    __CrestLoad(34214, (unsigned long )(& size), (long long )size);
# 789 "kwset.c"
    tmp = cwexec(kws, text, size, kwsmatch);
    __CrestClearStack(34215);
# 789 "kwset.c"
    __retres8 = tmp;
# 789 "kwset.c"
    goto return_label;
  }
  }
  return_label:
  {
  __CrestReturn(34216);
# 766 "kwset.c"
  return (__retres8);
  }
}
}
# 793 "kwset.c"
void kwsfree(kwset_t kws )
{
  struct kwset *kwset ;
  struct obstack *__o ;
  void *__obj ;
  char *tmp ;

  {
  __CrestCall(34217, 282);
# 799 "kwset.c"
  kwset = (struct kwset *)kws;
# 800 "kwset.c"
  __o = & kwset->obstack;
# 800 "kwset.c"
  __obj = (void *)0;
  {
  __CrestLoad(34220, (unsigned long )(& __obj), (long long )((unsigned long )__obj));
  __CrestLoad(34219, (unsigned long )(& __o->chunk), (long long )((unsigned long )__o->chunk));
  __CrestApply2(34218, 14, (long long )((unsigned long )__obj > (unsigned long )((void *)__o->chunk)));
# 800 "kwset.c"
  if ((unsigned long )__obj > (unsigned long )((void *)__o->chunk)) {
    __CrestBranch(34221, 7330, 1);
    {
    __CrestLoad(34225, (unsigned long )(& __obj), (long long )((unsigned long )__obj));
    __CrestLoad(34224, (unsigned long )(& __o->chunk_limit), (long long )((unsigned long )__o->chunk_limit));
    __CrestApply2(34223, 16, (long long )((unsigned long )__obj < (unsigned long )((void *)__o->chunk_limit)));
# 800 "kwset.c"
    if ((unsigned long )__obj < (unsigned long )((void *)__o->chunk_limit)) {
      __CrestBranch(34226, 7331, 1);
# 800 "kwset.c"
      tmp = (char *)__obj;
# 800 "kwset.c"
      __o->object_base = tmp;
# 800 "kwset.c"
      __o->next_free = tmp;
    } else {
      __CrestBranch(34227, 7332, 0);
# 800 "kwset.c"
      obstack_free(__o, __obj);
      __CrestClearStack(34228);
    }
    }
  } else {
    __CrestBranch(34222, 7333, 0);
# 800 "kwset.c"
    obstack_free(__o, __obj);
    __CrestClearStack(34229);
  }
  }
# 801 "kwset.c"
  free(kws);
  __CrestClearStack(34230);

  {
  __CrestReturn(34231);
# 793 "kwset.c"
  return;
  }
}
}
void __globinit_kwset(void)
{


  {
  __CrestInit();
}
}
