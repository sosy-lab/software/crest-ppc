# 1 "./regex.cil.c"
# 1 "/home/yavuz/crest/benchmarks/grep-2.2/src//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./regex.cil.c"



extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 353 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned int wint_t;
# 52 "/usr/include/wctype.h"
typedef unsigned long wctype_t;
# 45 "regex.h"
typedef long s_reg_t;
# 46 "regex.h"
typedef unsigned long active_reg_t;
# 53 "regex.h"
typedef unsigned long reg_syntax_t;
# 284 "regex.h"
enum __anonenum_reg_errcode_t_24 {
    REG_NOERROR = 0,
    REG_NOMATCH = 1,
    REG_BADPAT = 2,
    REG_ECOLLATE = 3,
    REG_ECTYPE = 4,
    REG_EESCAPE = 5,
    REG_ESUBREG = 6,
    REG_EBRACK = 7,
    REG_EPAREN = 8,
    REG_EBRACE = 9,
    REG_BADBR = 10,
    REG_ERANGE = 11,
    REG_ESPACE = 12,
    REG_BADRPT = 13,
    REG_EEND = 14,
    REG_ESIZE = 15,
    REG_ERPAREN = 16
} ;
# 284 "regex.h"
typedef enum __anonenum_reg_errcode_t_24 reg_errcode_t;
# 320 "regex.h"
struct re_pattern_buffer {
   unsigned char *buffer ;
   unsigned long allocated ;
   unsigned long used ;
   reg_syntax_t syntax ;
   char *fastmap ;
   char *translate ;
   size_t re_nsub ;
   unsigned int can_be_null : 1 ;
   unsigned int regs_allocated : 2 ;
   unsigned int fastmap_accurate : 1 ;
   unsigned int no_sub : 1 ;
   unsigned int not_bol : 1 ;
   unsigned int not_eol : 1 ;
   unsigned int newline_anchor : 1 ;
};
# 388 "regex.h"
typedef struct re_pattern_buffer regex_t;
# 391 "regex.h"
typedef int regoff_t;
# 396 "regex.h"
struct re_registers {
   unsigned int num_regs ;
   regoff_t *start ;
   regoff_t *end ;
};
# 415 "regex.h"
struct __anonstruct_regmatch_t_25 {
   regoff_t rm_so ;
   regoff_t rm_eo ;
};
# 415 "regex.h"
typedef struct __anonstruct_regmatch_t_25 regmatch_t;
# 343 "regex.c"
typedef char boolean;
# 354 "regex.c"
enum __anonenum_re_opcode_t_27 {
    no_op = 0,
    succeed = 1,
    exactn = 2,
    anychar = 3,
    charset = 4,
    charset_not = 5,
    start_memory = 6,
    stop_memory = 7,
    duplicate = 8,
    begline = 9,
    endline = 10,
    begbuf = 11,
    endbuf = 12,
    jump = 13,
    jump_past_alt = 14,
    on_failure_jump = 15,
    on_failure_keep_string_jump = 16,
    pop_failure_jump = 17,
    maybe_pop_jump = 18,
    dummy_failure_jump = 19,
    push_dummy_failure = 20,
    succeed_n = 21,
    jump_n = 22,
    set_number_at = 23,
    wordchar = 24,
    notwordchar = 25,
    wordbeg = 26,
    wordend = 27,
    wordbound = 28,
    notwordbound = 29
} ;
# 354 "regex.c"
typedef enum __anonenum_re_opcode_t_27 re_opcode_t;
# 1085 "regex.c"
union fail_stack_elt {
   unsigned char *pointer ;
   int integer ;
};
# 1091 "regex.c"
typedef union fail_stack_elt fail_stack_elt_t;
# 1093 "regex.c"
struct __anonstruct_fail_stack_type_28 {
   fail_stack_elt_t *stack ;
   unsigned int size ;
   unsigned int avail ;
};
# 1093 "regex.c"
typedef struct __anonstruct_fail_stack_type_28 fail_stack_type;
# 1411 "regex.c"
struct __anonstruct_bits_30 {
   unsigned int match_null_string_p : 2 ;
   unsigned int is_active : 1 ;
   unsigned int matched_something : 1 ;
   unsigned int ever_matched_something : 1 ;
};
# 1411 "regex.c"
union __anonunion_register_info_type_29 {
   fail_stack_elt_t word ;
   struct __anonstruct_bits_30 bits ;
};
# 1411 "regex.c"
typedef union __anonunion_register_info_type_29 register_info_type;
# 1623 "regex.c"
typedef unsigned int regnum_t;
# 1631 "regex.c"
typedef long pattern_offset_t;
# 1633 "regex.c"
struct __anonstruct_compile_stack_elt_t_31 {
   pattern_offset_t begalt_offset ;
   pattern_offset_t fixup_alt_jump ;
   pattern_offset_t inner_group_offset ;
   pattern_offset_t laststart_offset ;
   regnum_t regnum ;
};
# 1633 "regex.c"
typedef struct __anonstruct_compile_stack_elt_t_31 compile_stack_elt_t;
# 1643 "regex.c"
struct __anonstruct_compile_stack_type_32 {
   compile_stack_elt_t *stack ;
   unsigned int size ;
   unsigned int avail ;
};
# 1643 "regex.c"
typedef struct __anonstruct_compile_stack_type_32 compile_stack_type;
# 353 "/usr/include/wchar.h"
extern __attribute__((__nothrow__)) wint_t ( __attribute__((__leaf__)) btowc)(int __c ) ;
# 171 "/usr/include/wctype.h"
extern __attribute__((__nothrow__)) wctype_t ( __attribute__((__leaf__)) wctype)(char const *__property ) ;
# 175 "/usr/include/wctype.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) iswctype)(wint_t __wc ,
                                                                               wctype_t __desc ) ;
# 105 "../intl/libintl.h"
extern char *dcgettext(char const *__domainname , char const *__msgid , int __category ) ;
# 466 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__leaf__)) malloc)(size_t __size ) __attribute__((__malloc__)) ;
# 480 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__warn_unused_result__,
__leaf__)) realloc)(void *__ptr , size_t __size ) ;
# 483 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void ( __attribute__((__leaf__)) free)(void *__ptr ) ;
# 515 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__, __noreturn__)) void ( __attribute__((__leaf__)) abort)(void) ;
# 46 "/usr/include/string.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__nonnull__(1,2), __leaf__)) memcpy)(void * __restrict __dest ,
                                                                                                 void const * __restrict __src ,
                                                                                                 size_t __n ) ;
# 66 "/usr/include/string.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__nonnull__(1), __leaf__)) memset)(void *__s ,
                                                                                               int __c ,
                                                                                               size_t __n ) ;
# 69 "/usr/include/string.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1,2), __leaf__)) memcmp)(void const *__s1 ,
                                                                                               void const *__s2 ,
                                                                                               size_t __n ) __attribute__((__pure__)) ;
# 129 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1,2), __leaf__)) strcpy)(char * __restrict __dest ,
                                                                                                 char const * __restrict __src ) ;
# 132 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1,2), __leaf__)) strncpy)(char * __restrict __dest ,
                                                                                                  char const * __restrict __src ,
                                                                                                  size_t __n ) ;
# 144 "/usr/include/string.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1,2), __leaf__)) strcmp)(char const *__s1 ,
                                                                                               char const *__s2 ) __attribute__((__pure__)) ;
# 399 "/usr/include/string.h"
extern __attribute__((__nothrow__)) size_t ( __attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s ) __attribute__((__pure__)) ;
# 142 "regex.c"
static char re_syntax_table[256] ;
# 148 "regex.c"
static void init_syntax_once(void) ;
# 148 "regex.c"
static int done = 0;
# 144 "regex.c"
static void init_syntax_once(void)
{
  int c ;

  {
  __CrestCall(35005, 291);

  {
  __CrestLoad(35008, (unsigned long )(& done), (long long )done);
  __CrestLoad(35007, (unsigned long )0, (long long )0);
  __CrestApply2(35006, 13, (long long )(done != 0));
# 150 "regex.c"
  if (done != 0) {
    __CrestBranch(35009, 7734, 1);
# 151 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(35010, 7736, 0);

  }
  }
  __CrestLoad(35011, (unsigned long )0, (long long )0);
  __CrestLoad(35012, (unsigned long )0, (long long )sizeof(re_syntax_table));
# 153 "regex.c"
  memset((void *)(re_syntax_table), 0, sizeof(re_syntax_table));
  __CrestClearStack(35013);
  __CrestLoad(35014, (unsigned long )0, (long long )'a');
  __CrestStore(35015, (unsigned long )(& c));
# 155 "regex.c"
  c = 'a';
  {
# 155 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(35018, (unsigned long )(& c), (long long )c);
    __CrestLoad(35017, (unsigned long )0, (long long )122);
    __CrestApply2(35016, 15, (long long )(c <= 122));
# 155 "regex.c"
    if (c <= 122) {
      __CrestBranch(35019, 7742, 1);

    } else {
      __CrestBranch(35020, 7743, 0);
# 155 "regex.c"
      goto while_break;
    }
    }
    __CrestLoad(35021, (unsigned long )0, (long long )(char)1);
    __CrestStore(35022, (unsigned long )(& re_syntax_table[c]));
# 156 "regex.c"
    re_syntax_table[c] = (char)1;
    __CrestLoad(35025, (unsigned long )(& c), (long long )c);
    __CrestLoad(35024, (unsigned long )0, (long long )1);
    __CrestApply2(35023, 0, (long long )(c + 1));
    __CrestStore(35026, (unsigned long )(& c));
# 155 "regex.c"
    c ++;
  }
  while_break: ;
  }
  __CrestLoad(35027, (unsigned long )0, (long long )'A');
  __CrestStore(35028, (unsigned long )(& c));
# 158 "regex.c"
  c = 'A';
  {
# 158 "regex.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(35031, (unsigned long )(& c), (long long )c);
    __CrestLoad(35030, (unsigned long )0, (long long )90);
    __CrestApply2(35029, 15, (long long )(c <= 90));
# 158 "regex.c"
    if (c <= 90) {
      __CrestBranch(35032, 7751, 1);

    } else {
      __CrestBranch(35033, 7752, 0);
# 158 "regex.c"
      goto while_break___0;
    }
    }
    __CrestLoad(35034, (unsigned long )0, (long long )(char)1);
    __CrestStore(35035, (unsigned long )(& re_syntax_table[c]));
# 159 "regex.c"
    re_syntax_table[c] = (char)1;
    __CrestLoad(35038, (unsigned long )(& c), (long long )c);
    __CrestLoad(35037, (unsigned long )0, (long long )1);
    __CrestApply2(35036, 0, (long long )(c + 1));
    __CrestStore(35039, (unsigned long )(& c));
# 158 "regex.c"
    c ++;
  }
  while_break___0: ;
  }
  __CrestLoad(35040, (unsigned long )0, (long long )'0');
  __CrestStore(35041, (unsigned long )(& c));
# 161 "regex.c"
  c = '0';
  {
# 161 "regex.c"
  while (1) {
    while_continue___1: ;
    {
    __CrestLoad(35044, (unsigned long )(& c), (long long )c);
    __CrestLoad(35043, (unsigned long )0, (long long )57);
    __CrestApply2(35042, 15, (long long )(c <= 57));
# 161 "regex.c"
    if (c <= 57) {
      __CrestBranch(35045, 7760, 1);

    } else {
      __CrestBranch(35046, 7761, 0);
# 161 "regex.c"
      goto while_break___1;
    }
    }
    __CrestLoad(35047, (unsigned long )0, (long long )(char)1);
    __CrestStore(35048, (unsigned long )(& re_syntax_table[c]));
# 162 "regex.c"
    re_syntax_table[c] = (char)1;
    __CrestLoad(35051, (unsigned long )(& c), (long long )c);
    __CrestLoad(35050, (unsigned long )0, (long long )1);
    __CrestApply2(35049, 0, (long long )(c + 1));
    __CrestStore(35052, (unsigned long )(& c));
# 161 "regex.c"
    c ++;
  }
  while_break___1: ;
  }
  __CrestLoad(35053, (unsigned long )0, (long long )(char)1);
  __CrestStore(35054, (unsigned long )(& re_syntax_table['_']));
# 164 "regex.c"
  re_syntax_table['_'] = (char)1;
  __CrestLoad(35055, (unsigned long )0, (long long )1);
  __CrestStore(35056, (unsigned long )(& done));
# 166 "regex.c"
  done = 1;

  return_label:
  {
  __CrestReturn(35057);
# 144 "regex.c"
  return;
  }
}
}
# 168 "regex.h"
reg_syntax_t re_syntax_options ;
# 441 "regex.h"
reg_syntax_t re_set_syntax(reg_syntax_t syntax ) ;
# 446 "regex.h"
char const *re_compile_pattern(char const *pattern , size_t length , struct re_pattern_buffer *bufp ) ;
# 454 "regex.h"
int re_compile_fastmap(struct re_pattern_buffer *bufp ) ;
# 462 "regex.h"
int re_search(struct re_pattern_buffer *bufp , char const *string , int size , int startpos ,
              int range , struct re_registers *regs ) ;
# 469 "regex.h"
int re_search_2(struct re_pattern_buffer *bufp , char const *string1 , int size1 ,
                char const *string2 , int size2 , int startpos , int range , struct re_registers *regs ,
                int stop ) ;
# 477 "regex.h"
int re_match(struct re_pattern_buffer *bufp , char const *string , int size , int pos ,
             struct re_registers *regs ) ;
# 483 "regex.h"
int re_match_2(struct re_pattern_buffer *bufp , char const *string1 , int size1 ,
               char const *string2 , int size2 , int pos , struct re_registers *regs ,
               int stop ) ;
# 501 "regex.h"
void re_set_registers(struct re_pattern_buffer *bufp , struct re_registers *regs ,
                      unsigned int num_regs , regoff_t *starts , regoff_t *ends ) ;
# 514 "regex.h"
int regcomp(regex_t *preg , char const *pattern , int cflags ) ;
# 515 "regex.h"
int regexec(regex_t const *preg , char const *string , size_t nmatch , regmatch_t *pmatch ,
            int eflags ) ;
# 518 "regex.h"
size_t regerror(int errcode , regex_t const *preg , char *errbuf , size_t errbuf_size ) ;
# 521 "regex.h"
void regfree(regex_t *preg ) ;
# 79 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) unsigned short const **( __attribute__((__leaf__)) __ctype_b_loc)(void) __attribute__((__const__)) ;
# 124 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) tolower)(int __c ) ;
# 347 "regex.c"
static int re_match_2_internal(struct re_pattern_buffer *bufp , char const *string1 ,
                               int size1 , char const *string2 , int size2 , int pos ,
                               struct re_registers *regs , int stop ) ;
# 953 "regex.c"
reg_syntax_t re_set_syntax(reg_syntax_t syntax )
{
  reg_syntax_t ret ;

  {
  __CrestCall(35059, 292);
  __CrestStore(35058, (unsigned long )(& syntax));
  __CrestLoad(35060, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
  __CrestStore(35061, (unsigned long )(& ret));
# 957 "regex.c"
  ret = re_syntax_options;
  __CrestLoad(35062, (unsigned long )(& syntax), (long long )syntax);
  __CrestStore(35063, (unsigned long )(& re_syntax_options));
# 959 "regex.c"
  re_syntax_options = syntax;
  {
  __CrestLoad(35064, (unsigned long )(& ret), (long long )ret);
  __CrestReturn(35065);
# 966 "regex.c"
  return (ret);
  }
}
}
# 974 "regex.c"
static char const *re_error_msgid[17] =
# 974 "regex.c"
  { "Success", "No match", "Invalid regular expression", "Invalid collation character",
        "Invalid character class name", "Trailing backslash", "Invalid back reference", "Unmatched [ or [^",
        "Unmatched ( or \\(", "Unmatched \\{", "Invalid content of \\{\\}", "Invalid range end",
        "Memory exhausted", "Invalid preceding regular expression", "Premature end of regular expression", "Regular expression too big",
        "Unmatched ) or \\)"};
# 1080 "regex.c"
int re_max_failures = 20000;
# 1453 "regex.c"
static char reg_unset_dummy ;
# 1459 "regex.c"
static reg_errcode_t regex_compile(char const *pattern , size_t size , reg_syntax_t syntax ,
                                   struct re_pattern_buffer *bufp ) ;
# 1462 "regex.c"
static void store_op1(re_opcode_t op , unsigned char *loc , int arg ) ;
# 1463 "regex.c"
static void store_op2(re_opcode_t op , unsigned char *loc , int arg1 , int arg2 ) ;
# 1465 "regex.c"
static void insert_op1(re_opcode_t op , unsigned char *loc , int arg , unsigned char *end ) ;
# 1467 "regex.c"
static void insert_op2(re_opcode_t op , unsigned char *loc , int arg1 , int arg2 ,
                       unsigned char *end ) ;
# 1469 "regex.c"
static boolean at_begline_loc_p(char const *pattern , char const *p , reg_syntax_t syntax ) ;
# 1471 "regex.c"
static boolean at_endline_loc_p(char const *p , char const *pend , reg_syntax_t syntax ) ;
# 1473 "regex.c"
static reg_errcode_t compile_range(char const **p_ptr , char const *pend , char *translate ,
                                   reg_syntax_t syntax , unsigned char *b ) ;
# 1755 "regex.c"
static boolean group_in_compile_stack(compile_stack_type compile_stack , regnum_t regnum ) ;
# 1781 "regex.c"
static reg_errcode_t regex_compile(char const *pattern , size_t size , reg_syntax_t syntax ,
                                   struct re_pattern_buffer *bufp )
{
  unsigned char c ;
  unsigned char c1 ;
  char const *p1 ;
  unsigned char *b ;
  compile_stack_type compile_stack ;
  char const *p ;
  char const *pend ;
  char *translate ;
  unsigned char *pending_exact ;
  unsigned char *laststart ;
  unsigned char *begalt ;
  char const *beg_interval ;
  unsigned char *fixup_alt_jump ;
  regnum_t regnum ;
  void *tmp ;
  unsigned int tmp___0 ;
  void *tmp___1 ;
  void *tmp___2 ;
  char const *tmp___3 ;
  unsigned char *old_buffer ;
  void *tmp___4 ;
  unsigned char *tmp___5 ;
  boolean tmp___6 ;
  unsigned char *old_buffer___0 ;
  void *tmp___7 ;
  unsigned char *tmp___8 ;
  boolean tmp___9 ;
  boolean keep_string_p ;
  char zero_times_ok ;
  char many_times_ok ;
  char const *tmp___10 ;
  char const *tmp___11 ;
  unsigned char *old_buffer___1 ;
  void *tmp___12 ;
  int tmp___13 ;
  int tmp___14 ;
  int tmp___15 ;
  int tmp___16 ;
  unsigned char *old_buffer___2 ;
  void *tmp___17 ;
  int tmp___18 ;
  unsigned char *old_buffer___3 ;
  void *tmp___19 ;
  unsigned char *old_buffer___4 ;
  void *tmp___20 ;
  unsigned char *tmp___21 ;
  boolean had_char_class ;
  unsigned char *old_buffer___5 ;
  void *tmp___22 ;
  unsigned char *old_buffer___6 ;
  void *tmp___23 ;
  unsigned char *tmp___24 ;
  int tmp___25 ;
  unsigned char *old_buffer___7 ;
  void *tmp___26 ;
  unsigned char *tmp___27 ;
  char const *tmp___28 ;
  char const *tmp___29 ;
  reg_errcode_t ret ;
  reg_errcode_t tmp___30 ;
  reg_errcode_t ret___0 ;
  char const *tmp___31 ;
  char str[257] ;
  char const *tmp___32 ;
  char const *tmp___33 ;
  unsigned char tmp___34 ;
  boolean is_lower ;
  int tmp___35 ;
  boolean is_upper ;
  int tmp___36 ;
  wctype_t wt ;
  int ch ;
  char const *tmp___37 ;
  wint_t tmp___38 ;
  int tmp___39 ;
  unsigned short const **tmp___40 ;
  unsigned short const **tmp___41 ;
  unsigned char tmp___42 ;
  char const *tmp___43 ;
  void *tmp___44 ;
  unsigned char *old_buffer___8 ;
  void *tmp___45 ;
  unsigned char *tmp___46 ;
  unsigned char *tmp___47 ;
  unsigned char *tmp___48 ;
  unsigned char *old_buffer___9 ;
  void *tmp___49 ;
  unsigned char *tmp___50 ;
  regnum_t this_group_regnum ;
  unsigned char *inner_group_loc ;
  unsigned char *old_buffer___10 ;
  void *tmp___51 ;
  unsigned char *tmp___52 ;
  unsigned char *tmp___53 ;
  unsigned char *tmp___54 ;
  unsigned char *old_buffer___11 ;
  void *tmp___55 ;
  unsigned char *old_buffer___12 ;
  void *tmp___56 ;
  int lower_bound ;
  int upper_bound ;
  char const *tmp___57 ;
  char const *tmp___58 ;
  unsigned short const **tmp___59 ;
  char const *tmp___60 ;
  char const *tmp___61 ;
  unsigned short const **tmp___62 ;
  char const *tmp___63 ;
  unsigned char *old_buffer___13 ;
  void *tmp___64 ;
  unsigned int nbytes ;
  unsigned char *old_buffer___14 ;
  void *tmp___65 ;
  char const *tmp___66 ;
  unsigned char *old_buffer___15 ;
  void *tmp___67 ;
  unsigned char *tmp___68 ;
  unsigned char *old_buffer___16 ;
  void *tmp___69 ;
  unsigned char *tmp___70 ;
  unsigned char *old_buffer___17 ;
  void *tmp___71 ;
  unsigned char *tmp___72 ;
  unsigned char *old_buffer___18 ;
  void *tmp___73 ;
  unsigned char *tmp___74 ;
  unsigned char *old_buffer___19 ;
  void *tmp___75 ;
  unsigned char *tmp___76 ;
  unsigned char *old_buffer___20 ;
  void *tmp___77 ;
  unsigned char *tmp___78 ;
  unsigned char *old_buffer___21 ;
  void *tmp___79 ;
  unsigned char *tmp___80 ;
  unsigned char *old_buffer___22 ;
  void *tmp___81 ;
  unsigned char *tmp___82 ;
  boolean tmp___83 ;
  unsigned char *old_buffer___23 ;
  void *tmp___84 ;
  unsigned char *tmp___85 ;
  unsigned char *tmp___86 ;
  unsigned char *old_buffer___24 ;
  void *tmp___87 ;
  unsigned char *tmp___88 ;
  unsigned char *tmp___89 ;
  int tmp___90 ;
  int tmp___91 ;
  int tmp___92 ;
  int tmp___93 ;
  int tmp___94 ;
  unsigned char *old_buffer___25 ;
  void *tmp___95 ;
  unsigned char *tmp___96 ;
  unsigned char *old_buffer___26 ;
  void *tmp___97 ;
  unsigned char *tmp___98 ;
  char *mem_163 ;
  char *mem_164 ;
  char *mem_165 ;
  char const *mem_166 ;
  char *mem_167 ;
  char const *mem_168 ;
  char *mem_169 ;
  char *mem_170 ;
  char *mem_171 ;
  unsigned char *mem_172 ;
  unsigned char *mem_173 ;
  unsigned char *mem_174 ;
  char *mem_175 ;
  char *mem_176 ;
  unsigned char *mem_177 ;
  unsigned char *mem_178 ;
  char const *mem_179 ;
  char const *mem_180 ;
  char const *mem_181 ;
  char const *mem_182 ;
  char const *mem_183 ;
  char *mem_184 ;
  char *mem_185 ;
  char *mem_186 ;
  char *mem_187 ;
  unsigned char *mem_188 ;
  unsigned char *mem_189 ;
  unsigned short const *mem_190 ;
  unsigned char *mem_191 ;
  unsigned char *mem_192 ;
  unsigned short const *mem_193 ;
  unsigned char *mem_194 ;
  unsigned char *mem_195 ;
  unsigned char *mem_196 ;
  unsigned char *mem_197 ;
  unsigned char *mem_198 ;
  unsigned char *mem_199 ;
  unsigned char *mem_200 ;
  unsigned char *mem_201 ;
  unsigned char *mem_202 ;
  unsigned char *mem_203 ;
  unsigned char *mem_204 ;
  unsigned char *mem_205 ;
  unsigned char *mem_206 ;
  unsigned char *mem_207 ;
  unsigned char *mem_208 ;
  unsigned char *mem_209 ;
  unsigned char *mem_210 ;
  unsigned char *mem_211 ;
  compile_stack_elt_t *mem_212 ;
  compile_stack_elt_t *mem_213 ;
  compile_stack_elt_t *mem_214 ;
  compile_stack_elt_t *mem_215 ;
  compile_stack_elt_t *mem_216 ;
  compile_stack_elt_t *mem_217 ;
  compile_stack_elt_t *mem_218 ;
  compile_stack_elt_t *mem_219 ;
  compile_stack_elt_t *mem_220 ;
  compile_stack_elt_t *mem_221 ;
  compile_stack_elt_t *mem_222 ;
  compile_stack_elt_t *mem_223 ;
  char *mem_224 ;
  unsigned short const *mem_225 ;
  char *mem_226 ;
  char *mem_227 ;
  unsigned short const *mem_228 ;
  char *mem_229 ;
  char *mem_230 ;
  char *mem_231 ;
  char const *mem_232 ;
  char *mem_233 ;
  char const *mem_234 ;
  char const *mem_235 ;
  char const *mem_236 ;
  char const *mem_237 ;
  reg_errcode_t __retres238 ;

  {
  __CrestCall(35068, 293);
  __CrestStore(35067, (unsigned long )(& syntax));
  __CrestStore(35066, (unsigned long )(& size));
# 1803 "regex.c"
  p = pattern;
# 1804 "regex.c"
  pend = pattern + size;
# 1807 "regex.c"
  translate = bufp->translate;
# 1813 "regex.c"
  pending_exact = (unsigned char *)0;
# 1818 "regex.c"
  laststart = (unsigned char *)0;
# 1830 "regex.c"
  fixup_alt_jump = (unsigned char *)0;
  __CrestLoad(35069, (unsigned long )0, (long long )((regnum_t )0));
  __CrestStore(35070, (unsigned long )(& regnum));
# 1835 "regex.c"
  regnum = (regnum_t )0;
  __CrestLoad(35071, (unsigned long )0, (long long )(32UL * sizeof(compile_stack_elt_t )));
# 1850 "regex.c"
  tmp = malloc(32UL * sizeof(compile_stack_elt_t ));
  __CrestClearStack(35072);
# 1850 "regex.c"
  compile_stack.stack = (compile_stack_elt_t *)tmp;
  {
  __CrestLoad(35075, (unsigned long )(& compile_stack.stack), (long long )((unsigned long )compile_stack.stack));
  __CrestLoad(35074, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(35073, 12, (long long )((unsigned long )compile_stack.stack == (unsigned long )((void *)0)));
# 1851 "regex.c"
  if ((unsigned long )compile_stack.stack == (unsigned long )((void *)0)) {
    __CrestBranch(35076, 7771, 1);
    __CrestLoad(35078, (unsigned long )0, (long long )((reg_errcode_t )12));
    __CrestStore(35079, (unsigned long )(& __retres238));
# 1852 "regex.c"
    __retres238 = (reg_errcode_t )12;
# 1852 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(35077, 7773, 0);

  }
  }
  __CrestLoad(35080, (unsigned long )0, (long long )32U);
  __CrestStore(35081, (unsigned long )(& compile_stack.size));
# 1854 "regex.c"
  compile_stack.size = 32U;
  __CrestLoad(35082, (unsigned long )0, (long long )0U);
  __CrestStore(35083, (unsigned long )(& compile_stack.avail));
# 1855 "regex.c"
  compile_stack.avail = 0U;
  __CrestLoad(35084, (unsigned long )(& syntax), (long long )syntax);
  __CrestStore(35085, (unsigned long )(& bufp->syntax));
# 1858 "regex.c"
  bufp->syntax = syntax;
# 1859 "regex.c"
  bufp->fastmap_accurate = 0U;
  __CrestLoad(35086, (unsigned long )0, (long long )0U);
  __CrestStore(35087, (unsigned long )(& tmp___0));
# 1860 "regex.c"
  tmp___0 = 0U;
# 1860 "regex.c"
  bufp->not_eol = tmp___0;
# 1860 "regex.c"
  bufp->not_bol = tmp___0;
  __CrestLoad(35088, (unsigned long )0, (long long )0UL);
  __CrestStore(35089, (unsigned long )(& bufp->used));
# 1865 "regex.c"
  bufp->used = 0UL;
  __CrestLoad(35090, (unsigned long )0, (long long )((size_t )0));
  __CrestStore(35091, (unsigned long )(& bufp->re_nsub));
# 1868 "regex.c"
  bufp->re_nsub = (size_t )0;
# 1872 "regex.c"
  init_syntax_once();
  __CrestClearStack(35092);
  {
  __CrestLoad(35095, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
  __CrestLoad(35094, (unsigned long )0, (long long )0UL);
  __CrestApply2(35093, 12, (long long )(bufp->allocated == 0UL));
# 1875 "regex.c"
  if (bufp->allocated == 0UL) {
    __CrestBranch(35096, 7776, 1);
    {
    __CrestLoad(35100, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
    __CrestLoad(35099, (unsigned long )0, (long long )0);
    __CrestApply2(35098, 13, (long long )(bufp->buffer != 0));
# 1877 "regex.c"
    if (bufp->buffer != 0) {
      __CrestBranch(35101, 7777, 1);
      __CrestLoad(35103, (unsigned long )0, (long long )(32UL * sizeof(unsigned char )));
# 1881 "regex.c"
      tmp___1 = realloc((void *)bufp->buffer, 32UL * sizeof(unsigned char ));
      __CrestClearStack(35104);
# 1881 "regex.c"
      bufp->buffer = (unsigned char *)tmp___1;
    } else {
      __CrestBranch(35102, 7778, 0);
      __CrestLoad(35105, (unsigned long )0, (long long )(32UL * sizeof(unsigned char )));
# 1885 "regex.c"
      tmp___2 = malloc(32UL * sizeof(unsigned char ));
      __CrestClearStack(35106);
# 1885 "regex.c"
      bufp->buffer = (unsigned char *)tmp___2;
    }
    }
    {
    __CrestLoad(35109, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
    __CrestLoad(35108, (unsigned long )0, (long long )0);
    __CrestApply2(35107, 12, (long long )(bufp->buffer == 0));
# 1887 "regex.c"
    if (bufp->buffer == 0) {
      __CrestBranch(35110, 7780, 1);
# 1887 "regex.c"
      free((void *)compile_stack.stack);
      __CrestClearStack(35112);
      __CrestLoad(35113, (unsigned long )0, (long long )((reg_errcode_t )12));
      __CrestStore(35114, (unsigned long )(& __retres238));
# 1887 "regex.c"
      __retres238 = (reg_errcode_t )12;
# 1887 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(35111, 7783, 0);

    }
    }
    __CrestLoad(35115, (unsigned long )0, (long long )32UL);
    __CrestStore(35116, (unsigned long )(& bufp->allocated));
# 1889 "regex.c"
    bufp->allocated = 32UL;
  } else {
    __CrestBranch(35097, 7785, 0);

  }
  }
# 1892 "regex.c"
  b = bufp->buffer;
# 1892 "regex.c"
  begalt = b;
  {
# 1895 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(35119, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(35118, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(35117, 13, (long long )((unsigned long )p != (unsigned long )pend));
# 1895 "regex.c"
    if ((unsigned long )p != (unsigned long )pend) {
      __CrestBranch(35120, 7791, 1);

    } else {
      __CrestBranch(35121, 7792, 0);
# 1895 "regex.c"
      goto while_break;
    }
    }
    {
# 1897 "regex.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(35124, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(35123, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(35122, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 1897 "regex.c"
      if ((unsigned long )p == (unsigned long )pend) {
        __CrestBranch(35125, 7797, 1);
        __CrestLoad(35127, (unsigned long )0, (long long )((reg_errcode_t )14));
        __CrestStore(35128, (unsigned long )(& __retres238));
# 1897 "regex.c"
        __retres238 = (reg_errcode_t )14;
# 1897 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(35126, 7799, 0);

      }
      }
# 1897 "regex.c"
      tmp___3 = p;
# 1897 "regex.c"
      p ++;
      __CrestLoad(35129, (unsigned long )tmp___3, (long long )*tmp___3);
      __CrestStore(35130, (unsigned long )(& c));
# 1897 "regex.c"
      c = (unsigned char )*tmp___3;
      {
      __CrestLoad(35133, (unsigned long )(& translate), (long long )((unsigned long )translate));
      __CrestLoad(35132, (unsigned long )0, (long long )0);
      __CrestApply2(35131, 13, (long long )(translate != 0));
# 1897 "regex.c"
      if (translate != 0) {
        __CrestBranch(35134, 7802, 1);
# 1897 "regex.c"
        mem_163 = translate + c;
        __CrestLoad(35136, (unsigned long )mem_163, (long long )*mem_163);
        __CrestStore(35137, (unsigned long )(& c));
# 1897 "regex.c"
        c = (unsigned char )*mem_163;
      } else {
        __CrestBranch(35135, 7803, 0);

      }
      }
# 1897 "regex.c"
      goto while_break___0;
    }
    while_break___0: ;
    }
    {
    {
    __CrestLoad(35140, (unsigned long )(& c), (long long )c);
    __CrestLoad(35139, (unsigned long )0, (long long )94);
    __CrestApply2(35138, 12, (long long )((int )c == 94));
# 1901 "regex.c"
    if ((int )c == 94) {
      __CrestBranch(35141, 7808, 1);
# 1901 "regex.c"
      goto case_94;
    } else {
      __CrestBranch(35142, 7809, 0);

    }
    }
    {
    __CrestLoad(35145, (unsigned long )(& c), (long long )c);
    __CrestLoad(35144, (unsigned long )0, (long long )36);
    __CrestApply2(35143, 12, (long long )((int )c == 36));
# 1916 "regex.c"
    if ((int )c == 36) {
      __CrestBranch(35146, 7811, 1);
# 1916 "regex.c"
      goto case_36;
    } else {
      __CrestBranch(35147, 7812, 0);

    }
    }
    {
    __CrestLoad(35150, (unsigned long )(& c), (long long )c);
    __CrestLoad(35149, (unsigned long )0, (long long )63);
    __CrestApply2(35148, 12, (long long )((int )c == 63));
# 1932 "regex.c"
    if ((int )c == 63) {
      __CrestBranch(35151, 7814, 1);
# 1932 "regex.c"
      goto case_63;
    } else {
      __CrestBranch(35152, 7815, 0);

    }
    }
    {
    __CrestLoad(35155, (unsigned long )(& c), (long long )c);
    __CrestLoad(35154, (unsigned long )0, (long long )43);
    __CrestApply2(35153, 12, (long long )((int )c == 43));
# 1932 "regex.c"
    if ((int )c == 43) {
      __CrestBranch(35156, 7817, 1);
# 1932 "regex.c"
      goto case_63;
    } else {
      __CrestBranch(35157, 7818, 0);

    }
    }
    {
    __CrestLoad(35160, (unsigned long )(& c), (long long )c);
    __CrestLoad(35159, (unsigned long )0, (long long )42);
    __CrestApply2(35158, 12, (long long )((int )c == 42));
# 1937 "regex.c"
    if ((int )c == 42) {
      __CrestBranch(35161, 7820, 1);
# 1937 "regex.c"
      goto handle_plus;
    } else {
      __CrestBranch(35162, 7821, 0);

    }
    }
    {
    __CrestLoad(35165, (unsigned long )(& c), (long long )c);
    __CrestLoad(35164, (unsigned long )0, (long long )46);
    __CrestApply2(35163, 12, (long long )((int )c == 46));
# 2064 "regex.c"
    if ((int )c == 46) {
      __CrestBranch(35166, 7823, 1);
# 2064 "regex.c"
      goto case_46;
    } else {
      __CrestBranch(35167, 7824, 0);

    }
    }
    {
    __CrestLoad(35170, (unsigned long )(& c), (long long )c);
    __CrestLoad(35169, (unsigned long )0, (long long )91);
    __CrestApply2(35168, 12, (long long )((int )c == 91));
# 2070 "regex.c"
    if ((int )c == 91) {
      __CrestBranch(35171, 7826, 1);
# 2070 "regex.c"
      goto case_91;
    } else {
      __CrestBranch(35172, 7827, 0);

    }
    }
    {
    __CrestLoad(35175, (unsigned long )(& c), (long long )c);
    __CrestLoad(35174, (unsigned long )0, (long long )40);
    __CrestApply2(35173, 12, (long long )((int )c == 40));
# 2286 "regex.c"
    if ((int )c == 40) {
      __CrestBranch(35176, 7829, 1);
# 2286 "regex.c"
      goto case_40;
    } else {
      __CrestBranch(35177, 7830, 0);

    }
    }
    {
    __CrestLoad(35180, (unsigned long )(& c), (long long )c);
    __CrestLoad(35179, (unsigned long )0, (long long )41);
    __CrestApply2(35178, 12, (long long )((int )c == 41));
# 2293 "regex.c"
    if ((int )c == 41) {
      __CrestBranch(35181, 7832, 1);
# 2293 "regex.c"
      goto case_41;
    } else {
      __CrestBranch(35182, 7833, 0);

    }
    }
    {
    __CrestLoad(35185, (unsigned long )(& c), (long long )c);
    __CrestLoad(35184, (unsigned long )0, (long long )10);
    __CrestApply2(35183, 12, (long long )((int )c == 10));
# 2300 "regex.c"
    if ((int )c == 10) {
      __CrestBranch(35186, 7835, 1);
# 2300 "regex.c"
      goto case_10;
    } else {
      __CrestBranch(35187, 7836, 0);

    }
    }
    {
    __CrestLoad(35190, (unsigned long )(& c), (long long )c);
    __CrestLoad(35189, (unsigned long )0, (long long )124);
    __CrestApply2(35188, 12, (long long )((int )c == 124));
# 2307 "regex.c"
    if ((int )c == 124) {
      __CrestBranch(35191, 7838, 1);
# 2307 "regex.c"
      goto case_124;
    } else {
      __CrestBranch(35192, 7839, 0);

    }
    }
    {
    __CrestLoad(35195, (unsigned long )(& c), (long long )c);
    __CrestLoad(35194, (unsigned long )0, (long long )123);
    __CrestApply2(35193, 12, (long long )((int )c == 123));
# 2314 "regex.c"
    if ((int )c == 123) {
      __CrestBranch(35196, 7841, 1);
# 2314 "regex.c"
      goto case_123;
    } else {
      __CrestBranch(35197, 7842, 0);

    }
    }
    {
    __CrestLoad(35200, (unsigned long )(& c), (long long )c);
    __CrestLoad(35199, (unsigned long )0, (long long )92);
    __CrestApply2(35198, 12, (long long )((int )c == 92));
# 2321 "regex.c"
    if ((int )c == 92) {
      __CrestBranch(35201, 7844, 1);
# 2321 "regex.c"
      goto case_92;
    } else {
      __CrestBranch(35202, 7845, 0);

    }
    }
# 2770 "regex.c"
    goto normal_char;
    case_94:
    {
    __CrestLoad(35207, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(35206, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
    __CrestLoad(35205, (unsigned long )0, (long long )1);
    __CrestApply2(35204, 18, (long long )((unsigned long )(pattern + 1)));
    __CrestApply2(35203, 12, (long long )((unsigned long )p == (unsigned long )(pattern + 1)));
# 1903 "regex.c"
    if ((unsigned long )p == (unsigned long )(pattern + 1)) {
      __CrestBranch(35208, 7848, 1);
# 1903 "regex.c"
      goto _L;
    } else {
      __CrestBranch(35209, 7849, 0);
      {
      __CrestLoad(35214, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(35213, (unsigned long )0, (long long )(((1UL << 1) << 1) << 1));
      __CrestApply2(35212, 5, (long long )(syntax & (((1UL << 1) << 1) << 1)));
      __CrestLoad(35211, (unsigned long )0, (long long )0);
      __CrestApply2(35210, 13, (long long )((syntax & (((1UL << 1) << 1) << 1)) != 0));
# 1903 "regex.c"
      if ((syntax & (((1UL << 1) << 1) << 1)) != 0) {
        __CrestBranch(35215, 7850, 1);
# 1903 "regex.c"
        goto _L;
      } else {
        __CrestBranch(35216, 7851, 0);
        __CrestLoad(35217, (unsigned long )(& syntax), (long long )syntax);
# 1903 "regex.c"
        tmp___6 = at_begline_loc_p(pattern, p, syntax);
        __CrestHandleReturn(35219, (long long )tmp___6);
        __CrestStore(35218, (unsigned long )(& tmp___6));
        {
        __CrestLoad(35222, (unsigned long )(& tmp___6), (long long )tmp___6);
        __CrestLoad(35221, (unsigned long )0, (long long )0);
        __CrestApply2(35220, 13, (long long )(tmp___6 != 0));
# 1903 "regex.c"
        if (tmp___6 != 0) {
          __CrestBranch(35223, 7853, 1);
          _L:
          {
# 1909 "regex.c"
          while (1) {
            while_continue___1: ;
            {
# 1909 "regex.c"
            while (1) {
              while_continue___2: ;
              {
              __CrestLoad(35231, (unsigned long )(& b), (long long )((unsigned long )b));
              __CrestLoad(35230, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
              __CrestApply2(35229, 18, (long long )(b - bufp->buffer));
              __CrestLoad(35228, (unsigned long )0, (long long )1L);
              __CrestApply2(35227, 0, (long long )((b - bufp->buffer) + 1L));
              __CrestLoad(35226, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
              __CrestApply2(35225, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 1909 "regex.c"
              if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
                __CrestBranch(35232, 7860, 1);

              } else {
                __CrestBranch(35233, 7861, 0);
# 1909 "regex.c"
                goto while_break___2;
              }
              }
              {
# 1909 "regex.c"
              while (1) {
                while_continue___3: ;
# 1909 "regex.c"
                old_buffer = bufp->buffer;
                {
                __CrestLoad(35236, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                __CrestLoad(35235, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                __CrestApply2(35234, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 1909 "regex.c"
                if (bufp->allocated == (unsigned long )(1L << 16)) {
                  __CrestBranch(35237, 7867, 1);
                  __CrestLoad(35239, (unsigned long )0, (long long )((reg_errcode_t )15));
                  __CrestStore(35240, (unsigned long )(& __retres238));
# 1909 "regex.c"
                  __retres238 = (reg_errcode_t )15;
# 1909 "regex.c"
                  goto return_label;
                } else {
                  __CrestBranch(35238, 7869, 0);

                }
                }
                __CrestLoad(35243, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                __CrestLoad(35242, (unsigned long )0, (long long )1);
                __CrestApply2(35241, 8, (long long )(bufp->allocated << 1));
                __CrestStore(35244, (unsigned long )(& bufp->allocated));
# 1909 "regex.c"
                bufp->allocated <<= 1;
                {
                __CrestLoad(35247, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                __CrestLoad(35246, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                __CrestApply2(35245, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 1909 "regex.c"
                if (bufp->allocated > (unsigned long )(1L << 16)) {
                  __CrestBranch(35248, 7872, 1);
                  __CrestLoad(35250, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                  __CrestStore(35251, (unsigned long )(& bufp->allocated));
# 1909 "regex.c"
                  bufp->allocated = (unsigned long )(1L << 16);
                } else {
                  __CrestBranch(35249, 7873, 0);

                }
                }
                __CrestLoad(35252, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 1909 "regex.c"
                tmp___4 = realloc((void *)bufp->buffer, bufp->allocated);
                __CrestClearStack(35253);
# 1909 "regex.c"
                bufp->buffer = (unsigned char *)tmp___4;
                {
                __CrestLoad(35256, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                __CrestLoad(35255, (unsigned long )0, (long long )((unsigned long )((void *)0)));
                __CrestApply2(35254, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 1909 "regex.c"
                if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
                  __CrestBranch(35257, 7876, 1);
                  __CrestLoad(35259, (unsigned long )0, (long long )((reg_errcode_t )12));
                  __CrestStore(35260, (unsigned long )(& __retres238));
# 1909 "regex.c"
                  __retres238 = (reg_errcode_t )12;
# 1909 "regex.c"
                  goto return_label;
                } else {
                  __CrestBranch(35258, 7878, 0);

                }
                }
                {
                __CrestLoad(35263, (unsigned long )(& old_buffer), (long long )((unsigned long )old_buffer));
                __CrestLoad(35262, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                __CrestApply2(35261, 13, (long long )((unsigned long )old_buffer != (unsigned long )bufp->buffer));
# 1909 "regex.c"
                if ((unsigned long )old_buffer != (unsigned long )bufp->buffer) {
                  __CrestBranch(35264, 7880, 1);
# 1909 "regex.c"
                  b = bufp->buffer + (b - old_buffer);
# 1909 "regex.c"
                  begalt = bufp->buffer + (begalt - old_buffer);
                  {
                  __CrestLoad(35268, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
                  __CrestLoad(35267, (unsigned long )0, (long long )0);
                  __CrestApply2(35266, 13, (long long )(fixup_alt_jump != 0));
# 1909 "regex.c"
                  if (fixup_alt_jump != 0) {
                    __CrestBranch(35269, 7882, 1);
# 1909 "regex.c"
                    fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer);
                  } else {
                    __CrestBranch(35270, 7883, 0);

                  }
                  }
                  {
                  __CrestLoad(35273, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
                  __CrestLoad(35272, (unsigned long )0, (long long )0);
                  __CrestApply2(35271, 13, (long long )(laststart != 0));
# 1909 "regex.c"
                  if (laststart != 0) {
                    __CrestBranch(35274, 7885, 1);
# 1909 "regex.c"
                    laststart = bufp->buffer + (laststart - old_buffer);
                  } else {
                    __CrestBranch(35275, 7886, 0);

                  }
                  }
                  {
                  __CrestLoad(35278, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
                  __CrestLoad(35277, (unsigned long )0, (long long )0);
                  __CrestApply2(35276, 13, (long long )(pending_exact != 0));
# 1909 "regex.c"
                  if (pending_exact != 0) {
                    __CrestBranch(35279, 7888, 1);
# 1909 "regex.c"
                    pending_exact = bufp->buffer + (pending_exact - old_buffer);
                  } else {
                    __CrestBranch(35280, 7889, 0);

                  }
                  }
                } else {
                  __CrestBranch(35265, 7890, 0);

                }
                }
# 1909 "regex.c"
                goto while_break___3;
              }
              while_break___3: ;
              }
            }
            while_break___2: ;
            }
# 1909 "regex.c"
            tmp___5 = b;
# 1909 "regex.c"
            b ++;
            __CrestLoad(35281, (unsigned long )0, (long long )(unsigned char)9);
            __CrestStore(35282, (unsigned long )tmp___5);
# 1909 "regex.c"
            *tmp___5 = (unsigned char)9;
# 1909 "regex.c"
            goto while_break___1;
          }
          while_break___1: ;
          }
        } else {
          __CrestBranch(35224, 7897, 0);
# 1911 "regex.c"
          goto normal_char;
        }
        }
      }
      }
    }
    }
# 1913 "regex.c"
    goto switch_break;
    case_36:
    {
    __CrestLoad(35285, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(35284, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(35283, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 1918 "regex.c"
    if ((unsigned long )p == (unsigned long )pend) {
      __CrestBranch(35286, 7900, 1);
# 1918 "regex.c"
      goto _L___0;
    } else {
      __CrestBranch(35287, 7901, 0);
      {
      __CrestLoad(35292, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(35291, (unsigned long )0, (long long )(((1UL << 1) << 1) << 1));
      __CrestApply2(35290, 5, (long long )(syntax & (((1UL << 1) << 1) << 1)));
      __CrestLoad(35289, (unsigned long )0, (long long )0);
      __CrestApply2(35288, 13, (long long )((syntax & (((1UL << 1) << 1) << 1)) != 0));
# 1918 "regex.c"
      if ((syntax & (((1UL << 1) << 1) << 1)) != 0) {
        __CrestBranch(35293, 7902, 1);
# 1918 "regex.c"
        goto _L___0;
      } else {
        __CrestBranch(35294, 7903, 0);
        __CrestLoad(35295, (unsigned long )(& syntax), (long long )syntax);
# 1918 "regex.c"
        tmp___9 = at_endline_loc_p(p, pend, syntax);
        __CrestHandleReturn(35297, (long long )tmp___9);
        __CrestStore(35296, (unsigned long )(& tmp___9));
        {
        __CrestLoad(35300, (unsigned long )(& tmp___9), (long long )tmp___9);
        __CrestLoad(35299, (unsigned long )0, (long long )0);
        __CrestApply2(35298, 13, (long long )(tmp___9 != 0));
# 1918 "regex.c"
        if (tmp___9 != 0) {
          __CrestBranch(35301, 7905, 1);
          _L___0:
          {
# 1924 "regex.c"
          while (1) {
            while_continue___4: ;
            {
# 1924 "regex.c"
            while (1) {
              while_continue___5: ;
              {
              __CrestLoad(35309, (unsigned long )(& b), (long long )((unsigned long )b));
              __CrestLoad(35308, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
              __CrestApply2(35307, 18, (long long )(b - bufp->buffer));
              __CrestLoad(35306, (unsigned long )0, (long long )1L);
              __CrestApply2(35305, 0, (long long )((b - bufp->buffer) + 1L));
              __CrestLoad(35304, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
              __CrestApply2(35303, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 1924 "regex.c"
              if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
                __CrestBranch(35310, 7912, 1);

              } else {
                __CrestBranch(35311, 7913, 0);
# 1924 "regex.c"
                goto while_break___5;
              }
              }
              {
# 1924 "regex.c"
              while (1) {
                while_continue___6: ;
# 1924 "regex.c"
                old_buffer___0 = bufp->buffer;
                {
                __CrestLoad(35314, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                __CrestLoad(35313, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                __CrestApply2(35312, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 1924 "regex.c"
                if (bufp->allocated == (unsigned long )(1L << 16)) {
                  __CrestBranch(35315, 7919, 1);
                  __CrestLoad(35317, (unsigned long )0, (long long )((reg_errcode_t )15));
                  __CrestStore(35318, (unsigned long )(& __retres238));
# 1924 "regex.c"
                  __retres238 = (reg_errcode_t )15;
# 1924 "regex.c"
                  goto return_label;
                } else {
                  __CrestBranch(35316, 7921, 0);

                }
                }
                __CrestLoad(35321, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                __CrestLoad(35320, (unsigned long )0, (long long )1);
                __CrestApply2(35319, 8, (long long )(bufp->allocated << 1));
                __CrestStore(35322, (unsigned long )(& bufp->allocated));
# 1924 "regex.c"
                bufp->allocated <<= 1;
                {
                __CrestLoad(35325, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                __CrestLoad(35324, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                __CrestApply2(35323, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 1924 "regex.c"
                if (bufp->allocated > (unsigned long )(1L << 16)) {
                  __CrestBranch(35326, 7924, 1);
                  __CrestLoad(35328, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                  __CrestStore(35329, (unsigned long )(& bufp->allocated));
# 1924 "regex.c"
                  bufp->allocated = (unsigned long )(1L << 16);
                } else {
                  __CrestBranch(35327, 7925, 0);

                }
                }
                __CrestLoad(35330, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 1924 "regex.c"
                tmp___7 = realloc((void *)bufp->buffer, bufp->allocated);
                __CrestClearStack(35331);
# 1924 "regex.c"
                bufp->buffer = (unsigned char *)tmp___7;
                {
                __CrestLoad(35334, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                __CrestLoad(35333, (unsigned long )0, (long long )((unsigned long )((void *)0)));
                __CrestApply2(35332, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 1924 "regex.c"
                if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
                  __CrestBranch(35335, 7928, 1);
                  __CrestLoad(35337, (unsigned long )0, (long long )((reg_errcode_t )12));
                  __CrestStore(35338, (unsigned long )(& __retres238));
# 1924 "regex.c"
                  __retres238 = (reg_errcode_t )12;
# 1924 "regex.c"
                  goto return_label;
                } else {
                  __CrestBranch(35336, 7930, 0);

                }
                }
                {
                __CrestLoad(35341, (unsigned long )(& old_buffer___0), (long long )((unsigned long )old_buffer___0));
                __CrestLoad(35340, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                __CrestApply2(35339, 13, (long long )((unsigned long )old_buffer___0 != (unsigned long )bufp->buffer));
# 1924 "regex.c"
                if ((unsigned long )old_buffer___0 != (unsigned long )bufp->buffer) {
                  __CrestBranch(35342, 7932, 1);
# 1924 "regex.c"
                  b = bufp->buffer + (b - old_buffer___0);
# 1924 "regex.c"
                  begalt = bufp->buffer + (begalt - old_buffer___0);
                  {
                  __CrestLoad(35346, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
                  __CrestLoad(35345, (unsigned long )0, (long long )0);
                  __CrestApply2(35344, 13, (long long )(fixup_alt_jump != 0));
# 1924 "regex.c"
                  if (fixup_alt_jump != 0) {
                    __CrestBranch(35347, 7934, 1);
# 1924 "regex.c"
                    fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___0);
                  } else {
                    __CrestBranch(35348, 7935, 0);

                  }
                  }
                  {
                  __CrestLoad(35351, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
                  __CrestLoad(35350, (unsigned long )0, (long long )0);
                  __CrestApply2(35349, 13, (long long )(laststart != 0));
# 1924 "regex.c"
                  if (laststart != 0) {
                    __CrestBranch(35352, 7937, 1);
# 1924 "regex.c"
                    laststart = bufp->buffer + (laststart - old_buffer___0);
                  } else {
                    __CrestBranch(35353, 7938, 0);

                  }
                  }
                  {
                  __CrestLoad(35356, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
                  __CrestLoad(35355, (unsigned long )0, (long long )0);
                  __CrestApply2(35354, 13, (long long )(pending_exact != 0));
# 1924 "regex.c"
                  if (pending_exact != 0) {
                    __CrestBranch(35357, 7940, 1);
# 1924 "regex.c"
                    pending_exact = bufp->buffer + (pending_exact - old_buffer___0);
                  } else {
                    __CrestBranch(35358, 7941, 0);

                  }
                  }
                } else {
                  __CrestBranch(35343, 7942, 0);

                }
                }
# 1924 "regex.c"
                goto while_break___6;
              }
              while_break___6: ;
              }
            }
            while_break___5: ;
            }
# 1924 "regex.c"
            tmp___8 = b;
# 1924 "regex.c"
            b ++;
            __CrestLoad(35359, (unsigned long )0, (long long )(unsigned char)10);
            __CrestStore(35360, (unsigned long )tmp___8);
# 1924 "regex.c"
            *tmp___8 = (unsigned char)10;
# 1924 "regex.c"
            goto while_break___4;
          }
          while_break___4: ;
          }
        } else {
          __CrestBranch(35302, 7949, 0);
# 1926 "regex.c"
          goto normal_char;
        }
        }
      }
      }
    }
    }
# 1928 "regex.c"
    goto switch_break;
    case_63:
    case_43:
    {
    __CrestLoad(35365, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(35364, (unsigned long )0, (long long )(1UL << 1));
    __CrestApply2(35363, 5, (long long )(syntax & (1UL << 1)));
    __CrestLoad(35362, (unsigned long )0, (long long )0);
    __CrestApply2(35361, 13, (long long )((syntax & (1UL << 1)) != 0));
# 1933 "regex.c"
    if ((syntax & (1UL << 1)) != 0) {
      __CrestBranch(35366, 7952, 1);
# 1935 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(35367, 7953, 0);
      {
      __CrestLoad(35372, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(35371, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(35370, 5, (long long )(syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(35369, (unsigned long )0, (long long )0);
      __CrestApply2(35368, 13, (long long )((syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 1933 "regex.c"
      if ((syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(35373, 7954, 1);
# 1935 "regex.c"
        goto normal_char;
      } else {
        __CrestBranch(35374, 7955, 0);

      }
      }
    }
    }
    handle_plus:
    case_42:
    {
    __CrestLoad(35377, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
    __CrestLoad(35376, (unsigned long )0, (long long )0);
    __CrestApply2(35375, 12, (long long )(laststart == 0));
# 1939 "regex.c"
    if (laststart == 0) {
      __CrestBranch(35378, 7957, 1);
      {
      __CrestLoad(35384, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(35383, (unsigned long )0, (long long )(((((1UL << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(35382, 5, (long long )(syntax & (((((1UL << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(35381, (unsigned long )0, (long long )0);
      __CrestApply2(35380, 13, (long long )((syntax & (((((1UL << 1) << 1) << 1) << 1) << 1)) != 0));
# 1941 "regex.c"
      if ((syntax & (((((1UL << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(35385, 7958, 1);
# 1942 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(35387);
        __CrestLoad(35388, (unsigned long )0, (long long )((reg_errcode_t )13));
        __CrestStore(35389, (unsigned long )(& __retres238));
# 1942 "regex.c"
        __retres238 = (reg_errcode_t )13;
# 1942 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(35386, 7961, 0);
        {
        __CrestLoad(35394, (unsigned long )(& syntax), (long long )syntax);
        __CrestLoad(35393, (unsigned long )0, (long long )((((1UL << 1) << 1) << 1) << 1));
        __CrestApply2(35392, 5, (long long )(syntax & ((((1UL << 1) << 1) << 1) << 1)));
        __CrestLoad(35391, (unsigned long )0, (long long )0);
        __CrestApply2(35390, 12, (long long )((syntax & ((((1UL << 1) << 1) << 1) << 1)) == 0));
# 1943 "regex.c"
        if ((syntax & ((((1UL << 1) << 1) << 1) << 1)) == 0) {
          __CrestBranch(35395, 7962, 1);
# 1944 "regex.c"
          goto normal_char;
        } else {
          __CrestBranch(35396, 7963, 0);

        }
        }
      }
      }
    } else {
      __CrestBranch(35379, 7964, 0);

    }
    }
    __CrestLoad(35397, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(35398, (unsigned long )(& keep_string_p));
# 1949 "regex.c"
    keep_string_p = (boolean )0;
    __CrestLoad(35399, (unsigned long )0, (long long )(char)0);
    __CrestStore(35400, (unsigned long )(& zero_times_ok));
# 1952 "regex.c"
    zero_times_ok = (char)0;
    __CrestLoad(35401, (unsigned long )0, (long long )(char)0);
    __CrestStore(35402, (unsigned long )(& many_times_ok));
# 1952 "regex.c"
    many_times_ok = (char)0;
    {
# 1959 "regex.c"
    while (1) {
      while_continue___7: ;
      __CrestLoad(35407, (unsigned long )(& zero_times_ok), (long long )zero_times_ok);
      __CrestLoad(35406, (unsigned long )(& c), (long long )c);
      __CrestLoad(35405, (unsigned long )0, (long long )43);
      __CrestApply2(35404, 13, (long long )((int )c != 43));
      __CrestApply2(35403, 6, (long long )((int )zero_times_ok | ((int )c != 43)));
      __CrestStore(35408, (unsigned long )(& zero_times_ok));
# 1961 "regex.c"
      zero_times_ok = (char )((int )zero_times_ok | ((int )c != 43));
      __CrestLoad(35413, (unsigned long )(& many_times_ok), (long long )many_times_ok);
      __CrestLoad(35412, (unsigned long )(& c), (long long )c);
      __CrestLoad(35411, (unsigned long )0, (long long )63);
      __CrestApply2(35410, 13, (long long )((int )c != 63));
      __CrestApply2(35409, 6, (long long )((int )many_times_ok | ((int )c != 63)));
      __CrestStore(35414, (unsigned long )(& many_times_ok));
# 1962 "regex.c"
      many_times_ok = (char )((int )many_times_ok | ((int )c != 63));
      {
      __CrestLoad(35417, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(35416, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(35415, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 1964 "regex.c"
      if ((unsigned long )p == (unsigned long )pend) {
        __CrestBranch(35418, 7971, 1);
# 1965 "regex.c"
        goto while_break___7;
      } else {
        __CrestBranch(35419, 7972, 0);

      }
      }
      {
# 1967 "regex.c"
      while (1) {
        while_continue___8: ;
        {
        __CrestLoad(35422, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(35421, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(35420, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 1967 "regex.c"
        if ((unsigned long )p == (unsigned long )pend) {
          __CrestBranch(35423, 7977, 1);
          __CrestLoad(35425, (unsigned long )0, (long long )((reg_errcode_t )14));
          __CrestStore(35426, (unsigned long )(& __retres238));
# 1967 "regex.c"
          __retres238 = (reg_errcode_t )14;
# 1967 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(35424, 7979, 0);

        }
        }
# 1967 "regex.c"
        tmp___10 = p;
# 1967 "regex.c"
        p ++;
        __CrestLoad(35427, (unsigned long )tmp___10, (long long )*tmp___10);
        __CrestStore(35428, (unsigned long )(& c));
# 1967 "regex.c"
        c = (unsigned char )*tmp___10;
        {
        __CrestLoad(35431, (unsigned long )(& translate), (long long )((unsigned long )translate));
        __CrestLoad(35430, (unsigned long )0, (long long )0);
        __CrestApply2(35429, 13, (long long )(translate != 0));
# 1967 "regex.c"
        if (translate != 0) {
          __CrestBranch(35432, 7982, 1);
# 1967 "regex.c"
          mem_164 = translate + c;
          __CrestLoad(35434, (unsigned long )mem_164, (long long )*mem_164);
          __CrestStore(35435, (unsigned long )(& c));
# 1967 "regex.c"
          c = (unsigned char )*mem_164;
        } else {
          __CrestBranch(35433, 7983, 0);

        }
        }
# 1967 "regex.c"
        goto while_break___8;
      }
      while_break___8: ;
      }
      {
      __CrestLoad(35438, (unsigned long )(& c), (long long )c);
      __CrestLoad(35437, (unsigned long )0, (long long )42);
      __CrestApply2(35436, 12, (long long )((int )c == 42));
# 1969 "regex.c"
      if ((int )c == 42) {
        __CrestBranch(35439, 7987, 1);

      } else {
        __CrestBranch(35440, 7988, 0);
        {
        __CrestLoad(35445, (unsigned long )(& syntax), (long long )syntax);
        __CrestLoad(35444, (unsigned long )0, (long long )(1UL << 1));
        __CrestApply2(35443, 5, (long long )(syntax & (1UL << 1)));
        __CrestLoad(35442, (unsigned long )0, (long long )0);
        __CrestApply2(35441, 12, (long long )((syntax & (1UL << 1)) == 0));
# 1969 "regex.c"
        if ((syntax & (1UL << 1)) == 0) {
          __CrestBranch(35446, 7989, 1);
          {
          __CrestLoad(35450, (unsigned long )(& c), (long long )c);
          __CrestLoad(35449, (unsigned long )0, (long long )43);
          __CrestApply2(35448, 12, (long long )((int )c == 43));
# 1969 "regex.c"
          if ((int )c == 43) {
            __CrestBranch(35451, 7990, 1);

          } else {
            __CrestBranch(35452, 7991, 0);
            {
            __CrestLoad(35455, (unsigned long )(& c), (long long )c);
            __CrestLoad(35454, (unsigned long )0, (long long )63);
            __CrestApply2(35453, 12, (long long )((int )c == 63));
# 1969 "regex.c"
            if ((int )c == 63) {
              __CrestBranch(35456, 7992, 1);

            } else {
              __CrestBranch(35457, 7993, 0);
# 1969 "regex.c"
              goto _L___1;
            }
            }
          }
          }
        } else {
          __CrestBranch(35447, 7994, 0);
          _L___1:
          {
          __CrestLoad(35462, (unsigned long )(& syntax), (long long )syntax);
          __CrestLoad(35461, (unsigned long )0, (long long )(1UL << 1));
          __CrestApply2(35460, 5, (long long )(syntax & (1UL << 1)));
          __CrestLoad(35459, (unsigned long )0, (long long )0);
          __CrestApply2(35458, 13, (long long )((syntax & (1UL << 1)) != 0));
# 1973 "regex.c"
          if ((syntax & (1UL << 1)) != 0) {
            __CrestBranch(35463, 7995, 1);
            {
            __CrestLoad(35467, (unsigned long )(& c), (long long )c);
            __CrestLoad(35466, (unsigned long )0, (long long )92);
            __CrestApply2(35465, 12, (long long )((int )c == 92));
# 1973 "regex.c"
            if ((int )c == 92) {
              __CrestBranch(35468, 7996, 1);
              {
              __CrestLoad(35472, (unsigned long )(& p), (long long )((unsigned long )p));
              __CrestLoad(35471, (unsigned long )(& pend), (long long )((unsigned long )pend));
              __CrestApply2(35470, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 1975 "regex.c"
              if ((unsigned long )p == (unsigned long )pend) {
                __CrestBranch(35473, 7997, 1);
# 1975 "regex.c"
                free((void *)compile_stack.stack);
                __CrestClearStack(35475);
                __CrestLoad(35476, (unsigned long )0, (long long )((reg_errcode_t )5));
                __CrestStore(35477, (unsigned long )(& __retres238));
# 1975 "regex.c"
                __retres238 = (reg_errcode_t )5;
# 1975 "regex.c"
                goto return_label;
              } else {
                __CrestBranch(35474, 8000, 0);

              }
              }
              {
# 1977 "regex.c"
              while (1) {
                while_continue___9: ;
                {
                __CrestLoad(35480, (unsigned long )(& p), (long long )((unsigned long )p));
                __CrestLoad(35479, (unsigned long )(& pend), (long long )((unsigned long )pend));
                __CrestApply2(35478, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 1977 "regex.c"
                if ((unsigned long )p == (unsigned long )pend) {
                  __CrestBranch(35481, 8005, 1);
                  __CrestLoad(35483, (unsigned long )0, (long long )((reg_errcode_t )14));
                  __CrestStore(35484, (unsigned long )(& __retres238));
# 1977 "regex.c"
                  __retres238 = (reg_errcode_t )14;
# 1977 "regex.c"
                  goto return_label;
                } else {
                  __CrestBranch(35482, 8007, 0);

                }
                }
# 1977 "regex.c"
                tmp___11 = p;
# 1977 "regex.c"
                p ++;
                __CrestLoad(35485, (unsigned long )tmp___11, (long long )*tmp___11);
                __CrestStore(35486, (unsigned long )(& c1));
# 1977 "regex.c"
                c1 = (unsigned char )*tmp___11;
                {
                __CrestLoad(35489, (unsigned long )(& translate), (long long )((unsigned long )translate));
                __CrestLoad(35488, (unsigned long )0, (long long )0);
                __CrestApply2(35487, 13, (long long )(translate != 0));
# 1977 "regex.c"
                if (translate != 0) {
                  __CrestBranch(35490, 8010, 1);
# 1977 "regex.c"
                  mem_165 = translate + c1;
                  __CrestLoad(35492, (unsigned long )mem_165, (long long )*mem_165);
                  __CrestStore(35493, (unsigned long )(& c1));
# 1977 "regex.c"
                  c1 = (unsigned char )*mem_165;
                } else {
                  __CrestBranch(35491, 8011, 0);

                }
                }
# 1977 "regex.c"
                goto while_break___9;
              }
              while_break___9: ;
              }
              {
              __CrestLoad(35496, (unsigned long )(& c1), (long long )c1);
              __CrestLoad(35495, (unsigned long )0, (long long )43);
              __CrestApply2(35494, 12, (long long )((int )c1 == 43));
# 1978 "regex.c"
              if ((int )c1 == 43) {
                __CrestBranch(35497, 8015, 1);

              } else {
                __CrestBranch(35498, 8016, 0);
                {
                __CrestLoad(35501, (unsigned long )(& c1), (long long )c1);
                __CrestLoad(35500, (unsigned long )0, (long long )63);
                __CrestApply2(35499, 12, (long long )((int )c1 == 63));
# 1978 "regex.c"
                if ((int )c1 == 63) {
                  __CrestBranch(35502, 8017, 1);

                } else {
                  __CrestBranch(35503, 8018, 0);
# 1980 "regex.c"
                  p --;
# 1981 "regex.c"
                  p --;
# 1982 "regex.c"
                  goto while_break___7;
                }
                }
              }
              }
              __CrestLoad(35504, (unsigned long )(& c1), (long long )c1);
              __CrestStore(35505, (unsigned long )(& c));
# 1985 "regex.c"
              c = c1;
            } else {
              __CrestBranch(35469, 8021, 0);
# 1989 "regex.c"
              p --;
# 1990 "regex.c"
              goto while_break___7;
            }
            }
          } else {
            __CrestBranch(35464, 8023, 0);
# 1989 "regex.c"
            p --;
# 1990 "regex.c"
            goto while_break___7;
          }
          }
        }
        }
      }
      }
    }
    while_break___7: ;
    }
    {
    __CrestLoad(35508, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
    __CrestLoad(35507, (unsigned long )0, (long long )0);
    __CrestApply2(35506, 12, (long long )(laststart == 0));
# 1998 "regex.c"
    if (laststart == 0) {
      __CrestBranch(35509, 8027, 1);
# 1999 "regex.c"
      goto switch_break;
    } else {
      __CrestBranch(35510, 8028, 0);

    }
    }
    {
    __CrestLoad(35513, (unsigned long )(& many_times_ok), (long long )many_times_ok);
    __CrestLoad(35512, (unsigned long )0, (long long )0);
    __CrestApply2(35511, 13, (long long )(many_times_ok != 0));
# 2003 "regex.c"
    if (many_times_ok != 0) {
      __CrestBranch(35514, 8030, 1);
      {
# 2017 "regex.c"
      while (1) {
        while_continue___10: ;
        {
        __CrestLoad(35522, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(35521, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(35520, 18, (long long )(b - bufp->buffer));
        __CrestLoad(35519, (unsigned long )0, (long long )3L);
        __CrestApply2(35518, 0, (long long )((b - bufp->buffer) + 3L));
        __CrestLoad(35517, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(35516, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2017 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
          __CrestBranch(35523, 8034, 1);

        } else {
          __CrestBranch(35524, 8035, 0);
# 2017 "regex.c"
          goto while_break___10;
        }
        }
        {
# 2017 "regex.c"
        while (1) {
          while_continue___11: ;
# 2017 "regex.c"
          old_buffer___1 = bufp->buffer;
          {
          __CrestLoad(35527, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35526, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35525, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2017 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(35528, 8041, 1);
            __CrestLoad(35530, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(35531, (unsigned long )(& __retres238));
# 2017 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2017 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35529, 8043, 0);

          }
          }
          __CrestLoad(35534, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35533, (unsigned long )0, (long long )1);
          __CrestApply2(35532, 8, (long long )(bufp->allocated << 1));
          __CrestStore(35535, (unsigned long )(& bufp->allocated));
# 2017 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(35538, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35537, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35536, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2017 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(35539, 8046, 1);
            __CrestLoad(35541, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(35542, (unsigned long )(& bufp->allocated));
# 2017 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(35540, 8047, 0);

          }
          }
          __CrestLoad(35543, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2017 "regex.c"
          tmp___12 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(35544);
# 2017 "regex.c"
          bufp->buffer = (unsigned char *)tmp___12;
          {
          __CrestLoad(35547, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(35546, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(35545, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2017 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(35548, 8050, 1);
            __CrestLoad(35550, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(35551, (unsigned long )(& __retres238));
# 2017 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2017 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35549, 8052, 0);

          }
          }
          {
          __CrestLoad(35554, (unsigned long )(& old_buffer___1), (long long )((unsigned long )old_buffer___1));
          __CrestLoad(35553, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(35552, 13, (long long )((unsigned long )old_buffer___1 != (unsigned long )bufp->buffer));
# 2017 "regex.c"
          if ((unsigned long )old_buffer___1 != (unsigned long )bufp->buffer) {
            __CrestBranch(35555, 8054, 1);
# 2017 "regex.c"
            b = bufp->buffer + (b - old_buffer___1);
# 2017 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___1);
            {
            __CrestLoad(35559, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(35558, (unsigned long )0, (long long )0);
            __CrestApply2(35557, 13, (long long )(fixup_alt_jump != 0));
# 2017 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(35560, 8056, 1);
# 2017 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___1);
            } else {
              __CrestBranch(35561, 8057, 0);

            }
            }
            {
            __CrestLoad(35564, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(35563, (unsigned long )0, (long long )0);
            __CrestApply2(35562, 13, (long long )(laststart != 0));
# 2017 "regex.c"
            if (laststart != 0) {
              __CrestBranch(35565, 8059, 1);
# 2017 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___1);
            } else {
              __CrestBranch(35566, 8060, 0);

            }
            }
            {
            __CrestLoad(35569, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(35568, (unsigned long )0, (long long )0);
            __CrestApply2(35567, 13, (long long )(pending_exact != 0));
# 2017 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(35570, 8062, 1);
# 2017 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___1);
            } else {
              __CrestBranch(35571, 8063, 0);

            }
            }
          } else {
            __CrestBranch(35556, 8064, 0);

          }
          }
# 2017 "regex.c"
          goto while_break___11;
        }
        while_break___11: ;
        }
      }
      while_break___10: ;
      }
      {
      __CrestLoad(35574, (unsigned long )(& translate), (long long )((unsigned long )translate));
      __CrestLoad(35573, (unsigned long )0, (long long )0);
      __CrestApply2(35572, 13, (long long )(translate != 0));
# 2024 "regex.c"
      if (translate != 0) {
        __CrestBranch(35575, 8069, 1);
# 2024 "regex.c"
        mem_166 = p - 2;
# 2024 "regex.c"
        mem_167 = translate + (unsigned char )*mem_166;
        __CrestLoad(35577, (unsigned long )mem_167, (long long )*mem_167);
        __CrestStore(35578, (unsigned long )(& tmp___13));
# 2024 "regex.c"
        tmp___13 = (int )*mem_167;
      } else {
        __CrestBranch(35576, 8070, 0);
# 2024 "regex.c"
        mem_168 = p - 2;
        __CrestLoad(35579, (unsigned long )mem_168, (long long )*mem_168);
        __CrestStore(35580, (unsigned long )(& tmp___13));
# 2024 "regex.c"
        tmp___13 = (int )*mem_168;
      }
      }
      {
      __CrestLoad(35583, (unsigned long )(& translate), (long long )((unsigned long )translate));
      __CrestLoad(35582, (unsigned long )0, (long long )0);
      __CrestApply2(35581, 13, (long long )(translate != 0));
# 2024 "regex.c"
      if (translate != 0) {
        __CrestBranch(35584, 8072, 1);
# 2024 "regex.c"
        mem_169 = translate + (unsigned char )'.';
        __CrestLoad(35586, (unsigned long )mem_169, (long long )*mem_169);
        __CrestStore(35587, (unsigned long )(& tmp___14));
# 2024 "regex.c"
        tmp___14 = (int )*mem_169;
      } else {
        __CrestBranch(35585, 8073, 0);
        __CrestLoad(35588, (unsigned long )0, (long long )'.');
        __CrestStore(35589, (unsigned long )(& tmp___14));
# 2024 "regex.c"
        tmp___14 = '.';
      }
      }
      {
      __CrestLoad(35592, (unsigned long )(& tmp___13), (long long )tmp___13);
      __CrestLoad(35591, (unsigned long )(& tmp___14), (long long )tmp___14);
      __CrestApply2(35590, 12, (long long )(tmp___13 == tmp___14));
# 2024 "regex.c"
      if (tmp___13 == tmp___14) {
        __CrestBranch(35593, 8075, 1);
        {
        __CrestLoad(35597, (unsigned long )(& zero_times_ok), (long long )zero_times_ok);
        __CrestLoad(35596, (unsigned long )0, (long long )0);
        __CrestApply2(35595, 13, (long long )(zero_times_ok != 0));
# 2024 "regex.c"
        if (zero_times_ok != 0) {
          __CrestBranch(35598, 8076, 1);
          {
          __CrestLoad(35602, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(35601, (unsigned long )(& pend), (long long )((unsigned long )pend));
          __CrestApply2(35600, 16, (long long )((unsigned long )p < (unsigned long )pend));
# 2024 "regex.c"
          if ((unsigned long )p < (unsigned long )pend) {
            __CrestBranch(35603, 8077, 1);
            {
            __CrestLoad(35607, (unsigned long )(& translate), (long long )((unsigned long )translate));
            __CrestLoad(35606, (unsigned long )0, (long long )0);
            __CrestApply2(35605, 13, (long long )(translate != 0));
# 2024 "regex.c"
            if (translate != 0) {
              __CrestBranch(35608, 8078, 1);
# 2024 "regex.c"
              mem_170 = translate + (unsigned char )*p;
              __CrestLoad(35610, (unsigned long )mem_170, (long long )*mem_170);
              __CrestStore(35611, (unsigned long )(& tmp___15));
# 2024 "regex.c"
              tmp___15 = (int )*mem_170;
            } else {
              __CrestBranch(35609, 8079, 0);
              __CrestLoad(35612, (unsigned long )p, (long long )*p);
              __CrestStore(35613, (unsigned long )(& tmp___15));
# 2024 "regex.c"
              tmp___15 = (int )*p;
            }
            }
            {
            __CrestLoad(35616, (unsigned long )(& translate), (long long )((unsigned long )translate));
            __CrestLoad(35615, (unsigned long )0, (long long )0);
            __CrestApply2(35614, 13, (long long )(translate != 0));
# 2024 "regex.c"
            if (translate != 0) {
              __CrestBranch(35617, 8081, 1);
# 2024 "regex.c"
              mem_171 = translate + (unsigned char )'\n';
              __CrestLoad(35619, (unsigned long )mem_171, (long long )*mem_171);
              __CrestStore(35620, (unsigned long )(& tmp___16));
# 2024 "regex.c"
              tmp___16 = (int )*mem_171;
            } else {
              __CrestBranch(35618, 8082, 0);
              __CrestLoad(35621, (unsigned long )0, (long long )'\n');
              __CrestStore(35622, (unsigned long )(& tmp___16));
# 2024 "regex.c"
              tmp___16 = '\n';
            }
            }
            {
            __CrestLoad(35625, (unsigned long )(& tmp___15), (long long )tmp___15);
            __CrestLoad(35624, (unsigned long )(& tmp___16), (long long )tmp___16);
            __CrestApply2(35623, 12, (long long )(tmp___15 == tmp___16));
# 2024 "regex.c"
            if (tmp___15 == tmp___16) {
              __CrestBranch(35626, 8084, 1);
              {
              __CrestLoad(35632, (unsigned long )(& syntax), (long long )syntax);
              __CrestLoad(35631, (unsigned long )0, (long long )((((((1UL << 1) << 1) << 1) << 1) << 1) << 1));
              __CrestApply2(35630, 5, (long long )(syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)));
              __CrestLoad(35629, (unsigned long )0, (long long )0);
              __CrestApply2(35628, 12, (long long )((syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 2024 "regex.c"
              if ((syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
                __CrestBranch(35633, 8085, 1);
                __CrestLoad(35635, (unsigned long )0, (long long )((re_opcode_t )13));
                __CrestLoad(35640, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
                __CrestLoad(35639, (unsigned long )(& b), (long long )((unsigned long )b));
                __CrestApply2(35638, 18, (long long )(laststart - b));
                __CrestLoad(35637, (unsigned long )0, (long long )3L);
                __CrestApply2(35636, 1, (long long )((laststart - b) - 3L));
# 2029 "regex.c"
                store_op1((re_opcode_t )13, b, (int )((laststart - b) - 3L));
                __CrestClearStack(35641);
                __CrestLoad(35642, (unsigned long )0, (long long )((boolean )1));
                __CrestStore(35643, (unsigned long )(& keep_string_p));
# 2030 "regex.c"
                keep_string_p = (boolean )1;
              } else {
                __CrestBranch(35634, 8086, 0);
                __CrestLoad(35644, (unsigned long )0, (long long )((re_opcode_t )18));
                __CrestLoad(35651, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
                __CrestLoad(35650, (unsigned long )0, (long long )3);
                __CrestApply2(35649, 18, (long long )((unsigned long )(laststart - 3)));
                __CrestLoad(35648, (unsigned long )(& b), (long long )((unsigned long )b));
                __CrestApply2(35647, 18, (long long )((laststart - 3) - b));
                __CrestLoad(35646, (unsigned long )0, (long long )3L);
                __CrestApply2(35645, 1, (long long )(((laststart - 3) - b) - 3L));
# 2034 "regex.c"
                store_op1((re_opcode_t )18, b, (int )(((laststart - 3) - b) - 3L));
                __CrestClearStack(35652);
              }
              }
            } else {
              __CrestBranch(35627, 8087, 0);
              __CrestLoad(35653, (unsigned long )0, (long long )((re_opcode_t )18));
              __CrestLoad(35660, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
              __CrestLoad(35659, (unsigned long )0, (long long )3);
              __CrestApply2(35658, 18, (long long )((unsigned long )(laststart - 3)));
              __CrestLoad(35657, (unsigned long )(& b), (long long )((unsigned long )b));
              __CrestApply2(35656, 18, (long long )((laststart - 3) - b));
              __CrestLoad(35655, (unsigned long )0, (long long )3L);
              __CrestApply2(35654, 1, (long long )(((laststart - 3) - b) - 3L));
# 2034 "regex.c"
              store_op1((re_opcode_t )18, b, (int )(((laststart - 3) - b) - 3L));
              __CrestClearStack(35661);
            }
            }
          } else {
            __CrestBranch(35604, 8088, 0);
            __CrestLoad(35662, (unsigned long )0, (long long )((re_opcode_t )18));
            __CrestLoad(35669, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(35668, (unsigned long )0, (long long )3);
            __CrestApply2(35667, 18, (long long )((unsigned long )(laststart - 3)));
            __CrestLoad(35666, (unsigned long )(& b), (long long )((unsigned long )b));
            __CrestApply2(35665, 18, (long long )((laststart - 3) - b));
            __CrestLoad(35664, (unsigned long )0, (long long )3L);
            __CrestApply2(35663, 1, (long long )(((laststart - 3) - b) - 3L));
# 2034 "regex.c"
            store_op1((re_opcode_t )18, b, (int )(((laststart - 3) - b) - 3L));
            __CrestClearStack(35670);
          }
          }
        } else {
          __CrestBranch(35599, 8089, 0);
          __CrestLoad(35671, (unsigned long )0, (long long )((re_opcode_t )18));
          __CrestLoad(35678, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
          __CrestLoad(35677, (unsigned long )0, (long long )3);
          __CrestApply2(35676, 18, (long long )((unsigned long )(laststart - 3)));
          __CrestLoad(35675, (unsigned long )(& b), (long long )((unsigned long )b));
          __CrestApply2(35674, 18, (long long )((laststart - 3) - b));
          __CrestLoad(35673, (unsigned long )0, (long long )3L);
          __CrestApply2(35672, 1, (long long )(((laststart - 3) - b) - 3L));
# 2034 "regex.c"
          store_op1((re_opcode_t )18, b, (int )(((laststart - 3) - b) - 3L));
          __CrestClearStack(35679);
        }
        }
      } else {
        __CrestBranch(35594, 8090, 0);
        __CrestLoad(35680, (unsigned long )0, (long long )((re_opcode_t )18));
        __CrestLoad(35687, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
        __CrestLoad(35686, (unsigned long )0, (long long )3);
        __CrestApply2(35685, 18, (long long )((unsigned long )(laststart - 3)));
        __CrestLoad(35684, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestApply2(35683, 18, (long long )((laststart - 3) - b));
        __CrestLoad(35682, (unsigned long )0, (long long )3L);
        __CrestApply2(35681, 1, (long long )(((laststart - 3) - b) - 3L));
# 2034 "regex.c"
        store_op1((re_opcode_t )18, b, (int )(((laststart - 3) - b) - 3L));
        __CrestClearStack(35688);
      }
      }
# 2037 "regex.c"
      b += 3;
    } else {
      __CrestBranch(35515, 8092, 0);

    }
    }
    {
# 2042 "regex.c"
    while (1) {
      while_continue___12: ;
      {
      __CrestLoad(35695, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(35694, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
      __CrestApply2(35693, 18, (long long )(b - bufp->buffer));
      __CrestLoad(35692, (unsigned long )0, (long long )3L);
      __CrestApply2(35691, 0, (long long )((b - bufp->buffer) + 3L));
      __CrestLoad(35690, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
      __CrestApply2(35689, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2042 "regex.c"
      if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
        __CrestBranch(35696, 8097, 1);

      } else {
        __CrestBranch(35697, 8098, 0);
# 2042 "regex.c"
        goto while_break___12;
      }
      }
      {
# 2042 "regex.c"
      while (1) {
        while_continue___13: ;
# 2042 "regex.c"
        old_buffer___2 = bufp->buffer;
        {
        __CrestLoad(35700, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(35699, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(35698, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2042 "regex.c"
        if (bufp->allocated == (unsigned long )(1L << 16)) {
          __CrestBranch(35701, 8104, 1);
          __CrestLoad(35703, (unsigned long )0, (long long )((reg_errcode_t )15));
          __CrestStore(35704, (unsigned long )(& __retres238));
# 2042 "regex.c"
          __retres238 = (reg_errcode_t )15;
# 2042 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(35702, 8106, 0);

        }
        }
        __CrestLoad(35707, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(35706, (unsigned long )0, (long long )1);
        __CrestApply2(35705, 8, (long long )(bufp->allocated << 1));
        __CrestStore(35708, (unsigned long )(& bufp->allocated));
# 2042 "regex.c"
        bufp->allocated <<= 1;
        {
        __CrestLoad(35711, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(35710, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(35709, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2042 "regex.c"
        if (bufp->allocated > (unsigned long )(1L << 16)) {
          __CrestBranch(35712, 8109, 1);
          __CrestLoad(35714, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestStore(35715, (unsigned long )(& bufp->allocated));
# 2042 "regex.c"
          bufp->allocated = (unsigned long )(1L << 16);
        } else {
          __CrestBranch(35713, 8110, 0);

        }
        }
        __CrestLoad(35716, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2042 "regex.c"
        tmp___17 = realloc((void *)bufp->buffer, bufp->allocated);
        __CrestClearStack(35717);
# 2042 "regex.c"
        bufp->buffer = (unsigned char *)tmp___17;
        {
        __CrestLoad(35720, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestLoad(35719, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(35718, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2042 "regex.c"
        if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
          __CrestBranch(35721, 8113, 1);
          __CrestLoad(35723, (unsigned long )0, (long long )((reg_errcode_t )12));
          __CrestStore(35724, (unsigned long )(& __retres238));
# 2042 "regex.c"
          __retres238 = (reg_errcode_t )12;
# 2042 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(35722, 8115, 0);

        }
        }
        {
        __CrestLoad(35727, (unsigned long )(& old_buffer___2), (long long )((unsigned long )old_buffer___2));
        __CrestLoad(35726, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(35725, 13, (long long )((unsigned long )old_buffer___2 != (unsigned long )bufp->buffer));
# 2042 "regex.c"
        if ((unsigned long )old_buffer___2 != (unsigned long )bufp->buffer) {
          __CrestBranch(35728, 8117, 1);
# 2042 "regex.c"
          b = bufp->buffer + (b - old_buffer___2);
# 2042 "regex.c"
          begalt = bufp->buffer + (begalt - old_buffer___2);
          {
          __CrestLoad(35732, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
          __CrestLoad(35731, (unsigned long )0, (long long )0);
          __CrestApply2(35730, 13, (long long )(fixup_alt_jump != 0));
# 2042 "regex.c"
          if (fixup_alt_jump != 0) {
            __CrestBranch(35733, 8119, 1);
# 2042 "regex.c"
            fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___2);
          } else {
            __CrestBranch(35734, 8120, 0);

          }
          }
          {
          __CrestLoad(35737, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
          __CrestLoad(35736, (unsigned long )0, (long long )0);
          __CrestApply2(35735, 13, (long long )(laststart != 0));
# 2042 "regex.c"
          if (laststart != 0) {
            __CrestBranch(35738, 8122, 1);
# 2042 "regex.c"
            laststart = bufp->buffer + (laststart - old_buffer___2);
          } else {
            __CrestBranch(35739, 8123, 0);

          }
          }
          {
          __CrestLoad(35742, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
          __CrestLoad(35741, (unsigned long )0, (long long )0);
          __CrestApply2(35740, 13, (long long )(pending_exact != 0));
# 2042 "regex.c"
          if (pending_exact != 0) {
            __CrestBranch(35743, 8125, 1);
# 2042 "regex.c"
            pending_exact = bufp->buffer + (pending_exact - old_buffer___2);
          } else {
            __CrestBranch(35744, 8126, 0);

          }
          }
        } else {
          __CrestBranch(35729, 8127, 0);

        }
        }
# 2042 "regex.c"
        goto while_break___13;
      }
      while_break___13: ;
      }
    }
    while_break___12: ;
    }
    {
    __CrestLoad(35747, (unsigned long )(& keep_string_p), (long long )keep_string_p);
    __CrestLoad(35746, (unsigned long )0, (long long )0);
    __CrestApply2(35745, 13, (long long )(keep_string_p != 0));
# 2043 "regex.c"
    if (keep_string_p != 0) {
      __CrestBranch(35748, 8132, 1);
      __CrestLoad(35750, (unsigned long )0, (long long )16);
      __CrestStore(35751, (unsigned long )(& tmp___18));
# 2043 "regex.c"
      tmp___18 = 16;
    } else {
      __CrestBranch(35749, 8133, 0);
      __CrestLoad(35752, (unsigned long )0, (long long )15);
      __CrestStore(35753, (unsigned long )(& tmp___18));
# 2043 "regex.c"
      tmp___18 = 15;
    }
    }
    __CrestLoad(35754, (unsigned long )(& tmp___18), (long long )tmp___18);
    __CrestLoad(35761, (unsigned long )(& b), (long long )((unsigned long )b));
    __CrestLoad(35760, (unsigned long )0, (long long )3);
    __CrestApply2(35759, 18, (long long )((unsigned long )(b + 3)));
    __CrestLoad(35758, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
    __CrestApply2(35757, 18, (long long )((b + 3) - laststart));
    __CrestLoad(35756, (unsigned long )0, (long long )3L);
    __CrestApply2(35755, 1, (long long )(((b + 3) - laststart) - 3L));
# 2043 "regex.c"
    insert_op1((re_opcode_t )tmp___18, laststart, (int )(((b + 3) - laststart) - 3L),
               b);
    __CrestClearStack(35762);
# 2046 "regex.c"
    pending_exact = (unsigned char *)0;
# 2047 "regex.c"
    b += 3;
    {
    __CrestLoad(35765, (unsigned long )(& zero_times_ok), (long long )zero_times_ok);
    __CrestLoad(35764, (unsigned long )0, (long long )0);
    __CrestApply2(35763, 12, (long long )(zero_times_ok == 0));
# 2049 "regex.c"
    if (zero_times_ok == 0) {
      __CrestBranch(35766, 8136, 1);
      {
# 2056 "regex.c"
      while (1) {
        while_continue___14: ;
        {
        __CrestLoad(35774, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(35773, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(35772, 18, (long long )(b - bufp->buffer));
        __CrestLoad(35771, (unsigned long )0, (long long )3L);
        __CrestApply2(35770, 0, (long long )((b - bufp->buffer) + 3L));
        __CrestLoad(35769, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(35768, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2056 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
          __CrestBranch(35775, 8140, 1);

        } else {
          __CrestBranch(35776, 8141, 0);
# 2056 "regex.c"
          goto while_break___14;
        }
        }
        {
# 2056 "regex.c"
        while (1) {
          while_continue___15: ;
# 2056 "regex.c"
          old_buffer___3 = bufp->buffer;
          {
          __CrestLoad(35779, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35778, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35777, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2056 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(35780, 8147, 1);
            __CrestLoad(35782, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(35783, (unsigned long )(& __retres238));
# 2056 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2056 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35781, 8149, 0);

          }
          }
          __CrestLoad(35786, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35785, (unsigned long )0, (long long )1);
          __CrestApply2(35784, 8, (long long )(bufp->allocated << 1));
          __CrestStore(35787, (unsigned long )(& bufp->allocated));
# 2056 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(35790, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35789, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35788, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2056 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(35791, 8152, 1);
            __CrestLoad(35793, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(35794, (unsigned long )(& bufp->allocated));
# 2056 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(35792, 8153, 0);

          }
          }
          __CrestLoad(35795, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2056 "regex.c"
          tmp___19 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(35796);
# 2056 "regex.c"
          bufp->buffer = (unsigned char *)tmp___19;
          {
          __CrestLoad(35799, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(35798, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(35797, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2056 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(35800, 8156, 1);
            __CrestLoad(35802, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(35803, (unsigned long )(& __retres238));
# 2056 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2056 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35801, 8158, 0);

          }
          }
          {
          __CrestLoad(35806, (unsigned long )(& old_buffer___3), (long long )((unsigned long )old_buffer___3));
          __CrestLoad(35805, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(35804, 13, (long long )((unsigned long )old_buffer___3 != (unsigned long )bufp->buffer));
# 2056 "regex.c"
          if ((unsigned long )old_buffer___3 != (unsigned long )bufp->buffer) {
            __CrestBranch(35807, 8160, 1);
# 2056 "regex.c"
            b = bufp->buffer + (b - old_buffer___3);
# 2056 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___3);
            {
            __CrestLoad(35811, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(35810, (unsigned long )0, (long long )0);
            __CrestApply2(35809, 13, (long long )(fixup_alt_jump != 0));
# 2056 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(35812, 8162, 1);
# 2056 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___3);
            } else {
              __CrestBranch(35813, 8163, 0);

            }
            }
            {
            __CrestLoad(35816, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(35815, (unsigned long )0, (long long )0);
            __CrestApply2(35814, 13, (long long )(laststart != 0));
# 2056 "regex.c"
            if (laststart != 0) {
              __CrestBranch(35817, 8165, 1);
# 2056 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___3);
            } else {
              __CrestBranch(35818, 8166, 0);

            }
            }
            {
            __CrestLoad(35821, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(35820, (unsigned long )0, (long long )0);
            __CrestApply2(35819, 13, (long long )(pending_exact != 0));
# 2056 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(35822, 8168, 1);
# 2056 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___3);
            } else {
              __CrestBranch(35823, 8169, 0);

            }
            }
          } else {
            __CrestBranch(35808, 8170, 0);

          }
          }
# 2056 "regex.c"
          goto while_break___15;
        }
        while_break___15: ;
        }
      }
      while_break___14: ;
      }
      __CrestLoad(35824, (unsigned long )0, (long long )((re_opcode_t )19));
      __CrestLoad(35831, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
      __CrestLoad(35830, (unsigned long )0, (long long )6);
      __CrestApply2(35829, 18, (long long )((unsigned long )(laststart + 6)));
      __CrestLoad(35828, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
      __CrestApply2(35827, 18, (long long )((laststart + 6) - laststart));
      __CrestLoad(35826, (unsigned long )0, (long long )3L);
      __CrestApply2(35825, 1, (long long )(((laststart + 6) - laststart) - 3L));
# 2057 "regex.c"
      insert_op1((re_opcode_t )19, laststart, (int )(((laststart + 6) - laststart) - 3L),
                 b);
      __CrestClearStack(35832);
# 2058 "regex.c"
      b += 3;
    } else {
      __CrestBranch(35767, 8175, 0);

    }
    }
# 2061 "regex.c"
    goto switch_break;
    case_46:
# 2065 "regex.c"
    laststart = b;
    {
# 2066 "regex.c"
    while (1) {
      while_continue___16: ;
      {
# 2066 "regex.c"
      while (1) {
        while_continue___17: ;
        {
        __CrestLoad(35839, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(35838, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(35837, 18, (long long )(b - bufp->buffer));
        __CrestLoad(35836, (unsigned long )0, (long long )1L);
        __CrestApply2(35835, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(35834, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(35833, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2066 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(35840, 8185, 1);

        } else {
          __CrestBranch(35841, 8186, 0);
# 2066 "regex.c"
          goto while_break___17;
        }
        }
        {
# 2066 "regex.c"
        while (1) {
          while_continue___18: ;
# 2066 "regex.c"
          old_buffer___4 = bufp->buffer;
          {
          __CrestLoad(35844, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35843, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35842, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2066 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(35845, 8192, 1);
            __CrestLoad(35847, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(35848, (unsigned long )(& __retres238));
# 2066 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2066 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35846, 8194, 0);

          }
          }
          __CrestLoad(35851, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35850, (unsigned long )0, (long long )1);
          __CrestApply2(35849, 8, (long long )(bufp->allocated << 1));
          __CrestStore(35852, (unsigned long )(& bufp->allocated));
# 2066 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(35855, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35854, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35853, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2066 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(35856, 8197, 1);
            __CrestLoad(35858, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(35859, (unsigned long )(& bufp->allocated));
# 2066 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(35857, 8198, 0);

          }
          }
          __CrestLoad(35860, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2066 "regex.c"
          tmp___20 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(35861);
# 2066 "regex.c"
          bufp->buffer = (unsigned char *)tmp___20;
          {
          __CrestLoad(35864, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(35863, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(35862, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2066 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(35865, 8201, 1);
            __CrestLoad(35867, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(35868, (unsigned long )(& __retres238));
# 2066 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2066 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35866, 8203, 0);

          }
          }
          {
          __CrestLoad(35871, (unsigned long )(& old_buffer___4), (long long )((unsigned long )old_buffer___4));
          __CrestLoad(35870, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(35869, 13, (long long )((unsigned long )old_buffer___4 != (unsigned long )bufp->buffer));
# 2066 "regex.c"
          if ((unsigned long )old_buffer___4 != (unsigned long )bufp->buffer) {
            __CrestBranch(35872, 8205, 1);
# 2066 "regex.c"
            b = bufp->buffer + (b - old_buffer___4);
# 2066 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___4);
            {
            __CrestLoad(35876, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(35875, (unsigned long )0, (long long )0);
            __CrestApply2(35874, 13, (long long )(fixup_alt_jump != 0));
# 2066 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(35877, 8207, 1);
# 2066 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___4);
            } else {
              __CrestBranch(35878, 8208, 0);

            }
            }
            {
            __CrestLoad(35881, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(35880, (unsigned long )0, (long long )0);
            __CrestApply2(35879, 13, (long long )(laststart != 0));
# 2066 "regex.c"
            if (laststart != 0) {
              __CrestBranch(35882, 8210, 1);
# 2066 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___4);
            } else {
              __CrestBranch(35883, 8211, 0);

            }
            }
            {
            __CrestLoad(35886, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(35885, (unsigned long )0, (long long )0);
            __CrestApply2(35884, 13, (long long )(pending_exact != 0));
# 2066 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(35887, 8213, 1);
# 2066 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___4);
            } else {
              __CrestBranch(35888, 8214, 0);

            }
            }
          } else {
            __CrestBranch(35873, 8215, 0);

          }
          }
# 2066 "regex.c"
          goto while_break___18;
        }
        while_break___18: ;
        }
      }
      while_break___17: ;
      }
# 2066 "regex.c"
      tmp___21 = b;
# 2066 "regex.c"
      b ++;
      __CrestLoad(35889, (unsigned long )0, (long long )(unsigned char)3);
      __CrestStore(35890, (unsigned long )tmp___21);
# 2066 "regex.c"
      *tmp___21 = (unsigned char)3;
# 2066 "regex.c"
      goto while_break___16;
    }
    while_break___16: ;
    }
# 2067 "regex.c"
    goto switch_break;
    case_91:
    __CrestLoad(35891, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(35892, (unsigned long )(& had_char_class));
# 2072 "regex.c"
    had_char_class = (boolean )0;
    {
    __CrestLoad(35895, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(35894, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(35893, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2074 "regex.c"
    if ((unsigned long )p == (unsigned long )pend) {
      __CrestBranch(35896, 8225, 1);
# 2074 "regex.c"
      free((void *)compile_stack.stack);
      __CrestClearStack(35898);
      __CrestLoad(35899, (unsigned long )0, (long long )((reg_errcode_t )7));
      __CrestStore(35900, (unsigned long )(& __retres238));
# 2074 "regex.c"
      __retres238 = (reg_errcode_t )7;
# 2074 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(35897, 8228, 0);

    }
    }
    {
# 2078 "regex.c"
    while (1) {
      while_continue___19: ;
      {
      __CrestLoad(35907, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(35906, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
      __CrestApply2(35905, 18, (long long )(b - bufp->buffer));
      __CrestLoad(35904, (unsigned long )0, (long long )34L);
      __CrestApply2(35903, 0, (long long )((b - bufp->buffer) + 34L));
      __CrestLoad(35902, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
      __CrestApply2(35901, 14, (long long )((unsigned long )((b - bufp->buffer) + 34L) > bufp->allocated));
# 2078 "regex.c"
      if ((unsigned long )((b - bufp->buffer) + 34L) > bufp->allocated) {
        __CrestBranch(35908, 8233, 1);

      } else {
        __CrestBranch(35909, 8234, 0);
# 2078 "regex.c"
        goto while_break___19;
      }
      }
      {
# 2078 "regex.c"
      while (1) {
        while_continue___20: ;
# 2078 "regex.c"
        old_buffer___5 = bufp->buffer;
        {
        __CrestLoad(35912, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(35911, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(35910, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2078 "regex.c"
        if (bufp->allocated == (unsigned long )(1L << 16)) {
          __CrestBranch(35913, 8240, 1);
          __CrestLoad(35915, (unsigned long )0, (long long )((reg_errcode_t )15));
          __CrestStore(35916, (unsigned long )(& __retres238));
# 2078 "regex.c"
          __retres238 = (reg_errcode_t )15;
# 2078 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(35914, 8242, 0);

        }
        }
        __CrestLoad(35919, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(35918, (unsigned long )0, (long long )1);
        __CrestApply2(35917, 8, (long long )(bufp->allocated << 1));
        __CrestStore(35920, (unsigned long )(& bufp->allocated));
# 2078 "regex.c"
        bufp->allocated <<= 1;
        {
        __CrestLoad(35923, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(35922, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(35921, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2078 "regex.c"
        if (bufp->allocated > (unsigned long )(1L << 16)) {
          __CrestBranch(35924, 8245, 1);
          __CrestLoad(35926, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestStore(35927, (unsigned long )(& bufp->allocated));
# 2078 "regex.c"
          bufp->allocated = (unsigned long )(1L << 16);
        } else {
          __CrestBranch(35925, 8246, 0);

        }
        }
        __CrestLoad(35928, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2078 "regex.c"
        tmp___22 = realloc((void *)bufp->buffer, bufp->allocated);
        __CrestClearStack(35929);
# 2078 "regex.c"
        bufp->buffer = (unsigned char *)tmp___22;
        {
        __CrestLoad(35932, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestLoad(35931, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(35930, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2078 "regex.c"
        if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
          __CrestBranch(35933, 8249, 1);
          __CrestLoad(35935, (unsigned long )0, (long long )((reg_errcode_t )12));
          __CrestStore(35936, (unsigned long )(& __retres238));
# 2078 "regex.c"
          __retres238 = (reg_errcode_t )12;
# 2078 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(35934, 8251, 0);

        }
        }
        {
        __CrestLoad(35939, (unsigned long )(& old_buffer___5), (long long )((unsigned long )old_buffer___5));
        __CrestLoad(35938, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(35937, 13, (long long )((unsigned long )old_buffer___5 != (unsigned long )bufp->buffer));
# 2078 "regex.c"
        if ((unsigned long )old_buffer___5 != (unsigned long )bufp->buffer) {
          __CrestBranch(35940, 8253, 1);
# 2078 "regex.c"
          b = bufp->buffer + (b - old_buffer___5);
# 2078 "regex.c"
          begalt = bufp->buffer + (begalt - old_buffer___5);
          {
          __CrestLoad(35944, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
          __CrestLoad(35943, (unsigned long )0, (long long )0);
          __CrestApply2(35942, 13, (long long )(fixup_alt_jump != 0));
# 2078 "regex.c"
          if (fixup_alt_jump != 0) {
            __CrestBranch(35945, 8255, 1);
# 2078 "regex.c"
            fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___5);
          } else {
            __CrestBranch(35946, 8256, 0);

          }
          }
          {
          __CrestLoad(35949, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
          __CrestLoad(35948, (unsigned long )0, (long long )0);
          __CrestApply2(35947, 13, (long long )(laststart != 0));
# 2078 "regex.c"
          if (laststart != 0) {
            __CrestBranch(35950, 8258, 1);
# 2078 "regex.c"
            laststart = bufp->buffer + (laststart - old_buffer___5);
          } else {
            __CrestBranch(35951, 8259, 0);

          }
          }
          {
          __CrestLoad(35954, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
          __CrestLoad(35953, (unsigned long )0, (long long )0);
          __CrestApply2(35952, 13, (long long )(pending_exact != 0));
# 2078 "regex.c"
          if (pending_exact != 0) {
            __CrestBranch(35955, 8261, 1);
# 2078 "regex.c"
            pending_exact = bufp->buffer + (pending_exact - old_buffer___5);
          } else {
            __CrestBranch(35956, 8262, 0);

          }
          }
        } else {
          __CrestBranch(35941, 8263, 0);

        }
        }
# 2078 "regex.c"
        goto while_break___20;
      }
      while_break___20: ;
      }
    }
    while_break___19: ;
    }
# 2080 "regex.c"
    laststart = b;
    {
# 2084 "regex.c"
    while (1) {
      while_continue___21: ;
      {
# 2084 "regex.c"
      while (1) {
        while_continue___22: ;
        {
        __CrestLoad(35963, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(35962, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(35961, 18, (long long )(b - bufp->buffer));
        __CrestLoad(35960, (unsigned long )0, (long long )1L);
        __CrestApply2(35959, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(35958, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(35957, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2084 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(35964, 8275, 1);

        } else {
          __CrestBranch(35965, 8276, 0);
# 2084 "regex.c"
          goto while_break___22;
        }
        }
        {
# 2084 "regex.c"
        while (1) {
          while_continue___23: ;
# 2084 "regex.c"
          old_buffer___6 = bufp->buffer;
          {
          __CrestLoad(35968, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35967, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35966, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2084 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(35969, 8282, 1);
            __CrestLoad(35971, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(35972, (unsigned long )(& __retres238));
# 2084 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2084 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35970, 8284, 0);

          }
          }
          __CrestLoad(35975, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35974, (unsigned long )0, (long long )1);
          __CrestApply2(35973, 8, (long long )(bufp->allocated << 1));
          __CrestStore(35976, (unsigned long )(& bufp->allocated));
# 2084 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(35979, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(35978, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(35977, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2084 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(35980, 8287, 1);
            __CrestLoad(35982, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(35983, (unsigned long )(& bufp->allocated));
# 2084 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(35981, 8288, 0);

          }
          }
          __CrestLoad(35984, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2084 "regex.c"
          tmp___23 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(35985);
# 2084 "regex.c"
          bufp->buffer = (unsigned char *)tmp___23;
          {
          __CrestLoad(35988, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(35987, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(35986, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2084 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(35989, 8291, 1);
            __CrestLoad(35991, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(35992, (unsigned long )(& __retres238));
# 2084 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2084 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(35990, 8293, 0);

          }
          }
          {
          __CrestLoad(35995, (unsigned long )(& old_buffer___6), (long long )((unsigned long )old_buffer___6));
          __CrestLoad(35994, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(35993, 13, (long long )((unsigned long )old_buffer___6 != (unsigned long )bufp->buffer));
# 2084 "regex.c"
          if ((unsigned long )old_buffer___6 != (unsigned long )bufp->buffer) {
            __CrestBranch(35996, 8295, 1);
# 2084 "regex.c"
            b = bufp->buffer + (b - old_buffer___6);
# 2084 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___6);
            {
            __CrestLoad(36000, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(35999, (unsigned long )0, (long long )0);
            __CrestApply2(35998, 13, (long long )(fixup_alt_jump != 0));
# 2084 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(36001, 8297, 1);
# 2084 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___6);
            } else {
              __CrestBranch(36002, 8298, 0);

            }
            }
            {
            __CrestLoad(36005, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(36004, (unsigned long )0, (long long )0);
            __CrestApply2(36003, 13, (long long )(laststart != 0));
# 2084 "regex.c"
            if (laststart != 0) {
              __CrestBranch(36006, 8300, 1);
# 2084 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___6);
            } else {
              __CrestBranch(36007, 8301, 0);

            }
            }
            {
            __CrestLoad(36010, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(36009, (unsigned long )0, (long long )0);
            __CrestApply2(36008, 13, (long long )(pending_exact != 0));
# 2084 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(36011, 8303, 1);
# 2084 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___6);
            } else {
              __CrestBranch(36012, 8304, 0);

            }
            }
          } else {
            __CrestBranch(35997, 8305, 0);

          }
          }
# 2084 "regex.c"
          goto while_break___23;
        }
        while_break___23: ;
        }
      }
      while_break___22: ;
      }
# 2084 "regex.c"
      tmp___24 = b;
# 2084 "regex.c"
      b ++;
      {
      __CrestLoad(36015, (unsigned long )p, (long long )*p);
      __CrestLoad(36014, (unsigned long )0, (long long )94);
      __CrestApply2(36013, 12, (long long )((int const )*p == 94));
# 2084 "regex.c"
      if ((int const )*p == 94) {
        __CrestBranch(36016, 8311, 1);
        __CrestLoad(36018, (unsigned long )0, (long long )5);
        __CrestStore(36019, (unsigned long )(& tmp___25));
# 2084 "regex.c"
        tmp___25 = 5;
      } else {
        __CrestBranch(36017, 8312, 0);
        __CrestLoad(36020, (unsigned long )0, (long long )4);
        __CrestStore(36021, (unsigned long )(& tmp___25));
# 2084 "regex.c"
        tmp___25 = 4;
      }
      }
      __CrestLoad(36022, (unsigned long )(& tmp___25), (long long )tmp___25);
      __CrestStore(36023, (unsigned long )tmp___24);
# 2084 "regex.c"
      *tmp___24 = (unsigned char )tmp___25;
# 2084 "regex.c"
      goto while_break___21;
    }
    while_break___21: ;
    }
    {
    __CrestLoad(36026, (unsigned long )p, (long long )*p);
    __CrestLoad(36025, (unsigned long )0, (long long )94);
    __CrestApply2(36024, 12, (long long )((int const )*p == 94));
# 2085 "regex.c"
    if ((int const )*p == 94) {
      __CrestBranch(36027, 8317, 1);
# 2086 "regex.c"
      p ++;
    } else {
      __CrestBranch(36028, 8318, 0);

    }
    }
# 2089 "regex.c"
    p1 = p;
    {
# 2092 "regex.c"
    while (1) {
      while_continue___24: ;
      {
# 2092 "regex.c"
      while (1) {
        while_continue___25: ;
        {
        __CrestLoad(36035, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(36034, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(36033, 18, (long long )(b - bufp->buffer));
        __CrestLoad(36032, (unsigned long )0, (long long )1L);
        __CrestApply2(36031, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(36030, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(36029, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2092 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(36036, 8327, 1);

        } else {
          __CrestBranch(36037, 8328, 0);
# 2092 "regex.c"
          goto while_break___25;
        }
        }
        {
# 2092 "regex.c"
        while (1) {
          while_continue___26: ;
# 2092 "regex.c"
          old_buffer___7 = bufp->buffer;
          {
          __CrestLoad(36040, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(36039, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(36038, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2092 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(36041, 8334, 1);
            __CrestLoad(36043, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(36044, (unsigned long )(& __retres238));
# 2092 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2092 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(36042, 8336, 0);

          }
          }
          __CrestLoad(36047, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(36046, (unsigned long )0, (long long )1);
          __CrestApply2(36045, 8, (long long )(bufp->allocated << 1));
          __CrestStore(36048, (unsigned long )(& bufp->allocated));
# 2092 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(36051, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(36050, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(36049, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2092 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(36052, 8339, 1);
            __CrestLoad(36054, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(36055, (unsigned long )(& bufp->allocated));
# 2092 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(36053, 8340, 0);

          }
          }
          __CrestLoad(36056, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2092 "regex.c"
          tmp___26 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(36057);
# 2092 "regex.c"
          bufp->buffer = (unsigned char *)tmp___26;
          {
          __CrestLoad(36060, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(36059, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(36058, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2092 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(36061, 8343, 1);
            __CrestLoad(36063, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(36064, (unsigned long )(& __retres238));
# 2092 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2092 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(36062, 8345, 0);

          }
          }
          {
          __CrestLoad(36067, (unsigned long )(& old_buffer___7), (long long )((unsigned long )old_buffer___7));
          __CrestLoad(36066, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(36065, 13, (long long )((unsigned long )old_buffer___7 != (unsigned long )bufp->buffer));
# 2092 "regex.c"
          if ((unsigned long )old_buffer___7 != (unsigned long )bufp->buffer) {
            __CrestBranch(36068, 8347, 1);
# 2092 "regex.c"
            b = bufp->buffer + (b - old_buffer___7);
# 2092 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___7);
            {
            __CrestLoad(36072, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(36071, (unsigned long )0, (long long )0);
            __CrestApply2(36070, 13, (long long )(fixup_alt_jump != 0));
# 2092 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(36073, 8349, 1);
# 2092 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___7);
            } else {
              __CrestBranch(36074, 8350, 0);

            }
            }
            {
            __CrestLoad(36077, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(36076, (unsigned long )0, (long long )0);
            __CrestApply2(36075, 13, (long long )(laststart != 0));
# 2092 "regex.c"
            if (laststart != 0) {
              __CrestBranch(36078, 8352, 1);
# 2092 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___7);
            } else {
              __CrestBranch(36079, 8353, 0);

            }
            }
            {
            __CrestLoad(36082, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(36081, (unsigned long )0, (long long )0);
            __CrestApply2(36080, 13, (long long )(pending_exact != 0));
# 2092 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(36083, 8355, 1);
# 2092 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___7);
            } else {
              __CrestBranch(36084, 8356, 0);

            }
            }
          } else {
            __CrestBranch(36069, 8357, 0);

          }
          }
# 2092 "regex.c"
          goto while_break___26;
        }
        while_break___26: ;
        }
      }
      while_break___25: ;
      }
# 2092 "regex.c"
      tmp___27 = b;
# 2092 "regex.c"
      b ++;
      __CrestLoad(36085, (unsigned long )0, (long long )((unsigned char )((1 << 8) / 8)));
      __CrestStore(36086, (unsigned long )tmp___27);
# 2092 "regex.c"
      *tmp___27 = (unsigned char )((1 << 8) / 8);
# 2092 "regex.c"
      goto while_break___24;
    }
    while_break___24: ;
    }
    __CrestLoad(36087, (unsigned long )0, (long long )0);
    __CrestLoad(36088, (unsigned long )0, (long long )((size_t )((1 << 8) / 8)));
# 2095 "regex.c"
    memset((void *)b, 0, (size_t )((1 << 8) / 8));
    __CrestClearStack(36089);
    {
# 2098 "regex.c"
    mem_172 = b + -2;
    {
    __CrestLoad(36092, (unsigned long )mem_172, (long long )*mem_172);
    __CrestLoad(36091, (unsigned long )0, (long long )5U);
    __CrestApply2(36090, 12, (long long )((unsigned int )((re_opcode_t )*mem_172) == 5U));
# 2098 "regex.c"
    if ((unsigned int )((re_opcode_t )*mem_172) == 5U) {
      __CrestBranch(36093, 8368, 1);
      {
      __CrestLoad(36099, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(36098, (unsigned long )0, (long long )((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(36097, 5, (long long )(syntax & ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(36096, (unsigned long )0, (long long )0);
      __CrestApply2(36095, 13, (long long )((syntax & ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2098 "regex.c"
      if ((syntax & ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(36100, 8369, 1);
# 2100 "regex.c"
        mem_173 = b + 1;
# 2100 "regex.c"
        mem_174 = b + 1;
        __CrestLoad(36104, (unsigned long )mem_174, (long long )*mem_174);
        __CrestLoad(36103, (unsigned long )0, (long long )(1 << 2));
        __CrestApply2(36102, 6, (long long )((int )*mem_174 | (1 << 2)));
        __CrestStore(36105, (unsigned long )mem_173);
# 2100 "regex.c"
        *mem_173 = (unsigned char )((int )*mem_174 | (1 << 2));
      } else {
        __CrestBranch(36101, 8370, 0);

      }
      }
    } else {
      __CrestBranch(36094, 8371, 0);

    }
    }
    }
    {
# 2103 "regex.c"
    while (1) {
      while_continue___27: ;
      {
      __CrestLoad(36108, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(36107, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(36106, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2105 "regex.c"
      if ((unsigned long )p == (unsigned long )pend) {
        __CrestBranch(36109, 8376, 1);
# 2105 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(36111);
        __CrestLoad(36112, (unsigned long )0, (long long )((reg_errcode_t )7));
        __CrestStore(36113, (unsigned long )(& __retres238));
# 2105 "regex.c"
        __retres238 = (reg_errcode_t )7;
# 2105 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(36110, 8379, 0);

      }
      }
      {
# 2107 "regex.c"
      while (1) {
        while_continue___28: ;
        {
        __CrestLoad(36116, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(36115, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(36114, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2107 "regex.c"
        if ((unsigned long )p == (unsigned long )pend) {
          __CrestBranch(36117, 8384, 1);
          __CrestLoad(36119, (unsigned long )0, (long long )((reg_errcode_t )14));
          __CrestStore(36120, (unsigned long )(& __retres238));
# 2107 "regex.c"
          __retres238 = (reg_errcode_t )14;
# 2107 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(36118, 8386, 0);

        }
        }
# 2107 "regex.c"
        tmp___28 = p;
# 2107 "regex.c"
        p ++;
        __CrestLoad(36121, (unsigned long )tmp___28, (long long )*tmp___28);
        __CrestStore(36122, (unsigned long )(& c));
# 2107 "regex.c"
        c = (unsigned char )*tmp___28;
        {
        __CrestLoad(36125, (unsigned long )(& translate), (long long )((unsigned long )translate));
        __CrestLoad(36124, (unsigned long )0, (long long )0);
        __CrestApply2(36123, 13, (long long )(translate != 0));
# 2107 "regex.c"
        if (translate != 0) {
          __CrestBranch(36126, 8389, 1);
# 2107 "regex.c"
          mem_175 = translate + c;
          __CrestLoad(36128, (unsigned long )mem_175, (long long )*mem_175);
          __CrestStore(36129, (unsigned long )(& c));
# 2107 "regex.c"
          c = (unsigned char )*mem_175;
        } else {
          __CrestBranch(36127, 8390, 0);

        }
        }
# 2107 "regex.c"
        goto while_break___28;
      }
      while_break___28: ;
      }
      {
      __CrestLoad(36134, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(36133, (unsigned long )0, (long long )1UL);
      __CrestApply2(36132, 5, (long long )(syntax & 1UL));
      __CrestLoad(36131, (unsigned long )0, (long long )0);
      __CrestApply2(36130, 13, (long long )((syntax & 1UL) != 0));
# 2110 "regex.c"
      if ((syntax & 1UL) != 0) {
        __CrestBranch(36135, 8394, 1);
        {
        __CrestLoad(36139, (unsigned long )(& c), (long long )c);
        __CrestLoad(36138, (unsigned long )0, (long long )92);
        __CrestApply2(36137, 12, (long long )((int )c == 92));
# 2110 "regex.c"
        if ((int )c == 92) {
          __CrestBranch(36140, 8395, 1);
          {
          __CrestLoad(36144, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(36143, (unsigned long )(& pend), (long long )((unsigned long )pend));
          __CrestApply2(36142, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2112 "regex.c"
          if ((unsigned long )p == (unsigned long )pend) {
            __CrestBranch(36145, 8396, 1);
# 2112 "regex.c"
            free((void *)compile_stack.stack);
            __CrestClearStack(36147);
            __CrestLoad(36148, (unsigned long )0, (long long )((reg_errcode_t )5));
            __CrestStore(36149, (unsigned long )(& __retres238));
# 2112 "regex.c"
            __retres238 = (reg_errcode_t )5;
# 2112 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(36146, 8399, 0);

          }
          }
          {
# 2114 "regex.c"
          while (1) {
            while_continue___29: ;
            {
            __CrestLoad(36152, (unsigned long )(& p), (long long )((unsigned long )p));
            __CrestLoad(36151, (unsigned long )(& pend), (long long )((unsigned long )pend));
            __CrestApply2(36150, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2114 "regex.c"
            if ((unsigned long )p == (unsigned long )pend) {
              __CrestBranch(36153, 8404, 1);
              __CrestLoad(36155, (unsigned long )0, (long long )((reg_errcode_t )14));
              __CrestStore(36156, (unsigned long )(& __retres238));
# 2114 "regex.c"
              __retres238 = (reg_errcode_t )14;
# 2114 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(36154, 8406, 0);

            }
            }
# 2114 "regex.c"
            tmp___29 = p;
# 2114 "regex.c"
            p ++;
            __CrestLoad(36157, (unsigned long )tmp___29, (long long )*tmp___29);
            __CrestStore(36158, (unsigned long )(& c1));
# 2114 "regex.c"
            c1 = (unsigned char )*tmp___29;
            {
            __CrestLoad(36161, (unsigned long )(& translate), (long long )((unsigned long )translate));
            __CrestLoad(36160, (unsigned long )0, (long long )0);
            __CrestApply2(36159, 13, (long long )(translate != 0));
# 2114 "regex.c"
            if (translate != 0) {
              __CrestBranch(36162, 8409, 1);
# 2114 "regex.c"
              mem_176 = translate + c1;
              __CrestLoad(36164, (unsigned long )mem_176, (long long )*mem_176);
              __CrestStore(36165, (unsigned long )(& c1));
# 2114 "regex.c"
              c1 = (unsigned char )*mem_176;
            } else {
              __CrestBranch(36163, 8410, 0);

            }
            }
# 2114 "regex.c"
            goto while_break___29;
          }
          while_break___29: ;
          }
# 2115 "regex.c"
          mem_177 = b + (int )c1 / 8;
# 2115 "regex.c"
          mem_178 = b + (int )c1 / 8;
          __CrestLoad(36172, (unsigned long )mem_178, (long long )*mem_178);
          __CrestLoad(36171, (unsigned long )0, (long long )1);
          __CrestLoad(36170, (unsigned long )(& c1), (long long )c1);
          __CrestLoad(36169, (unsigned long )0, (long long )8);
          __CrestApply2(36168, 4, (long long )((int )c1 % 8));
          __CrestApply2(36167, 8, (long long )(1 << (int )c1 % 8));
          __CrestApply2(36166, 6, (long long )((int )*mem_178 | (1 << (int )c1 % 8)));
          __CrestStore(36173, (unsigned long )mem_177);
# 2115 "regex.c"
          *mem_177 = (unsigned char )((int )*mem_178 | (1 << (int )c1 % 8));
# 2116 "regex.c"
          goto __Cont;
        } else {
          __CrestBranch(36141, 8415, 0);

        }
        }
      } else {
        __CrestBranch(36136, 8416, 0);

      }
      }
      {
      __CrestLoad(36176, (unsigned long )(& c), (long long )c);
      __CrestLoad(36175, (unsigned long )0, (long long )93);
      __CrestApply2(36174, 12, (long long )((int )c == 93));
# 2122 "regex.c"
      if ((int )c == 93) {
        __CrestBranch(36177, 8418, 1);
        {
        __CrestLoad(36183, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(36182, (unsigned long )(& p1), (long long )((unsigned long )p1));
        __CrestLoad(36181, (unsigned long )0, (long long )1);
        __CrestApply2(36180, 18, (long long )((unsigned long )(p1 + 1)));
        __CrestApply2(36179, 13, (long long )((unsigned long )p != (unsigned long )(p1 + 1)));
# 2122 "regex.c"
        if ((unsigned long )p != (unsigned long )(p1 + 1)) {
          __CrestBranch(36184, 8419, 1);
# 2123 "regex.c"
          goto while_break___27;
        } else {
          __CrestBranch(36185, 8420, 0);

        }
        }
      } else {
        __CrestBranch(36178, 8421, 0);

      }
      }
      {
      __CrestLoad(36188, (unsigned long )(& had_char_class), (long long )had_char_class);
      __CrestLoad(36187, (unsigned long )0, (long long )0);
      __CrestApply2(36186, 13, (long long )(had_char_class != 0));
# 2127 "regex.c"
      if (had_char_class != 0) {
        __CrestBranch(36189, 8423, 1);
        {
        __CrestLoad(36193, (unsigned long )(& c), (long long )c);
        __CrestLoad(36192, (unsigned long )0, (long long )45);
        __CrestApply2(36191, 12, (long long )((int )c == 45));
# 2127 "regex.c"
        if ((int )c == 45) {
          __CrestBranch(36194, 8424, 1);
          {
          __CrestLoad(36198, (unsigned long )p, (long long )*p);
          __CrestLoad(36197, (unsigned long )0, (long long )93);
          __CrestApply2(36196, 13, (long long )((int const )*p != 93));
# 2127 "regex.c"
          if ((int const )*p != 93) {
            __CrestBranch(36199, 8425, 1);
# 2128 "regex.c"
            free((void *)compile_stack.stack);
            __CrestClearStack(36201);
            __CrestLoad(36202, (unsigned long )0, (long long )((reg_errcode_t )11));
            __CrestStore(36203, (unsigned long )(& __retres238));
# 2128 "regex.c"
            __retres238 = (reg_errcode_t )11;
# 2128 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(36200, 8428, 0);

          }
          }
        } else {
          __CrestBranch(36195, 8429, 0);

        }
        }
      } else {
        __CrestBranch(36190, 8430, 0);

      }
      }
      {
      __CrestLoad(36206, (unsigned long )(& c), (long long )c);
      __CrestLoad(36205, (unsigned long )0, (long long )45);
      __CrestApply2(36204, 12, (long long )((int )c == 45));
# 2134 "regex.c"
      if ((int )c == 45) {
        __CrestBranch(36207, 8432, 1);
        {
        __CrestLoad(36213, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(36212, (unsigned long )0, (long long )2);
        __CrestApply2(36211, 18, (long long )((unsigned long )(p - 2)));
        __CrestLoad(36210, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
        __CrestApply2(36209, 17, (long long )((unsigned long )(p - 2) >= (unsigned long )pattern));
# 2134 "regex.c"
        if ((unsigned long )(p - 2) >= (unsigned long )pattern) {
          __CrestBranch(36214, 8433, 1);
          {
# 2134 "regex.c"
          mem_179 = p + -2;
          {
          __CrestLoad(36218, (unsigned long )mem_179, (long long )*mem_179);
          __CrestLoad(36217, (unsigned long )0, (long long )91);
          __CrestApply2(36216, 12, (long long )((int const )*mem_179 == 91));
# 2134 "regex.c"
          if ((int const )*mem_179 == 91) {
            __CrestBranch(36219, 8436, 1);
# 2134 "regex.c"
            goto _L___10;
          } else {
            __CrestBranch(36220, 8437, 0);
# 2134 "regex.c"
            goto _L___11;
          }
          }
          }
        } else {
          __CrestBranch(36215, 8438, 0);
          _L___11:
          {
          __CrestLoad(36225, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(36224, (unsigned long )0, (long long )3);
          __CrestApply2(36223, 18, (long long )((unsigned long )(p - 3)));
          __CrestLoad(36222, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
          __CrestApply2(36221, 17, (long long )((unsigned long )(p - 3) >= (unsigned long )pattern));
# 2134 "regex.c"
          if ((unsigned long )(p - 3) >= (unsigned long )pattern) {
            __CrestBranch(36226, 8439, 1);
            {
# 2134 "regex.c"
            mem_180 = p + -3;
            {
            __CrestLoad(36230, (unsigned long )mem_180, (long long )*mem_180);
            __CrestLoad(36229, (unsigned long )0, (long long )91);
            __CrestApply2(36228, 12, (long long )((int const )*mem_180 == 91));
# 2134 "regex.c"
            if ((int const )*mem_180 == 91) {
              __CrestBranch(36231, 8442, 1);
              {
# 2134 "regex.c"
              mem_181 = p + -2;
              {
              __CrestLoad(36235, (unsigned long )mem_181, (long long )*mem_181);
              __CrestLoad(36234, (unsigned long )0, (long long )94);
              __CrestApply2(36233, 12, (long long )((int const )*mem_181 == 94));
# 2134 "regex.c"
              if ((int const )*mem_181 == 94) {
                __CrestBranch(36236, 8445, 1);
# 2134 "regex.c"
                goto _L___10;
              } else {
                __CrestBranch(36237, 8446, 0);
# 2134 "regex.c"
                goto _L___9;
              }
              }
              }
            } else {
              __CrestBranch(36232, 8447, 0);
# 2134 "regex.c"
              goto _L___9;
            }
            }
            }
          } else {
            __CrestBranch(36227, 8448, 0);
            _L___9:
            {
            __CrestLoad(36240, (unsigned long )p, (long long )*p);
            __CrestLoad(36239, (unsigned long )0, (long long )93);
            __CrestApply2(36238, 13, (long long )((int const )*p != 93));
# 2134 "regex.c"
            if ((int const )*p != 93) {
              __CrestBranch(36241, 8449, 1);
              __CrestLoad(36243, (unsigned long )(& syntax), (long long )syntax);
# 2139 "regex.c"
              tmp___30 = compile_range(& p, pend, translate, syntax, b);
              __CrestHandleReturn(36245, (long long )tmp___30);
              __CrestStore(36244, (unsigned long )(& tmp___30));
              __CrestLoad(36246, (unsigned long )(& tmp___30), (long long )tmp___30);
              __CrestStore(36247, (unsigned long )(& ret));
# 2139 "regex.c"
              ret = tmp___30;
              {
              __CrestLoad(36250, (unsigned long )(& ret), (long long )ret);
              __CrestLoad(36249, (unsigned long )0, (long long )0U);
              __CrestApply2(36248, 13, (long long )((unsigned int )ret != 0U));
# 2141 "regex.c"
              if ((unsigned int )ret != 0U) {
                __CrestBranch(36251, 8451, 1);
# 2141 "regex.c"
                free((void *)compile_stack.stack);
                __CrestClearStack(36253);
                __CrestLoad(36254, (unsigned long )(& ret), (long long )ret);
                __CrestStore(36255, (unsigned long )(& __retres238));
# 2141 "regex.c"
                __retres238 = ret;
# 2141 "regex.c"
                goto return_label;
              } else {
                __CrestBranch(36252, 8454, 0);

              }
              }
            } else {
              __CrestBranch(36242, 8455, 0);
# 2134 "regex.c"
              goto _L___10;
            }
            }
          }
          }
        }
        }
      } else {
        __CrestBranch(36208, 8456, 0);
        _L___10:
        {
# 2144 "regex.c"
        mem_182 = p + 0;
        {
        __CrestLoad(36258, (unsigned long )mem_182, (long long )*mem_182);
        __CrestLoad(36257, (unsigned long )0, (long long )45);
        __CrestApply2(36256, 12, (long long )((int const )*mem_182 == 45));
# 2144 "regex.c"
        if ((int const )*mem_182 == 45) {
          __CrestBranch(36259, 8459, 1);
          {
# 2144 "regex.c"
          mem_183 = p + 1;
          {
          __CrestLoad(36263, (unsigned long )mem_183, (long long )*mem_183);
          __CrestLoad(36262, (unsigned long )0, (long long )93);
          __CrestApply2(36261, 13, (long long )((int const )*mem_183 != 93));
# 2144 "regex.c"
          if ((int const )*mem_183 != 93) {
            __CrestBranch(36264, 8462, 1);
            {
# 2149 "regex.c"
            while (1) {
              while_continue___30: ;
              {
              __CrestLoad(36268, (unsigned long )(& p), (long long )((unsigned long )p));
              __CrestLoad(36267, (unsigned long )(& pend), (long long )((unsigned long )pend));
              __CrestApply2(36266, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2149 "regex.c"
              if ((unsigned long )p == (unsigned long )pend) {
                __CrestBranch(36269, 8466, 1);
                __CrestLoad(36271, (unsigned long )0, (long long )((reg_errcode_t )14));
                __CrestStore(36272, (unsigned long )(& __retres238));
# 2149 "regex.c"
                __retres238 = (reg_errcode_t )14;
# 2149 "regex.c"
                goto return_label;
              } else {
                __CrestBranch(36270, 8468, 0);

              }
              }
# 2149 "regex.c"
              tmp___31 = p;
# 2149 "regex.c"
              p ++;
              __CrestLoad(36273, (unsigned long )tmp___31, (long long )*tmp___31);
              __CrestStore(36274, (unsigned long )(& c1));
# 2149 "regex.c"
              c1 = (unsigned char )*tmp___31;
              {
              __CrestLoad(36277, (unsigned long )(& translate), (long long )((unsigned long )translate));
              __CrestLoad(36276, (unsigned long )0, (long long )0);
              __CrestApply2(36275, 13, (long long )(translate != 0));
# 2149 "regex.c"
              if (translate != 0) {
                __CrestBranch(36278, 8471, 1);
# 2149 "regex.c"
                mem_184 = translate + c1;
                __CrestLoad(36280, (unsigned long )mem_184, (long long )*mem_184);
                __CrestStore(36281, (unsigned long )(& c1));
# 2149 "regex.c"
                c1 = (unsigned char )*mem_184;
              } else {
                __CrestBranch(36279, 8472, 0);

              }
              }
# 2149 "regex.c"
              goto while_break___30;
            }
            while_break___30: ;
            }
            __CrestLoad(36282, (unsigned long )(& syntax), (long long )syntax);
# 2151 "regex.c"
            ret___0 = compile_range(& p, pend, translate, syntax, b);
            __CrestHandleReturn(36284, (long long )ret___0);
            __CrestStore(36283, (unsigned long )(& ret___0));
            {
            __CrestLoad(36287, (unsigned long )(& ret___0), (long long )ret___0);
            __CrestLoad(36286, (unsigned long )0, (long long )0U);
            __CrestApply2(36285, 13, (long long )((unsigned int )ret___0 != 0U));
# 2152 "regex.c"
            if ((unsigned int )ret___0 != 0U) {
              __CrestBranch(36288, 8477, 1);
# 2152 "regex.c"
              free((void *)compile_stack.stack);
              __CrestClearStack(36290);
              __CrestLoad(36291, (unsigned long )(& ret___0), (long long )ret___0);
              __CrestStore(36292, (unsigned long )(& __retres238));
# 2152 "regex.c"
              __retres238 = ret___0;
# 2152 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(36289, 8480, 0);

            }
            }
          } else {
            __CrestBranch(36265, 8481, 0);
# 2144 "regex.c"
            goto _L___5;
          }
          }
          }
        } else {
          __CrestBranch(36260, 8482, 0);
          _L___5:
          {
          __CrestLoad(36297, (unsigned long )(& syntax), (long long )syntax);
          __CrestLoad(36296, (unsigned long )0, (long long )((1UL << 1) << 1));
          __CrestApply2(36295, 5, (long long )(syntax & ((1UL << 1) << 1)));
          __CrestLoad(36294, (unsigned long )0, (long long )0);
          __CrestApply2(36293, 13, (long long )((syntax & ((1UL << 1) << 1)) != 0));
# 2158 "regex.c"
          if ((syntax & ((1UL << 1) << 1)) != 0) {
            __CrestBranch(36298, 8483, 1);
            {
            __CrestLoad(36302, (unsigned long )(& c), (long long )c);
            __CrestLoad(36301, (unsigned long )0, (long long )91);
            __CrestApply2(36300, 12, (long long )((int )c == 91));
# 2158 "regex.c"
            if ((int )c == 91) {
              __CrestBranch(36303, 8484, 1);
              {
              __CrestLoad(36307, (unsigned long )p, (long long )*p);
              __CrestLoad(36306, (unsigned long )0, (long long )58);
              __CrestApply2(36305, 12, (long long )((int const )*p == 58));
# 2158 "regex.c"
              if ((int const )*p == 58) {
                __CrestBranch(36308, 8485, 1);
                {
# 2162 "regex.c"
                while (1) {
                  while_continue___31: ;
                  {
                  __CrestLoad(36312, (unsigned long )(& p), (long long )((unsigned long )p));
                  __CrestLoad(36311, (unsigned long )(& pend), (long long )((unsigned long )pend));
                  __CrestApply2(36310, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2162 "regex.c"
                  if ((unsigned long )p == (unsigned long )pend) {
                    __CrestBranch(36313, 8489, 1);
                    __CrestLoad(36315, (unsigned long )0, (long long )((reg_errcode_t )14));
                    __CrestStore(36316, (unsigned long )(& __retres238));
# 2162 "regex.c"
                    __retres238 = (reg_errcode_t )14;
# 2162 "regex.c"
                    goto return_label;
                  } else {
                    __CrestBranch(36314, 8491, 0);

                  }
                  }
# 2162 "regex.c"
                  tmp___32 = p;
# 2162 "regex.c"
                  p ++;
                  __CrestLoad(36317, (unsigned long )tmp___32, (long long )*tmp___32);
                  __CrestStore(36318, (unsigned long )(& c));
# 2162 "regex.c"
                  c = (unsigned char )*tmp___32;
                  {
                  __CrestLoad(36321, (unsigned long )(& translate), (long long )((unsigned long )translate));
                  __CrestLoad(36320, (unsigned long )0, (long long )0);
                  __CrestApply2(36319, 13, (long long )(translate != 0));
# 2162 "regex.c"
                  if (translate != 0) {
                    __CrestBranch(36322, 8494, 1);
# 2162 "regex.c"
                    mem_185 = translate + c;
                    __CrestLoad(36324, (unsigned long )mem_185, (long long )*mem_185);
                    __CrestStore(36325, (unsigned long )(& c));
# 2162 "regex.c"
                    c = (unsigned char )*mem_185;
                  } else {
                    __CrestBranch(36323, 8495, 0);

                  }
                  }
# 2162 "regex.c"
                  goto while_break___31;
                }
                while_break___31: ;
                }
                __CrestLoad(36326, (unsigned long )0, (long long )(unsigned char)0);
                __CrestStore(36327, (unsigned long )(& c1));
# 2163 "regex.c"
                c1 = (unsigned char)0;
                {
                __CrestLoad(36330, (unsigned long )(& p), (long long )((unsigned long )p));
                __CrestLoad(36329, (unsigned long )(& pend), (long long )((unsigned long )pend));
                __CrestApply2(36328, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2166 "regex.c"
                if ((unsigned long )p == (unsigned long )pend) {
                  __CrestBranch(36331, 8500, 1);
# 2166 "regex.c"
                  free((void *)compile_stack.stack);
                  __CrestClearStack(36333);
                  __CrestLoad(36334, (unsigned long )0, (long long )((reg_errcode_t )7));
                  __CrestStore(36335, (unsigned long )(& __retres238));
# 2166 "regex.c"
                  __retres238 = (reg_errcode_t )7;
# 2166 "regex.c"
                  goto return_label;
                } else {
                  __CrestBranch(36332, 8503, 0);

                }
                }
                {
# 2168 "regex.c"
                while (1) {
                  while_continue___32: ;
                  {
# 2170 "regex.c"
                  while (1) {
                    while_continue___33: ;
                    {
                    __CrestLoad(36338, (unsigned long )(& p), (long long )((unsigned long )p));
                    __CrestLoad(36337, (unsigned long )(& pend), (long long )((unsigned long )pend));
                    __CrestApply2(36336, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2170 "regex.c"
                    if ((unsigned long )p == (unsigned long )pend) {
                      __CrestBranch(36339, 8511, 1);
                      __CrestLoad(36341, (unsigned long )0, (long long )((reg_errcode_t )14));
                      __CrestStore(36342, (unsigned long )(& __retres238));
# 2170 "regex.c"
                      __retres238 = (reg_errcode_t )14;
# 2170 "regex.c"
                      goto return_label;
                    } else {
                      __CrestBranch(36340, 8513, 0);

                    }
                    }
# 2170 "regex.c"
                    tmp___33 = p;
# 2170 "regex.c"
                    p ++;
                    __CrestLoad(36343, (unsigned long )tmp___33, (long long )*tmp___33);
                    __CrestStore(36344, (unsigned long )(& c));
# 2170 "regex.c"
                    c = (unsigned char )*tmp___33;
                    {
                    __CrestLoad(36347, (unsigned long )(& translate), (long long )((unsigned long )translate));
                    __CrestLoad(36346, (unsigned long )0, (long long )0);
                    __CrestApply2(36345, 13, (long long )(translate != 0));
# 2170 "regex.c"
                    if (translate != 0) {
                      __CrestBranch(36348, 8516, 1);
# 2170 "regex.c"
                      mem_186 = translate + c;
                      __CrestLoad(36350, (unsigned long )mem_186, (long long )*mem_186);
                      __CrestStore(36351, (unsigned long )(& c));
# 2170 "regex.c"
                      c = (unsigned char )*mem_186;
                    } else {
                      __CrestBranch(36349, 8517, 0);

                    }
                    }
# 2170 "regex.c"
                    goto while_break___33;
                  }
                  while_break___33: ;
                  }
                  {
                  __CrestLoad(36354, (unsigned long )(& c), (long long )c);
                  __CrestLoad(36353, (unsigned long )0, (long long )58);
                  __CrestApply2(36352, 12, (long long )((int )c == 58));
# 2171 "regex.c"
                  if ((int )c == 58) {
                    __CrestBranch(36355, 8521, 1);
                    {
                    __CrestLoad(36359, (unsigned long )p, (long long )*p);
                    __CrestLoad(36358, (unsigned long )0, (long long )93);
                    __CrestApply2(36357, 12, (long long )((int const )*p == 93));
# 2171 "regex.c"
                    if ((int const )*p == 93) {
                      __CrestBranch(36360, 8522, 1);
# 2173 "regex.c"
                      goto while_break___32;
                    } else {
                      __CrestBranch(36361, 8523, 0);
# 2171 "regex.c"
                      goto _L___2;
                    }
                    }
                  } else {
                    __CrestBranch(36356, 8524, 0);
                    _L___2:
                    {
                    __CrestLoad(36364, (unsigned long )(& p), (long long )((unsigned long )p));
                    __CrestLoad(36363, (unsigned long )(& pend), (long long )((unsigned long )pend));
                    __CrestApply2(36362, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2171 "regex.c"
                    if ((unsigned long )p == (unsigned long )pend) {
                      __CrestBranch(36365, 8525, 1);
# 2173 "regex.c"
                      goto while_break___32;
                    } else {
                      __CrestBranch(36366, 8526, 0);
                      {
                      __CrestLoad(36369, (unsigned long )(& c1), (long long )c1);
                      __CrestLoad(36368, (unsigned long )0, (long long )256);
                      __CrestApply2(36367, 12, (long long )((int )c1 == 256));
# 2171 "regex.c"
                      if ((int )c1 == 256) {
                        __CrestBranch(36370, 8527, 1);
# 2173 "regex.c"
                        goto while_break___32;
                      } else {
                        __CrestBranch(36371, 8528, 0);

                      }
                      }
                    }
                    }
                  }
                  }
                  __CrestLoad(36372, (unsigned long )(& c1), (long long )c1);
                  __CrestStore(36373, (unsigned long )(& tmp___34));
# 2174 "regex.c"
                  tmp___34 = c1;
                  __CrestLoad(36376, (unsigned long )(& c1), (long long )c1);
                  __CrestLoad(36375, (unsigned long )0, (long long )1);
                  __CrestApply2(36374, 0, (long long )((int )c1 + 1));
                  __CrestStore(36377, (unsigned long )(& c1));
# 2174 "regex.c"
                  c1 = (unsigned char )((int )c1 + 1);
                  __CrestLoad(36378, (unsigned long )(& c), (long long )c);
                  __CrestStore(36379, (unsigned long )(& str[tmp___34]));
# 2174 "regex.c"
                  str[tmp___34] = (char )c;
                }
                while_break___32: ;
                }
                __CrestLoad(36380, (unsigned long )0, (long long )((char )'\000'));
                __CrestStore(36381, (unsigned long )(& str[c1]));
# 2176 "regex.c"
                str[c1] = (char )'\000';
                {
                __CrestLoad(36384, (unsigned long )(& c), (long long )c);
                __CrestLoad(36383, (unsigned long )0, (long long )58);
                __CrestApply2(36382, 12, (long long )((int )c == 58));
# 2181 "regex.c"
                if ((int )c == 58) {
                  __CrestBranch(36385, 8533, 1);
                  {
                  __CrestLoad(36389, (unsigned long )p, (long long )*p);
                  __CrestLoad(36388, (unsigned long )0, (long long )93);
                  __CrestApply2(36387, 12, (long long )((int const )*p == 93));
# 2181 "regex.c"
                  if ((int const )*p == 93) {
                    __CrestBranch(36390, 8534, 1);
# 2184 "regex.c"
                    tmp___35 = strcmp((char const *)(str), "lower");
                    __CrestHandleReturn(36393, (long long )tmp___35);
                    __CrestStore(36392, (unsigned long )(& tmp___35));
                    __CrestLoad(36396, (unsigned long )(& tmp___35), (long long )tmp___35);
                    __CrestLoad(36395, (unsigned long )0, (long long )0);
                    __CrestApply2(36394, 12, (long long )(tmp___35 == 0));
                    __CrestStore(36397, (unsigned long )(& is_lower));
# 2184 "regex.c"
                    is_lower = (boolean )(tmp___35 == 0);
# 2185 "regex.c"
                    tmp___36 = strcmp((char const *)(str), "upper");
                    __CrestHandleReturn(36399, (long long )tmp___36);
                    __CrestStore(36398, (unsigned long )(& tmp___36));
                    __CrestLoad(36402, (unsigned long )(& tmp___36), (long long )tmp___36);
                    __CrestLoad(36401, (unsigned long )0, (long long )0);
                    __CrestApply2(36400, 12, (long long )(tmp___36 == 0));
                    __CrestStore(36403, (unsigned long )(& is_upper));
# 2185 "regex.c"
                    is_upper = (boolean )(tmp___36 == 0);
# 2189 "regex.c"
                    wt = wctype((char const *)(str));
                    __CrestHandleReturn(36405, (long long )wt);
                    __CrestStore(36404, (unsigned long )(& wt));
                    {
                    __CrestLoad(36408, (unsigned long )(& wt), (long long )wt);
                    __CrestLoad(36407, (unsigned long )0, (long long )0UL);
                    __CrestApply2(36406, 12, (long long )(wt == 0UL));
# 2190 "regex.c"
                    if (wt == 0UL) {
                      __CrestBranch(36409, 8536, 1);
# 2191 "regex.c"
                      free((void *)compile_stack.stack);
                      __CrestClearStack(36411);
                      __CrestLoad(36412, (unsigned long )0, (long long )((reg_errcode_t )4));
                      __CrestStore(36413, (unsigned long )(& __retres238));
# 2191 "regex.c"
                      __retres238 = (reg_errcode_t )4;
# 2191 "regex.c"
                      goto return_label;
                    } else {
                      __CrestBranch(36410, 8539, 0);

                    }
                    }
                    {
# 2195 "regex.c"
                    while (1) {
                      while_continue___34: ;
                      {
                      __CrestLoad(36416, (unsigned long )(& p), (long long )((unsigned long )p));
                      __CrestLoad(36415, (unsigned long )(& pend), (long long )((unsigned long )pend));
                      __CrestApply2(36414, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2195 "regex.c"
                      if ((unsigned long )p == (unsigned long )pend) {
                        __CrestBranch(36417, 8544, 1);
                        __CrestLoad(36419, (unsigned long )0, (long long )((reg_errcode_t )14));
                        __CrestStore(36420, (unsigned long )(& __retres238));
# 2195 "regex.c"
                        __retres238 = (reg_errcode_t )14;
# 2195 "regex.c"
                        goto return_label;
                      } else {
                        __CrestBranch(36418, 8546, 0);

                      }
                      }
# 2195 "regex.c"
                      tmp___37 = p;
# 2195 "regex.c"
                      p ++;
                      __CrestLoad(36421, (unsigned long )tmp___37, (long long )*tmp___37);
                      __CrestStore(36422, (unsigned long )(& c));
# 2195 "regex.c"
                      c = (unsigned char )*tmp___37;
                      {
                      __CrestLoad(36425, (unsigned long )(& translate), (long long )((unsigned long )translate));
                      __CrestLoad(36424, (unsigned long )0, (long long )0);
                      __CrestApply2(36423, 13, (long long )(translate != 0));
# 2195 "regex.c"
                      if (translate != 0) {
                        __CrestBranch(36426, 8549, 1);
# 2195 "regex.c"
                        mem_187 = translate + c;
                        __CrestLoad(36428, (unsigned long )mem_187, (long long )*mem_187);
                        __CrestStore(36429, (unsigned long )(& c));
# 2195 "regex.c"
                        c = (unsigned char )*mem_187;
                      } else {
                        __CrestBranch(36427, 8550, 0);

                      }
                      }
# 2195 "regex.c"
                      goto while_break___34;
                    }
                    while_break___34: ;
                    }
                    {
                    __CrestLoad(36432, (unsigned long )(& p), (long long )((unsigned long )p));
                    __CrestLoad(36431, (unsigned long )(& pend), (long long )((unsigned long )pend));
                    __CrestApply2(36430, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2197 "regex.c"
                    if ((unsigned long )p == (unsigned long )pend) {
                      __CrestBranch(36433, 8554, 1);
# 2197 "regex.c"
                      free((void *)compile_stack.stack);
                      __CrestClearStack(36435);
                      __CrestLoad(36436, (unsigned long )0, (long long )((reg_errcode_t )7));
                      __CrestStore(36437, (unsigned long )(& __retres238));
# 2197 "regex.c"
                      __retres238 = (reg_errcode_t )7;
# 2197 "regex.c"
                      goto return_label;
                    } else {
                      __CrestBranch(36434, 8557, 0);

                    }
                    }
                    __CrestLoad(36438, (unsigned long )0, (long long )0);
                    __CrestStore(36439, (unsigned long )(& ch));
# 2199 "regex.c"
                    ch = 0;
                    {
# 2199 "regex.c"
                    while (1) {
                      while_continue___35: ;
                      {
                      __CrestLoad(36442, (unsigned long )(& ch), (long long )ch);
                      __CrestLoad(36441, (unsigned long )0, (long long )(1 << 8));
                      __CrestApply2(36440, 16, (long long )(ch < 1 << 8));
# 2199 "regex.c"
                      if (ch < 1 << 8) {
                        __CrestBranch(36443, 8563, 1);

                      } else {
                        __CrestBranch(36444, 8564, 0);
# 2199 "regex.c"
                        goto while_break___35;
                      }
                      }
                      __CrestLoad(36445, (unsigned long )(& ch), (long long )ch);
# 2201 "regex.c"
                      tmp___38 = btowc(ch);
                      __CrestHandleReturn(36447, (long long )tmp___38);
                      __CrestStore(36446, (unsigned long )(& tmp___38));
                      __CrestLoad(36448, (unsigned long )(& tmp___38), (long long )tmp___38);
                      __CrestLoad(36449, (unsigned long )(& wt), (long long )wt);
# 2201 "regex.c"
                      tmp___39 = iswctype(tmp___38, wt);
                      __CrestHandleReturn(36451, (long long )tmp___39);
                      __CrestStore(36450, (unsigned long )(& tmp___39));
                      {
                      __CrestLoad(36454, (unsigned long )(& tmp___39), (long long )tmp___39);
                      __CrestLoad(36453, (unsigned long )0, (long long )0);
                      __CrestApply2(36452, 13, (long long )(tmp___39 != 0));
# 2201 "regex.c"
                      if (tmp___39 != 0) {
                        __CrestBranch(36455, 8567, 1);
# 2202 "regex.c"
                        mem_188 = b + (int )((unsigned char )ch) / 8;
# 2202 "regex.c"
                        mem_189 = b + (int )((unsigned char )ch) / 8;
                        __CrestLoad(36463, (unsigned long )mem_189, (long long )*mem_189);
                        __CrestLoad(36462, (unsigned long )0, (long long )1);
                        __CrestLoad(36461, (unsigned long )(& ch), (long long )ch);
                        __CrestLoad(36460, (unsigned long )0, (long long )8);
                        __CrestApply2(36459, 4, (long long )((int )((unsigned char )ch) % 8));
                        __CrestApply2(36458, 8, (long long )(1 << (int )((unsigned char )ch) % 8));
                        __CrestApply2(36457, 6, (long long )((int )*mem_189 | (1 << (int )((unsigned char )ch) % 8)));
                        __CrestStore(36464, (unsigned long )mem_188);
# 2202 "regex.c"
                        *mem_188 = (unsigned char )((int )*mem_189 | (1 << (int )((unsigned char )ch) % 8));
                      } else {
                        __CrestBranch(36456, 8568, 0);

                      }
                      }
                      {
                      __CrestLoad(36467, (unsigned long )(& translate), (long long )((unsigned long )translate));
                      __CrestLoad(36466, (unsigned long )0, (long long )0);
                      __CrestApply2(36465, 13, (long long )(translate != 0));
# 2204 "regex.c"
                      if (translate != 0) {
                        __CrestBranch(36468, 8570, 1);
                        {
                        __CrestLoad(36472, (unsigned long )(& is_upper), (long long )is_upper);
                        __CrestLoad(36471, (unsigned long )0, (long long )0);
                        __CrestApply2(36470, 13, (long long )(is_upper != 0));
# 2204 "regex.c"
                        if (is_upper != 0) {
                          __CrestBranch(36473, 8571, 1);
# 2204 "regex.c"
                          goto _L___3;
                        } else {
                          __CrestBranch(36474, 8572, 0);
                          {
                          __CrestLoad(36477, (unsigned long )(& is_lower), (long long )is_lower);
                          __CrestLoad(36476, (unsigned long )0, (long long )0);
                          __CrestApply2(36475, 13, (long long )(is_lower != 0));
# 2204 "regex.c"
                          if (is_lower != 0) {
                            __CrestBranch(36478, 8573, 1);
                            _L___3:
# 2204 "regex.c"
                            tmp___40 = __ctype_b_loc();
                            __CrestClearStack(36480);
                            {
# 2204 "regex.c"
                            mem_190 = *tmp___40 + ch;
                            {
                            __CrestLoad(36485, (unsigned long )mem_190, (long long )*mem_190);
                            __CrestLoad(36484, (unsigned long )0, (long long )256);
                            __CrestApply2(36483, 5, (long long )((int const )*mem_190 & 256));
                            __CrestLoad(36482, (unsigned long )0, (long long )0);
                            __CrestApply2(36481, 13, (long long )(((int const )*mem_190 & 256) != 0));
# 2204 "regex.c"
                            if (((int const )*mem_190 & 256) != 0) {
                              __CrestBranch(36486, 8577, 1);
# 2206 "regex.c"
                              mem_191 = b + (int )((unsigned char )ch) / 8;
# 2206 "regex.c"
                              mem_192 = b + (int )((unsigned char )ch) / 8;
                              __CrestLoad(36494, (unsigned long )mem_192, (long long )*mem_192);
                              __CrestLoad(36493, (unsigned long )0, (long long )1);
                              __CrestLoad(36492, (unsigned long )(& ch), (long long )ch);
                              __CrestLoad(36491, (unsigned long )0, (long long )8);
                              __CrestApply2(36490, 4, (long long )((int )((unsigned char )ch) % 8));
                              __CrestApply2(36489, 8, (long long )(1 << (int )((unsigned char )ch) % 8));
                              __CrestApply2(36488, 6, (long long )((int )*mem_192 | (1 << (int )((unsigned char )ch) % 8)));
                              __CrestStore(36495, (unsigned long )mem_191);
# 2206 "regex.c"
                              *mem_191 = (unsigned char )((int )*mem_192 | (1 << (int )((unsigned char )ch) % 8));
                            } else {
                              __CrestBranch(36487, 8578, 0);
# 2204 "regex.c"
                              tmp___41 = __ctype_b_loc();
                              __CrestClearStack(36496);
                              {
# 2204 "regex.c"
                              mem_193 = *tmp___41 + ch;
                              {
                              __CrestLoad(36501, (unsigned long )mem_193, (long long )*mem_193);
                              __CrestLoad(36500, (unsigned long )0, (long long )512);
                              __CrestApply2(36499, 5, (long long )((int const )*mem_193 & 512));
                              __CrestLoad(36498, (unsigned long )0, (long long )0);
                              __CrestApply2(36497, 13, (long long )(((int const )*mem_193 & 512) != 0));
# 2204 "regex.c"
                              if (((int const )*mem_193 & 512) != 0) {
                                __CrestBranch(36502, 8582, 1);
# 2206 "regex.c"
                                mem_194 = b + (int )((unsigned char )ch) / 8;
# 2206 "regex.c"
                                mem_195 = b + (int )((unsigned char )ch) / 8;
                                __CrestLoad(36510, (unsigned long )mem_195, (long long )*mem_195);
                                __CrestLoad(36509, (unsigned long )0, (long long )1);
                                __CrestLoad(36508, (unsigned long )(& ch), (long long )ch);
                                __CrestLoad(36507, (unsigned long )0, (long long )8);
                                __CrestApply2(36506, 4, (long long )((int )((unsigned char )ch) % 8));
                                __CrestApply2(36505, 8, (long long )(1 << (int )((unsigned char )ch) % 8));
                                __CrestApply2(36504, 6, (long long )((int )*mem_195 | (1 << (int )((unsigned char )ch) % 8)));
                                __CrestStore(36511, (unsigned long )mem_194);
# 2206 "regex.c"
                                *mem_194 = (unsigned char )((int )*mem_195 | (1 << (int )((unsigned char )ch) % 8));
                              } else {
                                __CrestBranch(36503, 8583, 0);

                              }
                              }
                              }
                            }
                            }
                            }
                          } else {
                            __CrestBranch(36479, 8584, 0);

                          }
                          }
                        }
                        }
                      } else {
                        __CrestBranch(36469, 8585, 0);

                      }
                      }
                      __CrestLoad(36514, (unsigned long )(& ch), (long long )ch);
                      __CrestLoad(36513, (unsigned long )0, (long long )1);
                      __CrestApply2(36512, 0, (long long )(ch + 1));
                      __CrestStore(36515, (unsigned long )(& ch));
# 2199 "regex.c"
                      ch ++;
                    }
                    while_break___35: ;
                    }
                    __CrestLoad(36516, (unsigned long )0, (long long )((boolean )1));
                    __CrestStore(36517, (unsigned long )(& had_char_class));
# 2209 "regex.c"
                    had_char_class = (boolean )1;
                  } else {
                    __CrestBranch(36391, 8589, 0);
# 2181 "regex.c"
                    goto _L___4;
                  }
                  }
                } else {
                  __CrestBranch(36386, 8590, 0);
                  _L___4:
                  __CrestLoad(36520, (unsigned long )(& c1), (long long )c1);
                  __CrestLoad(36519, (unsigned long )0, (long long )1);
                  __CrestApply2(36518, 0, (long long )((int )c1 + 1));
                  __CrestStore(36521, (unsigned long )(& c1));
# 2262 "regex.c"
                  c1 = (unsigned char )((int )c1 + 1);
                  {
# 2263 "regex.c"
                  while (1) {
                    while_continue___36: ;
                    __CrestLoad(36522, (unsigned long )(& c1), (long long )c1);
                    __CrestStore(36523, (unsigned long )(& tmp___42));
# 2263 "regex.c"
                    tmp___42 = c1;
                    __CrestLoad(36526, (unsigned long )(& c1), (long long )c1);
                    __CrestLoad(36525, (unsigned long )0, (long long )1);
                    __CrestApply2(36524, 1, (long long )((int )c1 - 1));
                    __CrestStore(36527, (unsigned long )(& c1));
# 2263 "regex.c"
                    c1 = (unsigned char )((int )c1 - 1);
                    {
                    __CrestLoad(36530, (unsigned long )(& tmp___42), (long long )tmp___42);
                    __CrestLoad(36529, (unsigned long )0, (long long )0);
                    __CrestApply2(36528, 13, (long long )(tmp___42 != 0));
# 2263 "regex.c"
                    if (tmp___42 != 0) {
                      __CrestBranch(36531, 8596, 1);

                    } else {
                      __CrestBranch(36532, 8597, 0);
# 2263 "regex.c"
                      goto while_break___36;
                    }
                    }
# 2264 "regex.c"
                    p --;
                  }
                  while_break___36: ;
                  }
# 2265 "regex.c"
                  mem_196 = b + 11;
# 2265 "regex.c"
                  mem_197 = b + 11;
                  __CrestLoad(36535, (unsigned long )mem_197, (long long )*mem_197);
                  __CrestLoad(36534, (unsigned long )0, (long long )(1 << 3));
                  __CrestApply2(36533, 6, (long long )((int )*mem_197 | (1 << 3)));
                  __CrestStore(36536, (unsigned long )mem_196);
# 2265 "regex.c"
                  *mem_196 = (unsigned char )((int )*mem_197 | (1 << 3));
# 2266 "regex.c"
                  mem_198 = b + 7;
# 2266 "regex.c"
                  mem_199 = b + 7;
                  __CrestLoad(36539, (unsigned long )mem_199, (long long )*mem_199);
                  __CrestLoad(36538, (unsigned long )0, (long long )(1 << 2));
                  __CrestApply2(36537, 6, (long long )((int )*mem_199 | (1 << 2)));
                  __CrestStore(36540, (unsigned long )mem_198);
# 2266 "regex.c"
                  *mem_198 = (unsigned char )((int )*mem_199 | (1 << 2));
                  __CrestLoad(36541, (unsigned long )0, (long long )((boolean )0));
                  __CrestStore(36542, (unsigned long )(& had_char_class));
# 2267 "regex.c"
                  had_char_class = (boolean )0;
                }
                }
              } else {
                __CrestBranch(36309, 8601, 0);
                __CrestLoad(36543, (unsigned long )0, (long long )((boolean )0));
                __CrestStore(36544, (unsigned long )(& had_char_class));
# 2272 "regex.c"
                had_char_class = (boolean )0;
# 2273 "regex.c"
                mem_200 = b + (int )c / 8;
# 2273 "regex.c"
                mem_201 = b + (int )c / 8;
                __CrestLoad(36551, (unsigned long )mem_201, (long long )*mem_201);
                __CrestLoad(36550, (unsigned long )0, (long long )1);
                __CrestLoad(36549, (unsigned long )(& c), (long long )c);
                __CrestLoad(36548, (unsigned long )0, (long long )8);
                __CrestApply2(36547, 4, (long long )((int )c % 8));
                __CrestApply2(36546, 8, (long long )(1 << (int )c % 8));
                __CrestApply2(36545, 6, (long long )((int )*mem_201 | (1 << (int )c % 8)));
                __CrestStore(36552, (unsigned long )mem_200);
# 2273 "regex.c"
                *mem_200 = (unsigned char )((int )*mem_201 | (1 << (int )c % 8));
              }
              }
            } else {
              __CrestBranch(36304, 8602, 0);
              __CrestLoad(36553, (unsigned long )0, (long long )((boolean )0));
              __CrestStore(36554, (unsigned long )(& had_char_class));
# 2272 "regex.c"
              had_char_class = (boolean )0;
# 2273 "regex.c"
              mem_202 = b + (int )c / 8;
# 2273 "regex.c"
              mem_203 = b + (int )c / 8;
              __CrestLoad(36561, (unsigned long )mem_203, (long long )*mem_203);
              __CrestLoad(36560, (unsigned long )0, (long long )1);
              __CrestLoad(36559, (unsigned long )(& c), (long long )c);
              __CrestLoad(36558, (unsigned long )0, (long long )8);
              __CrestApply2(36557, 4, (long long )((int )c % 8));
              __CrestApply2(36556, 8, (long long )(1 << (int )c % 8));
              __CrestApply2(36555, 6, (long long )((int )*mem_203 | (1 << (int )c % 8)));
              __CrestStore(36562, (unsigned long )mem_202);
# 2273 "regex.c"
              *mem_202 = (unsigned char )((int )*mem_203 | (1 << (int )c % 8));
            }
            }
          } else {
            __CrestBranch(36299, 8603, 0);
            __CrestLoad(36563, (unsigned long )0, (long long )((boolean )0));
            __CrestStore(36564, (unsigned long )(& had_char_class));
# 2272 "regex.c"
            had_char_class = (boolean )0;
# 2273 "regex.c"
            mem_204 = b + (int )c / 8;
# 2273 "regex.c"
            mem_205 = b + (int )c / 8;
            __CrestLoad(36571, (unsigned long )mem_205, (long long )*mem_205);
            __CrestLoad(36570, (unsigned long )0, (long long )1);
            __CrestLoad(36569, (unsigned long )(& c), (long long )c);
            __CrestLoad(36568, (unsigned long )0, (long long )8);
            __CrestApply2(36567, 4, (long long )((int )c % 8));
            __CrestApply2(36566, 8, (long long )(1 << (int )c % 8));
            __CrestApply2(36565, 6, (long long )((int )*mem_205 | (1 << (int )c % 8)));
            __CrestStore(36572, (unsigned long )mem_204);
# 2273 "regex.c"
            *mem_204 = (unsigned char )((int )*mem_205 | (1 << (int )c % 8));
          }
          }
        }
        }
        }
      }
      }
      __Cont: ;
    }
    while_break___27: ;
    }
    {
# 2279 "regex.c"
    while (1) {
      while_continue___37: ;
      {
# 2279 "regex.c"
      mem_206 = b + -1;
      {
      __CrestLoad(36575, (unsigned long )mem_206, (long long )*mem_206);
      __CrestLoad(36574, (unsigned long )0, (long long )0);
      __CrestApply2(36573, 14, (long long )((int )*mem_206 > 0));
# 2279 "regex.c"
      if ((int )*mem_206 > 0) {
        __CrestBranch(36576, 8612, 1);
        {
# 2279 "regex.c"
        mem_207 = b + -1;
# 2279 "regex.c"
        mem_208 = b + ((int )*mem_207 - 1);
        {
        __CrestLoad(36580, (unsigned long )mem_208, (long long )*mem_208);
        __CrestLoad(36579, (unsigned long )0, (long long )0);
        __CrestApply2(36578, 12, (long long )((int )*mem_208 == 0));
# 2279 "regex.c"
        if ((int )*mem_208 == 0) {
          __CrestBranch(36581, 8615, 1);

        } else {
          __CrestBranch(36582, 8616, 0);
# 2279 "regex.c"
          goto while_break___37;
        }
        }
        }
      } else {
        __CrestBranch(36577, 8617, 0);
# 2279 "regex.c"
        goto while_break___37;
      }
      }
      }
# 2280 "regex.c"
      mem_209 = b + -1;
# 2280 "regex.c"
      mem_210 = b + -1;
      __CrestLoad(36585, (unsigned long )mem_210, (long long )*mem_210);
      __CrestLoad(36584, (unsigned long )0, (long long )1);
      __CrestApply2(36583, 1, (long long )((int )*mem_210 - 1));
      __CrestStore(36586, (unsigned long )mem_209);
# 2280 "regex.c"
      *mem_209 = (unsigned char )((int )*mem_210 - 1);
    }
    while_break___37: ;
    }
# 2281 "regex.c"
    mem_211 = b + -1;
# 2281 "regex.c"
    b += (int )*mem_211;
# 2283 "regex.c"
    goto switch_break;
    case_40:
    {
    __CrestLoad(36591, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36590, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36589, 5, (long long )(syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36588, (unsigned long )0, (long long )0);
    __CrestApply2(36587, 13, (long long )((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2287 "regex.c"
    if ((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36592, 8623, 1);
# 2288 "regex.c"
      goto handle_open;
    } else {
      __CrestBranch(36593, 8624, 0);
# 2290 "regex.c"
      goto normal_char;
    }
    }
    case_41:
    {
    __CrestLoad(36598, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36597, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36596, 5, (long long )(syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36595, (unsigned long )0, (long long )0);
    __CrestApply2(36594, 13, (long long )((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2294 "regex.c"
    if ((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36599, 8626, 1);
# 2295 "regex.c"
      goto handle_close;
    } else {
      __CrestBranch(36600, 8627, 0);
# 2297 "regex.c"
      goto normal_char;
    }
    }
    case_10:
    {
    __CrestLoad(36605, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36604, (unsigned long )0, (long long )(((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36603, 5, (long long )(syntax & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36602, (unsigned long )0, (long long )0);
    __CrestApply2(36601, 13, (long long )((syntax & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2301 "regex.c"
    if ((syntax & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36606, 8629, 1);
# 2302 "regex.c"
      goto handle_alt;
    } else {
      __CrestBranch(36607, 8630, 0);
# 2304 "regex.c"
      goto normal_char;
    }
    }
    case_124:
    {
    __CrestLoad(36612, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36611, (unsigned long )0, (long long )(((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36610, 5, (long long )(syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36609, (unsigned long )0, (long long )0);
    __CrestApply2(36608, 13, (long long )((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2308 "regex.c"
    if ((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36613, 8632, 1);
# 2309 "regex.c"
      goto handle_alt;
    } else {
      __CrestBranch(36614, 8633, 0);
# 2311 "regex.c"
      goto normal_char;
    }
    }
    case_123:
    {
    __CrestLoad(36619, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36618, (unsigned long )0, (long long )(((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36617, 5, (long long )(syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36616, (unsigned long )0, (long long )0);
    __CrestApply2(36615, 13, (long long )((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2315 "regex.c"
    if ((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36620, 8635, 1);
      {
      __CrestLoad(36626, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(36625, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(36624, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(36623, (unsigned long )0, (long long )0);
      __CrestApply2(36622, 13, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2315 "regex.c"
      if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(36627, 8636, 1);
# 2316 "regex.c"
        goto handle_interval;
      } else {
        __CrestBranch(36628, 8637, 0);
# 2318 "regex.c"
        goto normal_char;
      }
      }
    } else {
      __CrestBranch(36621, 8638, 0);
# 2318 "regex.c"
      goto normal_char;
    }
    }
    case_92:
    {
    __CrestLoad(36631, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(36630, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(36629, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2322 "regex.c"
    if ((unsigned long )p == (unsigned long )pend) {
      __CrestBranch(36632, 8640, 1);
# 2322 "regex.c"
      free((void *)compile_stack.stack);
      __CrestClearStack(36634);
      __CrestLoad(36635, (unsigned long )0, (long long )((reg_errcode_t )5));
      __CrestStore(36636, (unsigned long )(& __retres238));
# 2322 "regex.c"
      __retres238 = (reg_errcode_t )5;
# 2322 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(36633, 8643, 0);

    }
    }
    {
# 2327 "regex.c"
    while (1) {
      while_continue___38: ;
      {
      __CrestLoad(36639, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(36638, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(36637, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2327 "regex.c"
      if ((unsigned long )p == (unsigned long )pend) {
        __CrestBranch(36640, 8648, 1);
        __CrestLoad(36642, (unsigned long )0, (long long )((reg_errcode_t )14));
        __CrestStore(36643, (unsigned long )(& __retres238));
# 2327 "regex.c"
        __retres238 = (reg_errcode_t )14;
# 2327 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(36641, 8650, 0);

      }
      }
# 2327 "regex.c"
      tmp___43 = p;
# 2327 "regex.c"
      p ++;
      __CrestLoad(36644, (unsigned long )tmp___43, (long long )*tmp___43);
      __CrestStore(36645, (unsigned long )(& c));
# 2327 "regex.c"
      c = (unsigned char )*tmp___43;
# 2327 "regex.c"
      goto while_break___38;
    }
    while_break___38: ;
    }
    {
    {
    __CrestLoad(36648, (unsigned long )(& c), (long long )c);
    __CrestLoad(36647, (unsigned long )0, (long long )40);
    __CrestApply2(36646, 12, (long long )((int )c == 40));
# 2331 "regex.c"
    if ((int )c == 40) {
      __CrestBranch(36649, 8656, 1);
# 2331 "regex.c"
      goto case_40___0;
    } else {
      __CrestBranch(36650, 8657, 0);

    }
    }
    {
    __CrestLoad(36653, (unsigned long )(& c), (long long )c);
    __CrestLoad(36652, (unsigned long )0, (long long )41);
    __CrestApply2(36651, 12, (long long )((int )c == 41));
# 2380 "regex.c"
    if ((int )c == 41) {
      __CrestBranch(36654, 8659, 1);
# 2380 "regex.c"
      goto case_41___0;
    } else {
      __CrestBranch(36655, 8660, 0);

    }
    }
    {
    __CrestLoad(36658, (unsigned long )(& c), (long long )c);
    __CrestLoad(36657, (unsigned long )0, (long long )124);
    __CrestApply2(36656, 12, (long long )((int )c == 124));
# 2446 "regex.c"
    if ((int )c == 124) {
      __CrestBranch(36659, 8662, 1);
# 2446 "regex.c"
      goto case_124___0;
    } else {
      __CrestBranch(36660, 8663, 0);

    }
    }
    {
    __CrestLoad(36663, (unsigned long )(& c), (long long )c);
    __CrestLoad(36662, (unsigned long )0, (long long )123);
    __CrestApply2(36661, 12, (long long )((int )c == 123));
# 2491 "regex.c"
    if ((int )c == 123) {
      __CrestBranch(36664, 8665, 1);
# 2491 "regex.c"
      goto case_123___0;
    } else {
      __CrestBranch(36665, 8666, 0);

    }
    }
    {
    __CrestLoad(36668, (unsigned long )(& c), (long long )c);
    __CrestLoad(36667, (unsigned long )0, (long long )119);
    __CrestApply2(36666, 12, (long long )((int )c == 119));
# 2681 "regex.c"
    if ((int )c == 119) {
      __CrestBranch(36669, 8668, 1);
# 2681 "regex.c"
      goto case_119;
    } else {
      __CrestBranch(36670, 8669, 0);

    }
    }
    {
    __CrestLoad(36673, (unsigned long )(& c), (long long )c);
    __CrestLoad(36672, (unsigned long )0, (long long )87);
    __CrestApply2(36671, 12, (long long )((int )c == 87));
# 2689 "regex.c"
    if ((int )c == 87) {
      __CrestBranch(36674, 8671, 1);
# 2689 "regex.c"
      goto case_87;
    } else {
      __CrestBranch(36675, 8672, 0);

    }
    }
    {
    __CrestLoad(36678, (unsigned long )(& c), (long long )c);
    __CrestLoad(36677, (unsigned long )0, (long long )60);
    __CrestApply2(36676, 12, (long long )((int )c == 60));
# 2697 "regex.c"
    if ((int )c == 60) {
      __CrestBranch(36679, 8674, 1);
# 2697 "regex.c"
      goto case_60;
    } else {
      __CrestBranch(36680, 8675, 0);

    }
    }
    {
    __CrestLoad(36683, (unsigned long )(& c), (long long )c);
    __CrestLoad(36682, (unsigned long )0, (long long )62);
    __CrestApply2(36681, 12, (long long )((int )c == 62));
# 2703 "regex.c"
    if ((int )c == 62) {
      __CrestBranch(36684, 8677, 1);
# 2703 "regex.c"
      goto case_62;
    } else {
      __CrestBranch(36685, 8678, 0);

    }
    }
    {
    __CrestLoad(36688, (unsigned long )(& c), (long long )c);
    __CrestLoad(36687, (unsigned long )0, (long long )98);
    __CrestApply2(36686, 12, (long long )((int )c == 98));
# 2709 "regex.c"
    if ((int )c == 98) {
      __CrestBranch(36689, 8680, 1);
# 2709 "regex.c"
      goto case_98;
    } else {
      __CrestBranch(36690, 8681, 0);

    }
    }
    {
    __CrestLoad(36693, (unsigned long )(& c), (long long )c);
    __CrestLoad(36692, (unsigned long )0, (long long )66);
    __CrestApply2(36691, 12, (long long )((int )c == 66));
# 2715 "regex.c"
    if ((int )c == 66) {
      __CrestBranch(36694, 8683, 1);
# 2715 "regex.c"
      goto case_66;
    } else {
      __CrestBranch(36695, 8684, 0);

    }
    }
    {
    __CrestLoad(36698, (unsigned long )(& c), (long long )c);
    __CrestLoad(36697, (unsigned long )0, (long long )96);
    __CrestApply2(36696, 12, (long long )((int )c == 96));
# 2721 "regex.c"
    if ((int )c == 96) {
      __CrestBranch(36699, 8686, 1);
# 2721 "regex.c"
      goto case_96;
    } else {
      __CrestBranch(36700, 8687, 0);

    }
    }
    {
    __CrestLoad(36703, (unsigned long )(& c), (long long )c);
    __CrestLoad(36702, (unsigned long )0, (long long )39);
    __CrestApply2(36701, 12, (long long )((int )c == 39));
# 2727 "regex.c"
    if ((int )c == 39) {
      __CrestBranch(36704, 8689, 1);
# 2727 "regex.c"
      goto case_39;
    } else {
      __CrestBranch(36705, 8690, 0);

    }
    }
    {
    __CrestLoad(36708, (unsigned long )(& c), (long long )c);
    __CrestLoad(36707, (unsigned long )0, (long long )57);
    __CrestApply2(36706, 12, (long long )((int )c == 57));
# 2734 "regex.c"
    if ((int )c == 57) {
      __CrestBranch(36709, 8692, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36710, 8693, 0);

    }
    }
    {
    __CrestLoad(36713, (unsigned long )(& c), (long long )c);
    __CrestLoad(36712, (unsigned long )0, (long long )56);
    __CrestApply2(36711, 12, (long long )((int )c == 56));
# 2734 "regex.c"
    if ((int )c == 56) {
      __CrestBranch(36714, 8695, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36715, 8696, 0);

    }
    }
    {
    __CrestLoad(36718, (unsigned long )(& c), (long long )c);
    __CrestLoad(36717, (unsigned long )0, (long long )55);
    __CrestApply2(36716, 12, (long long )((int )c == 55));
# 2734 "regex.c"
    if ((int )c == 55) {
      __CrestBranch(36719, 8698, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36720, 8699, 0);

    }
    }
    {
    __CrestLoad(36723, (unsigned long )(& c), (long long )c);
    __CrestLoad(36722, (unsigned long )0, (long long )54);
    __CrestApply2(36721, 12, (long long )((int )c == 54));
# 2734 "regex.c"
    if ((int )c == 54) {
      __CrestBranch(36724, 8701, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36725, 8702, 0);

    }
    }
    {
    __CrestLoad(36728, (unsigned long )(& c), (long long )c);
    __CrestLoad(36727, (unsigned long )0, (long long )53);
    __CrestApply2(36726, 12, (long long )((int )c == 53));
# 2734 "regex.c"
    if ((int )c == 53) {
      __CrestBranch(36729, 8704, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36730, 8705, 0);

    }
    }
    {
    __CrestLoad(36733, (unsigned long )(& c), (long long )c);
    __CrestLoad(36732, (unsigned long )0, (long long )52);
    __CrestApply2(36731, 12, (long long )((int )c == 52));
# 2734 "regex.c"
    if ((int )c == 52) {
      __CrestBranch(36734, 8707, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36735, 8708, 0);

    }
    }
    {
    __CrestLoad(36738, (unsigned long )(& c), (long long )c);
    __CrestLoad(36737, (unsigned long )0, (long long )51);
    __CrestApply2(36736, 12, (long long )((int )c == 51));
# 2734 "regex.c"
    if ((int )c == 51) {
      __CrestBranch(36739, 8710, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36740, 8711, 0);

    }
    }
    {
    __CrestLoad(36743, (unsigned long )(& c), (long long )c);
    __CrestLoad(36742, (unsigned long )0, (long long )50);
    __CrestApply2(36741, 12, (long long )((int )c == 50));
# 2734 "regex.c"
    if ((int )c == 50) {
      __CrestBranch(36744, 8713, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36745, 8714, 0);

    }
    }
    {
    __CrestLoad(36748, (unsigned long )(& c), (long long )c);
    __CrestLoad(36747, (unsigned long )0, (long long )49);
    __CrestApply2(36746, 12, (long long )((int )c == 49));
# 2734 "regex.c"
    if ((int )c == 49) {
      __CrestBranch(36749, 8716, 1);
# 2734 "regex.c"
      goto case_57;
    } else {
      __CrestBranch(36750, 8717, 0);

    }
    }
    {
    __CrestLoad(36753, (unsigned long )(& c), (long long )c);
    __CrestLoad(36752, (unsigned long )0, (long long )63);
    __CrestApply2(36751, 12, (long long )((int )c == 63));
# 2753 "regex.c"
    if ((int )c == 63) {
      __CrestBranch(36754, 8719, 1);
# 2753 "regex.c"
      goto case_63___0;
    } else {
      __CrestBranch(36755, 8720, 0);

    }
    }
    {
    __CrestLoad(36758, (unsigned long )(& c), (long long )c);
    __CrestLoad(36757, (unsigned long )0, (long long )43);
    __CrestApply2(36756, 12, (long long )((int )c == 43));
# 2753 "regex.c"
    if ((int )c == 43) {
      __CrestBranch(36759, 8722, 1);
# 2753 "regex.c"
      goto case_63___0;
    } else {
      __CrestBranch(36760, 8723, 0);

    }
    }
# 2759 "regex.c"
    goto normal_backslash;
    case_40___0:
    {
    __CrestLoad(36765, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36764, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36763, 5, (long long )(syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36762, (unsigned long )0, (long long )0);
    __CrestApply2(36761, 13, (long long )((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2332 "regex.c"
    if ((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36766, 8726, 1);
# 2333 "regex.c"
      goto normal_backslash;
    } else {
      __CrestBranch(36767, 8727, 0);

    }
    }
    handle_open:
    __CrestLoad(36770, (unsigned long )(& bufp->re_nsub), (long long )bufp->re_nsub);
    __CrestLoad(36769, (unsigned long )0, (long long )1UL);
    __CrestApply2(36768, 0, (long long )(bufp->re_nsub + 1UL));
    __CrestStore(36771, (unsigned long )(& bufp->re_nsub));
# 2336 "regex.c"
    (bufp->re_nsub) ++;
    __CrestLoad(36774, (unsigned long )(& regnum), (long long )regnum);
    __CrestLoad(36773, (unsigned long )0, (long long )1U);
    __CrestApply2(36772, 0, (long long )(regnum + 1U));
    __CrestStore(36775, (unsigned long )(& regnum));
# 2337 "regex.c"
    regnum ++;
    {
    __CrestLoad(36778, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
    __CrestLoad(36777, (unsigned long )(& compile_stack.size), (long long )compile_stack.size);
    __CrestApply2(36776, 12, (long long )(compile_stack.avail == compile_stack.size));
# 2339 "regex.c"
    if (compile_stack.avail == compile_stack.size) {
      __CrestBranch(36779, 8730, 1);
      __CrestLoad(36785, (unsigned long )(& compile_stack.size), (long long )compile_stack.size);
      __CrestLoad(36784, (unsigned long )0, (long long )1);
      __CrestApply2(36783, 8, (long long )(compile_stack.size << 1));
      __CrestLoad(36782, (unsigned long )0, (long long )sizeof(compile_stack_elt_t ));
      __CrestApply2(36781, 2, (long long )((unsigned long )(compile_stack.size << 1) * sizeof(compile_stack_elt_t )));
# 2341 "regex.c"
      tmp___44 = realloc((void *)compile_stack.stack, (unsigned long )(compile_stack.size << 1) * sizeof(compile_stack_elt_t ));
      __CrestClearStack(36786);
# 2341 "regex.c"
      compile_stack.stack = (compile_stack_elt_t *)tmp___44;
      {
      __CrestLoad(36789, (unsigned long )(& compile_stack.stack), (long long )((unsigned long )compile_stack.stack));
      __CrestLoad(36788, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(36787, 12, (long long )((unsigned long )compile_stack.stack == (unsigned long )((void *)0)));
# 2343 "regex.c"
      if ((unsigned long )compile_stack.stack == (unsigned long )((void *)0)) {
        __CrestBranch(36790, 8732, 1);
        __CrestLoad(36792, (unsigned long )0, (long long )((reg_errcode_t )12));
        __CrestStore(36793, (unsigned long )(& __retres238));
# 2343 "regex.c"
        __retres238 = (reg_errcode_t )12;
# 2343 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(36791, 8734, 0);

      }
      }
      __CrestLoad(36796, (unsigned long )(& compile_stack.size), (long long )compile_stack.size);
      __CrestLoad(36795, (unsigned long )0, (long long )1);
      __CrestApply2(36794, 8, (long long )(compile_stack.size << 1));
      __CrestStore(36797, (unsigned long )(& compile_stack.size));
# 2345 "regex.c"
      compile_stack.size <<= 1;
    } else {
      __CrestBranch(36780, 8736, 0);

    }
    }
# 2352 "regex.c"
    mem_212 = compile_stack.stack + compile_stack.avail;
    __CrestLoad(36800, (unsigned long )(& begalt), (long long )((unsigned long )begalt));
    __CrestLoad(36799, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
    __CrestApply2(36798, 18, (long long )(begalt - bufp->buffer));
    __CrestStore(36801, (unsigned long )(& mem_212->begalt_offset));
# 2352 "regex.c"
    mem_212->begalt_offset = begalt - bufp->buffer;
    {
    __CrestLoad(36804, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
    __CrestLoad(36803, (unsigned long )0, (long long )0);
    __CrestApply2(36802, 13, (long long )(fixup_alt_jump != 0));
# 2353 "regex.c"
    if (fixup_alt_jump != 0) {
      __CrestBranch(36805, 8739, 1);
# 2353 "regex.c"
      mem_213 = compile_stack.stack + compile_stack.avail;
      __CrestLoad(36811, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
      __CrestLoad(36810, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
      __CrestApply2(36809, 18, (long long )(fixup_alt_jump - bufp->buffer));
      __CrestLoad(36808, (unsigned long )0, (long long )1L);
      __CrestApply2(36807, 0, (long long )((fixup_alt_jump - bufp->buffer) + 1L));
      __CrestStore(36812, (unsigned long )(& mem_213->fixup_alt_jump));
# 2353 "regex.c"
      mem_213->fixup_alt_jump = (fixup_alt_jump - bufp->buffer) + 1L;
    } else {
      __CrestBranch(36806, 8740, 0);
# 2353 "regex.c"
      mem_214 = compile_stack.stack + compile_stack.avail;
      __CrestLoad(36813, (unsigned long )0, (long long )((pattern_offset_t )0));
      __CrestStore(36814, (unsigned long )(& mem_214->fixup_alt_jump));
# 2353 "regex.c"
      mem_214->fixup_alt_jump = (pattern_offset_t )0;
    }
    }
# 2355 "regex.c"
    mem_215 = compile_stack.stack + compile_stack.avail;
    __CrestLoad(36817, (unsigned long )(& b), (long long )((unsigned long )b));
    __CrestLoad(36816, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
    __CrestApply2(36815, 18, (long long )(b - bufp->buffer));
    __CrestStore(36818, (unsigned long )(& mem_215->laststart_offset));
# 2355 "regex.c"
    mem_215->laststart_offset = b - bufp->buffer;
# 2356 "regex.c"
    mem_216 = compile_stack.stack + compile_stack.avail;
    __CrestLoad(36819, (unsigned long )(& regnum), (long long )regnum);
    __CrestStore(36820, (unsigned long )(& mem_216->regnum));
# 2356 "regex.c"
    mem_216->regnum = regnum;
    {
    __CrestLoad(36823, (unsigned long )(& regnum), (long long )regnum);
    __CrestLoad(36822, (unsigned long )0, (long long )255U);
    __CrestApply2(36821, 15, (long long )(regnum <= 255U));
# 2362 "regex.c"
    if (regnum <= 255U) {
      __CrestBranch(36824, 8743, 1);
# 2364 "regex.c"
      mem_217 = compile_stack.stack + compile_stack.avail;
      __CrestLoad(36830, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(36829, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
      __CrestApply2(36828, 18, (long long )(b - bufp->buffer));
      __CrestLoad(36827, (unsigned long )0, (long long )2L);
      __CrestApply2(36826, 0, (long long )((b - bufp->buffer) + 2L));
      __CrestStore(36831, (unsigned long )(& mem_217->inner_group_offset));
# 2364 "regex.c"
      mem_217->inner_group_offset = (b - bufp->buffer) + 2L;
      {
# 2365 "regex.c"
      while (1) {
        while_continue___39: ;
        {
# 2365 "regex.c"
        while (1) {
          while_continue___40: ;
          {
          __CrestLoad(36838, (unsigned long )(& b), (long long )((unsigned long )b));
          __CrestLoad(36837, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(36836, 18, (long long )(b - bufp->buffer));
          __CrestLoad(36835, (unsigned long )0, (long long )3L);
          __CrestApply2(36834, 0, (long long )((b - bufp->buffer) + 3L));
          __CrestLoad(36833, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestApply2(36832, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2365 "regex.c"
          if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
            __CrestBranch(36839, 8751, 1);

          } else {
            __CrestBranch(36840, 8752, 0);
# 2365 "regex.c"
            goto while_break___40;
          }
          }
          {
# 2365 "regex.c"
          while (1) {
            while_continue___41: ;
# 2365 "regex.c"
            old_buffer___8 = bufp->buffer;
            {
            __CrestLoad(36843, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(36842, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestApply2(36841, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2365 "regex.c"
            if (bufp->allocated == (unsigned long )(1L << 16)) {
              __CrestBranch(36844, 8758, 1);
              __CrestLoad(36846, (unsigned long )0, (long long )((reg_errcode_t )15));
              __CrestStore(36847, (unsigned long )(& __retres238));
# 2365 "regex.c"
              __retres238 = (reg_errcode_t )15;
# 2365 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(36845, 8760, 0);

            }
            }
            __CrestLoad(36850, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(36849, (unsigned long )0, (long long )1);
            __CrestApply2(36848, 8, (long long )(bufp->allocated << 1));
            __CrestStore(36851, (unsigned long )(& bufp->allocated));
# 2365 "regex.c"
            bufp->allocated <<= 1;
            {
            __CrestLoad(36854, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(36853, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestApply2(36852, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2365 "regex.c"
            if (bufp->allocated > (unsigned long )(1L << 16)) {
              __CrestBranch(36855, 8763, 1);
              __CrestLoad(36857, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
              __CrestStore(36858, (unsigned long )(& bufp->allocated));
# 2365 "regex.c"
              bufp->allocated = (unsigned long )(1L << 16);
            } else {
              __CrestBranch(36856, 8764, 0);

            }
            }
            __CrestLoad(36859, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2365 "regex.c"
            tmp___45 = realloc((void *)bufp->buffer, bufp->allocated);
            __CrestClearStack(36860);
# 2365 "regex.c"
            bufp->buffer = (unsigned char *)tmp___45;
            {
            __CrestLoad(36863, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
            __CrestLoad(36862, (unsigned long )0, (long long )((unsigned long )((void *)0)));
            __CrestApply2(36861, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2365 "regex.c"
            if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
              __CrestBranch(36864, 8767, 1);
              __CrestLoad(36866, (unsigned long )0, (long long )((reg_errcode_t )12));
              __CrestStore(36867, (unsigned long )(& __retres238));
# 2365 "regex.c"
              __retres238 = (reg_errcode_t )12;
# 2365 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(36865, 8769, 0);

            }
            }
            {
            __CrestLoad(36870, (unsigned long )(& old_buffer___8), (long long )((unsigned long )old_buffer___8));
            __CrestLoad(36869, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
            __CrestApply2(36868, 13, (long long )((unsigned long )old_buffer___8 != (unsigned long )bufp->buffer));
# 2365 "regex.c"
            if ((unsigned long )old_buffer___8 != (unsigned long )bufp->buffer) {
              __CrestBranch(36871, 8771, 1);
# 2365 "regex.c"
              b = bufp->buffer + (b - old_buffer___8);
# 2365 "regex.c"
              begalt = bufp->buffer + (begalt - old_buffer___8);
              {
              __CrestLoad(36875, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
              __CrestLoad(36874, (unsigned long )0, (long long )0);
              __CrestApply2(36873, 13, (long long )(fixup_alt_jump != 0));
# 2365 "regex.c"
              if (fixup_alt_jump != 0) {
                __CrestBranch(36876, 8773, 1);
# 2365 "regex.c"
                fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___8);
              } else {
                __CrestBranch(36877, 8774, 0);

              }
              }
              {
              __CrestLoad(36880, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
              __CrestLoad(36879, (unsigned long )0, (long long )0);
              __CrestApply2(36878, 13, (long long )(laststart != 0));
# 2365 "regex.c"
              if (laststart != 0) {
                __CrestBranch(36881, 8776, 1);
# 2365 "regex.c"
                laststart = bufp->buffer + (laststart - old_buffer___8);
              } else {
                __CrestBranch(36882, 8777, 0);

              }
              }
              {
              __CrestLoad(36885, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
              __CrestLoad(36884, (unsigned long )0, (long long )0);
              __CrestApply2(36883, 13, (long long )(pending_exact != 0));
# 2365 "regex.c"
              if (pending_exact != 0) {
                __CrestBranch(36886, 8779, 1);
# 2365 "regex.c"
                pending_exact = bufp->buffer + (pending_exact - old_buffer___8);
              } else {
                __CrestBranch(36887, 8780, 0);

              }
              }
            } else {
              __CrestBranch(36872, 8781, 0);

            }
            }
# 2365 "regex.c"
            goto while_break___41;
          }
          while_break___41: ;
          }
        }
        while_break___40: ;
        }
# 2365 "regex.c"
        tmp___46 = b;
# 2365 "regex.c"
        b ++;
        __CrestLoad(36888, (unsigned long )0, (long long )(unsigned char)6);
        __CrestStore(36889, (unsigned long )tmp___46);
# 2365 "regex.c"
        *tmp___46 = (unsigned char)6;
# 2365 "regex.c"
        tmp___47 = b;
# 2365 "regex.c"
        b ++;
        __CrestLoad(36890, (unsigned long )(& regnum), (long long )regnum);
        __CrestStore(36891, (unsigned long )tmp___47);
# 2365 "regex.c"
        *tmp___47 = (unsigned char )regnum;
# 2365 "regex.c"
        tmp___48 = b;
# 2365 "regex.c"
        b ++;
        __CrestLoad(36892, (unsigned long )0, (long long )(unsigned char)0);
        __CrestStore(36893, (unsigned long )tmp___48);
# 2365 "regex.c"
        *tmp___48 = (unsigned char)0;
# 2365 "regex.c"
        goto while_break___39;
      }
      while_break___39: ;
      }
    } else {
      __CrestBranch(36825, 8788, 0);

    }
    }
    __CrestLoad(36896, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
    __CrestLoad(36895, (unsigned long )0, (long long )1U);
    __CrestApply2(36894, 0, (long long )(compile_stack.avail + 1U));
    __CrestStore(36897, (unsigned long )(& compile_stack.avail));
# 2368 "regex.c"
    (compile_stack.avail) ++;
# 2370 "regex.c"
    fixup_alt_jump = (unsigned char *)0;
# 2371 "regex.c"
    laststart = (unsigned char *)0;
# 2372 "regex.c"
    begalt = b;
# 2376 "regex.c"
    pending_exact = (unsigned char *)0;
# 2377 "regex.c"
    goto switch_break___0;
    case_41___0:
    {
    __CrestLoad(36902, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(36901, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(36900, 5, (long long )(syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(36899, (unsigned long )0, (long long )0);
    __CrestApply2(36898, 13, (long long )((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2381 "regex.c"
    if ((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(36903, 8792, 1);
# 2381 "regex.c"
      goto normal_backslash;
    } else {
      __CrestBranch(36904, 8793, 0);

    }
    }
    {
    __CrestLoad(36907, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
    __CrestLoad(36906, (unsigned long )0, (long long )0U);
    __CrestApply2(36905, 12, (long long )(compile_stack.avail == 0U));
# 2383 "regex.c"
    if (compile_stack.avail == 0U) {
      __CrestBranch(36908, 8795, 1);
      {
      __CrestLoad(36914, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(36913, (unsigned long )0, (long long )(((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(36912, 5, (long long )(syntax & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(36911, (unsigned long )0, (long long )0);
      __CrestApply2(36910, 13, (long long )((syntax & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2384 "regex.c"
      if ((syntax & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(36915, 8796, 1);
# 2385 "regex.c"
        goto normal_backslash;
      } else {
        __CrestBranch(36916, 8797, 0);
# 2387 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(36917);
        __CrestLoad(36918, (unsigned long )0, (long long )((reg_errcode_t )16));
        __CrestStore(36919, (unsigned long )(& __retres238));
# 2387 "regex.c"
        __retres238 = (reg_errcode_t )16;
# 2387 "regex.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(36909, 8800, 0);

    }
    }
    handle_close:
    {
    __CrestLoad(36922, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
    __CrestLoad(36921, (unsigned long )0, (long long )0);
    __CrestApply2(36920, 13, (long long )(fixup_alt_jump != 0));
# 2390 "regex.c"
    if (fixup_alt_jump != 0) {
      __CrestBranch(36923, 8802, 1);
      {
# 2395 "regex.c"
      while (1) {
        while_continue___42: ;
        {
# 2395 "regex.c"
        while (1) {
          while_continue___43: ;
          {
          __CrestLoad(36931, (unsigned long )(& b), (long long )((unsigned long )b));
          __CrestLoad(36930, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(36929, 18, (long long )(b - bufp->buffer));
          __CrestLoad(36928, (unsigned long )0, (long long )1L);
          __CrestApply2(36927, 0, (long long )((b - bufp->buffer) + 1L));
          __CrestLoad(36926, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestApply2(36925, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2395 "regex.c"
          if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
            __CrestBranch(36932, 8809, 1);

          } else {
            __CrestBranch(36933, 8810, 0);
# 2395 "regex.c"
            goto while_break___43;
          }
          }
          {
# 2395 "regex.c"
          while (1) {
            while_continue___44: ;
# 2395 "regex.c"
            old_buffer___9 = bufp->buffer;
            {
            __CrestLoad(36936, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(36935, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestApply2(36934, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2395 "regex.c"
            if (bufp->allocated == (unsigned long )(1L << 16)) {
              __CrestBranch(36937, 8816, 1);
              __CrestLoad(36939, (unsigned long )0, (long long )((reg_errcode_t )15));
              __CrestStore(36940, (unsigned long )(& __retres238));
# 2395 "regex.c"
              __retres238 = (reg_errcode_t )15;
# 2395 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(36938, 8818, 0);

            }
            }
            __CrestLoad(36943, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(36942, (unsigned long )0, (long long )1);
            __CrestApply2(36941, 8, (long long )(bufp->allocated << 1));
            __CrestStore(36944, (unsigned long )(& bufp->allocated));
# 2395 "regex.c"
            bufp->allocated <<= 1;
            {
            __CrestLoad(36947, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(36946, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestApply2(36945, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2395 "regex.c"
            if (bufp->allocated > (unsigned long )(1L << 16)) {
              __CrestBranch(36948, 8821, 1);
              __CrestLoad(36950, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
              __CrestStore(36951, (unsigned long )(& bufp->allocated));
# 2395 "regex.c"
              bufp->allocated = (unsigned long )(1L << 16);
            } else {
              __CrestBranch(36949, 8822, 0);

            }
            }
            __CrestLoad(36952, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2395 "regex.c"
            tmp___49 = realloc((void *)bufp->buffer, bufp->allocated);
            __CrestClearStack(36953);
# 2395 "regex.c"
            bufp->buffer = (unsigned char *)tmp___49;
            {
            __CrestLoad(36956, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
            __CrestLoad(36955, (unsigned long )0, (long long )((unsigned long )((void *)0)));
            __CrestApply2(36954, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2395 "regex.c"
            if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
              __CrestBranch(36957, 8825, 1);
              __CrestLoad(36959, (unsigned long )0, (long long )((reg_errcode_t )12));
              __CrestStore(36960, (unsigned long )(& __retres238));
# 2395 "regex.c"
              __retres238 = (reg_errcode_t )12;
# 2395 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(36958, 8827, 0);

            }
            }
            {
            __CrestLoad(36963, (unsigned long )(& old_buffer___9), (long long )((unsigned long )old_buffer___9));
            __CrestLoad(36962, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
            __CrestApply2(36961, 13, (long long )((unsigned long )old_buffer___9 != (unsigned long )bufp->buffer));
# 2395 "regex.c"
            if ((unsigned long )old_buffer___9 != (unsigned long )bufp->buffer) {
              __CrestBranch(36964, 8829, 1);
# 2395 "regex.c"
              b = bufp->buffer + (b - old_buffer___9);
# 2395 "regex.c"
              begalt = bufp->buffer + (begalt - old_buffer___9);
              {
              __CrestLoad(36968, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
              __CrestLoad(36967, (unsigned long )0, (long long )0);
              __CrestApply2(36966, 13, (long long )(fixup_alt_jump != 0));
# 2395 "regex.c"
              if (fixup_alt_jump != 0) {
                __CrestBranch(36969, 8831, 1);
# 2395 "regex.c"
                fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___9);
              } else {
                __CrestBranch(36970, 8832, 0);

              }
              }
              {
              __CrestLoad(36973, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
              __CrestLoad(36972, (unsigned long )0, (long long )0);
              __CrestApply2(36971, 13, (long long )(laststart != 0));
# 2395 "regex.c"
              if (laststart != 0) {
                __CrestBranch(36974, 8834, 1);
# 2395 "regex.c"
                laststart = bufp->buffer + (laststart - old_buffer___9);
              } else {
                __CrestBranch(36975, 8835, 0);

              }
              }
              {
              __CrestLoad(36978, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
              __CrestLoad(36977, (unsigned long )0, (long long )0);
              __CrestApply2(36976, 13, (long long )(pending_exact != 0));
# 2395 "regex.c"
              if (pending_exact != 0) {
                __CrestBranch(36979, 8837, 1);
# 2395 "regex.c"
                pending_exact = bufp->buffer + (pending_exact - old_buffer___9);
              } else {
                __CrestBranch(36980, 8838, 0);

              }
              }
            } else {
              __CrestBranch(36965, 8839, 0);

            }
            }
# 2395 "regex.c"
            goto while_break___44;
          }
          while_break___44: ;
          }
        }
        while_break___43: ;
        }
# 2395 "regex.c"
        tmp___50 = b;
# 2395 "regex.c"
        b ++;
        __CrestLoad(36981, (unsigned long )0, (long long )(unsigned char)20);
        __CrestStore(36982, (unsigned long )tmp___50);
# 2395 "regex.c"
        *tmp___50 = (unsigned char)20;
# 2395 "regex.c"
        goto while_break___42;
      }
      while_break___42: ;
      }
      __CrestLoad(36983, (unsigned long )0, (long long )((re_opcode_t )14));
      __CrestLoad(36990, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(36989, (unsigned long )0, (long long )1);
      __CrestApply2(36988, 18, (long long )((unsigned long )(b - 1)));
      __CrestLoad(36987, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
      __CrestApply2(36986, 18, (long long )((b - 1) - fixup_alt_jump));
      __CrestLoad(36985, (unsigned long )0, (long long )3L);
      __CrestApply2(36984, 1, (long long )(((b - 1) - fixup_alt_jump) - 3L));
# 2399 "regex.c"
      store_op1((re_opcode_t )14, fixup_alt_jump, (int )(((b - 1) - fixup_alt_jump) - 3L));
      __CrestClearStack(36991);
    } else {
      __CrestBranch(36924, 8847, 0);

    }
    }
    {
    __CrestLoad(36994, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
    __CrestLoad(36993, (unsigned long )0, (long long )0U);
    __CrestApply2(36992, 12, (long long )(compile_stack.avail == 0U));
# 2403 "regex.c"
    if (compile_stack.avail == 0U) {
      __CrestBranch(36995, 8849, 1);
      {
      __CrestLoad(37001, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(37000, (unsigned long )0, (long long )(((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(36999, 5, (long long )(syntax & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(36998, (unsigned long )0, (long long )0);
      __CrestApply2(36997, 13, (long long )((syntax & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2404 "regex.c"
      if ((syntax & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(37002, 8850, 1);
# 2405 "regex.c"
        goto normal_char;
      } else {
        __CrestBranch(37003, 8851, 0);
# 2407 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(37004);
        __CrestLoad(37005, (unsigned long )0, (long long )((reg_errcode_t )16));
        __CrestStore(37006, (unsigned long )(& __retres238));
# 2407 "regex.c"
        __retres238 = (reg_errcode_t )16;
# 2407 "regex.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(36996, 8854, 0);

    }
    }
    __CrestLoad(37009, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
    __CrestLoad(37008, (unsigned long )0, (long long )1U);
    __CrestApply2(37007, 1, (long long )(compile_stack.avail - 1U));
    __CrestStore(37010, (unsigned long )(& compile_stack.avail));
# 2418 "regex.c"
    (compile_stack.avail) --;
# 2419 "regex.c"
    mem_218 = compile_stack.stack + compile_stack.avail;
# 2419 "regex.c"
    begalt = bufp->buffer + mem_218->begalt_offset;
    {
# 2420 "regex.c"
    mem_219 = compile_stack.stack + compile_stack.avail;
    {
    __CrestLoad(37013, (unsigned long )(& mem_219->fixup_alt_jump), (long long )mem_219->fixup_alt_jump);
    __CrestLoad(37012, (unsigned long )0, (long long )0);
    __CrestApply2(37011, 13, (long long )(mem_219->fixup_alt_jump != 0));
# 2420 "regex.c"
    if (mem_219->fixup_alt_jump != 0) {
      __CrestBranch(37014, 8859, 1);
# 2420 "regex.c"
      mem_220 = compile_stack.stack + compile_stack.avail;
# 2420 "regex.c"
      fixup_alt_jump = (bufp->buffer + mem_220->fixup_alt_jump) - 1;
    } else {
      __CrestBranch(37015, 8860, 0);
# 2420 "regex.c"
      fixup_alt_jump = (unsigned char *)0;
    }
    }
    }
# 2424 "regex.c"
    mem_221 = compile_stack.stack + compile_stack.avail;
# 2424 "regex.c"
    laststart = bufp->buffer + mem_221->laststart_offset;
# 2425 "regex.c"
    mem_222 = compile_stack.stack + compile_stack.avail;
    __CrestLoad(37016, (unsigned long )(& mem_222->regnum), (long long )mem_222->regnum);
    __CrestStore(37017, (unsigned long )(& this_group_regnum));
# 2425 "regex.c"
    this_group_regnum = mem_222->regnum;
# 2429 "regex.c"
    pending_exact = (unsigned char *)0;
    {
    __CrestLoad(37020, (unsigned long )(& this_group_regnum), (long long )this_group_regnum);
    __CrestLoad(37019, (unsigned long )0, (long long )255U);
    __CrestApply2(37018, 15, (long long )(this_group_regnum <= 255U));
# 2433 "regex.c"
    if (this_group_regnum <= 255U) {
      __CrestBranch(37021, 8863, 1);
# 2435 "regex.c"
      mem_223 = compile_stack.stack + compile_stack.avail;
# 2435 "regex.c"
      inner_group_loc = bufp->buffer + mem_223->inner_group_offset;
      __CrestLoad(37025, (unsigned long )(& regnum), (long long )regnum);
      __CrestLoad(37024, (unsigned long )(& this_group_regnum), (long long )this_group_regnum);
      __CrestApply2(37023, 1, (long long )(regnum - this_group_regnum));
      __CrestStore(37026, (unsigned long )inner_group_loc);
# 2438 "regex.c"
      *inner_group_loc = (unsigned char )(regnum - this_group_regnum);
      {
# 2439 "regex.c"
      while (1) {
        while_continue___45: ;
        {
# 2439 "regex.c"
        while (1) {
          while_continue___46: ;
          {
          __CrestLoad(37033, (unsigned long )(& b), (long long )((unsigned long )b));
          __CrestLoad(37032, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37031, 18, (long long )(b - bufp->buffer));
          __CrestLoad(37030, (unsigned long )0, (long long )3L);
          __CrestApply2(37029, 0, (long long )((b - bufp->buffer) + 3L));
          __CrestLoad(37028, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestApply2(37027, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2439 "regex.c"
          if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
            __CrestBranch(37034, 8871, 1);

          } else {
            __CrestBranch(37035, 8872, 0);
# 2439 "regex.c"
            goto while_break___46;
          }
          }
          {
# 2439 "regex.c"
          while (1) {
            while_continue___47: ;
# 2439 "regex.c"
            old_buffer___10 = bufp->buffer;
            {
            __CrestLoad(37038, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(37037, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestApply2(37036, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2439 "regex.c"
            if (bufp->allocated == (unsigned long )(1L << 16)) {
              __CrestBranch(37039, 8878, 1);
              __CrestLoad(37041, (unsigned long )0, (long long )((reg_errcode_t )15));
              __CrestStore(37042, (unsigned long )(& __retres238));
# 2439 "regex.c"
              __retres238 = (reg_errcode_t )15;
# 2439 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(37040, 8880, 0);

            }
            }
            __CrestLoad(37045, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(37044, (unsigned long )0, (long long )1);
            __CrestApply2(37043, 8, (long long )(bufp->allocated << 1));
            __CrestStore(37046, (unsigned long )(& bufp->allocated));
# 2439 "regex.c"
            bufp->allocated <<= 1;
            {
            __CrestLoad(37049, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
            __CrestLoad(37048, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestApply2(37047, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2439 "regex.c"
            if (bufp->allocated > (unsigned long )(1L << 16)) {
              __CrestBranch(37050, 8883, 1);
              __CrestLoad(37052, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
              __CrestStore(37053, (unsigned long )(& bufp->allocated));
# 2439 "regex.c"
              bufp->allocated = (unsigned long )(1L << 16);
            } else {
              __CrestBranch(37051, 8884, 0);

            }
            }
            __CrestLoad(37054, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2439 "regex.c"
            tmp___51 = realloc((void *)bufp->buffer, bufp->allocated);
            __CrestClearStack(37055);
# 2439 "regex.c"
            bufp->buffer = (unsigned char *)tmp___51;
            {
            __CrestLoad(37058, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
            __CrestLoad(37057, (unsigned long )0, (long long )((unsigned long )((void *)0)));
            __CrestApply2(37056, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2439 "regex.c"
            if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
              __CrestBranch(37059, 8887, 1);
              __CrestLoad(37061, (unsigned long )0, (long long )((reg_errcode_t )12));
              __CrestStore(37062, (unsigned long )(& __retres238));
# 2439 "regex.c"
              __retres238 = (reg_errcode_t )12;
# 2439 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(37060, 8889, 0);

            }
            }
            {
            __CrestLoad(37065, (unsigned long )(& old_buffer___10), (long long )((unsigned long )old_buffer___10));
            __CrestLoad(37064, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
            __CrestApply2(37063, 13, (long long )((unsigned long )old_buffer___10 != (unsigned long )bufp->buffer));
# 2439 "regex.c"
            if ((unsigned long )old_buffer___10 != (unsigned long )bufp->buffer) {
              __CrestBranch(37066, 8891, 1);
# 2439 "regex.c"
              b = bufp->buffer + (b - old_buffer___10);
# 2439 "regex.c"
              begalt = bufp->buffer + (begalt - old_buffer___10);
              {
              __CrestLoad(37070, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
              __CrestLoad(37069, (unsigned long )0, (long long )0);
              __CrestApply2(37068, 13, (long long )(fixup_alt_jump != 0));
# 2439 "regex.c"
              if (fixup_alt_jump != 0) {
                __CrestBranch(37071, 8893, 1);
# 2439 "regex.c"
                fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___10);
              } else {
                __CrestBranch(37072, 8894, 0);

              }
              }
              {
              __CrestLoad(37075, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
              __CrestLoad(37074, (unsigned long )0, (long long )0);
              __CrestApply2(37073, 13, (long long )(laststart != 0));
# 2439 "regex.c"
              if (laststart != 0) {
                __CrestBranch(37076, 8896, 1);
# 2439 "regex.c"
                laststart = bufp->buffer + (laststart - old_buffer___10);
              } else {
                __CrestBranch(37077, 8897, 0);

              }
              }
              {
              __CrestLoad(37080, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
              __CrestLoad(37079, (unsigned long )0, (long long )0);
              __CrestApply2(37078, 13, (long long )(pending_exact != 0));
# 2439 "regex.c"
              if (pending_exact != 0) {
                __CrestBranch(37081, 8899, 1);
# 2439 "regex.c"
                pending_exact = bufp->buffer + (pending_exact - old_buffer___10);
              } else {
                __CrestBranch(37082, 8900, 0);

              }
              }
            } else {
              __CrestBranch(37067, 8901, 0);

            }
            }
# 2439 "regex.c"
            goto while_break___47;
          }
          while_break___47: ;
          }
        }
        while_break___46: ;
        }
# 2439 "regex.c"
        tmp___52 = b;
# 2439 "regex.c"
        b ++;
        __CrestLoad(37083, (unsigned long )0, (long long )(unsigned char)7);
        __CrestStore(37084, (unsigned long )tmp___52);
# 2439 "regex.c"
        *tmp___52 = (unsigned char)7;
# 2439 "regex.c"
        tmp___53 = b;
# 2439 "regex.c"
        b ++;
        __CrestLoad(37085, (unsigned long )(& this_group_regnum), (long long )this_group_regnum);
        __CrestStore(37086, (unsigned long )tmp___53);
# 2439 "regex.c"
        *tmp___53 = (unsigned char )this_group_regnum;
# 2439 "regex.c"
        tmp___54 = b;
# 2439 "regex.c"
        b ++;
        __CrestLoad(37089, (unsigned long )(& regnum), (long long )regnum);
        __CrestLoad(37088, (unsigned long )(& this_group_regnum), (long long )this_group_regnum);
        __CrestApply2(37087, 1, (long long )(regnum - this_group_regnum));
        __CrestStore(37090, (unsigned long )tmp___54);
# 2439 "regex.c"
        *tmp___54 = (unsigned char )(regnum - this_group_regnum);
# 2439 "regex.c"
        goto while_break___45;
      }
      while_break___45: ;
      }
    } else {
      __CrestBranch(37022, 8908, 0);

    }
    }
# 2443 "regex.c"
    goto switch_break___0;
    case_124___0:
    {
    __CrestLoad(37095, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(37094, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37093, 5, (long long )(syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37092, (unsigned long )0, (long long )0);
    __CrestApply2(37091, 13, (long long )((syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2447 "regex.c"
    if ((syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(37096, 8911, 1);
# 2448 "regex.c"
      goto normal_backslash;
    } else {
      __CrestBranch(37097, 8912, 0);
      {
      __CrestLoad(37102, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(37101, (unsigned long )0, (long long )(((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(37100, 5, (long long )(syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(37099, (unsigned long )0, (long long )0);
      __CrestApply2(37098, 13, (long long )((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2447 "regex.c"
      if ((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(37103, 8913, 1);
# 2448 "regex.c"
        goto normal_backslash;
      } else {
        __CrestBranch(37104, 8914, 0);

      }
      }
    }
    }
    handle_alt:
    {
    __CrestLoad(37109, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(37108, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37107, 5, (long long )(syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37106, (unsigned long )0, (long long )0);
    __CrestApply2(37105, 13, (long long )((syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2450 "regex.c"
    if ((syntax & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(37110, 8916, 1);
# 2451 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(37111, 8917, 0);

    }
    }
    {
# 2455 "regex.c"
    while (1) {
      while_continue___48: ;
      {
      __CrestLoad(37118, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(37117, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
      __CrestApply2(37116, 18, (long long )(b - bufp->buffer));
      __CrestLoad(37115, (unsigned long )0, (long long )3L);
      __CrestApply2(37114, 0, (long long )((b - bufp->buffer) + 3L));
      __CrestLoad(37113, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
      __CrestApply2(37112, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2455 "regex.c"
      if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
        __CrestBranch(37119, 8922, 1);

      } else {
        __CrestBranch(37120, 8923, 0);
# 2455 "regex.c"
        goto while_break___48;
      }
      }
      {
# 2455 "regex.c"
      while (1) {
        while_continue___49: ;
# 2455 "regex.c"
        old_buffer___11 = bufp->buffer;
        {
        __CrestLoad(37123, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(37122, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(37121, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2455 "regex.c"
        if (bufp->allocated == (unsigned long )(1L << 16)) {
          __CrestBranch(37124, 8929, 1);
          __CrestLoad(37126, (unsigned long )0, (long long )((reg_errcode_t )15));
          __CrestStore(37127, (unsigned long )(& __retres238));
# 2455 "regex.c"
          __retres238 = (reg_errcode_t )15;
# 2455 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(37125, 8931, 0);

        }
        }
        __CrestLoad(37130, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(37129, (unsigned long )0, (long long )1);
        __CrestApply2(37128, 8, (long long )(bufp->allocated << 1));
        __CrestStore(37131, (unsigned long )(& bufp->allocated));
# 2455 "regex.c"
        bufp->allocated <<= 1;
        {
        __CrestLoad(37134, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(37133, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(37132, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2455 "regex.c"
        if (bufp->allocated > (unsigned long )(1L << 16)) {
          __CrestBranch(37135, 8934, 1);
          __CrestLoad(37137, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestStore(37138, (unsigned long )(& bufp->allocated));
# 2455 "regex.c"
          bufp->allocated = (unsigned long )(1L << 16);
        } else {
          __CrestBranch(37136, 8935, 0);

        }
        }
        __CrestLoad(37139, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2455 "regex.c"
        tmp___55 = realloc((void *)bufp->buffer, bufp->allocated);
        __CrestClearStack(37140);
# 2455 "regex.c"
        bufp->buffer = (unsigned char *)tmp___55;
        {
        __CrestLoad(37143, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestLoad(37142, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(37141, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2455 "regex.c"
        if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
          __CrestBranch(37144, 8938, 1);
          __CrestLoad(37146, (unsigned long )0, (long long )((reg_errcode_t )12));
          __CrestStore(37147, (unsigned long )(& __retres238));
# 2455 "regex.c"
          __retres238 = (reg_errcode_t )12;
# 2455 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(37145, 8940, 0);

        }
        }
        {
        __CrestLoad(37150, (unsigned long )(& old_buffer___11), (long long )((unsigned long )old_buffer___11));
        __CrestLoad(37149, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37148, 13, (long long )((unsigned long )old_buffer___11 != (unsigned long )bufp->buffer));
# 2455 "regex.c"
        if ((unsigned long )old_buffer___11 != (unsigned long )bufp->buffer) {
          __CrestBranch(37151, 8942, 1);
# 2455 "regex.c"
          b = bufp->buffer + (b - old_buffer___11);
# 2455 "regex.c"
          begalt = bufp->buffer + (begalt - old_buffer___11);
          {
          __CrestLoad(37155, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
          __CrestLoad(37154, (unsigned long )0, (long long )0);
          __CrestApply2(37153, 13, (long long )(fixup_alt_jump != 0));
# 2455 "regex.c"
          if (fixup_alt_jump != 0) {
            __CrestBranch(37156, 8944, 1);
# 2455 "regex.c"
            fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___11);
          } else {
            __CrestBranch(37157, 8945, 0);

          }
          }
          {
          __CrestLoad(37160, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
          __CrestLoad(37159, (unsigned long )0, (long long )0);
          __CrestApply2(37158, 13, (long long )(laststart != 0));
# 2455 "regex.c"
          if (laststart != 0) {
            __CrestBranch(37161, 8947, 1);
# 2455 "regex.c"
            laststart = bufp->buffer + (laststart - old_buffer___11);
          } else {
            __CrestBranch(37162, 8948, 0);

          }
          }
          {
          __CrestLoad(37165, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
          __CrestLoad(37164, (unsigned long )0, (long long )0);
          __CrestApply2(37163, 13, (long long )(pending_exact != 0));
# 2455 "regex.c"
          if (pending_exact != 0) {
            __CrestBranch(37166, 8950, 1);
# 2455 "regex.c"
            pending_exact = bufp->buffer + (pending_exact - old_buffer___11);
          } else {
            __CrestBranch(37167, 8951, 0);

          }
          }
        } else {
          __CrestBranch(37152, 8952, 0);

        }
        }
# 2455 "regex.c"
        goto while_break___49;
      }
      while_break___49: ;
      }
    }
    while_break___48: ;
    }
    __CrestLoad(37168, (unsigned long )0, (long long )((re_opcode_t )15));
    __CrestLoad(37175, (unsigned long )(& b), (long long )((unsigned long )b));
    __CrestLoad(37174, (unsigned long )0, (long long )6);
    __CrestApply2(37173, 18, (long long )((unsigned long )(b + 6)));
    __CrestLoad(37172, (unsigned long )(& begalt), (long long )((unsigned long )begalt));
    __CrestApply2(37171, 18, (long long )((b + 6) - begalt));
    __CrestLoad(37170, (unsigned long )0, (long long )3L);
    __CrestApply2(37169, 1, (long long )(((b + 6) - begalt) - 3L));
# 2456 "regex.c"
    insert_op1((re_opcode_t )15, begalt, (int )(((b + 6) - begalt) - 3L), b);
    __CrestClearStack(37176);
# 2457 "regex.c"
    pending_exact = (unsigned char *)0;
# 2458 "regex.c"
    b += 3;
    {
    __CrestLoad(37179, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
    __CrestLoad(37178, (unsigned long )0, (long long )0);
    __CrestApply2(37177, 13, (long long )(fixup_alt_jump != 0));
# 2476 "regex.c"
    if (fixup_alt_jump != 0) {
      __CrestBranch(37180, 8958, 1);
      __CrestLoad(37182, (unsigned long )0, (long long )((re_opcode_t )14));
      __CrestLoad(37187, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(37186, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
      __CrestApply2(37185, 18, (long long )(b - fixup_alt_jump));
      __CrestLoad(37184, (unsigned long )0, (long long )3L);
      __CrestApply2(37183, 1, (long long )((b - fixup_alt_jump) - 3L));
# 2477 "regex.c"
      store_op1((re_opcode_t )14, fixup_alt_jump, (int )((b - fixup_alt_jump) - 3L));
      __CrestClearStack(37188);
    } else {
      __CrestBranch(37181, 8959, 0);

    }
    }
# 2482 "regex.c"
    fixup_alt_jump = b;
    {
# 2483 "regex.c"
    while (1) {
      while_continue___50: ;
      {
      __CrestLoad(37195, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(37194, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
      __CrestApply2(37193, 18, (long long )(b - bufp->buffer));
      __CrestLoad(37192, (unsigned long )0, (long long )3L);
      __CrestApply2(37191, 0, (long long )((b - bufp->buffer) + 3L));
      __CrestLoad(37190, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
      __CrestApply2(37189, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2483 "regex.c"
      if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
        __CrestBranch(37196, 8965, 1);

      } else {
        __CrestBranch(37197, 8966, 0);
# 2483 "regex.c"
        goto while_break___50;
      }
      }
      {
# 2483 "regex.c"
      while (1) {
        while_continue___51: ;
# 2483 "regex.c"
        old_buffer___12 = bufp->buffer;
        {
        __CrestLoad(37200, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(37199, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(37198, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2483 "regex.c"
        if (bufp->allocated == (unsigned long )(1L << 16)) {
          __CrestBranch(37201, 8972, 1);
          __CrestLoad(37203, (unsigned long )0, (long long )((reg_errcode_t )15));
          __CrestStore(37204, (unsigned long )(& __retres238));
# 2483 "regex.c"
          __retres238 = (reg_errcode_t )15;
# 2483 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(37202, 8974, 0);

        }
        }
        __CrestLoad(37207, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(37206, (unsigned long )0, (long long )1);
        __CrestApply2(37205, 8, (long long )(bufp->allocated << 1));
        __CrestStore(37208, (unsigned long )(& bufp->allocated));
# 2483 "regex.c"
        bufp->allocated <<= 1;
        {
        __CrestLoad(37211, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestLoad(37210, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
        __CrestApply2(37209, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2483 "regex.c"
        if (bufp->allocated > (unsigned long )(1L << 16)) {
          __CrestBranch(37212, 8977, 1);
          __CrestLoad(37214, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestStore(37215, (unsigned long )(& bufp->allocated));
# 2483 "regex.c"
          bufp->allocated = (unsigned long )(1L << 16);
        } else {
          __CrestBranch(37213, 8978, 0);

        }
        }
        __CrestLoad(37216, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2483 "regex.c"
        tmp___56 = realloc((void *)bufp->buffer, bufp->allocated);
        __CrestClearStack(37217);
# 2483 "regex.c"
        bufp->buffer = (unsigned char *)tmp___56;
        {
        __CrestLoad(37220, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestLoad(37219, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(37218, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2483 "regex.c"
        if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
          __CrestBranch(37221, 8981, 1);
          __CrestLoad(37223, (unsigned long )0, (long long )((reg_errcode_t )12));
          __CrestStore(37224, (unsigned long )(& __retres238));
# 2483 "regex.c"
          __retres238 = (reg_errcode_t )12;
# 2483 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(37222, 8983, 0);

        }
        }
        {
        __CrestLoad(37227, (unsigned long )(& old_buffer___12), (long long )((unsigned long )old_buffer___12));
        __CrestLoad(37226, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37225, 13, (long long )((unsigned long )old_buffer___12 != (unsigned long )bufp->buffer));
# 2483 "regex.c"
        if ((unsigned long )old_buffer___12 != (unsigned long )bufp->buffer) {
          __CrestBranch(37228, 8985, 1);
# 2483 "regex.c"
          b = bufp->buffer + (b - old_buffer___12);
# 2483 "regex.c"
          begalt = bufp->buffer + (begalt - old_buffer___12);
          {
          __CrestLoad(37232, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
          __CrestLoad(37231, (unsigned long )0, (long long )0);
          __CrestApply2(37230, 13, (long long )(fixup_alt_jump != 0));
# 2483 "regex.c"
          if (fixup_alt_jump != 0) {
            __CrestBranch(37233, 8987, 1);
# 2483 "regex.c"
            fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___12);
          } else {
            __CrestBranch(37234, 8988, 0);

          }
          }
          {
          __CrestLoad(37237, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
          __CrestLoad(37236, (unsigned long )0, (long long )0);
          __CrestApply2(37235, 13, (long long )(laststart != 0));
# 2483 "regex.c"
          if (laststart != 0) {
            __CrestBranch(37238, 8990, 1);
# 2483 "regex.c"
            laststart = bufp->buffer + (laststart - old_buffer___12);
          } else {
            __CrestBranch(37239, 8991, 0);

          }
          }
          {
          __CrestLoad(37242, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
          __CrestLoad(37241, (unsigned long )0, (long long )0);
          __CrestApply2(37240, 13, (long long )(pending_exact != 0));
# 2483 "regex.c"
          if (pending_exact != 0) {
            __CrestBranch(37243, 8993, 1);
# 2483 "regex.c"
            pending_exact = bufp->buffer + (pending_exact - old_buffer___12);
          } else {
            __CrestBranch(37244, 8994, 0);

          }
          }
        } else {
          __CrestBranch(37229, 8995, 0);

        }
        }
# 2483 "regex.c"
        goto while_break___51;
      }
      while_break___51: ;
      }
    }
    while_break___50: ;
    }
# 2484 "regex.c"
    b += 3;
# 2486 "regex.c"
    laststart = (unsigned char *)0;
# 2487 "regex.c"
    begalt = b;
# 2488 "regex.c"
    goto switch_break___0;
    case_123___0:
    {
    __CrestLoad(37249, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(37248, (unsigned long )0, (long long )(((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37247, 5, (long long )(syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37246, (unsigned long )0, (long long )0);
    __CrestApply2(37245, 12, (long long )((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 2493 "regex.c"
    if ((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(37250, 9002, 1);
# 2498 "regex.c"
      goto normal_backslash;
    } else {
      __CrestBranch(37251, 9003, 0);
      {
      __CrestLoad(37256, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(37255, (unsigned long )0, (long long )(((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(37254, 5, (long long )(syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(37253, (unsigned long )0, (long long )0);
      __CrestApply2(37252, 13, (long long )((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2493 "regex.c"
      if ((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(37257, 9004, 1);
        {
        __CrestLoad(37263, (unsigned long )(& syntax), (long long )syntax);
        __CrestLoad(37262, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
        __CrestApply2(37261, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
        __CrestLoad(37260, (unsigned long )0, (long long )0);
        __CrestApply2(37259, 13, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2493 "regex.c"
        if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
          __CrestBranch(37264, 9005, 1);
# 2498 "regex.c"
          goto normal_backslash;
        } else {
          __CrestBranch(37265, 9006, 0);
# 2493 "regex.c"
          goto _L___12;
        }
        }
      } else {
        __CrestBranch(37258, 9007, 0);
        _L___12:
        {
        __CrestLoad(37270, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(37269, (unsigned long )0, (long long )2);
        __CrestApply2(37268, 18, (long long )((unsigned long )(p - 2)));
        __CrestLoad(37267, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
        __CrestApply2(37266, 12, (long long )((unsigned long )(p - 2) == (unsigned long )pattern));
# 2493 "regex.c"
        if ((unsigned long )(p - 2) == (unsigned long )pattern) {
          __CrestBranch(37271, 9008, 1);
          {
          __CrestLoad(37275, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(37274, (unsigned long )(& pend), (long long )((unsigned long )pend));
          __CrestApply2(37273, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2493 "regex.c"
          if ((unsigned long )p == (unsigned long )pend) {
            __CrestBranch(37276, 9009, 1);
# 2498 "regex.c"
            goto normal_backslash;
          } else {
            __CrestBranch(37277, 9010, 0);

          }
          }
        } else {
          __CrestBranch(37272, 9011, 0);

        }
        }
      }
      }
    }
    }
    handle_interval:
    __CrestLoad(37278, (unsigned long )0, (long long )-1);
    __CrestStore(37279, (unsigned long )(& lower_bound));
# 2505 "regex.c"
    lower_bound = -1;
    __CrestLoad(37280, (unsigned long )0, (long long )-1);
    __CrestStore(37281, (unsigned long )(& upper_bound));
# 2505 "regex.c"
    upper_bound = -1;
# 2507 "regex.c"
    beg_interval = p - 1;
    {
    __CrestLoad(37284, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(37283, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(37282, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2509 "regex.c"
    if ((unsigned long )p == (unsigned long )pend) {
      __CrestBranch(37285, 9014, 1);
      {
      __CrestLoad(37291, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(37290, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(37289, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(37288, (unsigned long )0, (long long )0);
      __CrestApply2(37287, 13, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2511 "regex.c"
      if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(37292, 9015, 1);
# 2512 "regex.c"
        goto unfetch_interval;
      } else {
        __CrestBranch(37293, 9016, 0);
# 2514 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(37294);
        __CrestLoad(37295, (unsigned long )0, (long long )((reg_errcode_t )9));
        __CrestStore(37296, (unsigned long )(& __retres238));
# 2514 "regex.c"
        __retres238 = (reg_errcode_t )9;
# 2514 "regex.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(37286, 9019, 0);

    }
    }
    {
    __CrestLoad(37299, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(37298, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(37297, 13, (long long )((unsigned long )p != (unsigned long )pend));
# 2517 "regex.c"
    if ((unsigned long )p != (unsigned long )pend) {
      __CrestBranch(37300, 9021, 1);
      {
# 2517 "regex.c"
      while (1) {
        while_continue___52: ;
        {
        __CrestLoad(37304, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(37303, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(37302, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2517 "regex.c"
        if ((unsigned long )p == (unsigned long )pend) {
          __CrestBranch(37305, 9025, 1);
          __CrestLoad(37307, (unsigned long )0, (long long )((reg_errcode_t )14));
          __CrestStore(37308, (unsigned long )(& __retres238));
# 2517 "regex.c"
          __retres238 = (reg_errcode_t )14;
# 2517 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(37306, 9027, 0);

        }
        }
# 2517 "regex.c"
        tmp___57 = p;
# 2517 "regex.c"
        p ++;
        __CrestLoad(37309, (unsigned long )tmp___57, (long long )*tmp___57);
        __CrestStore(37310, (unsigned long )(& c));
# 2517 "regex.c"
        c = (unsigned char )*tmp___57;
        {
        __CrestLoad(37313, (unsigned long )(& translate), (long long )((unsigned long )translate));
        __CrestLoad(37312, (unsigned long )0, (long long )0);
        __CrestApply2(37311, 13, (long long )(translate != 0));
# 2517 "regex.c"
        if (translate != 0) {
          __CrestBranch(37314, 9030, 1);
# 2517 "regex.c"
          mem_224 = translate + c;
          __CrestLoad(37316, (unsigned long )mem_224, (long long )*mem_224);
          __CrestStore(37317, (unsigned long )(& c));
# 2517 "regex.c"
          c = (unsigned char )*mem_224;
        } else {
          __CrestBranch(37315, 9031, 0);

        }
        }
# 2517 "regex.c"
        goto while_break___52;
      }
      while_break___52: ;
      }
      {
# 2517 "regex.c"
      while (1) {
        while_continue___53: ;
# 2517 "regex.c"
        tmp___59 = __ctype_b_loc();
        __CrestClearStack(37318);
        {
# 2517 "regex.c"
        mem_225 = *tmp___59 + (int )c;
        {
        __CrestLoad(37323, (unsigned long )mem_225, (long long )*mem_225);
        __CrestLoad(37322, (unsigned long )0, (long long )2048);
        __CrestApply2(37321, 5, (long long )((int const )*mem_225 & 2048));
        __CrestLoad(37320, (unsigned long )0, (long long )0);
        __CrestApply2(37319, 13, (long long )(((int const )*mem_225 & 2048) != 0));
# 2517 "regex.c"
        if (((int const )*mem_225 & 2048) != 0) {
          __CrestBranch(37324, 9041, 1);

        } else {
          __CrestBranch(37325, 9042, 0);
# 2517 "regex.c"
          goto while_break___53;
        }
        }
        }
        {
        __CrestLoad(37328, (unsigned long )(& lower_bound), (long long )lower_bound);
        __CrestLoad(37327, (unsigned long )0, (long long )0);
        __CrestApply2(37326, 16, (long long )(lower_bound < 0));
# 2517 "regex.c"
        if (lower_bound < 0) {
          __CrestBranch(37329, 9044, 1);
          __CrestLoad(37331, (unsigned long )0, (long long )0);
          __CrestStore(37332, (unsigned long )(& lower_bound));
# 2517 "regex.c"
          lower_bound = 0;
        } else {
          __CrestBranch(37330, 9045, 0);

        }
        }
        __CrestLoad(37339, (unsigned long )(& lower_bound), (long long )lower_bound);
        __CrestLoad(37338, (unsigned long )0, (long long )10);
        __CrestApply2(37337, 2, (long long )(lower_bound * 10));
        __CrestLoad(37336, (unsigned long )(& c), (long long )c);
        __CrestApply2(37335, 0, (long long )(lower_bound * 10 + (int )c));
        __CrestLoad(37334, (unsigned long )0, (long long )48);
        __CrestApply2(37333, 1, (long long )((lower_bound * 10 + (int )c) - 48));
        __CrestStore(37340, (unsigned long )(& lower_bound));
# 2517 "regex.c"
        lower_bound = (lower_bound * 10 + (int )c) - 48;
        {
        __CrestLoad(37343, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(37342, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(37341, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2517 "regex.c"
        if ((unsigned long )p == (unsigned long )pend) {
          __CrestBranch(37344, 9048, 1);
# 2517 "regex.c"
          goto while_break___53;
        } else {
          __CrestBranch(37345, 9049, 0);

        }
        }
        {
# 2517 "regex.c"
        while (1) {
          while_continue___54: ;
          {
          __CrestLoad(37348, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(37347, (unsigned long )(& pend), (long long )((unsigned long )pend));
          __CrestApply2(37346, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2517 "regex.c"
          if ((unsigned long )p == (unsigned long )pend) {
            __CrestBranch(37349, 9054, 1);
            __CrestLoad(37351, (unsigned long )0, (long long )((reg_errcode_t )14));
            __CrestStore(37352, (unsigned long )(& __retres238));
# 2517 "regex.c"
            __retres238 = (reg_errcode_t )14;
# 2517 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37350, 9056, 0);

          }
          }
# 2517 "regex.c"
          tmp___58 = p;
# 2517 "regex.c"
          p ++;
          __CrestLoad(37353, (unsigned long )tmp___58, (long long )*tmp___58);
          __CrestStore(37354, (unsigned long )(& c));
# 2517 "regex.c"
          c = (unsigned char )*tmp___58;
          {
          __CrestLoad(37357, (unsigned long )(& translate), (long long )((unsigned long )translate));
          __CrestLoad(37356, (unsigned long )0, (long long )0);
          __CrestApply2(37355, 13, (long long )(translate != 0));
# 2517 "regex.c"
          if (translate != 0) {
            __CrestBranch(37358, 9059, 1);
# 2517 "regex.c"
            mem_226 = translate + c;
            __CrestLoad(37360, (unsigned long )mem_226, (long long )*mem_226);
            __CrestStore(37361, (unsigned long )(& c));
# 2517 "regex.c"
            c = (unsigned char )*mem_226;
          } else {
            __CrestBranch(37359, 9060, 0);

          }
          }
# 2517 "regex.c"
          goto while_break___54;
        }
        while_break___54: ;
        }
      }
      while_break___53: ;
      }
    } else {
      __CrestBranch(37301, 9064, 0);

    }
    }
    {
    __CrestLoad(37364, (unsigned long )(& c), (long long )c);
    __CrestLoad(37363, (unsigned long )0, (long long )44);
    __CrestApply2(37362, 12, (long long )((int )c == 44));
# 2519 "regex.c"
    if ((int )c == 44) {
      __CrestBranch(37365, 9066, 1);
      {
      __CrestLoad(37369, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(37368, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(37367, 13, (long long )((unsigned long )p != (unsigned long )pend));
# 2521 "regex.c"
      if ((unsigned long )p != (unsigned long )pend) {
        __CrestBranch(37370, 9067, 1);
        {
# 2521 "regex.c"
        while (1) {
          while_continue___55: ;
          {
          __CrestLoad(37374, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(37373, (unsigned long )(& pend), (long long )((unsigned long )pend));
          __CrestApply2(37372, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2521 "regex.c"
          if ((unsigned long )p == (unsigned long )pend) {
            __CrestBranch(37375, 9071, 1);
            __CrestLoad(37377, (unsigned long )0, (long long )((reg_errcode_t )14));
            __CrestStore(37378, (unsigned long )(& __retres238));
# 2521 "regex.c"
            __retres238 = (reg_errcode_t )14;
# 2521 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37376, 9073, 0);

          }
          }
# 2521 "regex.c"
          tmp___60 = p;
# 2521 "regex.c"
          p ++;
          __CrestLoad(37379, (unsigned long )tmp___60, (long long )*tmp___60);
          __CrestStore(37380, (unsigned long )(& c));
# 2521 "regex.c"
          c = (unsigned char )*tmp___60;
          {
          __CrestLoad(37383, (unsigned long )(& translate), (long long )((unsigned long )translate));
          __CrestLoad(37382, (unsigned long )0, (long long )0);
          __CrestApply2(37381, 13, (long long )(translate != 0));
# 2521 "regex.c"
          if (translate != 0) {
            __CrestBranch(37384, 9076, 1);
# 2521 "regex.c"
            mem_227 = translate + c;
            __CrestLoad(37386, (unsigned long )mem_227, (long long )*mem_227);
            __CrestStore(37387, (unsigned long )(& c));
# 2521 "regex.c"
            c = (unsigned char )*mem_227;
          } else {
            __CrestBranch(37385, 9077, 0);

          }
          }
# 2521 "regex.c"
          goto while_break___55;
        }
        while_break___55: ;
        }
        {
# 2521 "regex.c"
        while (1) {
          while_continue___56: ;
# 2521 "regex.c"
          tmp___62 = __ctype_b_loc();
          __CrestClearStack(37388);
          {
# 2521 "regex.c"
          mem_228 = *tmp___62 + (int )c;
          {
          __CrestLoad(37393, (unsigned long )mem_228, (long long )*mem_228);
          __CrestLoad(37392, (unsigned long )0, (long long )2048);
          __CrestApply2(37391, 5, (long long )((int const )*mem_228 & 2048));
          __CrestLoad(37390, (unsigned long )0, (long long )0);
          __CrestApply2(37389, 13, (long long )(((int const )*mem_228 & 2048) != 0));
# 2521 "regex.c"
          if (((int const )*mem_228 & 2048) != 0) {
            __CrestBranch(37394, 9087, 1);

          } else {
            __CrestBranch(37395, 9088, 0);
# 2521 "regex.c"
            goto while_break___56;
          }
          }
          }
          {
          __CrestLoad(37398, (unsigned long )(& upper_bound), (long long )upper_bound);
          __CrestLoad(37397, (unsigned long )0, (long long )0);
          __CrestApply2(37396, 16, (long long )(upper_bound < 0));
# 2521 "regex.c"
          if (upper_bound < 0) {
            __CrestBranch(37399, 9090, 1);
            __CrestLoad(37401, (unsigned long )0, (long long )0);
            __CrestStore(37402, (unsigned long )(& upper_bound));
# 2521 "regex.c"
            upper_bound = 0;
          } else {
            __CrestBranch(37400, 9091, 0);

          }
          }
          __CrestLoad(37409, (unsigned long )(& upper_bound), (long long )upper_bound);
          __CrestLoad(37408, (unsigned long )0, (long long )10);
          __CrestApply2(37407, 2, (long long )(upper_bound * 10));
          __CrestLoad(37406, (unsigned long )(& c), (long long )c);
          __CrestApply2(37405, 0, (long long )(upper_bound * 10 + (int )c));
          __CrestLoad(37404, (unsigned long )0, (long long )48);
          __CrestApply2(37403, 1, (long long )((upper_bound * 10 + (int )c) - 48));
          __CrestStore(37410, (unsigned long )(& upper_bound));
# 2521 "regex.c"
          upper_bound = (upper_bound * 10 + (int )c) - 48;
          {
          __CrestLoad(37413, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(37412, (unsigned long )(& pend), (long long )((unsigned long )pend));
          __CrestApply2(37411, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2521 "regex.c"
          if ((unsigned long )p == (unsigned long )pend) {
            __CrestBranch(37414, 9094, 1);
# 2521 "regex.c"
            goto while_break___56;
          } else {
            __CrestBranch(37415, 9095, 0);

          }
          }
          {
# 2521 "regex.c"
          while (1) {
            while_continue___57: ;
            {
            __CrestLoad(37418, (unsigned long )(& p), (long long )((unsigned long )p));
            __CrestLoad(37417, (unsigned long )(& pend), (long long )((unsigned long )pend));
            __CrestApply2(37416, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2521 "regex.c"
            if ((unsigned long )p == (unsigned long )pend) {
              __CrestBranch(37419, 9100, 1);
              __CrestLoad(37421, (unsigned long )0, (long long )((reg_errcode_t )14));
              __CrestStore(37422, (unsigned long )(& __retres238));
# 2521 "regex.c"
              __retres238 = (reg_errcode_t )14;
# 2521 "regex.c"
              goto return_label;
            } else {
              __CrestBranch(37420, 9102, 0);

            }
            }
# 2521 "regex.c"
            tmp___61 = p;
# 2521 "regex.c"
            p ++;
            __CrestLoad(37423, (unsigned long )tmp___61, (long long )*tmp___61);
            __CrestStore(37424, (unsigned long )(& c));
# 2521 "regex.c"
            c = (unsigned char )*tmp___61;
            {
            __CrestLoad(37427, (unsigned long )(& translate), (long long )((unsigned long )translate));
            __CrestLoad(37426, (unsigned long )0, (long long )0);
            __CrestApply2(37425, 13, (long long )(translate != 0));
# 2521 "regex.c"
            if (translate != 0) {
              __CrestBranch(37428, 9105, 1);
# 2521 "regex.c"
              mem_229 = translate + c;
              __CrestLoad(37430, (unsigned long )mem_229, (long long )*mem_229);
              __CrestStore(37431, (unsigned long )(& c));
# 2521 "regex.c"
              c = (unsigned char )*mem_229;
            } else {
              __CrestBranch(37429, 9106, 0);

            }
            }
# 2521 "regex.c"
            goto while_break___57;
          }
          while_break___57: ;
          }
        }
        while_break___56: ;
        }
      } else {
        __CrestBranch(37371, 9110, 0);

      }
      }
      {
      __CrestLoad(37434, (unsigned long )(& upper_bound), (long long )upper_bound);
      __CrestLoad(37433, (unsigned long )0, (long long )0);
      __CrestApply2(37432, 16, (long long )(upper_bound < 0));
# 2522 "regex.c"
      if (upper_bound < 0) {
        __CrestBranch(37435, 9112, 1);
        __CrestLoad(37437, (unsigned long )0, (long long )32767);
        __CrestStore(37438, (unsigned long )(& upper_bound));
# 2522 "regex.c"
        upper_bound = 32767;
      } else {
        __CrestBranch(37436, 9113, 0);

      }
      }
    } else {
      __CrestBranch(37366, 9114, 0);
      __CrestLoad(37439, (unsigned long )(& lower_bound), (long long )lower_bound);
      __CrestStore(37440, (unsigned long )(& upper_bound));
# 2526 "regex.c"
      upper_bound = lower_bound;
    }
    }
    {
    __CrestLoad(37443, (unsigned long )(& lower_bound), (long long )lower_bound);
    __CrestLoad(37442, (unsigned long )0, (long long )0);
    __CrestApply2(37441, 16, (long long )(lower_bound < 0));
# 2528 "regex.c"
    if (lower_bound < 0) {
      __CrestBranch(37444, 9116, 1);
# 2528 "regex.c"
      goto _L___13;
    } else {
      __CrestBranch(37445, 9117, 0);
      {
      __CrestLoad(37448, (unsigned long )(& upper_bound), (long long )upper_bound);
      __CrestLoad(37447, (unsigned long )0, (long long )32767);
      __CrestApply2(37446, 14, (long long )(upper_bound > 32767));
# 2528 "regex.c"
      if (upper_bound > 32767) {
        __CrestBranch(37449, 9118, 1);
# 2528 "regex.c"
        goto _L___13;
      } else {
        __CrestBranch(37450, 9119, 0);
        {
        __CrestLoad(37453, (unsigned long )(& lower_bound), (long long )lower_bound);
        __CrestLoad(37452, (unsigned long )(& upper_bound), (long long )upper_bound);
        __CrestApply2(37451, 14, (long long )(lower_bound > upper_bound));
# 2528 "regex.c"
        if (lower_bound > upper_bound) {
          __CrestBranch(37454, 9120, 1);
          _L___13:
          {
          __CrestLoad(37460, (unsigned long )(& syntax), (long long )syntax);
          __CrestLoad(37459, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
          __CrestApply2(37458, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
          __CrestLoad(37457, (unsigned long )0, (long long )0);
          __CrestApply2(37456, 13, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2531 "regex.c"
          if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
            __CrestBranch(37461, 9121, 1);
# 2532 "regex.c"
            goto unfetch_interval;
          } else {
            __CrestBranch(37462, 9122, 0);
# 2534 "regex.c"
            free((void *)compile_stack.stack);
            __CrestClearStack(37463);
            __CrestLoad(37464, (unsigned long )0, (long long )((reg_errcode_t )10));
            __CrestStore(37465, (unsigned long )(& __retres238));
# 2534 "regex.c"
            __retres238 = (reg_errcode_t )10;
# 2534 "regex.c"
            goto return_label;
          }
          }
        } else {
          __CrestBranch(37455, 9125, 0);

        }
        }
      }
      }
    }
    }
    {
    __CrestLoad(37470, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(37469, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37468, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37467, (unsigned long )0, (long long )0);
    __CrestApply2(37466, 12, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 2537 "regex.c"
    if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(37471, 9127, 1);
      {
      __CrestLoad(37475, (unsigned long )(& c), (long long )c);
      __CrestLoad(37474, (unsigned long )0, (long long )92);
      __CrestApply2(37473, 13, (long long )((int )c != 92));
# 2539 "regex.c"
      if ((int )c != 92) {
        __CrestBranch(37476, 9128, 1);
# 2539 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(37478);
        __CrestLoad(37479, (unsigned long )0, (long long )((reg_errcode_t )9));
        __CrestStore(37480, (unsigned long )(& __retres238));
# 2539 "regex.c"
        __retres238 = (reg_errcode_t )9;
# 2539 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(37477, 9131, 0);

      }
      }
      {
# 2541 "regex.c"
      while (1) {
        while_continue___58: ;
        {
        __CrestLoad(37483, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(37482, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(37481, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2541 "regex.c"
        if ((unsigned long )p == (unsigned long )pend) {
          __CrestBranch(37484, 9136, 1);
          __CrestLoad(37486, (unsigned long )0, (long long )((reg_errcode_t )14));
          __CrestStore(37487, (unsigned long )(& __retres238));
# 2541 "regex.c"
          __retres238 = (reg_errcode_t )14;
# 2541 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(37485, 9138, 0);

        }
        }
# 2541 "regex.c"
        tmp___63 = p;
# 2541 "regex.c"
        p ++;
        __CrestLoad(37488, (unsigned long )tmp___63, (long long )*tmp___63);
        __CrestStore(37489, (unsigned long )(& c));
# 2541 "regex.c"
        c = (unsigned char )*tmp___63;
        {
        __CrestLoad(37492, (unsigned long )(& translate), (long long )((unsigned long )translate));
        __CrestLoad(37491, (unsigned long )0, (long long )0);
        __CrestApply2(37490, 13, (long long )(translate != 0));
# 2541 "regex.c"
        if (translate != 0) {
          __CrestBranch(37493, 9141, 1);
# 2541 "regex.c"
          mem_230 = translate + c;
          __CrestLoad(37495, (unsigned long )mem_230, (long long )*mem_230);
          __CrestStore(37496, (unsigned long )(& c));
# 2541 "regex.c"
          c = (unsigned char )*mem_230;
        } else {
          __CrestBranch(37494, 9142, 0);

        }
        }
# 2541 "regex.c"
        goto while_break___58;
      }
      while_break___58: ;
      }
    } else {
      __CrestBranch(37472, 9145, 0);

    }
    }
    {
    __CrestLoad(37499, (unsigned long )(& c), (long long )c);
    __CrestLoad(37498, (unsigned long )0, (long long )125);
    __CrestApply2(37497, 13, (long long )((int )c != 125));
# 2544 "regex.c"
    if ((int )c != 125) {
      __CrestBranch(37500, 9147, 1);
      {
      __CrestLoad(37506, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(37505, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(37504, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(37503, (unsigned long )0, (long long )0);
      __CrestApply2(37502, 13, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2546 "regex.c"
      if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(37507, 9148, 1);
# 2547 "regex.c"
        goto unfetch_interval;
      } else {
        __CrestBranch(37508, 9149, 0);
# 2549 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(37509);
        __CrestLoad(37510, (unsigned long )0, (long long )((reg_errcode_t )10));
        __CrestStore(37511, (unsigned long )(& __retres238));
# 2549 "regex.c"
        __retres238 = (reg_errcode_t )10;
# 2549 "regex.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(37501, 9152, 0);

    }
    }
    {
    __CrestLoad(37514, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
    __CrestLoad(37513, (unsigned long )0, (long long )0);
    __CrestApply2(37512, 12, (long long )(laststart == 0));
# 2555 "regex.c"
    if (laststart == 0) {
      __CrestBranch(37515, 9154, 1);
      {
      __CrestLoad(37521, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(37520, (unsigned long )0, (long long )(((((1UL << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(37519, 5, (long long )(syntax & (((((1UL << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(37518, (unsigned long )0, (long long )0);
      __CrestApply2(37517, 13, (long long )((syntax & (((((1UL << 1) << 1) << 1) << 1) << 1)) != 0));
# 2557 "regex.c"
      if ((syntax & (((((1UL << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(37522, 9155, 1);
# 2558 "regex.c"
        free((void *)compile_stack.stack);
        __CrestClearStack(37524);
        __CrestLoad(37525, (unsigned long )0, (long long )((reg_errcode_t )13));
        __CrestStore(37526, (unsigned long )(& __retres238));
# 2558 "regex.c"
        __retres238 = (reg_errcode_t )13;
# 2558 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(37523, 9158, 0);
        {
        __CrestLoad(37531, (unsigned long )(& syntax), (long long )syntax);
        __CrestLoad(37530, (unsigned long )0, (long long )((((1UL << 1) << 1) << 1) << 1));
        __CrestApply2(37529, 5, (long long )(syntax & ((((1UL << 1) << 1) << 1) << 1)));
        __CrestLoad(37528, (unsigned long )0, (long long )0);
        __CrestApply2(37527, 13, (long long )((syntax & ((((1UL << 1) << 1) << 1) << 1)) != 0));
# 2559 "regex.c"
        if ((syntax & ((((1UL << 1) << 1) << 1) << 1)) != 0) {
          __CrestBranch(37532, 9159, 1);
# 2560 "regex.c"
          laststart = b;
        } else {
          __CrestBranch(37533, 9160, 0);
# 2562 "regex.c"
          goto unfetch_interval;
        }
        }
      }
      }
    } else {
      __CrestBranch(37516, 9161, 0);

    }
    }
    {
    __CrestLoad(37536, (unsigned long )(& upper_bound), (long long )upper_bound);
    __CrestLoad(37535, (unsigned long )0, (long long )0);
    __CrestApply2(37534, 12, (long long )(upper_bound == 0));
# 2568 "regex.c"
    if (upper_bound == 0) {
      __CrestBranch(37537, 9163, 1);
      {
# 2570 "regex.c"
      while (1) {
        while_continue___59: ;
        {
        __CrestLoad(37545, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37544, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37543, 18, (long long )(b - bufp->buffer));
        __CrestLoad(37542, (unsigned long )0, (long long )3L);
        __CrestApply2(37541, 0, (long long )((b - bufp->buffer) + 3L));
        __CrestLoad(37540, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(37539, 14, (long long )((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated));
# 2570 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 3L) > bufp->allocated) {
          __CrestBranch(37546, 9167, 1);

        } else {
          __CrestBranch(37547, 9168, 0);
# 2570 "regex.c"
          goto while_break___59;
        }
        }
        {
# 2570 "regex.c"
        while (1) {
          while_continue___60: ;
# 2570 "regex.c"
          old_buffer___13 = bufp->buffer;
          {
          __CrestLoad(37550, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37549, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37548, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2570 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(37551, 9174, 1);
            __CrestLoad(37553, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(37554, (unsigned long )(& __retres238));
# 2570 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2570 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37552, 9176, 0);

          }
          }
          __CrestLoad(37557, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37556, (unsigned long )0, (long long )1);
          __CrestApply2(37555, 8, (long long )(bufp->allocated << 1));
          __CrestStore(37558, (unsigned long )(& bufp->allocated));
# 2570 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(37561, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37560, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37559, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2570 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(37562, 9179, 1);
            __CrestLoad(37564, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(37565, (unsigned long )(& bufp->allocated));
# 2570 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(37563, 9180, 0);

          }
          }
          __CrestLoad(37566, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2570 "regex.c"
          tmp___64 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(37567);
# 2570 "regex.c"
          bufp->buffer = (unsigned char *)tmp___64;
          {
          __CrestLoad(37570, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(37569, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(37568, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2570 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(37571, 9183, 1);
            __CrestLoad(37573, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(37574, (unsigned long )(& __retres238));
# 2570 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2570 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37572, 9185, 0);

          }
          }
          {
          __CrestLoad(37577, (unsigned long )(& old_buffer___13), (long long )((unsigned long )old_buffer___13));
          __CrestLoad(37576, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37575, 13, (long long )((unsigned long )old_buffer___13 != (unsigned long )bufp->buffer));
# 2570 "regex.c"
          if ((unsigned long )old_buffer___13 != (unsigned long )bufp->buffer) {
            __CrestBranch(37578, 9187, 1);
# 2570 "regex.c"
            b = bufp->buffer + (b - old_buffer___13);
# 2570 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___13);
            {
            __CrestLoad(37582, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(37581, (unsigned long )0, (long long )0);
            __CrestApply2(37580, 13, (long long )(fixup_alt_jump != 0));
# 2570 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(37583, 9189, 1);
# 2570 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___13);
            } else {
              __CrestBranch(37584, 9190, 0);

            }
            }
            {
            __CrestLoad(37587, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(37586, (unsigned long )0, (long long )0);
            __CrestApply2(37585, 13, (long long )(laststart != 0));
# 2570 "regex.c"
            if (laststart != 0) {
              __CrestBranch(37588, 9192, 1);
# 2570 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___13);
            } else {
              __CrestBranch(37589, 9193, 0);

            }
            }
            {
            __CrestLoad(37592, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(37591, (unsigned long )0, (long long )0);
            __CrestApply2(37590, 13, (long long )(pending_exact != 0));
# 2570 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(37593, 9195, 1);
# 2570 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___13);
            } else {
              __CrestBranch(37594, 9196, 0);

            }
            }
          } else {
            __CrestBranch(37579, 9197, 0);

          }
          }
# 2570 "regex.c"
          goto while_break___60;
        }
        while_break___60: ;
        }
      }
      while_break___59: ;
      }
      __CrestLoad(37595, (unsigned long )0, (long long )((re_opcode_t )13));
      __CrestLoad(37602, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(37601, (unsigned long )0, (long long )3);
      __CrestApply2(37600, 18, (long long )((unsigned long )(b + 3)));
      __CrestLoad(37599, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
      __CrestApply2(37598, 18, (long long )((b + 3) - laststart));
      __CrestLoad(37597, (unsigned long )0, (long long )3L);
      __CrestApply2(37596, 1, (long long )(((b + 3) - laststart) - 3L));
# 2571 "regex.c"
      insert_op1((re_opcode_t )13, laststart, (int )(((b + 3) - laststart) - 3L),
                 b);
      __CrestClearStack(37603);
# 2572 "regex.c"
      b += 3;
    } else {
      __CrestBranch(37538, 9202, 0);
      __CrestLoad(37610, (unsigned long )0, (long long )10);
      __CrestLoad(37609, (unsigned long )(& upper_bound), (long long )upper_bound);
      __CrestLoad(37608, (unsigned long )0, (long long )1);
      __CrestApply2(37607, 14, (long long )(upper_bound > 1));
      __CrestLoad(37606, (unsigned long )0, (long long )10);
      __CrestApply2(37605, 2, (long long )((upper_bound > 1) * 10));
      __CrestApply2(37604, 0, (long long )(10 + (upper_bound > 1) * 10));
      __CrestStore(37611, (unsigned long )(& nbytes));
# 2587 "regex.c"
      nbytes = (unsigned int )(10 + (upper_bound > 1) * 10);
      {
# 2589 "regex.c"
      while (1) {
        while_continue___61: ;
        {
        __CrestLoad(37618, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37617, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37616, 18, (long long )(b - bufp->buffer));
        __CrestLoad(37615, (unsigned long )(& nbytes), (long long )nbytes);
        __CrestApply2(37614, 0, (long long )((b - bufp->buffer) + (long )nbytes));
        __CrestLoad(37613, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(37612, 14, (long long )((unsigned long )((b - bufp->buffer) + (long )nbytes) > bufp->allocated));
# 2589 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + (long )nbytes) > bufp->allocated) {
          __CrestBranch(37619, 9207, 1);

        } else {
          __CrestBranch(37620, 9208, 0);
# 2589 "regex.c"
          goto while_break___61;
        }
        }
        {
# 2589 "regex.c"
        while (1) {
          while_continue___62: ;
# 2589 "regex.c"
          old_buffer___14 = bufp->buffer;
          {
          __CrestLoad(37623, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37622, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37621, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2589 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(37624, 9214, 1);
            __CrestLoad(37626, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(37627, (unsigned long )(& __retres238));
# 2589 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2589 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37625, 9216, 0);

          }
          }
          __CrestLoad(37630, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37629, (unsigned long )0, (long long )1);
          __CrestApply2(37628, 8, (long long )(bufp->allocated << 1));
          __CrestStore(37631, (unsigned long )(& bufp->allocated));
# 2589 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(37634, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37633, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37632, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2589 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(37635, 9219, 1);
            __CrestLoad(37637, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(37638, (unsigned long )(& bufp->allocated));
# 2589 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(37636, 9220, 0);

          }
          }
          __CrestLoad(37639, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2589 "regex.c"
          tmp___65 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(37640);
# 2589 "regex.c"
          bufp->buffer = (unsigned char *)tmp___65;
          {
          __CrestLoad(37643, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(37642, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(37641, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2589 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(37644, 9223, 1);
            __CrestLoad(37646, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(37647, (unsigned long )(& __retres238));
# 2589 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2589 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37645, 9225, 0);

          }
          }
          {
          __CrestLoad(37650, (unsigned long )(& old_buffer___14), (long long )((unsigned long )old_buffer___14));
          __CrestLoad(37649, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37648, 13, (long long )((unsigned long )old_buffer___14 != (unsigned long )bufp->buffer));
# 2589 "regex.c"
          if ((unsigned long )old_buffer___14 != (unsigned long )bufp->buffer) {
            __CrestBranch(37651, 9227, 1);
# 2589 "regex.c"
            b = bufp->buffer + (b - old_buffer___14);
# 2589 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___14);
            {
            __CrestLoad(37655, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(37654, (unsigned long )0, (long long )0);
            __CrestApply2(37653, 13, (long long )(fixup_alt_jump != 0));
# 2589 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(37656, 9229, 1);
# 2589 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___14);
            } else {
              __CrestBranch(37657, 9230, 0);

            }
            }
            {
            __CrestLoad(37660, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(37659, (unsigned long )0, (long long )0);
            __CrestApply2(37658, 13, (long long )(laststart != 0));
# 2589 "regex.c"
            if (laststart != 0) {
              __CrestBranch(37661, 9232, 1);
# 2589 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___14);
            } else {
              __CrestBranch(37662, 9233, 0);

            }
            }
            {
            __CrestLoad(37665, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(37664, (unsigned long )0, (long long )0);
            __CrestApply2(37663, 13, (long long )(pending_exact != 0));
# 2589 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(37666, 9235, 1);
# 2589 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___14);
            } else {
              __CrestBranch(37667, 9236, 0);

            }
            }
          } else {
            __CrestBranch(37652, 9237, 0);

          }
          }
# 2589 "regex.c"
          goto while_break___62;
        }
        while_break___62: ;
        }
      }
      while_break___61: ;
      }
      __CrestLoad(37668, (unsigned long )0, (long long )((re_opcode_t )21));
      __CrestLoad(37681, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(37680, (unsigned long )0, (long long )5);
      __CrestApply2(37679, 18, (long long )((unsigned long )(b + 5)));
      __CrestLoad(37678, (unsigned long )(& upper_bound), (long long )upper_bound);
      __CrestLoad(37677, (unsigned long )0, (long long )1);
      __CrestApply2(37676, 14, (long long )(upper_bound > 1));
      __CrestLoad(37675, (unsigned long )0, (long long )5);
      __CrestApply2(37674, 2, (long long )((upper_bound > 1) * 5));
      __CrestApply2(37673, 18, (long long )((unsigned long )((b + 5) + (upper_bound > 1) * 5)));
      __CrestLoad(37672, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
      __CrestApply2(37671, 18, (long long )(((b + 5) + (upper_bound > 1) * 5) - laststart));
      __CrestLoad(37670, (unsigned long )0, (long long )3L);
      __CrestApply2(37669, 1, (long long )((((b + 5) + (upper_bound > 1) * 5) - laststart) - 3L));
      __CrestLoad(37682, (unsigned long )(& lower_bound), (long long )lower_bound);
# 2596 "regex.c"
      insert_op2((re_opcode_t )21, laststart, (int )((((b + 5) + (upper_bound > 1) * 5) - laststart) - 3L),
                 lower_bound, b);
      __CrestClearStack(37683);
# 2599 "regex.c"
      b += 5;
      __CrestLoad(37684, (unsigned long )0, (long long )((re_opcode_t )23));
      __CrestLoad(37685, (unsigned long )0, (long long )5);
      __CrestLoad(37686, (unsigned long )(& lower_bound), (long long )lower_bound);
# 2605 "regex.c"
      insert_op2((re_opcode_t )23, laststart, 5, lower_bound, b);
      __CrestClearStack(37687);
# 2606 "regex.c"
      b += 5;
      {
      __CrestLoad(37690, (unsigned long )(& upper_bound), (long long )upper_bound);
      __CrestLoad(37689, (unsigned long )0, (long long )1);
      __CrestApply2(37688, 14, (long long )(upper_bound > 1));
# 2608 "regex.c"
      if (upper_bound > 1) {
        __CrestBranch(37691, 9243, 1);
        __CrestLoad(37693, (unsigned long )0, (long long )((re_opcode_t )22));
        __CrestLoad(37700, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
        __CrestLoad(37699, (unsigned long )0, (long long )5);
        __CrestApply2(37698, 18, (long long )((unsigned long )(laststart + 5)));
        __CrestLoad(37697, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestApply2(37696, 18, (long long )((laststart + 5) - b));
        __CrestLoad(37695, (unsigned long )0, (long long )3L);
        __CrestApply2(37694, 1, (long long )(((laststart + 5) - b) - 3L));
        __CrestLoad(37703, (unsigned long )(& upper_bound), (long long )upper_bound);
        __CrestLoad(37702, (unsigned long )0, (long long )1);
        __CrestApply2(37701, 1, (long long )(upper_bound - 1));
# 2616 "regex.c"
        store_op2((re_opcode_t )22, b, (int )(((laststart + 5) - b) - 3L), upper_bound - 1);
        __CrestClearStack(37704);
# 2618 "regex.c"
        b += 5;
        __CrestLoad(37705, (unsigned long )0, (long long )((re_opcode_t )23));
        __CrestLoad(37708, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37707, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
        __CrestApply2(37706, 18, (long long )(b - laststart));
        __CrestLoad(37711, (unsigned long )(& upper_bound), (long long )upper_bound);
        __CrestLoad(37710, (unsigned long )0, (long long )1);
        __CrestApply2(37709, 1, (long long )(upper_bound - 1));
# 2634 "regex.c"
        insert_op2((re_opcode_t )23, laststart, (int )(b - laststart), upper_bound - 1,
                   b);
        __CrestClearStack(37712);
# 2636 "regex.c"
        b += 5;
      } else {
        __CrestBranch(37692, 9244, 0);

      }
      }
    }
    }
# 2639 "regex.c"
    pending_exact = (unsigned char *)0;
# 2640 "regex.c"
    beg_interval = (char const *)((void *)0);
# 2642 "regex.c"
    goto switch_break___0;
    unfetch_interval:
# 2647 "regex.c"
    p = beg_interval;
# 2648 "regex.c"
    beg_interval = (char const *)((void *)0);
    {
# 2651 "regex.c"
    while (1) {
      while_continue___63: ;
      {
      __CrestLoad(37715, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(37714, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(37713, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 2651 "regex.c"
      if ((unsigned long )p == (unsigned long )pend) {
        __CrestBranch(37716, 9252, 1);
        __CrestLoad(37718, (unsigned long )0, (long long )((reg_errcode_t )14));
        __CrestStore(37719, (unsigned long )(& __retres238));
# 2651 "regex.c"
        __retres238 = (reg_errcode_t )14;
# 2651 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(37717, 9254, 0);

      }
      }
# 2651 "regex.c"
      tmp___66 = p;
# 2651 "regex.c"
      p ++;
      __CrestLoad(37720, (unsigned long )tmp___66, (long long )*tmp___66);
      __CrestStore(37721, (unsigned long )(& c));
# 2651 "regex.c"
      c = (unsigned char )*tmp___66;
      {
      __CrestLoad(37724, (unsigned long )(& translate), (long long )((unsigned long )translate));
      __CrestLoad(37723, (unsigned long )0, (long long )0);
      __CrestApply2(37722, 13, (long long )(translate != 0));
# 2651 "regex.c"
      if (translate != 0) {
        __CrestBranch(37725, 9257, 1);
# 2651 "regex.c"
        mem_231 = translate + c;
        __CrestLoad(37727, (unsigned long )mem_231, (long long )*mem_231);
        __CrestStore(37728, (unsigned long )(& c));
# 2651 "regex.c"
        c = (unsigned char )*mem_231;
      } else {
        __CrestBranch(37726, 9258, 0);

      }
      }
# 2651 "regex.c"
      goto while_break___63;
    }
    while_break___63: ;
    }
    {
    __CrestLoad(37733, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(37732, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37731, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37730, (unsigned long )0, (long long )0);
    __CrestApply2(37729, 12, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 2653 "regex.c"
    if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(37734, 9262, 1);
      {
      __CrestLoad(37738, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(37737, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
      __CrestApply2(37736, 14, (long long )((unsigned long )p > (unsigned long )pattern));
# 2655 "regex.c"
      if ((unsigned long )p > (unsigned long )pattern) {
        __CrestBranch(37739, 9263, 1);
        {
# 2655 "regex.c"
        mem_232 = p + -1;
        {
        __CrestLoad(37743, (unsigned long )mem_232, (long long )*mem_232);
        __CrestLoad(37742, (unsigned long )0, (long long )92);
        __CrestApply2(37741, 12, (long long )((int const )*mem_232 == 92));
# 2655 "regex.c"
        if ((int const )*mem_232 == 92) {
          __CrestBranch(37744, 9266, 1);
# 2656 "regex.c"
          goto normal_backslash;
        } else {
          __CrestBranch(37745, 9267, 0);

        }
        }
        }
      } else {
        __CrestBranch(37740, 9268, 0);

      }
      }
    } else {
      __CrestBranch(37735, 9269, 0);

    }
    }
# 2658 "regex.c"
    goto normal_char;
    case_119:
    {
    __CrestLoad(37750, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(37749, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37748, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37747, (unsigned long )0, (long long )0);
    __CrestApply2(37746, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2682 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(37751, 9272, 1);
# 2683 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(37752, 9273, 0);

    }
    }
# 2684 "regex.c"
    laststart = b;
    {
# 2685 "regex.c"
    while (1) {
      while_continue___64: ;
      {
# 2685 "regex.c"
      while (1) {
        while_continue___65: ;
        {
        __CrestLoad(37759, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37758, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37757, 18, (long long )(b - bufp->buffer));
        __CrestLoad(37756, (unsigned long )0, (long long )1L);
        __CrestApply2(37755, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(37754, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(37753, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2685 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(37760, 9282, 1);

        } else {
          __CrestBranch(37761, 9283, 0);
# 2685 "regex.c"
          goto while_break___65;
        }
        }
        {
# 2685 "regex.c"
        while (1) {
          while_continue___66: ;
# 2685 "regex.c"
          old_buffer___15 = bufp->buffer;
          {
          __CrestLoad(37764, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37763, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37762, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2685 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(37765, 9289, 1);
            __CrestLoad(37767, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(37768, (unsigned long )(& __retres238));
# 2685 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2685 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37766, 9291, 0);

          }
          }
          __CrestLoad(37771, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37770, (unsigned long )0, (long long )1);
          __CrestApply2(37769, 8, (long long )(bufp->allocated << 1));
          __CrestStore(37772, (unsigned long )(& bufp->allocated));
# 2685 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(37775, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37774, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37773, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2685 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(37776, 9294, 1);
            __CrestLoad(37778, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(37779, (unsigned long )(& bufp->allocated));
# 2685 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(37777, 9295, 0);

          }
          }
          __CrestLoad(37780, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2685 "regex.c"
          tmp___67 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(37781);
# 2685 "regex.c"
          bufp->buffer = (unsigned char *)tmp___67;
          {
          __CrestLoad(37784, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(37783, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(37782, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2685 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(37785, 9298, 1);
            __CrestLoad(37787, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(37788, (unsigned long )(& __retres238));
# 2685 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2685 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37786, 9300, 0);

          }
          }
          {
          __CrestLoad(37791, (unsigned long )(& old_buffer___15), (long long )((unsigned long )old_buffer___15));
          __CrestLoad(37790, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37789, 13, (long long )((unsigned long )old_buffer___15 != (unsigned long )bufp->buffer));
# 2685 "regex.c"
          if ((unsigned long )old_buffer___15 != (unsigned long )bufp->buffer) {
            __CrestBranch(37792, 9302, 1);
# 2685 "regex.c"
            b = bufp->buffer + (b - old_buffer___15);
# 2685 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___15);
            {
            __CrestLoad(37796, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(37795, (unsigned long )0, (long long )0);
            __CrestApply2(37794, 13, (long long )(fixup_alt_jump != 0));
# 2685 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(37797, 9304, 1);
# 2685 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___15);
            } else {
              __CrestBranch(37798, 9305, 0);

            }
            }
            {
            __CrestLoad(37801, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(37800, (unsigned long )0, (long long )0);
            __CrestApply2(37799, 13, (long long )(laststart != 0));
# 2685 "regex.c"
            if (laststart != 0) {
              __CrestBranch(37802, 9307, 1);
# 2685 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___15);
            } else {
              __CrestBranch(37803, 9308, 0);

            }
            }
            {
            __CrestLoad(37806, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(37805, (unsigned long )0, (long long )0);
            __CrestApply2(37804, 13, (long long )(pending_exact != 0));
# 2685 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(37807, 9310, 1);
# 2685 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___15);
            } else {
              __CrestBranch(37808, 9311, 0);

            }
            }
          } else {
            __CrestBranch(37793, 9312, 0);

          }
          }
# 2685 "regex.c"
          goto while_break___66;
        }
        while_break___66: ;
        }
      }
      while_break___65: ;
      }
# 2685 "regex.c"
      tmp___68 = b;
# 2685 "regex.c"
      b ++;
      __CrestLoad(37809, (unsigned long )0, (long long )(unsigned char)24);
      __CrestStore(37810, (unsigned long )tmp___68);
# 2685 "regex.c"
      *tmp___68 = (unsigned char)24;
# 2685 "regex.c"
      goto while_break___64;
    }
    while_break___64: ;
    }
# 2686 "regex.c"
    goto switch_break___0;
    case_87:
    {
    __CrestLoad(37815, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(37814, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37813, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37812, (unsigned long )0, (long long )0);
    __CrestApply2(37811, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2690 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(37816, 9321, 1);
# 2691 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(37817, 9322, 0);

    }
    }
# 2692 "regex.c"
    laststart = b;
    {
# 2693 "regex.c"
    while (1) {
      while_continue___67: ;
      {
# 2693 "regex.c"
      while (1) {
        while_continue___68: ;
        {
        __CrestLoad(37824, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37823, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37822, 18, (long long )(b - bufp->buffer));
        __CrestLoad(37821, (unsigned long )0, (long long )1L);
        __CrestApply2(37820, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(37819, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(37818, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2693 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(37825, 9331, 1);

        } else {
          __CrestBranch(37826, 9332, 0);
# 2693 "regex.c"
          goto while_break___68;
        }
        }
        {
# 2693 "regex.c"
        while (1) {
          while_continue___69: ;
# 2693 "regex.c"
          old_buffer___16 = bufp->buffer;
          {
          __CrestLoad(37829, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37828, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37827, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2693 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(37830, 9338, 1);
            __CrestLoad(37832, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(37833, (unsigned long )(& __retres238));
# 2693 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2693 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37831, 9340, 0);

          }
          }
          __CrestLoad(37836, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37835, (unsigned long )0, (long long )1);
          __CrestApply2(37834, 8, (long long )(bufp->allocated << 1));
          __CrestStore(37837, (unsigned long )(& bufp->allocated));
# 2693 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(37840, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37839, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37838, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2693 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(37841, 9343, 1);
            __CrestLoad(37843, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(37844, (unsigned long )(& bufp->allocated));
# 2693 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(37842, 9344, 0);

          }
          }
          __CrestLoad(37845, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2693 "regex.c"
          tmp___69 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(37846);
# 2693 "regex.c"
          bufp->buffer = (unsigned char *)tmp___69;
          {
          __CrestLoad(37849, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(37848, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(37847, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2693 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(37850, 9347, 1);
            __CrestLoad(37852, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(37853, (unsigned long )(& __retres238));
# 2693 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2693 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37851, 9349, 0);

          }
          }
          {
          __CrestLoad(37856, (unsigned long )(& old_buffer___16), (long long )((unsigned long )old_buffer___16));
          __CrestLoad(37855, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37854, 13, (long long )((unsigned long )old_buffer___16 != (unsigned long )bufp->buffer));
# 2693 "regex.c"
          if ((unsigned long )old_buffer___16 != (unsigned long )bufp->buffer) {
            __CrestBranch(37857, 9351, 1);
# 2693 "regex.c"
            b = bufp->buffer + (b - old_buffer___16);
# 2693 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___16);
            {
            __CrestLoad(37861, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(37860, (unsigned long )0, (long long )0);
            __CrestApply2(37859, 13, (long long )(fixup_alt_jump != 0));
# 2693 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(37862, 9353, 1);
# 2693 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___16);
            } else {
              __CrestBranch(37863, 9354, 0);

            }
            }
            {
            __CrestLoad(37866, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(37865, (unsigned long )0, (long long )0);
            __CrestApply2(37864, 13, (long long )(laststart != 0));
# 2693 "regex.c"
            if (laststart != 0) {
              __CrestBranch(37867, 9356, 1);
# 2693 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___16);
            } else {
              __CrestBranch(37868, 9357, 0);

            }
            }
            {
            __CrestLoad(37871, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(37870, (unsigned long )0, (long long )0);
            __CrestApply2(37869, 13, (long long )(pending_exact != 0));
# 2693 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(37872, 9359, 1);
# 2693 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___16);
            } else {
              __CrestBranch(37873, 9360, 0);

            }
            }
          } else {
            __CrestBranch(37858, 9361, 0);

          }
          }
# 2693 "regex.c"
          goto while_break___69;
        }
        while_break___69: ;
        }
      }
      while_break___68: ;
      }
# 2693 "regex.c"
      tmp___70 = b;
# 2693 "regex.c"
      b ++;
      __CrestLoad(37874, (unsigned long )0, (long long )(unsigned char)25);
      __CrestStore(37875, (unsigned long )tmp___70);
# 2693 "regex.c"
      *tmp___70 = (unsigned char)25;
# 2693 "regex.c"
      goto while_break___67;
    }
    while_break___67: ;
    }
# 2694 "regex.c"
    goto switch_break___0;
    case_60:
    {
    __CrestLoad(37880, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(37879, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37878, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37877, (unsigned long )0, (long long )0);
    __CrestApply2(37876, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2698 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(37881, 9370, 1);
# 2699 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(37882, 9371, 0);

    }
    }
    {
# 2700 "regex.c"
    while (1) {
      while_continue___70: ;
      {
# 2700 "regex.c"
      while (1) {
        while_continue___71: ;
        {
        __CrestLoad(37889, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37888, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37887, 18, (long long )(b - bufp->buffer));
        __CrestLoad(37886, (unsigned long )0, (long long )1L);
        __CrestApply2(37885, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(37884, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(37883, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2700 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(37890, 9379, 1);

        } else {
          __CrestBranch(37891, 9380, 0);
# 2700 "regex.c"
          goto while_break___71;
        }
        }
        {
# 2700 "regex.c"
        while (1) {
          while_continue___72: ;
# 2700 "regex.c"
          old_buffer___17 = bufp->buffer;
          {
          __CrestLoad(37894, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37893, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37892, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2700 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(37895, 9386, 1);
            __CrestLoad(37897, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(37898, (unsigned long )(& __retres238));
# 2700 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2700 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37896, 9388, 0);

          }
          }
          __CrestLoad(37901, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37900, (unsigned long )0, (long long )1);
          __CrestApply2(37899, 8, (long long )(bufp->allocated << 1));
          __CrestStore(37902, (unsigned long )(& bufp->allocated));
# 2700 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(37905, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37904, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37903, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2700 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(37906, 9391, 1);
            __CrestLoad(37908, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(37909, (unsigned long )(& bufp->allocated));
# 2700 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(37907, 9392, 0);

          }
          }
          __CrestLoad(37910, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2700 "regex.c"
          tmp___71 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(37911);
# 2700 "regex.c"
          bufp->buffer = (unsigned char *)tmp___71;
          {
          __CrestLoad(37914, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(37913, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(37912, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2700 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(37915, 9395, 1);
            __CrestLoad(37917, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(37918, (unsigned long )(& __retres238));
# 2700 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2700 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37916, 9397, 0);

          }
          }
          {
          __CrestLoad(37921, (unsigned long )(& old_buffer___17), (long long )((unsigned long )old_buffer___17));
          __CrestLoad(37920, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37919, 13, (long long )((unsigned long )old_buffer___17 != (unsigned long )bufp->buffer));
# 2700 "regex.c"
          if ((unsigned long )old_buffer___17 != (unsigned long )bufp->buffer) {
            __CrestBranch(37922, 9399, 1);
# 2700 "regex.c"
            b = bufp->buffer + (b - old_buffer___17);
# 2700 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___17);
            {
            __CrestLoad(37926, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(37925, (unsigned long )0, (long long )0);
            __CrestApply2(37924, 13, (long long )(fixup_alt_jump != 0));
# 2700 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(37927, 9401, 1);
# 2700 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___17);
            } else {
              __CrestBranch(37928, 9402, 0);

            }
            }
            {
            __CrestLoad(37931, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(37930, (unsigned long )0, (long long )0);
            __CrestApply2(37929, 13, (long long )(laststart != 0));
# 2700 "regex.c"
            if (laststart != 0) {
              __CrestBranch(37932, 9404, 1);
# 2700 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___17);
            } else {
              __CrestBranch(37933, 9405, 0);

            }
            }
            {
            __CrestLoad(37936, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(37935, (unsigned long )0, (long long )0);
            __CrestApply2(37934, 13, (long long )(pending_exact != 0));
# 2700 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(37937, 9407, 1);
# 2700 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___17);
            } else {
              __CrestBranch(37938, 9408, 0);

            }
            }
          } else {
            __CrestBranch(37923, 9409, 0);

          }
          }
# 2700 "regex.c"
          goto while_break___72;
        }
        while_break___72: ;
        }
      }
      while_break___71: ;
      }
# 2700 "regex.c"
      tmp___72 = b;
# 2700 "regex.c"
      b ++;
      __CrestLoad(37939, (unsigned long )0, (long long )(unsigned char)26);
      __CrestStore(37940, (unsigned long )tmp___72);
# 2700 "regex.c"
      *tmp___72 = (unsigned char)26;
# 2700 "regex.c"
      goto while_break___70;
    }
    while_break___70: ;
    }
# 2701 "regex.c"
    goto switch_break___0;
    case_62:
    {
    __CrestLoad(37945, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(37944, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(37943, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(37942, (unsigned long )0, (long long )0);
    __CrestApply2(37941, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2704 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(37946, 9418, 1);
# 2705 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(37947, 9419, 0);

    }
    }
    {
# 2706 "regex.c"
    while (1) {
      while_continue___73: ;
      {
# 2706 "regex.c"
      while (1) {
        while_continue___74: ;
        {
        __CrestLoad(37954, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(37953, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(37952, 18, (long long )(b - bufp->buffer));
        __CrestLoad(37951, (unsigned long )0, (long long )1L);
        __CrestApply2(37950, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(37949, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(37948, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2706 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(37955, 9427, 1);

        } else {
          __CrestBranch(37956, 9428, 0);
# 2706 "regex.c"
          goto while_break___74;
        }
        }
        {
# 2706 "regex.c"
        while (1) {
          while_continue___75: ;
# 2706 "regex.c"
          old_buffer___18 = bufp->buffer;
          {
          __CrestLoad(37959, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37958, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37957, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2706 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(37960, 9434, 1);
            __CrestLoad(37962, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(37963, (unsigned long )(& __retres238));
# 2706 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2706 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37961, 9436, 0);

          }
          }
          __CrestLoad(37966, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37965, (unsigned long )0, (long long )1);
          __CrestApply2(37964, 8, (long long )(bufp->allocated << 1));
          __CrestStore(37967, (unsigned long )(& bufp->allocated));
# 2706 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(37970, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(37969, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(37968, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2706 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(37971, 9439, 1);
            __CrestLoad(37973, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(37974, (unsigned long )(& bufp->allocated));
# 2706 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(37972, 9440, 0);

          }
          }
          __CrestLoad(37975, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2706 "regex.c"
          tmp___73 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(37976);
# 2706 "regex.c"
          bufp->buffer = (unsigned char *)tmp___73;
          {
          __CrestLoad(37979, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(37978, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(37977, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2706 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(37980, 9443, 1);
            __CrestLoad(37982, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(37983, (unsigned long )(& __retres238));
# 2706 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2706 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(37981, 9445, 0);

          }
          }
          {
          __CrestLoad(37986, (unsigned long )(& old_buffer___18), (long long )((unsigned long )old_buffer___18));
          __CrestLoad(37985, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(37984, 13, (long long )((unsigned long )old_buffer___18 != (unsigned long )bufp->buffer));
# 2706 "regex.c"
          if ((unsigned long )old_buffer___18 != (unsigned long )bufp->buffer) {
            __CrestBranch(37987, 9447, 1);
# 2706 "regex.c"
            b = bufp->buffer + (b - old_buffer___18);
# 2706 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___18);
            {
            __CrestLoad(37991, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(37990, (unsigned long )0, (long long )0);
            __CrestApply2(37989, 13, (long long )(fixup_alt_jump != 0));
# 2706 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(37992, 9449, 1);
# 2706 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___18);
            } else {
              __CrestBranch(37993, 9450, 0);

            }
            }
            {
            __CrestLoad(37996, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(37995, (unsigned long )0, (long long )0);
            __CrestApply2(37994, 13, (long long )(laststart != 0));
# 2706 "regex.c"
            if (laststart != 0) {
              __CrestBranch(37997, 9452, 1);
# 2706 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___18);
            } else {
              __CrestBranch(37998, 9453, 0);

            }
            }
            {
            __CrestLoad(38001, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38000, (unsigned long )0, (long long )0);
            __CrestApply2(37999, 13, (long long )(pending_exact != 0));
# 2706 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38002, 9455, 1);
# 2706 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___18);
            } else {
              __CrestBranch(38003, 9456, 0);

            }
            }
          } else {
            __CrestBranch(37988, 9457, 0);

          }
          }
# 2706 "regex.c"
          goto while_break___75;
        }
        while_break___75: ;
        }
      }
      while_break___74: ;
      }
# 2706 "regex.c"
      tmp___74 = b;
# 2706 "regex.c"
      b ++;
      __CrestLoad(38004, (unsigned long )0, (long long )(unsigned char)27);
      __CrestStore(38005, (unsigned long )tmp___74);
# 2706 "regex.c"
      *tmp___74 = (unsigned char)27;
# 2706 "regex.c"
      goto while_break___73;
    }
    while_break___73: ;
    }
# 2707 "regex.c"
    goto switch_break___0;
    case_98:
    {
    __CrestLoad(38010, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(38009, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38008, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38007, (unsigned long )0, (long long )0);
    __CrestApply2(38006, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2710 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38011, 9466, 1);
# 2711 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(38012, 9467, 0);

    }
    }
    {
# 2712 "regex.c"
    while (1) {
      while_continue___76: ;
      {
# 2712 "regex.c"
      while (1) {
        while_continue___77: ;
        {
        __CrestLoad(38019, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38018, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38017, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38016, (unsigned long )0, (long long )1L);
        __CrestApply2(38015, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(38014, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38013, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2712 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(38020, 9475, 1);

        } else {
          __CrestBranch(38021, 9476, 0);
# 2712 "regex.c"
          goto while_break___77;
        }
        }
        {
# 2712 "regex.c"
        while (1) {
          while_continue___78: ;
# 2712 "regex.c"
          old_buffer___19 = bufp->buffer;
          {
          __CrestLoad(38024, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38023, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38022, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2712 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38025, 9482, 1);
            __CrestLoad(38027, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38028, (unsigned long )(& __retres238));
# 2712 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2712 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38026, 9484, 0);

          }
          }
          __CrestLoad(38031, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38030, (unsigned long )0, (long long )1);
          __CrestApply2(38029, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38032, (unsigned long )(& bufp->allocated));
# 2712 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38035, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38034, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38033, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2712 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38036, 9487, 1);
            __CrestLoad(38038, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38039, (unsigned long )(& bufp->allocated));
# 2712 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38037, 9488, 0);

          }
          }
          __CrestLoad(38040, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2712 "regex.c"
          tmp___75 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38041);
# 2712 "regex.c"
          bufp->buffer = (unsigned char *)tmp___75;
          {
          __CrestLoad(38044, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38043, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38042, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2712 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38045, 9491, 1);
            __CrestLoad(38047, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38048, (unsigned long )(& __retres238));
# 2712 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2712 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38046, 9493, 0);

          }
          }
          {
          __CrestLoad(38051, (unsigned long )(& old_buffer___19), (long long )((unsigned long )old_buffer___19));
          __CrestLoad(38050, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38049, 13, (long long )((unsigned long )old_buffer___19 != (unsigned long )bufp->buffer));
# 2712 "regex.c"
          if ((unsigned long )old_buffer___19 != (unsigned long )bufp->buffer) {
            __CrestBranch(38052, 9495, 1);
# 2712 "regex.c"
            b = bufp->buffer + (b - old_buffer___19);
# 2712 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___19);
            {
            __CrestLoad(38056, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38055, (unsigned long )0, (long long )0);
            __CrestApply2(38054, 13, (long long )(fixup_alt_jump != 0));
# 2712 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38057, 9497, 1);
# 2712 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___19);
            } else {
              __CrestBranch(38058, 9498, 0);

            }
            }
            {
            __CrestLoad(38061, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38060, (unsigned long )0, (long long )0);
            __CrestApply2(38059, 13, (long long )(laststart != 0));
# 2712 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38062, 9500, 1);
# 2712 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___19);
            } else {
              __CrestBranch(38063, 9501, 0);

            }
            }
            {
            __CrestLoad(38066, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38065, (unsigned long )0, (long long )0);
            __CrestApply2(38064, 13, (long long )(pending_exact != 0));
# 2712 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38067, 9503, 1);
# 2712 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___19);
            } else {
              __CrestBranch(38068, 9504, 0);

            }
            }
          } else {
            __CrestBranch(38053, 9505, 0);

          }
          }
# 2712 "regex.c"
          goto while_break___78;
        }
        while_break___78: ;
        }
      }
      while_break___77: ;
      }
# 2712 "regex.c"
      tmp___76 = b;
# 2712 "regex.c"
      b ++;
      __CrestLoad(38069, (unsigned long )0, (long long )(unsigned char)28);
      __CrestStore(38070, (unsigned long )tmp___76);
# 2712 "regex.c"
      *tmp___76 = (unsigned char)28;
# 2712 "regex.c"
      goto while_break___76;
    }
    while_break___76: ;
    }
# 2713 "regex.c"
    goto switch_break___0;
    case_66:
    {
    __CrestLoad(38075, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(38074, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38073, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38072, (unsigned long )0, (long long )0);
    __CrestApply2(38071, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2716 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38076, 9514, 1);
# 2717 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(38077, 9515, 0);

    }
    }
    {
# 2718 "regex.c"
    while (1) {
      while_continue___79: ;
      {
# 2718 "regex.c"
      while (1) {
        while_continue___80: ;
        {
        __CrestLoad(38084, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38083, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38082, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38081, (unsigned long )0, (long long )1L);
        __CrestApply2(38080, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(38079, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38078, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2718 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(38085, 9523, 1);

        } else {
          __CrestBranch(38086, 9524, 0);
# 2718 "regex.c"
          goto while_break___80;
        }
        }
        {
# 2718 "regex.c"
        while (1) {
          while_continue___81: ;
# 2718 "regex.c"
          old_buffer___20 = bufp->buffer;
          {
          __CrestLoad(38089, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38088, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38087, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2718 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38090, 9530, 1);
            __CrestLoad(38092, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38093, (unsigned long )(& __retres238));
# 2718 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2718 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38091, 9532, 0);

          }
          }
          __CrestLoad(38096, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38095, (unsigned long )0, (long long )1);
          __CrestApply2(38094, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38097, (unsigned long )(& bufp->allocated));
# 2718 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38100, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38099, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38098, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2718 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38101, 9535, 1);
            __CrestLoad(38103, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38104, (unsigned long )(& bufp->allocated));
# 2718 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38102, 9536, 0);

          }
          }
          __CrestLoad(38105, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2718 "regex.c"
          tmp___77 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38106);
# 2718 "regex.c"
          bufp->buffer = (unsigned char *)tmp___77;
          {
          __CrestLoad(38109, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38108, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38107, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2718 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38110, 9539, 1);
            __CrestLoad(38112, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38113, (unsigned long )(& __retres238));
# 2718 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2718 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38111, 9541, 0);

          }
          }
          {
          __CrestLoad(38116, (unsigned long )(& old_buffer___20), (long long )((unsigned long )old_buffer___20));
          __CrestLoad(38115, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38114, 13, (long long )((unsigned long )old_buffer___20 != (unsigned long )bufp->buffer));
# 2718 "regex.c"
          if ((unsigned long )old_buffer___20 != (unsigned long )bufp->buffer) {
            __CrestBranch(38117, 9543, 1);
# 2718 "regex.c"
            b = bufp->buffer + (b - old_buffer___20);
# 2718 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___20);
            {
            __CrestLoad(38121, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38120, (unsigned long )0, (long long )0);
            __CrestApply2(38119, 13, (long long )(fixup_alt_jump != 0));
# 2718 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38122, 9545, 1);
# 2718 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___20);
            } else {
              __CrestBranch(38123, 9546, 0);

            }
            }
            {
            __CrestLoad(38126, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38125, (unsigned long )0, (long long )0);
            __CrestApply2(38124, 13, (long long )(laststart != 0));
# 2718 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38127, 9548, 1);
# 2718 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___20);
            } else {
              __CrestBranch(38128, 9549, 0);

            }
            }
            {
            __CrestLoad(38131, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38130, (unsigned long )0, (long long )0);
            __CrestApply2(38129, 13, (long long )(pending_exact != 0));
# 2718 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38132, 9551, 1);
# 2718 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___20);
            } else {
              __CrestBranch(38133, 9552, 0);

            }
            }
          } else {
            __CrestBranch(38118, 9553, 0);

          }
          }
# 2718 "regex.c"
          goto while_break___81;
        }
        while_break___81: ;
        }
      }
      while_break___80: ;
      }
# 2718 "regex.c"
      tmp___78 = b;
# 2718 "regex.c"
      b ++;
      __CrestLoad(38134, (unsigned long )0, (long long )(unsigned char)29);
      __CrestStore(38135, (unsigned long )tmp___78);
# 2718 "regex.c"
      *tmp___78 = (unsigned char)29;
# 2718 "regex.c"
      goto while_break___79;
    }
    while_break___79: ;
    }
# 2719 "regex.c"
    goto switch_break___0;
    case_96:
    {
    __CrestLoad(38140, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(38139, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38138, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38137, (unsigned long )0, (long long )0);
    __CrestApply2(38136, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2722 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38141, 9562, 1);
# 2723 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(38142, 9563, 0);

    }
    }
    {
# 2724 "regex.c"
    while (1) {
      while_continue___82: ;
      {
# 2724 "regex.c"
      while (1) {
        while_continue___83: ;
        {
        __CrestLoad(38149, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38148, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38147, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38146, (unsigned long )0, (long long )1L);
        __CrestApply2(38145, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(38144, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38143, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2724 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(38150, 9571, 1);

        } else {
          __CrestBranch(38151, 9572, 0);
# 2724 "regex.c"
          goto while_break___83;
        }
        }
        {
# 2724 "regex.c"
        while (1) {
          while_continue___84: ;
# 2724 "regex.c"
          old_buffer___21 = bufp->buffer;
          {
          __CrestLoad(38154, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38153, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38152, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2724 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38155, 9578, 1);
            __CrestLoad(38157, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38158, (unsigned long )(& __retres238));
# 2724 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2724 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38156, 9580, 0);

          }
          }
          __CrestLoad(38161, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38160, (unsigned long )0, (long long )1);
          __CrestApply2(38159, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38162, (unsigned long )(& bufp->allocated));
# 2724 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38165, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38164, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38163, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2724 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38166, 9583, 1);
            __CrestLoad(38168, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38169, (unsigned long )(& bufp->allocated));
# 2724 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38167, 9584, 0);

          }
          }
          __CrestLoad(38170, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2724 "regex.c"
          tmp___79 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38171);
# 2724 "regex.c"
          bufp->buffer = (unsigned char *)tmp___79;
          {
          __CrestLoad(38174, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38173, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38172, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2724 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38175, 9587, 1);
            __CrestLoad(38177, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38178, (unsigned long )(& __retres238));
# 2724 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2724 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38176, 9589, 0);

          }
          }
          {
          __CrestLoad(38181, (unsigned long )(& old_buffer___21), (long long )((unsigned long )old_buffer___21));
          __CrestLoad(38180, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38179, 13, (long long )((unsigned long )old_buffer___21 != (unsigned long )bufp->buffer));
# 2724 "regex.c"
          if ((unsigned long )old_buffer___21 != (unsigned long )bufp->buffer) {
            __CrestBranch(38182, 9591, 1);
# 2724 "regex.c"
            b = bufp->buffer + (b - old_buffer___21);
# 2724 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___21);
            {
            __CrestLoad(38186, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38185, (unsigned long )0, (long long )0);
            __CrestApply2(38184, 13, (long long )(fixup_alt_jump != 0));
# 2724 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38187, 9593, 1);
# 2724 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___21);
            } else {
              __CrestBranch(38188, 9594, 0);

            }
            }
            {
            __CrestLoad(38191, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38190, (unsigned long )0, (long long )0);
            __CrestApply2(38189, 13, (long long )(laststart != 0));
# 2724 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38192, 9596, 1);
# 2724 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___21);
            } else {
              __CrestBranch(38193, 9597, 0);

            }
            }
            {
            __CrestLoad(38196, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38195, (unsigned long )0, (long long )0);
            __CrestApply2(38194, 13, (long long )(pending_exact != 0));
# 2724 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38197, 9599, 1);
# 2724 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___21);
            } else {
              __CrestBranch(38198, 9600, 0);

            }
            }
          } else {
            __CrestBranch(38183, 9601, 0);

          }
          }
# 2724 "regex.c"
          goto while_break___84;
        }
        while_break___84: ;
        }
      }
      while_break___83: ;
      }
# 2724 "regex.c"
      tmp___80 = b;
# 2724 "regex.c"
      b ++;
      __CrestLoad(38199, (unsigned long )0, (long long )(unsigned char)11);
      __CrestStore(38200, (unsigned long )tmp___80);
# 2724 "regex.c"
      *tmp___80 = (unsigned char)11;
# 2724 "regex.c"
      goto while_break___82;
    }
    while_break___82: ;
    }
# 2725 "regex.c"
    goto switch_break___0;
    case_39:
    {
    __CrestLoad(38205, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
    __CrestLoad(38204, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38203, 5, (long long )(re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38202, (unsigned long )0, (long long )0);
    __CrestApply2(38201, 13, (long long )((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2728 "regex.c"
    if ((re_syntax_options & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38206, 9610, 1);
# 2729 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(38207, 9611, 0);

    }
    }
    {
# 2730 "regex.c"
    while (1) {
      while_continue___85: ;
      {
# 2730 "regex.c"
      while (1) {
        while_continue___86: ;
        {
        __CrestLoad(38214, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38213, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38212, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38211, (unsigned long )0, (long long )1L);
        __CrestApply2(38210, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(38209, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38208, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2730 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(38215, 9619, 1);

        } else {
          __CrestBranch(38216, 9620, 0);
# 2730 "regex.c"
          goto while_break___86;
        }
        }
        {
# 2730 "regex.c"
        while (1) {
          while_continue___87: ;
# 2730 "regex.c"
          old_buffer___22 = bufp->buffer;
          {
          __CrestLoad(38219, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38218, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38217, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2730 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38220, 9626, 1);
            __CrestLoad(38222, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38223, (unsigned long )(& __retres238));
# 2730 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2730 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38221, 9628, 0);

          }
          }
          __CrestLoad(38226, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38225, (unsigned long )0, (long long )1);
          __CrestApply2(38224, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38227, (unsigned long )(& bufp->allocated));
# 2730 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38230, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38229, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38228, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2730 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38231, 9631, 1);
            __CrestLoad(38233, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38234, (unsigned long )(& bufp->allocated));
# 2730 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38232, 9632, 0);

          }
          }
          __CrestLoad(38235, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2730 "regex.c"
          tmp___81 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38236);
# 2730 "regex.c"
          bufp->buffer = (unsigned char *)tmp___81;
          {
          __CrestLoad(38239, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38238, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38237, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2730 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38240, 9635, 1);
            __CrestLoad(38242, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38243, (unsigned long )(& __retres238));
# 2730 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2730 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38241, 9637, 0);

          }
          }
          {
          __CrestLoad(38246, (unsigned long )(& old_buffer___22), (long long )((unsigned long )old_buffer___22));
          __CrestLoad(38245, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38244, 13, (long long )((unsigned long )old_buffer___22 != (unsigned long )bufp->buffer));
# 2730 "regex.c"
          if ((unsigned long )old_buffer___22 != (unsigned long )bufp->buffer) {
            __CrestBranch(38247, 9639, 1);
# 2730 "regex.c"
            b = bufp->buffer + (b - old_buffer___22);
# 2730 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___22);
            {
            __CrestLoad(38251, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38250, (unsigned long )0, (long long )0);
            __CrestApply2(38249, 13, (long long )(fixup_alt_jump != 0));
# 2730 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38252, 9641, 1);
# 2730 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___22);
            } else {
              __CrestBranch(38253, 9642, 0);

            }
            }
            {
            __CrestLoad(38256, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38255, (unsigned long )0, (long long )0);
            __CrestApply2(38254, 13, (long long )(laststart != 0));
# 2730 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38257, 9644, 1);
# 2730 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___22);
            } else {
              __CrestBranch(38258, 9645, 0);

            }
            }
            {
            __CrestLoad(38261, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38260, (unsigned long )0, (long long )0);
            __CrestApply2(38259, 13, (long long )(pending_exact != 0));
# 2730 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38262, 9647, 1);
# 2730 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___22);
            } else {
              __CrestBranch(38263, 9648, 0);

            }
            }
          } else {
            __CrestBranch(38248, 9649, 0);

          }
          }
# 2730 "regex.c"
          goto while_break___87;
        }
        while_break___87: ;
        }
      }
      while_break___86: ;
      }
# 2730 "regex.c"
      tmp___82 = b;
# 2730 "regex.c"
      b ++;
      __CrestLoad(38264, (unsigned long )0, (long long )(unsigned char)12);
      __CrestStore(38265, (unsigned long )tmp___82);
# 2730 "regex.c"
      *tmp___82 = (unsigned char)12;
# 2730 "regex.c"
      goto while_break___85;
    }
    while_break___85: ;
    }
# 2731 "regex.c"
    goto switch_break___0;
    case_57:
    case_56:
    case_55:
    case_54:
    case_53:
    case_52:
    case_51:
    case_50:
    case_49:
    {
    __CrestLoad(38270, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(38269, (unsigned long )0, (long long )((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38268, 5, (long long )(syntax & ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38267, (unsigned long )0, (long long )0);
    __CrestApply2(38266, 13, (long long )((syntax & ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2735 "regex.c"
    if ((syntax & ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38271, 9658, 1);
# 2736 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(38272, 9659, 0);

    }
    }
    __CrestLoad(38275, (unsigned long )(& c), (long long )c);
    __CrestLoad(38274, (unsigned long )0, (long long )48);
    __CrestApply2(38273, 1, (long long )((int )c - 48));
    __CrestStore(38276, (unsigned long )(& c1));
# 2738 "regex.c"
    c1 = (unsigned char )((int )c - 48);
    {
    __CrestLoad(38279, (unsigned long )(& c1), (long long )c1);
    __CrestLoad(38278, (unsigned long )(& regnum), (long long )regnum);
    __CrestApply2(38277, 14, (long long )((regnum_t )c1 > regnum));
# 2740 "regex.c"
    if ((regnum_t )c1 > regnum) {
      __CrestBranch(38280, 9662, 1);
# 2741 "regex.c"
      free((void *)compile_stack.stack);
      __CrestClearStack(38282);
      __CrestLoad(38283, (unsigned long )0, (long long )((reg_errcode_t )6));
      __CrestStore(38284, (unsigned long )(& __retres238));
# 2741 "regex.c"
      __retres238 = (reg_errcode_t )6;
# 2741 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(38281, 9665, 0);

    }
    }
    __CrestLoad(38285, (unsigned long )(& c1), (long long )c1);
# 2744 "regex.c"
    tmp___83 = group_in_compile_stack(compile_stack, (regnum_t )c1);
    __CrestHandleReturn(38287, (long long )tmp___83);
    __CrestStore(38286, (unsigned long )(& tmp___83));
    {
    __CrestLoad(38290, (unsigned long )(& tmp___83), (long long )tmp___83);
    __CrestLoad(38289, (unsigned long )0, (long long )0);
    __CrestApply2(38288, 13, (long long )(tmp___83 != 0));
# 2744 "regex.c"
    if (tmp___83 != 0) {
      __CrestBranch(38291, 9668, 1);
# 2745 "regex.c"
      goto normal_char;
    } else {
      __CrestBranch(38292, 9669, 0);

    }
    }
# 2747 "regex.c"
    laststart = b;
    {
# 2748 "regex.c"
    while (1) {
      while_continue___88: ;
      {
# 2748 "regex.c"
      while (1) {
        while_continue___89: ;
        {
        __CrestLoad(38299, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38298, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38297, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38296, (unsigned long )0, (long long )2L);
        __CrestApply2(38295, 0, (long long )((b - bufp->buffer) + 2L));
        __CrestLoad(38294, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38293, 14, (long long )((unsigned long )((b - bufp->buffer) + 2L) > bufp->allocated));
# 2748 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 2L) > bufp->allocated) {
          __CrestBranch(38300, 9678, 1);

        } else {
          __CrestBranch(38301, 9679, 0);
# 2748 "regex.c"
          goto while_break___89;
        }
        }
        {
# 2748 "regex.c"
        while (1) {
          while_continue___90: ;
# 2748 "regex.c"
          old_buffer___23 = bufp->buffer;
          {
          __CrestLoad(38304, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38303, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38302, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2748 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38305, 9685, 1);
            __CrestLoad(38307, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38308, (unsigned long )(& __retres238));
# 2748 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2748 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38306, 9687, 0);

          }
          }
          __CrestLoad(38311, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38310, (unsigned long )0, (long long )1);
          __CrestApply2(38309, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38312, (unsigned long )(& bufp->allocated));
# 2748 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38315, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38314, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38313, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2748 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38316, 9690, 1);
            __CrestLoad(38318, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38319, (unsigned long )(& bufp->allocated));
# 2748 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38317, 9691, 0);

          }
          }
          __CrestLoad(38320, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2748 "regex.c"
          tmp___84 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38321);
# 2748 "regex.c"
          bufp->buffer = (unsigned char *)tmp___84;
          {
          __CrestLoad(38324, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38323, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38322, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2748 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38325, 9694, 1);
            __CrestLoad(38327, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38328, (unsigned long )(& __retres238));
# 2748 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2748 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38326, 9696, 0);

          }
          }
          {
          __CrestLoad(38331, (unsigned long )(& old_buffer___23), (long long )((unsigned long )old_buffer___23));
          __CrestLoad(38330, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38329, 13, (long long )((unsigned long )old_buffer___23 != (unsigned long )bufp->buffer));
# 2748 "regex.c"
          if ((unsigned long )old_buffer___23 != (unsigned long )bufp->buffer) {
            __CrestBranch(38332, 9698, 1);
# 2748 "regex.c"
            b = bufp->buffer + (b - old_buffer___23);
# 2748 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___23);
            {
            __CrestLoad(38336, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38335, (unsigned long )0, (long long )0);
            __CrestApply2(38334, 13, (long long )(fixup_alt_jump != 0));
# 2748 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38337, 9700, 1);
# 2748 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___23);
            } else {
              __CrestBranch(38338, 9701, 0);

            }
            }
            {
            __CrestLoad(38341, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38340, (unsigned long )0, (long long )0);
            __CrestApply2(38339, 13, (long long )(laststart != 0));
# 2748 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38342, 9703, 1);
# 2748 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___23);
            } else {
              __CrestBranch(38343, 9704, 0);

            }
            }
            {
            __CrestLoad(38346, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38345, (unsigned long )0, (long long )0);
            __CrestApply2(38344, 13, (long long )(pending_exact != 0));
# 2748 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38347, 9706, 1);
# 2748 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___23);
            } else {
              __CrestBranch(38348, 9707, 0);

            }
            }
          } else {
            __CrestBranch(38333, 9708, 0);

          }
          }
# 2748 "regex.c"
          goto while_break___90;
        }
        while_break___90: ;
        }
      }
      while_break___89: ;
      }
# 2748 "regex.c"
      tmp___85 = b;
# 2748 "regex.c"
      b ++;
      __CrestLoad(38349, (unsigned long )0, (long long )(unsigned char)8);
      __CrestStore(38350, (unsigned long )tmp___85);
# 2748 "regex.c"
      *tmp___85 = (unsigned char)8;
# 2748 "regex.c"
      tmp___86 = b;
# 2748 "regex.c"
      b ++;
      __CrestLoad(38351, (unsigned long )(& c1), (long long )c1);
      __CrestStore(38352, (unsigned long )tmp___86);
# 2748 "regex.c"
      *tmp___86 = c1;
# 2748 "regex.c"
      goto while_break___88;
    }
    while_break___88: ;
    }
# 2749 "regex.c"
    goto switch_break___0;
    case_63___0:
    case_43___0:
    {
    __CrestLoad(38357, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(38356, (unsigned long )0, (long long )(1UL << 1));
    __CrestApply2(38355, 5, (long long )(syntax & (1UL << 1)));
    __CrestLoad(38354, (unsigned long )0, (long long )0);
    __CrestApply2(38353, 13, (long long )((syntax & (1UL << 1)) != 0));
# 2754 "regex.c"
    if ((syntax & (1UL << 1)) != 0) {
      __CrestBranch(38358, 9717, 1);
# 2755 "regex.c"
      goto handle_plus;
    } else {
      __CrestBranch(38359, 9718, 0);

    }
    }
    normal_backslash:
    switch_default:
    {
    __CrestLoad(38362, (unsigned long )(& translate), (long long )((unsigned long )translate));
    __CrestLoad(38361, (unsigned long )0, (long long )0);
    __CrestApply2(38360, 13, (long long )(translate != 0));
# 2764 "regex.c"
    if (translate != 0) {
      __CrestBranch(38363, 9720, 1);
# 2764 "regex.c"
      mem_233 = translate + c;
      __CrestLoad(38365, (unsigned long )mem_233, (long long )*mem_233);
      __CrestStore(38366, (unsigned long )(& c));
# 2764 "regex.c"
      c = (unsigned char )*mem_233;
    } else {
      __CrestBranch(38364, 9721, 0);
      __CrestLoad(38367, (unsigned long )(& c), (long long )c);
      __CrestStore(38368, (unsigned long )(& c));
# 2764 "regex.c"
      c = c;
    }
    }
# 2765 "regex.c"
    goto normal_char;
    switch_break___0: ;
    }
# 2767 "regex.c"
    goto switch_break;
    normal_char:
    switch_default___0:
    {
    __CrestLoad(38371, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
    __CrestLoad(38370, (unsigned long )0, (long long )0);
    __CrestApply2(38369, 12, (long long )(pending_exact == 0));
# 2774 "regex.c"
    if (pending_exact == 0) {
      __CrestBranch(38372, 9726, 1);
# 2774 "regex.c"
      goto _L___14;
    } else {
      __CrestBranch(38373, 9727, 0);
      {
      __CrestLoad(38380, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
      __CrestLoad(38379, (unsigned long )pending_exact, (long long )*pending_exact);
      __CrestApply2(38378, 18, (long long )((unsigned long )(pending_exact + (int )*pending_exact)));
      __CrestLoad(38377, (unsigned long )0, (long long )1);
      __CrestApply2(38376, 18, (long long )((unsigned long )((pending_exact + (int )*pending_exact) + 1)));
      __CrestLoad(38375, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestApply2(38374, 13, (long long )((unsigned long )((pending_exact + (int )*pending_exact) + 1) != (unsigned long )b));
# 2774 "regex.c"
      if ((unsigned long )((pending_exact + (int )*pending_exact) + 1) != (unsigned long )b) {
        __CrestBranch(38381, 9728, 1);
# 2774 "regex.c"
        goto _L___14;
      } else {
        __CrestBranch(38382, 9729, 0);
        {
        __CrestLoad(38385, (unsigned long )pending_exact, (long long )*pending_exact);
        __CrestLoad(38384, (unsigned long )0, (long long )((1 << 8) - 1));
        __CrestApply2(38383, 12, (long long )((int )*pending_exact == (1 << 8) - 1));
# 2774 "regex.c"
        if ((int )*pending_exact == (1 << 8) - 1) {
          __CrestBranch(38386, 9730, 1);
# 2774 "regex.c"
          goto _L___14;
        } else {
          __CrestBranch(38387, 9731, 0);
          {
          __CrestLoad(38390, (unsigned long )p, (long long )*p);
          __CrestLoad(38389, (unsigned long )0, (long long )42);
          __CrestApply2(38388, 12, (long long )((int const )*p == 42));
# 2774 "regex.c"
          if ((int const )*p == 42) {
            __CrestBranch(38391, 9732, 1);
# 2774 "regex.c"
            goto _L___14;
          } else {
            __CrestBranch(38392, 9733, 0);
            {
            __CrestLoad(38395, (unsigned long )p, (long long )*p);
            __CrestLoad(38394, (unsigned long )0, (long long )94);
            __CrestApply2(38393, 12, (long long )((int const )*p == 94));
# 2774 "regex.c"
            if ((int const )*p == 94) {
              __CrestBranch(38396, 9734, 1);
# 2774 "regex.c"
              goto _L___14;
            } else {
              __CrestBranch(38397, 9735, 0);
              {
              __CrestLoad(38402, (unsigned long )(& syntax), (long long )syntax);
              __CrestLoad(38401, (unsigned long )0, (long long )(1UL << 1));
              __CrestApply2(38400, 5, (long long )(syntax & (1UL << 1)));
              __CrestLoad(38399, (unsigned long )0, (long long )0);
              __CrestApply2(38398, 13, (long long )((syntax & (1UL << 1)) != 0));
# 2774 "regex.c"
              if ((syntax & (1UL << 1)) != 0) {
                __CrestBranch(38403, 9736, 1);
                {
                __CrestLoad(38407, (unsigned long )p, (long long )*p);
                __CrestLoad(38406, (unsigned long )0, (long long )92);
                __CrestApply2(38405, 12, (long long )((int const )*p == 92));
# 2774 "regex.c"
                if ((int const )*p == 92) {
                  __CrestBranch(38408, 9737, 1);
                  {
# 2774 "regex.c"
                  mem_234 = p + 1;
                  {
                  __CrestLoad(38412, (unsigned long )mem_234, (long long )*mem_234);
                  __CrestLoad(38411, (unsigned long )0, (long long )43);
                  __CrestApply2(38410, 12, (long long )((int const )*mem_234 == 43));
# 2774 "regex.c"
                  if ((int const )*mem_234 == 43) {
                    __CrestBranch(38413, 9740, 1);
                    __CrestLoad(38415, (unsigned long )0, (long long )1);
                    __CrestStore(38416, (unsigned long )(& tmp___90));
# 2774 "regex.c"
                    tmp___90 = 1;
                  } else {
                    __CrestBranch(38414, 9741, 0);
                    {
# 2774 "regex.c"
                    mem_235 = p + 1;
                    {
                    __CrestLoad(38419, (unsigned long )mem_235, (long long )*mem_235);
                    __CrestLoad(38418, (unsigned long )0, (long long )63);
                    __CrestApply2(38417, 12, (long long )((int const )*mem_235 == 63));
# 2774 "regex.c"
                    if ((int const )*mem_235 == 63) {
                      __CrestBranch(38420, 9744, 1);
                      __CrestLoad(38422, (unsigned long )0, (long long )1);
                      __CrestStore(38423, (unsigned long )(& tmp___90));
# 2774 "regex.c"
                      tmp___90 = 1;
                    } else {
                      __CrestBranch(38421, 9745, 0);
                      __CrestLoad(38424, (unsigned long )0, (long long )0);
                      __CrestStore(38425, (unsigned long )(& tmp___90));
# 2774 "regex.c"
                      tmp___90 = 0;
                    }
                    }
                    }
                  }
                  }
                  }
                } else {
                  __CrestBranch(38409, 9746, 0);
                  __CrestLoad(38426, (unsigned long )0, (long long )0);
                  __CrestStore(38427, (unsigned long )(& tmp___90));
# 2774 "regex.c"
                  tmp___90 = 0;
                }
                }
                __CrestLoad(38428, (unsigned long )(& tmp___90), (long long )tmp___90);
                __CrestStore(38429, (unsigned long )(& tmp___92));
# 2774 "regex.c"
                tmp___92 = tmp___90;
              } else {
                __CrestBranch(38404, 9748, 0);
                {
                __CrestLoad(38432, (unsigned long )p, (long long )*p);
                __CrestLoad(38431, (unsigned long )0, (long long )43);
                __CrestApply2(38430, 12, (long long )((int const )*p == 43));
# 2774 "regex.c"
                if ((int const )*p == 43) {
                  __CrestBranch(38433, 9749, 1);
                  __CrestLoad(38435, (unsigned long )0, (long long )1);
                  __CrestStore(38436, (unsigned long )(& tmp___91));
# 2774 "regex.c"
                  tmp___91 = 1;
                } else {
                  __CrestBranch(38434, 9750, 0);
                  {
                  __CrestLoad(38439, (unsigned long )p, (long long )*p);
                  __CrestLoad(38438, (unsigned long )0, (long long )63);
                  __CrestApply2(38437, 12, (long long )((int const )*p == 63));
# 2774 "regex.c"
                  if ((int const )*p == 63) {
                    __CrestBranch(38440, 9751, 1);
                    __CrestLoad(38442, (unsigned long )0, (long long )1);
                    __CrestStore(38443, (unsigned long )(& tmp___91));
# 2774 "regex.c"
                    tmp___91 = 1;
                  } else {
                    __CrestBranch(38441, 9752, 0);
                    __CrestLoad(38444, (unsigned long )0, (long long )0);
                    __CrestStore(38445, (unsigned long )(& tmp___91));
# 2774 "regex.c"
                    tmp___91 = 0;
                  }
                  }
                }
                }
                __CrestLoad(38446, (unsigned long )(& tmp___91), (long long )tmp___91);
                __CrestStore(38447, (unsigned long )(& tmp___92));
# 2774 "regex.c"
                tmp___92 = tmp___91;
              }
              }
              {
              __CrestLoad(38450, (unsigned long )(& tmp___92), (long long )tmp___92);
              __CrestLoad(38449, (unsigned long )0, (long long )0);
              __CrestApply2(38448, 13, (long long )(tmp___92 != 0));
# 2774 "regex.c"
              if (tmp___92 != 0) {
                __CrestBranch(38451, 9755, 1);
# 2774 "regex.c"
                goto _L___14;
              } else {
                __CrestBranch(38452, 9756, 0);
                {
                __CrestLoad(38457, (unsigned long )(& syntax), (long long )syntax);
                __CrestLoad(38456, (unsigned long )0, (long long )(((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
                __CrestApply2(38455, 5, (long long )(syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
                __CrestLoad(38454, (unsigned long )0, (long long )0);
                __CrestApply2(38453, 13, (long long )((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2774 "regex.c"
                if ((syntax & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
                  __CrestBranch(38458, 9757, 1);
                  {
                  __CrestLoad(38464, (unsigned long )(& syntax), (long long )syntax);
                  __CrestLoad(38463, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
                  __CrestApply2(38462, 5, (long long )(syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
                  __CrestLoad(38461, (unsigned long )0, (long long )0);
                  __CrestApply2(38460, 13, (long long )((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2774 "regex.c"
                  if ((syntax & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
                    __CrestBranch(38465, 9758, 1);
                    __CrestLoad(38469, (unsigned long )p, (long long )*p);
                    __CrestLoad(38468, (unsigned long )0, (long long )123);
                    __CrestApply2(38467, 12, (long long )((int const )*p == 123));
                    __CrestStore(38470, (unsigned long )(& tmp___94));
# 2774 "regex.c"
                    tmp___94 = (int const )*p == 123;
                  } else {
                    __CrestBranch(38466, 9759, 0);
                    {
# 2774 "regex.c"
                    mem_236 = p + 0;
                    {
                    __CrestLoad(38473, (unsigned long )mem_236, (long long )*mem_236);
                    __CrestLoad(38472, (unsigned long )0, (long long )92);
                    __CrestApply2(38471, 12, (long long )((int const )*mem_236 == 92));
# 2774 "regex.c"
                    if ((int const )*mem_236 == 92) {
                      __CrestBranch(38474, 9762, 1);
                      {
# 2774 "regex.c"
                      mem_237 = p + 1;
                      {
                      __CrestLoad(38478, (unsigned long )mem_237, (long long )*mem_237);
                      __CrestLoad(38477, (unsigned long )0, (long long )123);
                      __CrestApply2(38476, 12, (long long )((int const )*mem_237 == 123));
# 2774 "regex.c"
                      if ((int const )*mem_237 == 123) {
                        __CrestBranch(38479, 9765, 1);
                        __CrestLoad(38481, (unsigned long )0, (long long )1);
                        __CrestStore(38482, (unsigned long )(& tmp___93));
# 2774 "regex.c"
                        tmp___93 = 1;
                      } else {
                        __CrestBranch(38480, 9766, 0);
                        __CrestLoad(38483, (unsigned long )0, (long long )0);
                        __CrestStore(38484, (unsigned long )(& tmp___93));
# 2774 "regex.c"
                        tmp___93 = 0;
                      }
                      }
                      }
                    } else {
                      __CrestBranch(38475, 9767, 0);
                      __CrestLoad(38485, (unsigned long )0, (long long )0);
                      __CrestStore(38486, (unsigned long )(& tmp___93));
# 2774 "regex.c"
                      tmp___93 = 0;
                    }
                    }
                    }
                    __CrestLoad(38487, (unsigned long )(& tmp___93), (long long )tmp___93);
                    __CrestStore(38488, (unsigned long )(& tmp___94));
# 2774 "regex.c"
                    tmp___94 = tmp___93;
                  }
                  }
                  {
                  __CrestLoad(38491, (unsigned long )(& tmp___94), (long long )tmp___94);
                  __CrestLoad(38490, (unsigned long )0, (long long )0);
                  __CrestApply2(38489, 13, (long long )(tmp___94 != 0));
# 2774 "regex.c"
                  if (tmp___94 != 0) {
                    __CrestBranch(38492, 9770, 1);
                    _L___14:
# 2794 "regex.c"
                    laststart = b;
                    {
# 2796 "regex.c"
                    while (1) {
                      while_continue___91: ;
                      {
# 2796 "regex.c"
                      while (1) {
                        while_continue___92: ;
                        {
                        __CrestLoad(38500, (unsigned long )(& b), (long long )((unsigned long )b));
                        __CrestLoad(38499, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                        __CrestApply2(38498, 18, (long long )(b - bufp->buffer));
                        __CrestLoad(38497, (unsigned long )0, (long long )2L);
                        __CrestApply2(38496, 0, (long long )((b - bufp->buffer) + 2L));
                        __CrestLoad(38495, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
                        __CrestApply2(38494, 14, (long long )((unsigned long )((b - bufp->buffer) + 2L) > bufp->allocated));
# 2796 "regex.c"
                        if ((unsigned long )((b - bufp->buffer) + 2L) > bufp->allocated) {
                          __CrestBranch(38501, 9778, 1);

                        } else {
                          __CrestBranch(38502, 9779, 0);
# 2796 "regex.c"
                          goto while_break___92;
                        }
                        }
                        {
# 2796 "regex.c"
                        while (1) {
                          while_continue___93: ;
# 2796 "regex.c"
                          old_buffer___24 = bufp->buffer;
                          {
                          __CrestLoad(38505, (unsigned long )(& bufp->allocated),
                                      (long long )bufp->allocated);
                          __CrestLoad(38504, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                          __CrestApply2(38503, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2796 "regex.c"
                          if (bufp->allocated == (unsigned long )(1L << 16)) {
                            __CrestBranch(38506, 9785, 1);
                            __CrestLoad(38508, (unsigned long )0, (long long )((reg_errcode_t )15));
                            __CrestStore(38509, (unsigned long )(& __retres238));
# 2796 "regex.c"
                            __retres238 = (reg_errcode_t )15;
# 2796 "regex.c"
                            goto return_label;
                          } else {
                            __CrestBranch(38507, 9787, 0);

                          }
                          }
                          __CrestLoad(38512, (unsigned long )(& bufp->allocated),
                                      (long long )bufp->allocated);
                          __CrestLoad(38511, (unsigned long )0, (long long )1);
                          __CrestApply2(38510, 8, (long long )(bufp->allocated << 1));
                          __CrestStore(38513, (unsigned long )(& bufp->allocated));
# 2796 "regex.c"
                          bufp->allocated <<= 1;
                          {
                          __CrestLoad(38516, (unsigned long )(& bufp->allocated),
                                      (long long )bufp->allocated);
                          __CrestLoad(38515, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                          __CrestApply2(38514, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2796 "regex.c"
                          if (bufp->allocated > (unsigned long )(1L << 16)) {
                            __CrestBranch(38517, 9790, 1);
                            __CrestLoad(38519, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
                            __CrestStore(38520, (unsigned long )(& bufp->allocated));
# 2796 "regex.c"
                            bufp->allocated = (unsigned long )(1L << 16);
                          } else {
                            __CrestBranch(38518, 9791, 0);

                          }
                          }
                          __CrestLoad(38521, (unsigned long )(& bufp->allocated),
                                      (long long )bufp->allocated);
# 2796 "regex.c"
                          tmp___87 = realloc((void *)bufp->buffer, bufp->allocated);
                          __CrestClearStack(38522);
# 2796 "regex.c"
                          bufp->buffer = (unsigned char *)tmp___87;
                          {
                          __CrestLoad(38525, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                          __CrestLoad(38524, (unsigned long )0, (long long )((unsigned long )((void *)0)));
                          __CrestApply2(38523, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2796 "regex.c"
                          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
                            __CrestBranch(38526, 9794, 1);
                            __CrestLoad(38528, (unsigned long )0, (long long )((reg_errcode_t )12));
                            __CrestStore(38529, (unsigned long )(& __retres238));
# 2796 "regex.c"
                            __retres238 = (reg_errcode_t )12;
# 2796 "regex.c"
                            goto return_label;
                          } else {
                            __CrestBranch(38527, 9796, 0);

                          }
                          }
                          {
                          __CrestLoad(38532, (unsigned long )(& old_buffer___24),
                                      (long long )((unsigned long )old_buffer___24));
                          __CrestLoad(38531, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
                          __CrestApply2(38530, 13, (long long )((unsigned long )old_buffer___24 != (unsigned long )bufp->buffer));
# 2796 "regex.c"
                          if ((unsigned long )old_buffer___24 != (unsigned long )bufp->buffer) {
                            __CrestBranch(38533, 9798, 1);
# 2796 "regex.c"
                            b = bufp->buffer + (b - old_buffer___24);
# 2796 "regex.c"
                            begalt = bufp->buffer + (begalt - old_buffer___24);
                            {
                            __CrestLoad(38537, (unsigned long )(& fixup_alt_jump),
                                        (long long )((unsigned long )fixup_alt_jump));
                            __CrestLoad(38536, (unsigned long )0, (long long )0);
                            __CrestApply2(38535, 13, (long long )(fixup_alt_jump != 0));
# 2796 "regex.c"
                            if (fixup_alt_jump != 0) {
                              __CrestBranch(38538, 9800, 1);
# 2796 "regex.c"
                              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___24);
                            } else {
                              __CrestBranch(38539, 9801, 0);

                            }
                            }
                            {
                            __CrestLoad(38542, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
                            __CrestLoad(38541, (unsigned long )0, (long long )0);
                            __CrestApply2(38540, 13, (long long )(laststart != 0));
# 2796 "regex.c"
                            if (laststart != 0) {
                              __CrestBranch(38543, 9803, 1);
# 2796 "regex.c"
                              laststart = bufp->buffer + (laststart - old_buffer___24);
                            } else {
                              __CrestBranch(38544, 9804, 0);

                            }
                            }
                            {
                            __CrestLoad(38547, (unsigned long )(& pending_exact),
                                        (long long )((unsigned long )pending_exact));
                            __CrestLoad(38546, (unsigned long )0, (long long )0);
                            __CrestApply2(38545, 13, (long long )(pending_exact != 0));
# 2796 "regex.c"
                            if (pending_exact != 0) {
                              __CrestBranch(38548, 9806, 1);
# 2796 "regex.c"
                              pending_exact = bufp->buffer + (pending_exact - old_buffer___24);
                            } else {
                              __CrestBranch(38549, 9807, 0);

                            }
                            }
                          } else {
                            __CrestBranch(38534, 9808, 0);

                          }
                          }
# 2796 "regex.c"
                          goto while_break___93;
                        }
                        while_break___93: ;
                        }
                      }
                      while_break___92: ;
                      }
# 2796 "regex.c"
                      tmp___88 = b;
# 2796 "regex.c"
                      b ++;
                      __CrestLoad(38550, (unsigned long )0, (long long )(unsigned char)2);
                      __CrestStore(38551, (unsigned long )tmp___88);
# 2796 "regex.c"
                      *tmp___88 = (unsigned char)2;
# 2796 "regex.c"
                      tmp___89 = b;
# 2796 "regex.c"
                      b ++;
                      __CrestLoad(38552, (unsigned long )0, (long long )(unsigned char)0);
                      __CrestStore(38553, (unsigned long )tmp___89);
# 2796 "regex.c"
                      *tmp___89 = (unsigned char)0;
# 2796 "regex.c"
                      goto while_break___91;
                    }
                    while_break___91: ;
                    }
# 2797 "regex.c"
                    pending_exact = b - 1;
                  } else {
                    __CrestBranch(38493, 9816, 0);

                  }
                  }
                } else {
                  __CrestBranch(38459, 9817, 0);

                }
                }
              }
              }
            }
            }
          }
          }
        }
        }
      }
      }
    }
    }
    {
# 2800 "regex.c"
    while (1) {
      while_continue___94: ;
      {
# 2800 "regex.c"
      while (1) {
        while_continue___95: ;
        {
        __CrestLoad(38560, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38559, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38558, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38557, (unsigned long )0, (long long )1L);
        __CrestApply2(38556, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(38555, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38554, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2800 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(38561, 9825, 1);

        } else {
          __CrestBranch(38562, 9826, 0);
# 2800 "regex.c"
          goto while_break___95;
        }
        }
        {
# 2800 "regex.c"
        while (1) {
          while_continue___96: ;
# 2800 "regex.c"
          old_buffer___25 = bufp->buffer;
          {
          __CrestLoad(38565, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38564, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38563, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2800 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38566, 9832, 1);
            __CrestLoad(38568, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38569, (unsigned long )(& __retres238));
# 2800 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2800 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38567, 9834, 0);

          }
          }
          __CrestLoad(38572, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38571, (unsigned long )0, (long long )1);
          __CrestApply2(38570, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38573, (unsigned long )(& bufp->allocated));
# 2800 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38576, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38575, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38574, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2800 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38577, 9837, 1);
            __CrestLoad(38579, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38580, (unsigned long )(& bufp->allocated));
# 2800 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38578, 9838, 0);

          }
          }
          __CrestLoad(38581, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2800 "regex.c"
          tmp___95 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38582);
# 2800 "regex.c"
          bufp->buffer = (unsigned char *)tmp___95;
          {
          __CrestLoad(38585, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38584, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38583, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2800 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38586, 9841, 1);
            __CrestLoad(38588, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38589, (unsigned long )(& __retres238));
# 2800 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2800 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38587, 9843, 0);

          }
          }
          {
          __CrestLoad(38592, (unsigned long )(& old_buffer___25), (long long )((unsigned long )old_buffer___25));
          __CrestLoad(38591, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38590, 13, (long long )((unsigned long )old_buffer___25 != (unsigned long )bufp->buffer));
# 2800 "regex.c"
          if ((unsigned long )old_buffer___25 != (unsigned long )bufp->buffer) {
            __CrestBranch(38593, 9845, 1);
# 2800 "regex.c"
            b = bufp->buffer + (b - old_buffer___25);
# 2800 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___25);
            {
            __CrestLoad(38597, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38596, (unsigned long )0, (long long )0);
            __CrestApply2(38595, 13, (long long )(fixup_alt_jump != 0));
# 2800 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38598, 9847, 1);
# 2800 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___25);
            } else {
              __CrestBranch(38599, 9848, 0);

            }
            }
            {
            __CrestLoad(38602, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38601, (unsigned long )0, (long long )0);
            __CrestApply2(38600, 13, (long long )(laststart != 0));
# 2800 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38603, 9850, 1);
# 2800 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___25);
            } else {
              __CrestBranch(38604, 9851, 0);

            }
            }
            {
            __CrestLoad(38607, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38606, (unsigned long )0, (long long )0);
            __CrestApply2(38605, 13, (long long )(pending_exact != 0));
# 2800 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38608, 9853, 1);
# 2800 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___25);
            } else {
              __CrestBranch(38609, 9854, 0);

            }
            }
          } else {
            __CrestBranch(38594, 9855, 0);

          }
          }
# 2800 "regex.c"
          goto while_break___96;
        }
        while_break___96: ;
        }
      }
      while_break___95: ;
      }
# 2800 "regex.c"
      tmp___96 = b;
# 2800 "regex.c"
      b ++;
      __CrestLoad(38610, (unsigned long )(& c), (long long )c);
      __CrestStore(38611, (unsigned long )tmp___96);
# 2800 "regex.c"
      *tmp___96 = c;
# 2800 "regex.c"
      goto while_break___94;
    }
    while_break___94: ;
    }
    __CrestLoad(38614, (unsigned long )pending_exact, (long long )*pending_exact);
    __CrestLoad(38613, (unsigned long )0, (long long )1);
    __CrestApply2(38612, 0, (long long )((int )*pending_exact + 1));
    __CrestStore(38615, (unsigned long )pending_exact);
# 2801 "regex.c"
    *pending_exact = (unsigned char )((int )*pending_exact + 1);
# 2802 "regex.c"
    goto switch_break;
    switch_break: ;
    }
  }
  while_break: ;
  }
  {
  __CrestLoad(38618, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
  __CrestLoad(38617, (unsigned long )0, (long long )0);
  __CrestApply2(38616, 13, (long long )(fixup_alt_jump != 0));
# 2809 "regex.c"
  if (fixup_alt_jump != 0) {
    __CrestBranch(38619, 9867, 1);
    __CrestLoad(38621, (unsigned long )0, (long long )((re_opcode_t )14));
    __CrestLoad(38626, (unsigned long )(& b), (long long )((unsigned long )b));
    __CrestLoad(38625, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
    __CrestApply2(38624, 18, (long long )(b - fixup_alt_jump));
    __CrestLoad(38623, (unsigned long )0, (long long )3L);
    __CrestApply2(38622, 1, (long long )((b - fixup_alt_jump) - 3L));
# 2810 "regex.c"
    store_op1((re_opcode_t )14, fixup_alt_jump, (int )((b - fixup_alt_jump) - 3L));
    __CrestClearStack(38627);
  } else {
    __CrestBranch(38620, 9868, 0);

  }
  }
  {
  __CrestLoad(38630, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
  __CrestLoad(38629, (unsigned long )0, (long long )0U);
  __CrestApply2(38628, 13, (long long )(compile_stack.avail != 0U));
# 2812 "regex.c"
  if (compile_stack.avail != 0U) {
    __CrestBranch(38631, 9870, 1);
# 2813 "regex.c"
    free((void *)compile_stack.stack);
    __CrestClearStack(38633);
    __CrestLoad(38634, (unsigned long )0, (long long )((reg_errcode_t )8));
    __CrestStore(38635, (unsigned long )(& __retres238));
# 2813 "regex.c"
    __retres238 = (reg_errcode_t )8;
# 2813 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(38632, 9873, 0);

  }
  }
  {
  __CrestLoad(38640, (unsigned long )(& syntax), (long long )syntax);
  __CrestLoad(38639, (unsigned long )0, (long long )((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
  __CrestApply2(38638, 5, (long long )(syntax & ((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
  __CrestLoad(38637, (unsigned long )0, (long long )0);
  __CrestApply2(38636, 13, (long long )((syntax & ((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2817 "regex.c"
  if ((syntax & ((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
    __CrestBranch(38641, 9875, 1);
    {
# 2818 "regex.c"
    while (1) {
      while_continue___97: ;
      {
# 2818 "regex.c"
      while (1) {
        while_continue___98: ;
        {
        __CrestLoad(38649, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(38648, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
        __CrestApply2(38647, 18, (long long )(b - bufp->buffer));
        __CrestLoad(38646, (unsigned long )0, (long long )1L);
        __CrestApply2(38645, 0, (long long )((b - bufp->buffer) + 1L));
        __CrestLoad(38644, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
        __CrestApply2(38643, 14, (long long )((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated));
# 2818 "regex.c"
        if ((unsigned long )((b - bufp->buffer) + 1L) > bufp->allocated) {
          __CrestBranch(38650, 9882, 1);

        } else {
          __CrestBranch(38651, 9883, 0);
# 2818 "regex.c"
          goto while_break___98;
        }
        }
        {
# 2818 "regex.c"
        while (1) {
          while_continue___99: ;
# 2818 "regex.c"
          old_buffer___26 = bufp->buffer;
          {
          __CrestLoad(38654, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38653, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38652, 12, (long long )(bufp->allocated == (unsigned long )(1L << 16)));
# 2818 "regex.c"
          if (bufp->allocated == (unsigned long )(1L << 16)) {
            __CrestBranch(38655, 9889, 1);
            __CrestLoad(38657, (unsigned long )0, (long long )((reg_errcode_t )15));
            __CrestStore(38658, (unsigned long )(& __retres238));
# 2818 "regex.c"
            __retres238 = (reg_errcode_t )15;
# 2818 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38656, 9891, 0);

          }
          }
          __CrestLoad(38661, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38660, (unsigned long )0, (long long )1);
          __CrestApply2(38659, 8, (long long )(bufp->allocated << 1));
          __CrestStore(38662, (unsigned long )(& bufp->allocated));
# 2818 "regex.c"
          bufp->allocated <<= 1;
          {
          __CrestLoad(38665, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
          __CrestLoad(38664, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
          __CrestApply2(38663, 14, (long long )(bufp->allocated > (unsigned long )(1L << 16)));
# 2818 "regex.c"
          if (bufp->allocated > (unsigned long )(1L << 16)) {
            __CrestBranch(38666, 9894, 1);
            __CrestLoad(38668, (unsigned long )0, (long long )((unsigned long )(1L << 16)));
            __CrestStore(38669, (unsigned long )(& bufp->allocated));
# 2818 "regex.c"
            bufp->allocated = (unsigned long )(1L << 16);
          } else {
            __CrestBranch(38667, 9895, 0);

          }
          }
          __CrestLoad(38670, (unsigned long )(& bufp->allocated), (long long )bufp->allocated);
# 2818 "regex.c"
          tmp___97 = realloc((void *)bufp->buffer, bufp->allocated);
          __CrestClearStack(38671);
# 2818 "regex.c"
          bufp->buffer = (unsigned char *)tmp___97;
          {
          __CrestLoad(38674, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestLoad(38673, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(38672, 12, (long long )((unsigned long )bufp->buffer == (unsigned long )((void *)0)));
# 2818 "regex.c"
          if ((unsigned long )bufp->buffer == (unsigned long )((void *)0)) {
            __CrestBranch(38675, 9898, 1);
            __CrestLoad(38677, (unsigned long )0, (long long )((reg_errcode_t )12));
            __CrestStore(38678, (unsigned long )(& __retres238));
# 2818 "regex.c"
            __retres238 = (reg_errcode_t )12;
# 2818 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(38676, 9900, 0);

          }
          }
          {
          __CrestLoad(38681, (unsigned long )(& old_buffer___26), (long long )((unsigned long )old_buffer___26));
          __CrestLoad(38680, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
          __CrestApply2(38679, 13, (long long )((unsigned long )old_buffer___26 != (unsigned long )bufp->buffer));
# 2818 "regex.c"
          if ((unsigned long )old_buffer___26 != (unsigned long )bufp->buffer) {
            __CrestBranch(38682, 9902, 1);
# 2818 "regex.c"
            b = bufp->buffer + (b - old_buffer___26);
# 2818 "regex.c"
            begalt = bufp->buffer + (begalt - old_buffer___26);
            {
            __CrestLoad(38686, (unsigned long )(& fixup_alt_jump), (long long )((unsigned long )fixup_alt_jump));
            __CrestLoad(38685, (unsigned long )0, (long long )0);
            __CrestApply2(38684, 13, (long long )(fixup_alt_jump != 0));
# 2818 "regex.c"
            if (fixup_alt_jump != 0) {
              __CrestBranch(38687, 9904, 1);
# 2818 "regex.c"
              fixup_alt_jump = bufp->buffer + (fixup_alt_jump - old_buffer___26);
            } else {
              __CrestBranch(38688, 9905, 0);

            }
            }
            {
            __CrestLoad(38691, (unsigned long )(& laststart), (long long )((unsigned long )laststart));
            __CrestLoad(38690, (unsigned long )0, (long long )0);
            __CrestApply2(38689, 13, (long long )(laststart != 0));
# 2818 "regex.c"
            if (laststart != 0) {
              __CrestBranch(38692, 9907, 1);
# 2818 "regex.c"
              laststart = bufp->buffer + (laststart - old_buffer___26);
            } else {
              __CrestBranch(38693, 9908, 0);

            }
            }
            {
            __CrestLoad(38696, (unsigned long )(& pending_exact), (long long )((unsigned long )pending_exact));
            __CrestLoad(38695, (unsigned long )0, (long long )0);
            __CrestApply2(38694, 13, (long long )(pending_exact != 0));
# 2818 "regex.c"
            if (pending_exact != 0) {
              __CrestBranch(38697, 9910, 1);
# 2818 "regex.c"
              pending_exact = bufp->buffer + (pending_exact - old_buffer___26);
            } else {
              __CrestBranch(38698, 9911, 0);

            }
            }
          } else {
            __CrestBranch(38683, 9912, 0);

          }
          }
# 2818 "regex.c"
          goto while_break___99;
        }
        while_break___99: ;
        }
      }
      while_break___98: ;
      }
# 2818 "regex.c"
      tmp___98 = b;
# 2818 "regex.c"
      b ++;
      __CrestLoad(38699, (unsigned long )0, (long long )(unsigned char)1);
      __CrestStore(38700, (unsigned long )tmp___98);
# 2818 "regex.c"
      *tmp___98 = (unsigned char)1;
# 2818 "regex.c"
      goto while_break___97;
    }
    while_break___97: ;
    }
  } else {
    __CrestBranch(38642, 9919, 0);

  }
  }
# 2820 "regex.c"
  free((void *)compile_stack.stack);
  __CrestClearStack(38701);
  __CrestLoad(38704, (unsigned long )(& b), (long long )((unsigned long )b));
  __CrestLoad(38703, (unsigned long )(& bufp->buffer), (long long )((unsigned long )bufp->buffer));
  __CrestApply2(38702, 18, (long long )(b - bufp->buffer));
  __CrestStore(38705, (unsigned long )(& bufp->used));
# 2823 "regex.c"
  bufp->used = (unsigned long )(b - bufp->buffer);
  __CrestLoad(38706, (unsigned long )0, (long long )((reg_errcode_t )0));
  __CrestStore(38707, (unsigned long )(& __retres238));
# 2874 "regex.c"
  __retres238 = (reg_errcode_t )0;
  return_label:
  {
  __CrestLoad(38708, (unsigned long )(& __retres238), (long long )__retres238);
  __CrestReturn(38709);
# 1781 "regex.c"
  return (__retres238);
  }
}
}
# 2881 "regex.c"
static void store_op1(re_opcode_t op , unsigned char *loc , int arg )
{
  unsigned char *mem_4 ;
  unsigned char *mem_5 ;

  {
  __CrestCall(38712, 294);
  __CrestStore(38711, (unsigned long )(& arg));
  __CrestStore(38710, (unsigned long )(& op));
  __CrestLoad(38713, (unsigned long )(& op), (long long )op);
  __CrestStore(38714, (unsigned long )loc);
# 2887 "regex.c"
  *loc = (unsigned char )op;
  {
# 2888 "regex.c"
  while (1) {
    while_continue: ;
# 2888 "regex.c"
    mem_4 = (loc + 1) + 0;
    __CrestLoad(38717, (unsigned long )(& arg), (long long )arg);
    __CrestLoad(38716, (unsigned long )0, (long long )255);
    __CrestApply2(38715, 5, (long long )(arg & 255));
    __CrestStore(38718, (unsigned long )mem_4);
# 2888 "regex.c"
    *mem_4 = (unsigned char )(arg & 255);
# 2888 "regex.c"
    mem_5 = (loc + 1) + 1;
    __CrestLoad(38721, (unsigned long )(& arg), (long long )arg);
    __CrestLoad(38720, (unsigned long )0, (long long )8);
    __CrestApply2(38719, 9, (long long )(arg >> 8));
    __CrestStore(38722, (unsigned long )mem_5);
# 2888 "regex.c"
    *mem_5 = (unsigned char )(arg >> 8);
# 2888 "regex.c"
    goto while_break;
  }
  while_break: ;
  }

  {
  __CrestReturn(38723);
# 2881 "regex.c"
  return;
  }
}
}
# 2894 "regex.c"
static void store_op2(re_opcode_t op , unsigned char *loc , int arg1 , int arg2 )
{
  unsigned char *mem_5 ;
  unsigned char *mem_6 ;
  unsigned char *mem_7 ;
  unsigned char *mem_8 ;

  {
  __CrestCall(38727, 295);
  __CrestStore(38726, (unsigned long )(& arg2));
  __CrestStore(38725, (unsigned long )(& arg1));
  __CrestStore(38724, (unsigned long )(& op));
  __CrestLoad(38728, (unsigned long )(& op), (long long )op);
  __CrestStore(38729, (unsigned long )loc);
# 2900 "regex.c"
  *loc = (unsigned char )op;
  {
# 2901 "regex.c"
  while (1) {
    while_continue: ;
# 2901 "regex.c"
    mem_5 = (loc + 1) + 0;
    __CrestLoad(38732, (unsigned long )(& arg1), (long long )arg1);
    __CrestLoad(38731, (unsigned long )0, (long long )255);
    __CrestApply2(38730, 5, (long long )(arg1 & 255));
    __CrestStore(38733, (unsigned long )mem_5);
# 2901 "regex.c"
    *mem_5 = (unsigned char )(arg1 & 255);
# 2901 "regex.c"
    mem_6 = (loc + 1) + 1;
    __CrestLoad(38736, (unsigned long )(& arg1), (long long )arg1);
    __CrestLoad(38735, (unsigned long )0, (long long )8);
    __CrestApply2(38734, 9, (long long )(arg1 >> 8));
    __CrestStore(38737, (unsigned long )mem_6);
# 2901 "regex.c"
    *mem_6 = (unsigned char )(arg1 >> 8);
# 2901 "regex.c"
    goto while_break;
  }
  while_break: ;
  }
  {
# 2902 "regex.c"
  while (1) {
    while_continue___0: ;
# 2902 "regex.c"
    mem_7 = (loc + 3) + 0;
    __CrestLoad(38740, (unsigned long )(& arg2), (long long )arg2);
    __CrestLoad(38739, (unsigned long )0, (long long )255);
    __CrestApply2(38738, 5, (long long )(arg2 & 255));
    __CrestStore(38741, (unsigned long )mem_7);
# 2902 "regex.c"
    *mem_7 = (unsigned char )(arg2 & 255);
# 2902 "regex.c"
    mem_8 = (loc + 3) + 1;
    __CrestLoad(38744, (unsigned long )(& arg2), (long long )arg2);
    __CrestLoad(38743, (unsigned long )0, (long long )8);
    __CrestApply2(38742, 9, (long long )(arg2 >> 8));
    __CrestStore(38745, (unsigned long )mem_8);
# 2902 "regex.c"
    *mem_8 = (unsigned char )(arg2 >> 8);
# 2902 "regex.c"
    goto while_break___0;
  }
  while_break___0: ;
  }

  {
  __CrestReturn(38746);
# 2894 "regex.c"
  return;
  }
}
}
# 2909 "regex.c"
static void insert_op1(re_opcode_t op , unsigned char *loc , int arg , unsigned char *end )
{
  unsigned char *pfrom ;
  register unsigned char *pto ;

  {
  __CrestCall(38749, 296);
  __CrestStore(38748, (unsigned long )(& arg));
  __CrestStore(38747, (unsigned long )(& op));
# 2916 "regex.c"
  pfrom = end;
# 2917 "regex.c"
  pto = end + 3;
  {
# 2919 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(38752, (unsigned long )(& pfrom), (long long )((unsigned long )pfrom));
    __CrestLoad(38751, (unsigned long )(& loc), (long long )((unsigned long )loc));
    __CrestApply2(38750, 13, (long long )((unsigned long )pfrom != (unsigned long )loc));
# 2919 "regex.c"
    if ((unsigned long )pfrom != (unsigned long )loc) {
      __CrestBranch(38753, 9952, 1);

    } else {
      __CrestBranch(38754, 9953, 0);
# 2919 "regex.c"
      goto while_break;
    }
    }
# 2920 "regex.c"
    pto --;
# 2920 "regex.c"
    pfrom --;
    __CrestLoad(38755, (unsigned long )pfrom, (long long )*pfrom);
    __CrestStore(38756, (unsigned long )pto);
# 2920 "regex.c"
    *pto = *pfrom;
  }
  while_break: ;
  }
  __CrestLoad(38757, (unsigned long )(& op), (long long )op);
  __CrestLoad(38758, (unsigned long )(& arg), (long long )arg);
# 2922 "regex.c"
  store_op1(op, loc, arg);
  __CrestClearStack(38759);

  {
  __CrestReturn(38760);
# 2909 "regex.c"
  return;
  }
}
}
# 2928 "regex.c"
static void insert_op2(re_opcode_t op , unsigned char *loc , int arg1 , int arg2 ,
                       unsigned char *end )
{
  unsigned char *pfrom ;
  register unsigned char *pto ;

  {
  __CrestCall(38764, 297);
  __CrestStore(38763, (unsigned long )(& arg2));
  __CrestStore(38762, (unsigned long )(& arg1));
  __CrestStore(38761, (unsigned long )(& op));
# 2935 "regex.c"
  pfrom = end;
# 2936 "regex.c"
  pto = end + 5;
  {
# 2938 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(38767, (unsigned long )(& pfrom), (long long )((unsigned long )pfrom));
    __CrestLoad(38766, (unsigned long )(& loc), (long long )((unsigned long )loc));
    __CrestApply2(38765, 13, (long long )((unsigned long )pfrom != (unsigned long )loc));
# 2938 "regex.c"
    if ((unsigned long )pfrom != (unsigned long )loc) {
      __CrestBranch(38768, 9964, 1);

    } else {
      __CrestBranch(38769, 9965, 0);
# 2938 "regex.c"
      goto while_break;
    }
    }
# 2939 "regex.c"
    pto --;
# 2939 "regex.c"
    pfrom --;
    __CrestLoad(38770, (unsigned long )pfrom, (long long )*pfrom);
    __CrestStore(38771, (unsigned long )pto);
# 2939 "regex.c"
    *pto = *pfrom;
  }
  while_break: ;
  }
  __CrestLoad(38772, (unsigned long )(& op), (long long )op);
  __CrestLoad(38773, (unsigned long )(& arg1), (long long )arg1);
  __CrestLoad(38774, (unsigned long )(& arg2), (long long )arg2);
# 2941 "regex.c"
  store_op2(op, loc, arg1, arg2);
  __CrestClearStack(38775);

  {
  __CrestReturn(38776);
# 2928 "regex.c"
  return;
  }
}
}
# 2949 "regex.c"
static boolean at_begline_loc_p(char const *pattern , char const *p , reg_syntax_t syntax )
{
  char const *prev ;
  boolean prev_prev_backslash ;
  int tmp ;
  int tmp___0 ;
  char const *mem_8 ;
  boolean __retres9 ;

  {
  __CrestCall(38778, 298);
  __CrestStore(38777, (unsigned long )(& syntax));
# 2954 "regex.c"
  prev = p - 2;
  {
  __CrestLoad(38781, (unsigned long )(& prev), (long long )((unsigned long )prev));
  __CrestLoad(38780, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
  __CrestApply2(38779, 14, (long long )((unsigned long )prev > (unsigned long )pattern));
# 2955 "regex.c"
  if ((unsigned long )prev > (unsigned long )pattern) {
    __CrestBranch(38782, 9973, 1);
    {
# 2955 "regex.c"
    mem_8 = prev + -1;
    {
    __CrestLoad(38786, (unsigned long )mem_8, (long long )*mem_8);
    __CrestLoad(38785, (unsigned long )0, (long long )92);
    __CrestApply2(38784, 12, (long long )((int const )*mem_8 == 92));
# 2955 "regex.c"
    if ((int const )*mem_8 == 92) {
      __CrestBranch(38787, 9976, 1);
      __CrestLoad(38789, (unsigned long )0, (long long )1);
      __CrestStore(38790, (unsigned long )(& tmp));
# 2955 "regex.c"
      tmp = 1;
    } else {
      __CrestBranch(38788, 9977, 0);
      __CrestLoad(38791, (unsigned long )0, (long long )0);
      __CrestStore(38792, (unsigned long )(& tmp));
# 2955 "regex.c"
      tmp = 0;
    }
    }
    }
  } else {
    __CrestBranch(38783, 9978, 0);
    __CrestLoad(38793, (unsigned long )0, (long long )0);
    __CrestStore(38794, (unsigned long )(& tmp));
# 2955 "regex.c"
    tmp = 0;
  }
  }
  __CrestLoad(38795, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(38796, (unsigned long )(& prev_prev_backslash));
# 2955 "regex.c"
  prev_prev_backslash = (boolean )tmp;
  {
  __CrestLoad(38799, (unsigned long )prev, (long long )*prev);
  __CrestLoad(38798, (unsigned long )0, (long long )40);
  __CrestApply2(38797, 12, (long long )((int const )*prev == 40));
# 2957 "regex.c"
  if ((int const )*prev == 40) {
    __CrestBranch(38800, 9981, 1);
    {
    __CrestLoad(38806, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(38805, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38804, 5, (long long )(syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38803, (unsigned long )0, (long long )0);
    __CrestApply2(38802, 13, (long long )((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2957 "regex.c"
    if ((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38807, 9982, 1);
      __CrestLoad(38809, (unsigned long )0, (long long )1);
      __CrestStore(38810, (unsigned long )(& tmp___0));
# 2957 "regex.c"
      tmp___0 = 1;
    } else {
      __CrestBranch(38808, 9983, 0);
      {
      __CrestLoad(38813, (unsigned long )(& prev_prev_backslash), (long long )prev_prev_backslash);
      __CrestLoad(38812, (unsigned long )0, (long long )0);
      __CrestApply2(38811, 13, (long long )(prev_prev_backslash != 0));
# 2957 "regex.c"
      if (prev_prev_backslash != 0) {
        __CrestBranch(38814, 9984, 1);
        __CrestLoad(38816, (unsigned long )0, (long long )1);
        __CrestStore(38817, (unsigned long )(& tmp___0));
# 2957 "regex.c"
        tmp___0 = 1;
      } else {
        __CrestBranch(38815, 9985, 0);
# 2957 "regex.c"
        goto _L;
      }
      }
    }
    }
  } else {
    __CrestBranch(38801, 9986, 0);
    _L:
    {
    __CrestLoad(38820, (unsigned long )prev, (long long )*prev);
    __CrestLoad(38819, (unsigned long )0, (long long )124);
    __CrestApply2(38818, 12, (long long )((int const )*prev == 124));
# 2957 "regex.c"
    if ((int const )*prev == 124) {
      __CrestBranch(38821, 9987, 1);
      {
      __CrestLoad(38827, (unsigned long )(& syntax), (long long )syntax);
      __CrestLoad(38826, (unsigned long )0, (long long )(((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(38825, 5, (long long )(syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(38824, (unsigned long )0, (long long )0);
      __CrestApply2(38823, 13, (long long )((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2957 "regex.c"
      if ((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(38828, 9988, 1);
        __CrestLoad(38830, (unsigned long )0, (long long )1);
        __CrestStore(38831, (unsigned long )(& tmp___0));
# 2957 "regex.c"
        tmp___0 = 1;
      } else {
        __CrestBranch(38829, 9989, 0);
        {
        __CrestLoad(38834, (unsigned long )(& prev_prev_backslash), (long long )prev_prev_backslash);
        __CrestLoad(38833, (unsigned long )0, (long long )0);
        __CrestApply2(38832, 13, (long long )(prev_prev_backslash != 0));
# 2957 "regex.c"
        if (prev_prev_backslash != 0) {
          __CrestBranch(38835, 9990, 1);
          __CrestLoad(38837, (unsigned long )0, (long long )1);
          __CrestStore(38838, (unsigned long )(& tmp___0));
# 2957 "regex.c"
          tmp___0 = 1;
        } else {
          __CrestBranch(38836, 9991, 0);
          __CrestLoad(38839, (unsigned long )0, (long long )0);
          __CrestStore(38840, (unsigned long )(& tmp___0));
# 2957 "regex.c"
          tmp___0 = 0;
        }
        }
      }
      }
    } else {
      __CrestBranch(38822, 9992, 0);
      __CrestLoad(38841, (unsigned long )0, (long long )0);
      __CrestStore(38842, (unsigned long )(& tmp___0));
# 2957 "regex.c"
      tmp___0 = 0;
    }
    }
  }
  }
  __CrestLoad(38843, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestStore(38844, (unsigned long )(& __retres9));
# 2957 "regex.c"
  __retres9 = (boolean )tmp___0;
  {
  __CrestLoad(38845, (unsigned long )(& __retres9), (long long )__retres9);
  __CrestReturn(38846);
# 2949 "regex.c"
  return (__retres9);
  }
}
}
# 2968 "regex.c"
static boolean at_endline_loc_p(char const *p , char const *pend , reg_syntax_t syntax )
{
  char const *next ;
  boolean next_backslash ;
  char const *next_next ;
  char const *tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  boolean __retres13 ;

  {
  __CrestCall(38848, 299);
  __CrestStore(38847, (unsigned long )(& syntax));
# 2973 "regex.c"
  next = p;
  __CrestLoad(38851, (unsigned long )next, (long long )*next);
  __CrestLoad(38850, (unsigned long )0, (long long )92);
  __CrestApply2(38849, 12, (long long )((int const )*next == 92));
  __CrestStore(38852, (unsigned long )(& next_backslash));
# 2974 "regex.c"
  next_backslash = (boolean )((int const )*next == 92);
  {
  __CrestLoad(38857, (unsigned long )(& p), (long long )((unsigned long )p));
  __CrestLoad(38856, (unsigned long )0, (long long )1);
  __CrestApply2(38855, 18, (long long )((unsigned long )(p + 1)));
  __CrestLoad(38854, (unsigned long )(& pend), (long long )((unsigned long )pend));
  __CrestApply2(38853, 16, (long long )((unsigned long )(p + 1) < (unsigned long )pend));
# 2975 "regex.c"
  if ((unsigned long )(p + 1) < (unsigned long )pend) {
    __CrestBranch(38858, 9997, 1);
# 2975 "regex.c"
    tmp = p + 1;
  } else {
    __CrestBranch(38859, 9998, 0);
# 2975 "regex.c"
    tmp = (char const *)0;
  }
  }
# 2975 "regex.c"
  next_next = tmp;
  {
  __CrestLoad(38864, (unsigned long )(& syntax), (long long )syntax);
  __CrestLoad(38863, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
  __CrestApply2(38862, 5, (long long )(syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
  __CrestLoad(38861, (unsigned long )0, (long long )0);
  __CrestApply2(38860, 13, (long long )((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2977 "regex.c"
  if ((syntax & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
    __CrestBranch(38865, 10001, 1);
    __CrestLoad(38869, (unsigned long )next, (long long )*next);
    __CrestLoad(38868, (unsigned long )0, (long long )41);
    __CrestApply2(38867, 12, (long long )((int const )*next == 41));
    __CrestStore(38870, (unsigned long )(& tmp___1));
# 2977 "regex.c"
    tmp___1 = (int const )*next == 41;
  } else {
    __CrestBranch(38866, 10002, 0);
    {
    __CrestLoad(38873, (unsigned long )(& next_backslash), (long long )next_backslash);
    __CrestLoad(38872, (unsigned long )0, (long long )0);
    __CrestApply2(38871, 13, (long long )(next_backslash != 0));
# 2977 "regex.c"
    if (next_backslash != 0) {
      __CrestBranch(38874, 10003, 1);
      {
      __CrestLoad(38878, (unsigned long )(& next_next), (long long )((unsigned long )next_next));
      __CrestLoad(38877, (unsigned long )0, (long long )0);
      __CrestApply2(38876, 13, (long long )(next_next != 0));
# 2977 "regex.c"
      if (next_next != 0) {
        __CrestBranch(38879, 10004, 1);
        {
        __CrestLoad(38883, (unsigned long )next_next, (long long )*next_next);
        __CrestLoad(38882, (unsigned long )0, (long long )41);
        __CrestApply2(38881, 12, (long long )((int const )*next_next == 41));
# 2977 "regex.c"
        if ((int const )*next_next == 41) {
          __CrestBranch(38884, 10005, 1);
          __CrestLoad(38886, (unsigned long )0, (long long )1);
          __CrestStore(38887, (unsigned long )(& tmp___0));
# 2977 "regex.c"
          tmp___0 = 1;
        } else {
          __CrestBranch(38885, 10006, 0);
          __CrestLoad(38888, (unsigned long )0, (long long )0);
          __CrestStore(38889, (unsigned long )(& tmp___0));
# 2977 "regex.c"
          tmp___0 = 0;
        }
        }
      } else {
        __CrestBranch(38880, 10007, 0);
        __CrestLoad(38890, (unsigned long )0, (long long )0);
        __CrestStore(38891, (unsigned long )(& tmp___0));
# 2977 "regex.c"
        tmp___0 = 0;
      }
      }
    } else {
      __CrestBranch(38875, 10008, 0);
      __CrestLoad(38892, (unsigned long )0, (long long )0);
      __CrestStore(38893, (unsigned long )(& tmp___0));
# 2977 "regex.c"
      tmp___0 = 0;
    }
    }
    __CrestLoad(38894, (unsigned long )(& tmp___0), (long long )tmp___0);
    __CrestStore(38895, (unsigned long )(& tmp___1));
# 2977 "regex.c"
    tmp___1 = tmp___0;
  }
  }
  {
  __CrestLoad(38898, (unsigned long )(& tmp___1), (long long )tmp___1);
  __CrestLoad(38897, (unsigned long )0, (long long )0);
  __CrestApply2(38896, 13, (long long )(tmp___1 != 0));
# 2977 "regex.c"
  if (tmp___1 != 0) {
    __CrestBranch(38899, 10011, 1);
    __CrestLoad(38901, (unsigned long )0, (long long )1);
    __CrestStore(38902, (unsigned long )(& tmp___4));
# 2977 "regex.c"
    tmp___4 = 1;
  } else {
    __CrestBranch(38900, 10012, 0);
    {
    __CrestLoad(38907, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(38906, (unsigned long )0, (long long )(((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38905, 5, (long long )(syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38904, (unsigned long )0, (long long )0);
    __CrestApply2(38903, 13, (long long )((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 2977 "regex.c"
    if ((syntax & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(38908, 10013, 1);
      __CrestLoad(38912, (unsigned long )next, (long long )*next);
      __CrestLoad(38911, (unsigned long )0, (long long )124);
      __CrestApply2(38910, 12, (long long )((int const )*next == 124));
      __CrestStore(38913, (unsigned long )(& tmp___3));
# 2977 "regex.c"
      tmp___3 = (int const )*next == 124;
    } else {
      __CrestBranch(38909, 10014, 0);
      {
      __CrestLoad(38916, (unsigned long )(& next_backslash), (long long )next_backslash);
      __CrestLoad(38915, (unsigned long )0, (long long )0);
      __CrestApply2(38914, 13, (long long )(next_backslash != 0));
# 2977 "regex.c"
      if (next_backslash != 0) {
        __CrestBranch(38917, 10015, 1);
        {
        __CrestLoad(38921, (unsigned long )(& next_next), (long long )((unsigned long )next_next));
        __CrestLoad(38920, (unsigned long )0, (long long )0);
        __CrestApply2(38919, 13, (long long )(next_next != 0));
# 2977 "regex.c"
        if (next_next != 0) {
          __CrestBranch(38922, 10016, 1);
          {
          __CrestLoad(38926, (unsigned long )next_next, (long long )*next_next);
          __CrestLoad(38925, (unsigned long )0, (long long )124);
          __CrestApply2(38924, 12, (long long )((int const )*next_next == 124));
# 2977 "regex.c"
          if ((int const )*next_next == 124) {
            __CrestBranch(38927, 10017, 1);
            __CrestLoad(38929, (unsigned long )0, (long long )1);
            __CrestStore(38930, (unsigned long )(& tmp___2));
# 2977 "regex.c"
            tmp___2 = 1;
          } else {
            __CrestBranch(38928, 10018, 0);
            __CrestLoad(38931, (unsigned long )0, (long long )0);
            __CrestStore(38932, (unsigned long )(& tmp___2));
# 2977 "regex.c"
            tmp___2 = 0;
          }
          }
        } else {
          __CrestBranch(38923, 10019, 0);
          __CrestLoad(38933, (unsigned long )0, (long long )0);
          __CrestStore(38934, (unsigned long )(& tmp___2));
# 2977 "regex.c"
          tmp___2 = 0;
        }
        }
      } else {
        __CrestBranch(38918, 10020, 0);
        __CrestLoad(38935, (unsigned long )0, (long long )0);
        __CrestStore(38936, (unsigned long )(& tmp___2));
# 2977 "regex.c"
        tmp___2 = 0;
      }
      }
      __CrestLoad(38937, (unsigned long )(& tmp___2), (long long )tmp___2);
      __CrestStore(38938, (unsigned long )(& tmp___3));
# 2977 "regex.c"
      tmp___3 = tmp___2;
    }
    }
    {
    __CrestLoad(38941, (unsigned long )(& tmp___3), (long long )tmp___3);
    __CrestLoad(38940, (unsigned long )0, (long long )0);
    __CrestApply2(38939, 13, (long long )(tmp___3 != 0));
# 2977 "regex.c"
    if (tmp___3 != 0) {
      __CrestBranch(38942, 10023, 1);
      __CrestLoad(38944, (unsigned long )0, (long long )1);
      __CrestStore(38945, (unsigned long )(& tmp___4));
# 2977 "regex.c"
      tmp___4 = 1;
    } else {
      __CrestBranch(38943, 10024, 0);
      __CrestLoad(38946, (unsigned long )0, (long long )0);
      __CrestStore(38947, (unsigned long )(& tmp___4));
# 2977 "regex.c"
      tmp___4 = 0;
    }
    }
  }
  }
  __CrestLoad(38948, (unsigned long )(& tmp___4), (long long )tmp___4);
  __CrestStore(38949, (unsigned long )(& __retres13));
# 2977 "regex.c"
  __retres13 = (boolean )tmp___4;
  {
  __CrestLoad(38950, (unsigned long )(& __retres13), (long long )__retres13);
  __CrestReturn(38951);
# 2968 "regex.c"
  return (__retres13);
  }
}
}
# 2990 "regex.c"
static boolean group_in_compile_stack(compile_stack_type compile_stack , regnum_t regnum )
{
  int this_element ;
  compile_stack_elt_t *mem_4 ;
  boolean __retres5 ;

  {
  __CrestCall(38953, 300);
  __CrestStore(38952, (unsigned long )(& regnum));
  __CrestLoad(38956, (unsigned long )(& compile_stack.avail), (long long )compile_stack.avail);
  __CrestLoad(38955, (unsigned long )0, (long long )1U);
  __CrestApply2(38954, 1, (long long )(compile_stack.avail - 1U));
  __CrestStore(38957, (unsigned long )(& this_element));
# 2997 "regex.c"
  this_element = (int )(compile_stack.avail - 1U);
  {
# 2997 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(38960, (unsigned long )(& this_element), (long long )this_element);
    __CrestLoad(38959, (unsigned long )0, (long long )0);
    __CrestApply2(38958, 17, (long long )(this_element >= 0));
# 2997 "regex.c"
    if (this_element >= 0) {
      __CrestBranch(38961, 10032, 1);

    } else {
      __CrestBranch(38962, 10033, 0);
# 2997 "regex.c"
      goto while_break;
    }
    }
    {
# 3000 "regex.c"
    mem_4 = compile_stack.stack + this_element;
    {
    __CrestLoad(38965, (unsigned long )(& mem_4->regnum), (long long )mem_4->regnum);
    __CrestLoad(38964, (unsigned long )(& regnum), (long long )regnum);
    __CrestApply2(38963, 12, (long long )(mem_4->regnum == regnum));
# 3000 "regex.c"
    if (mem_4->regnum == regnum) {
      __CrestBranch(38966, 10037, 1);
      __CrestLoad(38968, (unsigned long )0, (long long )((boolean )1));
      __CrestStore(38969, (unsigned long )(& __retres5));
# 3001 "regex.c"
      __retres5 = (boolean )1;
# 3001 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(38967, 10039, 0);

    }
    }
    }
    __CrestLoad(38972, (unsigned long )(& this_element), (long long )this_element);
    __CrestLoad(38971, (unsigned long )0, (long long )1);
    __CrestApply2(38970, 1, (long long )(this_element - 1));
    __CrestStore(38973, (unsigned long )(& this_element));
# 2997 "regex.c"
    this_element --;
  }
  while_break: ;
  }
  __CrestLoad(38974, (unsigned long )0, (long long )((boolean )0));
  __CrestStore(38975, (unsigned long )(& __retres5));
# 3003 "regex.c"
  __retres5 = (boolean )0;
  return_label:
  {
  __CrestLoad(38976, (unsigned long )(& __retres5), (long long )__retres5);
  __CrestReturn(38977);
# 2990 "regex.c"
  return (__retres5);
  }
}
}
# 3018 "regex.c"
static reg_errcode_t compile_range(char const **p_ptr , char const *pend , char *translate ,
                                   reg_syntax_t syntax , unsigned char *b )
{
  unsigned int this_char ;
  char const *p ;
  unsigned int range_start ;
  unsigned int range_end ;
  int tmp ;
  unsigned int tmp___0 ;
  unsigned int tmp___1 ;
  unsigned char const *mem_13 ;
  unsigned char const *mem_14 ;
  char *mem_15 ;
  char *mem_16 ;
  unsigned char *mem_17 ;
  unsigned char *mem_18 ;
  reg_errcode_t __retres19 ;

  {
  __CrestCall(38979, 301);
  __CrestStore(38978, (unsigned long )(& syntax));
# 3027 "regex.c"
  p = *p_ptr;
  {
  __CrestLoad(38982, (unsigned long )(& p), (long long )((unsigned long )p));
  __CrestLoad(38981, (unsigned long )(& pend), (long long )((unsigned long )pend));
  __CrestApply2(38980, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 3030 "regex.c"
  if ((unsigned long )p == (unsigned long )pend) {
    __CrestBranch(38983, 10046, 1);
    __CrestLoad(38985, (unsigned long )0, (long long )((reg_errcode_t )11));
    __CrestStore(38986, (unsigned long )(& __retres19));
# 3031 "regex.c"
    __retres19 = (reg_errcode_t )11;
# 3031 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(38984, 10048, 0);

  }
  }
# 3041 "regex.c"
  mem_13 = (unsigned char const *)p + -2;
  __CrestLoad(38987, (unsigned long )mem_13, (long long )*mem_13);
  __CrestStore(38988, (unsigned long )(& range_start));
# 3041 "regex.c"
  range_start = (unsigned int )*mem_13;
# 3042 "regex.c"
  mem_14 = (unsigned char const *)p + 0;
  __CrestLoad(38989, (unsigned long )mem_14, (long long )*mem_14);
  __CrestStore(38990, (unsigned long )(& range_end));
# 3042 "regex.c"
  range_end = (unsigned int )*mem_14;
# 3046 "regex.c"
  (*p_ptr) ++;
  {
  __CrestLoad(38993, (unsigned long )(& range_start), (long long )range_start);
  __CrestLoad(38992, (unsigned long )(& range_end), (long long )range_end);
  __CrestApply2(38991, 14, (long long )(range_start > range_end));
# 3049 "regex.c"
  if (range_start > range_end) {
    __CrestBranch(38994, 10051, 1);
    {
    __CrestLoad(39000, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(38999, (unsigned long )0, (long long )((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(38998, 5, (long long )(syntax & ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(38997, (unsigned long )0, (long long )0);
    __CrestApply2(38996, 13, (long long )((syntax & ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 3050 "regex.c"
    if ((syntax & ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(39001, 10052, 1);
      __CrestLoad(39003, (unsigned long )0, (long long )11);
      __CrestStore(39004, (unsigned long )(& tmp));
# 3050 "regex.c"
      tmp = 11;
    } else {
      __CrestBranch(39002, 10053, 0);
      __CrestLoad(39005, (unsigned long )0, (long long )0);
      __CrestStore(39006, (unsigned long )(& tmp));
# 3050 "regex.c"
      tmp = 0;
    }
    }
    __CrestLoad(39007, (unsigned long )(& tmp), (long long )tmp);
    __CrestStore(39008, (unsigned long )(& __retres19));
# 3050 "regex.c"
    __retres19 = (reg_errcode_t )tmp;
# 3050 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(38995, 10056, 0);

  }
  }
  __CrestLoad(39009, (unsigned long )(& range_start), (long long )range_start);
  __CrestStore(39010, (unsigned long )(& this_char));
# 3056 "regex.c"
  this_char = range_start;
  {
# 3056 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(39013, (unsigned long )(& this_char), (long long )this_char);
    __CrestLoad(39012, (unsigned long )(& range_end), (long long )range_end);
    __CrestApply2(39011, 15, (long long )(this_char <= range_end));
# 3056 "regex.c"
    if (this_char <= range_end) {
      __CrestBranch(39014, 10062, 1);

    } else {
      __CrestBranch(39015, 10063, 0);
# 3056 "regex.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(39018, (unsigned long )(& translate), (long long )((unsigned long )translate));
    __CrestLoad(39017, (unsigned long )0, (long long )0);
    __CrestApply2(39016, 13, (long long )(translate != 0));
# 3058 "regex.c"
    if (translate != 0) {
      __CrestBranch(39019, 10065, 1);
# 3058 "regex.c"
      mem_15 = translate + (unsigned char )this_char;
      __CrestLoad(39021, (unsigned long )mem_15, (long long )*mem_15);
      __CrestStore(39022, (unsigned long )(& tmp___0));
# 3058 "regex.c"
      tmp___0 = (unsigned int )*mem_15;
    } else {
      __CrestBranch(39020, 10066, 0);
      __CrestLoad(39023, (unsigned long )(& this_char), (long long )this_char);
      __CrestStore(39024, (unsigned long )(& tmp___0));
# 3058 "regex.c"
      tmp___0 = this_char;
    }
    }
    {
    __CrestLoad(39027, (unsigned long )(& translate), (long long )((unsigned long )translate));
    __CrestLoad(39026, (unsigned long )0, (long long )0);
    __CrestApply2(39025, 13, (long long )(translate != 0));
# 3058 "regex.c"
    if (translate != 0) {
      __CrestBranch(39028, 10068, 1);
# 3058 "regex.c"
      mem_16 = translate + (unsigned char )this_char;
      __CrestLoad(39030, (unsigned long )mem_16, (long long )*mem_16);
      __CrestStore(39031, (unsigned long )(& tmp___1));
# 3058 "regex.c"
      tmp___1 = (unsigned int )*mem_16;
    } else {
      __CrestBranch(39029, 10069, 0);
      __CrestLoad(39032, (unsigned long )(& this_char), (long long )this_char);
      __CrestStore(39033, (unsigned long )(& tmp___1));
# 3058 "regex.c"
      tmp___1 = this_char;
    }
    }
# 3058 "regex.c"
    mem_17 = b + (int )((unsigned char )tmp___0) / 8;
# 3058 "regex.c"
    mem_18 = b + (int )((unsigned char )tmp___0) / 8;
    __CrestLoad(39040, (unsigned long )mem_18, (long long )*mem_18);
    __CrestLoad(39039, (unsigned long )0, (long long )1);
    __CrestLoad(39038, (unsigned long )(& tmp___1), (long long )tmp___1);
    __CrestLoad(39037, (unsigned long )0, (long long )8);
    __CrestApply2(39036, 4, (long long )((int )((unsigned char )tmp___1) % 8));
    __CrestApply2(39035, 8, (long long )(1 << (int )((unsigned char )tmp___1) % 8));
    __CrestApply2(39034, 6, (long long )((int )*mem_18 | (1 << (int )((unsigned char )tmp___1) % 8)));
    __CrestStore(39041, (unsigned long )mem_17);
# 3058 "regex.c"
    *mem_17 = (unsigned char )((int )*mem_18 | (1 << (int )((unsigned char )tmp___1) % 8));
    __CrestLoad(39044, (unsigned long )(& this_char), (long long )this_char);
    __CrestLoad(39043, (unsigned long )0, (long long )1U);
    __CrestApply2(39042, 0, (long long )(this_char + 1U));
    __CrestStore(39045, (unsigned long )(& this_char));
# 3056 "regex.c"
    this_char ++;
  }
  while_break: ;
  }
  __CrestLoad(39046, (unsigned long )0, (long long )((reg_errcode_t )0));
  __CrestStore(39047, (unsigned long )(& __retres19));
# 3061 "regex.c"
  __retres19 = (reg_errcode_t )0;
  return_label:
  {
  __CrestLoad(39048, (unsigned long )(& __retres19), (long long )__retres19);
  __CrestReturn(39049);
# 3018 "regex.c"
  return (__retres19);
  }
}
}
# 3077 "regex.c"
int re_compile_fastmap(struct re_pattern_buffer *bufp )
{
  int j ;
  int k ;
  fail_stack_type fail_stack ;
  char *destination ;
  unsigned int num_regs ;
  register char *fastmap ;
  unsigned char *pattern ;
  unsigned char *p ;
  unsigned char *pend ;
  boolean path_can_be_null ;
  boolean succeed_n_p ;
  void *tmp ;
  unsigned char *tmp___0 ;
  unsigned char *tmp___1 ;
  unsigned char *tmp___2 ;
  int fastmap_newline ;
  unsigned int tmp___6 ;
  int tmp___7 ;
  void *tmp___8 ;
  int tmp___9 ;
  int tmp___10 ;
  fail_stack_elt_t *mem_26 ;
  unsigned char *mem_27 ;
  char *mem_28 ;
  unsigned char *mem_29 ;
  char *mem_30 ;
  char *mem_31 ;
  unsigned char *mem_32 ;
  char *mem_33 ;
  char *mem_34 ;
  char *mem_35 ;
  char *mem_36 ;
  char *mem_37 ;
  char *mem_38 ;
  unsigned char *mem_39 ;
  unsigned char *mem_40 ;
  fail_stack_elt_t *mem_41 ;
  unsigned char *mem_42 ;
  fail_stack_elt_t *mem_43 ;
  fail_stack_elt_t *mem_44 ;
  unsigned char *mem_45 ;
  unsigned char *mem_46 ;
  int __retres47 ;

  {
  __CrestCall(39050, 302);

  __CrestLoad(39051, (unsigned long )0, (long long )0U);
  __CrestStore(39052, (unsigned long )(& num_regs));
# 3089 "regex.c"
  num_regs = 0U;
# 3091 "regex.c"
  fastmap = bufp->fastmap;
# 3092 "regex.c"
  pattern = bufp->buffer;
# 3093 "regex.c"
  p = pattern;
# 3094 "regex.c"
  pend = pattern + bufp->used;
  __CrestLoad(39053, (unsigned long )0, (long long )((boolean )1));
  __CrestStore(39054, (unsigned long )(& path_can_be_null));
# 3106 "regex.c"
  path_can_be_null = (boolean )1;
  __CrestLoad(39055, (unsigned long )0, (long long )((boolean )0));
  __CrestStore(39056, (unsigned long )(& succeed_n_p));
# 3109 "regex.c"
  succeed_n_p = (boolean )0;
  {
# 3113 "regex.c"
  while (1) {
    while_continue: ;
    __CrestLoad(39057, (unsigned long )0, (long long )(5UL * sizeof(fail_stack_elt_t )));
# 3113 "regex.c"
    tmp = __builtin_alloca(5UL * sizeof(fail_stack_elt_t ));
    __CrestClearStack(39058);
# 3113 "regex.c"
    fail_stack.stack = (fail_stack_elt_t *)tmp;
    {
    __CrestLoad(39061, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
    __CrestLoad(39060, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(39059, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 3113 "regex.c"
    if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
      __CrestBranch(39062, 10080, 1);
      __CrestLoad(39064, (unsigned long )0, (long long )-2);
      __CrestStore(39065, (unsigned long )(& __retres47));
# 3113 "regex.c"
      __retres47 = -2;
# 3113 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(39063, 10082, 0);

    }
    }
    __CrestLoad(39066, (unsigned long )0, (long long )5U);
    __CrestStore(39067, (unsigned long )(& fail_stack.size));
# 3113 "regex.c"
    fail_stack.size = 5U;
    __CrestLoad(39068, (unsigned long )0, (long long )0U);
    __CrestStore(39069, (unsigned long )(& fail_stack.avail));
# 3113 "regex.c"
    fail_stack.avail = 0U;
# 3113 "regex.c"
    goto while_break;
  }
  while_break: ;
  }
  __CrestLoad(39070, (unsigned long )0, (long long )0);
  __CrestLoad(39071, (unsigned long )0, (long long )((size_t )(1 << 8)));
# 3114 "regex.c"
  memset((void *)fastmap, 0, (size_t )(1 << 8));
  __CrestClearStack(39072);
# 3115 "regex.c"
  bufp->fastmap_accurate = 1U;
# 3116 "regex.c"
  bufp->can_be_null = 0U;
  {
# 3118 "regex.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(39075, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(39074, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(39073, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 3120 "regex.c"
    if ((unsigned long )p == (unsigned long )pend) {
      __CrestBranch(39076, 10091, 1);
# 3120 "regex.c"
      goto _L;
    } else {
      __CrestBranch(39077, 10092, 0);
      {
      __CrestLoad(39080, (unsigned long )p, (long long )*p);
      __CrestLoad(39079, (unsigned long )0, (long long )1);
      __CrestApply2(39078, 12, (long long )((int )*p == 1));
# 3120 "regex.c"
      if ((int )*p == 1) {
        __CrestBranch(39081, 10093, 1);
        _L:
        {
        __CrestLoad(39085, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(39084, (unsigned long )0, (long long )0U);
        __CrestApply2(39083, 13, (long long )(fail_stack.avail != 0U));
# 3123 "regex.c"
        if (fail_stack.avail != 0U) {
          __CrestBranch(39086, 10094, 1);
# 3125 "regex.c"
          bufp->can_be_null |= (unsigned int )path_can_be_null;
          __CrestLoad(39088, (unsigned long )0, (long long )((boolean )1));
          __CrestStore(39089, (unsigned long )(& path_can_be_null));
# 3128 "regex.c"
          path_can_be_null = (boolean )1;
          __CrestLoad(39092, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
          __CrestLoad(39091, (unsigned long )0, (long long )1U);
          __CrestApply2(39090, 1, (long long )(fail_stack.avail - 1U));
          __CrestStore(39093, (unsigned long )(& fail_stack.avail));
# 3130 "regex.c"
          (fail_stack.avail) --;
# 3130 "regex.c"
          mem_26 = fail_stack.stack + fail_stack.avail;
# 3130 "regex.c"
          p = mem_26->pointer;
# 3132 "regex.c"
          goto while_continue___0;
        } else {
          __CrestBranch(39087, 10096, 0);
# 3135 "regex.c"
          goto while_break___0;
        }
        }
      } else {
        __CrestBranch(39082, 10097, 0);

      }
      }
    }
    }
# 3141 "regex.c"
    tmp___0 = p;
# 3141 "regex.c"
    p ++;
    {
    {
    __CrestLoad(39096, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39095, (unsigned long )0, (long long )8U);
    __CrestApply2(39094, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 8U));
# 3149 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 8U) {
      __CrestBranch(39097, 10101, 1);
# 3149 "regex.c"
      goto case_8;
    } else {
      __CrestBranch(39098, 10102, 0);

    }
    }
    {
    __CrestLoad(39101, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39100, (unsigned long )0, (long long )2U);
    __CrestApply2(39099, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 2U));
# 3157 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 2U) {
      __CrestBranch(39102, 10104, 1);
# 3157 "regex.c"
      goto case_2;
    } else {
      __CrestBranch(39103, 10105, 0);

    }
    }
    {
    __CrestLoad(39106, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39105, (unsigned long )0, (long long )4U);
    __CrestApply2(39104, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 4U));
# 3162 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 4U) {
      __CrestBranch(39107, 10107, 1);
# 3162 "regex.c"
      goto case_4;
    } else {
      __CrestBranch(39108, 10108, 0);

    }
    }
    {
    __CrestLoad(39111, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39110, (unsigned long )0, (long long )5U);
    __CrestApply2(39109, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 5U));
# 3169 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 5U) {
      __CrestBranch(39112, 10110, 1);
# 3169 "regex.c"
      goto case_5;
    } else {
      __CrestBranch(39113, 10111, 0);

    }
    }
    {
    __CrestLoad(39116, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39115, (unsigned long )0, (long long )24U);
    __CrestApply2(39114, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 24U));
# 3180 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 24U) {
      __CrestBranch(39117, 10113, 1);
# 3180 "regex.c"
      goto case_24;
    } else {
      __CrestBranch(39118, 10114, 0);

    }
    }
    {
    __CrestLoad(39121, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39120, (unsigned long )0, (long long )25U);
    __CrestApply2(39119, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 25U));
# 3187 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 25U) {
      __CrestBranch(39122, 10116, 1);
# 3187 "regex.c"
      goto case_25;
    } else {
      __CrestBranch(39123, 10117, 0);

    }
    }
    {
    __CrestLoad(39126, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39125, (unsigned long )0, (long long )3U);
    __CrestApply2(39124, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 3U));
# 3194 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 3U) {
      __CrestBranch(39127, 10119, 1);
# 3194 "regex.c"
      goto case_3;
    } else {
      __CrestBranch(39128, 10120, 0);

    }
    }
    {
    __CrestLoad(39131, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39130, (unsigned long )0, (long long )20U);
    __CrestApply2(39129, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 20U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 20U) {
      __CrestBranch(39132, 10122, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39133, 10123, 0);

    }
    }
    {
    __CrestLoad(39136, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39135, (unsigned long )0, (long long )27U);
    __CrestApply2(39134, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 27U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 27U) {
      __CrestBranch(39137, 10125, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39138, 10126, 0);

    }
    }
    {
    __CrestLoad(39141, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39140, (unsigned long )0, (long long )26U);
    __CrestApply2(39139, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 26U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 26U) {
      __CrestBranch(39142, 10128, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39143, 10129, 0);

    }
    }
    {
    __CrestLoad(39146, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39145, (unsigned long )0, (long long )29U);
    __CrestApply2(39144, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 29U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 29U) {
      __CrestBranch(39147, 10131, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39148, 10132, 0);

    }
    }
    {
    __CrestLoad(39151, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39150, (unsigned long )0, (long long )28U);
    __CrestApply2(39149, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 28U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 28U) {
      __CrestBranch(39152, 10134, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39153, 10135, 0);

    }
    }
    {
    __CrestLoad(39156, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39155, (unsigned long )0, (long long )12U);
    __CrestApply2(39154, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 12U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 12U) {
      __CrestBranch(39157, 10137, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39158, 10138, 0);

    }
    }
    {
    __CrestLoad(39161, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39160, (unsigned long )0, (long long )11U);
    __CrestApply2(39159, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 11U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 11U) {
      __CrestBranch(39162, 10140, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39163, 10141, 0);

    }
    }
    {
    __CrestLoad(39166, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39165, (unsigned long )0, (long long )10U);
    __CrestApply2(39164, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 10U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 10U) {
      __CrestBranch(39167, 10143, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39168, 10144, 0);

    }
    }
    {
    __CrestLoad(39171, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39170, (unsigned long )0, (long long )9U);
    __CrestApply2(39169, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 9U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 9U) {
      __CrestBranch(39172, 10146, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39173, 10147, 0);

    }
    }
    {
    __CrestLoad(39176, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39175, (unsigned long )0, (long long )0U);
    __CrestApply2(39174, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 0U));
# 3252 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 0U) {
      __CrestBranch(39177, 10149, 1);
# 3252 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(39178, 10150, 0);

    }
    }
    {
    __CrestLoad(39181, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39180, (unsigned long )0, (long long )19U);
    __CrestApply2(39179, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 19U));
# 3261 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 19U) {
      __CrestBranch(39182, 10152, 1);
# 3261 "regex.c"
      goto case_19;
    } else {
      __CrestBranch(39183, 10153, 0);

    }
    }
    {
    __CrestLoad(39186, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39185, (unsigned long )0, (long long )14U);
    __CrestApply2(39184, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 14U));
# 3261 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 14U) {
      __CrestBranch(39187, 10155, 1);
# 3261 "regex.c"
      goto case_19;
    } else {
      __CrestBranch(39188, 10156, 0);

    }
    }
    {
    __CrestLoad(39191, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39190, (unsigned long )0, (long long )13U);
    __CrestApply2(39189, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 13U));
# 3261 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 13U) {
      __CrestBranch(39192, 10158, 1);
# 3261 "regex.c"
      goto case_19;
    } else {
      __CrestBranch(39193, 10159, 0);

    }
    }
    {
    __CrestLoad(39196, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39195, (unsigned long )0, (long long )18U);
    __CrestApply2(39194, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 18U));
# 3261 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 18U) {
      __CrestBranch(39197, 10161, 1);
# 3261 "regex.c"
      goto case_19;
    } else {
      __CrestBranch(39198, 10162, 0);

    }
    }
    {
    __CrestLoad(39201, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39200, (unsigned long )0, (long long )17U);
    __CrestApply2(39199, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 17U));
# 3261 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 17U) {
      __CrestBranch(39202, 10164, 1);
# 3261 "regex.c"
      goto case_19;
    } else {
      __CrestBranch(39203, 10165, 0);

    }
    }
    {
    __CrestLoad(39206, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39205, (unsigned long )0, (long long )22U);
    __CrestApply2(39204, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 22U));
# 3261 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 22U) {
      __CrestBranch(39207, 10167, 1);
# 3261 "regex.c"
      goto case_19;
    } else {
      __CrestBranch(39208, 10168, 0);

    }
    }
    {
    __CrestLoad(39211, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39210, (unsigned long )0, (long long )16U);
    __CrestApply2(39209, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 16U));
# 3289 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 16U) {
      __CrestBranch(39212, 10170, 1);
# 3289 "regex.c"
      goto handle_on_failure_jump;
    } else {
      __CrestBranch(39213, 10171, 0);

    }
    }
    {
    __CrestLoad(39216, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39215, (unsigned long )0, (long long )15U);
    __CrestApply2(39214, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 15U));
# 3289 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 15U) {
      __CrestBranch(39217, 10173, 1);
# 3289 "regex.c"
      goto handle_on_failure_jump;
    } else {
      __CrestBranch(39218, 10174, 0);

    }
    }
    {
    __CrestLoad(39221, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39220, (unsigned long )0, (long long )21U);
    __CrestApply2(39219, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 21U));
# 3320 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 21U) {
      __CrestBranch(39222, 10176, 1);
# 3320 "regex.c"
      goto case_21;
    } else {
      __CrestBranch(39223, 10177, 0);

    }
    }
    {
    __CrestLoad(39226, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39225, (unsigned long )0, (long long )23U);
    __CrestApply2(39224, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 23U));
# 3335 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 23U) {
      __CrestBranch(39227, 10179, 1);
# 3335 "regex.c"
      goto case_23;
    } else {
      __CrestBranch(39228, 10180, 0);

    }
    }
    {
    __CrestLoad(39231, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39230, (unsigned long )0, (long long )7U);
    __CrestApply2(39229, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 7U));
# 3341 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 7U) {
      __CrestBranch(39232, 10182, 1);
# 3341 "regex.c"
      goto case_7;
    } else {
      __CrestBranch(39233, 10183, 0);

    }
    }
    {
    __CrestLoad(39236, (unsigned long )tmp___0, (long long )*tmp___0);
    __CrestLoad(39235, (unsigned long )0, (long long )6U);
    __CrestApply2(39234, 12, (long long )((unsigned int )((re_opcode_t )*tmp___0) == 6U));
# 3341 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___0) == 6U) {
      __CrestBranch(39237, 10185, 1);
# 3341 "regex.c"
      goto case_7;
    } else {
      __CrestBranch(39238, 10186, 0);

    }
    }
# 3346 "regex.c"
    goto switch_default;
    case_8:
# 3150 "regex.c"
    bufp->can_be_null = 1U;
# 3151 "regex.c"
    goto done;
    case_2:
# 3158 "regex.c"
    mem_27 = p + 1;
# 3158 "regex.c"
    mem_28 = fastmap + *mem_27;
    __CrestLoad(39239, (unsigned long )0, (long long )(char)1);
    __CrestStore(39240, (unsigned long )mem_28);
# 3158 "regex.c"
    *mem_28 = (char)1;
# 3159 "regex.c"
    goto switch_break;
    case_4:
# 3163 "regex.c"
    tmp___1 = p;
# 3163 "regex.c"
    p ++;
    __CrestLoad(39245, (unsigned long )tmp___1, (long long )*tmp___1);
    __CrestLoad(39244, (unsigned long )0, (long long )8);
    __CrestApply2(39243, 2, (long long )((int )*tmp___1 * 8));
    __CrestLoad(39242, (unsigned long )0, (long long )1);
    __CrestApply2(39241, 1, (long long )((int )*tmp___1 * 8 - 1));
    __CrestStore(39246, (unsigned long )(& j));
# 3163 "regex.c"
    j = (int )*tmp___1 * 8 - 1;
    {
# 3163 "regex.c"
    while (1) {
      while_continue___1: ;
      {
      __CrestLoad(39249, (unsigned long )(& j), (long long )j);
      __CrestLoad(39248, (unsigned long )0, (long long )0);
      __CrestApply2(39247, 17, (long long )(j >= 0));
# 3163 "regex.c"
      if (j >= 0) {
        __CrestBranch(39250, 10197, 1);

      } else {
        __CrestBranch(39251, 10198, 0);
# 3163 "regex.c"
        goto while_break___1;
      }
      }
      {
# 3164 "regex.c"
      mem_29 = p + j / 8;
      {
      __CrestLoad(39260, (unsigned long )mem_29, (long long )*mem_29);
      __CrestLoad(39259, (unsigned long )0, (long long )1);
      __CrestLoad(39258, (unsigned long )(& j), (long long )j);
      __CrestLoad(39257, (unsigned long )0, (long long )8);
      __CrestApply2(39256, 4, (long long )(j % 8));
      __CrestApply2(39255, 8, (long long )(1 << j % 8));
      __CrestApply2(39254, 5, (long long )((int )*mem_29 & (1 << j % 8)));
      __CrestLoad(39253, (unsigned long )0, (long long )0);
      __CrestApply2(39252, 13, (long long )(((int )*mem_29 & (1 << j % 8)) != 0));
# 3164 "regex.c"
      if (((int )*mem_29 & (1 << j % 8)) != 0) {
        __CrestBranch(39261, 10202, 1);
# 3165 "regex.c"
        mem_30 = fastmap + j;
        __CrestLoad(39263, (unsigned long )0, (long long )(char)1);
        __CrestStore(39264, (unsigned long )mem_30);
# 3165 "regex.c"
        *mem_30 = (char)1;
      } else {
        __CrestBranch(39262, 10203, 0);

      }
      }
      }
      __CrestLoad(39267, (unsigned long )(& j), (long long )j);
      __CrestLoad(39266, (unsigned long )0, (long long )1);
      __CrestApply2(39265, 1, (long long )(j - 1));
      __CrestStore(39268, (unsigned long )(& j));
# 3163 "regex.c"
      j --;
    }
    while_break___1: ;
    }
# 3166 "regex.c"
    goto switch_break;
    case_5:
    __CrestLoad(39271, (unsigned long )p, (long long )*p);
    __CrestLoad(39270, (unsigned long )0, (long long )8);
    __CrestApply2(39269, 2, (long long )((int )*p * 8));
    __CrestStore(39272, (unsigned long )(& j));
# 3171 "regex.c"
    j = (int )*p * 8;
    {
# 3171 "regex.c"
    while (1) {
      while_continue___2: ;
      {
      __CrestLoad(39275, (unsigned long )(& j), (long long )j);
      __CrestLoad(39274, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(39273, 16, (long long )(j < 1 << 8));
# 3171 "regex.c"
      if (j < 1 << 8) {
        __CrestBranch(39276, 10212, 1);

      } else {
        __CrestBranch(39277, 10213, 0);
# 3171 "regex.c"
        goto while_break___2;
      }
      }
# 3172 "regex.c"
      mem_31 = fastmap + j;
      __CrestLoad(39278, (unsigned long )0, (long long )(char)1);
      __CrestStore(39279, (unsigned long )mem_31);
# 3172 "regex.c"
      *mem_31 = (char)1;
      __CrestLoad(39282, (unsigned long )(& j), (long long )j);
      __CrestLoad(39281, (unsigned long )0, (long long )1);
      __CrestApply2(39280, 0, (long long )(j + 1));
      __CrestStore(39283, (unsigned long )(& j));
# 3171 "regex.c"
      j ++;
    }
    while_break___2: ;
    }
# 3174 "regex.c"
    tmp___2 = p;
# 3174 "regex.c"
    p ++;
    __CrestLoad(39288, (unsigned long )tmp___2, (long long )*tmp___2);
    __CrestLoad(39287, (unsigned long )0, (long long )8);
    __CrestApply2(39286, 2, (long long )((int )*tmp___2 * 8));
    __CrestLoad(39285, (unsigned long )0, (long long )1);
    __CrestApply2(39284, 1, (long long )((int )*tmp___2 * 8 - 1));
    __CrestStore(39289, (unsigned long )(& j));
# 3174 "regex.c"
    j = (int )*tmp___2 * 8 - 1;
    {
# 3174 "regex.c"
    while (1) {
      while_continue___3: ;
      {
      __CrestLoad(39292, (unsigned long )(& j), (long long )j);
      __CrestLoad(39291, (unsigned long )0, (long long )0);
      __CrestApply2(39290, 17, (long long )(j >= 0));
# 3174 "regex.c"
      if (j >= 0) {
        __CrestBranch(39293, 10221, 1);

      } else {
        __CrestBranch(39294, 10222, 0);
# 3174 "regex.c"
        goto while_break___3;
      }
      }
      {
# 3175 "regex.c"
      mem_32 = p + j / 8;
      {
      __CrestLoad(39303, (unsigned long )mem_32, (long long )*mem_32);
      __CrestLoad(39302, (unsigned long )0, (long long )1);
      __CrestLoad(39301, (unsigned long )(& j), (long long )j);
      __CrestLoad(39300, (unsigned long )0, (long long )8);
      __CrestApply2(39299, 4, (long long )(j % 8));
      __CrestApply2(39298, 8, (long long )(1 << j % 8));
      __CrestApply2(39297, 5, (long long )((int )*mem_32 & (1 << j % 8)));
      __CrestLoad(39296, (unsigned long )0, (long long )0);
      __CrestApply2(39295, 12, (long long )(((int )*mem_32 & (1 << j % 8)) == 0));
# 3175 "regex.c"
      if (((int )*mem_32 & (1 << j % 8)) == 0) {
        __CrestBranch(39304, 10226, 1);
# 3176 "regex.c"
        mem_33 = fastmap + j;
        __CrestLoad(39306, (unsigned long )0, (long long )(char)1);
        __CrestStore(39307, (unsigned long )mem_33);
# 3176 "regex.c"
        *mem_33 = (char)1;
      } else {
        __CrestBranch(39305, 10227, 0);

      }
      }
      }
      __CrestLoad(39310, (unsigned long )(& j), (long long )j);
      __CrestLoad(39309, (unsigned long )0, (long long )1);
      __CrestApply2(39308, 1, (long long )(j - 1));
      __CrestStore(39311, (unsigned long )(& j));
# 3174 "regex.c"
      j --;
    }
    while_break___3: ;
    }
# 3177 "regex.c"
    goto switch_break;
    case_24:
    __CrestLoad(39312, (unsigned long )0, (long long )0);
    __CrestStore(39313, (unsigned long )(& j));
# 3181 "regex.c"
    j = 0;
    {
# 3181 "regex.c"
    while (1) {
      while_continue___4: ;
      {
      __CrestLoad(39316, (unsigned long )(& j), (long long )j);
      __CrestLoad(39315, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(39314, 16, (long long )(j < 1 << 8));
# 3181 "regex.c"
      if (j < 1 << 8) {
        __CrestBranch(39317, 10236, 1);

      } else {
        __CrestBranch(39318, 10237, 0);
# 3181 "regex.c"
        goto while_break___4;
      }
      }
      {
      __CrestLoad(39321, (unsigned long )(& re_syntax_table[j]), (long long )re_syntax_table[j]);
      __CrestLoad(39320, (unsigned long )0, (long long )1);
      __CrestApply2(39319, 12, (long long )((int )re_syntax_table[j] == 1));
# 3182 "regex.c"
      if ((int )re_syntax_table[j] == 1) {
        __CrestBranch(39322, 10239, 1);
# 3183 "regex.c"
        mem_34 = fastmap + j;
        __CrestLoad(39324, (unsigned long )0, (long long )(char)1);
        __CrestStore(39325, (unsigned long )mem_34);
# 3183 "regex.c"
        *mem_34 = (char)1;
      } else {
        __CrestBranch(39323, 10240, 0);

      }
      }
      __CrestLoad(39328, (unsigned long )(& j), (long long )j);
      __CrestLoad(39327, (unsigned long )0, (long long )1);
      __CrestApply2(39326, 0, (long long )(j + 1));
      __CrestStore(39329, (unsigned long )(& j));
# 3181 "regex.c"
      j ++;
    }
    while_break___4: ;
    }
# 3184 "regex.c"
    goto switch_break;
    case_25:
    __CrestLoad(39330, (unsigned long )0, (long long )0);
    __CrestStore(39331, (unsigned long )(& j));
# 3188 "regex.c"
    j = 0;
    {
# 3188 "regex.c"
    while (1) {
      while_continue___5: ;
      {
      __CrestLoad(39334, (unsigned long )(& j), (long long )j);
      __CrestLoad(39333, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(39332, 16, (long long )(j < 1 << 8));
# 3188 "regex.c"
      if (j < 1 << 8) {
        __CrestBranch(39335, 10249, 1);

      } else {
        __CrestBranch(39336, 10250, 0);
# 3188 "regex.c"
        goto while_break___5;
      }
      }
      {
      __CrestLoad(39339, (unsigned long )(& re_syntax_table[j]), (long long )re_syntax_table[j]);
      __CrestLoad(39338, (unsigned long )0, (long long )1);
      __CrestApply2(39337, 13, (long long )((int )re_syntax_table[j] != 1));
# 3189 "regex.c"
      if ((int )re_syntax_table[j] != 1) {
        __CrestBranch(39340, 10252, 1);
# 3190 "regex.c"
        mem_35 = fastmap + j;
        __CrestLoad(39342, (unsigned long )0, (long long )(char)1);
        __CrestStore(39343, (unsigned long )mem_35);
# 3190 "regex.c"
        *mem_35 = (char)1;
      } else {
        __CrestBranch(39341, 10253, 0);

      }
      }
      __CrestLoad(39346, (unsigned long )(& j), (long long )j);
      __CrestLoad(39345, (unsigned long )0, (long long )1);
      __CrestApply2(39344, 0, (long long )(j + 1));
      __CrestStore(39347, (unsigned long )(& j));
# 3188 "regex.c"
      j ++;
    }
    while_break___5: ;
    }
# 3191 "regex.c"
    goto switch_break;
    case_3:
# 3196 "regex.c"
    mem_36 = fastmap + '\n';
    __CrestLoad(39348, (unsigned long )mem_36, (long long )*mem_36);
    __CrestStore(39349, (unsigned long )(& fastmap_newline));
# 3196 "regex.c"
    fastmap_newline = (int )*mem_36;
    __CrestLoad(39350, (unsigned long )0, (long long )0);
    __CrestStore(39351, (unsigned long )(& j));
# 3199 "regex.c"
    j = 0;
    {
# 3199 "regex.c"
    while (1) {
      while_continue___6: ;
      {
      __CrestLoad(39354, (unsigned long )(& j), (long long )j);
      __CrestLoad(39353, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(39352, 16, (long long )(j < 1 << 8));
# 3199 "regex.c"
      if (j < 1 << 8) {
        __CrestBranch(39355, 10262, 1);

      } else {
        __CrestBranch(39356, 10263, 0);
# 3199 "regex.c"
        goto while_break___6;
      }
      }
# 3200 "regex.c"
      mem_37 = fastmap + j;
      __CrestLoad(39357, (unsigned long )0, (long long )(char)1);
      __CrestStore(39358, (unsigned long )mem_37);
# 3200 "regex.c"
      *mem_37 = (char)1;
      __CrestLoad(39361, (unsigned long )(& j), (long long )j);
      __CrestLoad(39360, (unsigned long )0, (long long )1);
      __CrestApply2(39359, 0, (long long )(j + 1));
      __CrestStore(39362, (unsigned long )(& j));
# 3199 "regex.c"
      j ++;
    }
    while_break___6: ;
    }
    {
    __CrestLoad(39367, (unsigned long )(& bufp->syntax), (long long )bufp->syntax);
    __CrestLoad(39366, (unsigned long )0, (long long )((((((1UL << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(39365, 5, (long long )(bufp->syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(39364, (unsigned long )0, (long long )0);
    __CrestApply2(39363, 12, (long long )((bufp->syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 3203 "regex.c"
    if ((bufp->syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(39368, 10267, 1);
# 3204 "regex.c"
      mem_38 = fastmap + '\n';
      __CrestLoad(39370, (unsigned long )(& fastmap_newline), (long long )fastmap_newline);
      __CrestStore(39371, (unsigned long )mem_38);
# 3204 "regex.c"
      *mem_38 = (char )fastmap_newline;
    } else {
      __CrestBranch(39369, 10268, 0);
      {
      __CrestLoad(39374, (unsigned long )0, (long long )bufp->can_be_null);
      __CrestLoad(39373, (unsigned long )0, (long long )0);
      __CrestApply2(39372, 13, (long long )(bufp->can_be_null != 0));
# 3208 "regex.c"
      if (bufp->can_be_null != 0) {
        __CrestBranch(39375, 10269, 1);
# 3209 "regex.c"
        goto done;
      } else {
        __CrestBranch(39376, 10270, 0);

      }
      }
    }
    }
# 3212 "regex.c"
    goto switch_break;
    case_20:
    case_27:
    case_26:
    case_29:
    case_28:
    case_12:
    case_11:
    case_10:
    case_9:
    case_0:
# 3253 "regex.c"
    goto while_continue___0;
    case_19:
    case_14:
    case_13:
    case_18:
    case_17:
    case_22:
    {
# 3262 "regex.c"
    while (1) {
      while_continue___7: ;
      {
# 3262 "regex.c"
      while (1) {
        while_continue___8: ;
        __CrestLoad(39379, (unsigned long )p, (long long )*p);
        __CrestLoad(39378, (unsigned long )0, (long long )255);
        __CrestApply2(39377, 5, (long long )((int )*p & 255));
        __CrestStore(39380, (unsigned long )(& j));
# 3262 "regex.c"
        j = (int )*p & 255;
# 3262 "regex.c"
        mem_39 = p + 1;
        __CrestLoad(39385, (unsigned long )(& j), (long long )j);
        __CrestLoad(39384, (unsigned long )mem_39, (long long )*mem_39);
        __CrestLoad(39383, (unsigned long )0, (long long )8);
        __CrestApply2(39382, 8, (long long )((int )((signed char )*mem_39) << 8));
        __CrestApply2(39381, 0, (long long )(j + ((int )((signed char )*mem_39) << 8)));
        __CrestStore(39386, (unsigned long )(& j));
# 3262 "regex.c"
        j += (int )((signed char )*mem_39) << 8;
# 3262 "regex.c"
        goto while_break___8;
      }
      while_break___8: ;
      }
# 3262 "regex.c"
      p += 2;
# 3262 "regex.c"
      goto while_break___7;
    }
    while_break___7: ;
    }
# 3263 "regex.c"
    p += j;
    {
    __CrestLoad(39389, (unsigned long )(& j), (long long )j);
    __CrestLoad(39388, (unsigned long )0, (long long )0);
    __CrestApply2(39387, 14, (long long )(j > 0));
# 3264 "regex.c"
    if (j > 0) {
      __CrestBranch(39390, 10287, 1);
# 3265 "regex.c"
      goto while_continue___0;
    } else {
      __CrestBranch(39391, 10288, 0);

    }
    }
    {
    __CrestLoad(39394, (unsigned long )p, (long long )*p);
    __CrestLoad(39393, (unsigned long )0, (long long )15U);
    __CrestApply2(39392, 13, (long long )((unsigned int )((re_opcode_t )*p) != 15U));
# 3272 "regex.c"
    if ((unsigned int )((re_opcode_t )*p) != 15U) {
      __CrestBranch(39395, 10290, 1);
      {
      __CrestLoad(39399, (unsigned long )p, (long long )*p);
      __CrestLoad(39398, (unsigned long )0, (long long )21U);
      __CrestApply2(39397, 13, (long long )((unsigned int )((re_opcode_t )*p) != 21U));
# 3272 "regex.c"
      if ((unsigned int )((re_opcode_t )*p) != 21U) {
        __CrestBranch(39400, 10291, 1);
# 3274 "regex.c"
        goto while_continue___0;
      } else {
        __CrestBranch(39401, 10292, 0);

      }
      }
    } else {
      __CrestBranch(39396, 10293, 0);

    }
    }
# 3276 "regex.c"
    p ++;
    {
# 3277 "regex.c"
    while (1) {
      while_continue___9: ;
      {
# 3277 "regex.c"
      while (1) {
        while_continue___10: ;
        __CrestLoad(39404, (unsigned long )p, (long long )*p);
        __CrestLoad(39403, (unsigned long )0, (long long )255);
        __CrestApply2(39402, 5, (long long )((int )*p & 255));
        __CrestStore(39405, (unsigned long )(& j));
# 3277 "regex.c"
        j = (int )*p & 255;
# 3277 "regex.c"
        mem_40 = p + 1;
        __CrestLoad(39410, (unsigned long )(& j), (long long )j);
        __CrestLoad(39409, (unsigned long )mem_40, (long long )*mem_40);
        __CrestLoad(39408, (unsigned long )0, (long long )8);
        __CrestApply2(39407, 8, (long long )((int )((signed char )*mem_40) << 8));
        __CrestApply2(39406, 0, (long long )(j + ((int )((signed char )*mem_40) << 8)));
        __CrestStore(39411, (unsigned long )(& j));
# 3277 "regex.c"
        j += (int )((signed char )*mem_40) << 8;
# 3277 "regex.c"
        goto while_break___10;
      }
      while_break___10: ;
      }
# 3277 "regex.c"
      p += 2;
# 3277 "regex.c"
      goto while_break___9;
    }
    while_break___9: ;
    }
# 3278 "regex.c"
    p += j;
    {
    __CrestLoad(39414, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
    __CrestLoad(39413, (unsigned long )0, (long long )0U);
    __CrestApply2(39412, 13, (long long )(fail_stack.avail != 0U));
# 3281 "regex.c"
    if (fail_stack.avail != 0U) {
      __CrestBranch(39415, 10309, 1);
      {
# 3281 "regex.c"
      mem_41 = fail_stack.stack + (fail_stack.avail - 1U);
      {
      __CrestLoad(39419, (unsigned long )(& mem_41->pointer), (long long )((unsigned long )mem_41->pointer));
      __CrestLoad(39418, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestApply2(39417, 12, (long long )((unsigned long )mem_41->pointer == (unsigned long )p));
# 3281 "regex.c"
      if ((unsigned long )mem_41->pointer == (unsigned long )p) {
        __CrestBranch(39420, 10312, 1);
        __CrestLoad(39424, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(39423, (unsigned long )0, (long long )1U);
        __CrestApply2(39422, 1, (long long )(fail_stack.avail - 1U));
        __CrestStore(39425, (unsigned long )(& fail_stack.avail));
# 3283 "regex.c"
        (fail_stack.avail) --;
      } else {
        __CrestBranch(39421, 10313, 0);

      }
      }
      }
    } else {
      __CrestBranch(39416, 10314, 0);

    }
    }
# 3285 "regex.c"
    goto while_continue___0;
    handle_on_failure_jump:
    case_16:
    case_15:
    {
# 3291 "regex.c"
    while (1) {
      while_continue___11: ;
      {
# 3291 "regex.c"
      while (1) {
        while_continue___12: ;
        __CrestLoad(39428, (unsigned long )p, (long long )*p);
        __CrestLoad(39427, (unsigned long )0, (long long )255);
        __CrestApply2(39426, 5, (long long )((int )*p & 255));
        __CrestStore(39429, (unsigned long )(& j));
# 3291 "regex.c"
        j = (int )*p & 255;
# 3291 "regex.c"
        mem_42 = p + 1;
        __CrestLoad(39434, (unsigned long )(& j), (long long )j);
        __CrestLoad(39433, (unsigned long )mem_42, (long long )*mem_42);
        __CrestLoad(39432, (unsigned long )0, (long long )8);
        __CrestApply2(39431, 8, (long long )((int )((signed char )*mem_42) << 8));
        __CrestApply2(39430, 0, (long long )(j + ((int )((signed char )*mem_42) << 8)));
        __CrestStore(39435, (unsigned long )(& j));
# 3291 "regex.c"
        j += (int )((signed char )*mem_42) << 8;
# 3291 "regex.c"
        goto while_break___12;
      }
      while_break___12: ;
      }
# 3291 "regex.c"
      p += 2;
# 3291 "regex.c"
      goto while_break___11;
    }
    while_break___11: ;
    }
    {
    __CrestLoad(39440, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(39439, (unsigned long )(& j), (long long )j);
    __CrestApply2(39438, 18, (long long )((unsigned long )(p + j)));
    __CrestLoad(39437, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(39436, 16, (long long )((unsigned long )(p + j) < (unsigned long )pend));
# 3300 "regex.c"
    if ((unsigned long )(p + j) < (unsigned long )pend) {
      __CrestBranch(39441, 10329, 1);
      {
      __CrestLoad(39445, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(39444, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
      __CrestApply2(39443, 12, (long long )(fail_stack.avail == fail_stack.size));
# 3302 "regex.c"
      if (fail_stack.avail == fail_stack.size) {
        __CrestBranch(39446, 10330, 1);
        {
        __CrestLoad(39452, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(39451, (unsigned long )(& re_max_failures), (long long )re_max_failures);
        __CrestLoad(39450, (unsigned long )0, (long long )19);
        __CrestApply2(39449, 2, (long long )(re_max_failures * 19));
        __CrestApply2(39448, 14, (long long )(fail_stack.size > (unsigned int )(re_max_failures * 19)));
# 3302 "regex.c"
        if (fail_stack.size > (unsigned int )(re_max_failures * 19)) {
          __CrestBranch(39453, 10331, 1);
          __CrestLoad(39455, (unsigned long )0, (long long )0);
          __CrestStore(39456, (unsigned long )(& tmp___10));
# 3302 "regex.c"
          tmp___10 = 0;
        } else {
          __CrestBranch(39454, 10332, 0);
          __CrestLoad(39461, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(39460, (unsigned long )0, (long long )1);
          __CrestApply2(39459, 8, (long long )(fail_stack.size << 1));
          __CrestLoad(39458, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(39457, 2, (long long )((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t )));
# 3302 "regex.c"
          tmp___8 = __builtin_alloca((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t ));
          __CrestClearStack(39462);
# 3302 "regex.c"
          destination = (char *)tmp___8;
          __CrestLoad(39465, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(39464, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(39463, 2, (long long )((unsigned long )fail_stack.size * sizeof(fail_stack_elt_t )));
# 3302 "regex.c"
          memcpy((void * __restrict )destination, (void const * __restrict )fail_stack.stack,
                 (unsigned long )fail_stack.size * sizeof(fail_stack_elt_t ));
          __CrestClearStack(39466);
# 3302 "regex.c"
          fail_stack.stack = (fail_stack_elt_t *)destination;
          {
          __CrestLoad(39469, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
          __CrestLoad(39468, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(39467, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 3302 "regex.c"
          if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
            __CrestBranch(39470, 10334, 1);
            __CrestLoad(39472, (unsigned long )0, (long long )0);
            __CrestStore(39473, (unsigned long )(& tmp___9));
# 3302 "regex.c"
            tmp___9 = 0;
          } else {
            __CrestBranch(39471, 10335, 0);
            __CrestLoad(39476, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
            __CrestLoad(39475, (unsigned long )0, (long long )1);
            __CrestApply2(39474, 8, (long long )(fail_stack.size << 1));
            __CrestStore(39477, (unsigned long )(& fail_stack.size));
# 3302 "regex.c"
            fail_stack.size <<= 1;
            __CrestLoad(39478, (unsigned long )0, (long long )1);
            __CrestStore(39479, (unsigned long )(& tmp___9));
# 3302 "regex.c"
            tmp___9 = 1;
          }
          }
          __CrestLoad(39480, (unsigned long )(& tmp___9), (long long )tmp___9);
          __CrestStore(39481, (unsigned long )(& tmp___10));
# 3302 "regex.c"
          tmp___10 = tmp___9;
        }
        }
        {
        __CrestLoad(39484, (unsigned long )(& tmp___10), (long long )tmp___10);
        __CrestLoad(39483, (unsigned long )0, (long long )0);
        __CrestApply2(39482, 13, (long long )(tmp___10 != 0));
# 3302 "regex.c"
        if (tmp___10 != 0) {
          __CrestBranch(39485, 10338, 1);
          __CrestLoad(39487, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
          __CrestStore(39488, (unsigned long )(& tmp___6));
# 3302 "regex.c"
          tmp___6 = fail_stack.avail;
          __CrestLoad(39491, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
          __CrestLoad(39490, (unsigned long )0, (long long )1U);
          __CrestApply2(39489, 0, (long long )(fail_stack.avail + 1U));
          __CrestStore(39492, (unsigned long )(& fail_stack.avail));
# 3302 "regex.c"
          (fail_stack.avail) ++;
# 3302 "regex.c"
          mem_43 = fail_stack.stack + tmp___6;
# 3302 "regex.c"
          mem_43->pointer = p + j;
          __CrestLoad(39493, (unsigned long )0, (long long )1);
          __CrestStore(39494, (unsigned long )(& tmp___7));
# 3302 "regex.c"
          tmp___7 = 1;
        } else {
          __CrestBranch(39486, 10339, 0);
          __CrestLoad(39495, (unsigned long )0, (long long )0);
          __CrestStore(39496, (unsigned long )(& tmp___7));
# 3302 "regex.c"
          tmp___7 = 0;
        }
        }
      } else {
        __CrestBranch(39447, 10340, 0);
        __CrestLoad(39497, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(39498, (unsigned long )(& tmp___6));
# 3302 "regex.c"
        tmp___6 = fail_stack.avail;
        __CrestLoad(39501, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(39500, (unsigned long )0, (long long )1U);
        __CrestApply2(39499, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(39502, (unsigned long )(& fail_stack.avail));
# 3302 "regex.c"
        (fail_stack.avail) ++;
# 3302 "regex.c"
        mem_44 = fail_stack.stack + tmp___6;
# 3302 "regex.c"
        mem_44->pointer = p + j;
        __CrestLoad(39503, (unsigned long )0, (long long )1);
        __CrestStore(39504, (unsigned long )(& tmp___7));
# 3302 "regex.c"
        tmp___7 = 1;
      }
      }
      {
      __CrestLoad(39507, (unsigned long )(& tmp___7), (long long )tmp___7);
      __CrestLoad(39506, (unsigned long )0, (long long )0);
      __CrestApply2(39505, 13, (long long )(tmp___7 != 0));
# 3302 "regex.c"
      if (tmp___7 != 0) {
        __CrestBranch(39508, 10342, 1);

      } else {
        __CrestBranch(39509, 10343, 0);
        __CrestLoad(39510, (unsigned long )0, (long long )-2);
        __CrestStore(39511, (unsigned long )(& __retres47));
# 3305 "regex.c"
        __retres47 = -2;
# 3305 "regex.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(39442, 10345, 0);
# 3309 "regex.c"
      bufp->can_be_null = 1U;
    }
    }
    {
    __CrestLoad(39514, (unsigned long )(& succeed_n_p), (long long )succeed_n_p);
    __CrestLoad(39513, (unsigned long )0, (long long )0);
    __CrestApply2(39512, 13, (long long )(succeed_n_p != 0));
# 3311 "regex.c"
    if (succeed_n_p != 0) {
      __CrestBranch(39515, 10347, 1);
      {
# 3313 "regex.c"
      while (1) {
        while_continue___13: ;
        {
# 3313 "regex.c"
        while (1) {
          while_continue___14: ;
          __CrestLoad(39519, (unsigned long )p, (long long )*p);
          __CrestLoad(39518, (unsigned long )0, (long long )255);
          __CrestApply2(39517, 5, (long long )((int )*p & 255));
          __CrestStore(39520, (unsigned long )(& k));
# 3313 "regex.c"
          k = (int )*p & 255;
# 3313 "regex.c"
          mem_45 = p + 1;
          __CrestLoad(39525, (unsigned long )(& k), (long long )k);
          __CrestLoad(39524, (unsigned long )mem_45, (long long )*mem_45);
          __CrestLoad(39523, (unsigned long )0, (long long )8);
          __CrestApply2(39522, 8, (long long )((int )((signed char )*mem_45) << 8));
          __CrestApply2(39521, 0, (long long )(k + ((int )((signed char )*mem_45) << 8)));
          __CrestStore(39526, (unsigned long )(& k));
# 3313 "regex.c"
          k += (int )((signed char )*mem_45) << 8;
# 3313 "regex.c"
          goto while_break___14;
        }
        while_break___14: ;
        }
# 3313 "regex.c"
        p += 2;
# 3313 "regex.c"
        goto while_break___13;
      }
      while_break___13: ;
      }
      __CrestLoad(39527, (unsigned long )0, (long long )((boolean )0));
      __CrestStore(39528, (unsigned long )(& succeed_n_p));
# 3314 "regex.c"
      succeed_n_p = (boolean )0;
    } else {
      __CrestBranch(39516, 10360, 0);

    }
    }
# 3317 "regex.c"
    goto while_continue___0;
    case_21:
# 3322 "regex.c"
    p += 2;
    {
# 3325 "regex.c"
    while (1) {
      while_continue___15: ;
      {
# 3325 "regex.c"
      while (1) {
        while_continue___16: ;
        __CrestLoad(39531, (unsigned long )p, (long long )*p);
        __CrestLoad(39530, (unsigned long )0, (long long )255);
        __CrestApply2(39529, 5, (long long )((int )*p & 255));
        __CrestStore(39532, (unsigned long )(& k));
# 3325 "regex.c"
        k = (int )*p & 255;
# 3325 "regex.c"
        mem_46 = p + 1;
        __CrestLoad(39537, (unsigned long )(& k), (long long )k);
        __CrestLoad(39536, (unsigned long )mem_46, (long long )*mem_46);
        __CrestLoad(39535, (unsigned long )0, (long long )8);
        __CrestApply2(39534, 8, (long long )((int )((signed char )*mem_46) << 8));
        __CrestApply2(39533, 0, (long long )(k + ((int )((signed char )*mem_46) << 8)));
        __CrestStore(39538, (unsigned long )(& k));
# 3325 "regex.c"
        k += (int )((signed char )*mem_46) << 8;
# 3325 "regex.c"
        goto while_break___16;
      }
      while_break___16: ;
      }
# 3325 "regex.c"
      p += 2;
# 3325 "regex.c"
      goto while_break___15;
    }
    while_break___15: ;
    }
    {
    __CrestLoad(39541, (unsigned long )(& k), (long long )k);
    __CrestLoad(39540, (unsigned long )0, (long long )0);
    __CrestApply2(39539, 12, (long long )(k == 0));
# 3326 "regex.c"
    if (k == 0) {
      __CrestBranch(39542, 10376, 1);
# 3328 "regex.c"
      p -= 4;
      __CrestLoad(39544, (unsigned long )0, (long long )((boolean )1));
      __CrestStore(39545, (unsigned long )(& succeed_n_p));
# 3329 "regex.c"
      succeed_n_p = (boolean )1;
# 3330 "regex.c"
      goto handle_on_failure_jump;
    } else {
      __CrestBranch(39543, 10378, 0);

    }
    }
# 3332 "regex.c"
    goto while_continue___0;
    case_23:
# 3336 "regex.c"
    p += 4;
# 3337 "regex.c"
    goto while_continue___0;
    case_7:
    case_6:
# 3342 "regex.c"
    p += 2;
# 3343 "regex.c"
    goto while_continue___0;
    switch_default:
# 3347 "regex.c"
    abort();
    __CrestClearStack(39546);
    switch_break: ;
    }
    __CrestLoad(39547, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(39548, (unsigned long )(& path_can_be_null));
# 3356 "regex.c"
    path_can_be_null = (boolean )0;
# 3357 "regex.c"
    p = pend;
  }
  while_break___0: ;
  }
# 3362 "regex.c"
  bufp->can_be_null |= (unsigned int )path_can_be_null;
  done: ;
  __CrestLoad(39549, (unsigned long )0, (long long )0);
  __CrestStore(39550, (unsigned long )(& __retres47));
# 3366 "regex.c"
  __retres47 = 0;
  return_label:
  {
  __CrestLoad(39551, (unsigned long )(& __retres47), (long long )__retres47);
  __CrestReturn(39552);
# 3077 "regex.c"
  return (__retres47);
  }
}
}
# 3382 "regex.c"
void re_set_registers(struct re_pattern_buffer *bufp , struct re_registers *regs ,
                      unsigned int num_regs , regoff_t *starts , regoff_t *ends )
{
  regoff_t *tmp ;

  {
  __CrestCall(39554, 303);
  __CrestStore(39553, (unsigned long )(& num_regs));
  {
  __CrestLoad(39557, (unsigned long )(& num_regs), (long long )num_regs);
  __CrestLoad(39556, (unsigned long )0, (long long )0);
  __CrestApply2(39555, 13, (long long )(num_regs != 0));
# 3389 "regex.c"
  if (num_regs != 0) {
    __CrestBranch(39558, 10393, 1);
# 3391 "regex.c"
    bufp->regs_allocated = 1U;
    __CrestLoad(39560, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestStore(39561, (unsigned long )(& regs->num_regs));
# 3392 "regex.c"
    regs->num_regs = num_regs;
# 3393 "regex.c"
    regs->start = starts;
# 3394 "regex.c"
    regs->end = ends;
  } else {
    __CrestBranch(39559, 10394, 0);
# 3398 "regex.c"
    bufp->regs_allocated = 0U;
    __CrestLoad(39562, (unsigned long )0, (long long )0U);
    __CrestStore(39563, (unsigned long )(& regs->num_regs));
# 3399 "regex.c"
    regs->num_regs = 0U;
# 3400 "regex.c"
    tmp = (regoff_t *)0;
# 3400 "regex.c"
    regs->end = tmp;
# 3400 "regex.c"
    regs->start = tmp;
  }
  }

  {
  __CrestReturn(39564);
# 3382 "regex.c"
  return;
  }
}
}
# 3409 "regex.c"
int re_search(struct re_pattern_buffer *bufp , char const *string , int size , int startpos ,
              int range , struct re_registers *regs )
{
  int tmp ;

  {
  __CrestCall(39568, 304);
  __CrestStore(39567, (unsigned long )(& range));
  __CrestStore(39566, (unsigned long )(& startpos));
  __CrestStore(39565, (unsigned long )(& size));
  __CrestLoad(39569, (unsigned long )0, (long long )0);
  __CrestLoad(39570, (unsigned long )(& size), (long long )size);
  __CrestLoad(39571, (unsigned long )(& startpos), (long long )startpos);
  __CrestLoad(39572, (unsigned long )(& range), (long long )range);
  __CrestLoad(39573, (unsigned long )(& size), (long long )size);
# 3416 "regex.c"
  tmp = re_search_2(bufp, (char const *)((void *)0), 0, string, size, startpos,
                    range, regs, size);
  __CrestHandleReturn(39575, (long long )tmp);
  __CrestStore(39574, (unsigned long )(& tmp));
  {
  __CrestLoad(39576, (unsigned long )(& tmp), (long long )tmp);
  __CrestReturn(39577);
# 3416 "regex.c"
  return (tmp);
  }
}
}
# 3442 "regex.c"
int re_search_2(struct re_pattern_buffer *bufp , char const *string1 , int size1 ,
                char const *string2 , int size2 , int startpos , int range , struct re_registers *regs ,
                int stop )
{
  int val ;
  char *fastmap ;
  char *translate ;
  int total_size ;
  int endpos ;
  int tmp ;
  register char const *d ;
  int lim ;
  int irange ;
  char const *tmp___0 ;
  char const *tmp___1 ;
  char const *tmp___2 ;
  char c ;
  int tmp___3 ;
  int tmp___4 ;
  unsigned char *mem_25 ;
  char *mem_26 ;
  char *mem_27 ;
  char *mem_28 ;
  char const *mem_29 ;
  char const *mem_30 ;
  char const *mem_31 ;
  char *mem_32 ;
  char *mem_33 ;
  int __retres34 ;

  {
  __CrestCall(39583, 305);
  __CrestStore(39582, (unsigned long )(& stop));
  __CrestStore(39581, (unsigned long )(& range));
  __CrestStore(39580, (unsigned long )(& startpos));
  __CrestStore(39579, (unsigned long )(& size2));
  __CrestStore(39578, (unsigned long )(& size1));
# 3453 "regex.c"
  fastmap = bufp->fastmap;
# 3454 "regex.c"
  translate = bufp->translate;
  __CrestLoad(39586, (unsigned long )(& size1), (long long )size1);
  __CrestLoad(39585, (unsigned long )(& size2), (long long )size2);
  __CrestApply2(39584, 0, (long long )(size1 + size2));
  __CrestStore(39587, (unsigned long )(& total_size));
# 3455 "regex.c"
  total_size = size1 + size2;
  __CrestLoad(39590, (unsigned long )(& startpos), (long long )startpos);
  __CrestLoad(39589, (unsigned long )(& range), (long long )range);
  __CrestApply2(39588, 0, (long long )(startpos + range));
  __CrestStore(39591, (unsigned long )(& endpos));
# 3456 "regex.c"
  endpos = startpos + range;
  {
  __CrestLoad(39594, (unsigned long )(& startpos), (long long )startpos);
  __CrestLoad(39593, (unsigned long )0, (long long )0);
  __CrestApply2(39592, 16, (long long )(startpos < 0));
# 3459 "regex.c"
  if (startpos < 0) {
    __CrestBranch(39595, 10401, 1);
    __CrestLoad(39597, (unsigned long )0, (long long )-1);
    __CrestStore(39598, (unsigned long )(& __retres34));
# 3460 "regex.c"
    __retres34 = -1;
# 3460 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(39596, 10403, 0);
    {
    __CrestLoad(39601, (unsigned long )(& startpos), (long long )startpos);
    __CrestLoad(39600, (unsigned long )(& total_size), (long long )total_size);
    __CrestApply2(39599, 14, (long long )(startpos > total_size));
# 3459 "regex.c"
    if (startpos > total_size) {
      __CrestBranch(39602, 10404, 1);
      __CrestLoad(39604, (unsigned long )0, (long long )-1);
      __CrestStore(39605, (unsigned long )(& __retres34));
# 3460 "regex.c"
      __retres34 = -1;
# 3460 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(39603, 10406, 0);

    }
    }
  }
  }
  {
  __CrestLoad(39608, (unsigned long )(& endpos), (long long )endpos);
  __CrestLoad(39607, (unsigned long )0, (long long )0);
  __CrestApply2(39606, 16, (long long )(endpos < 0));
# 3465 "regex.c"
  if (endpos < 0) {
    __CrestBranch(39609, 10408, 1);
    __CrestLoad(39613, (unsigned long )0, (long long )0);
    __CrestLoad(39612, (unsigned long )(& startpos), (long long )startpos);
    __CrestApply2(39611, 1, (long long )(0 - startpos));
    __CrestStore(39614, (unsigned long )(& range));
# 3466 "regex.c"
    range = 0 - startpos;
  } else {
    __CrestBranch(39610, 10409, 0);
    {
    __CrestLoad(39617, (unsigned long )(& endpos), (long long )endpos);
    __CrestLoad(39616, (unsigned long )(& total_size), (long long )total_size);
    __CrestApply2(39615, 14, (long long )(endpos > total_size));
# 3467 "regex.c"
    if (endpos > total_size) {
      __CrestBranch(39618, 10410, 1);
      __CrestLoad(39622, (unsigned long )(& total_size), (long long )total_size);
      __CrestLoad(39621, (unsigned long )(& startpos), (long long )startpos);
      __CrestApply2(39620, 1, (long long )(total_size - startpos));
      __CrestStore(39623, (unsigned long )(& range));
# 3468 "regex.c"
      range = total_size - startpos;
    } else {
      __CrestBranch(39619, 10411, 0);

    }
    }
  }
  }
  {
  __CrestLoad(39626, (unsigned long )(& bufp->used), (long long )bufp->used);
  __CrestLoad(39625, (unsigned long )0, (long long )0UL);
  __CrestApply2(39624, 14, (long long )(bufp->used > 0UL));
# 3472 "regex.c"
  if (bufp->used > 0UL) {
    __CrestBranch(39627, 10413, 1);
    {
# 3472 "regex.c"
    mem_25 = bufp->buffer + 0;
    {
    __CrestLoad(39631, (unsigned long )mem_25, (long long )*mem_25);
    __CrestLoad(39630, (unsigned long )0, (long long )11U);
    __CrestApply2(39629, 12, (long long )((unsigned int )((re_opcode_t )*mem_25) == 11U));
# 3472 "regex.c"
    if ((unsigned int )((re_opcode_t )*mem_25) == 11U) {
      __CrestBranch(39632, 10416, 1);
      {
      __CrestLoad(39636, (unsigned long )(& range), (long long )range);
      __CrestLoad(39635, (unsigned long )0, (long long )0);
      __CrestApply2(39634, 14, (long long )(range > 0));
# 3472 "regex.c"
      if (range > 0) {
        __CrestBranch(39637, 10417, 1);
        {
        __CrestLoad(39641, (unsigned long )(& startpos), (long long )startpos);
        __CrestLoad(39640, (unsigned long )0, (long long )0);
        __CrestApply2(39639, 14, (long long )(startpos > 0));
# 3474 "regex.c"
        if (startpos > 0) {
          __CrestBranch(39642, 10418, 1);
          __CrestLoad(39644, (unsigned long )0, (long long )-1);
          __CrestStore(39645, (unsigned long )(& __retres34));
# 3475 "regex.c"
          __retres34 = -1;
# 3475 "regex.c"
          goto return_label;
        } else {
          __CrestBranch(39643, 10420, 0);
          __CrestLoad(39646, (unsigned long )0, (long long )1);
          __CrestStore(39647, (unsigned long )(& range));
# 3477 "regex.c"
          range = 1;
        }
        }
      } else {
        __CrestBranch(39638, 10421, 0);

      }
      }
    } else {
      __CrestBranch(39633, 10422, 0);

    }
    }
    }
  } else {
    __CrestBranch(39628, 10423, 0);

  }
  }
  {
  __CrestLoad(39650, (unsigned long )(& fastmap), (long long )((unsigned long )fastmap));
  __CrestLoad(39649, (unsigned long )0, (long long )0);
  __CrestApply2(39648, 13, (long long )(fastmap != 0));
# 3492 "regex.c"
  if (fastmap != 0) {
    __CrestBranch(39651, 10425, 1);
    {
    __CrestLoad(39655, (unsigned long )0, (long long )bufp->fastmap_accurate);
    __CrestLoad(39654, (unsigned long )0, (long long )0);
    __CrestApply2(39653, 12, (long long )(bufp->fastmap_accurate == 0));
# 3492 "regex.c"
    if (bufp->fastmap_accurate == 0) {
      __CrestBranch(39656, 10426, 1);
# 3493 "regex.c"
      tmp = re_compile_fastmap(bufp);
      __CrestHandleReturn(39659, (long long )tmp);
      __CrestStore(39658, (unsigned long )(& tmp));
      {
      __CrestLoad(39662, (unsigned long )(& tmp), (long long )tmp);
      __CrestLoad(39661, (unsigned long )0, (long long )-2);
      __CrestApply2(39660, 12, (long long )(tmp == -2));
# 3493 "regex.c"
      if (tmp == -2) {
        __CrestBranch(39663, 10428, 1);
        __CrestLoad(39665, (unsigned long )0, (long long )-2);
        __CrestStore(39666, (unsigned long )(& __retres34));
# 3494 "regex.c"
        __retres34 = -2;
# 3494 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(39664, 10430, 0);

      }
      }
    } else {
      __CrestBranch(39657, 10431, 0);

    }
    }
  } else {
    __CrestBranch(39652, 10432, 0);

  }
  }
  {
# 3497 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(39669, (unsigned long )(& fastmap), (long long )((unsigned long )fastmap));
    __CrestLoad(39668, (unsigned long )0, (long long )0);
    __CrestApply2(39667, 13, (long long )(fastmap != 0));
# 3503 "regex.c"
    if (fastmap != 0) {
      __CrestBranch(39670, 10437, 1);
      {
      __CrestLoad(39674, (unsigned long )(& startpos), (long long )startpos);
      __CrestLoad(39673, (unsigned long )(& total_size), (long long )total_size);
      __CrestApply2(39672, 16, (long long )(startpos < total_size));
# 3503 "regex.c"
      if (startpos < total_size) {
        __CrestBranch(39675, 10438, 1);
        {
        __CrestLoad(39679, (unsigned long )0, (long long )bufp->can_be_null);
        __CrestLoad(39678, (unsigned long )0, (long long )0);
        __CrestApply2(39677, 12, (long long )(bufp->can_be_null == 0));
# 3503 "regex.c"
        if (bufp->can_be_null == 0) {
          __CrestBranch(39680, 10439, 1);
          {
          __CrestLoad(39684, (unsigned long )(& range), (long long )range);
          __CrestLoad(39683, (unsigned long )0, (long long )0);
          __CrestApply2(39682, 14, (long long )(range > 0));
# 3505 "regex.c"
          if (range > 0) {
            __CrestBranch(39685, 10440, 1);
            __CrestLoad(39687, (unsigned long )0, (long long )0);
            __CrestStore(39688, (unsigned long )(& lim));
# 3508 "regex.c"
            lim = 0;
            __CrestLoad(39689, (unsigned long )(& range), (long long )range);
            __CrestStore(39690, (unsigned long )(& irange));
# 3509 "regex.c"
            irange = range;
            {
            __CrestLoad(39693, (unsigned long )(& startpos), (long long )startpos);
            __CrestLoad(39692, (unsigned long )(& size1), (long long )size1);
            __CrestApply2(39691, 16, (long long )(startpos < size1));
# 3511 "regex.c"
            if (startpos < size1) {
              __CrestBranch(39694, 10442, 1);
              {
              __CrestLoad(39700, (unsigned long )(& startpos), (long long )startpos);
              __CrestLoad(39699, (unsigned long )(& range), (long long )range);
              __CrestApply2(39698, 0, (long long )(startpos + range));
              __CrestLoad(39697, (unsigned long )(& size1), (long long )size1);
              __CrestApply2(39696, 17, (long long )(startpos + range >= size1));
# 3511 "regex.c"
              if (startpos + range >= size1) {
                __CrestBranch(39701, 10443, 1);
                __CrestLoad(39707, (unsigned long )(& range), (long long )range);
                __CrestLoad(39706, (unsigned long )(& size1), (long long )size1);
                __CrestLoad(39705, (unsigned long )(& startpos), (long long )startpos);
                __CrestApply2(39704, 1, (long long )(size1 - startpos));
                __CrestApply2(39703, 1, (long long )(range - (size1 - startpos)));
                __CrestStore(39708, (unsigned long )(& lim));
# 3512 "regex.c"
                lim = range - (size1 - startpos);
              } else {
                __CrestBranch(39702, 10444, 0);

              }
              }
            } else {
              __CrestBranch(39695, 10445, 0);

            }
            }
            {
            __CrestLoad(39711, (unsigned long )(& startpos), (long long )startpos);
            __CrestLoad(39710, (unsigned long )(& size1), (long long )size1);
            __CrestApply2(39709, 17, (long long )(startpos >= size1));
# 3514 "regex.c"
            if (startpos >= size1) {
              __CrestBranch(39712, 10447, 1);
# 3514 "regex.c"
              tmp___0 = string2 - size1;
            } else {
              __CrestBranch(39713, 10448, 0);
# 3514 "regex.c"
              tmp___0 = string1;
            }
            }
# 3514 "regex.c"
            d = tmp___0 + startpos;
            {
            __CrestLoad(39716, (unsigned long )(& translate), (long long )((unsigned long )translate));
            __CrestLoad(39715, (unsigned long )0, (long long )0);
            __CrestApply2(39714, 13, (long long )(translate != 0));
# 3518 "regex.c"
            if (translate != 0) {
              __CrestBranch(39717, 10451, 1);
              {
# 3519 "regex.c"
              while (1) {
                while_continue___0: ;
                {
                __CrestLoad(39721, (unsigned long )(& range), (long long )range);
                __CrestLoad(39720, (unsigned long )(& lim), (long long )lim);
                __CrestApply2(39719, 14, (long long )(range > lim));
# 3519 "regex.c"
                if (range > lim) {
                  __CrestBranch(39722, 10455, 1);
# 3519 "regex.c"
                  tmp___1 = d;
# 3519 "regex.c"
                  d ++;
                  {
# 3519 "regex.c"
                  mem_26 = translate + (unsigned char )*tmp___1;
# 3519 "regex.c"
                  mem_27 = fastmap + (unsigned char )*mem_26;
                  {
                  __CrestLoad(39726, (unsigned long )mem_27, (long long )*mem_27);
                  __CrestLoad(39725, (unsigned long )0, (long long )0);
                  __CrestApply2(39724, 13, (long long )(*mem_27 != 0));
# 3519 "regex.c"
                  if (*mem_27 != 0) {
                    __CrestBranch(39727, 10459, 1);
# 3519 "regex.c"
                    goto while_break___0;
                  } else {
                    __CrestBranch(39728, 10460, 0);

                  }
                  }
                  }
                } else {
                  __CrestBranch(39723, 10461, 0);
# 3519 "regex.c"
                  goto while_break___0;
                }
                }
                __CrestLoad(39731, (unsigned long )(& range), (long long )range);
                __CrestLoad(39730, (unsigned long )0, (long long )1);
                __CrestApply2(39729, 1, (long long )(range - 1));
                __CrestStore(39732, (unsigned long )(& range));
# 3522 "regex.c"
                range --;
              }
              while_break___0: ;
              }
            } else {
              __CrestBranch(39718, 10464, 0);
              {
# 3524 "regex.c"
              while (1) {
                while_continue___1: ;
                {
                __CrestLoad(39735, (unsigned long )(& range), (long long )range);
                __CrestLoad(39734, (unsigned long )(& lim), (long long )lim);
                __CrestApply2(39733, 14, (long long )(range > lim));
# 3524 "regex.c"
                if (range > lim) {
                  __CrestBranch(39736, 10468, 1);
# 3524 "regex.c"
                  tmp___2 = d;
# 3524 "regex.c"
                  d ++;
                  {
# 3524 "regex.c"
                  mem_28 = fastmap + (unsigned char )*tmp___2;
                  {
                  __CrestLoad(39740, (unsigned long )mem_28, (long long )*mem_28);
                  __CrestLoad(39739, (unsigned long )0, (long long )0);
                  __CrestApply2(39738, 13, (long long )(*mem_28 != 0));
# 3524 "regex.c"
                  if (*mem_28 != 0) {
                    __CrestBranch(39741, 10472, 1);
# 3524 "regex.c"
                    goto while_break___1;
                  } else {
                    __CrestBranch(39742, 10473, 0);

                  }
                  }
                  }
                } else {
                  __CrestBranch(39737, 10474, 0);
# 3524 "regex.c"
                  goto while_break___1;
                }
                }
                __CrestLoad(39745, (unsigned long )(& range), (long long )range);
                __CrestLoad(39744, (unsigned long )0, (long long )1);
                __CrestApply2(39743, 1, (long long )(range - 1));
                __CrestStore(39746, (unsigned long )(& range));
# 3525 "regex.c"
                range --;
              }
              while_break___1: ;
              }
            }
            }
            __CrestLoad(39751, (unsigned long )(& startpos), (long long )startpos);
            __CrestLoad(39750, (unsigned long )(& irange), (long long )irange);
            __CrestLoad(39749, (unsigned long )(& range), (long long )range);
            __CrestApply2(39748, 1, (long long )(irange - range));
            __CrestApply2(39747, 0, (long long )(startpos + (irange - range)));
            __CrestStore(39752, (unsigned long )(& startpos));
# 3527 "regex.c"
            startpos += irange - range;
          } else {
            __CrestBranch(39686, 10478, 0);
            {
            __CrestLoad(39755, (unsigned long )(& size1), (long long )size1);
            __CrestLoad(39754, (unsigned long )0, (long long )0);
            __CrestApply2(39753, 12, (long long )(size1 == 0));
# 3531 "regex.c"
            if (size1 == 0) {
              __CrestBranch(39756, 10479, 1);
# 3531 "regex.c"
              mem_29 = string2 + (startpos - size1);
              __CrestLoad(39758, (unsigned long )mem_29, (long long )*mem_29);
              __CrestStore(39759, (unsigned long )(& tmp___3));
# 3531 "regex.c"
              tmp___3 = (int const )*mem_29;
            } else {
              __CrestBranch(39757, 10480, 0);
              {
              __CrestLoad(39762, (unsigned long )(& startpos), (long long )startpos);
              __CrestLoad(39761, (unsigned long )(& size1), (long long )size1);
              __CrestApply2(39760, 17, (long long )(startpos >= size1));
# 3531 "regex.c"
              if (startpos >= size1) {
                __CrestBranch(39763, 10481, 1);
# 3531 "regex.c"
                mem_30 = string2 + (startpos - size1);
                __CrestLoad(39765, (unsigned long )mem_30, (long long )*mem_30);
                __CrestStore(39766, (unsigned long )(& tmp___3));
# 3531 "regex.c"
                tmp___3 = (int const )*mem_30;
              } else {
                __CrestBranch(39764, 10482, 0);
# 3531 "regex.c"
                mem_31 = string1 + startpos;
                __CrestLoad(39767, (unsigned long )mem_31, (long long )*mem_31);
                __CrestStore(39768, (unsigned long )(& tmp___3));
# 3531 "regex.c"
                tmp___3 = (int const )*mem_31;
              }
              }
            }
            }
            __CrestLoad(39769, (unsigned long )(& tmp___3), (long long )tmp___3);
            __CrestStore(39770, (unsigned long )(& c));
# 3531 "regex.c"
            c = (char )tmp___3;
            {
            __CrestLoad(39773, (unsigned long )(& translate), (long long )((unsigned long )translate));
            __CrestLoad(39772, (unsigned long )0, (long long )0);
            __CrestApply2(39771, 13, (long long )(translate != 0));
# 3535 "regex.c"
            if (translate != 0) {
              __CrestBranch(39774, 10485, 1);
# 3535 "regex.c"
              mem_32 = translate + (unsigned char )c;
              __CrestLoad(39776, (unsigned long )mem_32, (long long )*mem_32);
              __CrestStore(39777, (unsigned long )(& tmp___4));
# 3535 "regex.c"
              tmp___4 = (int )*mem_32;
            } else {
              __CrestBranch(39775, 10486, 0);
              __CrestLoad(39778, (unsigned long )(& c), (long long )c);
              __CrestStore(39779, (unsigned long )(& tmp___4));
# 3535 "regex.c"
              tmp___4 = (int )c;
            }
            }
            {
# 3535 "regex.c"
            mem_33 = fastmap + (unsigned char )tmp___4;
            {
            __CrestLoad(39782, (unsigned long )mem_33, (long long )*mem_33);
            __CrestLoad(39781, (unsigned long )0, (long long )0);
            __CrestApply2(39780, 13, (long long )(*mem_33 != 0));
# 3535 "regex.c"
            if (*mem_33 != 0) {
              __CrestBranch(39783, 10490, 1);

            } else {
              __CrestBranch(39784, 10491, 0);
# 3536 "regex.c"
              goto advance;
            }
            }
            }
          }
          }
        } else {
          __CrestBranch(39681, 10492, 0);

        }
        }
      } else {
        __CrestBranch(39676, 10493, 0);

      }
      }
    } else {
      __CrestBranch(39671, 10494, 0);

    }
    }
    {
    __CrestLoad(39787, (unsigned long )(& range), (long long )range);
    __CrestLoad(39786, (unsigned long )0, (long long )0);
    __CrestApply2(39785, 17, (long long )(range >= 0));
# 3541 "regex.c"
    if (range >= 0) {
      __CrestBranch(39788, 10496, 1);
      {
      __CrestLoad(39792, (unsigned long )(& startpos), (long long )startpos);
      __CrestLoad(39791, (unsigned long )(& total_size), (long long )total_size);
      __CrestApply2(39790, 12, (long long )(startpos == total_size));
# 3541 "regex.c"
      if (startpos == total_size) {
        __CrestBranch(39793, 10497, 1);
        {
        __CrestLoad(39797, (unsigned long )(& fastmap), (long long )((unsigned long )fastmap));
        __CrestLoad(39796, (unsigned long )0, (long long )0);
        __CrestApply2(39795, 13, (long long )(fastmap != 0));
# 3541 "regex.c"
        if (fastmap != 0) {
          __CrestBranch(39798, 10498, 1);
          {
          __CrestLoad(39802, (unsigned long )0, (long long )bufp->can_be_null);
          __CrestLoad(39801, (unsigned long )0, (long long )0);
          __CrestApply2(39800, 12, (long long )(bufp->can_be_null == 0));
# 3541 "regex.c"
          if (bufp->can_be_null == 0) {
            __CrestBranch(39803, 10499, 1);
            __CrestLoad(39805, (unsigned long )0, (long long )-1);
            __CrestStore(39806, (unsigned long )(& __retres34));
# 3543 "regex.c"
            __retres34 = -1;
# 3543 "regex.c"
            goto return_label;
          } else {
            __CrestBranch(39804, 10501, 0);

          }
          }
        } else {
          __CrestBranch(39799, 10502, 0);

        }
        }
      } else {
        __CrestBranch(39794, 10503, 0);

      }
      }
    } else {
      __CrestBranch(39789, 10504, 0);

    }
    }
    __CrestLoad(39807, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(39808, (unsigned long )(& size2), (long long )size2);
    __CrestLoad(39809, (unsigned long )(& startpos), (long long )startpos);
    __CrestLoad(39810, (unsigned long )(& stop), (long long )stop);
# 3545 "regex.c"
    val = re_match_2_internal(bufp, string1, size1, string2, size2, startpos, regs,
                              stop);
    __CrestHandleReturn(39812, (long long )val);
    __CrestStore(39811, (unsigned long )(& val));
    {
    __CrestLoad(39815, (unsigned long )(& val), (long long )val);
    __CrestLoad(39814, (unsigned long )0, (long long )0);
    __CrestApply2(39813, 17, (long long )(val >= 0));
# 3553 "regex.c"
    if (val >= 0) {
      __CrestBranch(39816, 10507, 1);
      __CrestLoad(39818, (unsigned long )(& startpos), (long long )startpos);
      __CrestStore(39819, (unsigned long )(& __retres34));
# 3554 "regex.c"
      __retres34 = startpos;
# 3554 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(39817, 10509, 0);

    }
    }
    {
    __CrestLoad(39822, (unsigned long )(& val), (long long )val);
    __CrestLoad(39821, (unsigned long )0, (long long )-2);
    __CrestApply2(39820, 12, (long long )(val == -2));
# 3556 "regex.c"
    if (val == -2) {
      __CrestBranch(39823, 10511, 1);
      __CrestLoad(39825, (unsigned long )0, (long long )-2);
      __CrestStore(39826, (unsigned long )(& __retres34));
# 3557 "regex.c"
      __retres34 = -2;
# 3557 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(39824, 10513, 0);

    }
    }
    advance:
    {
    __CrestLoad(39829, (unsigned long )(& range), (long long )range);
    __CrestLoad(39828, (unsigned long )0, (long long )0);
    __CrestApply2(39827, 12, (long long )(range == 0));
# 3560 "regex.c"
    if (range == 0) {
      __CrestBranch(39830, 10515, 1);
# 3561 "regex.c"
      goto while_break;
    } else {
      __CrestBranch(39831, 10516, 0);
      {
      __CrestLoad(39834, (unsigned long )(& range), (long long )range);
      __CrestLoad(39833, (unsigned long )0, (long long )0);
      __CrestApply2(39832, 14, (long long )(range > 0));
# 3562 "regex.c"
      if (range > 0) {
        __CrestBranch(39835, 10517, 1);
        __CrestLoad(39839, (unsigned long )(& range), (long long )range);
        __CrestLoad(39838, (unsigned long )0, (long long )1);
        __CrestApply2(39837, 1, (long long )(range - 1));
        __CrestStore(39840, (unsigned long )(& range));
# 3564 "regex.c"
        range --;
        __CrestLoad(39843, (unsigned long )(& startpos), (long long )startpos);
        __CrestLoad(39842, (unsigned long )0, (long long )1);
        __CrestApply2(39841, 0, (long long )(startpos + 1));
        __CrestStore(39844, (unsigned long )(& startpos));
# 3565 "regex.c"
        startpos ++;
      } else {
        __CrestBranch(39836, 10518, 0);
        __CrestLoad(39847, (unsigned long )(& range), (long long )range);
        __CrestLoad(39846, (unsigned long )0, (long long )1);
        __CrestApply2(39845, 0, (long long )(range + 1));
        __CrestStore(39848, (unsigned long )(& range));
# 3569 "regex.c"
        range ++;
        __CrestLoad(39851, (unsigned long )(& startpos), (long long )startpos);
        __CrestLoad(39850, (unsigned long )0, (long long )1);
        __CrestApply2(39849, 1, (long long )(startpos - 1));
        __CrestStore(39852, (unsigned long )(& startpos));
# 3570 "regex.c"
        startpos --;
      }
      }
    }
    }
  }
  while_break: ;
  }
  __CrestLoad(39853, (unsigned long )0, (long long )-1);
  __CrestStore(39854, (unsigned long )(& __retres34));
# 3573 "regex.c"
  __retres34 = -1;
  return_label:
  {
  __CrestLoad(39855, (unsigned long )(& __retres34), (long long )__retres34);
  __CrestReturn(39856);
# 3442 "regex.c"
  return (__retres34);
  }
}
}
# 3660 "regex.c"
int re_match(struct re_pattern_buffer *bufp , char const *string , int size , int pos ,
             struct re_registers *regs )
{
  int result ;
  int tmp ;

  {
  __CrestCall(39859, 306);
  __CrestStore(39858, (unsigned long )(& pos));
  __CrestStore(39857, (unsigned long )(& size));
  __CrestLoad(39860, (unsigned long )0, (long long )0);
  __CrestLoad(39861, (unsigned long )(& size), (long long )size);
  __CrestLoad(39862, (unsigned long )(& pos), (long long )pos);
  __CrestLoad(39863, (unsigned long )(& size), (long long )size);
# 3667 "regex.c"
  tmp = re_match_2_internal(bufp, (void *)0, 0, string, size, pos, regs, size);
  __CrestHandleReturn(39865, (long long )tmp);
  __CrestStore(39864, (unsigned long )(& tmp));
  __CrestLoad(39866, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(39867, (unsigned long )(& result));
# 3667 "regex.c"
  result = tmp;
  {
  __CrestLoad(39868, (unsigned long )(& result), (long long )result);
  __CrestReturn(39869);
# 3674 "regex.c"
  return (result);
  }
}
}
# 3678 "regex.c"
static boolean group_match_null_string_p(unsigned char **p , unsigned char *end ,
                                         register_info_type *reg_info ) ;
# 3681 "regex.c"
static boolean alt_match_null_string_p(unsigned char *p , unsigned char *end , register_info_type *reg_info ) ;
# 3684 "regex.c"
static boolean common_op_match_null_string_p(unsigned char **p , unsigned char *end ,
                                             register_info_type *reg_info ) ;
# 3687 "regex.c"
static int bcmp_translate(char const *s1 , char const *s2 , int len , char *translate ) ;
# 3703 "regex.c"
int re_match_2(struct re_pattern_buffer *bufp , char const *string1 , int size1 ,
               char const *string2 , int size2 , int pos , struct re_registers *regs ,
               int stop )
{
  int result ;
  int tmp ;

  {
  __CrestCall(39874, 307);
  __CrestStore(39873, (unsigned long )(& stop));
  __CrestStore(39872, (unsigned long )(& pos));
  __CrestStore(39871, (unsigned long )(& size2));
  __CrestStore(39870, (unsigned long )(& size1));
  __CrestLoad(39875, (unsigned long )(& size1), (long long )size1);
  __CrestLoad(39876, (unsigned long )(& size2), (long long )size2);
  __CrestLoad(39877, (unsigned long )(& pos), (long long )pos);
  __CrestLoad(39878, (unsigned long )(& stop), (long long )stop);
# 3712 "regex.c"
  tmp = re_match_2_internal(bufp, string1, size1, string2, size2, pos, regs, stop);
  __CrestHandleReturn(39880, (long long )tmp);
  __CrestStore(39879, (unsigned long )(& tmp));
  __CrestLoad(39881, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(39882, (unsigned long )(& result));
# 3712 "regex.c"
  result = tmp;
  {
  __CrestLoad(39883, (unsigned long )(& result), (long long )result);
  __CrestReturn(39884);
# 3719 "regex.c"
  return (result);
  }
}
}
# 3724 "regex.c"
static int re_match_2_internal(struct re_pattern_buffer *bufp , char const *string1 ,
                               int size1 , char const *string2 , int size2 , int pos ,
                               struct re_registers *regs , int stop )
{
  int mcnt ;
  unsigned char *p1 ;
  char const *end1 ;
  char const *end2 ;
  char const *end_match_1 ;
  char const *end_match_2 ;
  char const *d ;
  char const *dend ;
  unsigned char *p ;
  unsigned char *pend ;
  unsigned char *just_past_start_mem ;
  char *translate ;
  fail_stack_type fail_stack ;
  size_t num_regs ;
  active_reg_t lowest_active_reg ;
  active_reg_t highest_active_reg ;
  char const **regstart ;
  char const **regend ;
  char const **old_regstart ;
  char const **old_regend ;
  register_info_type *reg_info ;
  unsigned int best_regs_set ;
  char const **best_regstart ;
  char const **best_regend ;
  char const *match_end ;
  int set_regs_matched_done ;
  char const **reg_dummy ;
  register_info_type *reg_info_dummy ;
  void *tmp ;
  void *tmp___0 ;
  void *tmp___1 ;
  void *tmp___2 ;
  void *tmp___3 ;
  void *tmp___4 ;
  void *tmp___5 ;
  void *tmp___6 ;
  void *tmp___7 ;
  void *tmp___8 ;
  char const *tmp___9 ;
  char const *tmp___10 ;
  char const *tmp___11 ;
  boolean same_str_p ;
  int tmp___12 ;
  boolean best_match_p ;
  void *tmp___13 ;
  void *tmp___14 ;
  void *tmp___15 ;
  void *tmp___16 ;
  regoff_t tmp___17 ;
  regoff_t tmp___18 ;
  regoff_t tmp___19 ;
  size_t tmp___20 ;
  regoff_t tmp___21 ;
  char const *tmp___22 ;
  unsigned char *tmp___23 ;
  unsigned char *tmp___24 ;
  char const *tmp___25 ;
  unsigned char *tmp___26 ;
  char const *tmp___27 ;
  unsigned char *tmp___28 ;
  active_reg_t r ;
  unsigned int tmp___29 ;
  int tmp___30 ;
  int tmp___31 ;
  active_reg_t r___0 ;
  unsigned int tmp___32 ;
  unsigned char c ;
  boolean not ;
  active_reg_t r___1 ;
  unsigned int tmp___33 ;
  boolean tmp___34 ;
  char const *tmp___35 ;
  char const *tmp___36 ;
  unsigned char r___2 ;
  boolean is_a_jump_n ;
  unsigned char *tmp___37 ;
  unsigned int r___3 ;
  char *destination ;
  s_reg_t this_reg ;
  void *tmp___38 ;
  int tmp___39 ;
  int tmp___40 ;
  unsigned int tmp___41 ;
  unsigned int tmp___42 ;
  unsigned int tmp___43 ;
  unsigned int tmp___44 ;
  unsigned int tmp___45 ;
  unsigned int tmp___46 ;
  unsigned int tmp___47 ;
  char const *d2 ;
  char const *dend2 ;
  int regno ;
  unsigned char *tmp___48 ;
  int tmp___51 ;
  int tmp___52 ;
  int tmp___53 ;
  int tmp___54 ;
  int tmp___55 ;
  active_reg_t r___4 ;
  unsigned int tmp___56 ;
  char const *tmp___57 ;
  int tmp___58 ;
  char const *tmp___59 ;
  char *destination___0 ;
  s_reg_t this_reg___0 ;
  void *tmp___60 ;
  int tmp___61 ;
  int tmp___62 ;
  unsigned int tmp___63 ;
  unsigned int tmp___64 ;
  unsigned int tmp___65 ;
  unsigned int tmp___66 ;
  unsigned int tmp___67 ;
  unsigned int tmp___68 ;
  unsigned int tmp___69 ;
  char *destination___1 ;
  s_reg_t this_reg___1 ;
  void *tmp___70 ;
  int tmp___71 ;
  int tmp___72 ;
  unsigned int tmp___73 ;
  unsigned int tmp___74 ;
  unsigned int tmp___75 ;
  unsigned int tmp___76 ;
  unsigned int tmp___77 ;
  unsigned int tmp___78 ;
  unsigned int tmp___79 ;
  unsigned char *p2 ;
  unsigned char c___0 ;
  int tmp___80 ;
  int not___0 ;
  int idx ;
  int idx___0 ;
  active_reg_t dummy_low_reg ;
  active_reg_t dummy_high_reg ;
  unsigned char *pdummy ;
  char const *sdummy ;
  s_reg_t this_reg___2 ;
  unsigned char const *string_temp ;
  char *destination___2 ;
  s_reg_t this_reg___3 ;
  void *tmp___81 ;
  int tmp___82 ;
  int tmp___83 ;
  unsigned int tmp___84 ;
  unsigned int tmp___85 ;
  unsigned int tmp___86 ;
  unsigned int tmp___87 ;
  unsigned int tmp___88 ;
  unsigned int tmp___89 ;
  unsigned int tmp___90 ;
  char *destination___3 ;
  s_reg_t this_reg___4 ;
  void *tmp___91 ;
  int tmp___92 ;
  int tmp___93 ;
  unsigned int tmp___94 ;
  unsigned int tmp___95 ;
  unsigned int tmp___96 ;
  unsigned int tmp___97 ;
  unsigned int tmp___98 ;
  unsigned int tmp___99 ;
  unsigned int tmp___100 ;
  boolean prevchar ;
  boolean thischar ;
  char const *tmp___101 ;
  int tmp___102 ;
  int tmp___103 ;
  int tmp___104 ;
  int tmp___105 ;
  boolean prevchar___0 ;
  boolean thischar___0 ;
  char const *tmp___106 ;
  int tmp___107 ;
  int tmp___108 ;
  int tmp___109 ;
  int tmp___110 ;
  int tmp___111 ;
  int tmp___112 ;
  char const *tmp___113 ;
  int tmp___114 ;
  int tmp___115 ;
  char const *tmp___116 ;
  int tmp___117 ;
  int tmp___118 ;
  int tmp___119 ;
  int tmp___120 ;
  int tmp___121 ;
  int tmp___122 ;
  active_reg_t r___5 ;
  unsigned int tmp___123 ;
  int tmp___124 ;
  int tmp___125 ;
  active_reg_t r___6 ;
  unsigned int tmp___126 ;
  s_reg_t this_reg___5 ;
  unsigned char const *string_temp___0 ;
  boolean is_a_jump_n___0 ;
  char const **mem_209 ;
  char const **mem_210 ;
  char const **mem_211 ;
  char const **mem_212 ;
  register_info_type *mem_213 ;
  register_info_type *mem_214 ;
  register_info_type *mem_215 ;
  register_info_type *mem_216 ;
  char const **mem_217 ;
  char const **mem_218 ;
  char const **mem_219 ;
  char const **mem_220 ;
  char const **mem_221 ;
  char const **mem_222 ;
  char const **mem_223 ;
  char const **mem_224 ;
  regoff_t *mem_225 ;
  regoff_t *mem_226 ;
  regoff_t *mem_227 ;
  char const **mem_228 ;
  regoff_t *mem_229 ;
  regoff_t *mem_230 ;
  char const **mem_231 ;
  regoff_t *mem_232 ;
  regoff_t *mem_233 ;
  char const **mem_234 ;
  char const **mem_235 ;
  char const **mem_236 ;
  char const **mem_237 ;
  char const **mem_238 ;
  char const **mem_239 ;
  regoff_t *mem_240 ;
  char const **mem_241 ;
  char const **mem_242 ;
  char const **mem_243 ;
  char const **mem_244 ;
  char const **mem_245 ;
  char const **mem_246 ;
  regoff_t *mem_247 ;
  regoff_t *mem_248 ;
  regoff_t *mem_249 ;
  char *mem_250 ;
  register_info_type *mem_251 ;
  register_info_type *mem_252 ;
  char *mem_253 ;
  char *mem_254 ;
  register_info_type *mem_255 ;
  register_info_type *mem_256 ;
  unsigned char *mem_257 ;
  char *mem_258 ;
  unsigned char *mem_259 ;
  register_info_type *mem_260 ;
  register_info_type *mem_261 ;
  register_info_type *mem_262 ;
  register_info_type *mem_263 ;
  register_info_type *mem_264 ;
  char const **mem_265 ;
  char const **mem_266 ;
  char const **mem_267 ;
  char const **mem_268 ;
  char const **mem_269 ;
  char const **mem_270 ;
  register_info_type *mem_271 ;
  register_info_type *mem_272 ;
  register_info_type *mem_273 ;
  char const **mem_274 ;
  char const **mem_275 ;
  char const **mem_276 ;
  char const **mem_277 ;
  char const **mem_278 ;
  char const **mem_279 ;
  register_info_type *mem_280 ;
  register_info_type *mem_281 ;
  register_info_type *mem_282 ;
  unsigned char *mem_283 ;
  unsigned char *mem_284 ;
  unsigned char *mem_285 ;
  register_info_type *mem_286 ;
  register_info_type *mem_287 ;
  unsigned char *mem_288 ;
  char const **mem_289 ;
  char const **mem_290 ;
  char const **mem_291 ;
  char const **mem_292 ;
  char const **mem_293 ;
  char const **mem_294 ;
  unsigned char *mem_295 ;
  fail_stack_elt_t *mem_296 ;
  char const **mem_297 ;
  fail_stack_elt_t *mem_298 ;
  char const **mem_299 ;
  fail_stack_elt_t *mem_300 ;
  register_info_type *mem_301 ;
  fail_stack_elt_t *mem_302 ;
  fail_stack_elt_t *mem_303 ;
  fail_stack_elt_t *mem_304 ;
  fail_stack_elt_t *mem_305 ;
  char const **mem_306 ;
  char const **mem_307 ;
  char const **mem_308 ;
  char const **mem_309 ;
  char const **mem_310 ;
  char const **mem_311 ;
  char const **mem_312 ;
  char const **mem_313 ;
  char const **mem_314 ;
  char const **mem_315 ;
  register_info_type *mem_316 ;
  register_info_type *mem_317 ;
  char const *mem_318 ;
  unsigned char *mem_319 ;
  fail_stack_elt_t *mem_320 ;
  char const **mem_321 ;
  fail_stack_elt_t *mem_322 ;
  char const **mem_323 ;
  fail_stack_elt_t *mem_324 ;
  register_info_type *mem_325 ;
  fail_stack_elt_t *mem_326 ;
  fail_stack_elt_t *mem_327 ;
  fail_stack_elt_t *mem_328 ;
  fail_stack_elt_t *mem_329 ;
  unsigned char *mem_330 ;
  unsigned char *mem_331 ;
  unsigned char *mem_332 ;
  unsigned char *mem_333 ;
  fail_stack_elt_t *mem_334 ;
  char const **mem_335 ;
  fail_stack_elt_t *mem_336 ;
  char const **mem_337 ;
  fail_stack_elt_t *mem_338 ;
  register_info_type *mem_339 ;
  fail_stack_elt_t *mem_340 ;
  fail_stack_elt_t *mem_341 ;
  fail_stack_elt_t *mem_342 ;
  fail_stack_elt_t *mem_343 ;
  unsigned char *mem_344 ;
  unsigned char *mem_345 ;
  unsigned char *mem_346 ;
  unsigned char *mem_347 ;
  unsigned char *mem_348 ;
  unsigned char *mem_349 ;
  unsigned char *mem_350 ;
  unsigned char *mem_351 ;
  unsigned char *mem_352 ;
  unsigned char *mem_353 ;
  unsigned char *mem_354 ;
  unsigned char *mem_355 ;
  unsigned char *mem_356 ;
  unsigned char *mem_357 ;
  unsigned char *mem_358 ;
  unsigned char *mem_359 ;
  unsigned char *mem_360 ;
  unsigned char *mem_361 ;
  unsigned char *mem_362 ;
  unsigned char *mem_363 ;
  unsigned char *mem_364 ;
  unsigned char *mem_365 ;
  unsigned char *mem_366 ;
  unsigned char *mem_367 ;
  unsigned char *mem_368 ;
  unsigned char *mem_369 ;
  unsigned char *mem_370 ;
  unsigned char *mem_371 ;
  unsigned char *mem_372 ;
  unsigned char *mem_373 ;
  unsigned char *mem_374 ;
  unsigned char *mem_375 ;
  unsigned char *mem_376 ;
  unsigned char *mem_377 ;
  unsigned char *mem_378 ;
  unsigned char *mem_379 ;
  unsigned char *mem_380 ;
  unsigned char *mem_381 ;
  unsigned char *mem_382 ;
  fail_stack_elt_t *mem_383 ;
  fail_stack_elt_t *mem_384 ;
  fail_stack_elt_t *mem_385 ;
  fail_stack_elt_t *mem_386 ;
  register_info_type *mem_387 ;
  fail_stack_elt_t *mem_388 ;
  char const **mem_389 ;
  fail_stack_elt_t *mem_390 ;
  char const **mem_391 ;
  fail_stack_elt_t *mem_392 ;
  unsigned char *mem_393 ;
  fail_stack_elt_t *mem_394 ;
  char const **mem_395 ;
  fail_stack_elt_t *mem_396 ;
  char const **mem_397 ;
  fail_stack_elt_t *mem_398 ;
  register_info_type *mem_399 ;
  fail_stack_elt_t *mem_400 ;
  fail_stack_elt_t *mem_401 ;
  fail_stack_elt_t *mem_402 ;
  fail_stack_elt_t *mem_403 ;
  fail_stack_elt_t *mem_404 ;
  char const **mem_405 ;
  fail_stack_elt_t *mem_406 ;
  char const **mem_407 ;
  fail_stack_elt_t *mem_408 ;
  register_info_type *mem_409 ;
  fail_stack_elt_t *mem_410 ;
  fail_stack_elt_t *mem_411 ;
  fail_stack_elt_t *mem_412 ;
  fail_stack_elt_t *mem_413 ;
  unsigned char *mem_414 ;
  unsigned char *mem_415 ;
  unsigned char *mem_416 ;
  unsigned char *mem_417 ;
  unsigned char *mem_418 ;
  unsigned char *mem_419 ;
  unsigned char *mem_420 ;
  unsigned char *mem_421 ;
  unsigned char *mem_422 ;
  unsigned char *mem_423 ;
  unsigned char *mem_424 ;
  unsigned char *mem_425 ;
  unsigned char *mem_426 ;
  unsigned char *mem_427 ;
  char const *mem_428 ;
  char const *mem_429 ;
  char const *mem_430 ;
  char const *mem_431 ;
  char const *mem_432 ;
  char const *mem_433 ;
  char const *mem_434 ;
  char const *mem_435 ;
  char const *mem_436 ;
  char const *mem_437 ;
  char const *mem_438 ;
  char const *mem_439 ;
  char const *mem_440 ;
  register_info_type *mem_441 ;
  register_info_type *mem_442 ;
  char const *mem_443 ;
  register_info_type *mem_444 ;
  register_info_type *mem_445 ;
  fail_stack_elt_t *mem_446 ;
  fail_stack_elt_t *mem_447 ;
  fail_stack_elt_t *mem_448 ;
  fail_stack_elt_t *mem_449 ;
  register_info_type *mem_450 ;
  fail_stack_elt_t *mem_451 ;
  char const **mem_452 ;
  fail_stack_elt_t *mem_453 ;
  char const **mem_454 ;
  fail_stack_elt_t *mem_455 ;
  unsigned char *mem_456 ;
  int __retres457 ;

  {
  __CrestCall(39889, 308);
  __CrestStore(39888, (unsigned long )(& stop));
  __CrestStore(39887, (unsigned long )(& pos));
  __CrestStore(39886, (unsigned long )(& size2));
  __CrestStore(39885, (unsigned long )(& size1));
# 3748 "regex.c"
  p = bufp->buffer;
# 3749 "regex.c"
  pend = p + bufp->used;
# 3753 "regex.c"
  just_past_start_mem = (unsigned char *)0;
# 3756 "regex.c"
  translate = bufp->translate;
  __CrestLoad(39892, (unsigned long )(& bufp->re_nsub), (long long )bufp->re_nsub);
  __CrestLoad(39891, (unsigned long )0, (long long )1UL);
  __CrestApply2(39890, 0, (long long )(bufp->re_nsub + 1UL));
  __CrestStore(39893, (unsigned long )(& num_regs));
# 3784 "regex.c"
  num_regs = bufp->re_nsub + 1UL;
  __CrestLoad(39894, (unsigned long )0, (long long )((active_reg_t )((1 << 8) + 1)));
  __CrestStore(39895, (unsigned long )(& lowest_active_reg));
# 3787 "regex.c"
  lowest_active_reg = (active_reg_t )((1 << 8) + 1);
  __CrestLoad(39896, (unsigned long )0, (long long )((active_reg_t )(1 << 8)));
  __CrestStore(39897, (unsigned long )(& highest_active_reg));
# 3788 "regex.c"
  highest_active_reg = (active_reg_t )(1 << 8);
  __CrestLoad(39898, (unsigned long )0, (long long )0U);
  __CrestStore(39899, (unsigned long )(& best_regs_set));
# 3824 "regex.c"
  best_regs_set = 0U;
# 3837 "regex.c"
  match_end = (char const *)((void *)0);
  __CrestLoad(39900, (unsigned long )0, (long long )0);
  __CrestStore(39901, (unsigned long )(& set_regs_matched_done));
# 3840 "regex.c"
  set_regs_matched_done = 0;
  {
# 3855 "regex.c"
  while (1) {
    while_continue: ;
    __CrestLoad(39902, (unsigned long )0, (long long )(5UL * sizeof(fail_stack_elt_t )));
# 3855 "regex.c"
    tmp = __builtin_alloca(5UL * sizeof(fail_stack_elt_t ));
    __CrestClearStack(39903);
# 3855 "regex.c"
    fail_stack.stack = (fail_stack_elt_t *)tmp;
    {
    __CrestLoad(39906, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
    __CrestLoad(39905, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(39904, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 3855 "regex.c"
    if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
      __CrestBranch(39907, 10532, 1);
      __CrestLoad(39909, (unsigned long )0, (long long )-2);
      __CrestStore(39910, (unsigned long )(& __retres457));
# 3855 "regex.c"
      __retres457 = -2;
# 3855 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(39908, 10534, 0);

    }
    }
    __CrestLoad(39911, (unsigned long )0, (long long )5U);
    __CrestStore(39912, (unsigned long )(& fail_stack.size));
# 3855 "regex.c"
    fail_stack.size = 5U;
    __CrestLoad(39913, (unsigned long )0, (long long )0U);
    __CrestStore(39914, (unsigned long )(& fail_stack.avail));
# 3855 "regex.c"
    fail_stack.avail = 0U;
# 3855 "regex.c"
    goto while_break;
  }
  while_break: ;
  }
  {
  __CrestLoad(39917, (unsigned long )(& bufp->re_nsub), (long long )bufp->re_nsub);
  __CrestLoad(39916, (unsigned long )0, (long long )0);
  __CrestApply2(39915, 13, (long long )(bufp->re_nsub != 0));
# 3863 "regex.c"
  if (bufp->re_nsub != 0) {
    __CrestBranch(39918, 10539, 1);
    __CrestLoad(39922, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39921, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39920, 2, (long long )(num_regs * sizeof(char const *)));
# 3865 "regex.c"
    tmp___0 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39923);
# 3865 "regex.c"
    regstart = (char const **)tmp___0;
    __CrestLoad(39926, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39925, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39924, 2, (long long )(num_regs * sizeof(char const *)));
# 3866 "regex.c"
    tmp___1 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39927);
# 3866 "regex.c"
    regend = (char const **)tmp___1;
    __CrestLoad(39930, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39929, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39928, 2, (long long )(num_regs * sizeof(char const *)));
# 3867 "regex.c"
    tmp___2 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39931);
# 3867 "regex.c"
    old_regstart = (char const **)tmp___2;
    __CrestLoad(39934, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39933, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39932, 2, (long long )(num_regs * sizeof(char const *)));
# 3868 "regex.c"
    tmp___3 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39935);
# 3868 "regex.c"
    old_regend = (char const **)tmp___3;
    __CrestLoad(39938, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39937, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39936, 2, (long long )(num_regs * sizeof(char const *)));
# 3869 "regex.c"
    tmp___4 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39939);
# 3869 "regex.c"
    best_regstart = (char const **)tmp___4;
    __CrestLoad(39942, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39941, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39940, 2, (long long )(num_regs * sizeof(char const *)));
# 3870 "regex.c"
    tmp___5 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39943);
# 3870 "regex.c"
    best_regend = (char const **)tmp___5;
    __CrestLoad(39946, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39945, (unsigned long )0, (long long )sizeof(register_info_type ));
    __CrestApply2(39944, 2, (long long )(num_regs * sizeof(register_info_type )));
# 3871 "regex.c"
    tmp___6 = __builtin_alloca(num_regs * sizeof(register_info_type ));
    __CrestClearStack(39947);
# 3871 "regex.c"
    reg_info = (register_info_type *)tmp___6;
    __CrestLoad(39950, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39949, (unsigned long )0, (long long )sizeof(char const *));
    __CrestApply2(39948, 2, (long long )(num_regs * sizeof(char const *)));
# 3872 "regex.c"
    tmp___7 = __builtin_alloca(num_regs * sizeof(char const *));
    __CrestClearStack(39951);
# 3872 "regex.c"
    reg_dummy = (char const **)tmp___7;
    __CrestLoad(39954, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestLoad(39953, (unsigned long )0, (long long )sizeof(register_info_type ));
    __CrestApply2(39952, 2, (long long )(num_regs * sizeof(register_info_type )));
# 3873 "regex.c"
    tmp___8 = __builtin_alloca(num_regs * sizeof(register_info_type ));
    __CrestClearStack(39955);
# 3873 "regex.c"
    reg_info_dummy = (register_info_type *)tmp___8;
    {
    __CrestLoad(39958, (unsigned long )(& regstart), (long long )((unsigned long )regstart));
    __CrestLoad(39957, (unsigned long )0, (long long )0);
    __CrestApply2(39956, 13, (long long )(regstart != 0));
# 3875 "regex.c"
    if (regstart != 0) {
      __CrestBranch(39959, 10541, 1);
      {
      __CrestLoad(39963, (unsigned long )(& regend), (long long )((unsigned long )regend));
      __CrestLoad(39962, (unsigned long )0, (long long )0);
      __CrestApply2(39961, 13, (long long )(regend != 0));
# 3875 "regex.c"
      if (regend != 0) {
        __CrestBranch(39964, 10542, 1);
        {
        __CrestLoad(39968, (unsigned long )(& old_regstart), (long long )((unsigned long )old_regstart));
        __CrestLoad(39967, (unsigned long )0, (long long )0);
        __CrestApply2(39966, 13, (long long )(old_regstart != 0));
# 3875 "regex.c"
        if (old_regstart != 0) {
          __CrestBranch(39969, 10543, 1);
          {
          __CrestLoad(39973, (unsigned long )(& old_regend), (long long )((unsigned long )old_regend));
          __CrestLoad(39972, (unsigned long )0, (long long )0);
          __CrestApply2(39971, 13, (long long )(old_regend != 0));
# 3875 "regex.c"
          if (old_regend != 0) {
            __CrestBranch(39974, 10544, 1);
            {
            __CrestLoad(39978, (unsigned long )(& reg_info), (long long )((unsigned long )reg_info));
            __CrestLoad(39977, (unsigned long )0, (long long )0);
            __CrestApply2(39976, 13, (long long )(reg_info != 0));
# 3875 "regex.c"
            if (reg_info != 0) {
              __CrestBranch(39979, 10545, 1);
              {
              __CrestLoad(39983, (unsigned long )(& best_regstart), (long long )((unsigned long )best_regstart));
              __CrestLoad(39982, (unsigned long )0, (long long )0);
              __CrestApply2(39981, 13, (long long )(best_regstart != 0));
# 3875 "regex.c"
              if (best_regstart != 0) {
                __CrestBranch(39984, 10546, 1);
                {
                __CrestLoad(39988, (unsigned long )(& best_regend), (long long )((unsigned long )best_regend));
                __CrestLoad(39987, (unsigned long )0, (long long )0);
                __CrestApply2(39986, 13, (long long )(best_regend != 0));
# 3875 "regex.c"
                if (best_regend != 0) {
                  __CrestBranch(39989, 10547, 1);
                  {
                  __CrestLoad(39993, (unsigned long )(& reg_dummy), (long long )((unsigned long )reg_dummy));
                  __CrestLoad(39992, (unsigned long )0, (long long )0);
                  __CrestApply2(39991, 13, (long long )(reg_dummy != 0));
# 3875 "regex.c"
                  if (reg_dummy != 0) {
                    __CrestBranch(39994, 10548, 1);
                    {
                    __CrestLoad(39998, (unsigned long )(& reg_info_dummy), (long long )((unsigned long )reg_info_dummy));
                    __CrestLoad(39997, (unsigned long )0, (long long )0);
                    __CrestApply2(39996, 13, (long long )(reg_info_dummy != 0));
# 3875 "regex.c"
                    if (reg_info_dummy != 0) {
                      __CrestBranch(39999, 10549, 1);

                    } else {
                      __CrestBranch(40000, 10550, 0);
# 3875 "regex.c"
                      goto _L___6;
                    }
                    }
                  } else {
                    __CrestBranch(39995, 10551, 0);
# 3875 "regex.c"
                    goto _L___6;
                  }
                  }
                } else {
                  __CrestBranch(39990, 10552, 0);
# 3875 "regex.c"
                  goto _L___6;
                }
                }
              } else {
                __CrestBranch(39985, 10553, 0);
# 3875 "regex.c"
                goto _L___6;
              }
              }
            } else {
              __CrestBranch(39980, 10554, 0);
# 3875 "regex.c"
              goto _L___6;
            }
            }
          } else {
            __CrestBranch(39975, 10555, 0);
# 3875 "regex.c"
            goto _L___6;
          }
          }
        } else {
          __CrestBranch(39970, 10556, 0);
# 3875 "regex.c"
          goto _L___6;
        }
        }
      } else {
        __CrestBranch(39965, 10557, 0);
# 3875 "regex.c"
        goto _L___6;
      }
      }
    } else {
      __CrestBranch(39960, 10558, 0);
      _L___6:
      {
# 3878 "regex.c"
      while (1) {
        while_continue___0: ;
# 3878 "regex.c"
        regstart = (char const **)((void *)0);
# 3878 "regex.c"
        regend = (char const **)((void *)0);
# 3878 "regex.c"
        old_regstart = (char const **)((void *)0);
# 3878 "regex.c"
        old_regend = (char const **)((void *)0);
# 3878 "regex.c"
        best_regstart = (char const **)((void *)0);
# 3878 "regex.c"
        best_regend = (char const **)((void *)0);
# 3878 "regex.c"
        reg_info = (register_info_type *)((void *)0);
# 3878 "regex.c"
        reg_dummy = (char const **)((void *)0);
# 3878 "regex.c"
        reg_info_dummy = (register_info_type *)((void *)0);
# 3878 "regex.c"
        goto while_break___0;
      }
      while_break___0: ;
      }
      __CrestLoad(40001, (unsigned long )0, (long long )-2);
      __CrestStore(40002, (unsigned long )(& __retres457));
# 3879 "regex.c"
      __retres457 = -2;
# 3879 "regex.c"
      goto return_label;
    }
    }
  } else {
    __CrestBranch(39919, 10566, 0);
# 3886 "regex.c"
    reg_dummy = (char const **)((void *)0);
# 3886 "regex.c"
    best_regend = reg_dummy;
# 3886 "regex.c"
    best_regstart = best_regend;
# 3886 "regex.c"
    old_regend = best_regstart;
# 3886 "regex.c"
    old_regstart = old_regend;
# 3886 "regex.c"
    regend = old_regstart;
# 3886 "regex.c"
    regstart = regend;
# 3888 "regex.c"
    reg_info_dummy = (register_info_type *)((void *)0);
# 3888 "regex.c"
    reg_info = reg_info_dummy;
  }
  }
  {
  __CrestLoad(40005, (unsigned long )(& pos), (long long )pos);
  __CrestLoad(40004, (unsigned long )0, (long long )0);
  __CrestApply2(40003, 16, (long long )(pos < 0));
# 3893 "regex.c"
  if (pos < 0) {
    __CrestBranch(40006, 10568, 1);
# 3893 "regex.c"
    goto _L___7;
  } else {
    __CrestBranch(40007, 10569, 0);
    {
    __CrestLoad(40012, (unsigned long )(& pos), (long long )pos);
    __CrestLoad(40011, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(40010, (unsigned long )(& size2), (long long )size2);
    __CrestApply2(40009, 0, (long long )(size1 + size2));
    __CrestApply2(40008, 14, (long long )(pos > size1 + size2));
# 3893 "regex.c"
    if (pos > size1 + size2) {
      __CrestBranch(40013, 10570, 1);
      _L___7:
      {
# 3895 "regex.c"
      while (1) {
        while_continue___1: ;
# 3895 "regex.c"
        regstart = (char const **)((void *)0);
# 3895 "regex.c"
        regend = (char const **)((void *)0);
# 3895 "regex.c"
        old_regstart = (char const **)((void *)0);
# 3895 "regex.c"
        old_regend = (char const **)((void *)0);
# 3895 "regex.c"
        best_regstart = (char const **)((void *)0);
# 3895 "regex.c"
        best_regend = (char const **)((void *)0);
# 3895 "regex.c"
        reg_info = (register_info_type *)((void *)0);
# 3895 "regex.c"
        reg_dummy = (char const **)((void *)0);
# 3895 "regex.c"
        reg_info_dummy = (register_info_type *)((void *)0);
# 3895 "regex.c"
        goto while_break___1;
      }
      while_break___1: ;
      }
      __CrestLoad(40015, (unsigned long )0, (long long )-1);
      __CrestStore(40016, (unsigned long )(& __retres457));
# 3896 "regex.c"
      __retres457 = -1;
# 3896 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(40014, 10578, 0);

    }
    }
  }
  }
  __CrestLoad(40017, (unsigned long )0, (long long )1);
  __CrestStore(40018, (unsigned long )(& mcnt));
# 3902 "regex.c"
  mcnt = 1;
  {
# 3902 "regex.c"
  while (1) {
    while_continue___2: ;
    {
    __CrestLoad(40021, (unsigned long )(& mcnt), (long long )mcnt);
    __CrestLoad(40020, (unsigned long )(& num_regs), (long long )num_regs);
    __CrestApply2(40019, 16, (long long )((size_t )((unsigned int )mcnt) < num_regs));
# 3902 "regex.c"
    if ((size_t )((unsigned int )mcnt) < num_regs) {
      __CrestBranch(40022, 10584, 1);

    } else {
      __CrestBranch(40023, 10585, 0);
# 3902 "regex.c"
      goto while_break___2;
    }
    }
# 3904 "regex.c"
    tmp___11 = (char const *)(& reg_unset_dummy);
# 3904 "regex.c"
    mem_209 = old_regend + mcnt;
# 3904 "regex.c"
    *mem_209 = tmp___11;
# 3904 "regex.c"
    tmp___10 = tmp___11;
# 3904 "regex.c"
    mem_210 = old_regstart + mcnt;
# 3904 "regex.c"
    *mem_210 = tmp___10;
# 3904 "regex.c"
    tmp___9 = tmp___10;
# 3904 "regex.c"
    mem_211 = regend + mcnt;
# 3904 "regex.c"
    *mem_211 = tmp___9;
# 3904 "regex.c"
    mem_212 = regstart + mcnt;
# 3904 "regex.c"
    *mem_212 = tmp___9;
# 3907 "regex.c"
    mem_213 = reg_info + mcnt;
# 3907 "regex.c"
    mem_213->bits.match_null_string_p = 3U;
# 3908 "regex.c"
    mem_214 = reg_info + mcnt;
# 3908 "regex.c"
    mem_214->bits.is_active = 0U;
# 3909 "regex.c"
    mem_215 = reg_info + mcnt;
# 3909 "regex.c"
    mem_215->bits.matched_something = 0U;
# 3910 "regex.c"
    mem_216 = reg_info + mcnt;
# 3910 "regex.c"
    mem_216->bits.ever_matched_something = 0U;
    __CrestLoad(40026, (unsigned long )(& mcnt), (long long )mcnt);
    __CrestLoad(40025, (unsigned long )0, (long long )1);
    __CrestApply2(40024, 0, (long long )(mcnt + 1));
    __CrestStore(40027, (unsigned long )(& mcnt));
# 3902 "regex.c"
    mcnt ++;
  }
  while_break___2: ;
  }
  {
  __CrestLoad(40030, (unsigned long )(& size2), (long long )size2);
  __CrestLoad(40029, (unsigned long )0, (long long )0);
  __CrestApply2(40028, 12, (long long )(size2 == 0));
# 3915 "regex.c"
  if (size2 == 0) {
    __CrestBranch(40031, 10589, 1);
    {
    __CrestLoad(40035, (unsigned long )(& string1), (long long )((unsigned long )string1));
    __CrestLoad(40034, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(40033, 13, (long long )((unsigned long )string1 != (unsigned long )((void *)0)));
# 3915 "regex.c"
    if ((unsigned long )string1 != (unsigned long )((void *)0)) {
      __CrestBranch(40036, 10590, 1);
# 3917 "regex.c"
      string2 = string1;
      __CrestLoad(40038, (unsigned long )(& size1), (long long )size1);
      __CrestStore(40039, (unsigned long )(& size2));
# 3918 "regex.c"
      size2 = size1;
# 3919 "regex.c"
      string1 = (char const *)0;
      __CrestLoad(40040, (unsigned long )0, (long long )0);
      __CrestStore(40041, (unsigned long )(& size1));
# 3920 "regex.c"
      size1 = 0;
    } else {
      __CrestBranch(40037, 10591, 0);

    }
    }
  } else {
    __CrestBranch(40032, 10592, 0);

  }
  }
# 3922 "regex.c"
  end1 = string1 + size1;
# 3923 "regex.c"
  end2 = string2 + size2;
  {
  __CrestLoad(40044, (unsigned long )(& stop), (long long )stop);
  __CrestLoad(40043, (unsigned long )(& size1), (long long )size1);
  __CrestApply2(40042, 15, (long long )(stop <= size1));
# 3926 "regex.c"
  if (stop <= size1) {
    __CrestBranch(40045, 10595, 1);
# 3928 "regex.c"
    end_match_1 = string1 + stop;
# 3929 "regex.c"
    end_match_2 = string2;
  } else {
    __CrestBranch(40046, 10596, 0);
# 3933 "regex.c"
    end_match_1 = end1;
# 3934 "regex.c"
    end_match_2 = (string2 + stop) - size1;
  }
  }
  {
  __CrestLoad(40049, (unsigned long )(& size1), (long long )size1);
  __CrestLoad(40048, (unsigned long )0, (long long )0);
  __CrestApply2(40047, 14, (long long )(size1 > 0));
# 3943 "regex.c"
  if (size1 > 0) {
    __CrestBranch(40050, 10598, 1);
    {
    __CrestLoad(40054, (unsigned long )(& pos), (long long )pos);
    __CrestLoad(40053, (unsigned long )(& size1), (long long )size1);
    __CrestApply2(40052, 15, (long long )(pos <= size1));
# 3943 "regex.c"
    if (pos <= size1) {
      __CrestBranch(40055, 10599, 1);
# 3945 "regex.c"
      d = string1 + pos;
# 3946 "regex.c"
      dend = end_match_1;
    } else {
      __CrestBranch(40056, 10600, 0);
# 3950 "regex.c"
      d = (string2 + pos) - size1;
# 3951 "regex.c"
      dend = end_match_2;
    }
    }
  } else {
    __CrestBranch(40051, 10601, 0);
# 3950 "regex.c"
    d = (string2 + pos) - size1;
# 3951 "regex.c"
    dend = end_match_2;
  }
  }
  {
# 3963 "regex.c"
  while (1) {
    while_continue___3: ;
    {
    __CrestLoad(40059, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(40058, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(40057, 12, (long long )((unsigned long )p == (unsigned long )pend));
# 3971 "regex.c"
    if ((unsigned long )p == (unsigned long )pend) {
      __CrestBranch(40060, 10606, 1);
      {
      __CrestLoad(40064, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(40063, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
      __CrestApply2(40062, 13, (long long )((unsigned long )d != (unsigned long )end_match_2));
# 3977 "regex.c"
      if ((unsigned long )d != (unsigned long )end_match_2) {
        __CrestBranch(40065, 10607, 1);
        {
        __CrestLoad(40069, (unsigned long )(& size1), (long long )size1);
        __CrestLoad(40068, (unsigned long )0, (long long )0);
        __CrestApply2(40067, 13, (long long )(size1 != 0));
# 3981 "regex.c"
        if (size1 != 0) {
          __CrestBranch(40070, 10608, 1);
          {
          __CrestLoad(40074, (unsigned long )(& string1), (long long )((unsigned long )string1));
          __CrestLoad(40073, (unsigned long )(& match_end), (long long )((unsigned long )match_end));
          __CrestApply2(40072, 15, (long long )((unsigned long )string1 <= (unsigned long )match_end));
# 3981 "regex.c"
          if ((unsigned long )string1 <= (unsigned long )match_end) {
            __CrestBranch(40075, 10609, 1);
            {
            __CrestLoad(40081, (unsigned long )(& match_end), (long long )((unsigned long )match_end));
            __CrestLoad(40080, (unsigned long )(& string1), (long long )((unsigned long )string1));
            __CrestLoad(40079, (unsigned long )(& size1), (long long )size1);
            __CrestApply2(40078, 18, (long long )((unsigned long )(string1 + size1)));
            __CrestApply2(40077, 15, (long long )((unsigned long )match_end <= (unsigned long )(string1 + size1)));
# 3981 "regex.c"
            if ((unsigned long )match_end <= (unsigned long )(string1 + size1)) {
              __CrestBranch(40082, 10610, 1);
              __CrestLoad(40084, (unsigned long )0, (long long )1);
              __CrestStore(40085, (unsigned long )(& tmp___12));
# 3981 "regex.c"
              tmp___12 = 1;
            } else {
              __CrestBranch(40083, 10611, 0);
              __CrestLoad(40086, (unsigned long )0, (long long )0);
              __CrestStore(40087, (unsigned long )(& tmp___12));
# 3981 "regex.c"
              tmp___12 = 0;
            }
            }
          } else {
            __CrestBranch(40076, 10612, 0);
            __CrestLoad(40088, (unsigned long )0, (long long )0);
            __CrestStore(40089, (unsigned long )(& tmp___12));
# 3981 "regex.c"
            tmp___12 = 0;
          }
          }
        } else {
          __CrestBranch(40071, 10613, 0);
          __CrestLoad(40090, (unsigned long )0, (long long )0);
          __CrestStore(40091, (unsigned long )(& tmp___12));
# 3981 "regex.c"
          tmp___12 = 0;
        }
        }
        __CrestLoad(40096, (unsigned long )(& tmp___12), (long long )tmp___12);
        __CrestLoad(40095, (unsigned long )(& dend), (long long )((unsigned long )dend));
        __CrestLoad(40094, (unsigned long )(& end_match_1), (long long )((unsigned long )end_match_1));
        __CrestApply2(40093, 12, (long long )((unsigned long )dend == (unsigned long )end_match_1));
        __CrestApply2(40092, 12, (long long )(tmp___12 == ((unsigned long )dend == (unsigned long )end_match_1)));
        __CrestStore(40097, (unsigned long )(& same_str_p));
# 3981 "regex.c"
        same_str_p = (boolean )(tmp___12 == ((unsigned long )dend == (unsigned long )end_match_1));
        {
        __CrestLoad(40100, (unsigned long )(& same_str_p), (long long )same_str_p);
        __CrestLoad(40099, (unsigned long )0, (long long )0);
        __CrestApply2(40098, 13, (long long )(same_str_p != 0));
# 3988 "regex.c"
        if (same_str_p != 0) {
          __CrestBranch(40101, 10616, 1);
          __CrestLoad(40105, (unsigned long )(& d), (long long )((unsigned long )d));
          __CrestLoad(40104, (unsigned long )(& match_end), (long long )((unsigned long )match_end));
          __CrestApply2(40103, 14, (long long )((unsigned long )d > (unsigned long )match_end));
          __CrestStore(40106, (unsigned long )(& best_match_p));
# 3989 "regex.c"
          best_match_p = (boolean )((unsigned long )d > (unsigned long )match_end);
        } else {
          __CrestBranch(40102, 10617, 0);
          __CrestLoad(40110, (unsigned long )(& dend), (long long )((unsigned long )dend));
          __CrestLoad(40109, (unsigned long )(& end_match_1), (long long )((unsigned long )end_match_1));
          __CrestApply2(40108, 12, (long long )((unsigned long )dend == (unsigned long )end_match_1));
          __CrestApply1(40107, 21, (long long )(! ((unsigned long )dend == (unsigned long )end_match_1)));
          __CrestStore(40111, (unsigned long )(& best_match_p));
# 3991 "regex.c"
          best_match_p = (boolean )(! ((unsigned long )dend == (unsigned long )end_match_1));
        }
        }
        {
        __CrestLoad(40114, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(40113, (unsigned long )0, (long long )0U);
        __CrestApply2(40112, 13, (long long )(fail_stack.avail != 0U));
# 3995 "regex.c"
        if (fail_stack.avail != 0U) {
          __CrestBranch(40115, 10619, 1);
          {
          __CrestLoad(40119, (unsigned long )(& best_regs_set), (long long )best_regs_set);
          __CrestLoad(40118, (unsigned long )0, (long long )0);
          __CrestApply2(40117, 12, (long long )(best_regs_set == 0));
# 3999 "regex.c"
          if (best_regs_set == 0) {
            __CrestBranch(40120, 10620, 1);
# 3999 "regex.c"
            goto _L___8;
          } else {
            __CrestBranch(40121, 10621, 0);
            {
            __CrestLoad(40124, (unsigned long )(& best_match_p), (long long )best_match_p);
            __CrestLoad(40123, (unsigned long )0, (long long )0);
            __CrestApply2(40122, 13, (long long )(best_match_p != 0));
# 3999 "regex.c"
            if (best_match_p != 0) {
              __CrestBranch(40125, 10622, 1);
              _L___8:
              __CrestLoad(40127, (unsigned long )0, (long long )1U);
              __CrestStore(40128, (unsigned long )(& best_regs_set));
# 4001 "regex.c"
              best_regs_set = 1U;
# 4002 "regex.c"
              match_end = d;
              __CrestLoad(40129, (unsigned long )0, (long long )1);
              __CrestStore(40130, (unsigned long )(& mcnt));
# 4006 "regex.c"
              mcnt = 1;
              {
# 4006 "regex.c"
              while (1) {
                while_continue___4: ;
                {
                __CrestLoad(40133, (unsigned long )(& mcnt), (long long )mcnt);
                __CrestLoad(40132, (unsigned long )(& num_regs), (long long )num_regs);
                __CrestApply2(40131, 16, (long long )((size_t )((unsigned int )mcnt) < num_regs));
# 4006 "regex.c"
                if ((size_t )((unsigned int )mcnt) < num_regs) {
                  __CrestBranch(40134, 10627, 1);

                } else {
                  __CrestBranch(40135, 10628, 0);
# 4006 "regex.c"
                  goto while_break___4;
                }
                }
# 4008 "regex.c"
                mem_217 = best_regstart + mcnt;
# 4008 "regex.c"
                mem_218 = regstart + mcnt;
# 4008 "regex.c"
                *mem_217 = *mem_218;
# 4009 "regex.c"
                mem_219 = best_regend + mcnt;
# 4009 "regex.c"
                mem_220 = regend + mcnt;
# 4009 "regex.c"
                *mem_219 = *mem_220;
                __CrestLoad(40138, (unsigned long )(& mcnt), (long long )mcnt);
                __CrestLoad(40137, (unsigned long )0, (long long )1);
                __CrestApply2(40136, 0, (long long )(mcnt + 1));
                __CrestStore(40139, (unsigned long )(& mcnt));
# 4006 "regex.c"
                mcnt ++;
              }
              while_break___4: ;
              }
            } else {
              __CrestBranch(40126, 10631, 0);

            }
            }
          }
          }
# 4012 "regex.c"
          goto fail;
        } else {
          __CrestBranch(40116, 10633, 0);
          {
          __CrestLoad(40142, (unsigned long )(& best_regs_set), (long long )best_regs_set);
          __CrestLoad(40141, (unsigned long )0, (long long )0);
          __CrestApply2(40140, 13, (long long )(best_regs_set != 0));
# 4018 "regex.c"
          if (best_regs_set != 0) {
            __CrestBranch(40143, 10634, 1);
            {
            __CrestLoad(40147, (unsigned long )(& best_match_p), (long long )best_match_p);
            __CrestLoad(40146, (unsigned long )0, (long long )0);
            __CrestApply2(40145, 12, (long long )(best_match_p == 0));
# 4018 "regex.c"
            if (best_match_p == 0) {
              __CrestBranch(40148, 10635, 1);
              restore_best_regs:
# 4028 "regex.c"
              d = match_end;
              {
              __CrestLoad(40152, (unsigned long )(& d), (long long )((unsigned long )d));
              __CrestLoad(40151, (unsigned long )(& string1), (long long )((unsigned long )string1));
              __CrestApply2(40150, 17, (long long )((unsigned long )d >= (unsigned long )string1));
# 4029 "regex.c"
              if ((unsigned long )d >= (unsigned long )string1) {
                __CrestBranch(40153, 10637, 1);
                {
                __CrestLoad(40157, (unsigned long )(& d), (long long )((unsigned long )d));
                __CrestLoad(40156, (unsigned long )(& end1), (long long )((unsigned long )end1));
                __CrestApply2(40155, 15, (long long )((unsigned long )d <= (unsigned long )end1));
# 4029 "regex.c"
                if ((unsigned long )d <= (unsigned long )end1) {
                  __CrestBranch(40158, 10638, 1);
# 4029 "regex.c"
                  dend = end_match_1;
                } else {
                  __CrestBranch(40159, 10639, 0);
# 4029 "regex.c"
                  dend = end_match_2;
                }
                }
              } else {
                __CrestBranch(40154, 10640, 0);
# 4029 "regex.c"
                dend = end_match_2;
              }
              }
              __CrestLoad(40160, (unsigned long )0, (long long )1);
              __CrestStore(40161, (unsigned long )(& mcnt));
# 4032 "regex.c"
              mcnt = 1;
              {
# 4032 "regex.c"
              while (1) {
                while_continue___5: ;
                {
                __CrestLoad(40164, (unsigned long )(& mcnt), (long long )mcnt);
                __CrestLoad(40163, (unsigned long )(& num_regs), (long long )num_regs);
                __CrestApply2(40162, 16, (long long )((size_t )((unsigned int )mcnt) < num_regs));
# 4032 "regex.c"
                if ((size_t )((unsigned int )mcnt) < num_regs) {
                  __CrestBranch(40165, 10646, 1);

                } else {
                  __CrestBranch(40166, 10647, 0);
# 4032 "regex.c"
                  goto while_break___5;
                }
                }
# 4034 "regex.c"
                mem_221 = regstart + mcnt;
# 4034 "regex.c"
                mem_222 = best_regstart + mcnt;
# 4034 "regex.c"
                *mem_221 = *mem_222;
# 4035 "regex.c"
                mem_223 = regend + mcnt;
# 4035 "regex.c"
                mem_224 = best_regend + mcnt;
# 4035 "regex.c"
                *mem_223 = *mem_224;
                __CrestLoad(40169, (unsigned long )(& mcnt), (long long )mcnt);
                __CrestLoad(40168, (unsigned long )0, (long long )1);
                __CrestApply2(40167, 0, (long long )(mcnt + 1));
                __CrestStore(40170, (unsigned long )(& mcnt));
# 4032 "regex.c"
                mcnt ++;
              }
              while_break___5: ;
              }
            } else {
              __CrestBranch(40149, 10650, 0);

            }
            }
          } else {
            __CrestBranch(40144, 10651, 0);

          }
          }
        }
        }
      } else {
        __CrestBranch(40066, 10652, 0);

      }
      }
      succeed_label: ;
      {
      __CrestLoad(40173, (unsigned long )(& regs), (long long )((unsigned long )regs));
      __CrestLoad(40172, (unsigned long )0, (long long )0);
      __CrestApply2(40171, 13, (long long )(regs != 0));
# 4044 "regex.c"
      if (regs != 0) {
        __CrestBranch(40174, 10655, 1);
        {
        __CrestLoad(40178, (unsigned long )0, (long long )bufp->no_sub);
        __CrestLoad(40177, (unsigned long )0, (long long )0);
        __CrestApply2(40176, 12, (long long )(bufp->no_sub == 0));
# 4044 "regex.c"
        if (bufp->no_sub == 0) {
          __CrestBranch(40179, 10656, 1);
          {
          __CrestLoad(40183, (unsigned long )0, (long long )bufp->regs_allocated);
          __CrestLoad(40182, (unsigned long )0, (long long )0U);
          __CrestApply2(40181, 12, (long long )(bufp->regs_allocated == 0U));
# 4047 "regex.c"
          if (bufp->regs_allocated == 0U) {
            __CrestBranch(40184, 10657, 1);
            {
            __CrestLoad(40190, (unsigned long )0, (long long )30UL);
            __CrestLoad(40189, (unsigned long )(& num_regs), (long long )num_regs);
            __CrestLoad(40188, (unsigned long )0, (long long )1UL);
            __CrestApply2(40187, 0, (long long )(num_regs + 1UL));
            __CrestApply2(40186, 14, (long long )(30UL > num_regs + 1UL));
# 4051 "regex.c"
            if (30UL > num_regs + 1UL) {
              __CrestBranch(40191, 10658, 1);
              __CrestLoad(40193, (unsigned long )0, (long long )30U);
              __CrestStore(40194, (unsigned long )(& regs->num_regs));
# 4051 "regex.c"
              regs->num_regs = 30U;
            } else {
              __CrestBranch(40192, 10659, 0);
              __CrestLoad(40197, (unsigned long )(& num_regs), (long long )num_regs);
              __CrestLoad(40196, (unsigned long )0, (long long )1UL);
              __CrestApply2(40195, 0, (long long )(num_regs + 1UL));
              __CrestStore(40198, (unsigned long )(& regs->num_regs));
# 4051 "regex.c"
              regs->num_regs = (unsigned int )(num_regs + 1UL);
            }
            }
            __CrestLoad(40201, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
            __CrestLoad(40200, (unsigned long )0, (long long )sizeof(regoff_t ));
            __CrestApply2(40199, 2, (long long )((unsigned long )regs->num_regs * sizeof(regoff_t )));
# 4052 "regex.c"
            tmp___13 = malloc((unsigned long )regs->num_regs * sizeof(regoff_t ));
            __CrestClearStack(40202);
# 4052 "regex.c"
            regs->start = (regoff_t *)tmp___13;
            __CrestLoad(40205, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
            __CrestLoad(40204, (unsigned long )0, (long long )sizeof(regoff_t ));
            __CrestApply2(40203, 2, (long long )((unsigned long )regs->num_regs * sizeof(regoff_t )));
# 4053 "regex.c"
            tmp___14 = malloc((unsigned long )regs->num_regs * sizeof(regoff_t ));
            __CrestClearStack(40206);
# 4053 "regex.c"
            regs->end = (regoff_t *)tmp___14;
            {
            __CrestLoad(40209, (unsigned long )(& regs->start), (long long )((unsigned long )regs->start));
            __CrestLoad(40208, (unsigned long )0, (long long )((unsigned long )((void *)0)));
            __CrestApply2(40207, 12, (long long )((unsigned long )regs->start == (unsigned long )((void *)0)));
# 4054 "regex.c"
            if ((unsigned long )regs->start == (unsigned long )((void *)0)) {
              __CrestBranch(40210, 10662, 1);
# 4054 "regex.c"
              goto _L___9;
            } else {
              __CrestBranch(40211, 10663, 0);
              {
              __CrestLoad(40214, (unsigned long )(& regs->end), (long long )((unsigned long )regs->end));
              __CrestLoad(40213, (unsigned long )0, (long long )((unsigned long )((void *)0)));
              __CrestApply2(40212, 12, (long long )((unsigned long )regs->end == (unsigned long )((void *)0)));
# 4054 "regex.c"
              if ((unsigned long )regs->end == (unsigned long )((void *)0)) {
                __CrestBranch(40215, 10664, 1);
                _L___9:
                {
# 4056 "regex.c"
                while (1) {
                  while_continue___6: ;
# 4056 "regex.c"
                  regstart = (char const **)((void *)0);
# 4056 "regex.c"
                  regend = (char const **)((void *)0);
# 4056 "regex.c"
                  old_regstart = (char const **)((void *)0);
# 4056 "regex.c"
                  old_regend = (char const **)((void *)0);
# 4056 "regex.c"
                  best_regstart = (char const **)((void *)0);
# 4056 "regex.c"
                  best_regend = (char const **)((void *)0);
# 4056 "regex.c"
                  reg_info = (register_info_type *)((void *)0);
# 4056 "regex.c"
                  reg_dummy = (char const **)((void *)0);
# 4056 "regex.c"
                  reg_info_dummy = (register_info_type *)((void *)0);
# 4056 "regex.c"
                  goto while_break___6;
                }
                while_break___6: ;
                }
                __CrestLoad(40217, (unsigned long )0, (long long )-2);
                __CrestStore(40218, (unsigned long )(& __retres457));
# 4057 "regex.c"
                __retres457 = -2;
# 4057 "regex.c"
                goto return_label;
              } else {
                __CrestBranch(40216, 10672, 0);

              }
              }
            }
            }
# 4059 "regex.c"
            bufp->regs_allocated = 1U;
          } else {
            __CrestBranch(40185, 10674, 0);
            {
            __CrestLoad(40221, (unsigned long )0, (long long )bufp->regs_allocated);
            __CrestLoad(40220, (unsigned long )0, (long long )1U);
            __CrestApply2(40219, 12, (long long )(bufp->regs_allocated == 1U));
# 4061 "regex.c"
            if (bufp->regs_allocated == 1U) {
              __CrestBranch(40222, 10675, 1);
              {
              __CrestLoad(40228, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
              __CrestLoad(40227, (unsigned long )(& num_regs), (long long )num_regs);
              __CrestLoad(40226, (unsigned long )0, (long long )1UL);
              __CrestApply2(40225, 0, (long long )(num_regs + 1UL));
              __CrestApply2(40224, 16, (long long )((size_t )regs->num_regs < num_regs + 1UL));
# 4065 "regex.c"
              if ((size_t )regs->num_regs < num_regs + 1UL) {
                __CrestBranch(40229, 10676, 1);
                __CrestLoad(40233, (unsigned long )(& num_regs), (long long )num_regs);
                __CrestLoad(40232, (unsigned long )0, (long long )1UL);
                __CrestApply2(40231, 0, (long long )(num_regs + 1UL));
                __CrestStore(40234, (unsigned long )(& regs->num_regs));
# 4067 "regex.c"
                regs->num_regs = (unsigned int )(num_regs + 1UL);
                __CrestLoad(40237, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
                __CrestLoad(40236, (unsigned long )0, (long long )sizeof(regoff_t ));
                __CrestApply2(40235, 2, (long long )((unsigned long )regs->num_regs * sizeof(regoff_t )));
# 4068 "regex.c"
                tmp___15 = realloc((void *)regs->start, (unsigned long )regs->num_regs * sizeof(regoff_t ));
                __CrestClearStack(40238);
# 4068 "regex.c"
                regs->start = (regoff_t *)tmp___15;
                __CrestLoad(40241, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
                __CrestLoad(40240, (unsigned long )0, (long long )sizeof(regoff_t ));
                __CrestApply2(40239, 2, (long long )((unsigned long )regs->num_regs * sizeof(regoff_t )));
# 4069 "regex.c"
                tmp___16 = realloc((void *)regs->end, (unsigned long )regs->num_regs * sizeof(regoff_t ));
                __CrestClearStack(40242);
# 4069 "regex.c"
                regs->end = (regoff_t *)tmp___16;
                {
                __CrestLoad(40245, (unsigned long )(& regs->start), (long long )((unsigned long )regs->start));
                __CrestLoad(40244, (unsigned long )0, (long long )((unsigned long )((void *)0)));
                __CrestApply2(40243, 12, (long long )((unsigned long )regs->start == (unsigned long )((void *)0)));
# 4070 "regex.c"
                if ((unsigned long )regs->start == (unsigned long )((void *)0)) {
                  __CrestBranch(40246, 10678, 1);
# 4070 "regex.c"
                  goto _L___10;
                } else {
                  __CrestBranch(40247, 10679, 0);
                  {
                  __CrestLoad(40250, (unsigned long )(& regs->end), (long long )((unsigned long )regs->end));
                  __CrestLoad(40249, (unsigned long )0, (long long )((unsigned long )((void *)0)));
                  __CrestApply2(40248, 12, (long long )((unsigned long )regs->end == (unsigned long )((void *)0)));
# 4070 "regex.c"
                  if ((unsigned long )regs->end == (unsigned long )((void *)0)) {
                    __CrestBranch(40251, 10680, 1);
                    _L___10:
                    {
# 4072 "regex.c"
                    while (1) {
                      while_continue___7: ;
# 4072 "regex.c"
                      regstart = (char const **)((void *)0);
# 4072 "regex.c"
                      regend = (char const **)((void *)0);
# 4072 "regex.c"
                      old_regstart = (char const **)((void *)0);
# 4072 "regex.c"
                      old_regend = (char const **)((void *)0);
# 4072 "regex.c"
                      best_regstart = (char const **)((void *)0);
# 4072 "regex.c"
                      best_regend = (char const **)((void *)0);
# 4072 "regex.c"
                      reg_info = (register_info_type *)((void *)0);
# 4072 "regex.c"
                      reg_dummy = (char const **)((void *)0);
# 4072 "regex.c"
                      reg_info_dummy = (register_info_type *)((void *)0);
# 4072 "regex.c"
                      goto while_break___7;
                    }
                    while_break___7: ;
                    }
                    __CrestLoad(40253, (unsigned long )0, (long long )-2);
                    __CrestStore(40254, (unsigned long )(& __retres457));
# 4073 "regex.c"
                    __retres457 = -2;
# 4073 "regex.c"
                    goto return_label;
                  } else {
                    __CrestBranch(40252, 10688, 0);

                  }
                  }
                }
                }
              } else {
                __CrestBranch(40230, 10689, 0);

              }
              }
            } else {
              __CrestBranch(40223, 10690, 0);

            }
            }
          }
          }
          {
          __CrestLoad(40257, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
          __CrestLoad(40256, (unsigned long )0, (long long )0U);
          __CrestApply2(40255, 14, (long long )(regs->num_regs > 0U));
# 4087 "regex.c"
          if (regs->num_regs > 0U) {
            __CrestBranch(40258, 10692, 1);
# 4089 "regex.c"
            mem_225 = regs->start + 0;
            __CrestLoad(40260, (unsigned long )(& pos), (long long )pos);
            __CrestStore(40261, (unsigned long )mem_225);
# 4089 "regex.c"
            *mem_225 = pos;
            {
            __CrestLoad(40264, (unsigned long )(& dend), (long long )((unsigned long )dend));
            __CrestLoad(40263, (unsigned long )(& end_match_1), (long long )((unsigned long )end_match_1));
            __CrestApply2(40262, 12, (long long )((unsigned long )dend == (unsigned long )end_match_1));
# 4090 "regex.c"
            if ((unsigned long )dend == (unsigned long )end_match_1) {
              __CrestBranch(40265, 10694, 1);
# 4090 "regex.c"
              mem_226 = regs->end + 0;
              __CrestLoad(40269, (unsigned long )(& d), (long long )((unsigned long )d));
              __CrestLoad(40268, (unsigned long )(& string1), (long long )((unsigned long )string1));
              __CrestApply2(40267, 18, (long long )(d - string1));
              __CrestStore(40270, (unsigned long )mem_226);
# 4090 "regex.c"
              *mem_226 = (regoff_t )(d - string1);
            } else {
              __CrestBranch(40266, 10695, 0);
# 4090 "regex.c"
              mem_227 = regs->end + 0;
              __CrestLoad(40275, (unsigned long )(& d), (long long )((unsigned long )d));
              __CrestLoad(40274, (unsigned long )(& string2), (long long )((unsigned long )string2));
              __CrestApply2(40273, 18, (long long )(d - string2));
              __CrestLoad(40272, (unsigned long )(& size1), (long long )size1);
              __CrestApply2(40271, 0, (long long )((d - string2) + (long )size1));
              __CrestStore(40276, (unsigned long )mem_227);
# 4090 "regex.c"
              *mem_227 = (regoff_t )((d - string2) + (long )size1);
            }
            }
          } else {
            __CrestBranch(40259, 10696, 0);

          }
          }
          __CrestLoad(40277, (unsigned long )0, (long long )1);
          __CrestStore(40278, (unsigned long )(& mcnt));
# 4097 "regex.c"
          mcnt = 1;
          {
# 4097 "regex.c"
          while (1) {
            while_continue___8: ;
            {
            __CrestLoad(40281, (unsigned long )(& num_regs), (long long )num_regs);
            __CrestLoad(40280, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
            __CrestApply2(40279, 16, (long long )(num_regs < (size_t )regs->num_regs));
# 4097 "regex.c"
            if (num_regs < (size_t )regs->num_regs) {
              __CrestBranch(40282, 10702, 1);
              __CrestLoad(40284, (unsigned long )(& num_regs), (long long )num_regs);
              __CrestStore(40285, (unsigned long )(& tmp___20));
# 4097 "regex.c"
              tmp___20 = num_regs;
            } else {
              __CrestBranch(40283, 10703, 0);
              __CrestLoad(40286, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
              __CrestStore(40287, (unsigned long )(& tmp___20));
# 4097 "regex.c"
              tmp___20 = (size_t )regs->num_regs;
            }
            }
            {
            __CrestLoad(40290, (unsigned long )(& mcnt), (long long )mcnt);
            __CrestLoad(40289, (unsigned long )(& tmp___20), (long long )tmp___20);
            __CrestApply2(40288, 16, (long long )((size_t )((unsigned int )mcnt) < tmp___20));
# 4097 "regex.c"
            if ((size_t )((unsigned int )mcnt) < tmp___20) {
              __CrestBranch(40291, 10705, 1);

            } else {
              __CrestBranch(40292, 10706, 0);
# 4097 "regex.c"
              goto while_break___8;
            }
            }
            {
# 4100 "regex.c"
            mem_228 = regstart + mcnt;
            {
            __CrestLoad(40295, (unsigned long )mem_228, (long long )((unsigned long )*mem_228));
            __CrestLoad(40294, (unsigned long )0, (long long )((unsigned long )(& reg_unset_dummy)));
            __CrestApply2(40293, 12, (long long )((unsigned long )*mem_228 == (unsigned long )(& reg_unset_dummy)));
# 4100 "regex.c"
            if ((unsigned long )*mem_228 == (unsigned long )(& reg_unset_dummy)) {
              __CrestBranch(40296, 10710, 1);
              __CrestLoad(40298, (unsigned long )0, (long long )-1);
              __CrestStore(40299, (unsigned long )(& tmp___17));
# 4101 "regex.c"
              tmp___17 = -1;
# 4101 "regex.c"
              mem_229 = regs->end + mcnt;
              __CrestLoad(40300, (unsigned long )(& tmp___17), (long long )tmp___17);
              __CrestStore(40301, (unsigned long )mem_229);
# 4101 "regex.c"
              *mem_229 = tmp___17;
# 4101 "regex.c"
              mem_230 = regs->start + mcnt;
              __CrestLoad(40302, (unsigned long )(& tmp___17), (long long )tmp___17);
              __CrestStore(40303, (unsigned long )mem_230);
# 4101 "regex.c"
              *mem_230 = tmp___17;
            } else {
              __CrestBranch(40297, 10711, 0);
              {
# 4100 "regex.c"
              mem_231 = regend + mcnt;
              {
              __CrestLoad(40306, (unsigned long )mem_231, (long long )((unsigned long )*mem_231));
              __CrestLoad(40305, (unsigned long )0, (long long )((unsigned long )(& reg_unset_dummy)));
              __CrestApply2(40304, 12, (long long )((unsigned long )*mem_231 == (unsigned long )(& reg_unset_dummy)));
# 4100 "regex.c"
              if ((unsigned long )*mem_231 == (unsigned long )(& reg_unset_dummy)) {
                __CrestBranch(40307, 10714, 1);
                __CrestLoad(40309, (unsigned long )0, (long long )-1);
                __CrestStore(40310, (unsigned long )(& tmp___17));
# 4101 "regex.c"
                tmp___17 = -1;
# 4101 "regex.c"
                mem_232 = regs->end + mcnt;
                __CrestLoad(40311, (unsigned long )(& tmp___17), (long long )tmp___17);
                __CrestStore(40312, (unsigned long )mem_232);
# 4101 "regex.c"
                *mem_232 = tmp___17;
# 4101 "regex.c"
                mem_233 = regs->start + mcnt;
                __CrestLoad(40313, (unsigned long )(& tmp___17), (long long )tmp___17);
                __CrestStore(40314, (unsigned long )mem_233);
# 4101 "regex.c"
                *mem_233 = tmp___17;
              } else {
                __CrestBranch(40308, 10715, 0);
                {
                __CrestLoad(40317, (unsigned long )(& size1), (long long )size1);
                __CrestLoad(40316, (unsigned long )0, (long long )0);
                __CrestApply2(40315, 13, (long long )(size1 != 0));
# 4104 "regex.c"
                if (size1 != 0) {
                  __CrestBranch(40318, 10716, 1);
                  {
# 4104 "regex.c"
                  mem_234 = regstart + mcnt;
                  {
                  __CrestLoad(40322, (unsigned long )(& string1), (long long )((unsigned long )string1));
                  __CrestLoad(40321, (unsigned long )mem_234, (long long )((unsigned long )*mem_234));
                  __CrestApply2(40320, 15, (long long )((unsigned long )string1 <= (unsigned long )*mem_234));
# 4104 "regex.c"
                  if ((unsigned long )string1 <= (unsigned long )*mem_234) {
                    __CrestBranch(40323, 10719, 1);
                    {
# 4104 "regex.c"
                    mem_235 = regstart + mcnt;
                    {
                    __CrestLoad(40329, (unsigned long )mem_235, (long long )((unsigned long )*mem_235));
                    __CrestLoad(40328, (unsigned long )(& string1), (long long )((unsigned long )string1));
                    __CrestLoad(40327, (unsigned long )(& size1), (long long )size1);
                    __CrestApply2(40326, 18, (long long )((unsigned long )(string1 + size1)));
                    __CrestApply2(40325, 15, (long long )((unsigned long )*mem_235 <= (unsigned long )(string1 + size1)));
# 4104 "regex.c"
                    if ((unsigned long )*mem_235 <= (unsigned long )(string1 + size1)) {
                      __CrestBranch(40330, 10722, 1);
# 4104 "regex.c"
                      mem_236 = regstart + mcnt;
                      __CrestLoad(40334, (unsigned long )mem_236, (long long )((unsigned long )*mem_236));
                      __CrestLoad(40333, (unsigned long )(& string1), (long long )((unsigned long )string1));
                      __CrestApply2(40332, 18, (long long )(*mem_236 - string1));
                      __CrestStore(40335, (unsigned long )(& tmp___18));
# 4104 "regex.c"
                      tmp___18 = (regoff_t )(*mem_236 - string1);
                    } else {
                      __CrestBranch(40331, 10723, 0);
# 4104 "regex.c"
                      mem_237 = regstart + mcnt;
                      __CrestLoad(40340, (unsigned long )mem_237, (long long )((unsigned long )*mem_237));
                      __CrestLoad(40339, (unsigned long )(& string2), (long long )((unsigned long )string2));
                      __CrestApply2(40338, 18, (long long )(*mem_237 - string2));
                      __CrestLoad(40337, (unsigned long )(& size1), (long long )size1);
                      __CrestApply2(40336, 0, (long long )((*mem_237 - string2) + (long )size1));
                      __CrestStore(40341, (unsigned long )(& tmp___18));
# 4104 "regex.c"
                      tmp___18 = (regoff_t )((*mem_237 - string2) + (long )size1);
                    }
                    }
                    }
                  } else {
                    __CrestBranch(40324, 10724, 0);
# 4104 "regex.c"
                    mem_238 = regstart + mcnt;
                    __CrestLoad(40346, (unsigned long )mem_238, (long long )((unsigned long )*mem_238));
                    __CrestLoad(40345, (unsigned long )(& string2), (long long )((unsigned long )string2));
                    __CrestApply2(40344, 18, (long long )(*mem_238 - string2));
                    __CrestLoad(40343, (unsigned long )(& size1), (long long )size1);
                    __CrestApply2(40342, 0, (long long )((*mem_238 - string2) + (long )size1));
                    __CrestStore(40347, (unsigned long )(& tmp___18));
# 4104 "regex.c"
                    tmp___18 = (regoff_t )((*mem_238 - string2) + (long )size1);
                  }
                  }
                  }
                } else {
                  __CrestBranch(40319, 10725, 0);
# 4104 "regex.c"
                  mem_239 = regstart + mcnt;
                  __CrestLoad(40352, (unsigned long )mem_239, (long long )((unsigned long )*mem_239));
                  __CrestLoad(40351, (unsigned long )(& string2), (long long )((unsigned long )string2));
                  __CrestApply2(40350, 18, (long long )(*mem_239 - string2));
                  __CrestLoad(40349, (unsigned long )(& size1), (long long )size1);
                  __CrestApply2(40348, 0, (long long )((*mem_239 - string2) + (long )size1));
                  __CrestStore(40353, (unsigned long )(& tmp___18));
# 4104 "regex.c"
                  tmp___18 = (regoff_t )((*mem_239 - string2) + (long )size1);
                }
                }
# 4104 "regex.c"
                mem_240 = regs->start + mcnt;
                __CrestLoad(40354, (unsigned long )(& tmp___18), (long long )tmp___18);
                __CrestStore(40355, (unsigned long )mem_240);
# 4104 "regex.c"
                *mem_240 = tmp___18;
                {
                __CrestLoad(40358, (unsigned long )(& size1), (long long )size1);
                __CrestLoad(40357, (unsigned long )0, (long long )0);
                __CrestApply2(40356, 13, (long long )(size1 != 0));
# 4106 "regex.c"
                if (size1 != 0) {
                  __CrestBranch(40359, 10728, 1);
                  {
# 4106 "regex.c"
                  mem_241 = regend + mcnt;
                  {
                  __CrestLoad(40363, (unsigned long )(& string1), (long long )((unsigned long )string1));
                  __CrestLoad(40362, (unsigned long )mem_241, (long long )((unsigned long )*mem_241));
                  __CrestApply2(40361, 15, (long long )((unsigned long )string1 <= (unsigned long )*mem_241));
# 4106 "regex.c"
                  if ((unsigned long )string1 <= (unsigned long )*mem_241) {
                    __CrestBranch(40364, 10731, 1);
                    {
# 4106 "regex.c"
                    mem_242 = regend + mcnt;
                    {
                    __CrestLoad(40370, (unsigned long )mem_242, (long long )((unsigned long )*mem_242));
                    __CrestLoad(40369, (unsigned long )(& string1), (long long )((unsigned long )string1));
                    __CrestLoad(40368, (unsigned long )(& size1), (long long )size1);
                    __CrestApply2(40367, 18, (long long )((unsigned long )(string1 + size1)));
                    __CrestApply2(40366, 15, (long long )((unsigned long )*mem_242 <= (unsigned long )(string1 + size1)));
# 4106 "regex.c"
                    if ((unsigned long )*mem_242 <= (unsigned long )(string1 + size1)) {
                      __CrestBranch(40371, 10734, 1);
# 4106 "regex.c"
                      mem_243 = regend + mcnt;
                      __CrestLoad(40375, (unsigned long )mem_243, (long long )((unsigned long )*mem_243));
                      __CrestLoad(40374, (unsigned long )(& string1), (long long )((unsigned long )string1));
                      __CrestApply2(40373, 18, (long long )(*mem_243 - string1));
                      __CrestStore(40376, (unsigned long )(& tmp___19));
# 4106 "regex.c"
                      tmp___19 = (regoff_t )(*mem_243 - string1);
                    } else {
                      __CrestBranch(40372, 10735, 0);
# 4106 "regex.c"
                      mem_244 = regend + mcnt;
                      __CrestLoad(40381, (unsigned long )mem_244, (long long )((unsigned long )*mem_244));
                      __CrestLoad(40380, (unsigned long )(& string2), (long long )((unsigned long )string2));
                      __CrestApply2(40379, 18, (long long )(*mem_244 - string2));
                      __CrestLoad(40378, (unsigned long )(& size1), (long long )size1);
                      __CrestApply2(40377, 0, (long long )((*mem_244 - string2) + (long )size1));
                      __CrestStore(40382, (unsigned long )(& tmp___19));
# 4106 "regex.c"
                      tmp___19 = (regoff_t )((*mem_244 - string2) + (long )size1);
                    }
                    }
                    }
                  } else {
                    __CrestBranch(40365, 10736, 0);
# 4106 "regex.c"
                    mem_245 = regend + mcnt;
                    __CrestLoad(40387, (unsigned long )mem_245, (long long )((unsigned long )*mem_245));
                    __CrestLoad(40386, (unsigned long )(& string2), (long long )((unsigned long )string2));
                    __CrestApply2(40385, 18, (long long )(*mem_245 - string2));
                    __CrestLoad(40384, (unsigned long )(& size1), (long long )size1);
                    __CrestApply2(40383, 0, (long long )((*mem_245 - string2) + (long )size1));
                    __CrestStore(40388, (unsigned long )(& tmp___19));
# 4106 "regex.c"
                    tmp___19 = (regoff_t )((*mem_245 - string2) + (long )size1);
                  }
                  }
                  }
                } else {
                  __CrestBranch(40360, 10737, 0);
# 4106 "regex.c"
                  mem_246 = regend + mcnt;
                  __CrestLoad(40393, (unsigned long )mem_246, (long long )((unsigned long )*mem_246));
                  __CrestLoad(40392, (unsigned long )(& string2), (long long )((unsigned long )string2));
                  __CrestApply2(40391, 18, (long long )(*mem_246 - string2));
                  __CrestLoad(40390, (unsigned long )(& size1), (long long )size1);
                  __CrestApply2(40389, 0, (long long )((*mem_246 - string2) + (long )size1));
                  __CrestStore(40394, (unsigned long )(& tmp___19));
# 4106 "regex.c"
                  tmp___19 = (regoff_t )((*mem_246 - string2) + (long )size1);
                }
                }
# 4106 "regex.c"
                mem_247 = regs->end + mcnt;
                __CrestLoad(40395, (unsigned long )(& tmp___19), (long long )tmp___19);
                __CrestStore(40396, (unsigned long )mem_247);
# 4106 "regex.c"
                *mem_247 = tmp___19;
              }
              }
              }
            }
            }
            }
            __CrestLoad(40399, (unsigned long )(& mcnt), (long long )mcnt);
            __CrestLoad(40398, (unsigned long )0, (long long )1);
            __CrestApply2(40397, 0, (long long )(mcnt + 1));
            __CrestStore(40400, (unsigned long )(& mcnt));
# 4097 "regex.c"
            mcnt ++;
          }
          while_break___8: ;
          }
          __CrestLoad(40401, (unsigned long )(& num_regs), (long long )num_regs);
          __CrestStore(40402, (unsigned long )(& mcnt));
# 4116 "regex.c"
          mcnt = (int )num_regs;
          {
# 4116 "regex.c"
          while (1) {
            while_continue___9: ;
            {
            __CrestLoad(40405, (unsigned long )(& mcnt), (long long )mcnt);
            __CrestLoad(40404, (unsigned long )(& regs->num_regs), (long long )regs->num_regs);
            __CrestApply2(40403, 16, (long long )((unsigned int )mcnt < regs->num_regs));
# 4116 "regex.c"
            if ((unsigned int )mcnt < regs->num_regs) {
              __CrestBranch(40406, 10746, 1);

            } else {
              __CrestBranch(40407, 10747, 0);
# 4116 "regex.c"
              goto while_break___9;
            }
            }
            __CrestLoad(40408, (unsigned long )0, (long long )-1);
            __CrestStore(40409, (unsigned long )(& tmp___21));
# 4117 "regex.c"
            tmp___21 = -1;
# 4117 "regex.c"
            mem_248 = regs->end + mcnt;
            __CrestLoad(40410, (unsigned long )(& tmp___21), (long long )tmp___21);
            __CrestStore(40411, (unsigned long )mem_248);
# 4117 "regex.c"
            *mem_248 = tmp___21;
# 4117 "regex.c"
            mem_249 = regs->start + mcnt;
            __CrestLoad(40412, (unsigned long )(& tmp___21), (long long )tmp___21);
            __CrestStore(40413, (unsigned long )mem_249);
# 4117 "regex.c"
            *mem_249 = tmp___21;
            __CrestLoad(40416, (unsigned long )(& mcnt), (long long )mcnt);
            __CrestLoad(40415, (unsigned long )0, (long long )1);
            __CrestApply2(40414, 0, (long long )(mcnt + 1));
            __CrestStore(40417, (unsigned long )(& mcnt));
# 4116 "regex.c"
            mcnt ++;
          }
          while_break___9: ;
          }
        } else {
          __CrestBranch(40180, 10750, 0);

        }
        }
      } else {
        __CrestBranch(40175, 10751, 0);

      }
      }
      {
      __CrestLoad(40420, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestLoad(40419, (unsigned long )(& end_match_1), (long long )((unsigned long )end_match_1));
      __CrestApply2(40418, 12, (long long )((unsigned long )dend == (unsigned long )end_match_1));
# 4125 "regex.c"
      if ((unsigned long )dend == (unsigned long )end_match_1) {
        __CrestBranch(40421, 10753, 1);
# 4125 "regex.c"
        tmp___22 = string1;
      } else {
        __CrestBranch(40422, 10754, 0);
# 4125 "regex.c"
        tmp___22 = string2 - size1;
      }
      }
      __CrestLoad(40427, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(40426, (unsigned long )(& pos), (long long )pos);
      __CrestApply2(40425, 18, (long long )((unsigned long )(d - pos)));
      __CrestLoad(40424, (unsigned long )(& tmp___22), (long long )((unsigned long )tmp___22));
      __CrestApply2(40423, 18, (long long )((d - pos) - tmp___22));
      __CrestStore(40428, (unsigned long )(& mcnt));
# 4125 "regex.c"
      mcnt = (int )((d - pos) - tmp___22);
      {
# 4131 "regex.c"
      while (1) {
        while_continue___10: ;
# 4131 "regex.c"
        regstart = (char const **)((void *)0);
# 4131 "regex.c"
        regend = (char const **)((void *)0);
# 4131 "regex.c"
        old_regstart = (char const **)((void *)0);
# 4131 "regex.c"
        old_regend = (char const **)((void *)0);
# 4131 "regex.c"
        best_regstart = (char const **)((void *)0);
# 4131 "regex.c"
        best_regend = (char const **)((void *)0);
# 4131 "regex.c"
        reg_info = (register_info_type *)((void *)0);
# 4131 "regex.c"
        reg_dummy = (char const **)((void *)0);
# 4131 "regex.c"
        reg_info_dummy = (register_info_type *)((void *)0);
# 4131 "regex.c"
        goto while_break___10;
      }
      while_break___10: ;
      }
      __CrestLoad(40429, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestStore(40430, (unsigned long )(& __retres457));
# 4132 "regex.c"
      __retres457 = mcnt;
# 4132 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(40061, 10764, 0);

    }
    }
# 4136 "regex.c"
    tmp___23 = p;
# 4136 "regex.c"
    p ++;
    {
    {
    __CrestLoad(40433, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40432, (unsigned long )0, (long long )0U);
    __CrestApply2(40431, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 0U));
# 4140 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 0U) {
      __CrestBranch(40434, 10768, 1);
# 4140 "regex.c"
      goto case_0;
    } else {
      __CrestBranch(40435, 10769, 0);

    }
    }
    {
    __CrestLoad(40438, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40437, (unsigned long )0, (long long )1U);
    __CrestApply2(40436, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 1U));
# 4144 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 1U) {
      __CrestBranch(40439, 10771, 1);
# 4144 "regex.c"
      goto case_1;
    } else {
      __CrestBranch(40440, 10772, 0);

    }
    }
    {
    __CrestLoad(40443, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40442, (unsigned long )0, (long long )2U);
    __CrestApply2(40441, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 2U));
# 4151 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 2U) {
      __CrestBranch(40444, 10774, 1);
# 4151 "regex.c"
      goto case_2;
    } else {
      __CrestBranch(40445, 10775, 0);

    }
    }
    {
    __CrestLoad(40448, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40447, (unsigned long )0, (long long )3U);
    __CrestApply2(40446, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 3U));
# 4182 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 3U) {
      __CrestBranch(40449, 10777, 1);
# 4182 "regex.c"
      goto case_3;
    } else {
      __CrestBranch(40450, 10778, 0);

    }
    }
    {
    __CrestLoad(40453, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40452, (unsigned long )0, (long long )5U);
    __CrestApply2(40451, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 5U));
# 4198 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 5U) {
      __CrestBranch(40454, 10780, 1);
# 4198 "regex.c"
      goto case_5;
    } else {
      __CrestBranch(40455, 10781, 0);

    }
    }
    {
    __CrestLoad(40458, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40457, (unsigned long )0, (long long )4U);
    __CrestApply2(40456, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 4U));
# 4198 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 4U) {
      __CrestBranch(40459, 10783, 1);
# 4198 "regex.c"
      goto case_5;
    } else {
      __CrestBranch(40460, 10784, 0);

    }
    }
    {
    __CrestLoad(40463, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40462, (unsigned long )0, (long long )6U);
    __CrestApply2(40461, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 6U));
# 4229 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 6U) {
      __CrestBranch(40464, 10786, 1);
# 4229 "regex.c"
      goto case_6;
    } else {
      __CrestBranch(40465, 10787, 0);

    }
    }
    {
    __CrestLoad(40468, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40467, (unsigned long )0, (long long )7U);
    __CrestApply2(40466, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 7U));
# 4277 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 7U) {
      __CrestBranch(40469, 10789, 1);
# 4277 "regex.c"
      goto case_7;
    } else {
      __CrestBranch(40470, 10790, 0);

    }
    }
    {
    __CrestLoad(40473, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40472, (unsigned long )0, (long long )8U);
    __CrestApply2(40471, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 8U));
# 4413 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 8U) {
      __CrestBranch(40474, 10792, 1);
# 4413 "regex.c"
      goto case_8;
    } else {
      __CrestBranch(40475, 10793, 0);

    }
    }
    {
    __CrestLoad(40478, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40477, (unsigned long )0, (long long )9U);
    __CrestApply2(40476, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 9U));
# 4479 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 9U) {
      __CrestBranch(40479, 10795, 1);
# 4479 "regex.c"
      goto case_9;
    } else {
      __CrestBranch(40480, 10796, 0);

    }
    }
    {
    __CrestLoad(40483, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40482, (unsigned long )0, (long long )10U);
    __CrestApply2(40481, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 10U));
# 4495 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 10U) {
      __CrestBranch(40484, 10798, 1);
# 4495 "regex.c"
      goto case_10;
    } else {
      __CrestBranch(40485, 10799, 0);

    }
    }
    {
    __CrestLoad(40488, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40487, (unsigned long )0, (long long )11U);
    __CrestApply2(40486, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 11U));
# 4513 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 11U) {
      __CrestBranch(40489, 10801, 1);
# 4513 "regex.c"
      goto case_11;
    } else {
      __CrestBranch(40490, 10802, 0);

    }
    }
    {
    __CrestLoad(40493, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40492, (unsigned long )0, (long long )12U);
    __CrestApply2(40491, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 12U));
# 4521 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 12U) {
      __CrestBranch(40494, 10804, 1);
# 4521 "regex.c"
      goto case_12;
    } else {
      __CrestBranch(40495, 10805, 0);

    }
    }
    {
    __CrestLoad(40498, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40497, (unsigned long )0, (long long )16U);
    __CrestApply2(40496, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 16U));
# 4544 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 16U) {
      __CrestBranch(40499, 10807, 1);
# 4544 "regex.c"
      goto case_16;
    } else {
      __CrestBranch(40500, 10808, 0);

    }
    }
    {
    __CrestLoad(40503, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40502, (unsigned long )0, (long long )15U);
    __CrestApply2(40501, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 15U));
# 4570 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 15U) {
      __CrestBranch(40504, 10810, 1);
# 4570 "regex.c"
      goto on_failure;
    } else {
      __CrestBranch(40505, 10811, 0);

    }
    }
    {
    __CrestLoad(40508, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40507, (unsigned long )0, (long long )18U);
    __CrestApply2(40506, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 18U));
# 4617 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 18U) {
      __CrestBranch(40509, 10813, 1);
# 4617 "regex.c"
      goto case_18___0;
    } else {
      __CrestBranch(40510, 10814, 0);

    }
    }
    {
    __CrestLoad(40513, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40512, (unsigned long )0, (long long )17U);
    __CrestApply2(40511, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 17U));
# 4776 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 17U) {
      __CrestBranch(40514, 10816, 1);
# 4776 "regex.c"
      goto case_17___0;
    } else {
      __CrestBranch(40515, 10817, 0);

    }
    }
    {
    __CrestLoad(40518, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40517, (unsigned long )0, (long long )13U);
    __CrestApply2(40516, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 13U));
# 4803 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 13U) {
      __CrestBranch(40519, 10819, 1);
# 4803 "regex.c"
      goto case_13___0;
    } else {
      __CrestBranch(40520, 10820, 0);

    }
    }
    {
    __CrestLoad(40523, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40522, (unsigned long )0, (long long )14U);
    __CrestApply2(40521, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 14U));
# 4817 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 14U) {
      __CrestBranch(40524, 10822, 1);
# 4817 "regex.c"
      goto case_14;
    } else {
      __CrestBranch(40525, 10823, 0);

    }
    }
    {
    __CrestLoad(40528, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40527, (unsigned long )0, (long long )19U);
    __CrestApply2(40526, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 19U));
# 4827 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 19U) {
      __CrestBranch(40529, 10825, 1);
# 4827 "regex.c"
      goto case_19___0;
    } else {
      __CrestBranch(40530, 10826, 0);

    }
    }
    {
    __CrestLoad(40533, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40532, (unsigned long )0, (long long )20U);
    __CrestApply2(40531, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 20U));
# 4840 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 20U) {
      __CrestBranch(40534, 10828, 1);
# 4840 "regex.c"
      goto case_20;
    } else {
      __CrestBranch(40535, 10829, 0);

    }
    }
    {
    __CrestLoad(40538, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40537, (unsigned long )0, (long long )21U);
    __CrestApply2(40536, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 21U));
# 4849 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 21U) {
      __CrestBranch(40539, 10831, 1);
# 4849 "regex.c"
      goto case_21;
    } else {
      __CrestBranch(40540, 10832, 0);

    }
    }
    {
    __CrestLoad(40543, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40542, (unsigned long )0, (long long )22U);
    __CrestApply2(40541, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 22U));
# 4879 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 22U) {
      __CrestBranch(40544, 10834, 1);
# 4879 "regex.c"
      goto case_22___0;
    } else {
      __CrestBranch(40545, 10835, 0);

    }
    }
    {
    __CrestLoad(40548, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40547, (unsigned long )0, (long long )23U);
    __CrestApply2(40546, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 23U));
# 4900 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 23U) {
      __CrestBranch(40549, 10837, 1);
# 4900 "regex.c"
      goto case_23;
    } else {
      __CrestBranch(40550, 10838, 0);

    }
    }
    {
    __CrestLoad(40553, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40552, (unsigned long )0, (long long )28U);
    __CrestApply2(40551, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 28U));
# 4934 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 28U) {
      __CrestBranch(40554, 10840, 1);
# 4934 "regex.c"
      goto case_28;
    } else {
      __CrestBranch(40555, 10841, 0);

    }
    }
    {
    __CrestLoad(40558, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40557, (unsigned long )0, (long long )29U);
    __CrestApply2(40556, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 29U));
# 4949 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 29U) {
      __CrestBranch(40559, 10843, 1);
# 4949 "regex.c"
      goto case_29;
    } else {
      __CrestBranch(40560, 10844, 0);

    }
    }
    {
    __CrestLoad(40563, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40562, (unsigned long )0, (long long )26U);
    __CrestApply2(40561, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 26U));
# 4965 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 26U) {
      __CrestBranch(40564, 10846, 1);
# 4965 "regex.c"
      goto case_26;
    } else {
      __CrestBranch(40565, 10847, 0);

    }
    }
    {
    __CrestLoad(40568, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40567, (unsigned long )0, (long long )27U);
    __CrestApply2(40566, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 27U));
# 4971 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 27U) {
      __CrestBranch(40569, 10849, 1);
# 4971 "regex.c"
      goto case_27;
    } else {
      __CrestBranch(40570, 10850, 0);

    }
    }
    {
    __CrestLoad(40573, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40572, (unsigned long )0, (long long )24U);
    __CrestApply2(40571, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 24U));
# 5032 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 24U) {
      __CrestBranch(40574, 10852, 1);
# 5032 "regex.c"
      goto case_24;
    } else {
      __CrestBranch(40575, 10853, 0);

    }
    }
    {
    __CrestLoad(40578, (unsigned long )tmp___23, (long long )*tmp___23);
    __CrestLoad(40577, (unsigned long )0, (long long )25U);
    __CrestApply2(40576, 12, (long long )((unsigned int )((re_opcode_t )*tmp___23) == 25U));
# 5041 "regex.c"
    if ((unsigned int )((re_opcode_t )*tmp___23) == 25U) {
      __CrestBranch(40579, 10855, 1);
# 5041 "regex.c"
      goto case_25;
    } else {
      __CrestBranch(40580, 10856, 0);

    }
    }
# 5051 "regex.c"
    goto switch_default___0;
    case_0: ;
# 4142 "regex.c"
    goto switch_break;
    case_1: ;
# 4146 "regex.c"
    goto succeed_label;
    case_2:
# 4152 "regex.c"
    tmp___24 = p;
# 4152 "regex.c"
    p ++;
    __CrestLoad(40581, (unsigned long )tmp___24, (long long )*tmp___24);
    __CrestStore(40582, (unsigned long )(& mcnt));
# 4152 "regex.c"
    mcnt = (int )*tmp___24;
    {
    __CrestLoad(40585, (unsigned long )(& translate), (long long )((unsigned long )translate));
    __CrestLoad(40584, (unsigned long )0, (long long )0);
    __CrestApply2(40583, 13, (long long )(translate != 0));
# 4157 "regex.c"
    if (translate != 0) {
      __CrestBranch(40586, 10864, 1);
      {
# 4159 "regex.c"
      while (1) {
        while_continue___11: ;
        {
# 4161 "regex.c"
        while (1) {
          while_continue___12: ;
          {
          __CrestLoad(40590, (unsigned long )(& d), (long long )((unsigned long )d));
          __CrestLoad(40589, (unsigned long )(& dend), (long long )((unsigned long )dend));
          __CrestApply2(40588, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 4161 "regex.c"
          if ((unsigned long )d == (unsigned long )dend) {
            __CrestBranch(40591, 10871, 1);

          } else {
            __CrestBranch(40592, 10872, 0);
# 4161 "regex.c"
            goto while_break___12;
          }
          }
          {
          __CrestLoad(40595, (unsigned long )(& dend), (long long )((unsigned long )dend));
          __CrestLoad(40594, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
          __CrestApply2(40593, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 4161 "regex.c"
          if ((unsigned long )dend == (unsigned long )end_match_2) {
            __CrestBranch(40596, 10874, 1);
# 4161 "regex.c"
            goto fail;
          } else {
            __CrestBranch(40597, 10875, 0);

          }
          }
# 4161 "regex.c"
          d = string2;
# 4161 "regex.c"
          dend = end_match_2;
        }
        while_break___12: ;
        }
# 4162 "regex.c"
        tmp___25 = d;
# 4162 "regex.c"
        d ++;
# 4162 "regex.c"
        tmp___26 = p;
# 4162 "regex.c"
        p ++;
        {
# 4162 "regex.c"
        mem_250 = translate + (unsigned char )*tmp___25;
        {
        __CrestLoad(40600, (unsigned long )mem_250, (long long )*mem_250);
        __CrestLoad(40599, (unsigned long )tmp___26, (long long )*tmp___26);
        __CrestApply2(40598, 13, (long long )((int )((unsigned char )*mem_250) != (int )*tmp___26));
# 4162 "regex.c"
        if ((int )((unsigned char )*mem_250) != (int )*tmp___26) {
          __CrestBranch(40601, 10882, 1);
# 4164 "regex.c"
          goto fail;
        } else {
          __CrestBranch(40602, 10883, 0);

        }
        }
        }
        __CrestLoad(40605, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(40604, (unsigned long )0, (long long )1);
        __CrestApply2(40603, 1, (long long )(mcnt - 1));
        __CrestStore(40606, (unsigned long )(& mcnt));
# 4159 "regex.c"
        mcnt --;
        {
        __CrestLoad(40609, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(40608, (unsigned long )0, (long long )0);
        __CrestApply2(40607, 13, (long long )(mcnt != 0));
# 4159 "regex.c"
        if (mcnt != 0) {
          __CrestBranch(40610, 10886, 1);

        } else {
          __CrestBranch(40611, 10887, 0);
# 4159 "regex.c"
          goto while_break___11;
        }
        }
      }
      while_break___11: ;
      }
    } else {
      __CrestBranch(40587, 10889, 0);
      {
# 4170 "regex.c"
      while (1) {
        while_continue___13: ;
        {
# 4172 "regex.c"
        while (1) {
          while_continue___14: ;
          {
          __CrestLoad(40614, (unsigned long )(& d), (long long )((unsigned long )d));
          __CrestLoad(40613, (unsigned long )(& dend), (long long )((unsigned long )dend));
          __CrestApply2(40612, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 4172 "regex.c"
          if ((unsigned long )d == (unsigned long )dend) {
            __CrestBranch(40615, 10896, 1);

          } else {
            __CrestBranch(40616, 10897, 0);
# 4172 "regex.c"
            goto while_break___14;
          }
          }
          {
          __CrestLoad(40619, (unsigned long )(& dend), (long long )((unsigned long )dend));
          __CrestLoad(40618, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
          __CrestApply2(40617, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 4172 "regex.c"
          if ((unsigned long )dend == (unsigned long )end_match_2) {
            __CrestBranch(40620, 10899, 1);
# 4172 "regex.c"
            goto fail;
          } else {
            __CrestBranch(40621, 10900, 0);

          }
          }
# 4172 "regex.c"
          d = string2;
# 4172 "regex.c"
          dend = end_match_2;
        }
        while_break___14: ;
        }
# 4173 "regex.c"
        tmp___27 = d;
# 4173 "regex.c"
        d ++;
# 4173 "regex.c"
        tmp___28 = p;
# 4173 "regex.c"
        p ++;
        {
        __CrestLoad(40624, (unsigned long )tmp___27, (long long )*tmp___27);
        __CrestLoad(40623, (unsigned long )tmp___28, (long long )*tmp___28);
        __CrestApply2(40622, 13, (long long )((int const )*tmp___27 != (int const )((char )*tmp___28)));
# 4173 "regex.c"
        if ((int const )*tmp___27 != (int const )((char )*tmp___28)) {
          __CrestBranch(40625, 10905, 1);
# 4173 "regex.c"
          goto fail;
        } else {
          __CrestBranch(40626, 10906, 0);

        }
        }
        __CrestLoad(40629, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(40628, (unsigned long )0, (long long )1);
        __CrestApply2(40627, 1, (long long )(mcnt - 1));
        __CrestStore(40630, (unsigned long )(& mcnt));
# 4170 "regex.c"
        mcnt --;
        {
        __CrestLoad(40633, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(40632, (unsigned long )0, (long long )0);
        __CrestApply2(40631, 13, (long long )(mcnt != 0));
# 4170 "regex.c"
        if (mcnt != 0) {
          __CrestBranch(40634, 10909, 1);

        } else {
          __CrestBranch(40635, 10910, 0);
# 4170 "regex.c"
          goto while_break___13;
        }
        }
      }
      while_break___13: ;
      }
    }
    }
    {
# 4177 "regex.c"
    while (1) {
      while_continue___15: ;
      {
      __CrestLoad(40638, (unsigned long )(& set_regs_matched_done), (long long )set_regs_matched_done);
      __CrestLoad(40637, (unsigned long )0, (long long )0);
      __CrestApply2(40636, 12, (long long )(set_regs_matched_done == 0));
# 4177 "regex.c"
      if (set_regs_matched_done == 0) {
        __CrestBranch(40639, 10916, 1);
        __CrestLoad(40641, (unsigned long )0, (long long )1);
        __CrestStore(40642, (unsigned long )(& set_regs_matched_done));
# 4177 "regex.c"
        set_regs_matched_done = 1;
        __CrestLoad(40643, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestStore(40644, (unsigned long )(& r));
# 4177 "regex.c"
        r = lowest_active_reg;
        {
# 4177 "regex.c"
        while (1) {
          while_continue___16: ;
          {
          __CrestLoad(40647, (unsigned long )(& r), (long long )r);
          __CrestLoad(40646, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
          __CrestApply2(40645, 15, (long long )(r <= highest_active_reg));
# 4177 "regex.c"
          if (r <= highest_active_reg) {
            __CrestBranch(40648, 10921, 1);

          } else {
            __CrestBranch(40649, 10922, 0);
# 4177 "regex.c"
            goto while_break___16;
          }
          }
          __CrestLoad(40650, (unsigned long )0, (long long )1U);
          __CrestStore(40651, (unsigned long )(& tmp___29));
# 4177 "regex.c"
          tmp___29 = 1U;
# 4177 "regex.c"
          mem_251 = reg_info + r;
# 4177 "regex.c"
          mem_251->bits.ever_matched_something = tmp___29;
# 4177 "regex.c"
          mem_252 = reg_info + r;
# 4177 "regex.c"
          mem_252->bits.matched_something = tmp___29;
          __CrestLoad(40654, (unsigned long )(& r), (long long )r);
          __CrestLoad(40653, (unsigned long )0, (long long )1UL);
          __CrestApply2(40652, 0, (long long )(r + 1UL));
          __CrestStore(40655, (unsigned long )(& r));
# 4177 "regex.c"
          r ++;
        }
        while_break___16: ;
        }
      } else {
        __CrestBranch(40640, 10925, 0);

      }
      }
# 4177 "regex.c"
      goto while_break___15;
    }
    while_break___15: ;
    }
# 4178 "regex.c"
    goto switch_break;
    case_3: ;
    {
# 4185 "regex.c"
    while (1) {
      while_continue___17: ;
      {
      __CrestLoad(40658, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(40657, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestApply2(40656, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 4185 "regex.c"
      if ((unsigned long )d == (unsigned long )dend) {
        __CrestBranch(40659, 10934, 1);

      } else {
        __CrestBranch(40660, 10935, 0);
# 4185 "regex.c"
        goto while_break___17;
      }
      }
      {
      __CrestLoad(40663, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestLoad(40662, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
      __CrestApply2(40661, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 4185 "regex.c"
      if ((unsigned long )dend == (unsigned long )end_match_2) {
        __CrestBranch(40664, 10937, 1);
# 4185 "regex.c"
        goto fail;
      } else {
        __CrestBranch(40665, 10938, 0);

      }
      }
# 4185 "regex.c"
      d = string2;
# 4185 "regex.c"
      dend = end_match_2;
    }
    while_break___17: ;
    }
    {
    __CrestLoad(40670, (unsigned long )(& bufp->syntax), (long long )bufp->syntax);
    __CrestLoad(40669, (unsigned long )0, (long long )((((((1UL << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(40668, 5, (long long )(bufp->syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(40667, (unsigned long )0, (long long )0);
    __CrestApply2(40666, 12, (long long )((bufp->syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 4187 "regex.c"
    if ((bufp->syntax & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(40671, 10942, 1);
      {
      __CrestLoad(40675, (unsigned long )(& translate), (long long )((unsigned long )translate));
      __CrestLoad(40674, (unsigned long )0, (long long )0);
      __CrestApply2(40673, 13, (long long )(translate != 0));
# 4187 "regex.c"
      if (translate != 0) {
        __CrestBranch(40676, 10943, 1);
# 4187 "regex.c"
        mem_253 = translate + (unsigned char )*d;
        __CrestLoad(40678, (unsigned long )mem_253, (long long )*mem_253);
        __CrestStore(40679, (unsigned long )(& tmp___30));
# 4187 "regex.c"
        tmp___30 = (int )*mem_253;
      } else {
        __CrestBranch(40677, 10944, 0);
        __CrestLoad(40680, (unsigned long )d, (long long )*d);
        __CrestStore(40681, (unsigned long )(& tmp___30));
# 4187 "regex.c"
        tmp___30 = (int )*d;
      }
      }
      {
      __CrestLoad(40684, (unsigned long )(& tmp___30), (long long )tmp___30);
      __CrestLoad(40683, (unsigned long )0, (long long )10);
      __CrestApply2(40682, 12, (long long )(tmp___30 == 10));
# 4187 "regex.c"
      if (tmp___30 == 10) {
        __CrestBranch(40685, 10946, 1);
# 4189 "regex.c"
        goto fail;
      } else {
        __CrestBranch(40686, 10947, 0);
# 4187 "regex.c"
        goto _L___11;
      }
      }
    } else {
      __CrestBranch(40672, 10948, 0);
      _L___11:
      {
      __CrestLoad(40691, (unsigned long )(& bufp->syntax), (long long )bufp->syntax);
      __CrestLoad(40690, (unsigned long )0, (long long )(((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(40689, 5, (long long )(bufp->syntax & (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(40688, (unsigned long )0, (long long )0);
      __CrestApply2(40687, 13, (long long )((bufp->syntax & (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 4187 "regex.c"
      if ((bufp->syntax & (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(40692, 10949, 1);
        {
        __CrestLoad(40696, (unsigned long )(& translate), (long long )((unsigned long )translate));
        __CrestLoad(40695, (unsigned long )0, (long long )0);
        __CrestApply2(40694, 13, (long long )(translate != 0));
# 4187 "regex.c"
        if (translate != 0) {
          __CrestBranch(40697, 10950, 1);
# 4187 "regex.c"
          mem_254 = translate + (unsigned char )*d;
          __CrestLoad(40699, (unsigned long )mem_254, (long long )*mem_254);
          __CrestStore(40700, (unsigned long )(& tmp___31));
# 4187 "regex.c"
          tmp___31 = (int )*mem_254;
        } else {
          __CrestBranch(40698, 10951, 0);
          __CrestLoad(40701, (unsigned long )d, (long long )*d);
          __CrestStore(40702, (unsigned long )(& tmp___31));
# 4187 "regex.c"
          tmp___31 = (int )*d;
        }
        }
        {
        __CrestLoad(40705, (unsigned long )(& tmp___31), (long long )tmp___31);
        __CrestLoad(40704, (unsigned long )0, (long long )0);
        __CrestApply2(40703, 12, (long long )(tmp___31 == 0));
# 4187 "regex.c"
        if (tmp___31 == 0) {
          __CrestBranch(40706, 10953, 1);
# 4189 "regex.c"
          goto fail;
        } else {
          __CrestBranch(40707, 10954, 0);

        }
        }
      } else {
        __CrestBranch(40693, 10955, 0);

      }
      }
    }
    }
    {
# 4191 "regex.c"
    while (1) {
      while_continue___18: ;
      {
      __CrestLoad(40710, (unsigned long )(& set_regs_matched_done), (long long )set_regs_matched_done);
      __CrestLoad(40709, (unsigned long )0, (long long )0);
      __CrestApply2(40708, 12, (long long )(set_regs_matched_done == 0));
# 4191 "regex.c"
      if (set_regs_matched_done == 0) {
        __CrestBranch(40711, 10960, 1);
        __CrestLoad(40713, (unsigned long )0, (long long )1);
        __CrestStore(40714, (unsigned long )(& set_regs_matched_done));
# 4191 "regex.c"
        set_regs_matched_done = 1;
        __CrestLoad(40715, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestStore(40716, (unsigned long )(& r___0));
# 4191 "regex.c"
        r___0 = lowest_active_reg;
        {
# 4191 "regex.c"
        while (1) {
          while_continue___19: ;
          {
          __CrestLoad(40719, (unsigned long )(& r___0), (long long )r___0);
          __CrestLoad(40718, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
          __CrestApply2(40717, 15, (long long )(r___0 <= highest_active_reg));
# 4191 "regex.c"
          if (r___0 <= highest_active_reg) {
            __CrestBranch(40720, 10965, 1);

          } else {
            __CrestBranch(40721, 10966, 0);
# 4191 "regex.c"
            goto while_break___19;
          }
          }
          __CrestLoad(40722, (unsigned long )0, (long long )1U);
          __CrestStore(40723, (unsigned long )(& tmp___32));
# 4191 "regex.c"
          tmp___32 = 1U;
# 4191 "regex.c"
          mem_255 = reg_info + r___0;
# 4191 "regex.c"
          mem_255->bits.ever_matched_something = tmp___32;
# 4191 "regex.c"
          mem_256 = reg_info + r___0;
# 4191 "regex.c"
          mem_256->bits.matched_something = tmp___32;
          __CrestLoad(40726, (unsigned long )(& r___0), (long long )r___0);
          __CrestLoad(40725, (unsigned long )0, (long long )1UL);
          __CrestApply2(40724, 0, (long long )(r___0 + 1UL));
          __CrestStore(40727, (unsigned long )(& r___0));
# 4191 "regex.c"
          r___0 ++;
        }
        while_break___19: ;
        }
      } else {
        __CrestBranch(40712, 10969, 0);

      }
      }
# 4191 "regex.c"
      goto while_break___18;
    }
    while_break___18: ;
    }
# 4193 "regex.c"
    d ++;
# 4194 "regex.c"
    goto switch_break;
    case_5:
    case_4:
# 4201 "regex.c"
    mem_257 = p - 1;
    __CrestLoad(40730, (unsigned long )mem_257, (long long )*mem_257);
    __CrestLoad(40729, (unsigned long )0, (long long )5U);
    __CrestApply2(40728, 12, (long long )((unsigned int )((re_opcode_t )*mem_257) == 5U));
    __CrestStore(40731, (unsigned long )(& not));
# 4201 "regex.c"
    not = (boolean )((unsigned int )((re_opcode_t )*mem_257) == 5U);
    {
# 4205 "regex.c"
    while (1) {
      while_continue___20: ;
      {
      __CrestLoad(40734, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(40733, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestApply2(40732, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 4205 "regex.c"
      if ((unsigned long )d == (unsigned long )dend) {
        __CrestBranch(40735, 10979, 1);

      } else {
        __CrestBranch(40736, 10980, 0);
# 4205 "regex.c"
        goto while_break___20;
      }
      }
      {
      __CrestLoad(40739, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestLoad(40738, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
      __CrestApply2(40737, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 4205 "regex.c"
      if ((unsigned long )dend == (unsigned long )end_match_2) {
        __CrestBranch(40740, 10982, 1);
# 4205 "regex.c"
        goto fail;
      } else {
        __CrestBranch(40741, 10983, 0);

      }
      }
# 4205 "regex.c"
      d = string2;
# 4205 "regex.c"
      dend = end_match_2;
    }
    while_break___20: ;
    }
    {
    __CrestLoad(40744, (unsigned long )(& translate), (long long )((unsigned long )translate));
    __CrestLoad(40743, (unsigned long )0, (long long )0);
    __CrestApply2(40742, 13, (long long )(translate != 0));
# 4206 "regex.c"
    if (translate != 0) {
      __CrestBranch(40745, 10987, 1);
# 4206 "regex.c"
      mem_258 = translate + (unsigned char )*d;
      __CrestLoad(40747, (unsigned long )mem_258, (long long )*mem_258);
      __CrestStore(40748, (unsigned long )(& c));
# 4206 "regex.c"
      c = (unsigned char )*mem_258;
    } else {
      __CrestBranch(40746, 10988, 0);
      __CrestLoad(40749, (unsigned long )d, (long long )*d);
      __CrestStore(40750, (unsigned long )(& c));
# 4206 "regex.c"
      c = (unsigned char )*d;
    }
    }
    {
    __CrestLoad(40755, (unsigned long )(& c), (long long )c);
    __CrestLoad(40754, (unsigned long )p, (long long )*p);
    __CrestLoad(40753, (unsigned long )0, (long long )8);
    __CrestApply2(40752, 2, (long long )((int )*p * 8));
    __CrestApply2(40751, 16, (long long )((unsigned int )c < (unsigned int )((int )*p * 8)));
# 4210 "regex.c"
    if ((unsigned int )c < (unsigned int )((int )*p * 8)) {
      __CrestBranch(40756, 10990, 1);
      {
# 4210 "regex.c"
      mem_259 = p + (1 + (int )c / 8);
      {
      __CrestLoad(40766, (unsigned long )mem_259, (long long )*mem_259);
      __CrestLoad(40765, (unsigned long )0, (long long )1);
      __CrestLoad(40764, (unsigned long )(& c), (long long )c);
      __CrestLoad(40763, (unsigned long )0, (long long )8);
      __CrestApply2(40762, 4, (long long )((int )c % 8));
      __CrestApply2(40761, 8, (long long )(1 << (int )c % 8));
      __CrestApply2(40760, 5, (long long )((int )*mem_259 & (1 << (int )c % 8)));
      __CrestLoad(40759, (unsigned long )0, (long long )0);
      __CrestApply2(40758, 13, (long long )(((int )*mem_259 & (1 << (int )c % 8)) != 0));
# 4210 "regex.c"
      if (((int )*mem_259 & (1 << (int )c % 8)) != 0) {
        __CrestBranch(40767, 10993, 1);
        __CrestLoad(40770, (unsigned long )(& not), (long long )not);
        __CrestApply1(40769, 21, (long long )(! not));
        __CrestStore(40771, (unsigned long )(& not));
# 4212 "regex.c"
        not = (boolean )(! not);
      } else {
        __CrestBranch(40768, 10994, 0);

      }
      }
      }
    } else {
      __CrestBranch(40757, 10995, 0);

    }
    }
# 4214 "regex.c"
    p += 1 + (int )*p;
    {
    __CrestLoad(40774, (unsigned long )(& not), (long long )not);
    __CrestLoad(40773, (unsigned long )0, (long long )0);
    __CrestApply2(40772, 12, (long long )(not == 0));
# 4216 "regex.c"
    if (not == 0) {
      __CrestBranch(40775, 10998, 1);
# 4216 "regex.c"
      goto fail;
    } else {
      __CrestBranch(40776, 10999, 0);

    }
    }
    {
# 4218 "regex.c"
    while (1) {
      while_continue___21: ;
      {
      __CrestLoad(40779, (unsigned long )(& set_regs_matched_done), (long long )set_regs_matched_done);
      __CrestLoad(40778, (unsigned long )0, (long long )0);
      __CrestApply2(40777, 12, (long long )(set_regs_matched_done == 0));
# 4218 "regex.c"
      if (set_regs_matched_done == 0) {
        __CrestBranch(40780, 11004, 1);
        __CrestLoad(40782, (unsigned long )0, (long long )1);
        __CrestStore(40783, (unsigned long )(& set_regs_matched_done));
# 4218 "regex.c"
        set_regs_matched_done = 1;
        __CrestLoad(40784, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestStore(40785, (unsigned long )(& r___1));
# 4218 "regex.c"
        r___1 = lowest_active_reg;
        {
# 4218 "regex.c"
        while (1) {
          while_continue___22: ;
          {
          __CrestLoad(40788, (unsigned long )(& r___1), (long long )r___1);
          __CrestLoad(40787, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
          __CrestApply2(40786, 15, (long long )(r___1 <= highest_active_reg));
# 4218 "regex.c"
          if (r___1 <= highest_active_reg) {
            __CrestBranch(40789, 11009, 1);

          } else {
            __CrestBranch(40790, 11010, 0);
# 4218 "regex.c"
            goto while_break___22;
          }
          }
          __CrestLoad(40791, (unsigned long )0, (long long )1U);
          __CrestStore(40792, (unsigned long )(& tmp___33));
# 4218 "regex.c"
          tmp___33 = 1U;
# 4218 "regex.c"
          mem_260 = reg_info + r___1;
# 4218 "regex.c"
          mem_260->bits.ever_matched_something = tmp___33;
# 4218 "regex.c"
          mem_261 = reg_info + r___1;
# 4218 "regex.c"
          mem_261->bits.matched_something = tmp___33;
          __CrestLoad(40795, (unsigned long )(& r___1), (long long )r___1);
          __CrestLoad(40794, (unsigned long )0, (long long )1UL);
          __CrestApply2(40793, 0, (long long )(r___1 + 1UL));
          __CrestStore(40796, (unsigned long )(& r___1));
# 4218 "regex.c"
          r___1 ++;
        }
        while_break___22: ;
        }
      } else {
        __CrestBranch(40781, 11013, 0);

      }
      }
# 4218 "regex.c"
      goto while_break___21;
    }
    while_break___21: ;
    }
# 4219 "regex.c"
    d ++;
# 4220 "regex.c"
    goto switch_break;
    case_6:
# 4233 "regex.c"
    p1 = p;
    {
# 4235 "regex.c"
    mem_262 = reg_info + *p;
    {
    __CrestLoad(40799, (unsigned long )0, (long long )mem_262->bits.match_null_string_p);
    __CrestLoad(40798, (unsigned long )0, (long long )3U);
    __CrestApply2(40797, 12, (long long )(mem_262->bits.match_null_string_p == 3U));
# 4235 "regex.c"
    if (mem_262->bits.match_null_string_p == 3U) {
      __CrestBranch(40800, 11022, 1);
# 4236 "regex.c"
      tmp___34 = group_match_null_string_p(& p1, pend, reg_info);
      __CrestHandleReturn(40803, (long long )tmp___34);
      __CrestStore(40802, (unsigned long )(& tmp___34));
# 4236 "regex.c"
      mem_263 = reg_info + *p;
# 4236 "regex.c"
      mem_263->bits.match_null_string_p = (unsigned int )tmp___34;
    } else {
      __CrestBranch(40801, 11023, 0);

    }
    }
    }
    {
# 4244 "regex.c"
    mem_264 = reg_info + *p;
    {
    __CrestLoad(40806, (unsigned long )0, (long long )mem_264->bits.match_null_string_p);
    __CrestLoad(40805, (unsigned long )0, (long long )0);
    __CrestApply2(40804, 13, (long long )(mem_264->bits.match_null_string_p != 0));
# 4244 "regex.c"
    if (mem_264->bits.match_null_string_p != 0) {
      __CrestBranch(40807, 11027, 1);
      {
# 4244 "regex.c"
      mem_265 = regstart + *p;
      {
      __CrestLoad(40811, (unsigned long )mem_265, (long long )((unsigned long )*mem_265));
      __CrestLoad(40810, (unsigned long )0, (long long )((unsigned long )(& reg_unset_dummy)));
      __CrestApply2(40809, 12, (long long )((unsigned long )*mem_265 == (unsigned long )(& reg_unset_dummy)));
# 4244 "regex.c"
      if ((unsigned long )*mem_265 == (unsigned long )(& reg_unset_dummy)) {
        __CrestBranch(40812, 11030, 1);
# 4244 "regex.c"
        tmp___35 = d;
      } else {
        __CrestBranch(40813, 11031, 0);
# 4244 "regex.c"
        mem_266 = regstart + *p;
# 4244 "regex.c"
        tmp___35 = *mem_266;
      }
      }
      }
# 4244 "regex.c"
      mem_267 = old_regstart + *p;
# 4244 "regex.c"
      *mem_267 = tmp___35;
    } else {
      __CrestBranch(40808, 11033, 0);
# 4244 "regex.c"
      mem_268 = old_regstart + *p;
# 4244 "regex.c"
      mem_269 = regstart + *p;
# 4244 "regex.c"
      *mem_268 = *mem_269;
    }
    }
    }
# 4250 "regex.c"
    mem_270 = regstart + *p;
# 4250 "regex.c"
    *mem_270 = d;
# 4253 "regex.c"
    mem_271 = reg_info + *p;
# 4253 "regex.c"
    mem_271->bits.is_active = 1U;
# 4254 "regex.c"
    mem_272 = reg_info + *p;
# 4254 "regex.c"
    mem_272->bits.matched_something = 0U;
    __CrestLoad(40814, (unsigned long )0, (long long )0);
    __CrestStore(40815, (unsigned long )(& set_regs_matched_done));
# 4257 "regex.c"
    set_regs_matched_done = 0;
    __CrestLoad(40816, (unsigned long )p, (long long )*p);
    __CrestStore(40817, (unsigned long )(& highest_active_reg));
# 4260 "regex.c"
    highest_active_reg = (active_reg_t )*p;
    {
    __CrestLoad(40820, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
    __CrestLoad(40819, (unsigned long )0, (long long )((active_reg_t )((1 << 8) + 1)));
    __CrestApply2(40818, 12, (long long )(lowest_active_reg == (active_reg_t )((1 << 8) + 1)));
# 4264 "regex.c"
    if (lowest_active_reg == (active_reg_t )((1 << 8) + 1)) {
      __CrestBranch(40821, 11036, 1);
      __CrestLoad(40823, (unsigned long )p, (long long )*p);
      __CrestStore(40824, (unsigned long )(& lowest_active_reg));
# 4265 "regex.c"
      lowest_active_reg = (active_reg_t )*p;
    } else {
      __CrestBranch(40822, 11037, 0);

    }
    }
# 4268 "regex.c"
    p += 2;
# 4269 "regex.c"
    just_past_start_mem = p;
# 4271 "regex.c"
    goto switch_break;
    case_7: ;
    {
# 4285 "regex.c"
    mem_273 = reg_info + *p;
    {
    __CrestLoad(40827, (unsigned long )0, (long long )mem_273->bits.match_null_string_p);
    __CrestLoad(40826, (unsigned long )0, (long long )0);
    __CrestApply2(40825, 13, (long long )(mem_273->bits.match_null_string_p != 0));
# 4285 "regex.c"
    if (mem_273->bits.match_null_string_p != 0) {
      __CrestBranch(40828, 11044, 1);
      {
# 4285 "regex.c"
      mem_274 = regend + *p;
      {
      __CrestLoad(40832, (unsigned long )mem_274, (long long )((unsigned long )*mem_274));
      __CrestLoad(40831, (unsigned long )0, (long long )((unsigned long )(& reg_unset_dummy)));
      __CrestApply2(40830, 12, (long long )((unsigned long )*mem_274 == (unsigned long )(& reg_unset_dummy)));
# 4285 "regex.c"
      if ((unsigned long )*mem_274 == (unsigned long )(& reg_unset_dummy)) {
        __CrestBranch(40833, 11047, 1);
# 4285 "regex.c"
        tmp___36 = d;
      } else {
        __CrestBranch(40834, 11048, 0);
# 4285 "regex.c"
        mem_275 = regend + *p;
# 4285 "regex.c"
        tmp___36 = *mem_275;
      }
      }
      }
# 4285 "regex.c"
      mem_276 = old_regend + *p;
# 4285 "regex.c"
      *mem_276 = tmp___36;
    } else {
      __CrestBranch(40829, 11050, 0);
# 4285 "regex.c"
      mem_277 = old_regend + *p;
# 4285 "regex.c"
      mem_278 = regend + *p;
# 4285 "regex.c"
      *mem_277 = *mem_278;
    }
    }
    }
# 4291 "regex.c"
    mem_279 = regend + *p;
# 4291 "regex.c"
    *mem_279 = d;
# 4295 "regex.c"
    mem_280 = reg_info + *p;
# 4295 "regex.c"
    mem_280->bits.is_active = 0U;
    __CrestLoad(40835, (unsigned long )0, (long long )0);
    __CrestStore(40836, (unsigned long )(& set_regs_matched_done));
# 4298 "regex.c"
    set_regs_matched_done = 0;
    {
    __CrestLoad(40839, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
    __CrestLoad(40838, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
    __CrestApply2(40837, 12, (long long )(lowest_active_reg == highest_active_reg));
# 4302 "regex.c"
    if (lowest_active_reg == highest_active_reg) {
      __CrestBranch(40840, 11053, 1);
      __CrestLoad(40842, (unsigned long )0, (long long )((active_reg_t )((1 << 8) + 1)));
      __CrestStore(40843, (unsigned long )(& lowest_active_reg));
# 4304 "regex.c"
      lowest_active_reg = (active_reg_t )((1 << 8) + 1);
      __CrestLoad(40844, (unsigned long )0, (long long )((active_reg_t )(1 << 8)));
      __CrestStore(40845, (unsigned long )(& highest_active_reg));
# 4305 "regex.c"
      highest_active_reg = (active_reg_t )(1 << 8);
    } else {
      __CrestBranch(40841, 11054, 0);
      __CrestLoad(40848, (unsigned long )p, (long long )*p);
      __CrestLoad(40847, (unsigned long )0, (long long )1);
      __CrestApply2(40846, 1, (long long )((int )*p - 1));
      __CrestStore(40849, (unsigned long )(& r___2));
# 4312 "regex.c"
      r___2 = (unsigned char )((int )*p - 1);
      {
# 4313 "regex.c"
      while (1) {
        while_continue___23: ;
        {
        __CrestLoad(40852, (unsigned long )(& r___2), (long long )r___2);
        __CrestLoad(40851, (unsigned long )0, (long long )0);
        __CrestApply2(40850, 14, (long long )((int )r___2 > 0));
# 4313 "regex.c"
        if ((int )r___2 > 0) {
          __CrestBranch(40853, 11059, 1);
          {
# 4313 "regex.c"
          mem_281 = reg_info + r___2;
          {
          __CrestLoad(40857, (unsigned long )0, (long long )mem_281->bits.is_active);
          __CrestLoad(40856, (unsigned long )0, (long long )0);
          __CrestApply2(40855, 12, (long long )(mem_281->bits.is_active == 0));
# 4313 "regex.c"
          if (mem_281->bits.is_active == 0) {
            __CrestBranch(40858, 11062, 1);

          } else {
            __CrestBranch(40859, 11063, 0);
# 4313 "regex.c"
            goto while_break___23;
          }
          }
          }
        } else {
          __CrestBranch(40854, 11064, 0);
# 4313 "regex.c"
          goto while_break___23;
        }
        }
        __CrestLoad(40862, (unsigned long )(& r___2), (long long )r___2);
        __CrestLoad(40861, (unsigned long )0, (long long )1);
        __CrestApply2(40860, 1, (long long )((int )r___2 - 1));
        __CrestStore(40863, (unsigned long )(& r___2));
# 4314 "regex.c"
        r___2 = (unsigned char )((int )r___2 - 1);
      }
      while_break___23: ;
      }
      {
      __CrestLoad(40866, (unsigned long )(& r___2), (long long )r___2);
      __CrestLoad(40865, (unsigned long )0, (long long )0);
      __CrestApply2(40864, 12, (long long )((int )r___2 == 0));
# 4323 "regex.c"
      if ((int )r___2 == 0) {
        __CrestBranch(40867, 11068, 1);
        __CrestLoad(40869, (unsigned long )0, (long long )((active_reg_t )((1 << 8) + 1)));
        __CrestStore(40870, (unsigned long )(& lowest_active_reg));
# 4325 "regex.c"
        lowest_active_reg = (active_reg_t )((1 << 8) + 1);
        __CrestLoad(40871, (unsigned long )0, (long long )((active_reg_t )(1 << 8)));
        __CrestStore(40872, (unsigned long )(& highest_active_reg));
# 4326 "regex.c"
        highest_active_reg = (active_reg_t )(1 << 8);
      } else {
        __CrestBranch(40868, 11069, 0);
        __CrestLoad(40873, (unsigned long )(& r___2), (long long )r___2);
        __CrestStore(40874, (unsigned long )(& highest_active_reg));
# 4329 "regex.c"
        highest_active_reg = (active_reg_t )r___2;
      }
      }
    }
    }
    {
# 4337 "regex.c"
    mem_282 = reg_info + *p;
    {
    __CrestLoad(40877, (unsigned long )0, (long long )mem_282->bits.matched_something);
    __CrestLoad(40876, (unsigned long )0, (long long )0);
    __CrestApply2(40875, 12, (long long )(mem_282->bits.matched_something == 0));
# 4337 "regex.c"
    if (mem_282->bits.matched_something == 0) {
      __CrestBranch(40878, 11073, 1);
# 4337 "regex.c"
      goto _L___12;
    } else {
      __CrestBranch(40879, 11074, 0);
      {
      __CrestLoad(40884, (unsigned long )(& just_past_start_mem), (long long )((unsigned long )just_past_start_mem));
      __CrestLoad(40883, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(40882, (unsigned long )0, (long long )1);
      __CrestApply2(40881, 18, (long long )((unsigned long )(p - 1)));
      __CrestApply2(40880, 12, (long long )((unsigned long )just_past_start_mem == (unsigned long )(p - 1)));
# 4337 "regex.c"
      if ((unsigned long )just_past_start_mem == (unsigned long )(p - 1)) {
        __CrestBranch(40885, 11075, 1);
        _L___12:
        {
        __CrestLoad(40891, (unsigned long )(& p), (long long )((unsigned long )p));
        __CrestLoad(40890, (unsigned long )0, (long long )2);
        __CrestApply2(40889, 18, (long long )((unsigned long )(p + 2)));
        __CrestLoad(40888, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(40887, 16, (long long )((unsigned long )(p + 2) < (unsigned long )pend));
# 4337 "regex.c"
        if ((unsigned long )(p + 2) < (unsigned long )pend) {
          __CrestBranch(40892, 11076, 1);
          __CrestLoad(40894, (unsigned long )0, (long long )((boolean )0));
          __CrestStore(40895, (unsigned long )(& is_a_jump_n));
# 4341 "regex.c"
          is_a_jump_n = (boolean )0;
# 4343 "regex.c"
          p1 = p + 2;
          __CrestLoad(40896, (unsigned long )0, (long long )0);
          __CrestStore(40897, (unsigned long )(& mcnt));
# 4344 "regex.c"
          mcnt = 0;
# 4345 "regex.c"
          tmp___37 = p1;
# 4345 "regex.c"
          p1 ++;
          {
          {
          __CrestLoad(40900, (unsigned long )tmp___37, (long long )*tmp___37);
          __CrestLoad(40899, (unsigned long )0, (long long )22U);
          __CrestApply2(40898, 12, (long long )((unsigned int )((re_opcode_t )*tmp___37) == 22U));
# 4347 "regex.c"
          if ((unsigned int )((re_opcode_t )*tmp___37) == 22U) {
            __CrestBranch(40901, 11079, 1);
# 4347 "regex.c"
            goto case_22;
          } else {
            __CrestBranch(40902, 11080, 0);

          }
          }
          {
          __CrestLoad(40905, (unsigned long )tmp___37, (long long )*tmp___37);
          __CrestLoad(40904, (unsigned long )0, (long long )19U);
          __CrestApply2(40903, 12, (long long )((unsigned int )((re_opcode_t )*tmp___37) == 19U));
# 4352 "regex.c"
          if ((unsigned int )((re_opcode_t )*tmp___37) == 19U) {
            __CrestBranch(40906, 11082, 1);
# 4352 "regex.c"
            goto case_19;
          } else {
            __CrestBranch(40907, 11083, 0);

          }
          }
          {
          __CrestLoad(40910, (unsigned long )tmp___37, (long long )*tmp___37);
          __CrestLoad(40909, (unsigned long )0, (long long )13U);
          __CrestApply2(40908, 12, (long long )((unsigned int )((re_opcode_t )*tmp___37) == 13U));
# 4352 "regex.c"
          if ((unsigned int )((re_opcode_t )*tmp___37) == 13U) {
            __CrestBranch(40911, 11085, 1);
# 4352 "regex.c"
            goto case_19;
          } else {
            __CrestBranch(40912, 11086, 0);

          }
          }
          {
          __CrestLoad(40915, (unsigned long )tmp___37, (long long )*tmp___37);
          __CrestLoad(40914, (unsigned long )0, (long long )18U);
          __CrestApply2(40913, 12, (long long )((unsigned int )((re_opcode_t )*tmp___37) == 18U));
# 4352 "regex.c"
          if ((unsigned int )((re_opcode_t )*tmp___37) == 18U) {
            __CrestBranch(40916, 11088, 1);
# 4352 "regex.c"
            goto case_19;
          } else {
            __CrestBranch(40917, 11089, 0);

          }
          }
          {
          __CrestLoad(40920, (unsigned long )tmp___37, (long long )*tmp___37);
          __CrestLoad(40919, (unsigned long )0, (long long )17U);
          __CrestApply2(40918, 12, (long long )((unsigned int )((re_opcode_t )*tmp___37) == 17U));
# 4352 "regex.c"
          if ((unsigned int )((re_opcode_t )*tmp___37) == 17U) {
            __CrestBranch(40921, 11091, 1);
# 4352 "regex.c"
            goto case_19;
          } else {
            __CrestBranch(40922, 11092, 0);

          }
          }
# 4358 "regex.c"
          goto switch_default;
          case_22:
          __CrestLoad(40923, (unsigned long )0, (long long )((boolean )1));
          __CrestStore(40924, (unsigned long )(& is_a_jump_n));
# 4348 "regex.c"
          is_a_jump_n = (boolean )1;
          case_19:
          case_13:
          case_18:
          case_17:
          {
# 4353 "regex.c"
          while (1) {
            while_continue___24: ;
            {
# 4353 "regex.c"
            while (1) {
              while_continue___25: ;
              __CrestLoad(40927, (unsigned long )p1, (long long )*p1);
              __CrestLoad(40926, (unsigned long )0, (long long )255);
              __CrestApply2(40925, 5, (long long )((int )*p1 & 255));
              __CrestStore(40928, (unsigned long )(& mcnt));
# 4353 "regex.c"
              mcnt = (int )*p1 & 255;
# 4353 "regex.c"
              mem_283 = p1 + 1;
              __CrestLoad(40933, (unsigned long )(& mcnt), (long long )mcnt);
              __CrestLoad(40932, (unsigned long )mem_283, (long long )*mem_283);
              __CrestLoad(40931, (unsigned long )0, (long long )8);
              __CrestApply2(40930, 8, (long long )((int )((signed char )*mem_283) << 8));
              __CrestApply2(40929, 0, (long long )(mcnt + ((int )((signed char )*mem_283) << 8)));
              __CrestStore(40934, (unsigned long )(& mcnt));
# 4353 "regex.c"
              mcnt += (int )((signed char )*mem_283) << 8;
# 4353 "regex.c"
              goto while_break___25;
            }
            while_break___25: ;
            }
# 4353 "regex.c"
            p1 += 2;
# 4353 "regex.c"
            goto while_break___24;
          }
          while_break___24: ;
          }
          {
          __CrestLoad(40937, (unsigned long )(& is_a_jump_n), (long long )is_a_jump_n);
          __CrestLoad(40936, (unsigned long )0, (long long )0);
          __CrestApply2(40935, 13, (long long )(is_a_jump_n != 0));
# 4354 "regex.c"
          if (is_a_jump_n != 0) {
            __CrestBranch(40938, 11108, 1);
# 4355 "regex.c"
            p1 += 2;
          } else {
            __CrestBranch(40939, 11109, 0);

          }
          }
# 4356 "regex.c"
          goto switch_break___0;
          switch_default: ;
          switch_break___0: ;
          }
# 4361 "regex.c"
          p1 += mcnt;
          {
          __CrestLoad(40942, (unsigned long )(& mcnt), (long long )mcnt);
          __CrestLoad(40941, (unsigned long )0, (long long )0);
          __CrestApply2(40940, 16, (long long )(mcnt < 0));
# 4368 "regex.c"
          if (mcnt < 0) {
            __CrestBranch(40943, 11115, 1);
            {
            __CrestLoad(40947, (unsigned long )p1, (long long )*p1);
            __CrestLoad(40946, (unsigned long )0, (long long )15U);
            __CrestApply2(40945, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 15U));
# 4368 "regex.c"
            if ((unsigned int )((re_opcode_t )*p1) == 15U) {
              __CrestBranch(40948, 11116, 1);
              {
# 4368 "regex.c"
              mem_284 = p1 + 3;
              {
              __CrestLoad(40952, (unsigned long )mem_284, (long long )*mem_284);
              __CrestLoad(40951, (unsigned long )0, (long long )6U);
              __CrestApply2(40950, 12, (long long )((unsigned int )((re_opcode_t )*mem_284) == 6U));
# 4368 "regex.c"
              if ((unsigned int )((re_opcode_t )*mem_284) == 6U) {
                __CrestBranch(40953, 11119, 1);
                {
# 4368 "regex.c"
                mem_285 = p1 + 4;
                {
                __CrestLoad(40957, (unsigned long )mem_285, (long long )*mem_285);
                __CrestLoad(40956, (unsigned long )p, (long long )*p);
                __CrestApply2(40955, 12, (long long )((int )*mem_285 == (int )*p));
# 4368 "regex.c"
                if ((int )*mem_285 == (int )*p) {
                  __CrestBranch(40958, 11122, 1);
                  {
# 4381 "regex.c"
                  mem_286 = reg_info + *p;
                  {
                  __CrestLoad(40962, (unsigned long )0, (long long )mem_286->bits.ever_matched_something);
                  __CrestLoad(40961, (unsigned long )0, (long long )0);
                  __CrestApply2(40960, 13, (long long )(mem_286->bits.ever_matched_something != 0));
# 4381 "regex.c"
                  if (mem_286->bits.ever_matched_something != 0) {
                    __CrestBranch(40963, 11125, 1);
# 4385 "regex.c"
                    mem_287 = reg_info + *p;
# 4385 "regex.c"
                    mem_287->bits.ever_matched_something = 0U;
                    __CrestLoad(40965, (unsigned long )p, (long long )*p);
                    __CrestStore(40966, (unsigned long )(& r___3));
# 4388 "regex.c"
                    r___3 = (unsigned int )*p;
                    {
# 4388 "regex.c"
                    while (1) {
                      while_continue___26: ;
                      {
# 4388 "regex.c"
                      mem_288 = p + 1;
                      {
                      __CrestLoad(40971, (unsigned long )(& r___3), (long long )r___3);
                      __CrestLoad(40970, (unsigned long )p, (long long )*p);
                      __CrestLoad(40969, (unsigned long )mem_288, (long long )*mem_288);
                      __CrestApply2(40968, 0, (long long )((unsigned int )*p + (unsigned int )*mem_288));
                      __CrestApply2(40967, 16, (long long )(r___3 < (unsigned int )*p + (unsigned int )*mem_288));
# 4388 "regex.c"
                      if (r___3 < (unsigned int )*p + (unsigned int )*mem_288) {
                        __CrestBranch(40972, 11132, 1);

                      } else {
                        __CrestBranch(40973, 11133, 0);
# 4388 "regex.c"
                        goto while_break___26;
                      }
                      }
                      }
# 4391 "regex.c"
                      mem_289 = regstart + r___3;
# 4391 "regex.c"
                      mem_290 = old_regstart + r___3;
# 4391 "regex.c"
                      *mem_289 = *mem_290;
                      {
# 4394 "regex.c"
                      mem_291 = old_regend + r___3;
# 4394 "regex.c"
                      mem_292 = regstart + r___3;
                      {
                      __CrestLoad(40976, (unsigned long )mem_291, (long long )((unsigned long )*mem_291));
                      __CrestLoad(40975, (unsigned long )mem_292, (long long )((unsigned long )*mem_292));
                      __CrestApply2(40974, 17, (long long )((unsigned long )*mem_291 >= (unsigned long )*mem_292));
# 4394 "regex.c"
                      if ((unsigned long )*mem_291 >= (unsigned long )*mem_292) {
                        __CrestBranch(40977, 11138, 1);
# 4395 "regex.c"
                        mem_293 = regend + r___3;
# 4395 "regex.c"
                        mem_294 = old_regend + r___3;
# 4395 "regex.c"
                        *mem_293 = *mem_294;
                      } else {
                        __CrestBranch(40978, 11139, 0);

                      }
                      }
                      }
                      __CrestLoad(40981, (unsigned long )(& r___3), (long long )r___3);
                      __CrestLoad(40980, (unsigned long )0, (long long )1U);
                      __CrestApply2(40979, 0, (long long )(r___3 + 1U));
                      __CrestStore(40982, (unsigned long )(& r___3));
# 4388 "regex.c"
                      r___3 ++;
                    }
                    while_break___26: ;
                    }
                  } else {
                    __CrestBranch(40964, 11142, 0);

                  }
                  }
                  }
# 4398 "regex.c"
                  p1 ++;
                  {
# 4399 "regex.c"
                  while (1) {
                    while_continue___27: ;
                    {
# 4399 "regex.c"
                    while (1) {
                      while_continue___28: ;
                      __CrestLoad(40985, (unsigned long )p1, (long long )*p1);
                      __CrestLoad(40984, (unsigned long )0, (long long )255);
                      __CrestApply2(40983, 5, (long long )((int )*p1 & 255));
                      __CrestStore(40986, (unsigned long )(& mcnt));
# 4399 "regex.c"
                      mcnt = (int )*p1 & 255;
# 4399 "regex.c"
                      mem_295 = p1 + 1;
                      __CrestLoad(40991, (unsigned long )(& mcnt), (long long )mcnt);
                      __CrestLoad(40990, (unsigned long )mem_295, (long long )*mem_295);
                      __CrestLoad(40989, (unsigned long )0, (long long )8);
                      __CrestApply2(40988, 8, (long long )((int )((signed char )*mem_295) << 8));
                      __CrestApply2(40987, 0, (long long )(mcnt + ((int )((signed char )*mem_295) << 8)));
                      __CrestStore(40992, (unsigned long )(& mcnt));
# 4399 "regex.c"
                      mcnt += (int )((signed char )*mem_295) << 8;
# 4399 "regex.c"
                      goto while_break___28;
                    }
                    while_break___28: ;
                    }
# 4399 "regex.c"
                    p1 += 2;
# 4399 "regex.c"
                    goto while_break___27;
                  }
                  while_break___27: ;
                  }
                  {
# 4400 "regex.c"
                  while (1) {
                    while_continue___29: ;
                    {
# 4400 "regex.c"
                    while (1) {
                      while_continue___30: ;
                      {
                      __CrestLoad(41005, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
                      __CrestLoad(41004, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestApply2(41003, 1, (long long )(fail_stack.size - fail_stack.avail));
                      __CrestLoad(41002, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
                      __CrestLoad(41001, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
                      __CrestApply2(41000, 1, (long long )(highest_active_reg - lowest_active_reg));
                      __CrestLoad(40999, (unsigned long )0, (long long )1UL);
                      __CrestApply2(40998, 0, (long long )((highest_active_reg - lowest_active_reg) + 1UL));
                      __CrestLoad(40997, (unsigned long )0, (long long )3UL);
                      __CrestApply2(40996, 2, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL));
                      __CrestLoad(40995, (unsigned long )0, (long long )4UL);
                      __CrestApply2(40994, 0, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
                      __CrestApply2(40993, 16, (long long )((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
# 4400 "regex.c"
                      if ((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL) {
                        __CrestBranch(41006, 11163, 1);

                      } else {
                        __CrestBranch(41007, 11164, 0);
# 4400 "regex.c"
                        goto while_break___30;
                      }
                      }
                      {
                      __CrestLoad(41012, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
                      __CrestLoad(41011, (unsigned long )(& re_max_failures), (long long )re_max_failures);
                      __CrestLoad(41010, (unsigned long )0, (long long )19);
                      __CrestApply2(41009, 2, (long long )(re_max_failures * 19));
                      __CrestApply2(41008, 14, (long long )(fail_stack.size > (unsigned int )(re_max_failures * 19)));
# 4400 "regex.c"
                      if (fail_stack.size > (unsigned int )(re_max_failures * 19)) {
                        __CrestBranch(41013, 11166, 1);
                        __CrestLoad(41015, (unsigned long )0, (long long )0);
                        __CrestStore(41016, (unsigned long )(& tmp___40));
# 4400 "regex.c"
                        tmp___40 = 0;
                      } else {
                        __CrestBranch(41014, 11167, 0);
                        __CrestLoad(41021, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
                        __CrestLoad(41020, (unsigned long )0, (long long )1);
                        __CrestApply2(41019, 8, (long long )(fail_stack.size << 1));
                        __CrestLoad(41018, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
                        __CrestApply2(41017, 2, (long long )((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t )));
# 4400 "regex.c"
                        tmp___38 = __builtin_alloca((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t ));
                        __CrestClearStack(41022);
# 4400 "regex.c"
                        destination = (char *)tmp___38;
                        __CrestLoad(41025, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
                        __CrestLoad(41024, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
                        __CrestApply2(41023, 2, (long long )((unsigned long )fail_stack.size * sizeof(fail_stack_elt_t )));
# 4400 "regex.c"
                        memcpy((void * __restrict )destination, (void const * __restrict )fail_stack.stack,
                               (unsigned long )fail_stack.size * sizeof(fail_stack_elt_t ));
                        __CrestClearStack(41026);
# 4400 "regex.c"
                        fail_stack.stack = (fail_stack_elt_t *)destination;
                        {
                        __CrestLoad(41029, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
                        __CrestLoad(41028, (unsigned long )0, (long long )((unsigned long )((void *)0)));
                        __CrestApply2(41027, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 4400 "regex.c"
                        if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
                          __CrestBranch(41030, 11169, 1);
                          __CrestLoad(41032, (unsigned long )0, (long long )0);
                          __CrestStore(41033, (unsigned long )(& tmp___39));
# 4400 "regex.c"
                          tmp___39 = 0;
                        } else {
                          __CrestBranch(41031, 11170, 0);
                          __CrestLoad(41036, (unsigned long )(& fail_stack.size),
                                      (long long )fail_stack.size);
                          __CrestLoad(41035, (unsigned long )0, (long long )1);
                          __CrestApply2(41034, 8, (long long )(fail_stack.size << 1));
                          __CrestStore(41037, (unsigned long )(& fail_stack.size));
# 4400 "regex.c"
                          fail_stack.size <<= 1;
                          __CrestLoad(41038, (unsigned long )0, (long long )1);
                          __CrestStore(41039, (unsigned long )(& tmp___39));
# 4400 "regex.c"
                          tmp___39 = 1;
                        }
                        }
                        __CrestLoad(41040, (unsigned long )(& tmp___39), (long long )tmp___39);
                        __CrestStore(41041, (unsigned long )(& tmp___40));
# 4400 "regex.c"
                        tmp___40 = tmp___39;
                      }
                      }
                      {
                      __CrestLoad(41044, (unsigned long )(& tmp___40), (long long )tmp___40);
                      __CrestLoad(41043, (unsigned long )0, (long long )0);
                      __CrestApply2(41042, 13, (long long )(tmp___40 != 0));
# 4400 "regex.c"
                      if (tmp___40 != 0) {
                        __CrestBranch(41045, 11173, 1);

                      } else {
                        __CrestBranch(41046, 11174, 0);
                        __CrestLoad(41047, (unsigned long )0, (long long )-2);
                        __CrestStore(41048, (unsigned long )(& __retres457));
# 4400 "regex.c"
                        __retres457 = -2;
# 4400 "regex.c"
                        goto return_label;
                      }
                      }
                    }
                    while_break___30: ;
                    }
                    __CrestLoad(41049, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
                    __CrestStore(41050, (unsigned long )(& this_reg));
# 4400 "regex.c"
                    this_reg = (s_reg_t )lowest_active_reg;
                    {
# 4400 "regex.c"
                    while (1) {
                      while_continue___31: ;
                      {
                      __CrestLoad(41053, (unsigned long )(& this_reg), (long long )this_reg);
                      __CrestLoad(41052, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
                      __CrestApply2(41051, 15, (long long )((active_reg_t )this_reg <= highest_active_reg));
# 4400 "regex.c"
                      if ((active_reg_t )this_reg <= highest_active_reg) {
                        __CrestBranch(41054, 11182, 1);

                      } else {
                        __CrestBranch(41055, 11183, 0);
# 4400 "regex.c"
                        goto while_break___31;
                      }
                      }
                      __CrestLoad(41056, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestStore(41057, (unsigned long )(& tmp___41));
# 4400 "regex.c"
                      tmp___41 = fail_stack.avail;
                      __CrestLoad(41060, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestLoad(41059, (unsigned long )0, (long long )1U);
                      __CrestApply2(41058, 0, (long long )(fail_stack.avail + 1U));
                      __CrestStore(41061, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                      (fail_stack.avail) ++;
# 4400 "regex.c"
                      mem_296 = fail_stack.stack + tmp___41;
# 4400 "regex.c"
                      mem_297 = regstart + this_reg;
# 4400 "regex.c"
                      mem_296->pointer = (unsigned char *)*mem_297;
                      __CrestLoad(41062, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestStore(41063, (unsigned long )(& tmp___42));
# 4400 "regex.c"
                      tmp___42 = fail_stack.avail;
                      __CrestLoad(41066, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestLoad(41065, (unsigned long )0, (long long )1U);
                      __CrestApply2(41064, 0, (long long )(fail_stack.avail + 1U));
                      __CrestStore(41067, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                      (fail_stack.avail) ++;
# 4400 "regex.c"
                      mem_298 = fail_stack.stack + tmp___42;
# 4400 "regex.c"
                      mem_299 = regend + this_reg;
# 4400 "regex.c"
                      mem_298->pointer = (unsigned char *)*mem_299;
                      __CrestLoad(41068, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestStore(41069, (unsigned long )(& tmp___43));
# 4400 "regex.c"
                      tmp___43 = fail_stack.avail;
                      __CrestLoad(41072, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                      __CrestLoad(41071, (unsigned long )0, (long long )1U);
                      __CrestApply2(41070, 0, (long long )(fail_stack.avail + 1U));
                      __CrestStore(41073, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                      (fail_stack.avail) ++;
# 4400 "regex.c"
                      mem_300 = fail_stack.stack + tmp___43;
# 4400 "regex.c"
                      mem_301 = reg_info + this_reg;
# 4400 "regex.c"
                      *mem_300 = mem_301->word;
                      __CrestLoad(41076, (unsigned long )(& this_reg), (long long )this_reg);
                      __CrestLoad(41075, (unsigned long )0, (long long )1L);
                      __CrestApply2(41074, 0, (long long )(this_reg + 1L));
                      __CrestStore(41077, (unsigned long )(& this_reg));
# 4400 "regex.c"
                      this_reg ++;
                    }
                    while_break___31: ;
                    }
                    __CrestLoad(41078, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestStore(41079, (unsigned long )(& tmp___44));
# 4400 "regex.c"
                    tmp___44 = fail_stack.avail;
                    __CrestLoad(41082, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestLoad(41081, (unsigned long )0, (long long )1U);
                    __CrestApply2(41080, 0, (long long )(fail_stack.avail + 1U));
                    __CrestStore(41083, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                    (fail_stack.avail) ++;
# 4400 "regex.c"
                    mem_302 = fail_stack.stack + tmp___44;
                    __CrestLoad(41084, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
                    __CrestStore(41085, (unsigned long )(& mem_302->integer));
# 4400 "regex.c"
                    mem_302->integer = (int )lowest_active_reg;
                    __CrestLoad(41086, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestStore(41087, (unsigned long )(& tmp___45));
# 4400 "regex.c"
                    tmp___45 = fail_stack.avail;
                    __CrestLoad(41090, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestLoad(41089, (unsigned long )0, (long long )1U);
                    __CrestApply2(41088, 0, (long long )(fail_stack.avail + 1U));
                    __CrestStore(41091, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                    (fail_stack.avail) ++;
# 4400 "regex.c"
                    mem_303 = fail_stack.stack + tmp___45;
                    __CrestLoad(41092, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
                    __CrestStore(41093, (unsigned long )(& mem_303->integer));
# 4400 "regex.c"
                    mem_303->integer = (int )highest_active_reg;
                    __CrestLoad(41094, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestStore(41095, (unsigned long )(& tmp___46));
# 4400 "regex.c"
                    tmp___46 = fail_stack.avail;
                    __CrestLoad(41098, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestLoad(41097, (unsigned long )0, (long long )1U);
                    __CrestApply2(41096, 0, (long long )(fail_stack.avail + 1U));
                    __CrestStore(41099, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                    (fail_stack.avail) ++;
# 4400 "regex.c"
                    mem_304 = fail_stack.stack + tmp___46;
# 4400 "regex.c"
                    mem_304->pointer = p1 + mcnt;
                    __CrestLoad(41100, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestStore(41101, (unsigned long )(& tmp___47));
# 4400 "regex.c"
                    tmp___47 = fail_stack.avail;
                    __CrestLoad(41104, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
                    __CrestLoad(41103, (unsigned long )0, (long long )1U);
                    __CrestApply2(41102, 0, (long long )(fail_stack.avail + 1U));
                    __CrestStore(41105, (unsigned long )(& fail_stack.avail));
# 4400 "regex.c"
                    (fail_stack.avail) ++;
# 4400 "regex.c"
                    mem_305 = fail_stack.stack + tmp___47;
# 4400 "regex.c"
                    mem_305->pointer = (unsigned char *)d;
# 4400 "regex.c"
                    goto while_break___29;
                  }
                  while_break___29: ;
                  }
# 4402 "regex.c"
                  goto fail;
                } else {
                  __CrestBranch(40959, 11190, 0);

                }
                }
                }
              } else {
                __CrestBranch(40954, 11191, 0);

              }
              }
              }
            } else {
              __CrestBranch(40949, 11192, 0);

            }
            }
          } else {
            __CrestBranch(40944, 11193, 0);

          }
          }
        } else {
          __CrestBranch(40893, 11194, 0);

        }
        }
      } else {
        __CrestBranch(40886, 11195, 0);

      }
      }
    }
    }
    }
# 4407 "regex.c"
    p += 2;
# 4408 "regex.c"
    goto switch_break;
    case_8:
# 4416 "regex.c"
    tmp___48 = p;
# 4416 "regex.c"
    p ++;
    __CrestLoad(41106, (unsigned long )tmp___48, (long long )*tmp___48);
    __CrestStore(41107, (unsigned long )(& regno));
# 4416 "regex.c"
    regno = (int )*tmp___48;
    {
# 4420 "regex.c"
    mem_306 = regstart + regno;
    {
    __CrestLoad(41110, (unsigned long )mem_306, (long long )((unsigned long )*mem_306));
    __CrestLoad(41109, (unsigned long )0, (long long )((unsigned long )(& reg_unset_dummy)));
    __CrestApply2(41108, 12, (long long )((unsigned long )*mem_306 == (unsigned long )(& reg_unset_dummy)));
# 4420 "regex.c"
    if ((unsigned long )*mem_306 == (unsigned long )(& reg_unset_dummy)) {
      __CrestBranch(41111, 11202, 1);
# 4421 "regex.c"
      goto fail;
    } else {
      __CrestBranch(41112, 11203, 0);
      {
# 4420 "regex.c"
      mem_307 = regend + regno;
      {
      __CrestLoad(41115, (unsigned long )mem_307, (long long )((unsigned long )*mem_307));
      __CrestLoad(41114, (unsigned long )0, (long long )((unsigned long )(& reg_unset_dummy)));
      __CrestApply2(41113, 12, (long long )((unsigned long )*mem_307 == (unsigned long )(& reg_unset_dummy)));
# 4420 "regex.c"
      if ((unsigned long )*mem_307 == (unsigned long )(& reg_unset_dummy)) {
        __CrestBranch(41116, 11206, 1);
# 4421 "regex.c"
        goto fail;
      } else {
        __CrestBranch(41117, 11207, 0);

      }
      }
      }
    }
    }
    }
# 4424 "regex.c"
    mem_308 = regstart + regno;
# 4424 "regex.c"
    d2 = *mem_308;
    {
    __CrestLoad(41120, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(41119, (unsigned long )0, (long long )0);
    __CrestApply2(41118, 13, (long long )(size1 != 0));
# 4431 "regex.c"
    if (size1 != 0) {
      __CrestBranch(41121, 11210, 1);
      {
# 4431 "regex.c"
      mem_309 = regstart + regno;
      {
      __CrestLoad(41125, (unsigned long )(& string1), (long long )((unsigned long )string1));
      __CrestLoad(41124, (unsigned long )mem_309, (long long )((unsigned long )*mem_309));
      __CrestApply2(41123, 15, (long long )((unsigned long )string1 <= (unsigned long )*mem_309));
# 4431 "regex.c"
      if ((unsigned long )string1 <= (unsigned long )*mem_309) {
        __CrestBranch(41126, 11213, 1);
        {
# 4431 "regex.c"
        mem_310 = regstart + regno;
        {
        __CrestLoad(41132, (unsigned long )mem_310, (long long )((unsigned long )*mem_310));
        __CrestLoad(41131, (unsigned long )(& string1), (long long )((unsigned long )string1));
        __CrestLoad(41130, (unsigned long )(& size1), (long long )size1);
        __CrestApply2(41129, 18, (long long )((unsigned long )(string1 + size1)));
        __CrestApply2(41128, 15, (long long )((unsigned long )*mem_310 <= (unsigned long )(string1 + size1)));
# 4431 "regex.c"
        if ((unsigned long )*mem_310 <= (unsigned long )(string1 + size1)) {
          __CrestBranch(41133, 11216, 1);
          __CrestLoad(41135, (unsigned long )0, (long long )1);
          __CrestStore(41136, (unsigned long )(& tmp___51));
# 4431 "regex.c"
          tmp___51 = 1;
        } else {
          __CrestBranch(41134, 11217, 0);
          __CrestLoad(41137, (unsigned long )0, (long long )0);
          __CrestStore(41138, (unsigned long )(& tmp___51));
# 4431 "regex.c"
          tmp___51 = 0;
        }
        }
        }
      } else {
        __CrestBranch(41127, 11218, 0);
        __CrestLoad(41139, (unsigned long )0, (long long )0);
        __CrestStore(41140, (unsigned long )(& tmp___51));
# 4431 "regex.c"
        tmp___51 = 0;
      }
      }
      }
    } else {
      __CrestBranch(41122, 11219, 0);
      __CrestLoad(41141, (unsigned long )0, (long long )0);
      __CrestStore(41142, (unsigned long )(& tmp___51));
# 4431 "regex.c"
      tmp___51 = 0;
    }
    }
    {
    __CrestLoad(41145, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(41144, (unsigned long )0, (long long )0);
    __CrestApply2(41143, 13, (long long )(size1 != 0));
# 4431 "regex.c"
    if (size1 != 0) {
      __CrestBranch(41146, 11221, 1);
      {
# 4431 "regex.c"
      mem_311 = regend + regno;
      {
      __CrestLoad(41150, (unsigned long )(& string1), (long long )((unsigned long )string1));
      __CrestLoad(41149, (unsigned long )mem_311, (long long )((unsigned long )*mem_311));
      __CrestApply2(41148, 15, (long long )((unsigned long )string1 <= (unsigned long )*mem_311));
# 4431 "regex.c"
      if ((unsigned long )string1 <= (unsigned long )*mem_311) {
        __CrestBranch(41151, 11224, 1);
        {
# 4431 "regex.c"
        mem_312 = regend + regno;
        {
        __CrestLoad(41157, (unsigned long )mem_312, (long long )((unsigned long )*mem_312));
        __CrestLoad(41156, (unsigned long )(& string1), (long long )((unsigned long )string1));
        __CrestLoad(41155, (unsigned long )(& size1), (long long )size1);
        __CrestApply2(41154, 18, (long long )((unsigned long )(string1 + size1)));
        __CrestApply2(41153, 15, (long long )((unsigned long )*mem_312 <= (unsigned long )(string1 + size1)));
# 4431 "regex.c"
        if ((unsigned long )*mem_312 <= (unsigned long )(string1 + size1)) {
          __CrestBranch(41158, 11227, 1);
          __CrestLoad(41160, (unsigned long )0, (long long )1);
          __CrestStore(41161, (unsigned long )(& tmp___52));
# 4431 "regex.c"
          tmp___52 = 1;
        } else {
          __CrestBranch(41159, 11228, 0);
          __CrestLoad(41162, (unsigned long )0, (long long )0);
          __CrestStore(41163, (unsigned long )(& tmp___52));
# 4431 "regex.c"
          tmp___52 = 0;
        }
        }
        }
      } else {
        __CrestBranch(41152, 11229, 0);
        __CrestLoad(41164, (unsigned long )0, (long long )0);
        __CrestStore(41165, (unsigned long )(& tmp___52));
# 4431 "regex.c"
        tmp___52 = 0;
      }
      }
      }
    } else {
      __CrestBranch(41147, 11230, 0);
      __CrestLoad(41166, (unsigned long )0, (long long )0);
      __CrestStore(41167, (unsigned long )(& tmp___52));
# 4431 "regex.c"
      tmp___52 = 0;
    }
    }
    {
    __CrestLoad(41170, (unsigned long )(& tmp___51), (long long )tmp___51);
    __CrestLoad(41169, (unsigned long )(& tmp___52), (long long )tmp___52);
    __CrestApply2(41168, 12, (long long )(tmp___51 == tmp___52));
# 4431 "regex.c"
    if (tmp___51 == tmp___52) {
      __CrestBranch(41171, 11232, 1);
# 4431 "regex.c"
      mem_313 = regend + regno;
# 4431 "regex.c"
      dend2 = *mem_313;
    } else {
      __CrestBranch(41172, 11233, 0);
# 4431 "regex.c"
      dend2 = end_match_1;
    }
    }
    {
# 4434 "regex.c"
    while (1) {
      while_continue___32: ;
      {
# 4438 "regex.c"
      while (1) {
        while_continue___33: ;
        {
        __CrestLoad(41175, (unsigned long )(& d2), (long long )((unsigned long )d2));
        __CrestLoad(41174, (unsigned long )(& dend2), (long long )((unsigned long )dend2));
        __CrestApply2(41173, 12, (long long )((unsigned long )d2 == (unsigned long )dend2));
# 4438 "regex.c"
        if ((unsigned long )d2 == (unsigned long )dend2) {
          __CrestBranch(41176, 11241, 1);

        } else {
          __CrestBranch(41177, 11242, 0);
# 4438 "regex.c"
          goto while_break___33;
        }
        }
        {
        __CrestLoad(41180, (unsigned long )(& dend2), (long long )((unsigned long )dend2));
        __CrestLoad(41179, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
        __CrestApply2(41178, 12, (long long )((unsigned long )dend2 == (unsigned long )end_match_2));
# 4440 "regex.c"
        if ((unsigned long )dend2 == (unsigned long )end_match_2) {
          __CrestBranch(41181, 11244, 1);
# 4440 "regex.c"
          goto while_break___33;
        } else {
          __CrestBranch(41182, 11245, 0);

        }
        }
        {
# 4441 "regex.c"
        mem_314 = regend + regno;
        {
        __CrestLoad(41185, (unsigned long )(& dend2), (long long )((unsigned long )dend2));
        __CrestLoad(41184, (unsigned long )mem_314, (long long )((unsigned long )*mem_314));
        __CrestApply2(41183, 12, (long long )((unsigned long )dend2 == (unsigned long )*mem_314));
# 4441 "regex.c"
        if ((unsigned long )dend2 == (unsigned long )*mem_314) {
          __CrestBranch(41186, 11249, 1);
# 4441 "regex.c"
          goto while_break___33;
        } else {
          __CrestBranch(41187, 11250, 0);

        }
        }
        }
# 4444 "regex.c"
        d2 = string2;
# 4445 "regex.c"
        mem_315 = regend + regno;
# 4445 "regex.c"
        dend2 = *mem_315;
      }
      while_break___33: ;
      }
      {
      __CrestLoad(41190, (unsigned long )(& d2), (long long )((unsigned long )d2));
      __CrestLoad(41189, (unsigned long )(& dend2), (long long )((unsigned long )dend2));
      __CrestApply2(41188, 12, (long long )((unsigned long )d2 == (unsigned long )dend2));
# 4448 "regex.c"
      if ((unsigned long )d2 == (unsigned long )dend2) {
        __CrestBranch(41191, 11254, 1);
# 4448 "regex.c"
        goto while_break___32;
      } else {
        __CrestBranch(41192, 11255, 0);

      }
      }
      {
# 4451 "regex.c"
      while (1) {
        while_continue___34: ;
        {
        __CrestLoad(41195, (unsigned long )(& d), (long long )((unsigned long )d));
        __CrestLoad(41194, (unsigned long )(& dend), (long long )((unsigned long )dend));
        __CrestApply2(41193, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 4451 "regex.c"
        if ((unsigned long )d == (unsigned long )dend) {
          __CrestBranch(41196, 11260, 1);

        } else {
          __CrestBranch(41197, 11261, 0);
# 4451 "regex.c"
          goto while_break___34;
        }
        }
        {
        __CrestLoad(41200, (unsigned long )(& dend), (long long )((unsigned long )dend));
        __CrestLoad(41199, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
        __CrestApply2(41198, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 4451 "regex.c"
        if ((unsigned long )dend == (unsigned long )end_match_2) {
          __CrestBranch(41201, 11263, 1);
# 4451 "regex.c"
          goto fail;
        } else {
          __CrestBranch(41202, 11264, 0);

        }
        }
# 4451 "regex.c"
        d = string2;
# 4451 "regex.c"
        dend = end_match_2;
      }
      while_break___34: ;
      }
      __CrestLoad(41205, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestLoad(41204, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestApply2(41203, 18, (long long )(dend - d));
      __CrestStore(41206, (unsigned long )(& mcnt));
# 4454 "regex.c"
      mcnt = (int )(dend - d);
      {
      __CrestLoad(41211, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(41210, (unsigned long )(& dend2), (long long )((unsigned long )dend2));
      __CrestLoad(41209, (unsigned long )(& d2), (long long )((unsigned long )d2));
      __CrestApply2(41208, 18, (long long )(dend2 - d2));
      __CrestApply2(41207, 14, (long long )((long )mcnt > dend2 - d2));
# 4458 "regex.c"
      if ((long )mcnt > dend2 - d2) {
        __CrestBranch(41212, 11269, 1);
        __CrestLoad(41216, (unsigned long )(& dend2), (long long )((unsigned long )dend2));
        __CrestLoad(41215, (unsigned long )(& d2), (long long )((unsigned long )d2));
        __CrestApply2(41214, 18, (long long )(dend2 - d2));
        __CrestStore(41217, (unsigned long )(& mcnt));
# 4459 "regex.c"
        mcnt = (int )(dend2 - d2);
      } else {
        __CrestBranch(41213, 11270, 0);

      }
      }
      {
      __CrestLoad(41220, (unsigned long )(& translate), (long long )((unsigned long )translate));
      __CrestLoad(41219, (unsigned long )0, (long long )0);
      __CrestApply2(41218, 13, (long long )(translate != 0));
# 4463 "regex.c"
      if (translate != 0) {
        __CrestBranch(41221, 11272, 1);
        __CrestLoad(41223, (unsigned long )(& mcnt), (long long )mcnt);
# 4463 "regex.c"
        tmp___53 = bcmp_translate(d, d2, mcnt, translate);
        __CrestHandleReturn(41225, (long long )tmp___53);
        __CrestStore(41224, (unsigned long )(& tmp___53));
        __CrestLoad(41226, (unsigned long )(& tmp___53), (long long )tmp___53);
        __CrestStore(41227, (unsigned long )(& tmp___55));
# 4463 "regex.c"
        tmp___55 = tmp___53;
      } else {
        __CrestBranch(41222, 11273, 0);
        __CrestLoad(41228, (unsigned long )(& mcnt), (long long )mcnt);
# 4463 "regex.c"
        tmp___54 = memcmp((void const *)d, (void const *)d2, (size_t )mcnt);
        __CrestHandleReturn(41230, (long long )tmp___54);
        __CrestStore(41229, (unsigned long )(& tmp___54));
        __CrestLoad(41231, (unsigned long )(& tmp___54), (long long )tmp___54);
        __CrestStore(41232, (unsigned long )(& tmp___55));
# 4463 "regex.c"
        tmp___55 = tmp___54;
      }
      }
      {
      __CrestLoad(41235, (unsigned long )(& tmp___55), (long long )tmp___55);
      __CrestLoad(41234, (unsigned long )0, (long long )0);
      __CrestApply2(41233, 13, (long long )(tmp___55 != 0));
# 4463 "regex.c"
      if (tmp___55 != 0) {
        __CrestBranch(41236, 11275, 1);
# 4466 "regex.c"
        goto fail;
      } else {
        __CrestBranch(41237, 11276, 0);

      }
      }
# 4467 "regex.c"
      d += mcnt;
# 4467 "regex.c"
      d2 += mcnt;
      {
# 4470 "regex.c"
      while (1) {
        while_continue___35: ;
        {
        __CrestLoad(41240, (unsigned long )(& set_regs_matched_done), (long long )set_regs_matched_done);
        __CrestLoad(41239, (unsigned long )0, (long long )0);
        __CrestApply2(41238, 12, (long long )(set_regs_matched_done == 0));
# 4470 "regex.c"
        if (set_regs_matched_done == 0) {
          __CrestBranch(41241, 11282, 1);
          __CrestLoad(41243, (unsigned long )0, (long long )1);
          __CrestStore(41244, (unsigned long )(& set_regs_matched_done));
# 4470 "regex.c"
          set_regs_matched_done = 1;
          __CrestLoad(41245, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
          __CrestStore(41246, (unsigned long )(& r___4));
# 4470 "regex.c"
          r___4 = lowest_active_reg;
          {
# 4470 "regex.c"
          while (1) {
            while_continue___36: ;
            {
            __CrestLoad(41249, (unsigned long )(& r___4), (long long )r___4);
            __CrestLoad(41248, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
            __CrestApply2(41247, 15, (long long )(r___4 <= highest_active_reg));
# 4470 "regex.c"
            if (r___4 <= highest_active_reg) {
              __CrestBranch(41250, 11287, 1);

            } else {
              __CrestBranch(41251, 11288, 0);
# 4470 "regex.c"
              goto while_break___36;
            }
            }
            __CrestLoad(41252, (unsigned long )0, (long long )1U);
            __CrestStore(41253, (unsigned long )(& tmp___56));
# 4470 "regex.c"
            tmp___56 = 1U;
# 4470 "regex.c"
            mem_316 = reg_info + r___4;
# 4470 "regex.c"
            mem_316->bits.ever_matched_something = tmp___56;
# 4470 "regex.c"
            mem_317 = reg_info + r___4;
# 4470 "regex.c"
            mem_317->bits.matched_something = tmp___56;
            __CrestLoad(41256, (unsigned long )(& r___4), (long long )r___4);
            __CrestLoad(41255, (unsigned long )0, (long long )1UL);
            __CrestApply2(41254, 0, (long long )(r___4 + 1UL));
            __CrestStore(41257, (unsigned long )(& r___4));
# 4470 "regex.c"
            r___4 ++;
          }
          while_break___36: ;
          }
        } else {
          __CrestBranch(41242, 11291, 0);

        }
        }
# 4470 "regex.c"
        goto while_break___35;
      }
      while_break___35: ;
      }
    }
    while_break___32: ;
    }
# 4473 "regex.c"
    goto switch_break;
    case_9: ;
    {
    __CrestLoad(41260, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(41259, (unsigned long )0, (long long )0);
    __CrestApply2(41258, 13, (long long )(size1 != 0));
# 4482 "regex.c"
    if (size1 != 0) {
      __CrestBranch(41261, 11298, 1);
# 4482 "regex.c"
      tmp___57 = string1;
    } else {
      __CrestBranch(41262, 11299, 0);
# 4482 "regex.c"
      tmp___57 = string2;
    }
    }
    {
    __CrestLoad(41265, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(41264, (unsigned long )(& tmp___57), (long long )((unsigned long )tmp___57));
    __CrestApply2(41263, 12, (long long )((unsigned long )d == (unsigned long )tmp___57));
# 4482 "regex.c"
    if ((unsigned long )d == (unsigned long )tmp___57) {
      __CrestBranch(41266, 11301, 1);
# 4482 "regex.c"
      goto _L___13;
    } else {
      __CrestBranch(41267, 11302, 0);
      {
      __CrestLoad(41270, (unsigned long )(& size2), (long long )size2);
      __CrestLoad(41269, (unsigned long )0, (long long )0);
      __CrestApply2(41268, 12, (long long )(size2 == 0));
# 4482 "regex.c"
      if (size2 == 0) {
        __CrestBranch(41271, 11303, 1);
        _L___13:
        {
        __CrestLoad(41275, (unsigned long )0, (long long )bufp->not_bol);
        __CrestLoad(41274, (unsigned long )0, (long long )0);
        __CrestApply2(41273, 12, (long long )(bufp->not_bol == 0));
# 4484 "regex.c"
        if (bufp->not_bol == 0) {
          __CrestBranch(41276, 11304, 1);
# 4484 "regex.c"
          goto switch_break;
        } else {
          __CrestBranch(41277, 11305, 0);

        }
        }
      } else {
        __CrestBranch(41272, 11306, 0);
        {
# 4486 "regex.c"
        mem_318 = d + -1;
        {
        __CrestLoad(41280, (unsigned long )mem_318, (long long )*mem_318);
        __CrestLoad(41279, (unsigned long )0, (long long )10);
        __CrestApply2(41278, 12, (long long )((int const )*mem_318 == 10));
# 4486 "regex.c"
        if ((int const )*mem_318 == 10) {
          __CrestBranch(41281, 11309, 1);
          {
          __CrestLoad(41285, (unsigned long )0, (long long )bufp->newline_anchor);
          __CrestLoad(41284, (unsigned long )0, (long long )0);
          __CrestApply2(41283, 13, (long long )(bufp->newline_anchor != 0));
# 4486 "regex.c"
          if (bufp->newline_anchor != 0) {
            __CrestBranch(41286, 11310, 1);
# 4488 "regex.c"
            goto switch_break;
          } else {
            __CrestBranch(41287, 11311, 0);

          }
          }
        } else {
          __CrestBranch(41282, 11312, 0);

        }
        }
        }
      }
      }
    }
    }
# 4491 "regex.c"
    goto fail;
    case_10: ;
    {
    __CrestLoad(41290, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(41289, (unsigned long )(& end2), (long long )((unsigned long )end2));
    __CrestApply2(41288, 12, (long long )((unsigned long )d == (unsigned long )end2));
# 4498 "regex.c"
    if ((unsigned long )d == (unsigned long )end2) {
      __CrestBranch(41291, 11316, 1);
      {
      __CrestLoad(41295, (unsigned long )0, (long long )bufp->not_eol);
      __CrestLoad(41294, (unsigned long )0, (long long )0);
      __CrestApply2(41293, 12, (long long )(bufp->not_eol == 0));
# 4500 "regex.c"
      if (bufp->not_eol == 0) {
        __CrestBranch(41296, 11317, 1);
# 4500 "regex.c"
        goto switch_break;
      } else {
        __CrestBranch(41297, 11318, 0);

      }
      }
    } else {
      __CrestBranch(41292, 11319, 0);
      {
      __CrestLoad(41300, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(41299, (unsigned long )(& end1), (long long )((unsigned long )end1));
      __CrestApply2(41298, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 4504 "regex.c"
      if ((unsigned long )d == (unsigned long )end1) {
        __CrestBranch(41301, 11320, 1);
        __CrestLoad(41303, (unsigned long )string2, (long long )*string2);
        __CrestStore(41304, (unsigned long )(& tmp___58));
# 4504 "regex.c"
        tmp___58 = (int const )*string2;
      } else {
        __CrestBranch(41302, 11321, 0);
        __CrestLoad(41305, (unsigned long )d, (long long )*d);
        __CrestStore(41306, (unsigned long )(& tmp___58));
# 4504 "regex.c"
        tmp___58 = (int const )*d;
      }
      }
      {
      __CrestLoad(41309, (unsigned long )(& tmp___58), (long long )tmp___58);
      __CrestLoad(41308, (unsigned long )0, (long long )10);
      __CrestApply2(41307, 12, (long long )(tmp___58 == 10));
# 4504 "regex.c"
      if (tmp___58 == 10) {
        __CrestBranch(41310, 11323, 1);
        {
        __CrestLoad(41314, (unsigned long )0, (long long )bufp->newline_anchor);
        __CrestLoad(41313, (unsigned long )0, (long long )0);
        __CrestApply2(41312, 13, (long long )(bufp->newline_anchor != 0));
# 4504 "regex.c"
        if (bufp->newline_anchor != 0) {
          __CrestBranch(41315, 11324, 1);
# 4507 "regex.c"
          goto switch_break;
        } else {
          __CrestBranch(41316, 11325, 0);

        }
        }
      } else {
        __CrestBranch(41311, 11326, 0);

      }
      }
    }
    }
# 4509 "regex.c"
    goto fail;
    case_11: ;
    {
    __CrestLoad(41319, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(41318, (unsigned long )0, (long long )0);
    __CrestApply2(41317, 13, (long long )(size1 != 0));
# 4515 "regex.c"
    if (size1 != 0) {
      __CrestBranch(41320, 11330, 1);
# 4515 "regex.c"
      tmp___59 = string1;
    } else {
      __CrestBranch(41321, 11331, 0);
# 4515 "regex.c"
      tmp___59 = string2;
    }
    }
    {
    __CrestLoad(41324, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(41323, (unsigned long )(& tmp___59), (long long )((unsigned long )tmp___59));
    __CrestApply2(41322, 12, (long long )((unsigned long )d == (unsigned long )tmp___59));
# 4515 "regex.c"
    if ((unsigned long )d == (unsigned long )tmp___59) {
      __CrestBranch(41325, 11333, 1);
# 4516 "regex.c"
      goto switch_break;
    } else {
      __CrestBranch(41326, 11334, 0);
      {
      __CrestLoad(41329, (unsigned long )(& size2), (long long )size2);
      __CrestLoad(41328, (unsigned long )0, (long long )0);
      __CrestApply2(41327, 12, (long long )(size2 == 0));
# 4515 "regex.c"
      if (size2 == 0) {
        __CrestBranch(41330, 11335, 1);
# 4516 "regex.c"
        goto switch_break;
      } else {
        __CrestBranch(41331, 11336, 0);

      }
      }
    }
    }
# 4517 "regex.c"
    goto fail;
    case_12: ;
    {
    __CrestLoad(41334, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(41333, (unsigned long )(& end2), (long long )((unsigned long )end2));
    __CrestApply2(41332, 12, (long long )((unsigned long )d == (unsigned long )end2));
# 4523 "regex.c"
    if ((unsigned long )d == (unsigned long )end2) {
      __CrestBranch(41335, 11340, 1);
# 4524 "regex.c"
      goto switch_break;
    } else {
      __CrestBranch(41336, 11341, 0);

    }
    }
# 4525 "regex.c"
    goto fail;
    case_16: ;
    {
# 4547 "regex.c"
    while (1) {
      while_continue___37: ;
      {
# 4547 "regex.c"
      while (1) {
        while_continue___38: ;
        __CrestLoad(41339, (unsigned long )p, (long long )*p);
        __CrestLoad(41338, (unsigned long )0, (long long )255);
        __CrestApply2(41337, 5, (long long )((int )*p & 255));
        __CrestStore(41340, (unsigned long )(& mcnt));
# 4547 "regex.c"
        mcnt = (int )*p & 255;
# 4547 "regex.c"
        mem_319 = p + 1;
        __CrestLoad(41345, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(41344, (unsigned long )mem_319, (long long )*mem_319);
        __CrestLoad(41343, (unsigned long )0, (long long )8);
        __CrestApply2(41342, 8, (long long )((int )((signed char )*mem_319) << 8));
        __CrestApply2(41341, 0, (long long )(mcnt + ((int )((signed char )*mem_319) << 8)));
        __CrestStore(41346, (unsigned long )(& mcnt));
# 4547 "regex.c"
        mcnt += (int )((signed char )*mem_319) << 8;
# 4547 "regex.c"
        goto while_break___38;
      }
      while_break___38: ;
      }
# 4547 "regex.c"
      p += 2;
# 4547 "regex.c"
      goto while_break___37;
    }
    while_break___37: ;
    }
    {
# 4554 "regex.c"
    while (1) {
      while_continue___39: ;
      {
# 4554 "regex.c"
      while (1) {
        while_continue___40: ;
        {
        __CrestLoad(41359, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(41358, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestApply2(41357, 1, (long long )(fail_stack.size - fail_stack.avail));
        __CrestLoad(41356, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestLoad(41355, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestApply2(41354, 1, (long long )(highest_active_reg - lowest_active_reg));
        __CrestLoad(41353, (unsigned long )0, (long long )1UL);
        __CrestApply2(41352, 0, (long long )((highest_active_reg - lowest_active_reg) + 1UL));
        __CrestLoad(41351, (unsigned long )0, (long long )3UL);
        __CrestApply2(41350, 2, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL));
        __CrestLoad(41349, (unsigned long )0, (long long )4UL);
        __CrestApply2(41348, 0, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
        __CrestApply2(41347, 16, (long long )((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
# 4554 "regex.c"
        if ((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL) {
          __CrestBranch(41360, 11363, 1);

        } else {
          __CrestBranch(41361, 11364, 0);
# 4554 "regex.c"
          goto while_break___40;
        }
        }
        {
        __CrestLoad(41366, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(41365, (unsigned long )(& re_max_failures), (long long )re_max_failures);
        __CrestLoad(41364, (unsigned long )0, (long long )19);
        __CrestApply2(41363, 2, (long long )(re_max_failures * 19));
        __CrestApply2(41362, 14, (long long )(fail_stack.size > (unsigned int )(re_max_failures * 19)));
# 4554 "regex.c"
        if (fail_stack.size > (unsigned int )(re_max_failures * 19)) {
          __CrestBranch(41367, 11366, 1);
          __CrestLoad(41369, (unsigned long )0, (long long )0);
          __CrestStore(41370, (unsigned long )(& tmp___62));
# 4554 "regex.c"
          tmp___62 = 0;
        } else {
          __CrestBranch(41368, 11367, 0);
          __CrestLoad(41375, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(41374, (unsigned long )0, (long long )1);
          __CrestApply2(41373, 8, (long long )(fail_stack.size << 1));
          __CrestLoad(41372, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(41371, 2, (long long )((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t )));
# 4554 "regex.c"
          tmp___60 = __builtin_alloca((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t ));
          __CrestClearStack(41376);
# 4554 "regex.c"
          destination___0 = (char *)tmp___60;
          __CrestLoad(41379, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(41378, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(41377, 2, (long long )((unsigned long )fail_stack.size * sizeof(fail_stack_elt_t )));
# 4554 "regex.c"
          memcpy((void * __restrict )destination___0, (void const * __restrict )fail_stack.stack,
                 (unsigned long )fail_stack.size * sizeof(fail_stack_elt_t ));
          __CrestClearStack(41380);
# 4554 "regex.c"
          fail_stack.stack = (fail_stack_elt_t *)destination___0;
          {
          __CrestLoad(41383, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
          __CrestLoad(41382, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(41381, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 4554 "regex.c"
          if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
            __CrestBranch(41384, 11369, 1);
            __CrestLoad(41386, (unsigned long )0, (long long )0);
            __CrestStore(41387, (unsigned long )(& tmp___61));
# 4554 "regex.c"
            tmp___61 = 0;
          } else {
            __CrestBranch(41385, 11370, 0);
            __CrestLoad(41390, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
            __CrestLoad(41389, (unsigned long )0, (long long )1);
            __CrestApply2(41388, 8, (long long )(fail_stack.size << 1));
            __CrestStore(41391, (unsigned long )(& fail_stack.size));
# 4554 "regex.c"
            fail_stack.size <<= 1;
            __CrestLoad(41392, (unsigned long )0, (long long )1);
            __CrestStore(41393, (unsigned long )(& tmp___61));
# 4554 "regex.c"
            tmp___61 = 1;
          }
          }
          __CrestLoad(41394, (unsigned long )(& tmp___61), (long long )tmp___61);
          __CrestStore(41395, (unsigned long )(& tmp___62));
# 4554 "regex.c"
          tmp___62 = tmp___61;
        }
        }
        {
        __CrestLoad(41398, (unsigned long )(& tmp___62), (long long )tmp___62);
        __CrestLoad(41397, (unsigned long )0, (long long )0);
        __CrestApply2(41396, 13, (long long )(tmp___62 != 0));
# 4554 "regex.c"
        if (tmp___62 != 0) {
          __CrestBranch(41399, 11373, 1);

        } else {
          __CrestBranch(41400, 11374, 0);
          __CrestLoad(41401, (unsigned long )0, (long long )-2);
          __CrestStore(41402, (unsigned long )(& __retres457));
# 4554 "regex.c"
          __retres457 = -2;
# 4554 "regex.c"
          goto return_label;
        }
        }
      }
      while_break___40: ;
      }
      __CrestLoad(41403, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(41404, (unsigned long )(& this_reg___0));
# 4554 "regex.c"
      this_reg___0 = (s_reg_t )lowest_active_reg;
      {
# 4554 "regex.c"
      while (1) {
        while_continue___41: ;
        {
        __CrestLoad(41407, (unsigned long )(& this_reg___0), (long long )this_reg___0);
        __CrestLoad(41406, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestApply2(41405, 15, (long long )((active_reg_t )this_reg___0 <= highest_active_reg));
# 4554 "regex.c"
        if ((active_reg_t )this_reg___0 <= highest_active_reg) {
          __CrestBranch(41408, 11382, 1);

        } else {
          __CrestBranch(41409, 11383, 0);
# 4554 "regex.c"
          goto while_break___41;
        }
        }
        __CrestLoad(41410, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41411, (unsigned long )(& tmp___63));
# 4554 "regex.c"
        tmp___63 = fail_stack.avail;
        __CrestLoad(41414, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41413, (unsigned long )0, (long long )1U);
        __CrestApply2(41412, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41415, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
        (fail_stack.avail) ++;
# 4554 "regex.c"
        mem_320 = fail_stack.stack + tmp___63;
# 4554 "regex.c"
        mem_321 = regstart + this_reg___0;
# 4554 "regex.c"
        mem_320->pointer = (unsigned char *)*mem_321;
        __CrestLoad(41416, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41417, (unsigned long )(& tmp___64));
# 4554 "regex.c"
        tmp___64 = fail_stack.avail;
        __CrestLoad(41420, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41419, (unsigned long )0, (long long )1U);
        __CrestApply2(41418, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41421, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
        (fail_stack.avail) ++;
# 4554 "regex.c"
        mem_322 = fail_stack.stack + tmp___64;
# 4554 "regex.c"
        mem_323 = regend + this_reg___0;
# 4554 "regex.c"
        mem_322->pointer = (unsigned char *)*mem_323;
        __CrestLoad(41422, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41423, (unsigned long )(& tmp___65));
# 4554 "regex.c"
        tmp___65 = fail_stack.avail;
        __CrestLoad(41426, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41425, (unsigned long )0, (long long )1U);
        __CrestApply2(41424, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41427, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
        (fail_stack.avail) ++;
# 4554 "regex.c"
        mem_324 = fail_stack.stack + tmp___65;
# 4554 "regex.c"
        mem_325 = reg_info + this_reg___0;
# 4554 "regex.c"
        *mem_324 = mem_325->word;
        __CrestLoad(41430, (unsigned long )(& this_reg___0), (long long )this_reg___0);
        __CrestLoad(41429, (unsigned long )0, (long long )1L);
        __CrestApply2(41428, 0, (long long )(this_reg___0 + 1L));
        __CrestStore(41431, (unsigned long )(& this_reg___0));
# 4554 "regex.c"
        this_reg___0 ++;
      }
      while_break___41: ;
      }
      __CrestLoad(41432, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41433, (unsigned long )(& tmp___66));
# 4554 "regex.c"
      tmp___66 = fail_stack.avail;
      __CrestLoad(41436, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41435, (unsigned long )0, (long long )1U);
      __CrestApply2(41434, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41437, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
      (fail_stack.avail) ++;
# 4554 "regex.c"
      mem_326 = fail_stack.stack + tmp___66;
      __CrestLoad(41438, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(41439, (unsigned long )(& mem_326->integer));
# 4554 "regex.c"
      mem_326->integer = (int )lowest_active_reg;
      __CrestLoad(41440, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41441, (unsigned long )(& tmp___67));
# 4554 "regex.c"
      tmp___67 = fail_stack.avail;
      __CrestLoad(41444, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41443, (unsigned long )0, (long long )1U);
      __CrestApply2(41442, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41445, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
      (fail_stack.avail) ++;
# 4554 "regex.c"
      mem_327 = fail_stack.stack + tmp___67;
      __CrestLoad(41446, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
      __CrestStore(41447, (unsigned long )(& mem_327->integer));
# 4554 "regex.c"
      mem_327->integer = (int )highest_active_reg;
      __CrestLoad(41448, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41449, (unsigned long )(& tmp___68));
# 4554 "regex.c"
      tmp___68 = fail_stack.avail;
      __CrestLoad(41452, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41451, (unsigned long )0, (long long )1U);
      __CrestApply2(41450, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41453, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
      (fail_stack.avail) ++;
# 4554 "regex.c"
      mem_328 = fail_stack.stack + tmp___68;
# 4554 "regex.c"
      mem_328->pointer = p + mcnt;
      __CrestLoad(41454, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41455, (unsigned long )(& tmp___69));
# 4554 "regex.c"
      tmp___69 = fail_stack.avail;
      __CrestLoad(41458, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41457, (unsigned long )0, (long long )1U);
      __CrestApply2(41456, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41459, (unsigned long )(& fail_stack.avail));
# 4554 "regex.c"
      (fail_stack.avail) ++;
# 4554 "regex.c"
      mem_329 = fail_stack.stack + tmp___69;
# 4554 "regex.c"
      mem_329->pointer = (unsigned char *)((void *)0);
# 4554 "regex.c"
      goto while_break___39;
    }
    while_break___39: ;
    }
# 4555 "regex.c"
    goto switch_break;
    on_failure:
    case_15: ;
    {
# 4574 "regex.c"
    while (1) {
      while_continue___42: ;
      {
# 4574 "regex.c"
      while (1) {
        while_continue___43: ;
        __CrestLoad(41462, (unsigned long )p, (long long )*p);
        __CrestLoad(41461, (unsigned long )0, (long long )255);
        __CrestApply2(41460, 5, (long long )((int )*p & 255));
        __CrestStore(41463, (unsigned long )(& mcnt));
# 4574 "regex.c"
        mcnt = (int )*p & 255;
# 4574 "regex.c"
        mem_330 = p + 1;
        __CrestLoad(41468, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(41467, (unsigned long )mem_330, (long long )*mem_330);
        __CrestLoad(41466, (unsigned long )0, (long long )8);
        __CrestApply2(41465, 8, (long long )((int )((signed char )*mem_330) << 8));
        __CrestApply2(41464, 0, (long long )(mcnt + ((int )((signed char )*mem_330) << 8)));
        __CrestStore(41469, (unsigned long )(& mcnt));
# 4574 "regex.c"
        mcnt += (int )((signed char )*mem_330) << 8;
# 4574 "regex.c"
        goto while_break___43;
      }
      while_break___43: ;
      }
# 4574 "regex.c"
      p += 2;
# 4574 "regex.c"
      goto while_break___42;
    }
    while_break___42: ;
    }
# 4590 "regex.c"
    p1 = p;
    {
# 4596 "regex.c"
    while (1) {
      while_continue___44: ;
      {
      __CrestLoad(41472, (unsigned long )(& p1), (long long )((unsigned long )p1));
      __CrestLoad(41471, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(41470, 16, (long long )((unsigned long )p1 < (unsigned long )pend));
# 4596 "regex.c"
      if ((unsigned long )p1 < (unsigned long )pend) {
        __CrestBranch(41473, 11408, 1);
        {
        __CrestLoad(41477, (unsigned long )p1, (long long )*p1);
        __CrestLoad(41476, (unsigned long )0, (long long )0U);
        __CrestApply2(41475, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 0U));
# 4596 "regex.c"
        if ((unsigned int )((re_opcode_t )*p1) == 0U) {
          __CrestBranch(41478, 11409, 1);

        } else {
          __CrestBranch(41479, 11410, 0);
# 4596 "regex.c"
          goto while_break___44;
        }
        }
      } else {
        __CrestBranch(41474, 11411, 0);
# 4596 "regex.c"
        goto while_break___44;
      }
      }
# 4597 "regex.c"
      p1 ++;
    }
    while_break___44: ;
    }
    {
    __CrestLoad(41482, (unsigned long )(& p1), (long long )((unsigned long )p1));
    __CrestLoad(41481, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(41480, 16, (long long )((unsigned long )p1 < (unsigned long )pend));
# 4599 "regex.c"
    if ((unsigned long )p1 < (unsigned long )pend) {
      __CrestBranch(41483, 11415, 1);
      {
      __CrestLoad(41487, (unsigned long )p1, (long long )*p1);
      __CrestLoad(41486, (unsigned long )0, (long long )6U);
      __CrestApply2(41485, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 6U));
# 4599 "regex.c"
      if ((unsigned int )((re_opcode_t )*p1) == 6U) {
        __CrestBranch(41488, 11416, 1);
# 4605 "regex.c"
        mem_331 = p1 + 1;
# 4605 "regex.c"
        mem_332 = p1 + 2;
        __CrestLoad(41492, (unsigned long )mem_331, (long long )*mem_331);
        __CrestLoad(41491, (unsigned long )mem_332, (long long )*mem_332);
        __CrestApply2(41490, 0, (long long )((int )*mem_331 + (int )*mem_332));
        __CrestStore(41493, (unsigned long )(& highest_active_reg));
# 4605 "regex.c"
        highest_active_reg = (active_reg_t )((int )*mem_331 + (int )*mem_332);
        {
        __CrestLoad(41496, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestLoad(41495, (unsigned long )0, (long long )((active_reg_t )((1 << 8) + 1)));
        __CrestApply2(41494, 12, (long long )(lowest_active_reg == (active_reg_t )((1 << 8) + 1)));
# 4606 "regex.c"
        if (lowest_active_reg == (active_reg_t )((1 << 8) + 1)) {
          __CrestBranch(41497, 11418, 1);
# 4607 "regex.c"
          mem_333 = p1 + 1;
          __CrestLoad(41499, (unsigned long )mem_333, (long long )*mem_333);
          __CrestStore(41500, (unsigned long )(& lowest_active_reg));
# 4607 "regex.c"
          lowest_active_reg = (active_reg_t )*mem_333;
        } else {
          __CrestBranch(41498, 11419, 0);

        }
        }
      } else {
        __CrestBranch(41489, 11420, 0);

      }
      }
    } else {
      __CrestBranch(41484, 11421, 0);

    }
    }
    {
# 4611 "regex.c"
    while (1) {
      while_continue___45: ;
      {
# 4611 "regex.c"
      while (1) {
        while_continue___46: ;
        {
        __CrestLoad(41513, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(41512, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestApply2(41511, 1, (long long )(fail_stack.size - fail_stack.avail));
        __CrestLoad(41510, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestLoad(41509, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestApply2(41508, 1, (long long )(highest_active_reg - lowest_active_reg));
        __CrestLoad(41507, (unsigned long )0, (long long )1UL);
        __CrestApply2(41506, 0, (long long )((highest_active_reg - lowest_active_reg) + 1UL));
        __CrestLoad(41505, (unsigned long )0, (long long )3UL);
        __CrestApply2(41504, 2, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL));
        __CrestLoad(41503, (unsigned long )0, (long long )4UL);
        __CrestApply2(41502, 0, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
        __CrestApply2(41501, 16, (long long )((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
# 4611 "regex.c"
        if ((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL) {
          __CrestBranch(41514, 11429, 1);

        } else {
          __CrestBranch(41515, 11430, 0);
# 4611 "regex.c"
          goto while_break___46;
        }
        }
        {
        __CrestLoad(41520, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(41519, (unsigned long )(& re_max_failures), (long long )re_max_failures);
        __CrestLoad(41518, (unsigned long )0, (long long )19);
        __CrestApply2(41517, 2, (long long )(re_max_failures * 19));
        __CrestApply2(41516, 14, (long long )(fail_stack.size > (unsigned int )(re_max_failures * 19)));
# 4611 "regex.c"
        if (fail_stack.size > (unsigned int )(re_max_failures * 19)) {
          __CrestBranch(41521, 11432, 1);
          __CrestLoad(41523, (unsigned long )0, (long long )0);
          __CrestStore(41524, (unsigned long )(& tmp___72));
# 4611 "regex.c"
          tmp___72 = 0;
        } else {
          __CrestBranch(41522, 11433, 0);
          __CrestLoad(41529, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(41528, (unsigned long )0, (long long )1);
          __CrestApply2(41527, 8, (long long )(fail_stack.size << 1));
          __CrestLoad(41526, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(41525, 2, (long long )((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t )));
# 4611 "regex.c"
          tmp___70 = __builtin_alloca((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t ));
          __CrestClearStack(41530);
# 4611 "regex.c"
          destination___1 = (char *)tmp___70;
          __CrestLoad(41533, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(41532, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(41531, 2, (long long )((unsigned long )fail_stack.size * sizeof(fail_stack_elt_t )));
# 4611 "regex.c"
          memcpy((void * __restrict )destination___1, (void const * __restrict )fail_stack.stack,
                 (unsigned long )fail_stack.size * sizeof(fail_stack_elt_t ));
          __CrestClearStack(41534);
# 4611 "regex.c"
          fail_stack.stack = (fail_stack_elt_t *)destination___1;
          {
          __CrestLoad(41537, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
          __CrestLoad(41536, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(41535, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 4611 "regex.c"
          if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
            __CrestBranch(41538, 11435, 1);
            __CrestLoad(41540, (unsigned long )0, (long long )0);
            __CrestStore(41541, (unsigned long )(& tmp___71));
# 4611 "regex.c"
            tmp___71 = 0;
          } else {
            __CrestBranch(41539, 11436, 0);
            __CrestLoad(41544, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
            __CrestLoad(41543, (unsigned long )0, (long long )1);
            __CrestApply2(41542, 8, (long long )(fail_stack.size << 1));
            __CrestStore(41545, (unsigned long )(& fail_stack.size));
# 4611 "regex.c"
            fail_stack.size <<= 1;
            __CrestLoad(41546, (unsigned long )0, (long long )1);
            __CrestStore(41547, (unsigned long )(& tmp___71));
# 4611 "regex.c"
            tmp___71 = 1;
          }
          }
          __CrestLoad(41548, (unsigned long )(& tmp___71), (long long )tmp___71);
          __CrestStore(41549, (unsigned long )(& tmp___72));
# 4611 "regex.c"
          tmp___72 = tmp___71;
        }
        }
        {
        __CrestLoad(41552, (unsigned long )(& tmp___72), (long long )tmp___72);
        __CrestLoad(41551, (unsigned long )0, (long long )0);
        __CrestApply2(41550, 13, (long long )(tmp___72 != 0));
# 4611 "regex.c"
        if (tmp___72 != 0) {
          __CrestBranch(41553, 11439, 1);

        } else {
          __CrestBranch(41554, 11440, 0);
          __CrestLoad(41555, (unsigned long )0, (long long )-2);
          __CrestStore(41556, (unsigned long )(& __retres457));
# 4611 "regex.c"
          __retres457 = -2;
# 4611 "regex.c"
          goto return_label;
        }
        }
      }
      while_break___46: ;
      }
      __CrestLoad(41557, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(41558, (unsigned long )(& this_reg___1));
# 4611 "regex.c"
      this_reg___1 = (s_reg_t )lowest_active_reg;
      {
# 4611 "regex.c"
      while (1) {
        while_continue___47: ;
        {
        __CrestLoad(41561, (unsigned long )(& this_reg___1), (long long )this_reg___1);
        __CrestLoad(41560, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestApply2(41559, 15, (long long )((active_reg_t )this_reg___1 <= highest_active_reg));
# 4611 "regex.c"
        if ((active_reg_t )this_reg___1 <= highest_active_reg) {
          __CrestBranch(41562, 11448, 1);

        } else {
          __CrestBranch(41563, 11449, 0);
# 4611 "regex.c"
          goto while_break___47;
        }
        }
        __CrestLoad(41564, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41565, (unsigned long )(& tmp___73));
# 4611 "regex.c"
        tmp___73 = fail_stack.avail;
        __CrestLoad(41568, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41567, (unsigned long )0, (long long )1U);
        __CrestApply2(41566, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41569, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
        (fail_stack.avail) ++;
# 4611 "regex.c"
        mem_334 = fail_stack.stack + tmp___73;
# 4611 "regex.c"
        mem_335 = regstart + this_reg___1;
# 4611 "regex.c"
        mem_334->pointer = (unsigned char *)*mem_335;
        __CrestLoad(41570, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41571, (unsigned long )(& tmp___74));
# 4611 "regex.c"
        tmp___74 = fail_stack.avail;
        __CrestLoad(41574, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41573, (unsigned long )0, (long long )1U);
        __CrestApply2(41572, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41575, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
        (fail_stack.avail) ++;
# 4611 "regex.c"
        mem_336 = fail_stack.stack + tmp___74;
# 4611 "regex.c"
        mem_337 = regend + this_reg___1;
# 4611 "regex.c"
        mem_336->pointer = (unsigned char *)*mem_337;
        __CrestLoad(41576, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41577, (unsigned long )(& tmp___75));
# 4611 "regex.c"
        tmp___75 = fail_stack.avail;
        __CrestLoad(41580, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41579, (unsigned long )0, (long long )1U);
        __CrestApply2(41578, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41581, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
        (fail_stack.avail) ++;
# 4611 "regex.c"
        mem_338 = fail_stack.stack + tmp___75;
# 4611 "regex.c"
        mem_339 = reg_info + this_reg___1;
# 4611 "regex.c"
        *mem_338 = mem_339->word;
        __CrestLoad(41584, (unsigned long )(& this_reg___1), (long long )this_reg___1);
        __CrestLoad(41583, (unsigned long )0, (long long )1L);
        __CrestApply2(41582, 0, (long long )(this_reg___1 + 1L));
        __CrestStore(41585, (unsigned long )(& this_reg___1));
# 4611 "regex.c"
        this_reg___1 ++;
      }
      while_break___47: ;
      }
      __CrestLoad(41586, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41587, (unsigned long )(& tmp___76));
# 4611 "regex.c"
      tmp___76 = fail_stack.avail;
      __CrestLoad(41590, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41589, (unsigned long )0, (long long )1U);
      __CrestApply2(41588, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41591, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
      (fail_stack.avail) ++;
# 4611 "regex.c"
      mem_340 = fail_stack.stack + tmp___76;
      __CrestLoad(41592, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(41593, (unsigned long )(& mem_340->integer));
# 4611 "regex.c"
      mem_340->integer = (int )lowest_active_reg;
      __CrestLoad(41594, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41595, (unsigned long )(& tmp___77));
# 4611 "regex.c"
      tmp___77 = fail_stack.avail;
      __CrestLoad(41598, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41597, (unsigned long )0, (long long )1U);
      __CrestApply2(41596, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41599, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
      (fail_stack.avail) ++;
# 4611 "regex.c"
      mem_341 = fail_stack.stack + tmp___77;
      __CrestLoad(41600, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
      __CrestStore(41601, (unsigned long )(& mem_341->integer));
# 4611 "regex.c"
      mem_341->integer = (int )highest_active_reg;
      __CrestLoad(41602, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41603, (unsigned long )(& tmp___78));
# 4611 "regex.c"
      tmp___78 = fail_stack.avail;
      __CrestLoad(41606, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41605, (unsigned long )0, (long long )1U);
      __CrestApply2(41604, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41607, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
      (fail_stack.avail) ++;
# 4611 "regex.c"
      mem_342 = fail_stack.stack + tmp___78;
# 4611 "regex.c"
      mem_342->pointer = p + mcnt;
      __CrestLoad(41608, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(41609, (unsigned long )(& tmp___79));
# 4611 "regex.c"
      tmp___79 = fail_stack.avail;
      __CrestLoad(41612, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41611, (unsigned long )0, (long long )1U);
      __CrestApply2(41610, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(41613, (unsigned long )(& fail_stack.avail));
# 4611 "regex.c"
      (fail_stack.avail) ++;
# 4611 "regex.c"
      mem_343 = fail_stack.stack + tmp___79;
# 4611 "regex.c"
      mem_343->pointer = (unsigned char *)d;
# 4611 "regex.c"
      goto while_break___45;
    }
    while_break___45: ;
    }
# 4612 "regex.c"
    goto switch_break;
    case_18___0:
    {
# 4618 "regex.c"
    while (1) {
      while_continue___48: ;
      {
# 4618 "regex.c"
      while (1) {
        while_continue___49: ;
        __CrestLoad(41616, (unsigned long )p, (long long )*p);
        __CrestLoad(41615, (unsigned long )0, (long long )255);
        __CrestApply2(41614, 5, (long long )((int )*p & 255));
        __CrestStore(41617, (unsigned long )(& mcnt));
# 4618 "regex.c"
        mcnt = (int )*p & 255;
# 4618 "regex.c"
        mem_344 = p + 1;
        __CrestLoad(41622, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(41621, (unsigned long )mem_344, (long long )*mem_344);
        __CrestLoad(41620, (unsigned long )0, (long long )8);
        __CrestApply2(41619, 8, (long long )((int )((signed char )*mem_344) << 8));
        __CrestApply2(41618, 0, (long long )(mcnt + ((int )((signed char )*mem_344) << 8)));
        __CrestStore(41623, (unsigned long )(& mcnt));
# 4618 "regex.c"
        mcnt += (int )((signed char )*mem_344) << 8;
# 4618 "regex.c"
        goto while_break___49;
      }
      while_break___49: ;
      }
# 4618 "regex.c"
      p += 2;
# 4618 "regex.c"
      goto while_break___48;
    }
    while_break___48: ;
    }
# 4621 "regex.c"
    p2 = p;
    {
# 4640 "regex.c"
    while (1) {
      while_continue___50: ;
      {
      __CrestLoad(41628, (unsigned long )(& p2), (long long )((unsigned long )p2));
      __CrestLoad(41627, (unsigned long )0, (long long )2);
      __CrestApply2(41626, 18, (long long )((unsigned long )(p2 + 2)));
      __CrestLoad(41625, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(41624, 16, (long long )((unsigned long )(p2 + 2) < (unsigned long )pend));
# 4642 "regex.c"
      if ((unsigned long )(p2 + 2) < (unsigned long )pend) {
        __CrestBranch(41629, 11473, 1);
        {
        __CrestLoad(41633, (unsigned long )p2, (long long )*p2);
        __CrestLoad(41632, (unsigned long )0, (long long )7U);
        __CrestApply2(41631, 12, (long long )((unsigned int )((re_opcode_t )*p2) == 7U));
# 4642 "regex.c"
        if ((unsigned int )((re_opcode_t )*p2) == 7U) {
          __CrestBranch(41634, 11474, 1);
# 4645 "regex.c"
          p2 += 3;
        } else {
          __CrestBranch(41635, 11475, 0);
          {
          __CrestLoad(41638, (unsigned long )p2, (long long )*p2);
          __CrestLoad(41637, (unsigned long )0, (long long )6U);
          __CrestApply2(41636, 12, (long long )((unsigned int )((re_opcode_t )*p2) == 6U));
# 4642 "regex.c"
          if ((unsigned int )((re_opcode_t )*p2) == 6U) {
            __CrestBranch(41639, 11476, 1);
# 4645 "regex.c"
            p2 += 3;
          } else {
            __CrestBranch(41640, 11477, 0);
# 4642 "regex.c"
            goto _L___14;
          }
          }
        }
        }
      } else {
        __CrestBranch(41630, 11478, 0);
        _L___14:
        {
        __CrestLoad(41645, (unsigned long )(& p2), (long long )((unsigned long )p2));
        __CrestLoad(41644, (unsigned long )0, (long long )6);
        __CrestApply2(41643, 18, (long long )((unsigned long )(p2 + 6)));
        __CrestLoad(41642, (unsigned long )(& pend), (long long )((unsigned long )pend));
        __CrestApply2(41641, 16, (long long )((unsigned long )(p2 + 6) < (unsigned long )pend));
# 4646 "regex.c"
        if ((unsigned long )(p2 + 6) < (unsigned long )pend) {
          __CrestBranch(41646, 11479, 1);
          {
          __CrestLoad(41650, (unsigned long )p2, (long long )*p2);
          __CrestLoad(41649, (unsigned long )0, (long long )19U);
          __CrestApply2(41648, 12, (long long )((unsigned int )((re_opcode_t )*p2) == 19U));
# 4646 "regex.c"
          if ((unsigned int )((re_opcode_t )*p2) == 19U) {
            __CrestBranch(41651, 11480, 1);
# 4648 "regex.c"
            p2 += 6;
          } else {
            __CrestBranch(41652, 11481, 0);
# 4650 "regex.c"
            goto while_break___50;
          }
          }
        } else {
          __CrestBranch(41647, 11482, 0);
# 4650 "regex.c"
          goto while_break___50;
        }
        }
      }
      }
    }
    while_break___50: ;
    }
# 4653 "regex.c"
    p1 = p + mcnt;
    {
    __CrestLoad(41655, (unsigned long )(& p2), (long long )((unsigned long )p2));
    __CrestLoad(41654, (unsigned long )(& pend), (long long )((unsigned long )pend));
    __CrestApply2(41653, 12, (long long )((unsigned long )p2 == (unsigned long )pend));
# 4659 "regex.c"
    if ((unsigned long )p2 == (unsigned long )pend) {
      __CrestBranch(41656, 11486, 1);
# 4664 "regex.c"
      mem_345 = p + -3;
      __CrestLoad(41658, (unsigned long )0, (long long )(unsigned char)17);
      __CrestStore(41659, (unsigned long )mem_345);
# 4664 "regex.c"
      *mem_345 = (unsigned char)17;
    } else {
      __CrestBranch(41657, 11487, 0);
      {
      __CrestLoad(41662, (unsigned long )p2, (long long )*p2);
      __CrestLoad(41661, (unsigned long )0, (long long )2U);
      __CrestApply2(41660, 12, (long long )((unsigned int )((re_opcode_t )*p2) == 2U));
# 4669 "regex.c"
      if ((unsigned int )((re_opcode_t )*p2) == 2U) {
        __CrestBranch(41663, 11488, 1);
# 4669 "regex.c"
        goto _L___18;
      } else {
        __CrestBranch(41664, 11489, 0);
        {
        __CrestLoad(41667, (unsigned long )0, (long long )bufp->newline_anchor);
        __CrestLoad(41666, (unsigned long )0, (long long )0);
        __CrestApply2(41665, 13, (long long )(bufp->newline_anchor != 0));
# 4669 "regex.c"
        if (bufp->newline_anchor != 0) {
          __CrestBranch(41668, 11490, 1);
          {
          __CrestLoad(41672, (unsigned long )p2, (long long )*p2);
          __CrestLoad(41671, (unsigned long )0, (long long )10U);
          __CrestApply2(41670, 12, (long long )((unsigned int )((re_opcode_t )*p2) == 10U));
# 4669 "regex.c"
          if ((unsigned int )((re_opcode_t )*p2) == 10U) {
            __CrestBranch(41673, 11491, 1);
            _L___18:
            {
            __CrestLoad(41677, (unsigned long )p2, (long long )*p2);
            __CrestLoad(41676, (unsigned long )0, (long long )10);
            __CrestApply2(41675, 12, (long long )((int )*p2 == 10));
# 4672 "regex.c"
            if ((int )*p2 == 10) {
              __CrestBranch(41678, 11492, 1);
              __CrestLoad(41680, (unsigned long )0, (long long )'\n');
              __CrestStore(41681, (unsigned long )(& tmp___80));
# 4672 "regex.c"
              tmp___80 = '\n';
            } else {
              __CrestBranch(41679, 11493, 0);
# 4672 "regex.c"
              mem_346 = p2 + 2;
              __CrestLoad(41682, (unsigned long )mem_346, (long long )*mem_346);
              __CrestStore(41683, (unsigned long )(& tmp___80));
# 4672 "regex.c"
              tmp___80 = (int )*mem_346;
            }
            }
            __CrestLoad(41684, (unsigned long )(& tmp___80), (long long )tmp___80);
            __CrestStore(41685, (unsigned long )(& c___0));
# 4672 "regex.c"
            c___0 = (unsigned char )tmp___80;
            {
# 4675 "regex.c"
            mem_347 = p1 + 3;
            {
            __CrestLoad(41688, (unsigned long )mem_347, (long long )*mem_347);
            __CrestLoad(41687, (unsigned long )0, (long long )2U);
            __CrestApply2(41686, 12, (long long )((unsigned int )((re_opcode_t )*mem_347) == 2U));
# 4675 "regex.c"
            if ((unsigned int )((re_opcode_t )*mem_347) == 2U) {
              __CrestBranch(41689, 11498, 1);
              {
# 4675 "regex.c"
              mem_348 = p1 + 5;
              {
              __CrestLoad(41693, (unsigned long )mem_348, (long long )*mem_348);
              __CrestLoad(41692, (unsigned long )(& c___0), (long long )c___0);
              __CrestApply2(41691, 13, (long long )((int )*mem_348 != (int )c___0));
# 4675 "regex.c"
              if ((int )*mem_348 != (int )c___0) {
                __CrestBranch(41694, 11501, 1);
# 4677 "regex.c"
                mem_349 = p + -3;
                __CrestLoad(41696, (unsigned long )0, (long long )(unsigned char)17);
                __CrestStore(41697, (unsigned long )mem_349);
# 4677 "regex.c"
                *mem_349 = (unsigned char)17;
              } else {
                __CrestBranch(41695, 11502, 0);
# 4675 "regex.c"
                goto _L___16;
              }
              }
              }
            } else {
              __CrestBranch(41690, 11503, 0);
              _L___16:
              {
# 4682 "regex.c"
              mem_350 = p1 + 3;
              {
              __CrestLoad(41700, (unsigned long )mem_350, (long long )*mem_350);
              __CrestLoad(41699, (unsigned long )0, (long long )4U);
              __CrestApply2(41698, 12, (long long )((unsigned int )((re_opcode_t )*mem_350) == 4U));
# 4682 "regex.c"
              if ((unsigned int )((re_opcode_t )*mem_350) == 4U) {
                __CrestBranch(41701, 11506, 1);
# 4682 "regex.c"
                goto _L___15;
              } else {
                __CrestBranch(41702, 11507, 0);
                {
# 4682 "regex.c"
                mem_351 = p1 + 3;
                {
                __CrestLoad(41705, (unsigned long )mem_351, (long long )*mem_351);
                __CrestLoad(41704, (unsigned long )0, (long long )5U);
                __CrestApply2(41703, 12, (long long )((unsigned int )((re_opcode_t )*mem_351) == 5U));
# 4682 "regex.c"
                if ((unsigned int )((re_opcode_t )*mem_351) == 5U) {
                  __CrestBranch(41706, 11510, 1);
                  _L___15:
# 4685 "regex.c"
                  mem_352 = p1 + 3;
                  __CrestLoad(41710, (unsigned long )mem_352, (long long )*mem_352);
                  __CrestLoad(41709, (unsigned long )0, (long long )5U);
                  __CrestApply2(41708, 12, (long long )((unsigned int )((re_opcode_t )*mem_352) == 5U));
                  __CrestStore(41711, (unsigned long )(& not___0));
# 4685 "regex.c"
                  not___0 = (unsigned int )((re_opcode_t )*mem_352) == 5U;
                  {
# 4687 "regex.c"
                  mem_353 = p1 + 4;
                  {
                  __CrestLoad(41716, (unsigned long )(& c___0), (long long )c___0);
                  __CrestLoad(41715, (unsigned long )mem_353, (long long )*mem_353);
                  __CrestLoad(41714, (unsigned long )0, (long long )8);
                  __CrestApply2(41713, 2, (long long )((int )*mem_353 * 8));
                  __CrestApply2(41712, 16, (long long )((int )c___0 < (int )((unsigned char )((int )*mem_353 * 8))));
# 4687 "regex.c"
                  if ((int )c___0 < (int )((unsigned char )((int )*mem_353 * 8))) {
                    __CrestBranch(41717, 11514, 1);
                    {
# 4687 "regex.c"
                    mem_354 = p1 + (5 + (int )c___0 / 8);
                    {
                    __CrestLoad(41727, (unsigned long )mem_354, (long long )*mem_354);
                    __CrestLoad(41726, (unsigned long )0, (long long )1);
                    __CrestLoad(41725, (unsigned long )(& c___0), (long long )c___0);
                    __CrestLoad(41724, (unsigned long )0, (long long )8);
                    __CrestApply2(41723, 4, (long long )((int )c___0 % 8));
                    __CrestApply2(41722, 8, (long long )(1 << (int )c___0 % 8));
                    __CrestApply2(41721, 5, (long long )((int )*mem_354 & (1 << (int )c___0 % 8)));
                    __CrestLoad(41720, (unsigned long )0, (long long )0);
                    __CrestApply2(41719, 13, (long long )(((int )*mem_354 & (1 << (int )c___0 % 8)) != 0));
# 4687 "regex.c"
                    if (((int )*mem_354 & (1 << (int )c___0 % 8)) != 0) {
                      __CrestBranch(41728, 11517, 1);
                      __CrestLoad(41731, (unsigned long )(& not___0), (long long )not___0);
                      __CrestApply1(41730, 21, (long long )(! not___0));
                      __CrestStore(41732, (unsigned long )(& not___0));
# 4689 "regex.c"
                      not___0 = ! not___0;
                    } else {
                      __CrestBranch(41729, 11518, 0);

                    }
                    }
                    }
                  } else {
                    __CrestBranch(41718, 11519, 0);

                  }
                  }
                  }
                  {
                  __CrestLoad(41735, (unsigned long )(& not___0), (long long )not___0);
                  __CrestLoad(41734, (unsigned long )0, (long long )0);
                  __CrestApply2(41733, 12, (long long )(not___0 == 0));
# 4693 "regex.c"
                  if (not___0 == 0) {
                    __CrestBranch(41736, 11521, 1);
# 4695 "regex.c"
                    mem_355 = p + -3;
                    __CrestLoad(41738, (unsigned long )0, (long long )(unsigned char)17);
                    __CrestStore(41739, (unsigned long )mem_355);
# 4695 "regex.c"
                    *mem_355 = (unsigned char)17;
                  } else {
                    __CrestBranch(41737, 11522, 0);

                  }
                  }
                } else {
                  __CrestBranch(41707, 11523, 0);

                }
                }
                }
              }
              }
              }
            }
            }
            }
          } else {
            __CrestBranch(41674, 11524, 0);
# 4669 "regex.c"
            goto _L___19;
          }
          }
        } else {
          __CrestBranch(41669, 11525, 0);
          _L___19:
          {
          __CrestLoad(41742, (unsigned long )p2, (long long )*p2);
          __CrestLoad(41741, (unsigned long )0, (long long )4U);
          __CrestApply2(41740, 12, (long long )((unsigned int )((re_opcode_t )*p2) == 4U));
# 4700 "regex.c"
          if ((unsigned int )((re_opcode_t )*p2) == 4U) {
            __CrestBranch(41743, 11526, 1);
            {
# 4713 "regex.c"
            mem_356 = p1 + 3;
            {
            __CrestLoad(41747, (unsigned long )mem_356, (long long )*mem_356);
            __CrestLoad(41746, (unsigned long )0, (long long )2U);
            __CrestApply2(41745, 12, (long long )((unsigned int )((re_opcode_t )*mem_356) == 2U));
# 4713 "regex.c"
            if ((unsigned int )((re_opcode_t )*mem_356) == 2U) {
              __CrestBranch(41748, 11529, 1);
              {
# 4713 "regex.c"
              mem_357 = p2 + 1;
# 4713 "regex.c"
              mem_358 = p1 + 4;
              {
              __CrestLoad(41754, (unsigned long )mem_357, (long long )*mem_357);
              __CrestLoad(41753, (unsigned long )0, (long long )8);
              __CrestApply2(41752, 2, (long long )((int )*mem_357 * 8));
              __CrestLoad(41751, (unsigned long )mem_358, (long long )*mem_358);
              __CrestApply2(41750, 14, (long long )((int )*mem_357 * 8 > (int )*mem_358));
# 4713 "regex.c"
              if ((int )*mem_357 * 8 > (int )*mem_358) {
                __CrestBranch(41755, 11532, 1);
                {
# 4713 "regex.c"
                mem_359 = p1 + 4;
# 4713 "regex.c"
                mem_360 = p2 + (2 + (int )*mem_359 / 8);
# 4713 "regex.c"
                mem_361 = p1 + 4;
                {
                __CrestLoad(41765, (unsigned long )mem_360, (long long )*mem_360);
                __CrestLoad(41764, (unsigned long )0, (long long )1);
                __CrestLoad(41763, (unsigned long )mem_361, (long long )*mem_361);
                __CrestLoad(41762, (unsigned long )0, (long long )8);
                __CrestApply2(41761, 4, (long long )((int )*mem_361 % 8));
                __CrestApply2(41760, 8, (long long )(1 << (int )*mem_361 % 8));
                __CrestApply2(41759, 5, (long long )((int )*mem_360 & (1 << (int )*mem_361 % 8)));
                __CrestLoad(41758, (unsigned long )0, (long long )0);
                __CrestApply2(41757, 13, (long long )(((int )*mem_360 & (1 << (int )*mem_361 % 8)) != 0));
# 4713 "regex.c"
                if (((int )*mem_360 & (1 << (int )*mem_361 % 8)) != 0) {
                  __CrestBranch(41766, 11535, 1);
# 4713 "regex.c"
                  goto _L___17;
                } else {
                  __CrestBranch(41767, 11536, 0);
# 4719 "regex.c"
                  mem_362 = p + -3;
                  __CrestLoad(41768, (unsigned long )0, (long long )(unsigned char)17);
                  __CrestStore(41769, (unsigned long )mem_362);
# 4719 "regex.c"
                  *mem_362 = (unsigned char)17;
                }
                }
                }
              } else {
                __CrestBranch(41756, 11537, 0);
# 4719 "regex.c"
                mem_363 = p + -3;
                __CrestLoad(41770, (unsigned long )0, (long long )(unsigned char)17);
                __CrestStore(41771, (unsigned long )mem_363);
# 4719 "regex.c"
                *mem_363 = (unsigned char)17;
              }
              }
              }
            } else {
              __CrestBranch(41749, 11538, 0);
              _L___17:
              {
# 4724 "regex.c"
              mem_364 = p1 + 3;
              {
              __CrestLoad(41774, (unsigned long )mem_364, (long long )*mem_364);
              __CrestLoad(41773, (unsigned long )0, (long long )5U);
              __CrestApply2(41772, 12, (long long )((unsigned int )((re_opcode_t )*mem_364) == 5U));
# 4724 "regex.c"
              if ((unsigned int )((re_opcode_t )*mem_364) == 5U) {
                __CrestBranch(41775, 11541, 1);
                __CrestLoad(41777, (unsigned long )0, (long long )0);
                __CrestStore(41778, (unsigned long )(& idx));
# 4729 "regex.c"
                idx = 0;
                {
# 4729 "regex.c"
                while (1) {
                  while_continue___51: ;
                  {
# 4729 "regex.c"
                  mem_365 = p2 + 1;
                  {
                  __CrestLoad(41781, (unsigned long )(& idx), (long long )idx);
                  __CrestLoad(41780, (unsigned long )mem_365, (long long )*mem_365);
                  __CrestApply2(41779, 16, (long long )(idx < (int )*mem_365));
# 4729 "regex.c"
                  if (idx < (int )*mem_365) {
                    __CrestBranch(41782, 11548, 1);

                  } else {
                    __CrestBranch(41783, 11549, 0);
# 4729 "regex.c"
                    goto while_break___51;
                  }
                  }
                  }
                  {
# 4730 "regex.c"
                  mem_366 = p2 + (2 + idx);
                  {
                  __CrestLoad(41786, (unsigned long )mem_366, (long long )*mem_366);
                  __CrestLoad(41785, (unsigned long )0, (long long )0);
                  __CrestApply2(41784, 12, (long long )((int )*mem_366 == 0));
# 4730 "regex.c"
                  if ((int )*mem_366 == 0) {
                    __CrestBranch(41787, 11553, 1);

                  } else {
                    __CrestBranch(41788, 11554, 0);
                    {
# 4730 "regex.c"
                    mem_367 = p1 + 4;
                    {
                    __CrestLoad(41791, (unsigned long )(& idx), (long long )idx);
                    __CrestLoad(41790, (unsigned long )mem_367, (long long )*mem_367);
                    __CrestApply2(41789, 16, (long long )(idx < (int )*mem_367));
# 4730 "regex.c"
                    if (idx < (int )*mem_367) {
                      __CrestBranch(41792, 11557, 1);
                      {
# 4730 "regex.c"
                      mem_368 = p2 + (2 + idx);
# 4730 "regex.c"
                      mem_369 = p1 + (5 + idx);
                      {
                      __CrestLoad(41799, (unsigned long )mem_368, (long long )*mem_368);
                      __CrestLoad(41798, (unsigned long )mem_369, (long long )*mem_369);
                      __CrestApply1(41797, 20, (long long )(~ ((int )*mem_369)));
                      __CrestApply2(41796, 5, (long long )((int )*mem_368 & ~ ((int )*mem_369)));
                      __CrestLoad(41795, (unsigned long )0, (long long )0);
                      __CrestApply2(41794, 12, (long long )(((int )*mem_368 & ~ ((int )*mem_369)) == 0));
# 4730 "regex.c"
                      if (((int )*mem_368 & ~ ((int )*mem_369)) == 0) {
                        __CrestBranch(41800, 11560, 1);

                      } else {
                        __CrestBranch(41801, 11561, 0);
# 4733 "regex.c"
                        goto while_break___51;
                      }
                      }
                      }
                    } else {
                      __CrestBranch(41793, 11562, 0);
# 4733 "regex.c"
                      goto while_break___51;
                    }
                    }
                    }
                  }
                  }
                  }
                  __CrestLoad(41804, (unsigned long )(& idx), (long long )idx);
                  __CrestLoad(41803, (unsigned long )0, (long long )1);
                  __CrestApply2(41802, 0, (long long )(idx + 1));
                  __CrestStore(41805, (unsigned long )(& idx));
# 4729 "regex.c"
                  idx ++;
                }
                while_break___51: ;
                }
                {
# 4735 "regex.c"
                mem_370 = p2 + 1;
                {
                __CrestLoad(41808, (unsigned long )(& idx), (long long )idx);
                __CrestLoad(41807, (unsigned long )mem_370, (long long )*mem_370);
                __CrestApply2(41806, 12, (long long )(idx == (int )*mem_370));
# 4735 "regex.c"
                if (idx == (int )*mem_370) {
                  __CrestBranch(41809, 11568, 1);
# 4737 "regex.c"
                  mem_371 = p + -3;
                  __CrestLoad(41811, (unsigned long )0, (long long )(unsigned char)17);
                  __CrestStore(41812, (unsigned long )mem_371);
# 4737 "regex.c"
                  *mem_371 = (unsigned char)17;
                } else {
                  __CrestBranch(41810, 11569, 0);

                }
                }
                }
              } else {
                __CrestBranch(41776, 11570, 0);
                {
# 4741 "regex.c"
                mem_372 = p1 + 3;
                {
                __CrestLoad(41815, (unsigned long )mem_372, (long long )*mem_372);
                __CrestLoad(41814, (unsigned long )0, (long long )4U);
                __CrestApply2(41813, 12, (long long )((unsigned int )((re_opcode_t )*mem_372) == 4U));
# 4741 "regex.c"
                if ((unsigned int )((re_opcode_t )*mem_372) == 4U) {
                  __CrestBranch(41816, 11573, 1);
                  __CrestLoad(41818, (unsigned long )0, (long long )0);
                  __CrestStore(41819, (unsigned long )(& idx___0));
# 4746 "regex.c"
                  idx___0 = 0;
                  {
# 4746 "regex.c"
                  while (1) {
                    while_continue___52: ;
                    {
# 4746 "regex.c"
                    mem_373 = p2 + 1;
                    {
                    __CrestLoad(41822, (unsigned long )(& idx___0), (long long )idx___0);
                    __CrestLoad(41821, (unsigned long )mem_373, (long long )*mem_373);
                    __CrestApply2(41820, 16, (long long )(idx___0 < (int )*mem_373));
# 4746 "regex.c"
                    if (idx___0 < (int )*mem_373) {
                      __CrestBranch(41823, 11580, 1);
                      {
# 4746 "regex.c"
                      mem_374 = p1 + 4;
                      {
                      __CrestLoad(41827, (unsigned long )(& idx___0), (long long )idx___0);
                      __CrestLoad(41826, (unsigned long )mem_374, (long long )*mem_374);
                      __CrestApply2(41825, 16, (long long )(idx___0 < (int )*mem_374));
# 4746 "regex.c"
                      if (idx___0 < (int )*mem_374) {
                        __CrestBranch(41828, 11583, 1);

                      } else {
                        __CrestBranch(41829, 11584, 0);
# 4746 "regex.c"
                        goto while_break___52;
                      }
                      }
                      }
                    } else {
                      __CrestBranch(41824, 11585, 0);
# 4746 "regex.c"
                      goto while_break___52;
                    }
                    }
                    }
                    {
# 4749 "regex.c"
                    mem_375 = p2 + (2 + idx___0);
# 4749 "regex.c"
                    mem_376 = p1 + (5 + idx___0);
                    {
                    __CrestLoad(41834, (unsigned long )mem_375, (long long )*mem_375);
                    __CrestLoad(41833, (unsigned long )mem_376, (long long )*mem_376);
                    __CrestApply2(41832, 5, (long long )((int )*mem_375 & (int )*mem_376));
                    __CrestLoad(41831, (unsigned long )0, (long long )0);
                    __CrestApply2(41830, 13, (long long )(((int )*mem_375 & (int )*mem_376) != 0));
# 4749 "regex.c"
                    if (((int )*mem_375 & (int )*mem_376) != 0) {
                      __CrestBranch(41835, 11589, 1);
# 4750 "regex.c"
                      goto while_break___52;
                    } else {
                      __CrestBranch(41836, 11590, 0);

                    }
                    }
                    }
                    __CrestLoad(41839, (unsigned long )(& idx___0), (long long )idx___0);
                    __CrestLoad(41838, (unsigned long )0, (long long )1);
                    __CrestApply2(41837, 0, (long long )(idx___0 + 1));
                    __CrestStore(41840, (unsigned long )(& idx___0));
# 4746 "regex.c"
                    idx___0 ++;
                  }
                  while_break___52: ;
                  }
                  {
# 4752 "regex.c"
                  mem_377 = p2 + 1;
                  {
                  __CrestLoad(41843, (unsigned long )(& idx___0), (long long )idx___0);
                  __CrestLoad(41842, (unsigned long )mem_377, (long long )*mem_377);
                  __CrestApply2(41841, 12, (long long )(idx___0 == (int )*mem_377));
# 4752 "regex.c"
                  if (idx___0 == (int )*mem_377) {
                    __CrestBranch(41844, 11596, 1);
# 4754 "regex.c"
                    mem_378 = p + -3;
                    __CrestLoad(41846, (unsigned long )0, (long long )(unsigned char)17);
                    __CrestStore(41847, (unsigned long )mem_378);
# 4754 "regex.c"
                    *mem_378 = (unsigned char)17;
                  } else {
                    __CrestBranch(41845, 11597, 0);
                    {
# 4752 "regex.c"
                    mem_379 = p1 + 4;
                    {
                    __CrestLoad(41850, (unsigned long )(& idx___0), (long long )idx___0);
                    __CrestLoad(41849, (unsigned long )mem_379, (long long )*mem_379);
                    __CrestApply2(41848, 12, (long long )(idx___0 == (int )*mem_379));
# 4752 "regex.c"
                    if (idx___0 == (int )*mem_379) {
                      __CrestBranch(41851, 11600, 1);
# 4754 "regex.c"
                      mem_380 = p + -3;
                      __CrestLoad(41853, (unsigned long )0, (long long )(unsigned char)17);
                      __CrestStore(41854, (unsigned long )mem_380);
# 4754 "regex.c"
                      *mem_380 = (unsigned char)17;
                    } else {
                      __CrestBranch(41852, 11601, 0);

                    }
                    }
                    }
                  }
                  }
                  }
                } else {
                  __CrestBranch(41817, 11602, 0);

                }
                }
                }
              }
              }
              }
            }
            }
            }
          } else {
            __CrestBranch(41744, 11603, 0);

          }
          }
        }
        }
      }
      }
    }
    }
# 4760 "regex.c"
    p -= 2;
    {
# 4761 "regex.c"
    mem_381 = p + -1;
    {
    __CrestLoad(41857, (unsigned long )mem_381, (long long )*mem_381);
    __CrestLoad(41856, (unsigned long )0, (long long )17U);
    __CrestApply2(41855, 13, (long long )((unsigned int )((re_opcode_t )*mem_381) != 17U));
# 4761 "regex.c"
    if ((unsigned int )((re_opcode_t )*mem_381) != 17U) {
      __CrestBranch(41858, 11608, 1);
# 4763 "regex.c"
      mem_382 = p + -1;
      __CrestLoad(41860, (unsigned long )0, (long long )(unsigned char)13);
      __CrestStore(41861, (unsigned long )mem_382);
# 4763 "regex.c"
      *mem_382 = (unsigned char)13;
# 4765 "regex.c"
      goto unconditional_jump;
    } else {
      __CrestBranch(41859, 11610, 0);

    }
    }
    }
    case_17___0:
    __CrestLoad(41864, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
    __CrestLoad(41863, (unsigned long )0, (long long )1U);
    __CrestApply2(41862, 1, (long long )(fail_stack.avail - 1U));
    __CrestStore(41865, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
    (fail_stack.avail) --;
# 4788 "regex.c"
    mem_383 = fail_stack.stack + fail_stack.avail;
# 4788 "regex.c"
    string_temp = (unsigned char const *)mem_383->pointer;
    {
    __CrestLoad(41868, (unsigned long )(& string_temp), (long long )((unsigned long )string_temp));
    __CrestLoad(41867, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(41866, 13, (long long )((unsigned long )string_temp != (unsigned long )((void *)0)));
# 4788 "regex.c"
    if ((unsigned long )string_temp != (unsigned long )((void *)0)) {
      __CrestBranch(41869, 11613, 1);
# 4788 "regex.c"
      sdummy = (char const *)string_temp;
    } else {
      __CrestBranch(41870, 11614, 0);

    }
    }
    __CrestLoad(41873, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
    __CrestLoad(41872, (unsigned long )0, (long long )1U);
    __CrestApply2(41871, 1, (long long )(fail_stack.avail - 1U));
    __CrestStore(41874, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
    (fail_stack.avail) --;
# 4788 "regex.c"
    mem_384 = fail_stack.stack + fail_stack.avail;
# 4788 "regex.c"
    pdummy = mem_384->pointer;
    __CrestLoad(41877, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
    __CrestLoad(41876, (unsigned long )0, (long long )1U);
    __CrestApply2(41875, 1, (long long )(fail_stack.avail - 1U));
    __CrestStore(41878, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
    (fail_stack.avail) --;
# 4788 "regex.c"
    mem_385 = fail_stack.stack + fail_stack.avail;
    __CrestLoad(41879, (unsigned long )(& mem_385->integer), (long long )mem_385->integer);
    __CrestStore(41880, (unsigned long )(& dummy_high_reg));
# 4788 "regex.c"
    dummy_high_reg = (active_reg_t )mem_385->integer;
    __CrestLoad(41883, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
    __CrestLoad(41882, (unsigned long )0, (long long )1U);
    __CrestApply2(41881, 1, (long long )(fail_stack.avail - 1U));
    __CrestStore(41884, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
    (fail_stack.avail) --;
# 4788 "regex.c"
    mem_386 = fail_stack.stack + fail_stack.avail;
    __CrestLoad(41885, (unsigned long )(& mem_386->integer), (long long )mem_386->integer);
    __CrestStore(41886, (unsigned long )(& dummy_low_reg));
# 4788 "regex.c"
    dummy_low_reg = (active_reg_t )mem_386->integer;
    __CrestLoad(41887, (unsigned long )(& dummy_high_reg), (long long )dummy_high_reg);
    __CrestStore(41888, (unsigned long )(& this_reg___2));
# 4788 "regex.c"
    this_reg___2 = (s_reg_t )dummy_high_reg;
    {
# 4788 "regex.c"
    while (1) {
      while_continue___53: ;
      {
      __CrestLoad(41891, (unsigned long )(& this_reg___2), (long long )this_reg___2);
      __CrestLoad(41890, (unsigned long )(& dummy_low_reg), (long long )dummy_low_reg);
      __CrestApply2(41889, 17, (long long )((active_reg_t )this_reg___2 >= dummy_low_reg));
# 4788 "regex.c"
      if ((active_reg_t )this_reg___2 >= dummy_low_reg) {
        __CrestBranch(41892, 11620, 1);

      } else {
        __CrestBranch(41893, 11621, 0);
# 4788 "regex.c"
        goto while_break___53;
      }
      }
      __CrestLoad(41896, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41895, (unsigned long )0, (long long )1U);
      __CrestApply2(41894, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(41897, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
      (fail_stack.avail) --;
# 4788 "regex.c"
      mem_387 = reg_info_dummy + this_reg___2;
# 4788 "regex.c"
      mem_388 = fail_stack.stack + fail_stack.avail;
# 4788 "regex.c"
      mem_387->word = *mem_388;
      __CrestLoad(41900, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41899, (unsigned long )0, (long long )1U);
      __CrestApply2(41898, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(41901, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
      (fail_stack.avail) --;
# 4788 "regex.c"
      mem_389 = reg_dummy + this_reg___2;
# 4788 "regex.c"
      mem_390 = fail_stack.stack + fail_stack.avail;
# 4788 "regex.c"
      *mem_389 = (char const *)mem_390->pointer;
      __CrestLoad(41904, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(41903, (unsigned long )0, (long long )1U);
      __CrestApply2(41902, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(41905, (unsigned long )(& fail_stack.avail));
# 4788 "regex.c"
      (fail_stack.avail) --;
# 4788 "regex.c"
      mem_391 = reg_dummy + this_reg___2;
# 4788 "regex.c"
      mem_392 = fail_stack.stack + fail_stack.avail;
# 4788 "regex.c"
      *mem_391 = (char const *)mem_392->pointer;
      __CrestLoad(41908, (unsigned long )(& this_reg___2), (long long )this_reg___2);
      __CrestLoad(41907, (unsigned long )0, (long long )1L);
      __CrestApply2(41906, 1, (long long )(this_reg___2 - 1L));
      __CrestStore(41909, (unsigned long )(& this_reg___2));
# 4788 "regex.c"
      this_reg___2 --;
    }
    while_break___53: ;
    }
    __CrestLoad(41910, (unsigned long )0, (long long )0);
    __CrestStore(41911, (unsigned long )(& set_regs_matched_done));
# 4788 "regex.c"
    set_regs_matched_done = 0;
    unconditional_jump: ;
    case_13___0:
    {
# 4804 "regex.c"
    while (1) {
      while_continue___54: ;
      {
# 4804 "regex.c"
      while (1) {
        while_continue___55: ;
        __CrestLoad(41914, (unsigned long )p, (long long )*p);
        __CrestLoad(41913, (unsigned long )0, (long long )255);
        __CrestApply2(41912, 5, (long long )((int )*p & 255));
        __CrestStore(41915, (unsigned long )(& mcnt));
# 4804 "regex.c"
        mcnt = (int )*p & 255;
# 4804 "regex.c"
        mem_393 = p + 1;
        __CrestLoad(41920, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(41919, (unsigned long )mem_393, (long long )*mem_393);
        __CrestLoad(41918, (unsigned long )0, (long long )8);
        __CrestApply2(41917, 8, (long long )((int )((signed char )*mem_393) << 8));
        __CrestApply2(41916, 0, (long long )(mcnt + ((int )((signed char )*mem_393) << 8)));
        __CrestStore(41921, (unsigned long )(& mcnt));
# 4804 "regex.c"
        mcnt += (int )((signed char )*mem_393) << 8;
# 4804 "regex.c"
        goto while_break___55;
      }
      while_break___55: ;
      }
# 4804 "regex.c"
      p += 2;
# 4804 "regex.c"
      goto while_break___54;
    }
    while_break___54: ;
    }
# 4806 "regex.c"
    p += mcnt;
# 4812 "regex.c"
    goto switch_break;
    case_14: ;
# 4819 "regex.c"
    goto unconditional_jump;
    case_19___0: ;
    {
# 4831 "regex.c"
    while (1) {
      while_continue___56: ;
      {
# 4831 "regex.c"
      while (1) {
        while_continue___57: ;
        {
        __CrestLoad(41934, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(41933, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestApply2(41932, 1, (long long )(fail_stack.size - fail_stack.avail));
        __CrestLoad(41931, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestLoad(41930, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestApply2(41929, 1, (long long )(highest_active_reg - lowest_active_reg));
        __CrestLoad(41928, (unsigned long )0, (long long )1UL);
        __CrestApply2(41927, 0, (long long )((highest_active_reg - lowest_active_reg) + 1UL));
        __CrestLoad(41926, (unsigned long )0, (long long )3UL);
        __CrestApply2(41925, 2, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL));
        __CrestLoad(41924, (unsigned long )0, (long long )4UL);
        __CrestApply2(41923, 0, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
        __CrestApply2(41922, 16, (long long )((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
# 4831 "regex.c"
        if ((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL) {
          __CrestBranch(41935, 11650, 1);

        } else {
          __CrestBranch(41936, 11651, 0);
# 4831 "regex.c"
          goto while_break___57;
        }
        }
        {
        __CrestLoad(41941, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(41940, (unsigned long )(& re_max_failures), (long long )re_max_failures);
        __CrestLoad(41939, (unsigned long )0, (long long )19);
        __CrestApply2(41938, 2, (long long )(re_max_failures * 19));
        __CrestApply2(41937, 14, (long long )(fail_stack.size > (unsigned int )(re_max_failures * 19)));
# 4831 "regex.c"
        if (fail_stack.size > (unsigned int )(re_max_failures * 19)) {
          __CrestBranch(41942, 11653, 1);
          __CrestLoad(41944, (unsigned long )0, (long long )0);
          __CrestStore(41945, (unsigned long )(& tmp___83));
# 4831 "regex.c"
          tmp___83 = 0;
        } else {
          __CrestBranch(41943, 11654, 0);
          __CrestLoad(41950, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(41949, (unsigned long )0, (long long )1);
          __CrestApply2(41948, 8, (long long )(fail_stack.size << 1));
          __CrestLoad(41947, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(41946, 2, (long long )((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t )));
# 4831 "regex.c"
          tmp___81 = __builtin_alloca((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t ));
          __CrestClearStack(41951);
# 4831 "regex.c"
          destination___2 = (char *)tmp___81;
          __CrestLoad(41954, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(41953, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(41952, 2, (long long )((unsigned long )fail_stack.size * sizeof(fail_stack_elt_t )));
# 4831 "regex.c"
          memcpy((void * __restrict )destination___2, (void const * __restrict )fail_stack.stack,
                 (unsigned long )fail_stack.size * sizeof(fail_stack_elt_t ));
          __CrestClearStack(41955);
# 4831 "regex.c"
          fail_stack.stack = (fail_stack_elt_t *)destination___2;
          {
          __CrestLoad(41958, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
          __CrestLoad(41957, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(41956, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 4831 "regex.c"
          if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
            __CrestBranch(41959, 11656, 1);
            __CrestLoad(41961, (unsigned long )0, (long long )0);
            __CrestStore(41962, (unsigned long )(& tmp___82));
# 4831 "regex.c"
            tmp___82 = 0;
          } else {
            __CrestBranch(41960, 11657, 0);
            __CrestLoad(41965, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
            __CrestLoad(41964, (unsigned long )0, (long long )1);
            __CrestApply2(41963, 8, (long long )(fail_stack.size << 1));
            __CrestStore(41966, (unsigned long )(& fail_stack.size));
# 4831 "regex.c"
            fail_stack.size <<= 1;
            __CrestLoad(41967, (unsigned long )0, (long long )1);
            __CrestStore(41968, (unsigned long )(& tmp___82));
# 4831 "regex.c"
            tmp___82 = 1;
          }
          }
          __CrestLoad(41969, (unsigned long )(& tmp___82), (long long )tmp___82);
          __CrestStore(41970, (unsigned long )(& tmp___83));
# 4831 "regex.c"
          tmp___83 = tmp___82;
        }
        }
        {
        __CrestLoad(41973, (unsigned long )(& tmp___83), (long long )tmp___83);
        __CrestLoad(41972, (unsigned long )0, (long long )0);
        __CrestApply2(41971, 13, (long long )(tmp___83 != 0));
# 4831 "regex.c"
        if (tmp___83 != 0) {
          __CrestBranch(41974, 11660, 1);

        } else {
          __CrestBranch(41975, 11661, 0);
          __CrestLoad(41976, (unsigned long )0, (long long )-2);
          __CrestStore(41977, (unsigned long )(& __retres457));
# 4831 "regex.c"
          __retres457 = -2;
# 4831 "regex.c"
          goto return_label;
        }
        }
      }
      while_break___57: ;
      }
      __CrestLoad(41978, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(41979, (unsigned long )(& this_reg___3));
# 4831 "regex.c"
      this_reg___3 = (s_reg_t )lowest_active_reg;
      {
# 4831 "regex.c"
      while (1) {
        while_continue___58: ;
        {
        __CrestLoad(41982, (unsigned long )(& this_reg___3), (long long )this_reg___3);
        __CrestLoad(41981, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestApply2(41980, 15, (long long )((active_reg_t )this_reg___3 <= highest_active_reg));
# 4831 "regex.c"
        if ((active_reg_t )this_reg___3 <= highest_active_reg) {
          __CrestBranch(41983, 11669, 1);

        } else {
          __CrestBranch(41984, 11670, 0);
# 4831 "regex.c"
          goto while_break___58;
        }
        }
        __CrestLoad(41985, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41986, (unsigned long )(& tmp___84));
# 4831 "regex.c"
        tmp___84 = fail_stack.avail;
        __CrestLoad(41989, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41988, (unsigned long )0, (long long )1U);
        __CrestApply2(41987, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41990, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
        (fail_stack.avail) ++;
# 4831 "regex.c"
        mem_394 = fail_stack.stack + tmp___84;
# 4831 "regex.c"
        mem_395 = regstart + this_reg___3;
# 4831 "regex.c"
        mem_394->pointer = (unsigned char *)*mem_395;
        __CrestLoad(41991, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41992, (unsigned long )(& tmp___85));
# 4831 "regex.c"
        tmp___85 = fail_stack.avail;
        __CrestLoad(41995, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(41994, (unsigned long )0, (long long )1U);
        __CrestApply2(41993, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(41996, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
        (fail_stack.avail) ++;
# 4831 "regex.c"
        mem_396 = fail_stack.stack + tmp___85;
# 4831 "regex.c"
        mem_397 = regend + this_reg___3;
# 4831 "regex.c"
        mem_396->pointer = (unsigned char *)*mem_397;
        __CrestLoad(41997, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(41998, (unsigned long )(& tmp___86));
# 4831 "regex.c"
        tmp___86 = fail_stack.avail;
        __CrestLoad(42001, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42000, (unsigned long )0, (long long )1U);
        __CrestApply2(41999, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(42002, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
        (fail_stack.avail) ++;
# 4831 "regex.c"
        mem_398 = fail_stack.stack + tmp___86;
# 4831 "regex.c"
        mem_399 = reg_info + this_reg___3;
# 4831 "regex.c"
        *mem_398 = mem_399->word;
        __CrestLoad(42005, (unsigned long )(& this_reg___3), (long long )this_reg___3);
        __CrestLoad(42004, (unsigned long )0, (long long )1L);
        __CrestApply2(42003, 0, (long long )(this_reg___3 + 1L));
        __CrestStore(42006, (unsigned long )(& this_reg___3));
# 4831 "regex.c"
        this_reg___3 ++;
      }
      while_break___58: ;
      }
      __CrestLoad(42007, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42008, (unsigned long )(& tmp___87));
# 4831 "regex.c"
      tmp___87 = fail_stack.avail;
      __CrestLoad(42011, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42010, (unsigned long )0, (long long )1U);
      __CrestApply2(42009, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42012, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
      (fail_stack.avail) ++;
# 4831 "regex.c"
      mem_400 = fail_stack.stack + tmp___87;
      __CrestLoad(42013, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(42014, (unsigned long )(& mem_400->integer));
# 4831 "regex.c"
      mem_400->integer = (int )lowest_active_reg;
      __CrestLoad(42015, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42016, (unsigned long )(& tmp___88));
# 4831 "regex.c"
      tmp___88 = fail_stack.avail;
      __CrestLoad(42019, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42018, (unsigned long )0, (long long )1U);
      __CrestApply2(42017, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42020, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
      (fail_stack.avail) ++;
# 4831 "regex.c"
      mem_401 = fail_stack.stack + tmp___88;
      __CrestLoad(42021, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
      __CrestStore(42022, (unsigned long )(& mem_401->integer));
# 4831 "regex.c"
      mem_401->integer = (int )highest_active_reg;
      __CrestLoad(42023, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42024, (unsigned long )(& tmp___89));
# 4831 "regex.c"
      tmp___89 = fail_stack.avail;
      __CrestLoad(42027, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42026, (unsigned long )0, (long long )1U);
      __CrestApply2(42025, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42028, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
      (fail_stack.avail) ++;
# 4831 "regex.c"
      mem_402 = fail_stack.stack + tmp___89;
# 4831 "regex.c"
      mem_402->pointer = (unsigned char *)0;
      __CrestLoad(42029, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42030, (unsigned long )(& tmp___90));
# 4831 "regex.c"
      tmp___90 = fail_stack.avail;
      __CrestLoad(42033, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42032, (unsigned long )0, (long long )1U);
      __CrestApply2(42031, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42034, (unsigned long )(& fail_stack.avail));
# 4831 "regex.c"
      (fail_stack.avail) ++;
# 4831 "regex.c"
      mem_403 = fail_stack.stack + tmp___90;
# 4831 "regex.c"
      mem_403->pointer = (unsigned char *)0;
# 4831 "regex.c"
      goto while_break___56;
    }
    while_break___56: ;
    }
# 4832 "regex.c"
    goto unconditional_jump;
    case_20: ;
    {
# 4844 "regex.c"
    while (1) {
      while_continue___59: ;
      {
# 4844 "regex.c"
      while (1) {
        while_continue___60: ;
        {
        __CrestLoad(42047, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(42046, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestApply2(42045, 1, (long long )(fail_stack.size - fail_stack.avail));
        __CrestLoad(42044, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestLoad(42043, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestApply2(42042, 1, (long long )(highest_active_reg - lowest_active_reg));
        __CrestLoad(42041, (unsigned long )0, (long long )1UL);
        __CrestApply2(42040, 0, (long long )((highest_active_reg - lowest_active_reg) + 1UL));
        __CrestLoad(42039, (unsigned long )0, (long long )3UL);
        __CrestApply2(42038, 2, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL));
        __CrestLoad(42037, (unsigned long )0, (long long )4UL);
        __CrestApply2(42036, 0, (long long )(((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
        __CrestApply2(42035, 16, (long long )((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL));
# 4844 "regex.c"
        if ((active_reg_t )(fail_stack.size - fail_stack.avail) < ((highest_active_reg - lowest_active_reg) + 1UL) * 3UL + 4UL) {
          __CrestBranch(42048, 11685, 1);

        } else {
          __CrestBranch(42049, 11686, 0);
# 4844 "regex.c"
          goto while_break___60;
        }
        }
        {
        __CrestLoad(42054, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
        __CrestLoad(42053, (unsigned long )(& re_max_failures), (long long )re_max_failures);
        __CrestLoad(42052, (unsigned long )0, (long long )19);
        __CrestApply2(42051, 2, (long long )(re_max_failures * 19));
        __CrestApply2(42050, 14, (long long )(fail_stack.size > (unsigned int )(re_max_failures * 19)));
# 4844 "regex.c"
        if (fail_stack.size > (unsigned int )(re_max_failures * 19)) {
          __CrestBranch(42055, 11688, 1);
          __CrestLoad(42057, (unsigned long )0, (long long )0);
          __CrestStore(42058, (unsigned long )(& tmp___93));
# 4844 "regex.c"
          tmp___93 = 0;
        } else {
          __CrestBranch(42056, 11689, 0);
          __CrestLoad(42063, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(42062, (unsigned long )0, (long long )1);
          __CrestApply2(42061, 8, (long long )(fail_stack.size << 1));
          __CrestLoad(42060, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(42059, 2, (long long )((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t )));
# 4844 "regex.c"
          tmp___91 = __builtin_alloca((unsigned long )(fail_stack.size << 1) * sizeof(fail_stack_elt_t ));
          __CrestClearStack(42064);
# 4844 "regex.c"
          destination___3 = (char *)tmp___91;
          __CrestLoad(42067, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
          __CrestLoad(42066, (unsigned long )0, (long long )sizeof(fail_stack_elt_t ));
          __CrestApply2(42065, 2, (long long )((unsigned long )fail_stack.size * sizeof(fail_stack_elt_t )));
# 4844 "regex.c"
          memcpy((void * __restrict )destination___3, (void const * __restrict )fail_stack.stack,
                 (unsigned long )fail_stack.size * sizeof(fail_stack_elt_t ));
          __CrestClearStack(42068);
# 4844 "regex.c"
          fail_stack.stack = (fail_stack_elt_t *)destination___3;
          {
          __CrestLoad(42071, (unsigned long )(& fail_stack.stack), (long long )((unsigned long )fail_stack.stack));
          __CrestLoad(42070, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(42069, 12, (long long )((unsigned long )fail_stack.stack == (unsigned long )((void *)0)));
# 4844 "regex.c"
          if ((unsigned long )fail_stack.stack == (unsigned long )((void *)0)) {
            __CrestBranch(42072, 11691, 1);
            __CrestLoad(42074, (unsigned long )0, (long long )0);
            __CrestStore(42075, (unsigned long )(& tmp___92));
# 4844 "regex.c"
            tmp___92 = 0;
          } else {
            __CrestBranch(42073, 11692, 0);
            __CrestLoad(42078, (unsigned long )(& fail_stack.size), (long long )fail_stack.size);
            __CrestLoad(42077, (unsigned long )0, (long long )1);
            __CrestApply2(42076, 8, (long long )(fail_stack.size << 1));
            __CrestStore(42079, (unsigned long )(& fail_stack.size));
# 4844 "regex.c"
            fail_stack.size <<= 1;
            __CrestLoad(42080, (unsigned long )0, (long long )1);
            __CrestStore(42081, (unsigned long )(& tmp___92));
# 4844 "regex.c"
            tmp___92 = 1;
          }
          }
          __CrestLoad(42082, (unsigned long )(& tmp___92), (long long )tmp___92);
          __CrestStore(42083, (unsigned long )(& tmp___93));
# 4844 "regex.c"
          tmp___93 = tmp___92;
        }
        }
        {
        __CrestLoad(42086, (unsigned long )(& tmp___93), (long long )tmp___93);
        __CrestLoad(42085, (unsigned long )0, (long long )0);
        __CrestApply2(42084, 13, (long long )(tmp___93 != 0));
# 4844 "regex.c"
        if (tmp___93 != 0) {
          __CrestBranch(42087, 11695, 1);

        } else {
          __CrestBranch(42088, 11696, 0);
          __CrestLoad(42089, (unsigned long )0, (long long )-2);
          __CrestStore(42090, (unsigned long )(& __retres457));
# 4844 "regex.c"
          __retres457 = -2;
# 4844 "regex.c"
          goto return_label;
        }
        }
      }
      while_break___60: ;
      }
      __CrestLoad(42091, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(42092, (unsigned long )(& this_reg___4));
# 4844 "regex.c"
      this_reg___4 = (s_reg_t )lowest_active_reg;
      {
# 4844 "regex.c"
      while (1) {
        while_continue___61: ;
        {
        __CrestLoad(42095, (unsigned long )(& this_reg___4), (long long )this_reg___4);
        __CrestLoad(42094, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
        __CrestApply2(42093, 15, (long long )((active_reg_t )this_reg___4 <= highest_active_reg));
# 4844 "regex.c"
        if ((active_reg_t )this_reg___4 <= highest_active_reg) {
          __CrestBranch(42096, 11704, 1);

        } else {
          __CrestBranch(42097, 11705, 0);
# 4844 "regex.c"
          goto while_break___61;
        }
        }
        __CrestLoad(42098, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(42099, (unsigned long )(& tmp___94));
# 4844 "regex.c"
        tmp___94 = fail_stack.avail;
        __CrestLoad(42102, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42101, (unsigned long )0, (long long )1U);
        __CrestApply2(42100, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(42103, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
        (fail_stack.avail) ++;
# 4844 "regex.c"
        mem_404 = fail_stack.stack + tmp___94;
# 4844 "regex.c"
        mem_405 = regstart + this_reg___4;
# 4844 "regex.c"
        mem_404->pointer = (unsigned char *)*mem_405;
        __CrestLoad(42104, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(42105, (unsigned long )(& tmp___95));
# 4844 "regex.c"
        tmp___95 = fail_stack.avail;
        __CrestLoad(42108, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42107, (unsigned long )0, (long long )1U);
        __CrestApply2(42106, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(42109, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
        (fail_stack.avail) ++;
# 4844 "regex.c"
        mem_406 = fail_stack.stack + tmp___95;
# 4844 "regex.c"
        mem_407 = regend + this_reg___4;
# 4844 "regex.c"
        mem_406->pointer = (unsigned char *)*mem_407;
        __CrestLoad(42110, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestStore(42111, (unsigned long )(& tmp___96));
# 4844 "regex.c"
        tmp___96 = fail_stack.avail;
        __CrestLoad(42114, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42113, (unsigned long )0, (long long )1U);
        __CrestApply2(42112, 0, (long long )(fail_stack.avail + 1U));
        __CrestStore(42115, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
        (fail_stack.avail) ++;
# 4844 "regex.c"
        mem_408 = fail_stack.stack + tmp___96;
# 4844 "regex.c"
        mem_409 = reg_info + this_reg___4;
# 4844 "regex.c"
        *mem_408 = mem_409->word;
        __CrestLoad(42118, (unsigned long )(& this_reg___4), (long long )this_reg___4);
        __CrestLoad(42117, (unsigned long )0, (long long )1L);
        __CrestApply2(42116, 0, (long long )(this_reg___4 + 1L));
        __CrestStore(42119, (unsigned long )(& this_reg___4));
# 4844 "regex.c"
        this_reg___4 ++;
      }
      while_break___61: ;
      }
      __CrestLoad(42120, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42121, (unsigned long )(& tmp___97));
# 4844 "regex.c"
      tmp___97 = fail_stack.avail;
      __CrestLoad(42124, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42123, (unsigned long )0, (long long )1U);
      __CrestApply2(42122, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42125, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
      (fail_stack.avail) ++;
# 4844 "regex.c"
      mem_410 = fail_stack.stack + tmp___97;
      __CrestLoad(42126, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
      __CrestStore(42127, (unsigned long )(& mem_410->integer));
# 4844 "regex.c"
      mem_410->integer = (int )lowest_active_reg;
      __CrestLoad(42128, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42129, (unsigned long )(& tmp___98));
# 4844 "regex.c"
      tmp___98 = fail_stack.avail;
      __CrestLoad(42132, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42131, (unsigned long )0, (long long )1U);
      __CrestApply2(42130, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42133, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
      (fail_stack.avail) ++;
# 4844 "regex.c"
      mem_411 = fail_stack.stack + tmp___98;
      __CrestLoad(42134, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
      __CrestStore(42135, (unsigned long )(& mem_411->integer));
# 4844 "regex.c"
      mem_411->integer = (int )highest_active_reg;
      __CrestLoad(42136, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42137, (unsigned long )(& tmp___99));
# 4844 "regex.c"
      tmp___99 = fail_stack.avail;
      __CrestLoad(42140, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42139, (unsigned long )0, (long long )1U);
      __CrestApply2(42138, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42141, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
      (fail_stack.avail) ++;
# 4844 "regex.c"
      mem_412 = fail_stack.stack + tmp___99;
# 4844 "regex.c"
      mem_412->pointer = (unsigned char *)0;
      __CrestLoad(42142, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestStore(42143, (unsigned long )(& tmp___100));
# 4844 "regex.c"
      tmp___100 = fail_stack.avail;
      __CrestLoad(42146, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42145, (unsigned long )0, (long long )1U);
      __CrestApply2(42144, 0, (long long )(fail_stack.avail + 1U));
      __CrestStore(42147, (unsigned long )(& fail_stack.avail));
# 4844 "regex.c"
      (fail_stack.avail) ++;
# 4844 "regex.c"
      mem_413 = fail_stack.stack + tmp___100;
# 4844 "regex.c"
      mem_413->pointer = (unsigned char *)0;
# 4844 "regex.c"
      goto while_break___59;
    }
    while_break___59: ;
    }
# 4845 "regex.c"
    goto switch_break;
    case_21:
    {
# 4850 "regex.c"
    while (1) {
      while_continue___62: ;
# 4850 "regex.c"
      mem_414 = p + 2;
      __CrestLoad(42150, (unsigned long )mem_414, (long long )*mem_414);
      __CrestLoad(42149, (unsigned long )0, (long long )255);
      __CrestApply2(42148, 5, (long long )((int )*mem_414 & 255));
      __CrestStore(42151, (unsigned long )(& mcnt));
# 4850 "regex.c"
      mcnt = (int )*mem_414 & 255;
# 4850 "regex.c"
      mem_415 = (p + 2) + 1;
      __CrestLoad(42156, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42155, (unsigned long )mem_415, (long long )*mem_415);
      __CrestLoad(42154, (unsigned long )0, (long long )8);
      __CrestApply2(42153, 8, (long long )((int )((signed char )*mem_415) << 8));
      __CrestApply2(42152, 0, (long long )(mcnt + ((int )((signed char )*mem_415) << 8)));
      __CrestStore(42157, (unsigned long )(& mcnt));
# 4850 "regex.c"
      mcnt += (int )((signed char )*mem_415) << 8;
# 4850 "regex.c"
      goto while_break___62;
    }
    while_break___62: ;
    }
    {
    __CrestLoad(42160, (unsigned long )(& mcnt), (long long )mcnt);
    __CrestLoad(42159, (unsigned long )0, (long long )0);
    __CrestApply2(42158, 14, (long long )(mcnt > 0));
# 4855 "regex.c"
    if (mcnt > 0) {
      __CrestBranch(42161, 11719, 1);
      __CrestLoad(42165, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42164, (unsigned long )0, (long long )1);
      __CrestApply2(42163, 1, (long long )(mcnt - 1));
      __CrestStore(42166, (unsigned long )(& mcnt));
# 4857 "regex.c"
      mcnt --;
# 4858 "regex.c"
      p += 2;
      {
# 4859 "regex.c"
      while (1) {
        while_continue___63: ;
        {
# 4859 "regex.c"
        while (1) {
          while_continue___64: ;
# 4859 "regex.c"
          mem_416 = p + 0;
          __CrestLoad(42169, (unsigned long )(& mcnt), (long long )mcnt);
          __CrestLoad(42168, (unsigned long )0, (long long )255);
          __CrestApply2(42167, 5, (long long )(mcnt & 255));
          __CrestStore(42170, (unsigned long )mem_416);
# 4859 "regex.c"
          *mem_416 = (unsigned char )(mcnt & 255);
# 4859 "regex.c"
          mem_417 = p + 1;
          __CrestLoad(42173, (unsigned long )(& mcnt), (long long )mcnt);
          __CrestLoad(42172, (unsigned long )0, (long long )8);
          __CrestApply2(42171, 9, (long long )(mcnt >> 8));
          __CrestStore(42174, (unsigned long )mem_417);
# 4859 "regex.c"
          *mem_417 = (unsigned char )(mcnt >> 8);
# 4859 "regex.c"
          goto while_break___64;
        }
        while_break___64: ;
        }
# 4859 "regex.c"
        p += 2;
# 4859 "regex.c"
        goto while_break___63;
      }
      while_break___63: ;
      }
    } else {
      __CrestBranch(42162, 11732, 0);
      {
      __CrestLoad(42177, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42176, (unsigned long )0, (long long )0);
      __CrestApply2(42175, 12, (long long )(mcnt == 0));
# 4866 "regex.c"
      if (mcnt == 0) {
        __CrestBranch(42178, 11733, 1);
# 4873 "regex.c"
        mem_418 = p + 2;
        __CrestLoad(42180, (unsigned long )0, (long long )(unsigned char)0);
        __CrestStore(42181, (unsigned long )mem_418);
# 4873 "regex.c"
        *mem_418 = (unsigned char)0;
# 4874 "regex.c"
        mem_419 = p + 3;
        __CrestLoad(42182, (unsigned long )0, (long long )(unsigned char)0);
        __CrestStore(42183, (unsigned long )mem_419);
# 4874 "regex.c"
        *mem_419 = (unsigned char)0;
# 4875 "regex.c"
        goto on_failure;
      } else {
        __CrestBranch(42179, 11735, 0);

      }
      }
    }
    }
# 4877 "regex.c"
    goto switch_break;
    case_22___0:
    {
# 4880 "regex.c"
    while (1) {
      while_continue___65: ;
# 4880 "regex.c"
      mem_420 = p + 2;
      __CrestLoad(42186, (unsigned long )mem_420, (long long )*mem_420);
      __CrestLoad(42185, (unsigned long )0, (long long )255);
      __CrestApply2(42184, 5, (long long )((int )*mem_420 & 255));
      __CrestStore(42187, (unsigned long )(& mcnt));
# 4880 "regex.c"
      mcnt = (int )*mem_420 & 255;
# 4880 "regex.c"
      mem_421 = (p + 2) + 1;
      __CrestLoad(42192, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42191, (unsigned long )mem_421, (long long )*mem_421);
      __CrestLoad(42190, (unsigned long )0, (long long )8);
      __CrestApply2(42189, 8, (long long )((int )((signed char )*mem_421) << 8));
      __CrestApply2(42188, 0, (long long )(mcnt + ((int )((signed char )*mem_421) << 8)));
      __CrestStore(42193, (unsigned long )(& mcnt));
# 4880 "regex.c"
      mcnt += (int )((signed char )*mem_421) << 8;
# 4880 "regex.c"
      goto while_break___65;
    }
    while_break___65: ;
    }
    {
    __CrestLoad(42196, (unsigned long )(& mcnt), (long long )mcnt);
    __CrestLoad(42195, (unsigned long )0, (long long )0);
    __CrestApply2(42194, 13, (long long )(mcnt != 0));
# 4884 "regex.c"
    if (mcnt != 0) {
      __CrestBranch(42197, 11744, 1);
      __CrestLoad(42201, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42200, (unsigned long )0, (long long )1);
      __CrestApply2(42199, 1, (long long )(mcnt - 1));
      __CrestStore(42202, (unsigned long )(& mcnt));
# 4886 "regex.c"
      mcnt --;
      {
# 4887 "regex.c"
      while (1) {
        while_continue___66: ;
# 4887 "regex.c"
        mem_422 = (p + 2) + 0;
        __CrestLoad(42205, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42204, (unsigned long )0, (long long )255);
        __CrestApply2(42203, 5, (long long )(mcnt & 255));
        __CrestStore(42206, (unsigned long )mem_422);
# 4887 "regex.c"
        *mem_422 = (unsigned char )(mcnt & 255);
# 4887 "regex.c"
        mem_423 = (p + 2) + 1;
        __CrestLoad(42209, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42208, (unsigned long )0, (long long )8);
        __CrestApply2(42207, 9, (long long )(mcnt >> 8));
        __CrestStore(42210, (unsigned long )mem_423);
# 4887 "regex.c"
        *mem_423 = (unsigned char )(mcnt >> 8);
# 4887 "regex.c"
        goto while_break___66;
      }
      while_break___66: ;
      }
# 4893 "regex.c"
      goto unconditional_jump;
    } else {
      __CrestBranch(42198, 11752, 0);
# 4897 "regex.c"
      p += 4;
    }
    }
# 4898 "regex.c"
    goto switch_break;
    case_23:
    {
# 4904 "regex.c"
    while (1) {
      while_continue___67: ;
      {
# 4904 "regex.c"
      while (1) {
        while_continue___68: ;
        __CrestLoad(42213, (unsigned long )p, (long long )*p);
        __CrestLoad(42212, (unsigned long )0, (long long )255);
        __CrestApply2(42211, 5, (long long )((int )*p & 255));
        __CrestStore(42214, (unsigned long )(& mcnt));
# 4904 "regex.c"
        mcnt = (int )*p & 255;
# 4904 "regex.c"
        mem_424 = p + 1;
        __CrestLoad(42219, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42218, (unsigned long )mem_424, (long long )*mem_424);
        __CrestLoad(42217, (unsigned long )0, (long long )8);
        __CrestApply2(42216, 8, (long long )((int )((signed char )*mem_424) << 8));
        __CrestApply2(42215, 0, (long long )(mcnt + ((int )((signed char )*mem_424) << 8)));
        __CrestStore(42220, (unsigned long )(& mcnt));
# 4904 "regex.c"
        mcnt += (int )((signed char )*mem_424) << 8;
# 4904 "regex.c"
        goto while_break___68;
      }
      while_break___68: ;
      }
# 4904 "regex.c"
      p += 2;
# 4904 "regex.c"
      goto while_break___67;
    }
    while_break___67: ;
    }
# 4905 "regex.c"
    p1 = p + mcnt;
    {
# 4906 "regex.c"
    while (1) {
      while_continue___69: ;
      {
# 4906 "regex.c"
      while (1) {
        while_continue___70: ;
        __CrestLoad(42223, (unsigned long )p, (long long )*p);
        __CrestLoad(42222, (unsigned long )0, (long long )255);
        __CrestApply2(42221, 5, (long long )((int )*p & 255));
        __CrestStore(42224, (unsigned long )(& mcnt));
# 4906 "regex.c"
        mcnt = (int )*p & 255;
# 4906 "regex.c"
        mem_425 = p + 1;
        __CrestLoad(42229, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42228, (unsigned long )mem_425, (long long )*mem_425);
        __CrestLoad(42227, (unsigned long )0, (long long )8);
        __CrestApply2(42226, 8, (long long )((int )((signed char )*mem_425) << 8));
        __CrestApply2(42225, 0, (long long )(mcnt + ((int )((signed char )*mem_425) << 8)));
        __CrestStore(42230, (unsigned long )(& mcnt));
# 4906 "regex.c"
        mcnt += (int )((signed char )*mem_425) << 8;
# 4906 "regex.c"
        goto while_break___70;
      }
      while_break___70: ;
      }
# 4906 "regex.c"
      p += 2;
# 4906 "regex.c"
      goto while_break___69;
    }
    while_break___69: ;
    }
    {
# 4912 "regex.c"
    while (1) {
      while_continue___71: ;
# 4912 "regex.c"
      mem_426 = p1 + 0;
      __CrestLoad(42233, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42232, (unsigned long )0, (long long )255);
      __CrestApply2(42231, 5, (long long )(mcnt & 255));
      __CrestStore(42234, (unsigned long )mem_426);
# 4912 "regex.c"
      *mem_426 = (unsigned char )(mcnt & 255);
# 4912 "regex.c"
      mem_427 = p1 + 1;
      __CrestLoad(42237, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(42236, (unsigned long )0, (long long )8);
      __CrestApply2(42235, 9, (long long )(mcnt >> 8));
      __CrestStore(42238, (unsigned long )mem_427);
# 4912 "regex.c"
      *mem_427 = (unsigned char )(mcnt >> 8);
# 4912 "regex.c"
      goto while_break___71;
    }
    while_break___71: ;
    }
# 4913 "regex.c"
    goto switch_break;
    case_28:
    {
    __CrestLoad(42241, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(42240, (unsigned long )0, (long long )0);
    __CrestApply2(42239, 13, (long long )(size1 != 0));
# 4939 "regex.c"
    if (size1 != 0) {
      __CrestBranch(42242, 11787, 1);
# 4939 "regex.c"
      tmp___101 = string1;
    } else {
      __CrestBranch(42243, 11788, 0);
# 4939 "regex.c"
      tmp___101 = string2;
    }
    }
    {
    __CrestLoad(42246, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42245, (unsigned long )(& tmp___101), (long long )((unsigned long )tmp___101));
    __CrestApply2(42244, 12, (long long )((unsigned long )d == (unsigned long )tmp___101));
# 4939 "regex.c"
    if ((unsigned long )d == (unsigned long )tmp___101) {
      __CrestBranch(42247, 11790, 1);
# 4940 "regex.c"
      goto switch_break;
    } else {
      __CrestBranch(42248, 11791, 0);
      {
      __CrestLoad(42251, (unsigned long )(& size2), (long long )size2);
      __CrestLoad(42250, (unsigned long )0, (long long )0);
      __CrestApply2(42249, 12, (long long )(size2 == 0));
# 4939 "regex.c"
      if (size2 == 0) {
        __CrestBranch(42252, 11792, 1);
# 4940 "regex.c"
        goto switch_break;
      } else {
        __CrestBranch(42253, 11793, 0);
        {
        __CrestLoad(42256, (unsigned long )(& d), (long long )((unsigned long )d));
        __CrestLoad(42255, (unsigned long )(& end2), (long long )((unsigned long )end2));
        __CrestApply2(42254, 12, (long long )((unsigned long )d == (unsigned long )end2));
# 4939 "regex.c"
        if ((unsigned long )d == (unsigned long )end2) {
          __CrestBranch(42257, 11794, 1);
# 4940 "regex.c"
          goto switch_break;
        } else {
          __CrestBranch(42258, 11795, 0);

        }
        }
      }
      }
    }
    }
    {
    __CrestLoad(42263, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42262, (unsigned long )0, (long long )1);
    __CrestApply2(42261, 18, (long long )((unsigned long )(d - 1)));
    __CrestLoad(42260, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42259, 12, (long long )((unsigned long )(d - 1) == (unsigned long )end1));
# 4942 "regex.c"
    if ((unsigned long )(d - 1) == (unsigned long )end1) {
      __CrestBranch(42264, 11797, 1);
      __CrestLoad(42266, (unsigned long )string2, (long long )*string2);
      __CrestStore(42267, (unsigned long )(& tmp___103));
# 4942 "regex.c"
      tmp___103 = (int const )*string2;
    } else {
      __CrestBranch(42265, 11798, 0);
      {
      __CrestLoad(42274, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42273, (unsigned long )0, (long long )1);
      __CrestApply2(42272, 18, (long long )((unsigned long )(d - 1)));
      __CrestLoad(42271, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42270, (unsigned long )0, (long long )1);
      __CrestApply2(42269, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42268, 12, (long long )((unsigned long )(d - 1) == (unsigned long )(string2 - 1)));
# 4942 "regex.c"
      if ((unsigned long )(d - 1) == (unsigned long )(string2 - 1)) {
        __CrestBranch(42275, 11799, 1);
# 4942 "regex.c"
        mem_428 = end1 - 1;
        __CrestLoad(42277, (unsigned long )mem_428, (long long )*mem_428);
        __CrestStore(42278, (unsigned long )(& tmp___102));
# 4942 "regex.c"
        tmp___102 = (int const )*mem_428;
      } else {
        __CrestBranch(42276, 11800, 0);
# 4942 "regex.c"
        mem_429 = d - 1;
        __CrestLoad(42279, (unsigned long )mem_429, (long long )*mem_429);
        __CrestStore(42280, (unsigned long )(& tmp___102));
# 4942 "regex.c"
        tmp___102 = (int const )*mem_429;
      }
      }
      __CrestLoad(42281, (unsigned long )(& tmp___102), (long long )tmp___102);
      __CrestStore(42282, (unsigned long )(& tmp___103));
# 4942 "regex.c"
      tmp___103 = tmp___102;
    }
    }
    __CrestLoad(42285, (unsigned long )(& re_syntax_table[tmp___103]), (long long )re_syntax_table[tmp___103]);
    __CrestLoad(42284, (unsigned long )0, (long long )1);
    __CrestApply2(42283, 12, (long long )((int )re_syntax_table[tmp___103] == 1));
    __CrestStore(42286, (unsigned long )(& prevchar));
# 4942 "regex.c"
    prevchar = (boolean )((int )re_syntax_table[tmp___103] == 1);
    {
    __CrestLoad(42289, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42288, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42287, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 4943 "regex.c"
    if ((unsigned long )d == (unsigned long )end1) {
      __CrestBranch(42290, 11804, 1);
      __CrestLoad(42292, (unsigned long )string2, (long long )*string2);
      __CrestStore(42293, (unsigned long )(& tmp___105));
# 4943 "regex.c"
      tmp___105 = (int const )*string2;
    } else {
      __CrestBranch(42291, 11805, 0);
      {
      __CrestLoad(42298, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42297, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42296, (unsigned long )0, (long long )1);
      __CrestApply2(42295, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42294, 12, (long long )((unsigned long )d == (unsigned long )(string2 - 1)));
# 4943 "regex.c"
      if ((unsigned long )d == (unsigned long )(string2 - 1)) {
        __CrestBranch(42299, 11806, 1);
# 4943 "regex.c"
        mem_430 = end1 - 1;
        __CrestLoad(42301, (unsigned long )mem_430, (long long )*mem_430);
        __CrestStore(42302, (unsigned long )(& tmp___104));
# 4943 "regex.c"
        tmp___104 = (int const )*mem_430;
      } else {
        __CrestBranch(42300, 11807, 0);
        __CrestLoad(42303, (unsigned long )d, (long long )*d);
        __CrestStore(42304, (unsigned long )(& tmp___104));
# 4943 "regex.c"
        tmp___104 = (int const )*d;
      }
      }
      __CrestLoad(42305, (unsigned long )(& tmp___104), (long long )tmp___104);
      __CrestStore(42306, (unsigned long )(& tmp___105));
# 4943 "regex.c"
      tmp___105 = tmp___104;
    }
    }
    __CrestLoad(42309, (unsigned long )(& re_syntax_table[tmp___105]), (long long )re_syntax_table[tmp___105]);
    __CrestLoad(42308, (unsigned long )0, (long long )1);
    __CrestApply2(42307, 12, (long long )((int )re_syntax_table[tmp___105] == 1));
    __CrestStore(42310, (unsigned long )(& thischar));
# 4943 "regex.c"
    thischar = (boolean )((int )re_syntax_table[tmp___105] == 1);
    {
    __CrestLoad(42313, (unsigned long )(& prevchar), (long long )prevchar);
    __CrestLoad(42312, (unsigned long )(& thischar), (long long )thischar);
    __CrestApply2(42311, 13, (long long )((int )prevchar != (int )thischar));
# 4944 "regex.c"
    if ((int )prevchar != (int )thischar) {
      __CrestBranch(42314, 11811, 1);
# 4945 "regex.c"
      goto switch_break;
    } else {
      __CrestBranch(42315, 11812, 0);

    }
    }
# 4946 "regex.c"
    goto fail;
    case_29:
    {
    __CrestLoad(42318, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(42317, (unsigned long )0, (long long )0);
    __CrestApply2(42316, 13, (long long )(size1 != 0));
# 4954 "regex.c"
    if (size1 != 0) {
      __CrestBranch(42319, 11815, 1);
# 4954 "regex.c"
      tmp___106 = string1;
    } else {
      __CrestBranch(42320, 11816, 0);
# 4954 "regex.c"
      tmp___106 = string2;
    }
    }
    {
    __CrestLoad(42323, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42322, (unsigned long )(& tmp___106), (long long )((unsigned long )tmp___106));
    __CrestApply2(42321, 12, (long long )((unsigned long )d == (unsigned long )tmp___106));
# 4954 "regex.c"
    if ((unsigned long )d == (unsigned long )tmp___106) {
      __CrestBranch(42324, 11818, 1);
# 4955 "regex.c"
      goto fail;
    } else {
      __CrestBranch(42325, 11819, 0);
      {
      __CrestLoad(42328, (unsigned long )(& size2), (long long )size2);
      __CrestLoad(42327, (unsigned long )0, (long long )0);
      __CrestApply2(42326, 12, (long long )(size2 == 0));
# 4954 "regex.c"
      if (size2 == 0) {
        __CrestBranch(42329, 11820, 1);
# 4955 "regex.c"
        goto fail;
      } else {
        __CrestBranch(42330, 11821, 0);
        {
        __CrestLoad(42333, (unsigned long )(& d), (long long )((unsigned long )d));
        __CrestLoad(42332, (unsigned long )(& end2), (long long )((unsigned long )end2));
        __CrestApply2(42331, 12, (long long )((unsigned long )d == (unsigned long )end2));
# 4954 "regex.c"
        if ((unsigned long )d == (unsigned long )end2) {
          __CrestBranch(42334, 11822, 1);
# 4955 "regex.c"
          goto fail;
        } else {
          __CrestBranch(42335, 11823, 0);

        }
        }
      }
      }
    }
    }
    {
    __CrestLoad(42340, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42339, (unsigned long )0, (long long )1);
    __CrestApply2(42338, 18, (long long )((unsigned long )(d - 1)));
    __CrestLoad(42337, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42336, 12, (long long )((unsigned long )(d - 1) == (unsigned long )end1));
# 4957 "regex.c"
    if ((unsigned long )(d - 1) == (unsigned long )end1) {
      __CrestBranch(42341, 11825, 1);
      __CrestLoad(42343, (unsigned long )string2, (long long )*string2);
      __CrestStore(42344, (unsigned long )(& tmp___108));
# 4957 "regex.c"
      tmp___108 = (int const )*string2;
    } else {
      __CrestBranch(42342, 11826, 0);
      {
      __CrestLoad(42351, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42350, (unsigned long )0, (long long )1);
      __CrestApply2(42349, 18, (long long )((unsigned long )(d - 1)));
      __CrestLoad(42348, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42347, (unsigned long )0, (long long )1);
      __CrestApply2(42346, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42345, 12, (long long )((unsigned long )(d - 1) == (unsigned long )(string2 - 1)));
# 4957 "regex.c"
      if ((unsigned long )(d - 1) == (unsigned long )(string2 - 1)) {
        __CrestBranch(42352, 11827, 1);
# 4957 "regex.c"
        mem_431 = end1 - 1;
        __CrestLoad(42354, (unsigned long )mem_431, (long long )*mem_431);
        __CrestStore(42355, (unsigned long )(& tmp___107));
# 4957 "regex.c"
        tmp___107 = (int const )*mem_431;
      } else {
        __CrestBranch(42353, 11828, 0);
# 4957 "regex.c"
        mem_432 = d - 1;
        __CrestLoad(42356, (unsigned long )mem_432, (long long )*mem_432);
        __CrestStore(42357, (unsigned long )(& tmp___107));
# 4957 "regex.c"
        tmp___107 = (int const )*mem_432;
      }
      }
      __CrestLoad(42358, (unsigned long )(& tmp___107), (long long )tmp___107);
      __CrestStore(42359, (unsigned long )(& tmp___108));
# 4957 "regex.c"
      tmp___108 = tmp___107;
    }
    }
    __CrestLoad(42362, (unsigned long )(& re_syntax_table[tmp___108]), (long long )re_syntax_table[tmp___108]);
    __CrestLoad(42361, (unsigned long )0, (long long )1);
    __CrestApply2(42360, 12, (long long )((int )re_syntax_table[tmp___108] == 1));
    __CrestStore(42363, (unsigned long )(& prevchar___0));
# 4957 "regex.c"
    prevchar___0 = (boolean )((int )re_syntax_table[tmp___108] == 1);
    {
    __CrestLoad(42366, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42365, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42364, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 4958 "regex.c"
    if ((unsigned long )d == (unsigned long )end1) {
      __CrestBranch(42367, 11832, 1);
      __CrestLoad(42369, (unsigned long )string2, (long long )*string2);
      __CrestStore(42370, (unsigned long )(& tmp___110));
# 4958 "regex.c"
      tmp___110 = (int const )*string2;
    } else {
      __CrestBranch(42368, 11833, 0);
      {
      __CrestLoad(42375, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42374, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42373, (unsigned long )0, (long long )1);
      __CrestApply2(42372, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42371, 12, (long long )((unsigned long )d == (unsigned long )(string2 - 1)));
# 4958 "regex.c"
      if ((unsigned long )d == (unsigned long )(string2 - 1)) {
        __CrestBranch(42376, 11834, 1);
# 4958 "regex.c"
        mem_433 = end1 - 1;
        __CrestLoad(42378, (unsigned long )mem_433, (long long )*mem_433);
        __CrestStore(42379, (unsigned long )(& tmp___109));
# 4958 "regex.c"
        tmp___109 = (int const )*mem_433;
      } else {
        __CrestBranch(42377, 11835, 0);
        __CrestLoad(42380, (unsigned long )d, (long long )*d);
        __CrestStore(42381, (unsigned long )(& tmp___109));
# 4958 "regex.c"
        tmp___109 = (int const )*d;
      }
      }
      __CrestLoad(42382, (unsigned long )(& tmp___109), (long long )tmp___109);
      __CrestStore(42383, (unsigned long )(& tmp___110));
# 4958 "regex.c"
      tmp___110 = tmp___109;
    }
    }
    __CrestLoad(42386, (unsigned long )(& re_syntax_table[tmp___110]), (long long )re_syntax_table[tmp___110]);
    __CrestLoad(42385, (unsigned long )0, (long long )1);
    __CrestApply2(42384, 12, (long long )((int )re_syntax_table[tmp___110] == 1));
    __CrestStore(42387, (unsigned long )(& thischar___0));
# 4958 "regex.c"
    thischar___0 = (boolean )((int )re_syntax_table[tmp___110] == 1);
    {
    __CrestLoad(42390, (unsigned long )(& prevchar___0), (long long )prevchar___0);
    __CrestLoad(42389, (unsigned long )(& thischar___0), (long long )thischar___0);
    __CrestApply2(42388, 13, (long long )((int )prevchar___0 != (int )thischar___0));
# 4959 "regex.c"
    if ((int )prevchar___0 != (int )thischar___0) {
      __CrestBranch(42391, 11839, 1);
# 4960 "regex.c"
      goto fail;
    } else {
      __CrestBranch(42392, 11840, 0);

    }
    }
# 4961 "regex.c"
    goto switch_break;
    case_26: ;
    {
    __CrestLoad(42395, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42394, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42393, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 4967 "regex.c"
    if ((unsigned long )d == (unsigned long )end1) {
      __CrestBranch(42396, 11844, 1);
      __CrestLoad(42398, (unsigned long )string2, (long long )*string2);
      __CrestStore(42399, (unsigned long )(& tmp___112));
# 4967 "regex.c"
      tmp___112 = (int const )*string2;
    } else {
      __CrestBranch(42397, 11845, 0);
      {
      __CrestLoad(42404, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42403, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42402, (unsigned long )0, (long long )1);
      __CrestApply2(42401, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42400, 12, (long long )((unsigned long )d == (unsigned long )(string2 - 1)));
# 4967 "regex.c"
      if ((unsigned long )d == (unsigned long )(string2 - 1)) {
        __CrestBranch(42405, 11846, 1);
# 4967 "regex.c"
        mem_434 = end1 - 1;
        __CrestLoad(42407, (unsigned long )mem_434, (long long )*mem_434);
        __CrestStore(42408, (unsigned long )(& tmp___111));
# 4967 "regex.c"
        tmp___111 = (int const )*mem_434;
      } else {
        __CrestBranch(42406, 11847, 0);
        __CrestLoad(42409, (unsigned long )d, (long long )*d);
        __CrestStore(42410, (unsigned long )(& tmp___111));
# 4967 "regex.c"
        tmp___111 = (int const )*d;
      }
      }
      __CrestLoad(42411, (unsigned long )(& tmp___111), (long long )tmp___111);
      __CrestStore(42412, (unsigned long )(& tmp___112));
# 4967 "regex.c"
      tmp___112 = tmp___111;
    }
    }
    {
    __CrestLoad(42415, (unsigned long )(& re_syntax_table[tmp___112]), (long long )re_syntax_table[tmp___112]);
    __CrestLoad(42414, (unsigned long )0, (long long )1);
    __CrestApply2(42413, 12, (long long )((int )re_syntax_table[tmp___112] == 1));
# 4967 "regex.c"
    if ((int )re_syntax_table[tmp___112] == 1) {
      __CrestBranch(42416, 11850, 1);
      {
      __CrestLoad(42420, (unsigned long )(& size1), (long long )size1);
      __CrestLoad(42419, (unsigned long )0, (long long )0);
      __CrestApply2(42418, 13, (long long )(size1 != 0));
# 4967 "regex.c"
      if (size1 != 0) {
        __CrestBranch(42421, 11851, 1);
# 4967 "regex.c"
        tmp___113 = string1;
      } else {
        __CrestBranch(42422, 11852, 0);
# 4967 "regex.c"
        tmp___113 = string2;
      }
      }
      {
      __CrestLoad(42425, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42424, (unsigned long )(& tmp___113), (long long )((unsigned long )tmp___113));
      __CrestApply2(42423, 12, (long long )((unsigned long )d == (unsigned long )tmp___113));
# 4967 "regex.c"
      if ((unsigned long )d == (unsigned long )tmp___113) {
        __CrestBranch(42426, 11854, 1);
# 4968 "regex.c"
        goto switch_break;
      } else {
        __CrestBranch(42427, 11855, 0);
        {
        __CrestLoad(42430, (unsigned long )(& size2), (long long )size2);
        __CrestLoad(42429, (unsigned long )0, (long long )0);
        __CrestApply2(42428, 12, (long long )(size2 == 0));
# 4967 "regex.c"
        if (size2 == 0) {
          __CrestBranch(42431, 11856, 1);
# 4968 "regex.c"
          goto switch_break;
        } else {
          __CrestBranch(42432, 11857, 0);
          {
          __CrestLoad(42437, (unsigned long )(& d), (long long )((unsigned long )d));
          __CrestLoad(42436, (unsigned long )0, (long long )1);
          __CrestApply2(42435, 18, (long long )((unsigned long )(d - 1)));
          __CrestLoad(42434, (unsigned long )(& end1), (long long )((unsigned long )end1));
          __CrestApply2(42433, 12, (long long )((unsigned long )(d - 1) == (unsigned long )end1));
# 4967 "regex.c"
          if ((unsigned long )(d - 1) == (unsigned long )end1) {
            __CrestBranch(42438, 11858, 1);
            __CrestLoad(42440, (unsigned long )string2, (long long )*string2);
            __CrestStore(42441, (unsigned long )(& tmp___115));
# 4967 "regex.c"
            tmp___115 = (int const )*string2;
          } else {
            __CrestBranch(42439, 11859, 0);
            {
            __CrestLoad(42448, (unsigned long )(& d), (long long )((unsigned long )d));
            __CrestLoad(42447, (unsigned long )0, (long long )1);
            __CrestApply2(42446, 18, (long long )((unsigned long )(d - 1)));
            __CrestLoad(42445, (unsigned long )(& string2), (long long )((unsigned long )string2));
            __CrestLoad(42444, (unsigned long )0, (long long )1);
            __CrestApply2(42443, 18, (long long )((unsigned long )(string2 - 1)));
            __CrestApply2(42442, 12, (long long )((unsigned long )(d - 1) == (unsigned long )(string2 - 1)));
# 4967 "regex.c"
            if ((unsigned long )(d - 1) == (unsigned long )(string2 - 1)) {
              __CrestBranch(42449, 11860, 1);
# 4967 "regex.c"
              mem_435 = end1 - 1;
              __CrestLoad(42451, (unsigned long )mem_435, (long long )*mem_435);
              __CrestStore(42452, (unsigned long )(& tmp___114));
# 4967 "regex.c"
              tmp___114 = (int const )*mem_435;
            } else {
              __CrestBranch(42450, 11861, 0);
# 4967 "regex.c"
              mem_436 = d - 1;
              __CrestLoad(42453, (unsigned long )mem_436, (long long )*mem_436);
              __CrestStore(42454, (unsigned long )(& tmp___114));
# 4967 "regex.c"
              tmp___114 = (int const )*mem_436;
            }
            }
            __CrestLoad(42455, (unsigned long )(& tmp___114), (long long )tmp___114);
            __CrestStore(42456, (unsigned long )(& tmp___115));
# 4967 "regex.c"
            tmp___115 = tmp___114;
          }
          }
          {
          __CrestLoad(42459, (unsigned long )(& re_syntax_table[tmp___115]), (long long )re_syntax_table[tmp___115]);
          __CrestLoad(42458, (unsigned long )0, (long long )1);
          __CrestApply2(42457, 12, (long long )((int )re_syntax_table[tmp___115] == 1));
# 4967 "regex.c"
          if ((int )re_syntax_table[tmp___115] == 1) {
            __CrestBranch(42460, 11864, 1);

          } else {
            __CrestBranch(42461, 11865, 0);
# 4968 "regex.c"
            goto switch_break;
          }
          }
        }
        }
      }
      }
    } else {
      __CrestBranch(42417, 11866, 0);

    }
    }
# 4969 "regex.c"
    goto fail;
    case_27: ;
    {
    __CrestLoad(42464, (unsigned long )(& size1), (long long )size1);
    __CrestLoad(42463, (unsigned long )0, (long long )0);
    __CrestApply2(42462, 13, (long long )(size1 != 0));
# 4973 "regex.c"
    if (size1 != 0) {
      __CrestBranch(42465, 11870, 1);
# 4973 "regex.c"
      tmp___116 = string1;
    } else {
      __CrestBranch(42466, 11871, 0);
# 4973 "regex.c"
      tmp___116 = string2;
    }
    }
    {
    __CrestLoad(42469, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42468, (unsigned long )(& tmp___116), (long long )((unsigned long )tmp___116));
    __CrestApply2(42467, 12, (long long )((unsigned long )d == (unsigned long )tmp___116));
# 4973 "regex.c"
    if ((unsigned long )d == (unsigned long )tmp___116) {
      __CrestBranch(42470, 11873, 1);

    } else {
      __CrestBranch(42471, 11874, 0);
      {
      __CrestLoad(42474, (unsigned long )(& size2), (long long )size2);
      __CrestLoad(42473, (unsigned long )0, (long long )0);
      __CrestApply2(42472, 12, (long long )(size2 == 0));
# 4973 "regex.c"
      if (size2 == 0) {
        __CrestBranch(42475, 11875, 1);

      } else {
        __CrestBranch(42476, 11876, 0);
        {
        __CrestLoad(42481, (unsigned long )(& d), (long long )((unsigned long )d));
        __CrestLoad(42480, (unsigned long )0, (long long )1);
        __CrestApply2(42479, 18, (long long )((unsigned long )(d - 1)));
        __CrestLoad(42478, (unsigned long )(& end1), (long long )((unsigned long )end1));
        __CrestApply2(42477, 12, (long long )((unsigned long )(d - 1) == (unsigned long )end1));
# 4973 "regex.c"
        if ((unsigned long )(d - 1) == (unsigned long )end1) {
          __CrestBranch(42482, 11877, 1);
          __CrestLoad(42484, (unsigned long )string2, (long long )*string2);
          __CrestStore(42485, (unsigned long )(& tmp___118));
# 4973 "regex.c"
          tmp___118 = (int const )*string2;
        } else {
          __CrestBranch(42483, 11878, 0);
          {
          __CrestLoad(42492, (unsigned long )(& d), (long long )((unsigned long )d));
          __CrestLoad(42491, (unsigned long )0, (long long )1);
          __CrestApply2(42490, 18, (long long )((unsigned long )(d - 1)));
          __CrestLoad(42489, (unsigned long )(& string2), (long long )((unsigned long )string2));
          __CrestLoad(42488, (unsigned long )0, (long long )1);
          __CrestApply2(42487, 18, (long long )((unsigned long )(string2 - 1)));
          __CrestApply2(42486, 12, (long long )((unsigned long )(d - 1) == (unsigned long )(string2 - 1)));
# 4973 "regex.c"
          if ((unsigned long )(d - 1) == (unsigned long )(string2 - 1)) {
            __CrestBranch(42493, 11879, 1);
# 4973 "regex.c"
            mem_437 = end1 - 1;
            __CrestLoad(42495, (unsigned long )mem_437, (long long )*mem_437);
            __CrestStore(42496, (unsigned long )(& tmp___117));
# 4973 "regex.c"
            tmp___117 = (int const )*mem_437;
          } else {
            __CrestBranch(42494, 11880, 0);
# 4973 "regex.c"
            mem_438 = d - 1;
            __CrestLoad(42497, (unsigned long )mem_438, (long long )*mem_438);
            __CrestStore(42498, (unsigned long )(& tmp___117));
# 4973 "regex.c"
            tmp___117 = (int const )*mem_438;
          }
          }
          __CrestLoad(42499, (unsigned long )(& tmp___117), (long long )tmp___117);
          __CrestStore(42500, (unsigned long )(& tmp___118));
# 4973 "regex.c"
          tmp___118 = tmp___117;
        }
        }
        {
        __CrestLoad(42503, (unsigned long )(& re_syntax_table[tmp___118]), (long long )re_syntax_table[tmp___118]);
        __CrestLoad(42502, (unsigned long )0, (long long )1);
        __CrestApply2(42501, 12, (long long )((int )re_syntax_table[tmp___118] == 1));
# 4973 "regex.c"
        if ((int )re_syntax_table[tmp___118] == 1) {
          __CrestBranch(42504, 11883, 1);
          {
          __CrestLoad(42508, (unsigned long )(& d), (long long )((unsigned long )d));
          __CrestLoad(42507, (unsigned long )(& end1), (long long )((unsigned long )end1));
          __CrestApply2(42506, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 4973 "regex.c"
          if ((unsigned long )d == (unsigned long )end1) {
            __CrestBranch(42509, 11884, 1);
            __CrestLoad(42511, (unsigned long )string2, (long long )*string2);
            __CrestStore(42512, (unsigned long )(& tmp___120));
# 4973 "regex.c"
            tmp___120 = (int const )*string2;
          } else {
            __CrestBranch(42510, 11885, 0);
            {
            __CrestLoad(42517, (unsigned long )(& d), (long long )((unsigned long )d));
            __CrestLoad(42516, (unsigned long )(& string2), (long long )((unsigned long )string2));
            __CrestLoad(42515, (unsigned long )0, (long long )1);
            __CrestApply2(42514, 18, (long long )((unsigned long )(string2 - 1)));
            __CrestApply2(42513, 12, (long long )((unsigned long )d == (unsigned long )(string2 - 1)));
# 4973 "regex.c"
            if ((unsigned long )d == (unsigned long )(string2 - 1)) {
              __CrestBranch(42518, 11886, 1);
# 4973 "regex.c"
              mem_439 = end1 - 1;
              __CrestLoad(42520, (unsigned long )mem_439, (long long )*mem_439);
              __CrestStore(42521, (unsigned long )(& tmp___119));
# 4973 "regex.c"
              tmp___119 = (int const )*mem_439;
            } else {
              __CrestBranch(42519, 11887, 0);
              __CrestLoad(42522, (unsigned long )d, (long long )*d);
              __CrestStore(42523, (unsigned long )(& tmp___119));
# 4973 "regex.c"
              tmp___119 = (int const )*d;
            }
            }
            __CrestLoad(42524, (unsigned long )(& tmp___119), (long long )tmp___119);
            __CrestStore(42525, (unsigned long )(& tmp___120));
# 4973 "regex.c"
            tmp___120 = tmp___119;
          }
          }
          {
          __CrestLoad(42528, (unsigned long )(& re_syntax_table[tmp___120]), (long long )re_syntax_table[tmp___120]);
          __CrestLoad(42527, (unsigned long )0, (long long )1);
          __CrestApply2(42526, 12, (long long )((int )re_syntax_table[tmp___120] == 1));
# 4973 "regex.c"
          if ((int )re_syntax_table[tmp___120] == 1) {
            __CrestBranch(42529, 11890, 1);
            {
            __CrestLoad(42533, (unsigned long )(& d), (long long )((unsigned long )d));
            __CrestLoad(42532, (unsigned long )(& end2), (long long )((unsigned long )end2));
            __CrestApply2(42531, 12, (long long )((unsigned long )d == (unsigned long )end2));
# 4973 "regex.c"
            if ((unsigned long )d == (unsigned long )end2) {
              __CrestBranch(42534, 11891, 1);
# 4975 "regex.c"
              goto switch_break;
            } else {
              __CrestBranch(42535, 11892, 0);

            }
            }
          } else {
            __CrestBranch(42530, 11893, 0);
# 4975 "regex.c"
            goto switch_break;
          }
          }
        } else {
          __CrestBranch(42505, 11894, 0);

        }
        }
      }
      }
    }
    }
# 4976 "regex.c"
    goto fail;
    case_24: ;
    {
# 5034 "regex.c"
    while (1) {
      while_continue___72: ;
      {
      __CrestLoad(42538, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42537, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestApply2(42536, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 5034 "regex.c"
      if ((unsigned long )d == (unsigned long )dend) {
        __CrestBranch(42539, 11901, 1);

      } else {
        __CrestBranch(42540, 11902, 0);
# 5034 "regex.c"
        goto while_break___72;
      }
      }
      {
      __CrestLoad(42543, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestLoad(42542, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
      __CrestApply2(42541, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 5034 "regex.c"
      if ((unsigned long )dend == (unsigned long )end_match_2) {
        __CrestBranch(42544, 11904, 1);
# 5034 "regex.c"
        goto fail;
      } else {
        __CrestBranch(42545, 11905, 0);

      }
      }
# 5034 "regex.c"
      d = string2;
# 5034 "regex.c"
      dend = end_match_2;
    }
    while_break___72: ;
    }
    {
    __CrestLoad(42548, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42547, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42546, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 5035 "regex.c"
    if ((unsigned long )d == (unsigned long )end1) {
      __CrestBranch(42549, 11909, 1);
      __CrestLoad(42551, (unsigned long )string2, (long long )*string2);
      __CrestStore(42552, (unsigned long )(& tmp___122));
# 5035 "regex.c"
      tmp___122 = (int const )*string2;
    } else {
      __CrestBranch(42550, 11910, 0);
      {
      __CrestLoad(42557, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42556, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42555, (unsigned long )0, (long long )1);
      __CrestApply2(42554, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42553, 12, (long long )((unsigned long )d == (unsigned long )(string2 - 1)));
# 5035 "regex.c"
      if ((unsigned long )d == (unsigned long )(string2 - 1)) {
        __CrestBranch(42558, 11911, 1);
# 5035 "regex.c"
        mem_440 = end1 - 1;
        __CrestLoad(42560, (unsigned long )mem_440, (long long )*mem_440);
        __CrestStore(42561, (unsigned long )(& tmp___121));
# 5035 "regex.c"
        tmp___121 = (int const )*mem_440;
      } else {
        __CrestBranch(42559, 11912, 0);
        __CrestLoad(42562, (unsigned long )d, (long long )*d);
        __CrestStore(42563, (unsigned long )(& tmp___121));
# 5035 "regex.c"
        tmp___121 = (int const )*d;
      }
      }
      __CrestLoad(42564, (unsigned long )(& tmp___121), (long long )tmp___121);
      __CrestStore(42565, (unsigned long )(& tmp___122));
# 5035 "regex.c"
      tmp___122 = tmp___121;
    }
    }
    {
    __CrestLoad(42568, (unsigned long )(& re_syntax_table[tmp___122]), (long long )re_syntax_table[tmp___122]);
    __CrestLoad(42567, (unsigned long )0, (long long )1);
    __CrestApply2(42566, 12, (long long )((int )re_syntax_table[tmp___122] == 1));
# 5035 "regex.c"
    if ((int )re_syntax_table[tmp___122] == 1) {
      __CrestBranch(42569, 11915, 1);

    } else {
      __CrestBranch(42570, 11916, 0);
# 5036 "regex.c"
      goto fail;
    }
    }
    {
# 5037 "regex.c"
    while (1) {
      while_continue___73: ;
      {
      __CrestLoad(42573, (unsigned long )(& set_regs_matched_done), (long long )set_regs_matched_done);
      __CrestLoad(42572, (unsigned long )0, (long long )0);
      __CrestApply2(42571, 12, (long long )(set_regs_matched_done == 0));
# 5037 "regex.c"
      if (set_regs_matched_done == 0) {
        __CrestBranch(42574, 11921, 1);
        __CrestLoad(42576, (unsigned long )0, (long long )1);
        __CrestStore(42577, (unsigned long )(& set_regs_matched_done));
# 5037 "regex.c"
        set_regs_matched_done = 1;
        __CrestLoad(42578, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestStore(42579, (unsigned long )(& r___5));
# 5037 "regex.c"
        r___5 = lowest_active_reg;
        {
# 5037 "regex.c"
        while (1) {
          while_continue___74: ;
          {
          __CrestLoad(42582, (unsigned long )(& r___5), (long long )r___5);
          __CrestLoad(42581, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
          __CrestApply2(42580, 15, (long long )(r___5 <= highest_active_reg));
# 5037 "regex.c"
          if (r___5 <= highest_active_reg) {
            __CrestBranch(42583, 11926, 1);

          } else {
            __CrestBranch(42584, 11927, 0);
# 5037 "regex.c"
            goto while_break___74;
          }
          }
          __CrestLoad(42585, (unsigned long )0, (long long )1U);
          __CrestStore(42586, (unsigned long )(& tmp___123));
# 5037 "regex.c"
          tmp___123 = 1U;
# 5037 "regex.c"
          mem_441 = reg_info + r___5;
# 5037 "regex.c"
          mem_441->bits.ever_matched_something = tmp___123;
# 5037 "regex.c"
          mem_442 = reg_info + r___5;
# 5037 "regex.c"
          mem_442->bits.matched_something = tmp___123;
          __CrestLoad(42589, (unsigned long )(& r___5), (long long )r___5);
          __CrestLoad(42588, (unsigned long )0, (long long )1UL);
          __CrestApply2(42587, 0, (long long )(r___5 + 1UL));
          __CrestStore(42590, (unsigned long )(& r___5));
# 5037 "regex.c"
          r___5 ++;
        }
        while_break___74: ;
        }
      } else {
        __CrestBranch(42575, 11930, 0);

      }
      }
# 5037 "regex.c"
      goto while_break___73;
    }
    while_break___73: ;
    }
# 5038 "regex.c"
    d ++;
# 5039 "regex.c"
    goto switch_break;
    case_25: ;
    {
# 5043 "regex.c"
    while (1) {
      while_continue___75: ;
      {
      __CrestLoad(42593, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42592, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestApply2(42591, 12, (long long )((unsigned long )d == (unsigned long )dend));
# 5043 "regex.c"
      if ((unsigned long )d == (unsigned long )dend) {
        __CrestBranch(42594, 11940, 1);

      } else {
        __CrestBranch(42595, 11941, 0);
# 5043 "regex.c"
        goto while_break___75;
      }
      }
      {
      __CrestLoad(42598, (unsigned long )(& dend), (long long )((unsigned long )dend));
      __CrestLoad(42597, (unsigned long )(& end_match_2), (long long )((unsigned long )end_match_2));
      __CrestApply2(42596, 12, (long long )((unsigned long )dend == (unsigned long )end_match_2));
# 5043 "regex.c"
      if ((unsigned long )dend == (unsigned long )end_match_2) {
        __CrestBranch(42599, 11943, 1);
# 5043 "regex.c"
        goto fail;
      } else {
        __CrestBranch(42600, 11944, 0);

      }
      }
# 5043 "regex.c"
      d = string2;
# 5043 "regex.c"
      dend = end_match_2;
    }
    while_break___75: ;
    }
    {
    __CrestLoad(42603, (unsigned long )(& d), (long long )((unsigned long )d));
    __CrestLoad(42602, (unsigned long )(& end1), (long long )((unsigned long )end1));
    __CrestApply2(42601, 12, (long long )((unsigned long )d == (unsigned long )end1));
# 5044 "regex.c"
    if ((unsigned long )d == (unsigned long )end1) {
      __CrestBranch(42604, 11948, 1);
      __CrestLoad(42606, (unsigned long )string2, (long long )*string2);
      __CrestStore(42607, (unsigned long )(& tmp___125));
# 5044 "regex.c"
      tmp___125 = (int const )*string2;
    } else {
      __CrestBranch(42605, 11949, 0);
      {
      __CrestLoad(42612, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42611, (unsigned long )(& string2), (long long )((unsigned long )string2));
      __CrestLoad(42610, (unsigned long )0, (long long )1);
      __CrestApply2(42609, 18, (long long )((unsigned long )(string2 - 1)));
      __CrestApply2(42608, 12, (long long )((unsigned long )d == (unsigned long )(string2 - 1)));
# 5044 "regex.c"
      if ((unsigned long )d == (unsigned long )(string2 - 1)) {
        __CrestBranch(42613, 11950, 1);
# 5044 "regex.c"
        mem_443 = end1 - 1;
        __CrestLoad(42615, (unsigned long )mem_443, (long long )*mem_443);
        __CrestStore(42616, (unsigned long )(& tmp___124));
# 5044 "regex.c"
        tmp___124 = (int const )*mem_443;
      } else {
        __CrestBranch(42614, 11951, 0);
        __CrestLoad(42617, (unsigned long )d, (long long )*d);
        __CrestStore(42618, (unsigned long )(& tmp___124));
# 5044 "regex.c"
        tmp___124 = (int const )*d;
      }
      }
      __CrestLoad(42619, (unsigned long )(& tmp___124), (long long )tmp___124);
      __CrestStore(42620, (unsigned long )(& tmp___125));
# 5044 "regex.c"
      tmp___125 = tmp___124;
    }
    }
    {
    __CrestLoad(42623, (unsigned long )(& re_syntax_table[tmp___125]), (long long )re_syntax_table[tmp___125]);
    __CrestLoad(42622, (unsigned long )0, (long long )1);
    __CrestApply2(42621, 12, (long long )((int )re_syntax_table[tmp___125] == 1));
# 5044 "regex.c"
    if ((int )re_syntax_table[tmp___125] == 1) {
      __CrestBranch(42624, 11954, 1);
# 5045 "regex.c"
      goto fail;
    } else {
      __CrestBranch(42625, 11955, 0);

    }
    }
    {
# 5046 "regex.c"
    while (1) {
      while_continue___76: ;
      {
      __CrestLoad(42628, (unsigned long )(& set_regs_matched_done), (long long )set_regs_matched_done);
      __CrestLoad(42627, (unsigned long )0, (long long )0);
      __CrestApply2(42626, 12, (long long )(set_regs_matched_done == 0));
# 5046 "regex.c"
      if (set_regs_matched_done == 0) {
        __CrestBranch(42629, 11960, 1);
        __CrestLoad(42631, (unsigned long )0, (long long )1);
        __CrestStore(42632, (unsigned long )(& set_regs_matched_done));
# 5046 "regex.c"
        set_regs_matched_done = 1;
        __CrestLoad(42633, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestStore(42634, (unsigned long )(& r___6));
# 5046 "regex.c"
        r___6 = lowest_active_reg;
        {
# 5046 "regex.c"
        while (1) {
          while_continue___77: ;
          {
          __CrestLoad(42637, (unsigned long )(& r___6), (long long )r___6);
          __CrestLoad(42636, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
          __CrestApply2(42635, 15, (long long )(r___6 <= highest_active_reg));
# 5046 "regex.c"
          if (r___6 <= highest_active_reg) {
            __CrestBranch(42638, 11965, 1);

          } else {
            __CrestBranch(42639, 11966, 0);
# 5046 "regex.c"
            goto while_break___77;
          }
          }
          __CrestLoad(42640, (unsigned long )0, (long long )1U);
          __CrestStore(42641, (unsigned long )(& tmp___126));
# 5046 "regex.c"
          tmp___126 = 1U;
# 5046 "regex.c"
          mem_444 = reg_info + r___6;
# 5046 "regex.c"
          mem_444->bits.ever_matched_something = tmp___126;
# 5046 "regex.c"
          mem_445 = reg_info + r___6;
# 5046 "regex.c"
          mem_445->bits.matched_something = tmp___126;
          __CrestLoad(42644, (unsigned long )(& r___6), (long long )r___6);
          __CrestLoad(42643, (unsigned long )0, (long long )1UL);
          __CrestApply2(42642, 0, (long long )(r___6 + 1UL));
          __CrestStore(42645, (unsigned long )(& r___6));
# 5046 "regex.c"
          r___6 ++;
        }
        while_break___77: ;
        }
      } else {
        __CrestBranch(42630, 11969, 0);

      }
      }
# 5046 "regex.c"
      goto while_break___76;
    }
    while_break___76: ;
    }
# 5047 "regex.c"
    d ++;
# 5048 "regex.c"
    goto switch_break;
    switch_default___0:
# 5052 "regex.c"
    abort();
    __CrestClearStack(42646);
    switch_break: ;
    }
# 5054 "regex.c"
    goto __Cont;
    fail:
    {
    __CrestLoad(42649, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
    __CrestLoad(42648, (unsigned long )0, (long long )0U);
    __CrestApply2(42647, 13, (long long )(fail_stack.avail != 0U));
# 5059 "regex.c"
    if (fail_stack.avail != 0U) {
      __CrestBranch(42650, 11978, 1);
      __CrestLoad(42654, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42653, (unsigned long )0, (long long )1U);
      __CrestApply2(42652, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(42655, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
      (fail_stack.avail) --;
# 5062 "regex.c"
      mem_446 = fail_stack.stack + fail_stack.avail;
# 5062 "regex.c"
      string_temp___0 = (unsigned char const *)mem_446->pointer;
      {
      __CrestLoad(42658, (unsigned long )(& string_temp___0), (long long )((unsigned long )string_temp___0));
      __CrestLoad(42657, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(42656, 13, (long long )((unsigned long )string_temp___0 != (unsigned long )((void *)0)));
# 5062 "regex.c"
      if ((unsigned long )string_temp___0 != (unsigned long )((void *)0)) {
        __CrestBranch(42659, 11980, 1);
# 5062 "regex.c"
        d = (char const *)string_temp___0;
      } else {
        __CrestBranch(42660, 11981, 0);

      }
      }
      __CrestLoad(42663, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42662, (unsigned long )0, (long long )1U);
      __CrestApply2(42661, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(42664, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
      (fail_stack.avail) --;
# 5062 "regex.c"
      mem_447 = fail_stack.stack + fail_stack.avail;
# 5062 "regex.c"
      p = mem_447->pointer;
      __CrestLoad(42667, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42666, (unsigned long )0, (long long )1U);
      __CrestApply2(42665, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(42668, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
      (fail_stack.avail) --;
# 5062 "regex.c"
      mem_448 = fail_stack.stack + fail_stack.avail;
      __CrestLoad(42669, (unsigned long )(& mem_448->integer), (long long )mem_448->integer);
      __CrestStore(42670, (unsigned long )(& highest_active_reg));
# 5062 "regex.c"
      highest_active_reg = (active_reg_t )mem_448->integer;
      __CrestLoad(42673, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
      __CrestLoad(42672, (unsigned long )0, (long long )1U);
      __CrestApply2(42671, 1, (long long )(fail_stack.avail - 1U));
      __CrestStore(42674, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
      (fail_stack.avail) --;
# 5062 "regex.c"
      mem_449 = fail_stack.stack + fail_stack.avail;
      __CrestLoad(42675, (unsigned long )(& mem_449->integer), (long long )mem_449->integer);
      __CrestStore(42676, (unsigned long )(& lowest_active_reg));
# 5062 "regex.c"
      lowest_active_reg = (active_reg_t )mem_449->integer;
      __CrestLoad(42677, (unsigned long )(& highest_active_reg), (long long )highest_active_reg);
      __CrestStore(42678, (unsigned long )(& this_reg___5));
# 5062 "regex.c"
      this_reg___5 = (s_reg_t )highest_active_reg;
      {
# 5062 "regex.c"
      while (1) {
        while_continue___78: ;
        {
        __CrestLoad(42681, (unsigned long )(& this_reg___5), (long long )this_reg___5);
        __CrestLoad(42680, (unsigned long )(& lowest_active_reg), (long long )lowest_active_reg);
        __CrestApply2(42679, 17, (long long )((active_reg_t )this_reg___5 >= lowest_active_reg));
# 5062 "regex.c"
        if ((active_reg_t )this_reg___5 >= lowest_active_reg) {
          __CrestBranch(42682, 11987, 1);

        } else {
          __CrestBranch(42683, 11988, 0);
# 5062 "regex.c"
          goto while_break___78;
        }
        }
        __CrestLoad(42686, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42685, (unsigned long )0, (long long )1U);
        __CrestApply2(42684, 1, (long long )(fail_stack.avail - 1U));
        __CrestStore(42687, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
        (fail_stack.avail) --;
# 5062 "regex.c"
        mem_450 = reg_info + this_reg___5;
# 5062 "regex.c"
        mem_451 = fail_stack.stack + fail_stack.avail;
# 5062 "regex.c"
        mem_450->word = *mem_451;
        __CrestLoad(42690, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42689, (unsigned long )0, (long long )1U);
        __CrestApply2(42688, 1, (long long )(fail_stack.avail - 1U));
        __CrestStore(42691, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
        (fail_stack.avail) --;
# 5062 "regex.c"
        mem_452 = regend + this_reg___5;
# 5062 "regex.c"
        mem_453 = fail_stack.stack + fail_stack.avail;
# 5062 "regex.c"
        *mem_452 = (char const *)mem_453->pointer;
        __CrestLoad(42694, (unsigned long )(& fail_stack.avail), (long long )fail_stack.avail);
        __CrestLoad(42693, (unsigned long )0, (long long )1U);
        __CrestApply2(42692, 1, (long long )(fail_stack.avail - 1U));
        __CrestStore(42695, (unsigned long )(& fail_stack.avail));
# 5062 "regex.c"
        (fail_stack.avail) --;
# 5062 "regex.c"
        mem_454 = regstart + this_reg___5;
# 5062 "regex.c"
        mem_455 = fail_stack.stack + fail_stack.avail;
# 5062 "regex.c"
        *mem_454 = (char const *)mem_455->pointer;
        __CrestLoad(42698, (unsigned long )(& this_reg___5), (long long )this_reg___5);
        __CrestLoad(42697, (unsigned long )0, (long long )1L);
        __CrestApply2(42696, 1, (long long )(this_reg___5 - 1L));
        __CrestStore(42699, (unsigned long )(& this_reg___5));
# 5062 "regex.c"
        this_reg___5 --;
      }
      while_break___78: ;
      }
      __CrestLoad(42700, (unsigned long )0, (long long )0);
      __CrestStore(42701, (unsigned long )(& set_regs_matched_done));
# 5062 "regex.c"
      set_regs_matched_done = 0;
      {
      __CrestLoad(42704, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(42703, (unsigned long )0, (long long )0);
      __CrestApply2(42702, 12, (long long )(p == 0));
# 5067 "regex.c"
      if (p == 0) {
        __CrestBranch(42705, 11993, 1);
# 5068 "regex.c"
        goto fail;
      } else {
        __CrestBranch(42706, 11994, 0);

      }
      }
      {
      __CrestLoad(42709, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(42708, (unsigned long )(& pend), (long long )((unsigned long )pend));
      __CrestApply2(42707, 16, (long long )((unsigned long )p < (unsigned long )pend));
# 5072 "regex.c"
      if ((unsigned long )p < (unsigned long )pend) {
        __CrestBranch(42710, 11996, 1);
        __CrestLoad(42712, (unsigned long )0, (long long )((boolean )0));
        __CrestStore(42713, (unsigned long )(& is_a_jump_n___0));
# 5074 "regex.c"
        is_a_jump_n___0 = (boolean )0;
        {
        {
        __CrestLoad(42716, (unsigned long )p, (long long )*p);
        __CrestLoad(42715, (unsigned long )0, (long long )22U);
        __CrestApply2(42714, 12, (long long )((unsigned int )((re_opcode_t )*p) == 22U));
# 5080 "regex.c"
        if ((unsigned int )((re_opcode_t )*p) == 22U) {
          __CrestBranch(42717, 11999, 1);
# 5080 "regex.c"
          goto case_22___1;
        } else {
          __CrestBranch(42718, 12000, 0);

        }
        }
        {
        __CrestLoad(42721, (unsigned long )p, (long long )*p);
        __CrestLoad(42720, (unsigned long )0, (long long )13U);
        __CrestApply2(42719, 12, (long long )((unsigned int )((re_opcode_t )*p) == 13U));
# 5084 "regex.c"
        if ((unsigned int )((re_opcode_t )*p) == 13U) {
          __CrestBranch(42722, 12002, 1);
# 5084 "regex.c"
          goto case_13___1;
        } else {
          __CrestBranch(42723, 12003, 0);

        }
        }
        {
        __CrestLoad(42726, (unsigned long )p, (long long )*p);
        __CrestLoad(42725, (unsigned long )0, (long long )17U);
        __CrestApply2(42724, 12, (long long )((unsigned int )((re_opcode_t )*p) == 17U));
# 5084 "regex.c"
        if ((unsigned int )((re_opcode_t )*p) == 17U) {
          __CrestBranch(42727, 12005, 1);
# 5084 "regex.c"
          goto case_13___1;
        } else {
          __CrestBranch(42728, 12006, 0);

        }
        }
        {
        __CrestLoad(42731, (unsigned long )p, (long long )*p);
        __CrestLoad(42730, (unsigned long )0, (long long )18U);
        __CrestApply2(42729, 12, (long long )((unsigned int )((re_opcode_t )*p) == 18U));
# 5084 "regex.c"
        if ((unsigned int )((re_opcode_t )*p) == 18U) {
          __CrestBranch(42732, 12008, 1);
# 5084 "regex.c"
          goto case_13___1;
        } else {
          __CrestBranch(42733, 12009, 0);

        }
        }
# 5094 "regex.c"
        goto switch_default___1;
        case_22___1:
        __CrestLoad(42734, (unsigned long )0, (long long )((boolean )1));
        __CrestStore(42735, (unsigned long )(& is_a_jump_n___0));
# 5081 "regex.c"
        is_a_jump_n___0 = (boolean )1;
        case_13___1:
        case_17___1:
        case_18___1:
# 5085 "regex.c"
        p1 = p + 1;
        {
# 5086 "regex.c"
        while (1) {
          while_continue___79: ;
          {
# 5086 "regex.c"
          while (1) {
            while_continue___80: ;
            __CrestLoad(42738, (unsigned long )p1, (long long )*p1);
            __CrestLoad(42737, (unsigned long )0, (long long )255);
            __CrestApply2(42736, 5, (long long )((int )*p1 & 255));
            __CrestStore(42739, (unsigned long )(& mcnt));
# 5086 "regex.c"
            mcnt = (int )*p1 & 255;
# 5086 "regex.c"
            mem_456 = p1 + 1;
            __CrestLoad(42744, (unsigned long )(& mcnt), (long long )mcnt);
            __CrestLoad(42743, (unsigned long )mem_456, (long long )*mem_456);
            __CrestLoad(42742, (unsigned long )0, (long long )8);
            __CrestApply2(42741, 8, (long long )((int )((signed char )*mem_456) << 8));
            __CrestApply2(42740, 0, (long long )(mcnt + ((int )((signed char )*mem_456) << 8)));
            __CrestStore(42745, (unsigned long )(& mcnt));
# 5086 "regex.c"
            mcnt += (int )((signed char )*mem_456) << 8;
# 5086 "regex.c"
            goto while_break___80;
          }
          while_break___80: ;
          }
# 5086 "regex.c"
          p1 += 2;
# 5086 "regex.c"
          goto while_break___79;
        }
        while_break___79: ;
        }
# 5087 "regex.c"
        p1 += mcnt;
        {
        __CrestLoad(42748, (unsigned long )(& is_a_jump_n___0), (long long )is_a_jump_n___0);
        __CrestLoad(42747, (unsigned long )0, (long long )0);
        __CrestApply2(42746, 13, (long long )(is_a_jump_n___0 != 0));
# 5089 "regex.c"
        if (is_a_jump_n___0 != 0) {
          __CrestBranch(42749, 12027, 1);
          {
          __CrestLoad(42753, (unsigned long )p1, (long long )*p1);
          __CrestLoad(42752, (unsigned long )0, (long long )21U);
          __CrestApply2(42751, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 21U));
# 5089 "regex.c"
          if ((unsigned int )((re_opcode_t )*p1) == 21U) {
            __CrestBranch(42754, 12028, 1);
# 5092 "regex.c"
            goto fail;
          } else {
            __CrestBranch(42755, 12029, 0);
# 5089 "regex.c"
            goto _L___20;
          }
          }
        } else {
          __CrestBranch(42750, 12030, 0);
          _L___20:
          {
          __CrestLoad(42758, (unsigned long )(& is_a_jump_n___0), (long long )is_a_jump_n___0);
          __CrestLoad(42757, (unsigned long )0, (long long )0);
          __CrestApply2(42756, 12, (long long )(is_a_jump_n___0 == 0));
# 5089 "regex.c"
          if (is_a_jump_n___0 == 0) {
            __CrestBranch(42759, 12031, 1);
            {
            __CrestLoad(42763, (unsigned long )p1, (long long )*p1);
            __CrestLoad(42762, (unsigned long )0, (long long )15U);
            __CrestApply2(42761, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 15U));
# 5089 "regex.c"
            if ((unsigned int )((re_opcode_t )*p1) == 15U) {
              __CrestBranch(42764, 12032, 1);
# 5092 "regex.c"
              goto fail;
            } else {
              __CrestBranch(42765, 12033, 0);

            }
            }
          } else {
            __CrestBranch(42760, 12034, 0);

          }
          }
        }
        }
# 5093 "regex.c"
        goto switch_break___1;
        switch_default___1: ;
        switch_break___1: ;
        }
      } else {
        __CrestBranch(42711, 12038, 0);

      }
      }
      {
      __CrestLoad(42768, (unsigned long )(& d), (long long )((unsigned long )d));
      __CrestLoad(42767, (unsigned long )(& string1), (long long )((unsigned long )string1));
      __CrestApply2(42766, 17, (long long )((unsigned long )d >= (unsigned long )string1));
# 5099 "regex.c"
      if ((unsigned long )d >= (unsigned long )string1) {
        __CrestBranch(42769, 12040, 1);
        {
        __CrestLoad(42773, (unsigned long )(& d), (long long )((unsigned long )d));
        __CrestLoad(42772, (unsigned long )(& end1), (long long )((unsigned long )end1));
        __CrestApply2(42771, 15, (long long )((unsigned long )d <= (unsigned long )end1));
# 5099 "regex.c"
        if ((unsigned long )d <= (unsigned long )end1) {
          __CrestBranch(42774, 12041, 1);
# 5100 "regex.c"
          dend = end_match_1;
        } else {
          __CrestBranch(42775, 12042, 0);

        }
        }
      } else {
        __CrestBranch(42770, 12043, 0);

      }
      }
    } else {
      __CrestBranch(42651, 12044, 0);
# 5103 "regex.c"
      goto while_break___3;
    }
    }
    __Cont: ;
  }
  while_break___3: ;
  }
  {
  __CrestLoad(42778, (unsigned long )(& best_regs_set), (long long )best_regs_set);
  __CrestLoad(42777, (unsigned long )0, (long long )0);
  __CrestApply2(42776, 13, (long long )(best_regs_set != 0));
# 5106 "regex.c"
  if (best_regs_set != 0) {
    __CrestBranch(42779, 12048, 1);
# 5107 "regex.c"
    goto restore_best_regs;
  } else {
    __CrestBranch(42780, 12049, 0);

  }
  }
  {
# 5109 "regex.c"
  while (1) {
    while_continue___81: ;
# 5109 "regex.c"
    regstart = (char const **)((void *)0);
# 5109 "regex.c"
    regend = (char const **)((void *)0);
# 5109 "regex.c"
    old_regstart = (char const **)((void *)0);
# 5109 "regex.c"
    old_regend = (char const **)((void *)0);
# 5109 "regex.c"
    best_regstart = (char const **)((void *)0);
# 5109 "regex.c"
    best_regend = (char const **)((void *)0);
# 5109 "regex.c"
    reg_info = (register_info_type *)((void *)0);
# 5109 "regex.c"
    reg_dummy = (char const **)((void *)0);
# 5109 "regex.c"
    reg_info_dummy = (register_info_type *)((void *)0);
# 5109 "regex.c"
    goto while_break___81;
  }
  while_break___81: ;
  }
  __CrestLoad(42781, (unsigned long )0, (long long )-1);
  __CrestStore(42782, (unsigned long )(& __retres457));
# 5111 "regex.c"
  __retres457 = -1;
  return_label:
  {
  __CrestLoad(42783, (unsigned long )(& __retres457), (long long )__retres457);
  __CrestReturn(42784);
# 3724 "regex.c"
  return (__retres457);
  }
}
}
# 5127 "regex.c"
static boolean group_match_null_string_p(unsigned char **p , unsigned char *end ,
                                         register_info_type *reg_info )
{
  int mcnt ;
  unsigned char *p1 ;
  boolean tmp ;
  boolean tmp___0 ;
  boolean tmp___1 ;
  unsigned char *mem_9 ;
  unsigned char *mem_10 ;
  unsigned char *mem_11 ;
  unsigned char *mem_12 ;
  unsigned char *mem_13 ;
  unsigned char *mem_14 ;
  boolean __retres15 ;

  {
  __CrestCall(42785, 309);
# 5134 "regex.c"
  p1 = *p + 2;
  {
# 5136 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(42788, (unsigned long )(& p1), (long long )((unsigned long )p1));
    __CrestLoad(42787, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestApply2(42786, 16, (long long )((unsigned long )p1 < (unsigned long )end));
# 5136 "regex.c"
    if ((unsigned long )p1 < (unsigned long )end) {
      __CrestBranch(42789, 12063, 1);

    } else {
      __CrestBranch(42790, 12064, 0);
# 5136 "regex.c"
      goto while_break;
    }
    }
    {
    {
    __CrestLoad(42793, (unsigned long )p1, (long long )*p1);
    __CrestLoad(42792, (unsigned long )0, (long long )15U);
    __CrestApply2(42791, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 15U));
# 5145 "regex.c"
    if ((unsigned int )((re_opcode_t )*p1) == 15U) {
      __CrestBranch(42794, 12067, 1);
# 5145 "regex.c"
      goto case_15;
    } else {
      __CrestBranch(42795, 12068, 0);

    }
    }
    {
    __CrestLoad(42798, (unsigned long )p1, (long long )*p1);
    __CrestLoad(42797, (unsigned long )0, (long long )7U);
    __CrestApply2(42796, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 7U));
# 5216 "regex.c"
    if ((unsigned int )((re_opcode_t )*p1) == 7U) {
      __CrestBranch(42799, 12070, 1);
# 5216 "regex.c"
      goto case_7;
    } else {
      __CrestBranch(42800, 12071, 0);

    }
    }
# 5222 "regex.c"
    goto switch_default;
    case_15:
# 5146 "regex.c"
    p1 ++;
    {
# 5147 "regex.c"
    while (1) {
      while_continue___0: ;
      {
# 5147 "regex.c"
      while (1) {
        while_continue___1: ;
        __CrestLoad(42803, (unsigned long )p1, (long long )*p1);
        __CrestLoad(42802, (unsigned long )0, (long long )255);
        __CrestApply2(42801, 5, (long long )((int )*p1 & 255));
        __CrestStore(42804, (unsigned long )(& mcnt));
# 5147 "regex.c"
        mcnt = (int )*p1 & 255;
# 5147 "regex.c"
        mem_9 = p1 + 1;
        __CrestLoad(42809, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42808, (unsigned long )mem_9, (long long )*mem_9);
        __CrestLoad(42807, (unsigned long )0, (long long )8);
        __CrestApply2(42806, 8, (long long )((int )((signed char )*mem_9) << 8));
        __CrestApply2(42805, 0, (long long )(mcnt + ((int )((signed char )*mem_9) << 8)));
        __CrestStore(42810, (unsigned long )(& mcnt));
# 5147 "regex.c"
        mcnt += (int )((signed char )*mem_9) << 8;
# 5147 "regex.c"
        goto while_break___1;
      }
      while_break___1: ;
      }
# 5147 "regex.c"
      p1 += 2;
# 5147 "regex.c"
      goto while_break___0;
    }
    while_break___0: ;
    }
    {
    __CrestLoad(42813, (unsigned long )(& mcnt), (long long )mcnt);
    __CrestLoad(42812, (unsigned long )0, (long long )0);
    __CrestApply2(42811, 17, (long long )(mcnt >= 0));
# 5152 "regex.c"
    if (mcnt >= 0) {
      __CrestBranch(42814, 12087, 1);
      {
# 5172 "regex.c"
      while (1) {
        while_continue___2: ;
        {
# 5172 "regex.c"
        mem_10 = p1 + (mcnt - 3);
        {
        __CrestLoad(42818, (unsigned long )mem_10, (long long )*mem_10);
        __CrestLoad(42817, (unsigned long )0, (long long )14U);
        __CrestApply2(42816, 12, (long long )((unsigned int )((re_opcode_t )*mem_10) == 14U));
# 5172 "regex.c"
        if ((unsigned int )((re_opcode_t )*mem_10) == 14U) {
          __CrestBranch(42819, 12093, 1);

        } else {
          __CrestBranch(42820, 12094, 0);
# 5172 "regex.c"
          goto while_break___2;
        }
        }
        }
# 5178 "regex.c"
        tmp = alt_match_null_string_p(p1, (p1 + mcnt) - 3, reg_info);
        __CrestHandleReturn(42822, (long long )tmp);
        __CrestStore(42821, (unsigned long )(& tmp));
        {
        __CrestLoad(42825, (unsigned long )(& tmp), (long long )tmp);
        __CrestLoad(42824, (unsigned long )0, (long long )0);
        __CrestApply2(42823, 13, (long long )(tmp != 0));
# 5178 "regex.c"
        if (tmp != 0) {
          __CrestBranch(42826, 12097, 1);

        } else {
          __CrestBranch(42827, 12098, 0);
          __CrestLoad(42828, (unsigned long )0, (long long )((boolean )0));
          __CrestStore(42829, (unsigned long )(& __retres15));
# 5180 "regex.c"
          __retres15 = (boolean )0;
# 5180 "regex.c"
          goto return_label;
        }
        }
# 5184 "regex.c"
        p1 += mcnt;
        {
        __CrestLoad(42832, (unsigned long )p1, (long long )*p1);
        __CrestLoad(42831, (unsigned long )0, (long long )15U);
        __CrestApply2(42830, 13, (long long )((unsigned int )((re_opcode_t )*p1) != 15U));
# 5188 "regex.c"
        if ((unsigned int )((re_opcode_t )*p1) != 15U) {
          __CrestBranch(42833, 12102, 1);
# 5189 "regex.c"
          goto while_break___2;
        } else {
          __CrestBranch(42834, 12103, 0);

        }
        }
# 5193 "regex.c"
        p1 ++;
        {
# 5194 "regex.c"
        while (1) {
          while_continue___3: ;
          {
# 5194 "regex.c"
          while (1) {
            while_continue___4: ;
            __CrestLoad(42837, (unsigned long )p1, (long long )*p1);
            __CrestLoad(42836, (unsigned long )0, (long long )255);
            __CrestApply2(42835, 5, (long long )((int )*p1 & 255));
            __CrestStore(42838, (unsigned long )(& mcnt));
# 5194 "regex.c"
            mcnt = (int )*p1 & 255;
# 5194 "regex.c"
            mem_11 = p1 + 1;
            __CrestLoad(42843, (unsigned long )(& mcnt), (long long )mcnt);
            __CrestLoad(42842, (unsigned long )mem_11, (long long )*mem_11);
            __CrestLoad(42841, (unsigned long )0, (long long )8);
            __CrestApply2(42840, 8, (long long )((int )((signed char )*mem_11) << 8));
            __CrestApply2(42839, 0, (long long )(mcnt + ((int )((signed char )*mem_11) << 8)));
            __CrestStore(42844, (unsigned long )(& mcnt));
# 5194 "regex.c"
            mcnt += (int )((signed char )*mem_11) << 8;
# 5194 "regex.c"
            goto while_break___4;
          }
          while_break___4: ;
          }
# 5194 "regex.c"
          p1 += 2;
# 5194 "regex.c"
          goto while_break___3;
        }
        while_break___3: ;
        }
        {
# 5195 "regex.c"
        mem_12 = p1 + (mcnt - 3);
        {
        __CrestLoad(42847, (unsigned long )mem_12, (long long )*mem_12);
        __CrestLoad(42846, (unsigned long )0, (long long )14U);
        __CrestApply2(42845, 13, (long long )((unsigned int )((re_opcode_t )*mem_12) != 14U));
# 5195 "regex.c"
        if ((unsigned int )((re_opcode_t )*mem_12) != 14U) {
          __CrestBranch(42848, 12120, 1);
# 5198 "regex.c"
          p1 -= 3;
# 5199 "regex.c"
          goto while_break___2;
        } else {
          __CrestBranch(42849, 12122, 0);

        }
        }
        }
      }
      while_break___2: ;
      }
      {
# 5206 "regex.c"
      while (1) {
        while_continue___5: ;
# 5206 "regex.c"
        mem_13 = p1 - 2;
        __CrestLoad(42852, (unsigned long )mem_13, (long long )*mem_13);
        __CrestLoad(42851, (unsigned long )0, (long long )255);
        __CrestApply2(42850, 5, (long long )((int )*mem_13 & 255));
        __CrestStore(42853, (unsigned long )(& mcnt));
# 5206 "regex.c"
        mcnt = (int )*mem_13 & 255;
# 5206 "regex.c"
        mem_14 = (p1 - 2) + 1;
        __CrestLoad(42858, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42857, (unsigned long )mem_14, (long long )*mem_14);
        __CrestLoad(42856, (unsigned long )0, (long long )8);
        __CrestApply2(42855, 8, (long long )((int )((signed char )*mem_14) << 8));
        __CrestApply2(42854, 0, (long long )(mcnt + ((int )((signed char )*mem_14) << 8)));
        __CrestStore(42859, (unsigned long )(& mcnt));
# 5206 "regex.c"
        mcnt += (int )((signed char )*mem_14) << 8;
# 5206 "regex.c"
        goto while_break___5;
      }
      while_break___5: ;
      }
# 5208 "regex.c"
      tmp___0 = alt_match_null_string_p(p1, p1 + mcnt, reg_info);
      __CrestHandleReturn(42861, (long long )tmp___0);
      __CrestStore(42860, (unsigned long )(& tmp___0));
      {
      __CrestLoad(42864, (unsigned long )(& tmp___0), (long long )tmp___0);
      __CrestLoad(42863, (unsigned long )0, (long long )0);
      __CrestApply2(42862, 13, (long long )(tmp___0 != 0));
# 5208 "regex.c"
      if (tmp___0 != 0) {
        __CrestBranch(42865, 12132, 1);

      } else {
        __CrestBranch(42866, 12133, 0);
        __CrestLoad(42867, (unsigned long )0, (long long )((boolean )0));
        __CrestStore(42868, (unsigned long )(& __retres15));
# 5209 "regex.c"
        __retres15 = (boolean )0;
# 5209 "regex.c"
        goto return_label;
      }
      }
# 5211 "regex.c"
      p1 += mcnt;
    } else {
      __CrestBranch(42815, 12136, 0);

    }
    }
# 5213 "regex.c"
    goto switch_break;
    case_7:
# 5218 "regex.c"
    *p = p1 + 2;
    __CrestLoad(42869, (unsigned long )0, (long long )((boolean )1));
    __CrestStore(42870, (unsigned long )(& __retres15));
# 5219 "regex.c"
    __retres15 = (boolean )1;
# 5219 "regex.c"
    goto return_label;
    switch_default:
# 5223 "regex.c"
    tmp___1 = common_op_match_null_string_p(& p1, end, reg_info);
    __CrestHandleReturn(42872, (long long )tmp___1);
    __CrestStore(42871, (unsigned long )(& tmp___1));
    {
    __CrestLoad(42875, (unsigned long )(& tmp___1), (long long )tmp___1);
    __CrestLoad(42874, (unsigned long )0, (long long )0);
    __CrestApply2(42873, 13, (long long )(tmp___1 != 0));
# 5223 "regex.c"
    if (tmp___1 != 0) {
      __CrestBranch(42876, 12143, 1);

    } else {
      __CrestBranch(42877, 12144, 0);
      __CrestLoad(42878, (unsigned long )0, (long long )((boolean )0));
      __CrestStore(42879, (unsigned long )(& __retres15));
# 5224 "regex.c"
      __retres15 = (boolean )0;
# 5224 "regex.c"
      goto return_label;
    }
    }
    switch_break: ;
    }
  }
  while_break: ;
  }
  __CrestLoad(42880, (unsigned long )0, (long long )((boolean )0));
  __CrestStore(42881, (unsigned long )(& __retres15));
# 5228 "regex.c"
  __retres15 = (boolean )0;
  return_label:
  {
  __CrestLoad(42882, (unsigned long )(& __retres15), (long long )__retres15);
  __CrestReturn(42883);
# 5127 "regex.c"
  return (__retres15);
  }
}
}
# 5236 "regex.c"
static boolean alt_match_null_string_p(unsigned char *p , unsigned char *end , register_info_type *reg_info )
{
  int mcnt ;
  unsigned char *p1 ;
  boolean tmp ;
  unsigned char *mem_7 ;
  boolean __retres8 ;

  {
  __CrestCall(42884, 310);
# 5242 "regex.c"
  p1 = p;
  {
# 5244 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(42887, (unsigned long )(& p1), (long long )((unsigned long )p1));
    __CrestLoad(42886, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestApply2(42885, 16, (long long )((unsigned long )p1 < (unsigned long )end));
# 5244 "regex.c"
    if ((unsigned long )p1 < (unsigned long )end) {
      __CrestBranch(42888, 12155, 1);

    } else {
      __CrestBranch(42889, 12156, 0);
# 5244 "regex.c"
      goto while_break;
    }
    }
    {
    {
    __CrestLoad(42892, (unsigned long )p1, (long long )*p1);
    __CrestLoad(42891, (unsigned long )0, (long long )15U);
    __CrestApply2(42890, 12, (long long )((unsigned int )((re_opcode_t )*p1) == 15U));
# 5252 "regex.c"
    if ((unsigned int )((re_opcode_t )*p1) == 15U) {
      __CrestBranch(42893, 12159, 1);
# 5252 "regex.c"
      goto case_15;
    } else {
      __CrestBranch(42894, 12160, 0);

    }
    }
# 5258 "regex.c"
    goto switch_default;
    case_15:
# 5253 "regex.c"
    p1 ++;
    {
# 5254 "regex.c"
    while (1) {
      while_continue___0: ;
      {
# 5254 "regex.c"
      while (1) {
        while_continue___1: ;
        __CrestLoad(42897, (unsigned long )p1, (long long )*p1);
        __CrestLoad(42896, (unsigned long )0, (long long )255);
        __CrestApply2(42895, 5, (long long )((int )*p1 & 255));
        __CrestStore(42898, (unsigned long )(& mcnt));
# 5254 "regex.c"
        mcnt = (int )*p1 & 255;
# 5254 "regex.c"
        mem_7 = p1 + 1;
        __CrestLoad(42903, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(42902, (unsigned long )mem_7, (long long )*mem_7);
        __CrestLoad(42901, (unsigned long )0, (long long )8);
        __CrestApply2(42900, 8, (long long )((int )((signed char )*mem_7) << 8));
        __CrestApply2(42899, 0, (long long )(mcnt + ((int )((signed char )*mem_7) << 8)));
        __CrestStore(42904, (unsigned long )(& mcnt));
# 5254 "regex.c"
        mcnt += (int )((signed char )*mem_7) << 8;
# 5254 "regex.c"
        goto while_break___1;
      }
      while_break___1: ;
      }
# 5254 "regex.c"
      p1 += 2;
# 5254 "regex.c"
      goto while_break___0;
    }
    while_break___0: ;
    }
# 5255 "regex.c"
    p1 += mcnt;
# 5256 "regex.c"
    goto switch_break;
    switch_default:
# 5259 "regex.c"
    tmp = common_op_match_null_string_p(& p1, end, reg_info);
    __CrestHandleReturn(42906, (long long )tmp);
    __CrestStore(42905, (unsigned long )(& tmp));
    {
    __CrestLoad(42909, (unsigned long )(& tmp), (long long )tmp);
    __CrestLoad(42908, (unsigned long )0, (long long )0);
    __CrestApply2(42907, 13, (long long )(tmp != 0));
# 5259 "regex.c"
    if (tmp != 0) {
      __CrestBranch(42910, 12179, 1);

    } else {
      __CrestBranch(42911, 12180, 0);
      __CrestLoad(42912, (unsigned long )0, (long long )((boolean )0));
      __CrestStore(42913, (unsigned long )(& __retres8));
# 5260 "regex.c"
      __retres8 = (boolean )0;
# 5260 "regex.c"
      goto return_label;
    }
    }
    switch_break: ;
    }
  }
  while_break: ;
  }
  __CrestLoad(42914, (unsigned long )0, (long long )((boolean )1));
  __CrestStore(42915, (unsigned long )(& __retres8));
# 5264 "regex.c"
  __retres8 = (boolean )1;
  return_label:
  {
  __CrestLoad(42916, (unsigned long )(& __retres8), (long long )__retres8);
  __CrestReturn(42917);
# 5236 "regex.c"
  return (__retres8);
  }
}
}
# 5273 "regex.c"
static boolean common_op_match_null_string_p(unsigned char **p , unsigned char *end ,
                                             register_info_type *reg_info )
{
  int mcnt ;
  boolean ret ;
  int reg_no ;
  unsigned char *p1 ;
  unsigned char *tmp ;
  register_info_type *mem_9 ;
  register_info_type *mem_10 ;
  unsigned char *mem_11 ;
  unsigned char *mem_12 ;
  unsigned char *mem_13 ;
  register_info_type *mem_14 ;
  boolean __retres15 ;

  {
  __CrestCall(42918, 311);
# 5281 "regex.c"
  p1 = *p;
# 5283 "regex.c"
  tmp = p1;
# 5283 "regex.c"
  p1 ++;
  {
  {
  __CrestLoad(42921, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42920, (unsigned long )0, (long long )29U);
  __CrestApply2(42919, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 29U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 29U) {
    __CrestBranch(42922, 12189, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42923, 12190, 0);

  }
  }
  {
  __CrestLoad(42926, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42925, (unsigned long )0, (long long )28U);
  __CrestApply2(42924, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 28U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 28U) {
    __CrestBranch(42927, 12192, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42928, 12193, 0);

  }
  }
  {
  __CrestLoad(42931, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42930, (unsigned long )0, (long long )27U);
  __CrestApply2(42929, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 27U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 27U) {
    __CrestBranch(42932, 12195, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42933, 12196, 0);

  }
  }
  {
  __CrestLoad(42936, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42935, (unsigned long )0, (long long )26U);
  __CrestApply2(42934, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 26U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 26U) {
    __CrestBranch(42937, 12198, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42938, 12199, 0);

  }
  }
  {
  __CrestLoad(42941, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42940, (unsigned long )0, (long long )12U);
  __CrestApply2(42939, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 12U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 12U) {
    __CrestBranch(42942, 12201, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42943, 12202, 0);

  }
  }
  {
  __CrestLoad(42946, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42945, (unsigned long )0, (long long )11U);
  __CrestApply2(42944, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 11U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 11U) {
    __CrestBranch(42947, 12204, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42948, 12205, 0);

  }
  }
  {
  __CrestLoad(42951, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42950, (unsigned long )0, (long long )10U);
  __CrestApply2(42949, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 10U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 10U) {
    __CrestBranch(42952, 12207, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42953, 12208, 0);

  }
  }
  {
  __CrestLoad(42956, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42955, (unsigned long )0, (long long )9U);
  __CrestApply2(42954, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 9U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 9U) {
    __CrestBranch(42957, 12210, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42958, 12211, 0);

  }
  }
  {
  __CrestLoad(42961, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42960, (unsigned long )0, (long long )0U);
  __CrestApply2(42959, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 0U));
# 5293 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 0U) {
    __CrestBranch(42962, 12213, 1);
# 5293 "regex.c"
    goto case_29;
  } else {
    __CrestBranch(42963, 12214, 0);

  }
  }
  {
  __CrestLoad(42966, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42965, (unsigned long )0, (long long )6U);
  __CrestApply2(42964, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 6U));
# 5301 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 6U) {
    __CrestBranch(42967, 12216, 1);
# 5301 "regex.c"
    goto case_6;
  } else {
    __CrestBranch(42968, 12217, 0);

  }
  }
  {
  __CrestLoad(42971, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42970, (unsigned long )0, (long long )13U);
  __CrestApply2(42969, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 13U));
# 5317 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 13U) {
    __CrestBranch(42972, 12219, 1);
# 5317 "regex.c"
    goto case_13;
  } else {
    __CrestBranch(42973, 12220, 0);

  }
  }
  {
  __CrestLoad(42976, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42975, (unsigned long )0, (long long )21U);
  __CrestApply2(42974, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 21U));
# 5325 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 21U) {
    __CrestBranch(42977, 12222, 1);
# 5325 "regex.c"
    goto case_21;
  } else {
    __CrestBranch(42978, 12223, 0);

  }
  }
  {
  __CrestLoad(42981, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42980, (unsigned long )0, (long long )8U);
  __CrestApply2(42979, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 8U));
# 5340 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 8U) {
    __CrestBranch(42982, 12225, 1);
# 5340 "regex.c"
    goto case_8;
  } else {
    __CrestBranch(42983, 12226, 0);

  }
  }
  {
  __CrestLoad(42986, (unsigned long )tmp, (long long )*tmp);
  __CrestLoad(42985, (unsigned long )0, (long long )23U);
  __CrestApply2(42984, 12, (long long )((unsigned int )((re_opcode_t )*tmp) == 23U));
# 5345 "regex.c"
  if ((unsigned int )((re_opcode_t )*tmp) == 23U) {
    __CrestBranch(42987, 12228, 1);
# 5345 "regex.c"
    goto case_23;
  } else {
    __CrestBranch(42988, 12229, 0);

  }
  }
# 5348 "regex.c"
  goto switch_default;
  case_29:
  case_28:
  case_27:
  case_26:
  case_12:
  case_11:
  case_10:
  case_9:
  case_0:
# 5299 "regex.c"
  goto switch_break;
  case_6:
  __CrestLoad(42989, (unsigned long )p1, (long long )*p1);
  __CrestStore(42990, (unsigned long )(& reg_no));
# 5302 "regex.c"
  reg_no = (int )*p1;
# 5304 "regex.c"
  ret = group_match_null_string_p(& p1, end, reg_info);
  __CrestHandleReturn(42992, (long long )ret);
  __CrestStore(42991, (unsigned long )(& ret));
  {
# 5309 "regex.c"
  mem_9 = reg_info + reg_no;
  {
  __CrestLoad(42995, (unsigned long )0, (long long )mem_9->bits.match_null_string_p);
  __CrestLoad(42994, (unsigned long )0, (long long )3U);
  __CrestApply2(42993, 12, (long long )(mem_9->bits.match_null_string_p == 3U));
# 5309 "regex.c"
  if (mem_9->bits.match_null_string_p == 3U) {
    __CrestBranch(42996, 12236, 1);
# 5310 "regex.c"
    mem_10 = reg_info + reg_no;
# 5310 "regex.c"
    mem_10->bits.match_null_string_p = (unsigned int )ret;
  } else {
    __CrestBranch(42997, 12237, 0);

  }
  }
  }
  {
  __CrestLoad(43000, (unsigned long )(& ret), (long long )ret);
  __CrestLoad(42999, (unsigned long )0, (long long )0);
  __CrestApply2(42998, 12, (long long )(ret == 0));
# 5312 "regex.c"
  if (ret == 0) {
    __CrestBranch(43001, 12239, 1);
    __CrestLoad(43003, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(43004, (unsigned long )(& __retres15));
# 5313 "regex.c"
    __retres15 = (boolean )0;
# 5313 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(43002, 12241, 0);

  }
  }
# 5314 "regex.c"
  goto switch_break;
  case_13:
  {
# 5318 "regex.c"
  while (1) {
    while_continue: ;
    {
# 5318 "regex.c"
    while (1) {
      while_continue___0: ;
      __CrestLoad(43007, (unsigned long )p1, (long long )*p1);
      __CrestLoad(43006, (unsigned long )0, (long long )255);
      __CrestApply2(43005, 5, (long long )((int )*p1 & 255));
      __CrestStore(43008, (unsigned long )(& mcnt));
# 5318 "regex.c"
      mcnt = (int )*p1 & 255;
# 5318 "regex.c"
      mem_11 = p1 + 1;
      __CrestLoad(43013, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(43012, (unsigned long )mem_11, (long long )*mem_11);
      __CrestLoad(43011, (unsigned long )0, (long long )8);
      __CrestApply2(43010, 8, (long long )((int )((signed char )*mem_11) << 8));
      __CrestApply2(43009, 0, (long long )(mcnt + ((int )((signed char )*mem_11) << 8)));
      __CrestStore(43014, (unsigned long )(& mcnt));
# 5318 "regex.c"
      mcnt += (int )((signed char )*mem_11) << 8;
# 5318 "regex.c"
      goto while_break___0;
    }
    while_break___0: ;
    }
# 5318 "regex.c"
    p1 += 2;
# 5318 "regex.c"
    goto while_break;
  }
  while_break: ;
  }
  {
  __CrestLoad(43017, (unsigned long )(& mcnt), (long long )mcnt);
  __CrestLoad(43016, (unsigned long )0, (long long )0);
  __CrestApply2(43015, 17, (long long )(mcnt >= 0));
# 5319 "regex.c"
  if (mcnt >= 0) {
    __CrestBranch(43018, 12256, 1);
# 5320 "regex.c"
    p1 += mcnt;
  } else {
    __CrestBranch(43019, 12257, 0);
    __CrestLoad(43020, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(43021, (unsigned long )(& __retres15));
# 5322 "regex.c"
    __retres15 = (boolean )0;
# 5322 "regex.c"
    goto return_label;
  }
  }
# 5323 "regex.c"
  goto switch_break;
  case_21:
# 5327 "regex.c"
  p1 += 2;
  {
# 5328 "regex.c"
  while (1) {
    while_continue___1: ;
    {
# 5328 "regex.c"
    while (1) {
      while_continue___2: ;
      __CrestLoad(43024, (unsigned long )p1, (long long )*p1);
      __CrestLoad(43023, (unsigned long )0, (long long )255);
      __CrestApply2(43022, 5, (long long )((int )*p1 & 255));
      __CrestStore(43025, (unsigned long )(& mcnt));
# 5328 "regex.c"
      mcnt = (int )*p1 & 255;
# 5328 "regex.c"
      mem_12 = p1 + 1;
      __CrestLoad(43030, (unsigned long )(& mcnt), (long long )mcnt);
      __CrestLoad(43029, (unsigned long )mem_12, (long long )*mem_12);
      __CrestLoad(43028, (unsigned long )0, (long long )8);
      __CrestApply2(43027, 8, (long long )((int )((signed char )*mem_12) << 8));
      __CrestApply2(43026, 0, (long long )(mcnt + ((int )((signed char )*mem_12) << 8)));
      __CrestStore(43031, (unsigned long )(& mcnt));
# 5328 "regex.c"
      mcnt += (int )((signed char )*mem_12) << 8;
# 5328 "regex.c"
      goto while_break___2;
    }
    while_break___2: ;
    }
# 5328 "regex.c"
    p1 += 2;
# 5328 "regex.c"
    goto while_break___1;
  }
  while_break___1: ;
  }
  {
  __CrestLoad(43034, (unsigned long )(& mcnt), (long long )mcnt);
  __CrestLoad(43033, (unsigned long )0, (long long )0);
  __CrestApply2(43032, 12, (long long )(mcnt == 0));
# 5330 "regex.c"
  if (mcnt == 0) {
    __CrestBranch(43035, 12274, 1);
# 5332 "regex.c"
    p1 -= 4;
    {
# 5333 "regex.c"
    while (1) {
      while_continue___3: ;
      {
# 5333 "regex.c"
      while (1) {
        while_continue___4: ;
        __CrestLoad(43039, (unsigned long )p1, (long long )*p1);
        __CrestLoad(43038, (unsigned long )0, (long long )255);
        __CrestApply2(43037, 5, (long long )((int )*p1 & 255));
        __CrestStore(43040, (unsigned long )(& mcnt));
# 5333 "regex.c"
        mcnt = (int )*p1 & 255;
# 5333 "regex.c"
        mem_13 = p1 + 1;
        __CrestLoad(43045, (unsigned long )(& mcnt), (long long )mcnt);
        __CrestLoad(43044, (unsigned long )mem_13, (long long )*mem_13);
        __CrestLoad(43043, (unsigned long )0, (long long )8);
        __CrestApply2(43042, 8, (long long )((int )((signed char )*mem_13) << 8));
        __CrestApply2(43041, 0, (long long )(mcnt + ((int )((signed char )*mem_13) << 8)));
        __CrestStore(43046, (unsigned long )(& mcnt));
# 5333 "regex.c"
        mcnt += (int )((signed char )*mem_13) << 8;
# 5333 "regex.c"
        goto while_break___4;
      }
      while_break___4: ;
      }
# 5333 "regex.c"
      p1 += 2;
# 5333 "regex.c"
      goto while_break___3;
    }
    while_break___3: ;
    }
# 5334 "regex.c"
    p1 += mcnt;
  } else {
    __CrestBranch(43036, 12288, 0);
    __CrestLoad(43047, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(43048, (unsigned long )(& __retres15));
# 5337 "regex.c"
    __retres15 = (boolean )0;
# 5337 "regex.c"
    goto return_label;
  }
  }
# 5338 "regex.c"
  goto switch_break;
  case_8:
  {
# 5341 "regex.c"
  mem_14 = reg_info + *p1;
  {
  __CrestLoad(43051, (unsigned long )0, (long long )mem_14->bits.match_null_string_p);
  __CrestLoad(43050, (unsigned long )0, (long long )0);
  __CrestApply2(43049, 12, (long long )(mem_14->bits.match_null_string_p == 0));
# 5341 "regex.c"
  if (mem_14->bits.match_null_string_p == 0) {
    __CrestBranch(43052, 12294, 1);
    __CrestLoad(43054, (unsigned long )0, (long long )((boolean )0));
    __CrestStore(43055, (unsigned long )(& __retres15));
# 5342 "regex.c"
    __retres15 = (boolean )0;
# 5342 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(43053, 12296, 0);

  }
  }
  }
# 5343 "regex.c"
  goto switch_break;
  case_23:
# 5346 "regex.c"
  p1 += 4;
  switch_default:
  __CrestLoad(43056, (unsigned long )0, (long long )((boolean )0));
  __CrestStore(43057, (unsigned long )(& __retres15));
# 5350 "regex.c"
  __retres15 = (boolean )0;
# 5350 "regex.c"
  goto return_label;
  switch_break: ;
  }
# 5353 "regex.c"
  *p = p1;
  __CrestLoad(43058, (unsigned long )0, (long long )((boolean )1));
  __CrestStore(43059, (unsigned long )(& __retres15));
# 5354 "regex.c"
  __retres15 = (boolean )1;
  return_label:
  {
  __CrestLoad(43060, (unsigned long )(& __retres15), (long long )__retres15);
  __CrestReturn(43061);
# 5273 "regex.c"
  return (__retres15);
  }
}
}
# 5361 "regex.c"
static int bcmp_translate(char const *s1 , char const *s2 , int len , char *translate )
{
  register unsigned char const *p1 ;
  register unsigned char const *p2 ;
  unsigned char const *tmp ;
  unsigned char const *tmp___0 ;
  char *mem_9 ;
  char *mem_10 ;
  int __retres11 ;

  {
  __CrestCall(43063, 312);
  __CrestStore(43062, (unsigned long )(& len));
# 5367 "regex.c"
  p1 = (unsigned char const *)s1;
# 5368 "regex.c"
  p2 = (unsigned char const *)s2;
  {
# 5369 "regex.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(43066, (unsigned long )(& len), (long long )len);
    __CrestLoad(43065, (unsigned long )0, (long long )0);
    __CrestApply2(43064, 13, (long long )(len != 0));
# 5369 "regex.c"
    if (len != 0) {
      __CrestBranch(43067, 12310, 1);

    } else {
      __CrestBranch(43068, 12311, 0);
# 5369 "regex.c"
      goto while_break;
    }
    }
# 5371 "regex.c"
    tmp = p1;
# 5371 "regex.c"
    p1 ++;
# 5371 "regex.c"
    tmp___0 = p2;
# 5371 "regex.c"
    p2 ++;
    {
# 5371 "regex.c"
    mem_9 = translate + *tmp;
# 5371 "regex.c"
    mem_10 = translate + *tmp___0;
    {
    __CrestLoad(43071, (unsigned long )mem_9, (long long )*mem_9);
    __CrestLoad(43070, (unsigned long )mem_10, (long long )*mem_10);
    __CrestApply2(43069, 13, (long long )((int )*mem_9 != (int )*mem_10));
# 5371 "regex.c"
    if ((int )*mem_9 != (int )*mem_10) {
      __CrestBranch(43072, 12316, 1);
      __CrestLoad(43074, (unsigned long )0, (long long )1);
      __CrestStore(43075, (unsigned long )(& __retres11));
# 5371 "regex.c"
      __retres11 = 1;
# 5371 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(43073, 12318, 0);

    }
    }
    }
    __CrestLoad(43078, (unsigned long )(& len), (long long )len);
    __CrestLoad(43077, (unsigned long )0, (long long )1);
    __CrestApply2(43076, 1, (long long )(len - 1));
    __CrestStore(43079, (unsigned long )(& len));
# 5372 "regex.c"
    len --;
  }
  while_break: ;
  }
  __CrestLoad(43080, (unsigned long )0, (long long )0);
  __CrestStore(43081, (unsigned long )(& __retres11));
# 5374 "regex.c"
  __retres11 = 0;
  return_label:
  {
  __CrestLoad(43082, (unsigned long )(& __retres11), (long long )__retres11);
  __CrestReturn(43083);
# 5361 "regex.c"
  return (__retres11);
  }
}
}
# 5388 "regex.c"
char const *re_compile_pattern(char const *pattern , size_t length , struct re_pattern_buffer *bufp )
{
  reg_errcode_t ret ;
  char *tmp ;
  char const *__retres6 ;

  {
  __CrestCall(43085, 313);
  __CrestStore(43084, (unsigned long )(& length));
# 5398 "regex.c"
  bufp->regs_allocated = 0U;
# 5403 "regex.c"
  bufp->no_sub = 0U;
# 5406 "regex.c"
  bufp->newline_anchor = 1U;
  __CrestLoad(43086, (unsigned long )(& length), (long long )length);
  __CrestLoad(43087, (unsigned long )(& re_syntax_options), (long long )re_syntax_options);
# 5408 "regex.c"
  ret = regex_compile(pattern, length, re_syntax_options, bufp);
  __CrestHandleReturn(43089, (long long )ret);
  __CrestStore(43088, (unsigned long )(& ret));
  {
  __CrestLoad(43092, (unsigned long )(& ret), (long long )ret);
  __CrestLoad(43091, (unsigned long )0, (long long )0);
  __CrestApply2(43090, 12, (long long )(ret == 0));
# 5410 "regex.c"
  if (ret == 0) {
    __CrestBranch(43093, 12325, 1);
# 5411 "regex.c"
    __retres6 = (char const *)((void *)0);
# 5411 "regex.c"
    goto return_label;
  } else {
    __CrestBranch(43094, 12327, 0);

  }
  }
  __CrestLoad(43095, (unsigned long )0, (long long )5);
# 5412 "regex.c"
  tmp = dcgettext((char const *)((void *)0), re_error_msgid[(int )ret], 5);
  __CrestClearStack(43096);
# 5412 "regex.c"
  __retres6 = (char const *)tmp;
  return_label:
  {
  __CrestReturn(43097);
# 5388 "regex.c"
  return (__retres6);
  }
}
}
# 5522 "regex.c"
int regcomp(regex_t *preg , char const *pattern , int cflags )
{
  reg_errcode_t ret ;
  reg_syntax_t syntax ;
  unsigned long tmp ;
  unsigned int i ;
  void *tmp___0 ;
  int tmp___2 ;
  unsigned short const **tmp___3 ;
  size_t tmp___4 ;
  char *mem_13 ;
  unsigned short const *mem_14 ;
  char *mem_15 ;
  char *mem_16 ;
  int __retres17 ;

  {
  __CrestCall(43099, 314);
  __CrestStore(43098, (unsigned long )(& cflags));
  {
  __CrestLoad(43104, (unsigned long )(& cflags), (long long )cflags);
  __CrestLoad(43103, (unsigned long )0, (long long )1);
  __CrestApply2(43102, 5, (long long )(cflags & 1));
  __CrestLoad(43101, (unsigned long )0, (long long )0);
  __CrestApply2(43100, 13, (long long )((cflags & 1) != 0));
# 5529 "regex.c"
  if ((cflags & 1) != 0) {
    __CrestBranch(43105, 12332, 1);
    __CrestLoad(43107, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestStore(43108, (unsigned long )(& tmp));
# 5529 "regex.c"
    tmp = (((((((((((1UL << 1) << 1) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1);
  } else {
    __CrestBranch(43106, 12333, 0);
    __CrestLoad(43109, (unsigned long )0, (long long )(((((((1UL << 1) << 1) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (1UL << 1)));
    __CrestStore(43110, (unsigned long )(& tmp));
# 5529 "regex.c"
    tmp = ((((((1UL << 1) << 1) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (1UL << 1);
  }
  }
  __CrestLoad(43111, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(43112, (unsigned long )(& syntax));
# 5529 "regex.c"
  syntax = tmp;
# 5534 "regex.c"
  preg->buffer = (unsigned char *)0;
  __CrestLoad(43113, (unsigned long )0, (long long )0UL);
  __CrestStore(43114, (unsigned long )(& preg->allocated));
# 5535 "regex.c"
  preg->allocated = 0UL;
  __CrestLoad(43115, (unsigned long )0, (long long )0UL);
  __CrestStore(43116, (unsigned long )(& preg->used));
# 5536 "regex.c"
  preg->used = 0UL;
# 5542 "regex.c"
  preg->fastmap = (char *)0;
  {
  __CrestLoad(43121, (unsigned long )(& cflags), (long long )cflags);
  __CrestLoad(43120, (unsigned long )0, (long long )(1 << 1));
  __CrestApply2(43119, 5, (long long )(cflags & (1 << 1)));
  __CrestLoad(43118, (unsigned long )0, (long long )0);
  __CrestApply2(43117, 13, (long long )((cflags & (1 << 1)) != 0));
# 5544 "regex.c"
  if ((cflags & (1 << 1)) != 0) {
    __CrestBranch(43122, 12336, 1);
# 5548 "regex.c"
    mem_13 = (char *)0;
    __CrestLoad(43124, (unsigned long )0, (long long )(256UL * sizeof(*mem_13)));
# 5548 "regex.c"
    tmp___0 = malloc(256UL * sizeof(*mem_13));
    __CrestClearStack(43125);
# 5548 "regex.c"
    preg->translate = (char *)tmp___0;
    {
    __CrestLoad(43128, (unsigned long )(& preg->translate), (long long )((unsigned long )preg->translate));
    __CrestLoad(43127, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(43126, 12, (long long )((unsigned long )preg->translate == (unsigned long )((void *)0)));
# 5551 "regex.c"
    if ((unsigned long )preg->translate == (unsigned long )((void *)0)) {
      __CrestBranch(43129, 12338, 1);
      __CrestLoad(43131, (unsigned long )0, (long long )12);
      __CrestStore(43132, (unsigned long )(& __retres17));
# 5552 "regex.c"
      __retres17 = 12;
# 5552 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(43130, 12340, 0);

    }
    }
    __CrestLoad(43133, (unsigned long )0, (long long )0U);
    __CrestStore(43134, (unsigned long )(& i));
# 5555 "regex.c"
    i = 0U;
    {
# 5555 "regex.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(43137, (unsigned long )(& i), (long long )i);
      __CrestLoad(43136, (unsigned long )0, (long long )256U);
      __CrestApply2(43135, 16, (long long )(i < 256U));
# 5555 "regex.c"
      if (i < 256U) {
        __CrestBranch(43138, 12346, 1);

      } else {
        __CrestBranch(43139, 12347, 0);
# 5555 "regex.c"
        goto while_break;
      }
      }
# 5556 "regex.c"
      tmp___3 = __ctype_b_loc();
      __CrestClearStack(43140);
      {
# 5556 "regex.c"
      mem_14 = *tmp___3 + (int )i;
      {
      __CrestLoad(43145, (unsigned long )mem_14, (long long )*mem_14);
      __CrestLoad(43144, (unsigned long )0, (long long )256);
      __CrestApply2(43143, 5, (long long )((int const )*mem_14 & 256));
      __CrestLoad(43142, (unsigned long )0, (long long )0);
      __CrestApply2(43141, 13, (long long )(((int const )*mem_14 & 256) != 0));
# 5556 "regex.c"
      if (((int const )*mem_14 & 256) != 0) {
        __CrestBranch(43146, 12352, 1);
        __CrestLoad(43148, (unsigned long )(& i), (long long )i);
# 5556 "regex.c"
        tmp___2 = tolower((int )i);
        __CrestHandleReturn(43150, (long long )tmp___2);
        __CrestStore(43149, (unsigned long )(& tmp___2));
# 5556 "regex.c"
        mem_15 = preg->translate + i;
        __CrestLoad(43151, (unsigned long )(& tmp___2), (long long )tmp___2);
        __CrestStore(43152, (unsigned long )mem_15);
# 5556 "regex.c"
        *mem_15 = (char )tmp___2;
      } else {
        __CrestBranch(43147, 12353, 0);
# 5556 "regex.c"
        mem_16 = preg->translate + i;
        __CrestLoad(43153, (unsigned long )(& i), (long long )i);
        __CrestStore(43154, (unsigned long )mem_16);
# 5556 "regex.c"
        *mem_16 = (char )i;
      }
      }
      }
      __CrestLoad(43157, (unsigned long )(& i), (long long )i);
      __CrestLoad(43156, (unsigned long )0, (long long )1U);
      __CrestApply2(43155, 0, (long long )(i + 1U));
      __CrestStore(43158, (unsigned long )(& i));
# 5555 "regex.c"
      i ++;
    }
    while_break: ;
    }
  } else {
    __CrestBranch(43123, 12356, 0);
# 5559 "regex.c"
    preg->translate = (char *)((void *)0);
  }
  }
  {
  __CrestLoad(43163, (unsigned long )(& cflags), (long long )cflags);
  __CrestLoad(43162, (unsigned long )0, (long long )((1 << 1) << 1));
  __CrestApply2(43161, 5, (long long )(cflags & ((1 << 1) << 1)));
  __CrestLoad(43160, (unsigned long )0, (long long )0);
  __CrestApply2(43159, 13, (long long )((cflags & ((1 << 1) << 1)) != 0));
# 5562 "regex.c"
  if ((cflags & ((1 << 1) << 1)) != 0) {
    __CrestBranch(43164, 12358, 1);
    __CrestLoad(43168, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(43167, (unsigned long )0, (long long )(~ ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestApply2(43166, 5, (long long )(syntax & ~ ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestStore(43169, (unsigned long )(& syntax));
# 5564 "regex.c"
    syntax &= ~ ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1);
    __CrestLoad(43172, (unsigned long )(& syntax), (long long )syntax);
    __CrestLoad(43171, (unsigned long )0, (long long )((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(43170, 6, (long long )(syntax | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestStore(43173, (unsigned long )(& syntax));
# 5565 "regex.c"
    syntax |= (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1;
# 5567 "regex.c"
    preg->newline_anchor = 1U;
  } else {
    __CrestBranch(43165, 12359, 0);
# 5570 "regex.c"
    preg->newline_anchor = 0U;
  }
  }
# 5572 "regex.c"
  preg->no_sub = (unsigned int )(! (! (cflags & (((1 << 1) << 1) << 1))));
# 5576 "regex.c"
  tmp___4 = strlen(pattern);
  __CrestHandleReturn(43175, (long long )tmp___4);
  __CrestStore(43174, (unsigned long )(& tmp___4));
  __CrestLoad(43176, (unsigned long )(& tmp___4), (long long )tmp___4);
  __CrestLoad(43177, (unsigned long )(& syntax), (long long )syntax);
# 5576 "regex.c"
  ret = regex_compile(pattern, tmp___4, syntax, preg);
  __CrestHandleReturn(43179, (long long )ret);
  __CrestStore(43178, (unsigned long )(& ret));
  {
  __CrestLoad(43182, (unsigned long )(& ret), (long long )ret);
  __CrestLoad(43181, (unsigned long )0, (long long )16U);
  __CrestApply2(43180, 12, (long long )((unsigned int )ret == 16U));
# 5580 "regex.c"
  if ((unsigned int )ret == 16U) {
    __CrestBranch(43183, 12362, 1);
    __CrestLoad(43185, (unsigned long )0, (long long )((reg_errcode_t )8));
    __CrestStore(43186, (unsigned long )(& ret));
# 5580 "regex.c"
    ret = (reg_errcode_t )8;
  } else {
    __CrestBranch(43184, 12363, 0);

  }
  }
  __CrestLoad(43187, (unsigned long )(& ret), (long long )ret);
  __CrestStore(43188, (unsigned long )(& __retres17));
# 5582 "regex.c"
  __retres17 = (int )ret;
  return_label:
  {
  __CrestLoad(43189, (unsigned long )(& __retres17), (long long )__retres17);
  __CrestReturn(43190);
# 5522 "regex.c"
  return (__retres17);
  }
}
}
# 5600 "regex.c"
int regexec(regex_t const *preg , char const *string , size_t nmatch , regmatch_t *pmatch ,
            int eflags )
{
  int ret ;
  struct re_registers regs ;
  regex_t private_preg ;
  int len ;
  size_t tmp ;
  boolean want_reg_info ;
  int tmp___0 ;
  void *tmp___1 ;
  void *tmp___2 ;
  struct re_registers *tmp___3 ;
  unsigned int r ;
  int tmp___4 ;
  regmatch_t *mem_18 ;
  regoff_t *mem_19 ;
  regmatch_t *mem_20 ;
  regoff_t *mem_21 ;
  int __retres22 ;

  {
  __CrestCall(43193, 315);
  __CrestStore(43192, (unsigned long )(& eflags));
  __CrestStore(43191, (unsigned long )(& nmatch));
# 5611 "regex.c"
  tmp = strlen(string);
  __CrestHandleReturn(43195, (long long )tmp);
  __CrestStore(43194, (unsigned long )(& tmp));
  __CrestLoad(43196, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(43197, (unsigned long )(& len));
# 5611 "regex.c"
  len = (int )tmp;
  {
  __CrestLoad(43200, (unsigned long )0, (long long )preg->no_sub);
  __CrestLoad(43199, (unsigned long )0, (long long )0);
  __CrestApply2(43198, 12, (long long )(preg->no_sub == 0));
# 5612 "regex.c"
  if (preg->no_sub == 0) {
    __CrestBranch(43201, 12368, 1);
    {
    __CrestLoad(43205, (unsigned long )(& nmatch), (long long )nmatch);
    __CrestLoad(43204, (unsigned long )0, (long long )0UL);
    __CrestApply2(43203, 14, (long long )(nmatch > 0UL));
# 5612 "regex.c"
    if (nmatch > 0UL) {
      __CrestBranch(43206, 12369, 1);
      __CrestLoad(43208, (unsigned long )0, (long long )1);
      __CrestStore(43209, (unsigned long )(& tmp___0));
# 5612 "regex.c"
      tmp___0 = 1;
    } else {
      __CrestBranch(43207, 12370, 0);
      __CrestLoad(43210, (unsigned long )0, (long long )0);
      __CrestStore(43211, (unsigned long )(& tmp___0));
# 5612 "regex.c"
      tmp___0 = 0;
    }
    }
  } else {
    __CrestBranch(43202, 12371, 0);
    __CrestLoad(43212, (unsigned long )0, (long long )0);
    __CrestStore(43213, (unsigned long )(& tmp___0));
# 5612 "regex.c"
    tmp___0 = 0;
  }
  }
  __CrestLoad(43214, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestStore(43215, (unsigned long )(& want_reg_info));
# 5612 "regex.c"
  want_reg_info = (boolean )tmp___0;
# 5614 "regex.c"
  private_preg = (regex_t )*preg;
# 5616 "regex.c"
  private_preg.not_bol = (unsigned int )(! (! (eflags & 1)));
# 5617 "regex.c"
  private_preg.not_eol = (unsigned int )(! (! (eflags & (1 << 1))));
# 5622 "regex.c"
  private_preg.regs_allocated = 2U;
  {
  __CrestLoad(43218, (unsigned long )(& want_reg_info), (long long )want_reg_info);
  __CrestLoad(43217, (unsigned long )0, (long long )0);
  __CrestApply2(43216, 13, (long long )(want_reg_info != 0));
# 5624 "regex.c"
  if (want_reg_info != 0) {
    __CrestBranch(43219, 12374, 1);
    __CrestLoad(43221, (unsigned long )(& nmatch), (long long )nmatch);
    __CrestStore(43222, (unsigned long )(& regs.num_regs));
# 5626 "regex.c"
    regs.num_regs = (unsigned int )nmatch;
    __CrestLoad(43225, (unsigned long )(& nmatch), (long long )nmatch);
    __CrestLoad(43224, (unsigned long )0, (long long )sizeof(regoff_t ));
    __CrestApply2(43223, 2, (long long )(nmatch * sizeof(regoff_t )));
# 5627 "regex.c"
    tmp___1 = malloc(nmatch * sizeof(regoff_t ));
    __CrestClearStack(43226);
# 5627 "regex.c"
    regs.start = (regoff_t *)tmp___1;
    __CrestLoad(43229, (unsigned long )(& nmatch), (long long )nmatch);
    __CrestLoad(43228, (unsigned long )0, (long long )sizeof(regoff_t ));
    __CrestApply2(43227, 2, (long long )(nmatch * sizeof(regoff_t )));
# 5628 "regex.c"
    tmp___2 = malloc(nmatch * sizeof(regoff_t ));
    __CrestClearStack(43230);
# 5628 "regex.c"
    regs.end = (regoff_t *)tmp___2;
    {
    __CrestLoad(43233, (unsigned long )(& regs.start), (long long )((unsigned long )regs.start));
    __CrestLoad(43232, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(43231, 12, (long long )((unsigned long )regs.start == (unsigned long )((void *)0)));
# 5629 "regex.c"
    if ((unsigned long )regs.start == (unsigned long )((void *)0)) {
      __CrestBranch(43234, 12376, 1);
      __CrestLoad(43236, (unsigned long )0, (long long )1);
      __CrestStore(43237, (unsigned long )(& __retres22));
# 5630 "regex.c"
      __retres22 = 1;
# 5630 "regex.c"
      goto return_label;
    } else {
      __CrestBranch(43235, 12378, 0);
      {
      __CrestLoad(43240, (unsigned long )(& regs.end), (long long )((unsigned long )regs.end));
      __CrestLoad(43239, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(43238, 12, (long long )((unsigned long )regs.end == (unsigned long )((void *)0)));
# 5629 "regex.c"
      if ((unsigned long )regs.end == (unsigned long )((void *)0)) {
        __CrestBranch(43241, 12379, 1);
        __CrestLoad(43243, (unsigned long )0, (long long )1);
        __CrestStore(43244, (unsigned long )(& __retres22));
# 5630 "regex.c"
        __retres22 = 1;
# 5630 "regex.c"
        goto return_label;
      } else {
        __CrestBranch(43242, 12381, 0);

      }
      }
    }
    }
  } else {
    __CrestBranch(43220, 12382, 0);

  }
  }
  {
  __CrestLoad(43247, (unsigned long )(& want_reg_info), (long long )want_reg_info);
  __CrestLoad(43246, (unsigned long )0, (long long )0);
  __CrestApply2(43245, 13, (long long )(want_reg_info != 0));
# 5634 "regex.c"
  if (want_reg_info != 0) {
    __CrestBranch(43248, 12384, 1);
# 5634 "regex.c"
    tmp___3 = & regs;
  } else {
    __CrestBranch(43249, 12385, 0);
# 5634 "regex.c"
    tmp___3 = (struct re_registers *)0;
  }
  }
  __CrestLoad(43250, (unsigned long )(& len), (long long )len);
  __CrestLoad(43251, (unsigned long )0, (long long )0);
  __CrestLoad(43252, (unsigned long )(& len), (long long )len);
# 5634 "regex.c"
  ret = re_search(& private_preg, string, len, 0, len, tmp___3);
  __CrestHandleReturn(43254, (long long )ret);
  __CrestStore(43253, (unsigned long )(& ret));
  {
  __CrestLoad(43257, (unsigned long )(& want_reg_info), (long long )want_reg_info);
  __CrestLoad(43256, (unsigned long )0, (long long )0);
  __CrestApply2(43255, 13, (long long )(want_reg_info != 0));
# 5639 "regex.c"
  if (want_reg_info != 0) {
    __CrestBranch(43258, 12388, 1);
    {
    __CrestLoad(43262, (unsigned long )(& ret), (long long )ret);
    __CrestLoad(43261, (unsigned long )0, (long long )0);
    __CrestApply2(43260, 17, (long long )(ret >= 0));
# 5641 "regex.c"
    if (ret >= 0) {
      __CrestBranch(43263, 12389, 1);
      __CrestLoad(43265, (unsigned long )0, (long long )0U);
      __CrestStore(43266, (unsigned long )(& r));
# 5645 "regex.c"
      r = 0U;
      {
# 5645 "regex.c"
      while (1) {
        while_continue: ;
        {
        __CrestLoad(43269, (unsigned long )(& r), (long long )r);
        __CrestLoad(43268, (unsigned long )(& nmatch), (long long )nmatch);
        __CrestApply2(43267, 16, (long long )((size_t )r < nmatch));
# 5645 "regex.c"
        if ((size_t )r < nmatch) {
          __CrestBranch(43270, 12394, 1);

        } else {
          __CrestBranch(43271, 12395, 0);
# 5645 "regex.c"
          goto while_break;
        }
        }
# 5647 "regex.c"
        mem_18 = pmatch + r;
# 5647 "regex.c"
        mem_19 = regs.start + r;
        __CrestLoad(43272, (unsigned long )mem_19, (long long )*mem_19);
        __CrestStore(43273, (unsigned long )(& mem_18->rm_so));
# 5647 "regex.c"
        mem_18->rm_so = *mem_19;
# 5648 "regex.c"
        mem_20 = pmatch + r;
# 5648 "regex.c"
        mem_21 = regs.end + r;
        __CrestLoad(43274, (unsigned long )mem_21, (long long )*mem_21);
        __CrestStore(43275, (unsigned long )(& mem_20->rm_eo));
# 5648 "regex.c"
        mem_20->rm_eo = *mem_21;
        __CrestLoad(43278, (unsigned long )(& r), (long long )r);
        __CrestLoad(43277, (unsigned long )0, (long long )1U);
        __CrestApply2(43276, 0, (long long )(r + 1U));
        __CrestStore(43279, (unsigned long )(& r));
# 5645 "regex.c"
        r ++;
      }
      while_break: ;
      }
    } else {
      __CrestBranch(43264, 12398, 0);

    }
    }
# 5653 "regex.c"
    free((void *)regs.start);
    __CrestClearStack(43280);
# 5654 "regex.c"
    free((void *)regs.end);
    __CrestClearStack(43281);
  } else {
    __CrestBranch(43259, 12400, 0);

  }
  }
  {
  __CrestLoad(43284, (unsigned long )(& ret), (long long )ret);
  __CrestLoad(43283, (unsigned long )0, (long long )0);
  __CrestApply2(43282, 17, (long long )(ret >= 0));
# 5658 "regex.c"
  if (ret >= 0) {
    __CrestBranch(43285, 12402, 1);
    __CrestLoad(43287, (unsigned long )0, (long long )0);
    __CrestStore(43288, (unsigned long )(& tmp___4));
# 5658 "regex.c"
    tmp___4 = 0;
  } else {
    __CrestBranch(43286, 12403, 0);
    __CrestLoad(43289, (unsigned long )0, (long long )1);
    __CrestStore(43290, (unsigned long )(& tmp___4));
# 5658 "regex.c"
    tmp___4 = 1;
  }
  }
  __CrestLoad(43291, (unsigned long )(& tmp___4), (long long )tmp___4);
  __CrestStore(43292, (unsigned long )(& __retres22));
# 5658 "regex.c"
  __retres22 = tmp___4;
  return_label:
  {
  __CrestLoad(43293, (unsigned long )(& __retres22), (long long )__retres22);
  __CrestReturn(43294);
# 5600 "regex.c"
  return (__retres22);
  }
}
}
# 5665 "regex.c"
size_t regerror(int errcode , regex_t const *preg , char *errbuf , size_t errbuf_size )
{
  char const *msg ;
  size_t msg_size ;
  char *tmp ;
  size_t tmp___0 ;
  char *mem_9 ;

  {
  __CrestCall(43297, 316);
  __CrestStore(43296, (unsigned long )(& errbuf_size));
  __CrestStore(43295, (unsigned long )(& errcode));
  {
  __CrestLoad(43300, (unsigned long )(& errcode), (long long )errcode);
  __CrestLoad(43299, (unsigned long )0, (long long )0);
  __CrestApply2(43298, 16, (long long )(errcode < 0));
# 5675 "regex.c"
  if (errcode < 0) {
    __CrestBranch(43301, 12407, 1);
# 5682 "regex.c"
    abort();
    __CrestClearStack(43303);
  } else {
    __CrestBranch(43302, 12408, 0);
    {
    __CrestLoad(43306, (unsigned long )(& errcode), (long long )errcode);
    __CrestLoad(43305, (unsigned long )0, (long long )((int )(sizeof(re_error_msgid) / sizeof(re_error_msgid[0]))));
    __CrestApply2(43304, 17, (long long )(errcode >= (int )(sizeof(re_error_msgid) / sizeof(re_error_msgid[0]))));
# 5675 "regex.c"
    if (errcode >= (int )(sizeof(re_error_msgid) / sizeof(re_error_msgid[0]))) {
      __CrestBranch(43307, 12409, 1);
# 5682 "regex.c"
      abort();
      __CrestClearStack(43309);
    } else {
      __CrestBranch(43308, 12410, 0);

    }
    }
  }
  }
  __CrestLoad(43310, (unsigned long )0, (long long )5);
# 5684 "regex.c"
  tmp = dcgettext((char const *)((void *)0), re_error_msgid[errcode], 5);
  __CrestClearStack(43311);
# 5684 "regex.c"
  msg = (char const *)tmp;
# 5686 "regex.c"
  tmp___0 = strlen(msg);
  __CrestHandleReturn(43313, (long long )tmp___0);
  __CrestStore(43312, (unsigned long )(& tmp___0));
  __CrestLoad(43316, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestLoad(43315, (unsigned long )0, (long long )1UL);
  __CrestApply2(43314, 0, (long long )(tmp___0 + 1UL));
  __CrestStore(43317, (unsigned long )(& msg_size));
# 5686 "regex.c"
  msg_size = tmp___0 + 1UL;
  {
  __CrestLoad(43320, (unsigned long )(& errbuf_size), (long long )errbuf_size);
  __CrestLoad(43319, (unsigned long )0, (long long )0UL);
  __CrestApply2(43318, 13, (long long )(errbuf_size != 0UL));
# 5688 "regex.c"
  if (errbuf_size != 0UL) {
    __CrestBranch(43321, 12413, 1);
    {
    __CrestLoad(43325, (unsigned long )(& msg_size), (long long )msg_size);
    __CrestLoad(43324, (unsigned long )(& errbuf_size), (long long )errbuf_size);
    __CrestApply2(43323, 14, (long long )(msg_size > errbuf_size));
# 5690 "regex.c"
    if (msg_size > errbuf_size) {
      __CrestBranch(43326, 12414, 1);
      __CrestLoad(43330, (unsigned long )(& errbuf_size), (long long )errbuf_size);
      __CrestLoad(43329, (unsigned long )0, (long long )1UL);
      __CrestApply2(43328, 1, (long long )(errbuf_size - 1UL));
# 5692 "regex.c"
      strncpy((char * __restrict )errbuf, (char const * __restrict )msg, errbuf_size - 1UL);
      __CrestClearStack(43331);
# 5693 "regex.c"
      mem_9 = errbuf + (errbuf_size - 1UL);
      __CrestLoad(43332, (unsigned long )0, (long long )(char)0);
      __CrestStore(43333, (unsigned long )mem_9);
# 5693 "regex.c"
      *mem_9 = (char)0;
    } else {
      __CrestBranch(43327, 12415, 0);
# 5696 "regex.c"
      strcpy((char * __restrict )errbuf, (char const * __restrict )msg);
      __CrestClearStack(43334);
    }
    }
  } else {
    __CrestBranch(43322, 12416, 0);

  }
  }
  {
  __CrestLoad(43335, (unsigned long )(& msg_size), (long long )msg_size);
  __CrestReturn(43336);
# 5699 "regex.c"
  return (msg_size);
  }
}
}
# 5705 "regex.c"
void regfree(regex_t *preg )
{


  {
  __CrestCall(43337, 317);

  {
  __CrestLoad(43340, (unsigned long )(& preg->buffer), (long long )((unsigned long )preg->buffer));
  __CrestLoad(43339, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(43338, 13, (long long )((unsigned long )preg->buffer != (unsigned long )((void *)0)));
# 5709 "regex.c"
  if ((unsigned long )preg->buffer != (unsigned long )((void *)0)) {
    __CrestBranch(43341, 12419, 1);
# 5710 "regex.c"
    free((void *)preg->buffer);
    __CrestClearStack(43343);
  } else {
    __CrestBranch(43342, 12420, 0);

  }
  }
# 5711 "regex.c"
  preg->buffer = (unsigned char *)((void *)0);
  __CrestLoad(43344, (unsigned long )0, (long long )0UL);
  __CrestStore(43345, (unsigned long )(& preg->allocated));
# 5713 "regex.c"
  preg->allocated = 0UL;
  __CrestLoad(43346, (unsigned long )0, (long long )0UL);
  __CrestStore(43347, (unsigned long )(& preg->used));
# 5714 "regex.c"
  preg->used = 0UL;
  {
  __CrestLoad(43350, (unsigned long )(& preg->fastmap), (long long )((unsigned long )preg->fastmap));
  __CrestLoad(43349, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(43348, 13, (long long )((unsigned long )preg->fastmap != (unsigned long )((void *)0)));
# 5716 "regex.c"
  if ((unsigned long )preg->fastmap != (unsigned long )((void *)0)) {
    __CrestBranch(43351, 12423, 1);
# 5717 "regex.c"
    free((void *)preg->fastmap);
    __CrestClearStack(43353);
  } else {
    __CrestBranch(43352, 12424, 0);

  }
  }
# 5718 "regex.c"
  preg->fastmap = (char *)((void *)0);
# 5719 "regex.c"
  preg->fastmap_accurate = 0U;
  {
  __CrestLoad(43356, (unsigned long )(& preg->translate), (long long )((unsigned long )preg->translate));
  __CrestLoad(43355, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(43354, 13, (long long )((unsigned long )preg->translate != (unsigned long )((void *)0)));
# 5721 "regex.c"
  if ((unsigned long )preg->translate != (unsigned long )((void *)0)) {
    __CrestBranch(43357, 12427, 1);
# 5722 "regex.c"
    free((void *)preg->translate);
    __CrestClearStack(43359);
  } else {
    __CrestBranch(43358, 12428, 0);

  }
  }
# 5723 "regex.c"
  preg->translate = (char *)((void *)0);

  {
  __CrestReturn(43360);
# 5705 "regex.c"
  return;
  }
}
}
void __globinit_regex(void)
{


  {
  __CrestInit();
}
}
