# 1 "./isSorted.cil.c"
# 1 "/home/yavuz/crest-ppc3/test//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./isSorted.cil.c"
# 12 "isSorted.c"
void __globinit_isSorted(void) ;
extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 131 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off_t;
# 132 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off64_t;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 48 "/usr/include/stdio.h"
typedef struct _IO_FILE FILE;
# 144 "/usr/include/libio.h"
struct _IO_FILE;
# 154 "/usr/include/libio.h"
typedef void _IO_lock_t;
# 160 "/usr/include/libio.h"
struct _IO_marker {
   struct _IO_marker *_next ;
   struct _IO_FILE *_sbuf ;
   int _pos ;
};
# 245 "/usr/include/libio.h"
struct _IO_FILE {
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   void *__pad3 ;
   void *__pad4 ;
   size_t __pad5 ;
   int _mode ;
   char _unused2[(15UL * sizeof(int ) - 4UL * sizeof(void *)) - sizeof(size_t )] ;
};
# 202 "../bin/../include/crest.h"
extern void __CrestInt(int *x ) __attribute__((__crest_skip__)) ;
# 170 "/usr/include/stdio.h"
extern struct _IO_FILE *stderr ;
# 356 "/usr/include/stdio.h"
extern int fprintf(FILE * __restrict __stream , char const * __restrict __format
                   , ...) ;
# 12 "isSorted.c"
int main(void)
{
  int a ;
  int b ;
  int c ;
  int __retres4 ;

  {
  __globinit_isSorted();
  __CrestCall(1, 1);
# 14 "isSorted.c"
  __CrestInt(& a);
# 15 "isSorted.c"
  __CrestInt(& b);
# 16 "isSorted.c"
  __CrestInt(& c);
  __CrestLoad(4, (unsigned long )(& a), (long long )a);
  __CrestLoad(3, (unsigned long )(& b), (long long )b);
  __CrestApply2(2, 14, (long long )(a > b));
# 18 "isSorted.c"
  if (a > b) {
    __CrestBranch(5, 3, 1);
# 19 "isSorted.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )"NO\n");
    __CrestClearStack(7);
    __CrestLoad(8, (unsigned long )0, (long long )1);
    __CrestStore(9, (unsigned long )(& __retres4));
# 20 "isSorted.c"
    __retres4 = 1;
# 20 "isSorted.c"
    goto return_label;
  } else {
    __CrestBranch(6, 6, 0);
    {
    __CrestLoad(12, (unsigned long )(& a), (long long )a);
    __CrestLoad(11, (unsigned long )(& c), (long long )c);
    __CrestApply2(10, 14, (long long )(a > c));
# 21 "isSorted.c"
    if (a > c) {
      __CrestBranch(13, 7, 1);
# 22 "isSorted.c"
      fprintf((FILE * __restrict )stderr, (char const * __restrict )"NO\n");
      __CrestClearStack(15);
      __CrestLoad(16, (unsigned long )0, (long long )1);
      __CrestStore(17, (unsigned long )(& __retres4));
# 23 "isSorted.c"
      __retres4 = 1;
# 23 "isSorted.c"
      goto return_label;
    } else {
      __CrestBranch(14, 10, 0);
      {
      __CrestLoad(20, (unsigned long )(& b), (long long )b);
      __CrestLoad(19, (unsigned long )(& c), (long long )c);
      __CrestApply2(18, 14, (long long )(b > c));
# 24 "isSorted.c"
      if (b > c) {
        __CrestBranch(21, 11, 1);
# 25 "isSorted.c"
        fprintf((FILE * __restrict )stderr, (char const * __restrict )"NO\n");
        __CrestClearStack(23);
        __CrestLoad(24, (unsigned long )0, (long long )1);
        __CrestStore(25, (unsigned long )(& __retres4));
# 26 "isSorted.c"
        __retres4 = 1;
# 26 "isSorted.c"
        goto return_label;
      } else {
        __CrestBranch(22, 14, 0);
# 28 "isSorted.c"
        fprintf((FILE * __restrict )stderr, (char const * __restrict )"YES\n");
        __CrestClearStack(26);
        __CrestLoad(27, (unsigned long )0, (long long )0);
        __CrestStore(28, (unsigned long )(& __retres4));
# 29 "isSorted.c"
        __retres4 = 0;
# 29 "isSorted.c"
        goto return_label;
      }
      }
    }
    }
  }
  return_label:
  {
  __CrestLoad(29, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(30);
# 12 "isSorted.c"
  return (__retres4);
  }
}
}
void __globinit_isSorted(void)
{


  {
  __CrestInit();
}
}
