# 1 "./rectangle.cil.c"
# 1 "/home/yavuz/crest-ppc3/test//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./rectangle.cil.c"
# 20 "rectangle.c"
void __globinit_rectangle(void) ;
extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 131 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off_t;
# 132 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off64_t;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 48 "/usr/include/stdio.h"
typedef struct _IO_FILE FILE;
# 144 "/usr/include/libio.h"
struct _IO_FILE;
# 154 "/usr/include/libio.h"
typedef void _IO_lock_t;
# 160 "/usr/include/libio.h"
struct _IO_marker {
   struct _IO_marker *_next ;
   struct _IO_FILE *_sbuf ;
   int _pos ;
};
# 245 "/usr/include/libio.h"
struct _IO_FILE {
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   void *__pad3 ;
   void *__pad4 ;
   size_t __pad5 ;
   int _mode ;
   char _unused2[(15UL * sizeof(int ) - 4UL * sizeof(void *)) - sizeof(size_t )] ;
};
# 202 "../bin/../include/crest.h"
extern void __CrestInt(int *x ) __attribute__((__crest_skip__)) ;
# 170 "/usr/include/stdio.h"
extern struct _IO_FILE *stderr ;
# 356 "/usr/include/stdio.h"
extern int fprintf(FILE * __restrict __stream , char const * __restrict __format
                   , ...) ;
# 20 "rectangle.c"
int main(void)
{
  int e1 ;
  int a12 ;
  int e2 ;
  int a23 ;
  int e3 ;
  int __retres6 ;

  {
  __globinit_rectangle();
  __CrestCall(1, 1);
# 22 "rectangle.c"
  __CrestInt(& e1);
# 23 "rectangle.c"
  __CrestInt(& a12);
# 24 "rectangle.c"
  __CrestInt(& e2);
# 25 "rectangle.c"
  __CrestInt(& a23);
# 26 "rectangle.c"
  __CrestInt(& e3);
  __CrestLoad(4, (unsigned long )(& e1), (long long )e1);
  __CrestLoad(3, (unsigned long )0, (long long )0);
  __CrestApply2(2, 15, (long long )(e1 <= 0));
# 28 "rectangle.c"
  if (e1 <= 0) {
    __CrestBranch(5, 3, 1);
# 29 "rectangle.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A QUADRILATERAL\n");
    __CrestClearStack(7);
    __CrestLoad(8, (unsigned long )0, (long long )1);
    __CrestStore(9, (unsigned long )(& __retres6));
# 30 "rectangle.c"
    __retres6 = 1;
# 30 "rectangle.c"
    goto return_label;
  } else {
    __CrestBranch(6, 6, 0);
    {
    __CrestLoad(12, (unsigned long )(& e2), (long long )e2);
    __CrestLoad(11, (unsigned long )0, (long long )0);
    __CrestApply2(10, 15, (long long )(e2 <= 0));
# 31 "rectangle.c"
    if (e2 <= 0) {
      __CrestBranch(13, 7, 1);
# 32 "rectangle.c"
      fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A QUADRILATERAL\n");
      __CrestClearStack(15);
      __CrestLoad(16, (unsigned long )0, (long long )1);
      __CrestStore(17, (unsigned long )(& __retres6));
# 33 "rectangle.c"
      __retres6 = 1;
# 33 "rectangle.c"
      goto return_label;
    } else {
      __CrestBranch(14, 10, 0);
      {
      __CrestLoad(20, (unsigned long )(& e3), (long long )e3);
      __CrestLoad(19, (unsigned long )0, (long long )0);
      __CrestApply2(18, 15, (long long )(e3 <= 0));
# 34 "rectangle.c"
      if (e3 <= 0) {
        __CrestBranch(21, 11, 1);
# 35 "rectangle.c"
        fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A QUADRILATERAL\n");
        __CrestClearStack(23);
        __CrestLoad(24, (unsigned long )0, (long long )1);
        __CrestStore(25, (unsigned long )(& __retres6));
# 36 "rectangle.c"
        __retres6 = 1;
# 36 "rectangle.c"
        goto return_label;
      } else {
        __CrestBranch(22, 14, 0);
        {
        __CrestLoad(28, (unsigned long )(& a12), (long long )a12);
        __CrestLoad(27, (unsigned long )0, (long long )0);
        __CrestApply2(26, 15, (long long )(a12 <= 0));
# 37 "rectangle.c"
        if (a12 <= 0) {
          __CrestBranch(29, 15, 1);
# 38 "rectangle.c"
          fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A QUADRILATERAL\n");
          __CrestClearStack(31);
          __CrestLoad(32, (unsigned long )0, (long long )1);
          __CrestStore(33, (unsigned long )(& __retres6));
# 39 "rectangle.c"
          __retres6 = 1;
# 39 "rectangle.c"
          goto return_label;
        } else {
          __CrestBranch(30, 18, 0);
          {
          __CrestLoad(36, (unsigned long )(& a23), (long long )a23);
          __CrestLoad(35, (unsigned long )0, (long long )0);
          __CrestApply2(34, 15, (long long )(a23 <= 0));
# 40 "rectangle.c"
          if (a23 <= 0) {
            __CrestBranch(37, 19, 1);
# 41 "rectangle.c"
            fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A QUADRILATERAL\n");
            __CrestClearStack(39);
            __CrestLoad(40, (unsigned long )0, (long long )1);
            __CrestStore(41, (unsigned long )(& __retres6));
# 42 "rectangle.c"
            __retres6 = 1;
# 42 "rectangle.c"
            goto return_label;
          } else {
            __CrestBranch(38, 22, 0);
            {
            __CrestLoad(46, (unsigned long )(& a12), (long long )a12);
            __CrestLoad(45, (unsigned long )(& a23), (long long )a23);
            __CrestApply2(44, 0, (long long )(a12 + a23));
            __CrestLoad(43, (unsigned long )0, (long long )360);
            __CrestApply2(42, 14, (long long )(a12 + a23 > 360));
# 44 "rectangle.c"
            if (a12 + a23 > 360) {
              __CrestBranch(47, 23, 1);
# 45 "rectangle.c"
              fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A QUADRILATERAL\n");
              __CrestClearStack(49);
              __CrestLoad(50, (unsigned long )0, (long long )1);
              __CrestStore(51, (unsigned long )(& __retres6));
# 46 "rectangle.c"
              __retres6 = 1;
# 46 "rectangle.c"
              goto return_label;
            } else {
              __CrestBranch(48, 26, 0);
              {
              __CrestLoad(56, (unsigned long )(& a12), (long long )a12);
              __CrestLoad(55, (unsigned long )(& a23), (long long )a23);
              __CrestApply2(54, 0, (long long )(a12 + a23));
              __CrestLoad(53, (unsigned long )0, (long long )180);
              __CrestApply2(52, 12, (long long )(a12 + a23 == 180));
# 48 "rectangle.c"
              if (a12 + a23 == 180) {
                __CrestBranch(57, 27, 1);
                {
                __CrestLoad(61, (unsigned long )(& e1), (long long )e1);
                __CrestLoad(60, (unsigned long )(& e3), (long long )e3);
                __CrestApply2(59, 12, (long long )(e1 == e3));
# 49 "rectangle.c"
                if (e1 == e3) {
                  __CrestBranch(62, 28, 1);
                  {
                  __CrestLoad(66, (unsigned long )(& e2), (long long )e2);
                  __CrestLoad(65, (unsigned long )(& e3), (long long )e3);
                  __CrestApply2(64, 12, (long long )(e2 == e3));
# 50 "rectangle.c"
                  if (e2 == e3) {
                    __CrestBranch(67, 29, 1);
                    {
                    __CrestLoad(71, (unsigned long )(& a12), (long long )a12);
                    __CrestLoad(70, (unsigned long )0, (long long )90);
                    __CrestApply2(69, 12, (long long )(a12 == 90));
# 51 "rectangle.c"
                    if (a12 == 90) {
                      __CrestBranch(72, 30, 1);
# 52 "rectangle.c"
                      fprintf((FILE * __restrict )stderr, (char const * __restrict )"SQUARE\n");
                      __CrestClearStack(74);
                      __CrestLoad(75, (unsigned long )0, (long long )0);
                      __CrestStore(76, (unsigned long )(& __retres6));
# 53 "rectangle.c"
                      __retres6 = 0;
# 53 "rectangle.c"
                      goto return_label;
                    } else {
                      __CrestBranch(73, 33, 0);
# 55 "rectangle.c"
                      fprintf((FILE * __restrict )stderr, (char const * __restrict )"RHOMBUS\n");
                      __CrestClearStack(77);
                      __CrestLoad(78, (unsigned long )0, (long long )0);
                      __CrestStore(79, (unsigned long )(& __retres6));
# 56 "rectangle.c"
                      __retres6 = 0;
# 56 "rectangle.c"
                      goto return_label;
                    }
                    }
                  } else {
                    __CrestBranch(68, 36, 0);
# 59 "rectangle.c"
                    fprintf((FILE * __restrict )stderr, (char const * __restrict )"PARALLELOGRAM\n");
                    __CrestClearStack(80);
                    __CrestLoad(81, (unsigned long )0, (long long )0);
                    __CrestStore(82, (unsigned long )(& __retres6));
# 60 "rectangle.c"
                    __retres6 = 0;
# 60 "rectangle.c"
                    goto return_label;
                  }
                  }
                } else {
                  __CrestBranch(63, 39, 0);
# 63 "rectangle.c"
                  fprintf((FILE * __restrict )stderr, (char const * __restrict )"TRAPEZOID\n");
                  __CrestClearStack(83);
                  __CrestLoad(84, (unsigned long )0, (long long )0);
                  __CrestStore(85, (unsigned long )(& __retres6));
# 64 "rectangle.c"
                  __retres6 = 0;
# 64 "rectangle.c"
                  goto return_label;
                }
                }
              } else {
                __CrestBranch(58, 42, 0);
# 67 "rectangle.c"
                fprintf((FILE * __restrict )stderr, (char const * __restrict )"QUADRILATERAL\n");
                __CrestClearStack(86);
                __CrestLoad(87, (unsigned long )0, (long long )0);
                __CrestStore(88, (unsigned long )(& __retres6));
# 68 "rectangle.c"
                __retres6 = 0;
# 68 "rectangle.c"
                goto return_label;
              }
              }
            }
            }
          }
          }
        }
        }
      }
      }
    }
    }
  }
  return_label:
  {
  __CrestLoad(89, (unsigned long )(& __retres6), (long long )__retres6);
  __CrestReturn(90);
# 20 "rectangle.c"
  return (__retres6);
  }
}
}
void __globinit_rectangle(void)
{


  {
  __CrestInit();
}
}
