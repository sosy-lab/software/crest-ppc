/* Author: Yavuz Köroğlu
 * Date: 2014 
 *
 * Categorizes a quadrilateral.
 * 
 */

#include <crest.h>
#include <stdio.h>
#include <pthread.h>

#define not_a_quadrilateral  "NOT A QUADRILATERAL\n"
#define square 			     "SQUARE\n"
#define rectangle 			 "RECTANGLE\n"
#define parallelogram      	 "PARALLELOGRAM\n"
#define trapezoid		   	 "TRAPEZOID\n"
#define quadrilateral        "QUADRILATERAL\n"
#define rhombus         	 "RHOMBUS\n"

int main(int argc, char *argv[]) {
    int e1, a12, e2, a23, e3;
	sscanf(argv[1], "%d", &e1);
	sscanf(argv[2], "%d", &a12);
	sscanf(argv[3], "%d", &e2);
	sscanf(argv[4], "%d", &a23);
	sscanf(argv[5], "%d", &e3);
    
	fprintf(stderr,"%d,%d,%d,%d,%d\n", e1,a12,e2,a23,e3);
    
    if (e1 <= 0) {
        fprintf(stderr, not_a_quadrilateral);
        return 1;
	} else if (e2 <= 0) {
        fprintf(stderr, not_a_quadrilateral);
        return 1;
	} else if (e3 <= 0) {
        fprintf(stderr, not_a_quadrilateral);
        return 1;
	} else if (a12 <= 0) {
        fprintf(stderr, not_a_quadrilateral);
        return 1;
	} else if (a23 <= 0) {
        fprintf(stderr, not_a_quadrilateral);
        return 1;
	} else {		
		if (a12 + a23 > 360) {
			fprintf(stderr, not_a_quadrilateral);
			return 1;
		} else {
			if (a12 + a23 == 180) {
				if (e1 == e3) {
					if (e2 == e3) {
						if (a12 == 90) {
							fprintf(stderr, square);
							return 0;
						} else {
							fprintf(stderr, rhombus);
							return 0;							
						}
					} else {
						fprintf(stderr, parallelogram);
						return 0;
					}
				} else {
					fprintf(stderr, trapezoid);
					return 0;					
				}
			} else {
				fprintf(stderr, quadrilateral);
				return 0;					
			}
		}
	}
}
