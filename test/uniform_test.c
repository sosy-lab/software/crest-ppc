/* Copyright (c) 2008, Jacob Burnim (jburnim@cs.berkeley.edu)
 *
 * This file is part of CREST, which is distributed under the revised
 * BSD license.  A copy of this license can be found in the file LICENSE.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See LICENSE
 * for details.
 */

#include <crest.h>
#include <stdio.h>
#include <assert.h>

int main(void) {
  int a, b;
  CREST_int(a);
  CREST_int(b);

    if (a == 3) { 
        if (a == b) {
            if (b != a) {
                fprintf(stderr, "GOAL!\n");
                assert(0);
            }
        }
    }

  return 0;
}
