// Program to print all prime factors
# include <crest.h>
# include <stdio.h>

/* 
int mod(int a, int base) {
	while(a > base)
		a -= base;
	
	return a;
}
*/
 
int main(void)
{
	int n;
    CREST_int(n);
    
    if (n > 1)
		return 0;
	
	/*
    // Print the number of 2s that divide n
    while (n / 2 == (n+1) / 2)
    {
        //fprintf(stderr, "%d ", 2);
        n = n/2;
    }
 
    // n must be odd at this point.  So we can skip one element (Note i = i +2)
    int i;
    for (i = 3; i <= n; i = i+2)
    {
        // While i divides n, print i and divide n
        while (mod(n,2) == 0)
        {
            //fprintf(stderr, "%d ", i);
            n = n/i;
        }
    }
 
    // This condition is to handle the case whien n is a prime number
    // greater than 2
    if (n > 2)
        //fprintf (stderr, "%d ", n);
	*/
    return 0;
}
