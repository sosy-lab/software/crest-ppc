/* Author: Yavuz Köroğlu
 * Date: 2014 
 *
 * Categorizes a triangle.
 * 
 */

#include <crest.h>
#include <stdio.h>

#define not_a_triangle  "NOT A TRIANGLE\n"
#define equilateral     "EQUILATERAL\n"
#define right_isosceles "RIGHT ISOSCELES\n"
#define isosceles       "ISOSCELES\n"
#define right_scalene   "RIGHT SCALENE\n"
#define scalene         "SCALENE\n"

int main(void) {
    int a, b, c;
    CREST_int(a);
    CREST_int(b);
    CREST_int(c);
    
    if (a <= 0) {
        fprintf(stderr, not_a_triangle);
        return 1;
    } else if (b <= 0) {
        fprintf(stderr, not_a_triangle);
        return 1;
    } else if (c <= 0) {
        fprintf(stderr, not_a_triangle);
        return 1;
    } else if (a + b <= c) {
        fprintf(stderr, not_a_triangle);
        return 1;
    } else if (a + c <=  b) {
        fprintf(stderr, not_a_triangle);
        return 1;
    } else if (b + c <= a) {
        fprintf(stderr, not_a_triangle);
        return 1;
    }
    
    if (a == b) {
        if (b == c) {
            fprintf(stderr, equilateral);
            return 0;
        } else {
            fprintf(stderr, isosceles);
            return 0;
        }
    } else if (a == c) {
        fprintf(stderr, isosceles);
        return 0;
    } else {
        if (b == c) {
            fprintf(stderr, isosceles);
            return 0;
        } else {
            fprintf(stderr, scalene);
            return 0;
        }
    }
}
