/* Author: Yavuz Köroğlu
 * Date: 2015
 * 
 */

#include <crest.h>
#include <stdio.h>

#define no "NO\n"
#define yes "YES\n"

int main(void) {
	int a, b, c;
    CREST_int(a);
    CREST_int(b);
    CREST_int(c);
	
	if (a > b) {
        fprintf(stderr, no);
		return 1;
	} else if (a > c) {
        fprintf(stderr, no);
		return 1;
	} else if (b > c) {
        fprintf(stderr, no);
		return 1;
	} else {
        fprintf(stderr, yes);
		return 0;
	}
}
